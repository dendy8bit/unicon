﻿namespace BEMN.MR761Obr.Measuring
{
    partial class MeasuringForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            BEMN.Devices.Structures.DateTimeStruct dateTimeStruct1 = new BEMN.Devices.Structures.DateTimeStruct();
            this._controlSignalsTabPage = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._commandBtn = new System.Windows.Forms.Button();
            this._commandComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox28 = new System.Windows.Forms.GroupBox();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this._currenGroupLabel = new System.Windows.Forms.Label();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this._groupCombo = new System.Windows.Forms.ComboBox();
            this._switchGroupButton = new System.Windows.Forms.Button();
            this.groupBox27 = new System.Windows.Forms.GroupBox();
            this.groupBox40 = new System.Windows.Forms.GroupBox();
            this._logicState = new BEMN.Forms.LedControl();
            this.stopLogic = new System.Windows.Forms.Button();
            this.startLogic = new System.Windows.Forms.Button();
            this.label314 = new System.Windows.Forms.Label();
            this._breakerOffBut = new System.Windows.Forms.Button();
            this.label100 = new System.Windows.Forms.Label();
            this._breakerOnBut = new System.Windows.Forms.Button();
            this.label101 = new System.Windows.Forms.Label();
            this._switchOn = new BEMN.Forms.LedControl();
            this._switchOff = new BEMN.Forms.LedControl();
            this._startOscBtn = new System.Windows.Forms.Button();
            this._availabilityFaultSystemJournal = new BEMN.Forms.LedControl();
            this._newRecordOscJournal = new BEMN.Forms.LedControl();
            this._newRecordAlarmJournal = new BEMN.Forms.LedControl();
            this._newRecordSystemJournal = new BEMN.Forms.LedControl();
            this._resetAnButton = new System.Windows.Forms.Button();
            this._resetAvailabilityFaultSystemJournalButton = new System.Windows.Forms.Button();
            this._resetOscJournalButton = new System.Windows.Forms.Button();
            this._resetAlarmJournalButton = new System.Windows.Forms.Button();
            this._resetSystemJournalButton = new System.Windows.Forms.Button();
            this.label227 = new System.Windows.Forms.Label();
            this.label225 = new System.Windows.Forms.Label();
            this.label226 = new System.Windows.Forms.Label();
            this.label231 = new System.Windows.Forms.Label();
            this._discretTabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox49 = new System.Windows.Forms.GroupBox();
            this.label347 = new System.Windows.Forms.Label();
            this._faultDisable2 = new BEMN.Forms.LedControl();
            this.label346 = new System.Windows.Forms.Label();
            this.label345 = new System.Windows.Forms.Label();
            this.label311 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this._faultDisable1 = new BEMN.Forms.LedControl();
            this._faultSwithON = new BEMN.Forms.LedControl();
            this._faultManage = new BEMN.Forms.LedControl();
            this._faultBlockCon = new BEMN.Forms.LedControl();
            this._faultOut = new BEMN.Forms.LedControl();
            this.groupBox38 = new System.Windows.Forms.GroupBox();
            this.groupBox39 = new System.Windows.Forms.GroupBox();
            this._fSpl1 = new BEMN.Forms.LedControl();
            this.label315 = new System.Windows.Forms.Label();
            this.label316 = new System.Windows.Forms.Label();
            this._fSpl2 = new BEMN.Forms.LedControl();
            this.label317 = new System.Windows.Forms.Label();
            this._fSpl3 = new BEMN.Forms.LedControl();
            this._fSpl5 = new BEMN.Forms.LedControl();
            this.label320 = new System.Windows.Forms.Label();
            this.label319 = new System.Windows.Forms.Label();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.label344 = new System.Windows.Forms.Label();
            this._faultSwitchOff = new BEMN.Forms.LedControl();
            this._faultAlarmJournal = new BEMN.Forms.LedControl();
            this.label192 = new System.Windows.Forms.Label();
            this._faultOsc = new BEMN.Forms.LedControl();
            this.label193 = new System.Windows.Forms.Label();
            this._faultModule5 = new BEMN.Forms.LedControl();
            this._faultSystemJournal = new BEMN.Forms.LedControl();
            this.label200 = new System.Windows.Forms.Label();
            this.label194 = new System.Windows.Forms.Label();
            this._faultGroupsOfSetpoints = new BEMN.Forms.LedControl();
            this.label201 = new System.Windows.Forms.Label();
            this._faultModule4 = new BEMN.Forms.LedControl();
            this._faultLogic = new BEMN.Forms.LedControl();
            this.label79 = new System.Windows.Forms.Label();
            this._faultSetpoints = new BEMN.Forms.LedControl();
            this.label202 = new System.Windows.Forms.Label();
            this.label195 = new System.Windows.Forms.Label();
            this._faultPass = new BEMN.Forms.LedControl();
            this.label203 = new System.Windows.Forms.Label();
            this._faultModule3 = new BEMN.Forms.LedControl();
            this.label196 = new System.Windows.Forms.Label();
            this._faultSoftware = new BEMN.Forms.LedControl();
            this.label205 = new System.Windows.Forms.Label();
            this._faultModule2 = new BEMN.Forms.LedControl();
            this._faultHardware = new BEMN.Forms.LedControl();
            this.label206 = new System.Windows.Forms.Label();
            this.label197 = new System.Windows.Forms.Label();
            this._faultModule1 = new BEMN.Forms.LedControl();
            this.label198 = new System.Windows.Forms.Label();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this._vz16 = new BEMN.Forms.LedControl();
            this.label111 = new System.Windows.Forms.Label();
            this._vz15 = new BEMN.Forms.LedControl();
            this.label112 = new System.Windows.Forms.Label();
            this._vz14 = new BEMN.Forms.LedControl();
            this.label113 = new System.Windows.Forms.Label();
            this._vz13 = new BEMN.Forms.LedControl();
            this.label114 = new System.Windows.Forms.Label();
            this._vz12 = new BEMN.Forms.LedControl();
            this.label115 = new System.Windows.Forms.Label();
            this._vz11 = new BEMN.Forms.LedControl();
            this.label116 = new System.Windows.Forms.Label();
            this._vz10 = new BEMN.Forms.LedControl();
            this.label117 = new System.Windows.Forms.Label();
            this._vz9 = new BEMN.Forms.LedControl();
            this.label118 = new System.Windows.Forms.Label();
            this._vz8 = new BEMN.Forms.LedControl();
            this.label119 = new System.Windows.Forms.Label();
            this._vz7 = new BEMN.Forms.LedControl();
            this.label120 = new System.Windows.Forms.Label();
            this._vz6 = new BEMN.Forms.LedControl();
            this.label121 = new System.Windows.Forms.Label();
            this._vz5 = new BEMN.Forms.LedControl();
            this.label122 = new System.Windows.Forms.Label();
            this._vz4 = new BEMN.Forms.LedControl();
            this.label123 = new System.Windows.Forms.Label();
            this._vz3 = new BEMN.Forms.LedControl();
            this.label124 = new System.Windows.Forms.Label();
            this._vz2 = new BEMN.Forms.LedControl();
            this.label125 = new System.Windows.Forms.Label();
            this._vz1 = new BEMN.Forms.LedControl();
            this.label126 = new System.Windows.Forms.Label();
            this._dateTimeControl = new BEMN.Forms.DateTimeControl();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this._fault = new BEMN.Forms.LedControl();
            this.label210 = new System.Windows.Forms.Label();
            this._faultOff = new BEMN.Forms.LedControl();
            this.label21 = new System.Windows.Forms.Label();
            this._acceleration = new BEMN.Forms.LedControl();
            this._auto4Led = new System.Windows.Forms.Label();
            this._signaling = new BEMN.Forms.LedControl();
            this._auto2Led = new System.Windows.Forms.Label();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this._ssl32 = new BEMN.Forms.LedControl();
            this.label237 = new System.Windows.Forms.Label();
            this._ssl31 = new BEMN.Forms.LedControl();
            this.label238 = new System.Windows.Forms.Label();
            this._ssl30 = new BEMN.Forms.LedControl();
            this.label239 = new System.Windows.Forms.Label();
            this._ssl29 = new BEMN.Forms.LedControl();
            this.label240 = new System.Windows.Forms.Label();
            this._ssl28 = new BEMN.Forms.LedControl();
            this.label241 = new System.Windows.Forms.Label();
            this._ssl27 = new BEMN.Forms.LedControl();
            this.label242 = new System.Windows.Forms.Label();
            this._ssl26 = new BEMN.Forms.LedControl();
            this.label243 = new System.Windows.Forms.Label();
            this._ssl25 = new BEMN.Forms.LedControl();
            this.label244 = new System.Windows.Forms.Label();
            this._ssl24 = new BEMN.Forms.LedControl();
            this.label245 = new System.Windows.Forms.Label();
            this._ssl23 = new BEMN.Forms.LedControl();
            this.label246 = new System.Windows.Forms.Label();
            this._ssl22 = new BEMN.Forms.LedControl();
            this.label247 = new System.Windows.Forms.Label();
            this._ssl21 = new BEMN.Forms.LedControl();
            this.label248 = new System.Windows.Forms.Label();
            this._ssl20 = new BEMN.Forms.LedControl();
            this.label249 = new System.Windows.Forms.Label();
            this._ssl19 = new BEMN.Forms.LedControl();
            this.label250 = new System.Windows.Forms.Label();
            this._ssl18 = new BEMN.Forms.LedControl();
            this.label251 = new System.Windows.Forms.Label();
            this._ssl17 = new BEMN.Forms.LedControl();
            this.label252 = new System.Windows.Forms.Label();
            this._ssl16 = new BEMN.Forms.LedControl();
            this.label253 = new System.Windows.Forms.Label();
            this._ssl15 = new BEMN.Forms.LedControl();
            this.label254 = new System.Windows.Forms.Label();
            this._ssl14 = new BEMN.Forms.LedControl();
            this.label255 = new System.Windows.Forms.Label();
            this._ssl13 = new BEMN.Forms.LedControl();
            this.label256 = new System.Windows.Forms.Label();
            this._ssl12 = new BEMN.Forms.LedControl();
            this.label257 = new System.Windows.Forms.Label();
            this._ssl11 = new BEMN.Forms.LedControl();
            this.label258 = new System.Windows.Forms.Label();
            this._ssl10 = new BEMN.Forms.LedControl();
            this.label259 = new System.Windows.Forms.Label();
            this._ssl9 = new BEMN.Forms.LedControl();
            this.label260 = new System.Windows.Forms.Label();
            this._ssl8 = new BEMN.Forms.LedControl();
            this.label261 = new System.Windows.Forms.Label();
            this._ssl7 = new BEMN.Forms.LedControl();
            this.label262 = new System.Windows.Forms.Label();
            this._ssl6 = new BEMN.Forms.LedControl();
            this.label263 = new System.Windows.Forms.Label();
            this._ssl5 = new BEMN.Forms.LedControl();
            this.label264 = new System.Windows.Forms.Label();
            this._ssl4 = new BEMN.Forms.LedControl();
            this.label265 = new System.Windows.Forms.Label();
            this._ssl3 = new BEMN.Forms.LedControl();
            this.label266 = new System.Windows.Forms.Label();
            this._ssl2 = new BEMN.Forms.LedControl();
            this.label267 = new System.Windows.Forms.Label();
            this._ssl1 = new BEMN.Forms.LedControl();
            this.label268 = new System.Windows.Forms.Label();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.diod12 = new BEMN.Forms.Diod();
            this.diod11 = new BEMN.Forms.Diod();
            this.diod10 = new BEMN.Forms.Diod();
            this.diod9 = new BEMN.Forms.Diod();
            this.diod8 = new BEMN.Forms.Diod();
            this.diod7 = new BEMN.Forms.Diod();
            this.diod6 = new BEMN.Forms.Diod();
            this.diod5 = new BEMN.Forms.Diod();
            this.diod4 = new BEMN.Forms.Diod();
            this.diod3 = new BEMN.Forms.Diod();
            this.diod2 = new BEMN.Forms.Diod();
            this.diod1 = new BEMN.Forms.Diod();
            this.label190 = new System.Windows.Forms.Label();
            this.label189 = new System.Windows.Forms.Label();
            this.label179 = new System.Windows.Forms.Label();
            this.label180 = new System.Windows.Forms.Label();
            this.label181 = new System.Windows.Forms.Label();
            this.label182 = new System.Windows.Forms.Label();
            this.label183 = new System.Windows.Forms.Label();
            this.label184 = new System.Windows.Forms.Label();
            this.label185 = new System.Windows.Forms.Label();
            this.label186 = new System.Windows.Forms.Label();
            this.label187 = new System.Windows.Forms.Label();
            this.label188 = new System.Windows.Forms.Label();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this._module34 = new BEMN.Forms.LedControl();
            this.label18 = new System.Windows.Forms.Label();
            this._module33 = new BEMN.Forms.LedControl();
            this.label20 = new System.Windows.Forms.Label();
            this._module18 = new BEMN.Forms.LedControl();
            this._module10 = new BEMN.Forms.LedControl();
            this.label161 = new System.Windows.Forms.Label();
            this._module32 = new BEMN.Forms.LedControl();
            this._module17 = new BEMN.Forms.LedControl();
            this.label138 = new System.Windows.Forms.Label();
            this.label162 = new System.Windows.Forms.Label();
            this._module31 = new BEMN.Forms.LedControl();
            this.label164 = new System.Windows.Forms.Label();
            this.label139 = new System.Windows.Forms.Label();
            this._module9 = new BEMN.Forms.LedControl();
            this._module30 = new BEMN.Forms.LedControl();
            this._module16 = new BEMN.Forms.LedControl();
            this.label140 = new System.Windows.Forms.Label();
            this.label165 = new System.Windows.Forms.Label();
            this._module29 = new BEMN.Forms.LedControl();
            this.label163 = new System.Windows.Forms.Label();
            this.label141 = new System.Windows.Forms.Label();
            this._module15 = new BEMN.Forms.LedControl();
            this._module28 = new BEMN.Forms.LedControl();
            this._module5 = new BEMN.Forms.LedControl();
            this.label142 = new System.Windows.Forms.Label();
            this.label169 = new System.Windows.Forms.Label();
            this._module27 = new BEMN.Forms.LedControl();
            this._module8 = new BEMN.Forms.LedControl();
            this.label232 = new System.Windows.Forms.Label();
            this._module14 = new BEMN.Forms.LedControl();
            this._module26 = new BEMN.Forms.LedControl();
            this.label176 = new System.Windows.Forms.Label();
            this.label127 = new System.Windows.Forms.Label();
            this.label170 = new System.Windows.Forms.Label();
            this._module25 = new BEMN.Forms.LedControl();
            this.label128 = new System.Windows.Forms.Label();
            this.label166 = new System.Windows.Forms.Label();
            this._module13 = new BEMN.Forms.LedControl();
            this._module24 = new BEMN.Forms.LedControl();
            this._module1 = new BEMN.Forms.LedControl();
            this.label129 = new System.Windows.Forms.Label();
            this.label171 = new System.Windows.Forms.Label();
            this._module23 = new BEMN.Forms.LedControl();
            this._module7 = new BEMN.Forms.LedControl();
            this.label130 = new System.Windows.Forms.Label();
            this._module12 = new BEMN.Forms.LedControl();
            this._module22 = new BEMN.Forms.LedControl();
            this.label175 = new System.Windows.Forms.Label();
            this.label131 = new System.Windows.Forms.Label();
            this.label177 = new System.Windows.Forms.Label();
            this._module21 = new BEMN.Forms.LedControl();
            this.label167 = new System.Windows.Forms.Label();
            this.label132 = new System.Windows.Forms.Label();
            this._module11 = new BEMN.Forms.LedControl();
            this._module20 = new BEMN.Forms.LedControl();
            this.label178 = new System.Windows.Forms.Label();
            this.label133 = new System.Windows.Forms.Label();
            this._module2 = new BEMN.Forms.LedControl();
            this._module19 = new BEMN.Forms.LedControl();
            this.label134 = new System.Windows.Forms.Label();
            this._module6 = new BEMN.Forms.LedControl();
            this.label168 = new System.Windows.Forms.Label();
            this.label174 = new System.Windows.Forms.Label();
            this._module3 = new BEMN.Forms.LedControl();
            this.label173 = new System.Windows.Forms.Label();
            this.label172 = new System.Windows.Forms.Label();
            this._module4 = new BEMN.Forms.LedControl();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this._vls16 = new BEMN.Forms.LedControl();
            this.label63 = new System.Windows.Forms.Label();
            this._vls15 = new BEMN.Forms.LedControl();
            this.label64 = new System.Windows.Forms.Label();
            this._vls14 = new BEMN.Forms.LedControl();
            this.label65 = new System.Windows.Forms.Label();
            this._vls13 = new BEMN.Forms.LedControl();
            this.label66 = new System.Windows.Forms.Label();
            this._vls12 = new BEMN.Forms.LedControl();
            this.label67 = new System.Windows.Forms.Label();
            this._vls11 = new BEMN.Forms.LedControl();
            this.label68 = new System.Windows.Forms.Label();
            this._vls10 = new BEMN.Forms.LedControl();
            this.label69 = new System.Windows.Forms.Label();
            this._vls9 = new BEMN.Forms.LedControl();
            this.label70 = new System.Windows.Forms.Label();
            this._vls8 = new BEMN.Forms.LedControl();
            this.label71 = new System.Windows.Forms.Label();
            this._vls7 = new BEMN.Forms.LedControl();
            this.label72 = new System.Windows.Forms.Label();
            this._vls6 = new BEMN.Forms.LedControl();
            this.label73 = new System.Windows.Forms.Label();
            this._vls5 = new BEMN.Forms.LedControl();
            this.label74 = new System.Windows.Forms.Label();
            this._vls4 = new BEMN.Forms.LedControl();
            this.label75 = new System.Windows.Forms.Label();
            this._vls3 = new BEMN.Forms.LedControl();
            this.label76 = new System.Windows.Forms.Label();
            this._vls2 = new BEMN.Forms.LedControl();
            this.label77 = new System.Windows.Forms.Label();
            this._vls1 = new BEMN.Forms.LedControl();
            this.label78 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this._ls16 = new BEMN.Forms.LedControl();
            this.label47 = new System.Windows.Forms.Label();
            this._ls15 = new BEMN.Forms.LedControl();
            this.label48 = new System.Windows.Forms.Label();
            this._ls14 = new BEMN.Forms.LedControl();
            this.label49 = new System.Windows.Forms.Label();
            this._ls13 = new BEMN.Forms.LedControl();
            this.label50 = new System.Windows.Forms.Label();
            this._ls12 = new BEMN.Forms.LedControl();
            this.label51 = new System.Windows.Forms.Label();
            this._ls11 = new BEMN.Forms.LedControl();
            this.label52 = new System.Windows.Forms.Label();
            this._ls10 = new BEMN.Forms.LedControl();
            this.label53 = new System.Windows.Forms.Label();
            this._ls9 = new BEMN.Forms.LedControl();
            this.label54 = new System.Windows.Forms.Label();
            this._ls8 = new BEMN.Forms.LedControl();
            this.label55 = new System.Windows.Forms.Label();
            this._ls7 = new BEMN.Forms.LedControl();
            this.label56 = new System.Windows.Forms.Label();
            this._ls6 = new BEMN.Forms.LedControl();
            this.label57 = new System.Windows.Forms.Label();
            this._ls5 = new BEMN.Forms.LedControl();
            this.label58 = new System.Windows.Forms.Label();
            this._ls4 = new BEMN.Forms.LedControl();
            this.label59 = new System.Windows.Forms.Label();
            this._ls3 = new BEMN.Forms.LedControl();
            this.label60 = new System.Windows.Forms.Label();
            this._ls2 = new BEMN.Forms.LedControl();
            this.label61 = new System.Windows.Forms.Label();
            this._ls1 = new BEMN.Forms.LedControl();
            this.label62 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this._k2 = new BEMN.Forms.LedControl();
            this.label95 = new System.Windows.Forms.Label();
            this._k1 = new BEMN.Forms.LedControl();
            this.label97 = new System.Windows.Forms.Label();
            this._d72 = new BEMN.Forms.LedControl();
            this.label9 = new System.Windows.Forms.Label();
            this._d71 = new BEMN.Forms.LedControl();
            this.label10 = new System.Windows.Forms.Label();
            this._d70 = new BEMN.Forms.LedControl();
            this.label11 = new System.Windows.Forms.Label();
            this._d69 = new BEMN.Forms.LedControl();
            this.label13 = new System.Windows.Forms.Label();
            this._d68 = new BEMN.Forms.LedControl();
            this.label14 = new System.Windows.Forms.Label();
            this._d67 = new BEMN.Forms.LedControl();
            this.label15 = new System.Windows.Forms.Label();
            this._d66 = new BEMN.Forms.LedControl();
            this.label16 = new System.Windows.Forms.Label();
            this._d65 = new BEMN.Forms.LedControl();
            this.label17 = new System.Windows.Forms.Label();
            this._d64 = new BEMN.Forms.LedControl();
            this.label19 = new System.Windows.Forms.Label();
            this._d63 = new BEMN.Forms.LedControl();
            this.label80 = new System.Windows.Forms.Label();
            this._d62 = new BEMN.Forms.LedControl();
            this.label81 = new System.Windows.Forms.Label();
            this._d61 = new BEMN.Forms.LedControl();
            this.label82 = new System.Windows.Forms.Label();
            this._d60 = new BEMN.Forms.LedControl();
            this.label83 = new System.Windows.Forms.Label();
            this._d59 = new BEMN.Forms.LedControl();
            this.label84 = new System.Windows.Forms.Label();
            this._d58 = new BEMN.Forms.LedControl();
            this.label85 = new System.Windows.Forms.Label();
            this._d57 = new BEMN.Forms.LedControl();
            this.label86 = new System.Windows.Forms.Label();
            this._d56 = new BEMN.Forms.LedControl();
            this.label87 = new System.Windows.Forms.Label();
            this._d55 = new BEMN.Forms.LedControl();
            this.label88 = new System.Windows.Forms.Label();
            this._d54 = new BEMN.Forms.LedControl();
            this.label89 = new System.Windows.Forms.Label();
            this._d53 = new BEMN.Forms.LedControl();
            this.label90 = new System.Windows.Forms.Label();
            this._d52 = new BEMN.Forms.LedControl();
            this.label91 = new System.Windows.Forms.Label();
            this._d51 = new BEMN.Forms.LedControl();
            this.label92 = new System.Windows.Forms.Label();
            this._d50 = new BEMN.Forms.LedControl();
            this.label93 = new System.Windows.Forms.Label();
            this._d49 = new BEMN.Forms.LedControl();
            this.label94 = new System.Windows.Forms.Label();
            this._d48 = new BEMN.Forms.LedControl();
            this.label1 = new System.Windows.Forms.Label();
            this._d47 = new BEMN.Forms.LedControl();
            this.label2 = new System.Windows.Forms.Label();
            this._d46 = new BEMN.Forms.LedControl();
            this.label3 = new System.Windows.Forms.Label();
            this._d45 = new BEMN.Forms.LedControl();
            this.label4 = new System.Windows.Forms.Label();
            this._d44 = new BEMN.Forms.LedControl();
            this.label5 = new System.Windows.Forms.Label();
            this._d43 = new BEMN.Forms.LedControl();
            this.label6 = new System.Windows.Forms.Label();
            this._d42 = new BEMN.Forms.LedControl();
            this.label7 = new System.Windows.Forms.Label();
            this._d41 = new BEMN.Forms.LedControl();
            this.label8 = new System.Windows.Forms.Label();
            this._d40 = new BEMN.Forms.LedControl();
            this._d24 = new BEMN.Forms.LedControl();
            this.label233 = new System.Windows.Forms.Label();
            this._d8 = new BEMN.Forms.LedControl();
            this._d39 = new BEMN.Forms.LedControl();
            this.label39 = new System.Windows.Forms.Label();
            this.label234 = new System.Windows.Forms.Label();
            this._d38 = new BEMN.Forms.LedControl();
            this._d23 = new BEMN.Forms.LedControl();
            this.label235 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this._d37 = new BEMN.Forms.LedControl();
            this.label40 = new System.Windows.Forms.Label();
            this.label236 = new System.Windows.Forms.Label();
            this._d22 = new BEMN.Forms.LedControl();
            this._d36 = new BEMN.Forms.LedControl();
            this._d7 = new BEMN.Forms.LedControl();
            this.label271 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this._d35 = new BEMN.Forms.LedControl();
            this.label29 = new System.Windows.Forms.Label();
            this.label272 = new System.Windows.Forms.Label();
            this._d21 = new BEMN.Forms.LedControl();
            this._d34 = new BEMN.Forms.LedControl();
            this._d1 = new BEMN.Forms.LedControl();
            this.label273 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this._d33 = new BEMN.Forms.LedControl();
            this.label274 = new System.Windows.Forms.Label();
            this._d6 = new BEMN.Forms.LedControl();
            this._d20 = new BEMN.Forms.LedControl();
            this._d32 = new BEMN.Forms.LedControl();
            this.label23 = new System.Windows.Forms.Label();
            this.label275 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this._d31 = new BEMN.Forms.LedControl();
            this.label28 = new System.Windows.Forms.Label();
            this.label276 = new System.Windows.Forms.Label();
            this._d19 = new BEMN.Forms.LedControl();
            this._d30 = new BEMN.Forms.LedControl();
            this.label24 = new System.Windows.Forms.Label();
            this.label277 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this._d29 = new BEMN.Forms.LedControl();
            this._d5 = new BEMN.Forms.LedControl();
            this.label278 = new System.Windows.Forms.Label();
            this._d18 = new BEMN.Forms.LedControl();
            this._d28 = new BEMN.Forms.LedControl();
            this._d2 = new BEMN.Forms.LedControl();
            this.label279 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this._d27 = new BEMN.Forms.LedControl();
            this.label27 = new System.Windows.Forms.Label();
            this.label280 = new System.Windows.Forms.Label();
            this._d17 = new BEMN.Forms.LedControl();
            this._d26 = new BEMN.Forms.LedControl();
            this.label46 = new System.Windows.Forms.Label();
            this.label281 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this._d25 = new BEMN.Forms.LedControl();
            this.label282 = new System.Windows.Forms.Label();
            this._d4 = new BEMN.Forms.LedControl();
            this._d16 = new BEMN.Forms.LedControl();
            this._d3 = new BEMN.Forms.LedControl();
            this.label31 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this._d15 = new BEMN.Forms.LedControl();
            this._d9 = new BEMN.Forms.LedControl();
            this.label32 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this._d14 = new BEMN.Forms.LedControl();
            this.label37 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this._d10 = new BEMN.Forms.LedControl();
            this._d13 = new BEMN.Forms.LedControl();
            this.label36 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this._d11 = new BEMN.Forms.LedControl();
            this._d12 = new BEMN.Forms.LedControl();
            this.label35 = new System.Windows.Forms.Label();
            this._dataBaseTabControl = new System.Windows.Forms.TabControl();
            this._controlSignalsTabPage.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox28.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.groupBox27.SuspendLayout();
            this.groupBox40.SuspendLayout();
            this._discretTabPage1.SuspendLayout();
            this.groupBox49.SuspendLayout();
            this.groupBox38.SuspendLayout();
            this.groupBox39.SuspendLayout();
            this.groupBox19.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox21.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this._dataBaseTabControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // _controlSignalsTabPage
            // 
            this._controlSignalsTabPage.Controls.Add(this.groupBox1);
            this._controlSignalsTabPage.Controls.Add(this.groupBox28);
            this._controlSignalsTabPage.Controls.Add(this.groupBox27);
            this._controlSignalsTabPage.Location = new System.Drawing.Point(4, 22);
            this._controlSignalsTabPage.Name = "_controlSignalsTabPage";
            this._controlSignalsTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._controlSignalsTabPage.Size = new System.Drawing.Size(933, 599);
            this._controlSignalsTabPage.TabIndex = 2;
            this._controlSignalsTabPage.Text = "Управляющие сигналы";
            this._controlSignalsTabPage.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._commandBtn);
            this.groupBox1.Controls.Add(this._commandComboBox);
            this.groupBox1.Location = new System.Drawing.Point(329, 117);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(222, 57);
            this.groupBox1.TabIndex = 25;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Команды";
            // 
            // _commandBtn
            // 
            this._commandBtn.Location = new System.Drawing.Point(141, 16);
            this._commandBtn.Name = "_commandBtn";
            this._commandBtn.Size = new System.Drawing.Size(75, 23);
            this._commandBtn.TabIndex = 1;
            this._commandBtn.Text = "Выполнить";
            this._commandBtn.UseVisualStyleBackColor = true;
            this._commandBtn.Click += new System.EventHandler(this.CommandRunClick);
            // 
            // _commandComboBox
            // 
            this._commandComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._commandComboBox.FormattingEnabled = true;
            this._commandComboBox.Items.AddRange(new object[] {
            "Команда 1",
            "Команда 2",
            "Команда 3",
            "Команда 4",
            "Команда 5",
            "Команда 6",
            "Команда 7",
            "Команда 8",
            "Команда 9",
            "Команда 10",
            "Команда 11",
            "Команда 12",
            "Команда 13",
            "Команда 14",
            "Команда 15",
            "Команда 16",
            "Команда 17",
            "Команда 18",
            "Команда 19",
            "Команда 20",
            "Команда 21",
            "Команда 22",
            "Команда 23",
            "Команда 24",
            "Команда 25",
            "Команда 26",
            "Команда 27",
            "Команда 28",
            "Команда 29",
            "Команда 30",
            "Команда 31",
            "Команда 32"});
            this._commandComboBox.Location = new System.Drawing.Point(7, 18);
            this._commandComboBox.Name = "_commandComboBox";
            this._commandComboBox.Size = new System.Drawing.Size(128, 21);
            this._commandComboBox.TabIndex = 0;
            // 
            // groupBox28
            // 
            this.groupBox28.Controls.Add(this.groupBox16);
            this.groupBox28.Controls.Add(this.groupBox15);
            this.groupBox28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox28.Location = new System.Drawing.Point(328, 6);
            this.groupBox28.Name = "groupBox28";
            this.groupBox28.Size = new System.Drawing.Size(223, 101);
            this.groupBox28.TabIndex = 24;
            this.groupBox28.TabStop = false;
            this.groupBox28.Text = "Группа уставок";
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this._currenGroupLabel);
            this.groupBox16.Location = new System.Drawing.Point(114, 14);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(103, 81);
            this.groupBox16.TabIndex = 32;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Текущая группа";
            // 
            // _currenGroupLabel
            // 
            this._currenGroupLabel.AutoSize = true;
            this._currenGroupLabel.Location = new System.Drawing.Point(17, 33);
            this._currenGroupLabel.Name = "_currenGroupLabel";
            this._currenGroupLabel.Size = new System.Drawing.Size(62, 13);
            this._currenGroupLabel.TabIndex = 0;
            this._currenGroupLabel.Text = "Группа №1";
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this._groupCombo);
            this.groupBox15.Controls.Add(this._switchGroupButton);
            this.groupBox15.Location = new System.Drawing.Point(6, 14);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(102, 81);
            this.groupBox15.TabIndex = 32;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Выбрать группу";
            // 
            // _groupCombo
            // 
            this._groupCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._groupCombo.FormattingEnabled = true;
            this._groupCombo.Items.AddRange(new object[] {
            "Группа 1",
            "Группа 2",
            "Группа 3",
            "Группа 4",
            "Группа 5",
            "Группа 6"});
            this._groupCombo.Location = new System.Drawing.Point(6, 25);
            this._groupCombo.Name = "_groupCombo";
            this._groupCombo.Size = new System.Drawing.Size(86, 21);
            this._groupCombo.TabIndex = 31;
            // 
            // _switchGroupButton
            // 
            this._switchGroupButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._switchGroupButton.Location = new System.Drawing.Point(6, 52);
            this._switchGroupButton.Name = "_switchGroupButton";
            this._switchGroupButton.Size = new System.Drawing.Size(86, 23);
            this._switchGroupButton.TabIndex = 30;
            this._switchGroupButton.Text = "Переключить";
            this._switchGroupButton.UseVisualStyleBackColor = true;
            this._switchGroupButton.Click += new System.EventHandler(this._switchGroupButton_Click);
            // 
            // groupBox27
            // 
            this.groupBox27.Controls.Add(this.groupBox40);
            this.groupBox27.Controls.Add(this._breakerOffBut);
            this.groupBox27.Controls.Add(this.label100);
            this.groupBox27.Controls.Add(this._breakerOnBut);
            this.groupBox27.Controls.Add(this.label101);
            this.groupBox27.Controls.Add(this._switchOn);
            this.groupBox27.Controls.Add(this._switchOff);
            this.groupBox27.Controls.Add(this._startOscBtn);
            this.groupBox27.Controls.Add(this._availabilityFaultSystemJournal);
            this.groupBox27.Controls.Add(this._newRecordOscJournal);
            this.groupBox27.Controls.Add(this._newRecordAlarmJournal);
            this.groupBox27.Controls.Add(this._newRecordSystemJournal);
            this.groupBox27.Controls.Add(this._resetAnButton);
            this.groupBox27.Controls.Add(this._resetAvailabilityFaultSystemJournalButton);
            this.groupBox27.Controls.Add(this._resetOscJournalButton);
            this.groupBox27.Controls.Add(this._resetAlarmJournalButton);
            this.groupBox27.Controls.Add(this._resetSystemJournalButton);
            this.groupBox27.Controls.Add(this.label227);
            this.groupBox27.Controls.Add(this.label225);
            this.groupBox27.Controls.Add(this.label226);
            this.groupBox27.Controls.Add(this.label231);
            this.groupBox27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox27.Location = new System.Drawing.Point(6, 6);
            this.groupBox27.Name = "groupBox27";
            this.groupBox27.Size = new System.Drawing.Size(316, 305);
            this.groupBox27.TabIndex = 23;
            this.groupBox27.TabStop = false;
            this.groupBox27.Text = "Управляющие сигналы";
            // 
            // groupBox40
            // 
            this.groupBox40.Controls.Add(this._logicState);
            this.groupBox40.Controls.Add(this.stopLogic);
            this.groupBox40.Controls.Add(this.startLogic);
            this.groupBox40.Controls.Add(this.label314);
            this.groupBox40.Location = new System.Drawing.Point(6, 176);
            this.groupBox40.Name = "groupBox40";
            this.groupBox40.Size = new System.Drawing.Size(304, 60);
            this.groupBox40.TabIndex = 54;
            this.groupBox40.TabStop = false;
            this.groupBox40.Text = "Свободно программируемая логика";
            // 
            // _logicState
            // 
            this._logicState.Location = new System.Drawing.Point(6, 25);
            this._logicState.Name = "_logicState";
            this._logicState.Size = new System.Drawing.Size(13, 13);
            this._logicState.State = BEMN.Forms.LedState.Off;
            this._logicState.TabIndex = 17;
            // 
            // stopLogic
            // 
            this.stopLogic.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.stopLogic.Location = new System.Drawing.Point(223, 32);
            this.stopLogic.Name = "stopLogic";
            this.stopLogic.Size = new System.Drawing.Size(75, 23);
            this.stopLogic.TabIndex = 15;
            this.stopLogic.Text = "Остановить";
            this.stopLogic.UseVisualStyleBackColor = true;
            this.stopLogic.Click += new System.EventHandler(this.StopLogicBtnClick);
            // 
            // startLogic
            // 
            this.startLogic.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.startLogic.Location = new System.Drawing.Point(223, 10);
            this.startLogic.Name = "startLogic";
            this.startLogic.Size = new System.Drawing.Size(75, 23);
            this.startLogic.TabIndex = 16;
            this.startLogic.Text = "Запустить";
            this.startLogic.UseVisualStyleBackColor = true;
            this.startLogic.Click += new System.EventHandler(this.StartLogicBtnClick);
            // 
            // label314
            // 
            this.label314.AutoSize = true;
            this.label314.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label314.Location = new System.Drawing.Point(30, 25);
            this.label314.Name = "label314";
            this.label314.Size = new System.Drawing.Size(137, 13);
            this.label314.TabIndex = 14;
            this.label314.Text = "Состояние задачи логики";
            // 
            // _breakerOffBut
            // 
            this._breakerOffBut.Location = new System.Drawing.Point(235, 37);
            this._breakerOffBut.Name = "_breakerOffBut";
            this._breakerOffBut.Size = new System.Drawing.Size(75, 23);
            this._breakerOffBut.TabIndex = 53;
            this._breakerOffBut.Text = "Отключить";
            this._breakerOffBut.UseVisualStyleBackColor = true;
            this._breakerOffBut.Click += new System.EventHandler(this._breakerOffBut_Click);
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(30, 42);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(122, 13);
            this.label100.TabIndex = 52;
            this.label100.Text = "Выключатель включен";
            // 
            // _breakerOnBut
            // 
            this._breakerOnBut.Location = new System.Drawing.Point(235, 14);
            this._breakerOnBut.Name = "_breakerOnBut";
            this._breakerOnBut.Size = new System.Drawing.Size(75, 23);
            this._breakerOnBut.TabIndex = 51;
            this._breakerOnBut.Text = "Включить";
            this._breakerOnBut.UseVisualStyleBackColor = true;
            this._breakerOnBut.Click += new System.EventHandler(this._breakerOnBut_Click);
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(30, 19);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(127, 13);
            this.label101.TabIndex = 50;
            this.label101.Text = "Выключатель отключен";
            // 
            // _switchOn
            // 
            this._switchOn.Location = new System.Drawing.Point(6, 42);
            this._switchOn.Name = "_switchOn";
            this._switchOn.Size = new System.Drawing.Size(13, 13);
            this._switchOn.State = BEMN.Forms.LedState.Off;
            this._switchOn.TabIndex = 49;
            // 
            // _switchOff
            // 
            this._switchOff.Location = new System.Drawing.Point(6, 19);
            this._switchOff.Name = "_switchOff";
            this._switchOff.Size = new System.Drawing.Size(13, 13);
            this._switchOff.State = BEMN.Forms.LedState.Off;
            this._switchOff.TabIndex = 48;
            // 
            // _startOscBtn
            // 
            this._startOscBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._startOscBtn.Location = new System.Drawing.Point(6, 271);
            this._startOscBtn.Name = "_startOscBtn";
            this._startOscBtn.Size = new System.Drawing.Size(304, 23);
            this._startOscBtn.TabIndex = 14;
            this._startOscBtn.Text = "Пуск осциллографа";
            this._startOscBtn.UseVisualStyleBackColor = true;
            this._startOscBtn.Click += new System.EventHandler(this._startOscBtnClick);
            // 
            // _availabilityFaultSystemJournal
            // 
            this._availabilityFaultSystemJournal.Location = new System.Drawing.Point(6, 134);
            this._availabilityFaultSystemJournal.Name = "_availabilityFaultSystemJournal";
            this._availabilityFaultSystemJournal.Size = new System.Drawing.Size(13, 13);
            this._availabilityFaultSystemJournal.State = BEMN.Forms.LedState.Off;
            this._availabilityFaultSystemJournal.TabIndex = 13;
            // 
            // _newRecordOscJournal
            // 
            this._newRecordOscJournal.Location = new System.Drawing.Point(6, 111);
            this._newRecordOscJournal.Name = "_newRecordOscJournal";
            this._newRecordOscJournal.Size = new System.Drawing.Size(13, 13);
            this._newRecordOscJournal.State = BEMN.Forms.LedState.Off;
            this._newRecordOscJournal.TabIndex = 12;
            // 
            // _newRecordAlarmJournal
            // 
            this._newRecordAlarmJournal.Location = new System.Drawing.Point(6, 88);
            this._newRecordAlarmJournal.Name = "_newRecordAlarmJournal";
            this._newRecordAlarmJournal.Size = new System.Drawing.Size(13, 13);
            this._newRecordAlarmJournal.State = BEMN.Forms.LedState.Off;
            this._newRecordAlarmJournal.TabIndex = 11;
            // 
            // _newRecordSystemJournal
            // 
            this._newRecordSystemJournal.Location = new System.Drawing.Point(6, 65);
            this._newRecordSystemJournal.Name = "_newRecordSystemJournal";
            this._newRecordSystemJournal.Size = new System.Drawing.Size(13, 13);
            this._newRecordSystemJournal.State = BEMN.Forms.LedState.Off;
            this._newRecordSystemJournal.TabIndex = 10;
            // 
            // _resetAnButton
            // 
            this._resetAnButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._resetAnButton.Location = new System.Drawing.Point(6, 242);
            this._resetAnButton.Name = "_resetAnButton";
            this._resetAnButton.Size = new System.Drawing.Size(304, 23);
            this._resetAnButton.TabIndex = 9;
            this._resetAnButton.Text = "Сброс блинкеров";
            this._resetAnButton.UseVisualStyleBackColor = true;
            this._resetAnButton.Click += new System.EventHandler(this._resetAnButton_Click);
            // 
            // _resetAvailabilityFaultSystemJournalButton
            // 
            this._resetAvailabilityFaultSystemJournalButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._resetAvailabilityFaultSystemJournalButton.Location = new System.Drawing.Point(235, 129);
            this._resetAvailabilityFaultSystemJournalButton.Name = "_resetAvailabilityFaultSystemJournalButton";
            this._resetAvailabilityFaultSystemJournalButton.Size = new System.Drawing.Size(75, 23);
            this._resetAvailabilityFaultSystemJournalButton.TabIndex = 8;
            this._resetAvailabilityFaultSystemJournalButton.Text = "Сбросить";
            this._resetAvailabilityFaultSystemJournalButton.UseVisualStyleBackColor = true;
            this._resetAvailabilityFaultSystemJournalButton.Click += new System.EventHandler(this._resetAvailabilityFaultSystemJournalButton_Click);
            // 
            // _resetOscJournalButton
            // 
            this._resetOscJournalButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._resetOscJournalButton.Location = new System.Drawing.Point(235, 106);
            this._resetOscJournalButton.Name = "_resetOscJournalButton";
            this._resetOscJournalButton.Size = new System.Drawing.Size(75, 23);
            this._resetOscJournalButton.TabIndex = 7;
            this._resetOscJournalButton.Text = "Сбросить";
            this._resetOscJournalButton.UseVisualStyleBackColor = true;
            this._resetOscJournalButton.Click += new System.EventHandler(this._resetOscJournalButton_Click);
            // 
            // _resetAlarmJournalButton
            // 
            this._resetAlarmJournalButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._resetAlarmJournalButton.Location = new System.Drawing.Point(235, 83);
            this._resetAlarmJournalButton.Name = "_resetAlarmJournalButton";
            this._resetAlarmJournalButton.Size = new System.Drawing.Size(75, 23);
            this._resetAlarmJournalButton.TabIndex = 6;
            this._resetAlarmJournalButton.Text = "Сбросить";
            this._resetAlarmJournalButton.UseVisualStyleBackColor = true;
            this._resetAlarmJournalButton.Click += new System.EventHandler(this._resetAlarmJournalButton_Click);
            // 
            // _resetSystemJournalButton
            // 
            this._resetSystemJournalButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._resetSystemJournalButton.Location = new System.Drawing.Point(235, 60);
            this._resetSystemJournalButton.Name = "_resetSystemJournalButton";
            this._resetSystemJournalButton.Size = new System.Drawing.Size(75, 23);
            this._resetSystemJournalButton.TabIndex = 5;
            this._resetSystemJournalButton.Text = "Сбросить";
            this._resetSystemJournalButton.UseVisualStyleBackColor = true;
            this._resetSystemJournalButton.Click += new System.EventHandler(this._resetSystemJournalButton_Click);
            // 
            // label227
            // 
            this.label227.AutoSize = true;
            this.label227.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label227.Location = new System.Drawing.Point(30, 134);
            this.label227.Name = "label227";
            this.label227.Size = new System.Drawing.Size(166, 13);
            this.label227.TabIndex = 3;
            this.label227.Text = "Наличие неисправности по ЖС";
            // 
            // label225
            // 
            this.label225.AutoSize = true;
            this.label225.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label225.Location = new System.Drawing.Point(30, 111);
            this.label225.Name = "label225";
            this.label225.Size = new System.Drawing.Size(200, 13);
            this.label225.TabIndex = 2;
            this.label225.Text = "Новая запись журнала осциллографа";
            // 
            // label226
            // 
            this.label226.AutoSize = true;
            this.label226.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label226.Location = new System.Drawing.Point(30, 88);
            this.label226.Name = "label226";
            this.label226.Size = new System.Drawing.Size(172, 13);
            this.label226.TabIndex = 1;
            this.label226.Text = "Новая запись в журнале аварий";
            // 
            // label231
            // 
            this.label231.AutoSize = true;
            this.label231.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label231.Location = new System.Drawing.Point(30, 65);
            this.label231.Name = "label231";
            this.label231.Size = new System.Drawing.Size(181, 13);
            this.label231.TabIndex = 0;
            this.label231.Text = "Новая запись в журнале системы";
            // 
            // _discretTabPage1
            // 
            this._discretTabPage1.Controls.Add(this.groupBox49);
            this._discretTabPage1.Controls.Add(this.groupBox38);
            this._discretTabPage1.Controls.Add(this.groupBox19);
            this._discretTabPage1.Controls.Add(this.groupBox12);
            this._discretTabPage1.Controls.Add(this._dateTimeControl);
            this._discretTabPage1.Controls.Add(this.groupBox14);
            this._discretTabPage1.Controls.Add(this.groupBox21);
            this._discretTabPage1.Controls.Add(this.groupBox18);
            this._discretTabPage1.Controls.Add(this.groupBox17);
            this._discretTabPage1.Controls.Add(this.groupBox10);
            this._discretTabPage1.Controls.Add(this.groupBox9);
            this._discretTabPage1.Controls.Add(this.groupBox6);
            this._discretTabPage1.Location = new System.Drawing.Point(4, 22);
            this._discretTabPage1.Name = "_discretTabPage1";
            this._discretTabPage1.Padding = new System.Windows.Forms.Padding(3);
            this._discretTabPage1.Size = new System.Drawing.Size(933, 599);
            this._discretTabPage1.TabIndex = 1;
            this._discretTabPage1.Text = "Основные сигналы";
            this._discretTabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox49
            // 
            this.groupBox49.Controls.Add(this.label347);
            this.groupBox49.Controls.Add(this._faultDisable2);
            this.groupBox49.Controls.Add(this.label346);
            this.groupBox49.Controls.Add(this.label345);
            this.groupBox49.Controls.Add(this.label311);
            this.groupBox49.Controls.Add(this.label98);
            this.groupBox49.Controls.Add(this.label22);
            this.groupBox49.Controls.Add(this._faultDisable1);
            this.groupBox49.Controls.Add(this._faultSwithON);
            this.groupBox49.Controls.Add(this._faultManage);
            this.groupBox49.Controls.Add(this._faultBlockCon);
            this.groupBox49.Controls.Add(this._faultOut);
            this.groupBox49.Location = new System.Drawing.Point(573, 386);
            this.groupBox49.Name = "groupBox49";
            this.groupBox49.Size = new System.Drawing.Size(179, 146);
            this.groupBox49.TabIndex = 48;
            this.groupBox49.TabStop = false;
            this.groupBox49.Text = "Неисправности выключателя";
            // 
            // label347
            // 
            this.label347.AutoSize = true;
            this.label347.Location = new System.Drawing.Point(25, 118);
            this.label347.Name = "label347";
            this.label347.Size = new System.Drawing.Size(105, 13);
            this.label347.TabIndex = 13;
            this.label347.Text = "Цепи отключения 2";
            // 
            // _faultDisable2
            // 
            this._faultDisable2.Location = new System.Drawing.Point(6, 117);
            this._faultDisable2.Name = "_faultDisable2";
            this._faultDisable2.Size = new System.Drawing.Size(13, 13);
            this._faultDisable2.State = BEMN.Forms.LedState.Off;
            this._faultDisable2.TabIndex = 12;
            // 
            // label346
            // 
            this.label346.AutoSize = true;
            this.label346.Location = new System.Drawing.Point(25, 98);
            this.label346.Name = "label346";
            this.label346.Size = new System.Drawing.Size(105, 13);
            this.label346.TabIndex = 11;
            this.label346.Text = "Цепи отключения 1";
            // 
            // label345
            // 
            this.label345.AutoSize = true;
            this.label345.Location = new System.Drawing.Point(25, 79);
            this.label345.Name = "label345";
            this.label345.Size = new System.Drawing.Size(91, 13);
            this.label345.TabIndex = 10;
            this.label345.Text = "Цепи включения";
            // 
            // label311
            // 
            this.label311.AutoSize = true;
            this.label311.Location = new System.Drawing.Point(25, 60);
            this.label311.Name = "label311";
            this.label311.Size = new System.Drawing.Size(69, 13);
            this.label311.TabIndex = 9;
            this.label311.Text = "Управления";
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(25, 41);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(105, 13);
            this.label98.TabIndex = 7;
            this.label98.Text = "По блок-контактам";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(25, 22);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(55, 13);
            this.label22.TabIndex = 6;
            this.label22.Text = "Внешняя ";
            // 
            // _faultDisable1
            // 
            this._faultDisable1.Location = new System.Drawing.Point(6, 98);
            this._faultDisable1.Name = "_faultDisable1";
            this._faultDisable1.Size = new System.Drawing.Size(13, 13);
            this._faultDisable1.State = BEMN.Forms.LedState.Off;
            this._faultDisable1.TabIndex = 5;
            // 
            // _faultSwithON
            // 
            this._faultSwithON.Location = new System.Drawing.Point(6, 79);
            this._faultSwithON.Name = "_faultSwithON";
            this._faultSwithON.Size = new System.Drawing.Size(13, 13);
            this._faultSwithON.State = BEMN.Forms.LedState.Off;
            this._faultSwithON.TabIndex = 4;
            // 
            // _faultManage
            // 
            this._faultManage.Location = new System.Drawing.Point(6, 60);
            this._faultManage.Name = "_faultManage";
            this._faultManage.Size = new System.Drawing.Size(13, 13);
            this._faultManage.State = BEMN.Forms.LedState.Off;
            this._faultManage.TabIndex = 3;
            // 
            // _faultBlockCon
            // 
            this._faultBlockCon.Location = new System.Drawing.Point(6, 41);
            this._faultBlockCon.Name = "_faultBlockCon";
            this._faultBlockCon.Size = new System.Drawing.Size(13, 13);
            this._faultBlockCon.State = BEMN.Forms.LedState.Off;
            this._faultBlockCon.TabIndex = 1;
            // 
            // _faultOut
            // 
            this._faultOut.Location = new System.Drawing.Point(6, 22);
            this._faultOut.Name = "_faultOut";
            this._faultOut.Size = new System.Drawing.Size(13, 13);
            this._faultOut.State = BEMN.Forms.LedState.Off;
            this._faultOut.TabIndex = 0;
            // 
            // groupBox38
            // 
            this.groupBox38.Controls.Add(this.groupBox39);
            this.groupBox38.Controls.Add(this._fSpl5);
            this.groupBox38.Controls.Add(this.label320);
            this.groupBox38.Controls.Add(this.label319);
            this.groupBox38.Location = new System.Drawing.Point(443, 386);
            this.groupBox38.Name = "groupBox38";
            this.groupBox38.Size = new System.Drawing.Size(122, 146);
            this.groupBox38.TabIndex = 47;
            this.groupBox38.TabStop = false;
            this.groupBox38.Text = "Ошибки СПЛ";
            // 
            // groupBox39
            // 
            this.groupBox39.Controls.Add(this._fSpl1);
            this.groupBox39.Controls.Add(this.label315);
            this.groupBox39.Controls.Add(this.label316);
            this.groupBox39.Controls.Add(this._fSpl2);
            this.groupBox39.Controls.Add(this.label317);
            this.groupBox39.Controls.Add(this._fSpl3);
            this.groupBox39.Location = new System.Drawing.Point(6, 44);
            this.groupBox39.Name = "groupBox39";
            this.groupBox39.Size = new System.Drawing.Size(110, 83);
            this.groupBox39.TabIndex = 32;
            this.groupBox39.TabStop = false;
            this.groupBox39.Text = "Ошибки CRC";
            // 
            // _fSpl1
            // 
            this._fSpl1.Location = new System.Drawing.Point(6, 19);
            this._fSpl1.Name = "_fSpl1";
            this._fSpl1.Size = new System.Drawing.Size(13, 13);
            this._fSpl1.State = BEMN.Forms.LedState.Off;
            this._fSpl1.TabIndex = 25;
            // 
            // label315
            // 
            this.label315.AutoSize = true;
            this.label315.Location = new System.Drawing.Point(25, 19);
            this.label315.Name = "label315";
            this.label315.Size = new System.Drawing.Size(54, 13);
            this.label315.TabIndex = 24;
            this.label315.Text = "Констант";
            // 
            // label316
            // 
            this.label316.AutoSize = true;
            this.label316.Location = new System.Drawing.Point(25, 38);
            this.label316.Name = "label316";
            this.label316.Size = new System.Drawing.Size(70, 13);
            this.label316.TabIndex = 26;
            this.label316.Text = "Разрешения";
            // 
            // _fSpl2
            // 
            this._fSpl2.Location = new System.Drawing.Point(6, 38);
            this._fSpl2.Name = "_fSpl2";
            this._fSpl2.Size = new System.Drawing.Size(13, 13);
            this._fSpl2.State = BEMN.Forms.LedState.Off;
            this._fSpl2.TabIndex = 27;
            // 
            // label317
            // 
            this.label317.AutoSize = true;
            this.label317.Location = new System.Drawing.Point(25, 57);
            this.label317.Name = "label317";
            this.label317.Size = new System.Drawing.Size(68, 13);
            this.label317.TabIndex = 28;
            this.label317.Text = "Программы";
            // 
            // _fSpl3
            // 
            this._fSpl3.Location = new System.Drawing.Point(6, 57);
            this._fSpl3.Name = "_fSpl3";
            this._fSpl3.Size = new System.Drawing.Size(13, 13);
            this._fSpl3.State = BEMN.Forms.LedState.Off;
            this._fSpl3.TabIndex = 29;
            // 
            // _fSpl5
            // 
            this._fSpl5.Location = new System.Drawing.Point(4, 19);
            this._fSpl5.Name = "_fSpl5";
            this._fSpl5.Size = new System.Drawing.Size(13, 13);
            this._fSpl5.State = BEMN.Forms.LedState.Off;
            this._fSpl5.TabIndex = 31;
            // 
            // label320
            // 
            this.label320.AutoSize = true;
            this.label320.Location = new System.Drawing.Point(17, 28);
            this.label320.Name = "label320";
            this.label320.Size = new System.Drawing.Size(66, 13);
            this.label320.TabIndex = 30;
            this.label320.Text = "программы";
            // 
            // label319
            // 
            this.label319.AutoSize = true;
            this.label319.Location = new System.Drawing.Point(16, 14);
            this.label319.Name = "label319";
            this.label319.Size = new System.Drawing.Size(105, 13);
            this.label319.TabIndex = 30;
            this.label319.Text = "В ходе выполнения";
            // 
            // groupBox19
            // 
            this.groupBox19.BackColor = System.Drawing.Color.Transparent;
            this.groupBox19.Controls.Add(this.label344);
            this.groupBox19.Controls.Add(this._faultSwitchOff);
            this.groupBox19.Controls.Add(this._faultAlarmJournal);
            this.groupBox19.Controls.Add(this.label192);
            this.groupBox19.Controls.Add(this._faultOsc);
            this.groupBox19.Controls.Add(this.label193);
            this.groupBox19.Controls.Add(this._faultModule5);
            this.groupBox19.Controls.Add(this._faultSystemJournal);
            this.groupBox19.Controls.Add(this.label200);
            this.groupBox19.Controls.Add(this.label194);
            this.groupBox19.Controls.Add(this._faultGroupsOfSetpoints);
            this.groupBox19.Controls.Add(this.label201);
            this.groupBox19.Controls.Add(this._faultModule4);
            this.groupBox19.Controls.Add(this._faultLogic);
            this.groupBox19.Controls.Add(this.label79);
            this.groupBox19.Controls.Add(this._faultSetpoints);
            this.groupBox19.Controls.Add(this.label202);
            this.groupBox19.Controls.Add(this.label195);
            this.groupBox19.Controls.Add(this._faultPass);
            this.groupBox19.Controls.Add(this.label203);
            this.groupBox19.Controls.Add(this._faultModule3);
            this.groupBox19.Controls.Add(this.label196);
            this.groupBox19.Controls.Add(this._faultSoftware);
            this.groupBox19.Controls.Add(this.label205);
            this.groupBox19.Controls.Add(this._faultModule2);
            this.groupBox19.Controls.Add(this._faultHardware);
            this.groupBox19.Controls.Add(this.label206);
            this.groupBox19.Controls.Add(this.label197);
            this.groupBox19.Controls.Add(this._faultModule1);
            this.groupBox19.Controls.Add(this.label198);
            this.groupBox19.Location = new System.Drawing.Point(211, 386);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(226, 175);
            this.groupBox19.TabIndex = 46;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "Неисправности";
            // 
            // label344
            // 
            this.label344.AutoSize = true;
            this.label344.Location = new System.Drawing.Point(25, 57);
            this.label344.Name = "label344";
            this.label344.Size = new System.Drawing.Size(76, 13);
            this.label344.TabIndex = 35;
            this.label344.Text = "Выключателя";
            // 
            // _faultSwitchOff
            // 
            this._faultSwitchOff.Location = new System.Drawing.Point(6, 57);
            this._faultSwitchOff.Name = "_faultSwitchOff";
            this._faultSwitchOff.Size = new System.Drawing.Size(13, 13);
            this._faultSwitchOff.State = BEMN.Forms.LedState.Off;
            this._faultSwitchOff.TabIndex = 34;
            // 
            // _faultAlarmJournal
            // 
            this._faultAlarmJournal.Location = new System.Drawing.Point(117, 57);
            this._faultAlarmJournal.Name = "_faultAlarmJournal";
            this._faultAlarmJournal.Size = new System.Drawing.Size(13, 13);
            this._faultAlarmJournal.State = BEMN.Forms.LedState.Off;
            this._faultAlarmJournal.TabIndex = 29;
            // 
            // label192
            // 
            this.label192.AutoSize = true;
            this.label192.Location = new System.Drawing.Point(136, 57);
            this.label192.Name = "label192";
            this.label192.Size = new System.Drawing.Size(25, 13);
            this.label192.TabIndex = 28;
            this.label192.Text = "ЖА";
            // 
            // _faultOsc
            // 
            this._faultOsc.Location = new System.Drawing.Point(117, 19);
            this._faultOsc.Name = "_faultOsc";
            this._faultOsc.Size = new System.Drawing.Size(13, 13);
            this._faultOsc.State = BEMN.Forms.LedState.Off;
            this._faultOsc.TabIndex = 27;
            // 
            // label193
            // 
            this.label193.AutoSize = true;
            this.label193.Location = new System.Drawing.Point(136, 19);
            this.label193.Name = "label193";
            this.label193.Size = new System.Drawing.Size(84, 13);
            this.label193.TabIndex = 26;
            this.label193.Text = "Осциллограмы";
            // 
            // _faultModule5
            // 
            this._faultModule5.Location = new System.Drawing.Point(117, 152);
            this._faultModule5.Name = "_faultModule5";
            this._faultModule5.Size = new System.Drawing.Size(13, 13);
            this._faultModule5.State = BEMN.Forms.LedState.Off;
            this._faultModule5.TabIndex = 25;
            // 
            // _faultSystemJournal
            // 
            this._faultSystemJournal.Location = new System.Drawing.Point(117, 38);
            this._faultSystemJournal.Name = "_faultSystemJournal";
            this._faultSystemJournal.Size = new System.Drawing.Size(13, 13);
            this._faultSystemJournal.State = BEMN.Forms.LedState.Off;
            this._faultSystemJournal.TabIndex = 13;
            // 
            // label200
            // 
            this.label200.AutoSize = true;
            this.label200.Location = new System.Drawing.Point(136, 38);
            this.label200.Name = "label200";
            this.label200.Size = new System.Drawing.Size(25, 13);
            this.label200.TabIndex = 12;
            this.label200.Text = "ЖС";
            // 
            // label194
            // 
            this.label194.AutoSize = true;
            this.label194.Location = new System.Drawing.Point(136, 152);
            this.label194.Name = "label194";
            this.label194.Size = new System.Drawing.Size(54, 13);
            this.label194.TabIndex = 24;
            this.label194.Text = "Модуля 5";
            // 
            // _faultGroupsOfSetpoints
            // 
            this._faultGroupsOfSetpoints.Location = new System.Drawing.Point(6, 114);
            this._faultGroupsOfSetpoints.Name = "_faultGroupsOfSetpoints";
            this._faultGroupsOfSetpoints.Size = new System.Drawing.Size(13, 13);
            this._faultGroupsOfSetpoints.State = BEMN.Forms.LedState.Off;
            this._faultGroupsOfSetpoints.TabIndex = 11;
            // 
            // label201
            // 
            this.label201.AutoSize = true;
            this.label201.Location = new System.Drawing.Point(25, 114);
            this.label201.Name = "label201";
            this.label201.Size = new System.Drawing.Size(79, 13);
            this.label201.TabIndex = 10;
            this.label201.Text = "Групп уставок";
            // 
            // _faultModule4
            // 
            this._faultModule4.Location = new System.Drawing.Point(117, 133);
            this._faultModule4.Name = "_faultModule4";
            this._faultModule4.Size = new System.Drawing.Size(13, 13);
            this._faultModule4.State = BEMN.Forms.LedState.Off;
            this._faultModule4.TabIndex = 23;
            // 
            // _faultLogic
            // 
            this._faultLogic.Location = new System.Drawing.Point(6, 76);
            this._faultLogic.Name = "_faultLogic";
            this._faultLogic.Size = new System.Drawing.Size(13, 13);
            this._faultLogic.State = BEMN.Forms.LedState.Off;
            this._faultLogic.TabIndex = 9;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(25, 76);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(44, 13);
            this.label79.TabIndex = 8;
            this.label79.Text = "Логики";
            // 
            // _faultSetpoints
            // 
            this._faultSetpoints.Location = new System.Drawing.Point(6, 95);
            this._faultSetpoints.Name = "_faultSetpoints";
            this._faultSetpoints.Size = new System.Drawing.Size(13, 13);
            this._faultSetpoints.State = BEMN.Forms.LedState.Off;
            this._faultSetpoints.TabIndex = 9;
            // 
            // label202
            // 
            this.label202.AutoSize = true;
            this.label202.Location = new System.Drawing.Point(25, 95);
            this.label202.Name = "label202";
            this.label202.Size = new System.Drawing.Size(50, 13);
            this.label202.TabIndex = 8;
            this.label202.Text = "Уставок";
            // 
            // label195
            // 
            this.label195.AutoSize = true;
            this.label195.Location = new System.Drawing.Point(136, 133);
            this.label195.Name = "label195";
            this.label195.Size = new System.Drawing.Size(54, 13);
            this.label195.TabIndex = 22;
            this.label195.Text = "Модуля 4";
            // 
            // _faultPass
            // 
            this._faultPass.Location = new System.Drawing.Point(6, 133);
            this._faultPass.Name = "_faultPass";
            this._faultPass.Size = new System.Drawing.Size(13, 13);
            this._faultPass.State = BEMN.Forms.LedState.Off;
            this._faultPass.TabIndex = 7;
            // 
            // label203
            // 
            this.label203.AutoSize = true;
            this.label203.Location = new System.Drawing.Point(25, 133);
            this.label203.Name = "label203";
            this.label203.Size = new System.Drawing.Size(45, 13);
            this.label203.TabIndex = 6;
            this.label203.Text = "Пароля";
            // 
            // _faultModule3
            // 
            this._faultModule3.Location = new System.Drawing.Point(117, 114);
            this._faultModule3.Name = "_faultModule3";
            this._faultModule3.Size = new System.Drawing.Size(13, 13);
            this._faultModule3.State = BEMN.Forms.LedState.Off;
            this._faultModule3.TabIndex = 21;
            // 
            // label196
            // 
            this.label196.AutoSize = true;
            this.label196.Location = new System.Drawing.Point(136, 114);
            this.label196.Name = "label196";
            this.label196.Size = new System.Drawing.Size(54, 13);
            this.label196.TabIndex = 20;
            this.label196.Text = "Модуля 3";
            // 
            // _faultSoftware
            // 
            this._faultSoftware.Location = new System.Drawing.Point(6, 38);
            this._faultSoftware.Name = "_faultSoftware";
            this._faultSoftware.Size = new System.Drawing.Size(13, 13);
            this._faultSoftware.State = BEMN.Forms.LedState.Off;
            this._faultSoftware.TabIndex = 3;
            // 
            // label205
            // 
            this.label205.AutoSize = true;
            this.label205.Location = new System.Drawing.Point(25, 38);
            this.label205.Name = "label205";
            this.label205.Size = new System.Drawing.Size(78, 13);
            this.label205.TabIndex = 2;
            this.label205.Text = "Программная";
            // 
            // _faultModule2
            // 
            this._faultModule2.Location = new System.Drawing.Point(117, 95);
            this._faultModule2.Name = "_faultModule2";
            this._faultModule2.Size = new System.Drawing.Size(13, 13);
            this._faultModule2.State = BEMN.Forms.LedState.Off;
            this._faultModule2.TabIndex = 19;
            // 
            // _faultHardware
            // 
            this._faultHardware.Location = new System.Drawing.Point(6, 19);
            this._faultHardware.Name = "_faultHardware";
            this._faultHardware.Size = new System.Drawing.Size(13, 13);
            this._faultHardware.State = BEMN.Forms.LedState.Off;
            this._faultHardware.TabIndex = 1;
            // 
            // label206
            // 
            this.label206.AutoSize = true;
            this.label206.Location = new System.Drawing.Point(25, 19);
            this.label206.Name = "label206";
            this.label206.Size = new System.Drawing.Size(67, 13);
            this.label206.TabIndex = 0;
            this.label206.Text = "Аппаратная";
            // 
            // label197
            // 
            this.label197.AutoSize = true;
            this.label197.Location = new System.Drawing.Point(136, 95);
            this.label197.Name = "label197";
            this.label197.Size = new System.Drawing.Size(54, 13);
            this.label197.TabIndex = 18;
            this.label197.Text = "Модуля 2";
            // 
            // _faultModule1
            // 
            this._faultModule1.Location = new System.Drawing.Point(117, 76);
            this._faultModule1.Name = "_faultModule1";
            this._faultModule1.Size = new System.Drawing.Size(13, 13);
            this._faultModule1.State = BEMN.Forms.LedState.Off;
            this._faultModule1.TabIndex = 17;
            // 
            // label198
            // 
            this.label198.AutoSize = true;
            this.label198.Location = new System.Drawing.Point(136, 76);
            this.label198.Name = "label198";
            this.label198.Size = new System.Drawing.Size(54, 13);
            this.label198.TabIndex = 16;
            this.label198.Text = "Модуля 1";
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this._vz16);
            this.groupBox12.Controls.Add(this.label111);
            this.groupBox12.Controls.Add(this._vz15);
            this.groupBox12.Controls.Add(this.label112);
            this.groupBox12.Controls.Add(this._vz14);
            this.groupBox12.Controls.Add(this.label113);
            this.groupBox12.Controls.Add(this._vz13);
            this.groupBox12.Controls.Add(this.label114);
            this.groupBox12.Controls.Add(this._vz12);
            this.groupBox12.Controls.Add(this.label115);
            this.groupBox12.Controls.Add(this._vz11);
            this.groupBox12.Controls.Add(this.label116);
            this.groupBox12.Controls.Add(this._vz10);
            this.groupBox12.Controls.Add(this.label117);
            this.groupBox12.Controls.Add(this._vz9);
            this.groupBox12.Controls.Add(this.label118);
            this.groupBox12.Controls.Add(this._vz8);
            this.groupBox12.Controls.Add(this.label119);
            this.groupBox12.Controls.Add(this._vz7);
            this.groupBox12.Controls.Add(this.label120);
            this.groupBox12.Controls.Add(this._vz6);
            this.groupBox12.Controls.Add(this.label121);
            this.groupBox12.Controls.Add(this._vz5);
            this.groupBox12.Controls.Add(this.label122);
            this.groupBox12.Controls.Add(this._vz4);
            this.groupBox12.Controls.Add(this.label123);
            this.groupBox12.Controls.Add(this._vz3);
            this.groupBox12.Controls.Add(this.label124);
            this.groupBox12.Controls.Add(this._vz2);
            this.groupBox12.Controls.Add(this.label125);
            this.groupBox12.Controls.Add(this._vz1);
            this.groupBox12.Controls.Add(this.label126);
            this.groupBox12.Location = new System.Drawing.Point(590, 182);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(172, 175);
            this.groupBox12.TabIndex = 45;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Внешние защиты";
            // 
            // _vz16
            // 
            this._vz16.Location = new System.Drawing.Point(84, 152);
            this._vz16.Name = "_vz16";
            this._vz16.Size = new System.Drawing.Size(13, 13);
            this._vz16.State = BEMN.Forms.LedState.Off;
            this._vz16.TabIndex = 31;
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Location = new System.Drawing.Point(103, 152);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(56, 13);
            this.label111.TabIndex = 30;
            this.label111.Text = "ВНЕШ. 16";
            // 
            // _vz15
            // 
            this._vz15.Location = new System.Drawing.Point(84, 133);
            this._vz15.Name = "_vz15";
            this._vz15.Size = new System.Drawing.Size(13, 13);
            this._vz15.State = BEMN.Forms.LedState.Off;
            this._vz15.TabIndex = 29;
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Location = new System.Drawing.Point(103, 133);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(56, 13);
            this.label112.TabIndex = 28;
            this.label112.Text = "ВНЕШ. 15";
            // 
            // _vz14
            // 
            this._vz14.Location = new System.Drawing.Point(84, 114);
            this._vz14.Name = "_vz14";
            this._vz14.Size = new System.Drawing.Size(13, 13);
            this._vz14.State = BEMN.Forms.LedState.Off;
            this._vz14.TabIndex = 27;
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Location = new System.Drawing.Point(103, 114);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(56, 13);
            this.label113.TabIndex = 26;
            this.label113.Text = "ВНЕШ. 14";
            // 
            // _vz13
            // 
            this._vz13.Location = new System.Drawing.Point(84, 95);
            this._vz13.Name = "_vz13";
            this._vz13.Size = new System.Drawing.Size(13, 13);
            this._vz13.State = BEMN.Forms.LedState.Off;
            this._vz13.TabIndex = 25;
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Location = new System.Drawing.Point(103, 95);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(56, 13);
            this.label114.TabIndex = 24;
            this.label114.Text = "ВНЕШ. 13";
            // 
            // _vz12
            // 
            this._vz12.Location = new System.Drawing.Point(84, 76);
            this._vz12.Name = "_vz12";
            this._vz12.Size = new System.Drawing.Size(13, 13);
            this._vz12.State = BEMN.Forms.LedState.Off;
            this._vz12.TabIndex = 23;
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Location = new System.Drawing.Point(103, 76);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(56, 13);
            this.label115.TabIndex = 22;
            this.label115.Text = "ВНЕШ. 12";
            // 
            // _vz11
            // 
            this._vz11.Location = new System.Drawing.Point(84, 57);
            this._vz11.Name = "_vz11";
            this._vz11.Size = new System.Drawing.Size(13, 13);
            this._vz11.State = BEMN.Forms.LedState.Off;
            this._vz11.TabIndex = 21;
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Location = new System.Drawing.Point(103, 57);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(56, 13);
            this.label116.TabIndex = 20;
            this.label116.Text = "ВНЕШ. 11";
            // 
            // _vz10
            // 
            this._vz10.Location = new System.Drawing.Point(84, 38);
            this._vz10.Name = "_vz10";
            this._vz10.Size = new System.Drawing.Size(13, 13);
            this._vz10.State = BEMN.Forms.LedState.Off;
            this._vz10.TabIndex = 19;
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Location = new System.Drawing.Point(103, 38);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(56, 13);
            this.label117.TabIndex = 18;
            this.label117.Text = "ВНЕШ. 10";
            // 
            // _vz9
            // 
            this._vz9.Location = new System.Drawing.Point(84, 19);
            this._vz9.Name = "_vz9";
            this._vz9.Size = new System.Drawing.Size(13, 13);
            this._vz9.State = BEMN.Forms.LedState.Off;
            this._vz9.TabIndex = 17;
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Location = new System.Drawing.Point(103, 19);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(50, 13);
            this.label118.TabIndex = 16;
            this.label118.Text = "ВНЕШ. 9";
            // 
            // _vz8
            // 
            this._vz8.Location = new System.Drawing.Point(6, 152);
            this._vz8.Name = "_vz8";
            this._vz8.Size = new System.Drawing.Size(13, 13);
            this._vz8.State = BEMN.Forms.LedState.Off;
            this._vz8.TabIndex = 15;
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Location = new System.Drawing.Point(25, 152);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(50, 13);
            this.label119.TabIndex = 14;
            this.label119.Text = "ВНЕШ. 8";
            // 
            // _vz7
            // 
            this._vz7.Location = new System.Drawing.Point(6, 133);
            this._vz7.Name = "_vz7";
            this._vz7.Size = new System.Drawing.Size(13, 13);
            this._vz7.State = BEMN.Forms.LedState.Off;
            this._vz7.TabIndex = 13;
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Location = new System.Drawing.Point(25, 133);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(50, 13);
            this.label120.TabIndex = 12;
            this.label120.Text = "ВНЕШ. 7";
            // 
            // _vz6
            // 
            this._vz6.Location = new System.Drawing.Point(6, 114);
            this._vz6.Name = "_vz6";
            this._vz6.Size = new System.Drawing.Size(13, 13);
            this._vz6.State = BEMN.Forms.LedState.Off;
            this._vz6.TabIndex = 11;
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Location = new System.Drawing.Point(25, 114);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(50, 13);
            this.label121.TabIndex = 10;
            this.label121.Text = "ВНЕШ. 6";
            // 
            // _vz5
            // 
            this._vz5.Location = new System.Drawing.Point(6, 95);
            this._vz5.Name = "_vz5";
            this._vz5.Size = new System.Drawing.Size(13, 13);
            this._vz5.State = BEMN.Forms.LedState.Off;
            this._vz5.TabIndex = 9;
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Location = new System.Drawing.Point(25, 95);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(50, 13);
            this.label122.TabIndex = 8;
            this.label122.Text = "ВНЕШ. 5";
            // 
            // _vz4
            // 
            this._vz4.Location = new System.Drawing.Point(6, 76);
            this._vz4.Name = "_vz4";
            this._vz4.Size = new System.Drawing.Size(13, 13);
            this._vz4.State = BEMN.Forms.LedState.Off;
            this._vz4.TabIndex = 7;
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Location = new System.Drawing.Point(25, 76);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(50, 13);
            this.label123.TabIndex = 6;
            this.label123.Text = "ВНЕШ. 4";
            // 
            // _vz3
            // 
            this._vz3.Location = new System.Drawing.Point(6, 57);
            this._vz3.Name = "_vz3";
            this._vz3.Size = new System.Drawing.Size(13, 13);
            this._vz3.State = BEMN.Forms.LedState.Off;
            this._vz3.TabIndex = 5;
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Location = new System.Drawing.Point(25, 57);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(50, 13);
            this.label124.TabIndex = 4;
            this.label124.Text = "ВНЕШ. 3";
            // 
            // _vz2
            // 
            this._vz2.Location = new System.Drawing.Point(6, 38);
            this._vz2.Name = "_vz2";
            this._vz2.Size = new System.Drawing.Size(13, 13);
            this._vz2.State = BEMN.Forms.LedState.Off;
            this._vz2.TabIndex = 3;
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Location = new System.Drawing.Point(25, 38);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(50, 13);
            this.label125.TabIndex = 2;
            this.label125.Text = "ВНЕШ. 2";
            // 
            // _vz1
            // 
            this._vz1.Location = new System.Drawing.Point(6, 19);
            this._vz1.Name = "_vz1";
            this._vz1.Size = new System.Drawing.Size(13, 13);
            this._vz1.State = BEMN.Forms.LedState.Off;
            this._vz1.TabIndex = 1;
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Location = new System.Drawing.Point(25, 19);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(50, 13);
            this.label126.TabIndex = 0;
            this.label126.Text = "ВНЕШ. 1";
            // 
            // _dateTimeControl
            // 
            this._dateTimeControl.DateTime = dateTimeStruct1;
            this._dateTimeControl.Location = new System.Drawing.Point(5, 385);
            this._dateTimeControl.Name = "_dateTimeControl";
            this._dateTimeControl.Size = new System.Drawing.Size(201, 166);
            this._dateTimeControl.TabIndex = 44;
            this._dateTimeControl.TimeChanged += new System.Action(this.dateTimeControl_TimeChanged);
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this._fault);
            this.groupBox14.Controls.Add(this.label210);
            this.groupBox14.Controls.Add(this._faultOff);
            this.groupBox14.Controls.Add(this.label21);
            this.groupBox14.Controls.Add(this._acceleration);
            this.groupBox14.Controls.Add(this._auto4Led);
            this.groupBox14.Controls.Add(this._signaling);
            this.groupBox14.Controls.Add(this._auto2Led);
            this.groupBox14.Location = new System.Drawing.Point(768, 182);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(135, 103);
            this.groupBox14.TabIndex = 24;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Общие сигналы";
            // 
            // _fault
            // 
            this._fault.Location = new System.Drawing.Point(6, 19);
            this._fault.Name = "_fault";
            this._fault.Size = new System.Drawing.Size(13, 13);
            this._fault.State = BEMN.Forms.LedState.Off;
            this._fault.TabIndex = 37;
            // 
            // label210
            // 
            this.label210.AutoSize = true;
            this.label210.Location = new System.Drawing.Point(25, 19);
            this.label210.Name = "label210";
            this.label210.Size = new System.Drawing.Size(86, 13);
            this.label210.TabIndex = 36;
            this.label210.Text = "Неисправность";
            // 
            // _faultOff
            // 
            this._faultOff.Location = new System.Drawing.Point(6, 76);
            this._faultOff.Name = "_faultOff";
            this._faultOff.Size = new System.Drawing.Size(13, 13);
            this._faultOff.State = BEMN.Forms.LedState.Off;
            this._faultOff.TabIndex = 35;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(25, 76);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(98, 13);
            this.label21.TabIndex = 34;
            this.label21.Text = "Авар. отключение";
            // 
            // _acceleration
            // 
            this._acceleration.Location = new System.Drawing.Point(6, 57);
            this._acceleration.Name = "_acceleration";
            this._acceleration.Size = new System.Drawing.Size(13, 13);
            this._acceleration.State = BEMN.Forms.LedState.Off;
            this._acceleration.TabIndex = 31;
            // 
            // _auto4Led
            // 
            this._auto4Led.AutoSize = true;
            this._auto4Led.Location = new System.Drawing.Point(25, 57);
            this._auto4Led.Name = "_auto4Led";
            this._auto4Led.Size = new System.Drawing.Size(81, 13);
            this._auto4Led.TabIndex = 30;
            this._auto4Led.Text = "Ускор. по вкл.";
            // 
            // _signaling
            // 
            this._signaling.Location = new System.Drawing.Point(6, 38);
            this._signaling.Name = "_signaling";
            this._signaling.Size = new System.Drawing.Size(13, 13);
            this._signaling.State = BEMN.Forms.LedState.Off;
            this._signaling.TabIndex = 27;
            // 
            // _auto2Led
            // 
            this._auto2Led.AutoSize = true;
            this._auto2Led.Location = new System.Drawing.Point(25, 38);
            this._auto2Led.Name = "_auto2Led";
            this._auto2Led.Size = new System.Drawing.Size(79, 13);
            this._auto2Led.TabIndex = 26;
            this._auto2Led.Text = "Сигнализация";
            // 
            // groupBox21
            // 
            this.groupBox21.Controls.Add(this._ssl32);
            this.groupBox21.Controls.Add(this.label237);
            this.groupBox21.Controls.Add(this._ssl31);
            this.groupBox21.Controls.Add(this.label238);
            this.groupBox21.Controls.Add(this._ssl30);
            this.groupBox21.Controls.Add(this.label239);
            this.groupBox21.Controls.Add(this._ssl29);
            this.groupBox21.Controls.Add(this.label240);
            this.groupBox21.Controls.Add(this._ssl28);
            this.groupBox21.Controls.Add(this.label241);
            this.groupBox21.Controls.Add(this._ssl27);
            this.groupBox21.Controls.Add(this.label242);
            this.groupBox21.Controls.Add(this._ssl26);
            this.groupBox21.Controls.Add(this.label243);
            this.groupBox21.Controls.Add(this._ssl25);
            this.groupBox21.Controls.Add(this.label244);
            this.groupBox21.Controls.Add(this._ssl24);
            this.groupBox21.Controls.Add(this.label245);
            this.groupBox21.Controls.Add(this._ssl23);
            this.groupBox21.Controls.Add(this.label246);
            this.groupBox21.Controls.Add(this._ssl22);
            this.groupBox21.Controls.Add(this.label247);
            this.groupBox21.Controls.Add(this._ssl21);
            this.groupBox21.Controls.Add(this.label248);
            this.groupBox21.Controls.Add(this._ssl20);
            this.groupBox21.Controls.Add(this.label249);
            this.groupBox21.Controls.Add(this._ssl19);
            this.groupBox21.Controls.Add(this.label250);
            this.groupBox21.Controls.Add(this._ssl18);
            this.groupBox21.Controls.Add(this.label251);
            this.groupBox21.Controls.Add(this._ssl17);
            this.groupBox21.Controls.Add(this.label252);
            this.groupBox21.Controls.Add(this._ssl16);
            this.groupBox21.Controls.Add(this.label253);
            this.groupBox21.Controls.Add(this._ssl15);
            this.groupBox21.Controls.Add(this.label254);
            this.groupBox21.Controls.Add(this._ssl14);
            this.groupBox21.Controls.Add(this.label255);
            this.groupBox21.Controls.Add(this._ssl13);
            this.groupBox21.Controls.Add(this.label256);
            this.groupBox21.Controls.Add(this._ssl12);
            this.groupBox21.Controls.Add(this.label257);
            this.groupBox21.Controls.Add(this._ssl11);
            this.groupBox21.Controls.Add(this.label258);
            this.groupBox21.Controls.Add(this._ssl10);
            this.groupBox21.Controls.Add(this.label259);
            this.groupBox21.Controls.Add(this._ssl9);
            this.groupBox21.Controls.Add(this.label260);
            this.groupBox21.Controls.Add(this._ssl8);
            this.groupBox21.Controls.Add(this.label261);
            this.groupBox21.Controls.Add(this._ssl7);
            this.groupBox21.Controls.Add(this.label262);
            this.groupBox21.Controls.Add(this._ssl6);
            this.groupBox21.Controls.Add(this.label263);
            this.groupBox21.Controls.Add(this._ssl5);
            this.groupBox21.Controls.Add(this.label264);
            this.groupBox21.Controls.Add(this._ssl4);
            this.groupBox21.Controls.Add(this.label265);
            this.groupBox21.Controls.Add(this._ssl3);
            this.groupBox21.Controls.Add(this.label266);
            this.groupBox21.Controls.Add(this._ssl2);
            this.groupBox21.Controls.Add(this.label267);
            this.groupBox21.Controls.Add(this._ssl1);
            this.groupBox21.Controls.Add(this.label268);
            this.groupBox21.Location = new System.Drawing.Point(6, 184);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(286, 175);
            this.groupBox21.TabIndex = 14;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "Сигналы СП-логики";
            // 
            // _ssl32
            // 
            this._ssl32.Location = new System.Drawing.Point(219, 152);
            this._ssl32.Name = "_ssl32";
            this._ssl32.Size = new System.Drawing.Size(13, 13);
            this._ssl32.State = BEMN.Forms.LedState.Off;
            this._ssl32.TabIndex = 63;
            // 
            // label237
            // 
            this.label237.AutoSize = true;
            this.label237.Location = new System.Drawing.Point(238, 152);
            this.label237.Name = "label237";
            this.label237.Size = new System.Drawing.Size(41, 13);
            this.label237.TabIndex = 62;
            this.label237.Text = "ССЛ32";
            // 
            // _ssl31
            // 
            this._ssl31.Location = new System.Drawing.Point(219, 133);
            this._ssl31.Name = "_ssl31";
            this._ssl31.Size = new System.Drawing.Size(13, 13);
            this._ssl31.State = BEMN.Forms.LedState.Off;
            this._ssl31.TabIndex = 61;
            // 
            // label238
            // 
            this.label238.AutoSize = true;
            this.label238.Location = new System.Drawing.Point(238, 133);
            this.label238.Name = "label238";
            this.label238.Size = new System.Drawing.Size(41, 13);
            this.label238.TabIndex = 60;
            this.label238.Text = "ССЛ31";
            // 
            // _ssl30
            // 
            this._ssl30.Location = new System.Drawing.Point(219, 114);
            this._ssl30.Name = "_ssl30";
            this._ssl30.Size = new System.Drawing.Size(13, 13);
            this._ssl30.State = BEMN.Forms.LedState.Off;
            this._ssl30.TabIndex = 59;
            // 
            // label239
            // 
            this.label239.AutoSize = true;
            this.label239.Location = new System.Drawing.Point(238, 114);
            this.label239.Name = "label239";
            this.label239.Size = new System.Drawing.Size(41, 13);
            this.label239.TabIndex = 58;
            this.label239.Text = "ССЛ30";
            // 
            // _ssl29
            // 
            this._ssl29.Location = new System.Drawing.Point(219, 95);
            this._ssl29.Name = "_ssl29";
            this._ssl29.Size = new System.Drawing.Size(13, 13);
            this._ssl29.State = BEMN.Forms.LedState.Off;
            this._ssl29.TabIndex = 57;
            // 
            // label240
            // 
            this.label240.AutoSize = true;
            this.label240.Location = new System.Drawing.Point(238, 95);
            this.label240.Name = "label240";
            this.label240.Size = new System.Drawing.Size(41, 13);
            this.label240.TabIndex = 56;
            this.label240.Text = "ССЛ29";
            // 
            // _ssl28
            // 
            this._ssl28.Location = new System.Drawing.Point(219, 76);
            this._ssl28.Name = "_ssl28";
            this._ssl28.Size = new System.Drawing.Size(13, 13);
            this._ssl28.State = BEMN.Forms.LedState.Off;
            this._ssl28.TabIndex = 55;
            // 
            // label241
            // 
            this.label241.AutoSize = true;
            this.label241.Location = new System.Drawing.Point(238, 76);
            this.label241.Name = "label241";
            this.label241.Size = new System.Drawing.Size(41, 13);
            this.label241.TabIndex = 54;
            this.label241.Text = "ССЛ28";
            // 
            // _ssl27
            // 
            this._ssl27.Location = new System.Drawing.Point(219, 57);
            this._ssl27.Name = "_ssl27";
            this._ssl27.Size = new System.Drawing.Size(13, 13);
            this._ssl27.State = BEMN.Forms.LedState.Off;
            this._ssl27.TabIndex = 53;
            // 
            // label242
            // 
            this.label242.AutoSize = true;
            this.label242.Location = new System.Drawing.Point(238, 57);
            this.label242.Name = "label242";
            this.label242.Size = new System.Drawing.Size(41, 13);
            this.label242.TabIndex = 52;
            this.label242.Text = "ССЛ27";
            // 
            // _ssl26
            // 
            this._ssl26.Location = new System.Drawing.Point(219, 38);
            this._ssl26.Name = "_ssl26";
            this._ssl26.Size = new System.Drawing.Size(13, 13);
            this._ssl26.State = BEMN.Forms.LedState.Off;
            this._ssl26.TabIndex = 51;
            // 
            // label243
            // 
            this.label243.AutoSize = true;
            this.label243.Location = new System.Drawing.Point(238, 38);
            this.label243.Name = "label243";
            this.label243.Size = new System.Drawing.Size(41, 13);
            this.label243.TabIndex = 50;
            this.label243.Text = "ССЛ26";
            // 
            // _ssl25
            // 
            this._ssl25.Location = new System.Drawing.Point(219, 19);
            this._ssl25.Name = "_ssl25";
            this._ssl25.Size = new System.Drawing.Size(13, 13);
            this._ssl25.State = BEMN.Forms.LedState.Off;
            this._ssl25.TabIndex = 49;
            // 
            // label244
            // 
            this.label244.AutoSize = true;
            this.label244.Location = new System.Drawing.Point(238, 19);
            this.label244.Name = "label244";
            this.label244.Size = new System.Drawing.Size(41, 13);
            this.label244.TabIndex = 48;
            this.label244.Text = "ССЛ25";
            // 
            // _ssl24
            // 
            this._ssl24.Location = new System.Drawing.Point(142, 152);
            this._ssl24.Name = "_ssl24";
            this._ssl24.Size = new System.Drawing.Size(13, 13);
            this._ssl24.State = BEMN.Forms.LedState.Off;
            this._ssl24.TabIndex = 47;
            // 
            // label245
            // 
            this.label245.AutoSize = true;
            this.label245.Location = new System.Drawing.Point(161, 152);
            this.label245.Name = "label245";
            this.label245.Size = new System.Drawing.Size(41, 13);
            this.label245.TabIndex = 46;
            this.label245.Text = "ССЛ24";
            // 
            // _ssl23
            // 
            this._ssl23.Location = new System.Drawing.Point(142, 133);
            this._ssl23.Name = "_ssl23";
            this._ssl23.Size = new System.Drawing.Size(13, 13);
            this._ssl23.State = BEMN.Forms.LedState.Off;
            this._ssl23.TabIndex = 45;
            // 
            // label246
            // 
            this.label246.AutoSize = true;
            this.label246.Location = new System.Drawing.Point(161, 133);
            this.label246.Name = "label246";
            this.label246.Size = new System.Drawing.Size(41, 13);
            this.label246.TabIndex = 44;
            this.label246.Text = "ССЛ23";
            // 
            // _ssl22
            // 
            this._ssl22.Location = new System.Drawing.Point(142, 114);
            this._ssl22.Name = "_ssl22";
            this._ssl22.Size = new System.Drawing.Size(13, 13);
            this._ssl22.State = BEMN.Forms.LedState.Off;
            this._ssl22.TabIndex = 43;
            // 
            // label247
            // 
            this.label247.AutoSize = true;
            this.label247.Location = new System.Drawing.Point(161, 114);
            this.label247.Name = "label247";
            this.label247.Size = new System.Drawing.Size(41, 13);
            this.label247.TabIndex = 42;
            this.label247.Text = "ССЛ22";
            // 
            // _ssl21
            // 
            this._ssl21.Location = new System.Drawing.Point(142, 95);
            this._ssl21.Name = "_ssl21";
            this._ssl21.Size = new System.Drawing.Size(13, 13);
            this._ssl21.State = BEMN.Forms.LedState.Off;
            this._ssl21.TabIndex = 41;
            // 
            // label248
            // 
            this.label248.AutoSize = true;
            this.label248.Location = new System.Drawing.Point(161, 95);
            this.label248.Name = "label248";
            this.label248.Size = new System.Drawing.Size(41, 13);
            this.label248.TabIndex = 40;
            this.label248.Text = "ССЛ21";
            // 
            // _ssl20
            // 
            this._ssl20.Location = new System.Drawing.Point(142, 76);
            this._ssl20.Name = "_ssl20";
            this._ssl20.Size = new System.Drawing.Size(13, 13);
            this._ssl20.State = BEMN.Forms.LedState.Off;
            this._ssl20.TabIndex = 39;
            // 
            // label249
            // 
            this.label249.AutoSize = true;
            this.label249.Location = new System.Drawing.Point(162, 76);
            this.label249.Name = "label249";
            this.label249.Size = new System.Drawing.Size(41, 13);
            this.label249.TabIndex = 38;
            this.label249.Text = "ССЛ20";
            // 
            // _ssl19
            // 
            this._ssl19.Location = new System.Drawing.Point(142, 57);
            this._ssl19.Name = "_ssl19";
            this._ssl19.Size = new System.Drawing.Size(13, 13);
            this._ssl19.State = BEMN.Forms.LedState.Off;
            this._ssl19.TabIndex = 37;
            // 
            // label250
            // 
            this.label250.AutoSize = true;
            this.label250.Location = new System.Drawing.Point(161, 57);
            this.label250.Name = "label250";
            this.label250.Size = new System.Drawing.Size(41, 13);
            this.label250.TabIndex = 36;
            this.label250.Text = "ССЛ19";
            // 
            // _ssl18
            // 
            this._ssl18.Location = new System.Drawing.Point(142, 38);
            this._ssl18.Name = "_ssl18";
            this._ssl18.Size = new System.Drawing.Size(13, 13);
            this._ssl18.State = BEMN.Forms.LedState.Off;
            this._ssl18.TabIndex = 35;
            // 
            // label251
            // 
            this.label251.AutoSize = true;
            this.label251.Location = new System.Drawing.Point(161, 38);
            this.label251.Name = "label251";
            this.label251.Size = new System.Drawing.Size(41, 13);
            this.label251.TabIndex = 34;
            this.label251.Text = "ССЛ18";
            // 
            // _ssl17
            // 
            this._ssl17.Location = new System.Drawing.Point(142, 19);
            this._ssl17.Name = "_ssl17";
            this._ssl17.Size = new System.Drawing.Size(13, 13);
            this._ssl17.State = BEMN.Forms.LedState.Off;
            this._ssl17.TabIndex = 33;
            // 
            // label252
            // 
            this.label252.AutoSize = true;
            this.label252.Location = new System.Drawing.Point(161, 19);
            this.label252.Name = "label252";
            this.label252.Size = new System.Drawing.Size(41, 13);
            this.label252.TabIndex = 32;
            this.label252.Text = "ССЛ17";
            // 
            // _ssl16
            // 
            this._ssl16.Location = new System.Drawing.Point(71, 152);
            this._ssl16.Name = "_ssl16";
            this._ssl16.Size = new System.Drawing.Size(13, 13);
            this._ssl16.State = BEMN.Forms.LedState.Off;
            this._ssl16.TabIndex = 31;
            // 
            // label253
            // 
            this.label253.AutoSize = true;
            this.label253.Location = new System.Drawing.Point(90, 152);
            this.label253.Name = "label253";
            this.label253.Size = new System.Drawing.Size(41, 13);
            this.label253.TabIndex = 30;
            this.label253.Text = "ССЛ16";
            // 
            // _ssl15
            // 
            this._ssl15.Location = new System.Drawing.Point(71, 133);
            this._ssl15.Name = "_ssl15";
            this._ssl15.Size = new System.Drawing.Size(13, 13);
            this._ssl15.State = BEMN.Forms.LedState.Off;
            this._ssl15.TabIndex = 29;
            // 
            // label254
            // 
            this.label254.AutoSize = true;
            this.label254.Location = new System.Drawing.Point(90, 133);
            this.label254.Name = "label254";
            this.label254.Size = new System.Drawing.Size(41, 13);
            this.label254.TabIndex = 28;
            this.label254.Text = "ССЛ15";
            // 
            // _ssl14
            // 
            this._ssl14.Location = new System.Drawing.Point(71, 114);
            this._ssl14.Name = "_ssl14";
            this._ssl14.Size = new System.Drawing.Size(13, 13);
            this._ssl14.State = BEMN.Forms.LedState.Off;
            this._ssl14.TabIndex = 27;
            // 
            // label255
            // 
            this.label255.AutoSize = true;
            this.label255.Location = new System.Drawing.Point(90, 114);
            this.label255.Name = "label255";
            this.label255.Size = new System.Drawing.Size(41, 13);
            this.label255.TabIndex = 26;
            this.label255.Text = "ССЛ14";
            // 
            // _ssl13
            // 
            this._ssl13.Location = new System.Drawing.Point(71, 95);
            this._ssl13.Name = "_ssl13";
            this._ssl13.Size = new System.Drawing.Size(13, 13);
            this._ssl13.State = BEMN.Forms.LedState.Off;
            this._ssl13.TabIndex = 25;
            // 
            // label256
            // 
            this.label256.AutoSize = true;
            this.label256.Location = new System.Drawing.Point(90, 95);
            this.label256.Name = "label256";
            this.label256.Size = new System.Drawing.Size(41, 13);
            this.label256.TabIndex = 24;
            this.label256.Text = "ССЛ13";
            // 
            // _ssl12
            // 
            this._ssl12.Location = new System.Drawing.Point(71, 76);
            this._ssl12.Name = "_ssl12";
            this._ssl12.Size = new System.Drawing.Size(13, 13);
            this._ssl12.State = BEMN.Forms.LedState.Off;
            this._ssl12.TabIndex = 23;
            // 
            // label257
            // 
            this.label257.AutoSize = true;
            this.label257.Location = new System.Drawing.Point(90, 76);
            this.label257.Name = "label257";
            this.label257.Size = new System.Drawing.Size(41, 13);
            this.label257.TabIndex = 22;
            this.label257.Text = "ССЛ12";
            // 
            // _ssl11
            // 
            this._ssl11.Location = new System.Drawing.Point(71, 57);
            this._ssl11.Name = "_ssl11";
            this._ssl11.Size = new System.Drawing.Size(13, 13);
            this._ssl11.State = BEMN.Forms.LedState.Off;
            this._ssl11.TabIndex = 21;
            // 
            // label258
            // 
            this.label258.AutoSize = true;
            this.label258.Location = new System.Drawing.Point(90, 57);
            this.label258.Name = "label258";
            this.label258.Size = new System.Drawing.Size(41, 13);
            this.label258.TabIndex = 20;
            this.label258.Text = "ССЛ11";
            // 
            // _ssl10
            // 
            this._ssl10.Location = new System.Drawing.Point(71, 38);
            this._ssl10.Name = "_ssl10";
            this._ssl10.Size = new System.Drawing.Size(13, 13);
            this._ssl10.State = BEMN.Forms.LedState.Off;
            this._ssl10.TabIndex = 19;
            // 
            // label259
            // 
            this.label259.AutoSize = true;
            this.label259.Location = new System.Drawing.Point(90, 38);
            this.label259.Name = "label259";
            this.label259.Size = new System.Drawing.Size(41, 13);
            this.label259.TabIndex = 18;
            this.label259.Text = "ССЛ10";
            // 
            // _ssl9
            // 
            this._ssl9.Location = new System.Drawing.Point(71, 19);
            this._ssl9.Name = "_ssl9";
            this._ssl9.Size = new System.Drawing.Size(13, 13);
            this._ssl9.State = BEMN.Forms.LedState.Off;
            this._ssl9.TabIndex = 17;
            // 
            // label260
            // 
            this.label260.AutoSize = true;
            this.label260.Location = new System.Drawing.Point(90, 19);
            this.label260.Name = "label260";
            this.label260.Size = new System.Drawing.Size(35, 13);
            this.label260.TabIndex = 16;
            this.label260.Text = "ССЛ9";
            // 
            // _ssl8
            // 
            this._ssl8.Location = new System.Drawing.Point(6, 152);
            this._ssl8.Name = "_ssl8";
            this._ssl8.Size = new System.Drawing.Size(13, 13);
            this._ssl8.State = BEMN.Forms.LedState.Off;
            this._ssl8.TabIndex = 15;
            // 
            // label261
            // 
            this.label261.AutoSize = true;
            this.label261.Location = new System.Drawing.Point(25, 152);
            this.label261.Name = "label261";
            this.label261.Size = new System.Drawing.Size(35, 13);
            this.label261.TabIndex = 14;
            this.label261.Text = "ССЛ8";
            // 
            // _ssl7
            // 
            this._ssl7.Location = new System.Drawing.Point(6, 133);
            this._ssl7.Name = "_ssl7";
            this._ssl7.Size = new System.Drawing.Size(13, 13);
            this._ssl7.State = BEMN.Forms.LedState.Off;
            this._ssl7.TabIndex = 13;
            // 
            // label262
            // 
            this.label262.AutoSize = true;
            this.label262.Location = new System.Drawing.Point(25, 133);
            this.label262.Name = "label262";
            this.label262.Size = new System.Drawing.Size(35, 13);
            this.label262.TabIndex = 12;
            this.label262.Text = "ССЛ7";
            // 
            // _ssl6
            // 
            this._ssl6.Location = new System.Drawing.Point(6, 114);
            this._ssl6.Name = "_ssl6";
            this._ssl6.Size = new System.Drawing.Size(13, 13);
            this._ssl6.State = BEMN.Forms.LedState.Off;
            this._ssl6.TabIndex = 11;
            // 
            // label263
            // 
            this.label263.AutoSize = true;
            this.label263.Location = new System.Drawing.Point(25, 114);
            this.label263.Name = "label263";
            this.label263.Size = new System.Drawing.Size(35, 13);
            this.label263.TabIndex = 10;
            this.label263.Text = "ССЛ6";
            // 
            // _ssl5
            // 
            this._ssl5.Location = new System.Drawing.Point(6, 95);
            this._ssl5.Name = "_ssl5";
            this._ssl5.Size = new System.Drawing.Size(13, 13);
            this._ssl5.State = BEMN.Forms.LedState.Off;
            this._ssl5.TabIndex = 9;
            // 
            // label264
            // 
            this.label264.AutoSize = true;
            this.label264.Location = new System.Drawing.Point(25, 95);
            this.label264.Name = "label264";
            this.label264.Size = new System.Drawing.Size(35, 13);
            this.label264.TabIndex = 8;
            this.label264.Text = "ССЛ5";
            // 
            // _ssl4
            // 
            this._ssl4.Location = new System.Drawing.Point(6, 76);
            this._ssl4.Name = "_ssl4";
            this._ssl4.Size = new System.Drawing.Size(13, 13);
            this._ssl4.State = BEMN.Forms.LedState.Off;
            this._ssl4.TabIndex = 7;
            // 
            // label265
            // 
            this.label265.AutoSize = true;
            this.label265.Location = new System.Drawing.Point(26, 76);
            this.label265.Name = "label265";
            this.label265.Size = new System.Drawing.Size(35, 13);
            this.label265.TabIndex = 6;
            this.label265.Text = "ССЛ4";
            // 
            // _ssl3
            // 
            this._ssl3.Location = new System.Drawing.Point(6, 57);
            this._ssl3.Name = "_ssl3";
            this._ssl3.Size = new System.Drawing.Size(13, 13);
            this._ssl3.State = BEMN.Forms.LedState.Off;
            this._ssl3.TabIndex = 5;
            // 
            // label266
            // 
            this.label266.AutoSize = true;
            this.label266.Location = new System.Drawing.Point(25, 57);
            this.label266.Name = "label266";
            this.label266.Size = new System.Drawing.Size(35, 13);
            this.label266.TabIndex = 4;
            this.label266.Text = "ССЛ3";
            // 
            // _ssl2
            // 
            this._ssl2.Location = new System.Drawing.Point(6, 38);
            this._ssl2.Name = "_ssl2";
            this._ssl2.Size = new System.Drawing.Size(13, 13);
            this._ssl2.State = BEMN.Forms.LedState.Off;
            this._ssl2.TabIndex = 3;
            // 
            // label267
            // 
            this.label267.AutoSize = true;
            this.label267.Location = new System.Drawing.Point(25, 38);
            this.label267.Name = "label267";
            this.label267.Size = new System.Drawing.Size(35, 13);
            this.label267.TabIndex = 2;
            this.label267.Text = "ССЛ2";
            // 
            // _ssl1
            // 
            this._ssl1.Location = new System.Drawing.Point(6, 19);
            this._ssl1.Name = "_ssl1";
            this._ssl1.Size = new System.Drawing.Size(13, 13);
            this._ssl1.State = BEMN.Forms.LedState.Off;
            this._ssl1.TabIndex = 1;
            // 
            // label268
            // 
            this.label268.AutoSize = true;
            this.label268.Location = new System.Drawing.Point(25, 19);
            this.label268.Name = "label268";
            this.label268.Size = new System.Drawing.Size(35, 13);
            this.label268.TabIndex = 0;
            this.label268.Text = "ССЛ1";
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this.diod12);
            this.groupBox18.Controls.Add(this.diod11);
            this.groupBox18.Controls.Add(this.diod10);
            this.groupBox18.Controls.Add(this.diod9);
            this.groupBox18.Controls.Add(this.diod8);
            this.groupBox18.Controls.Add(this.diod7);
            this.groupBox18.Controls.Add(this.diod6);
            this.groupBox18.Controls.Add(this.diod5);
            this.groupBox18.Controls.Add(this.diod4);
            this.groupBox18.Controls.Add(this.diod3);
            this.groupBox18.Controls.Add(this.diod2);
            this.groupBox18.Controls.Add(this.diod1);
            this.groupBox18.Controls.Add(this.label190);
            this.groupBox18.Controls.Add(this.label189);
            this.groupBox18.Controls.Add(this.label179);
            this.groupBox18.Controls.Add(this.label180);
            this.groupBox18.Controls.Add(this.label181);
            this.groupBox18.Controls.Add(this.label182);
            this.groupBox18.Controls.Add(this.label183);
            this.groupBox18.Controls.Add(this.label184);
            this.groupBox18.Controls.Add(this.label185);
            this.groupBox18.Controls.Add(this.label186);
            this.groupBox18.Controls.Add(this.label187);
            this.groupBox18.Controls.Add(this.label188);
            this.groupBox18.Location = new System.Drawing.Point(6, 6);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(85, 170);
            this.groupBox18.TabIndex = 11;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "Инд.";
            // 
            // diod12
            // 
            this.diod12.BackColor = System.Drawing.Color.Transparent;
            this.diod12.Location = new System.Drawing.Point(44, 115);
            this.diod12.Name = "diod12";
            this.diod12.Size = new System.Drawing.Size(14, 14);
            this.diod12.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod12.TabIndex = 30;
            // 
            // diod11
            // 
            this.diod11.BackColor = System.Drawing.Color.Transparent;
            this.diod11.Location = new System.Drawing.Point(44, 95);
            this.diod11.Name = "diod11";
            this.diod11.Size = new System.Drawing.Size(14, 14);
            this.diod11.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod11.TabIndex = 31;
            // 
            // diod10
            // 
            this.diod10.BackColor = System.Drawing.Color.Transparent;
            this.diod10.Location = new System.Drawing.Point(44, 75);
            this.diod10.Name = "diod10";
            this.diod10.Size = new System.Drawing.Size(14, 14);
            this.diod10.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod10.TabIndex = 32;
            // 
            // diod9
            // 
            this.diod9.BackColor = System.Drawing.Color.Transparent;
            this.diod9.Location = new System.Drawing.Point(44, 55);
            this.diod9.Name = "diod9";
            this.diod9.Size = new System.Drawing.Size(14, 14);
            this.diod9.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod9.TabIndex = 33;
            // 
            // diod8
            // 
            this.diod8.BackColor = System.Drawing.Color.Transparent;
            this.diod8.Location = new System.Drawing.Point(44, 35);
            this.diod8.Name = "diod8";
            this.diod8.Size = new System.Drawing.Size(14, 14);
            this.diod8.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod8.TabIndex = 34;
            // 
            // diod7
            // 
            this.diod7.BackColor = System.Drawing.Color.Transparent;
            this.diod7.Location = new System.Drawing.Point(44, 15);
            this.diod7.Name = "diod7";
            this.diod7.Size = new System.Drawing.Size(14, 14);
            this.diod7.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod7.TabIndex = 35;
            // 
            // diod6
            // 
            this.diod6.BackColor = System.Drawing.Color.Transparent;
            this.diod6.Location = new System.Drawing.Point(6, 115);
            this.diod6.Name = "diod6";
            this.diod6.Size = new System.Drawing.Size(14, 14);
            this.diod6.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod6.TabIndex = 36;
            // 
            // diod5
            // 
            this.diod5.BackColor = System.Drawing.Color.Transparent;
            this.diod5.Location = new System.Drawing.Point(6, 95);
            this.diod5.Name = "diod5";
            this.diod5.Size = new System.Drawing.Size(14, 14);
            this.diod5.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod5.TabIndex = 37;
            // 
            // diod4
            // 
            this.diod4.BackColor = System.Drawing.Color.Transparent;
            this.diod4.Location = new System.Drawing.Point(6, 75);
            this.diod4.Name = "diod4";
            this.diod4.Size = new System.Drawing.Size(14, 14);
            this.diod4.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod4.TabIndex = 38;
            // 
            // diod3
            // 
            this.diod3.BackColor = System.Drawing.Color.Transparent;
            this.diod3.Location = new System.Drawing.Point(6, 55);
            this.diod3.Name = "diod3";
            this.diod3.Size = new System.Drawing.Size(14, 14);
            this.diod3.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod3.TabIndex = 39;
            // 
            // diod2
            // 
            this.diod2.BackColor = System.Drawing.Color.Transparent;
            this.diod2.Location = new System.Drawing.Point(6, 35);
            this.diod2.Name = "diod2";
            this.diod2.Size = new System.Drawing.Size(14, 14);
            this.diod2.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod2.TabIndex = 40;
            // 
            // diod1
            // 
            this.diod1.BackColor = System.Drawing.Color.Transparent;
            this.diod1.Location = new System.Drawing.Point(6, 15);
            this.diod1.Name = "diod1";
            this.diod1.Size = new System.Drawing.Size(14, 14);
            this.diod1.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod1.TabIndex = 41;
            // 
            // label190
            // 
            this.label190.AutoSize = true;
            this.label190.Location = new System.Drawing.Point(63, 96);
            this.label190.Name = "label190";
            this.label190.Size = new System.Drawing.Size(19, 13);
            this.label190.TabIndex = 28;
            this.label190.Text = "11";
            // 
            // label189
            // 
            this.label189.AutoSize = true;
            this.label189.Location = new System.Drawing.Point(25, 116);
            this.label189.Name = "label189";
            this.label189.Size = new System.Drawing.Size(13, 13);
            this.label189.TabIndex = 26;
            this.label189.Text = "6";
            // 
            // label179
            // 
            this.label179.AutoSize = true;
            this.label179.Location = new System.Drawing.Point(63, 76);
            this.label179.Name = "label179";
            this.label179.Size = new System.Drawing.Size(19, 13);
            this.label179.TabIndex = 24;
            this.label179.Text = "10";
            // 
            // label180
            // 
            this.label180.AutoSize = true;
            this.label180.Location = new System.Drawing.Point(63, 56);
            this.label180.Name = "label180";
            this.label180.Size = new System.Drawing.Size(13, 13);
            this.label180.TabIndex = 22;
            this.label180.Text = "9";
            // 
            // label181
            // 
            this.label181.AutoSize = true;
            this.label181.Location = new System.Drawing.Point(63, 36);
            this.label181.Name = "label181";
            this.label181.Size = new System.Drawing.Size(13, 13);
            this.label181.TabIndex = 20;
            this.label181.Text = "8";
            // 
            // label182
            // 
            this.label182.AutoSize = true;
            this.label182.Location = new System.Drawing.Point(63, 16);
            this.label182.Name = "label182";
            this.label182.Size = new System.Drawing.Size(13, 13);
            this.label182.TabIndex = 18;
            this.label182.Text = "7";
            // 
            // label183
            // 
            this.label183.AutoSize = true;
            this.label183.Location = new System.Drawing.Point(63, 116);
            this.label183.Name = "label183";
            this.label183.Size = new System.Drawing.Size(19, 13);
            this.label183.TabIndex = 16;
            this.label183.Text = "12";
            // 
            // label184
            // 
            this.label184.AutoSize = true;
            this.label184.Location = new System.Drawing.Point(25, 96);
            this.label184.Name = "label184";
            this.label184.Size = new System.Drawing.Size(13, 13);
            this.label184.TabIndex = 8;
            this.label184.Text = "5";
            // 
            // label185
            // 
            this.label185.AutoSize = true;
            this.label185.Location = new System.Drawing.Point(25, 76);
            this.label185.Name = "label185";
            this.label185.Size = new System.Drawing.Size(13, 13);
            this.label185.TabIndex = 6;
            this.label185.Text = "4";
            // 
            // label186
            // 
            this.label186.AutoSize = true;
            this.label186.Location = new System.Drawing.Point(25, 56);
            this.label186.Name = "label186";
            this.label186.Size = new System.Drawing.Size(13, 13);
            this.label186.TabIndex = 4;
            this.label186.Text = "3";
            // 
            // label187
            // 
            this.label187.AutoSize = true;
            this.label187.Location = new System.Drawing.Point(25, 36);
            this.label187.Name = "label187";
            this.label187.Size = new System.Drawing.Size(13, 13);
            this.label187.TabIndex = 2;
            this.label187.Text = "2";
            // 
            // label188
            // 
            this.label188.AutoSize = true;
            this.label188.Location = new System.Drawing.Point(25, 16);
            this.label188.Name = "label188";
            this.label188.Size = new System.Drawing.Size(13, 13);
            this.label188.TabIndex = 0;
            this.label188.Text = "1";
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this._module34);
            this.groupBox17.Controls.Add(this.label18);
            this.groupBox17.Controls.Add(this._module33);
            this.groupBox17.Controls.Add(this.label20);
            this.groupBox17.Controls.Add(this._module18);
            this.groupBox17.Controls.Add(this._module10);
            this.groupBox17.Controls.Add(this.label161);
            this.groupBox17.Controls.Add(this._module32);
            this.groupBox17.Controls.Add(this._module17);
            this.groupBox17.Controls.Add(this.label138);
            this.groupBox17.Controls.Add(this.label162);
            this.groupBox17.Controls.Add(this._module31);
            this.groupBox17.Controls.Add(this.label164);
            this.groupBox17.Controls.Add(this.label139);
            this.groupBox17.Controls.Add(this._module9);
            this.groupBox17.Controls.Add(this._module30);
            this.groupBox17.Controls.Add(this._module16);
            this.groupBox17.Controls.Add(this.label140);
            this.groupBox17.Controls.Add(this.label165);
            this.groupBox17.Controls.Add(this._module29);
            this.groupBox17.Controls.Add(this.label163);
            this.groupBox17.Controls.Add(this.label141);
            this.groupBox17.Controls.Add(this._module15);
            this.groupBox17.Controls.Add(this._module28);
            this.groupBox17.Controls.Add(this._module5);
            this.groupBox17.Controls.Add(this.label142);
            this.groupBox17.Controls.Add(this.label169);
            this.groupBox17.Controls.Add(this._module27);
            this.groupBox17.Controls.Add(this._module8);
            this.groupBox17.Controls.Add(this.label232);
            this.groupBox17.Controls.Add(this._module14);
            this.groupBox17.Controls.Add(this._module26);
            this.groupBox17.Controls.Add(this.label176);
            this.groupBox17.Controls.Add(this.label127);
            this.groupBox17.Controls.Add(this.label170);
            this.groupBox17.Controls.Add(this._module25);
            this.groupBox17.Controls.Add(this.label128);
            this.groupBox17.Controls.Add(this.label166);
            this.groupBox17.Controls.Add(this._module13);
            this.groupBox17.Controls.Add(this._module24);
            this.groupBox17.Controls.Add(this._module1);
            this.groupBox17.Controls.Add(this.label129);
            this.groupBox17.Controls.Add(this.label171);
            this.groupBox17.Controls.Add(this._module23);
            this.groupBox17.Controls.Add(this._module7);
            this.groupBox17.Controls.Add(this.label130);
            this.groupBox17.Controls.Add(this._module12);
            this.groupBox17.Controls.Add(this._module22);
            this.groupBox17.Controls.Add(this.label175);
            this.groupBox17.Controls.Add(this.label131);
            this.groupBox17.Controls.Add(this.label177);
            this.groupBox17.Controls.Add(this._module21);
            this.groupBox17.Controls.Add(this.label167);
            this.groupBox17.Controls.Add(this.label132);
            this.groupBox17.Controls.Add(this._module11);
            this.groupBox17.Controls.Add(this._module20);
            this.groupBox17.Controls.Add(this.label178);
            this.groupBox17.Controls.Add(this.label133);
            this.groupBox17.Controls.Add(this._module2);
            this.groupBox17.Controls.Add(this._module19);
            this.groupBox17.Controls.Add(this.label134);
            this.groupBox17.Controls.Add(this._module6);
            this.groupBox17.Controls.Add(this.label168);
            this.groupBox17.Controls.Add(this.label174);
            this.groupBox17.Controls.Add(this._module3);
            this.groupBox17.Controls.Add(this.label173);
            this.groupBox17.Controls.Add(this.label172);
            this.groupBox17.Controls.Add(this._module4);
            this.groupBox17.Location = new System.Drawing.Point(97, 6);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(242, 169);
            this.groupBox17.TabIndex = 10;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "Реле";
            // 
            // _module34
            // 
            this._module34.Location = new System.Drawing.Point(200, 151);
            this._module34.Name = "_module34";
            this._module34.Size = new System.Drawing.Size(13, 13);
            this._module34.State = BEMN.Forms.LedState.Off;
            this._module34.TabIndex = 31;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(219, 151);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(19, 13);
            this.label18.TabIndex = 30;
            this.label18.Text = "34";
            // 
            // _module33
            // 
            this._module33.Location = new System.Drawing.Point(200, 132);
            this._module33.Name = "_module33";
            this._module33.Size = new System.Drawing.Size(13, 13);
            this._module33.State = BEMN.Forms.LedState.Off;
            this._module33.TabIndex = 29;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(219, 132);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(19, 13);
            this.label20.TabIndex = 28;
            this.label20.Text = "33";
            // 
            // _module18
            // 
            this._module18.Location = new System.Drawing.Point(112, 151);
            this._module18.Name = "_module18";
            this._module18.Size = new System.Drawing.Size(13, 13);
            this._module18.State = BEMN.Forms.LedState.Off;
            this._module18.TabIndex = 15;
            // 
            // _module10
            // 
            this._module10.Location = new System.Drawing.Point(71, 95);
            this._module10.Name = "_module10";
            this._module10.Size = new System.Drawing.Size(13, 13);
            this._module10.State = BEMN.Forms.LedState.Off;
            this._module10.TabIndex = 25;
            // 
            // label161
            // 
            this.label161.AutoSize = true;
            this.label161.Location = new System.Drawing.Point(131, 151);
            this.label161.Name = "label161";
            this.label161.Size = new System.Drawing.Size(19, 13);
            this.label161.TabIndex = 14;
            this.label161.Text = "18";
            // 
            // _module32
            // 
            this._module32.Location = new System.Drawing.Point(200, 113);
            this._module32.Name = "_module32";
            this._module32.Size = new System.Drawing.Size(13, 13);
            this._module32.State = BEMN.Forms.LedState.Off;
            this._module32.TabIndex = 27;
            // 
            // _module17
            // 
            this._module17.Location = new System.Drawing.Point(112, 132);
            this._module17.Name = "_module17";
            this._module17.Size = new System.Drawing.Size(13, 13);
            this._module17.State = BEMN.Forms.LedState.Off;
            this._module17.TabIndex = 13;
            // 
            // label138
            // 
            this.label138.AutoSize = true;
            this.label138.Location = new System.Drawing.Point(219, 113);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(19, 13);
            this.label138.TabIndex = 26;
            this.label138.Text = "32";
            // 
            // label162
            // 
            this.label162.AutoSize = true;
            this.label162.Location = new System.Drawing.Point(131, 132);
            this.label162.Name = "label162";
            this.label162.Size = new System.Drawing.Size(19, 13);
            this.label162.TabIndex = 12;
            this.label162.Text = "17";
            // 
            // _module31
            // 
            this._module31.Location = new System.Drawing.Point(200, 94);
            this._module31.Name = "_module31";
            this._module31.Size = new System.Drawing.Size(13, 13);
            this._module31.State = BEMN.Forms.LedState.Off;
            this._module31.TabIndex = 25;
            // 
            // label164
            // 
            this.label164.AutoSize = true;
            this.label164.Location = new System.Drawing.Point(90, 95);
            this.label164.Name = "label164";
            this.label164.Size = new System.Drawing.Size(19, 13);
            this.label164.TabIndex = 24;
            this.label164.Text = "10";
            // 
            // label139
            // 
            this.label139.AutoSize = true;
            this.label139.Location = new System.Drawing.Point(219, 94);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(19, 13);
            this.label139.TabIndex = 24;
            this.label139.Text = "31";
            // 
            // _module9
            // 
            this._module9.Location = new System.Drawing.Point(71, 76);
            this._module9.Name = "_module9";
            this._module9.Size = new System.Drawing.Size(13, 13);
            this._module9.State = BEMN.Forms.LedState.Off;
            this._module9.TabIndex = 23;
            // 
            // _module30
            // 
            this._module30.Location = new System.Drawing.Point(200, 75);
            this._module30.Name = "_module30";
            this._module30.Size = new System.Drawing.Size(13, 13);
            this._module30.State = BEMN.Forms.LedState.Off;
            this._module30.TabIndex = 23;
            // 
            // _module16
            // 
            this._module16.Location = new System.Drawing.Point(112, 113);
            this._module16.Name = "_module16";
            this._module16.Size = new System.Drawing.Size(13, 13);
            this._module16.State = BEMN.Forms.LedState.Off;
            this._module16.TabIndex = 11;
            // 
            // label140
            // 
            this.label140.AutoSize = true;
            this.label140.Location = new System.Drawing.Point(219, 75);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(19, 13);
            this.label140.TabIndex = 22;
            this.label140.Text = "30";
            // 
            // label165
            // 
            this.label165.AutoSize = true;
            this.label165.Location = new System.Drawing.Point(90, 76);
            this.label165.Name = "label165";
            this.label165.Size = new System.Drawing.Size(13, 13);
            this.label165.TabIndex = 22;
            this.label165.Text = "9";
            // 
            // _module29
            // 
            this._module29.Location = new System.Drawing.Point(200, 56);
            this._module29.Name = "_module29";
            this._module29.Size = new System.Drawing.Size(13, 13);
            this._module29.State = BEMN.Forms.LedState.Off;
            this._module29.TabIndex = 21;
            // 
            // label163
            // 
            this.label163.AutoSize = true;
            this.label163.Location = new System.Drawing.Point(131, 113);
            this.label163.Name = "label163";
            this.label163.Size = new System.Drawing.Size(19, 13);
            this.label163.TabIndex = 10;
            this.label163.Text = "16";
            // 
            // label141
            // 
            this.label141.AutoSize = true;
            this.label141.Location = new System.Drawing.Point(219, 56);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(19, 13);
            this.label141.TabIndex = 20;
            this.label141.Text = "29";
            // 
            // _module15
            // 
            this._module15.Location = new System.Drawing.Point(112, 94);
            this._module15.Name = "_module15";
            this._module15.Size = new System.Drawing.Size(13, 13);
            this._module15.State = BEMN.Forms.LedState.Off;
            this._module15.TabIndex = 9;
            // 
            // _module28
            // 
            this._module28.Location = new System.Drawing.Point(200, 37);
            this._module28.Name = "_module28";
            this._module28.Size = new System.Drawing.Size(13, 13);
            this._module28.State = BEMN.Forms.LedState.Off;
            this._module28.TabIndex = 19;
            // 
            // _module5
            // 
            this._module5.Location = new System.Drawing.Point(6, 95);
            this._module5.Name = "_module5";
            this._module5.Size = new System.Drawing.Size(13, 13);
            this._module5.State = BEMN.Forms.LedState.Off;
            this._module5.TabIndex = 9;
            // 
            // label142
            // 
            this.label142.AutoSize = true;
            this.label142.Location = new System.Drawing.Point(219, 37);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(19, 13);
            this.label142.TabIndex = 18;
            this.label142.Text = "28";
            // 
            // label169
            // 
            this.label169.AutoSize = true;
            this.label169.Location = new System.Drawing.Point(131, 94);
            this.label169.Name = "label169";
            this.label169.Size = new System.Drawing.Size(19, 13);
            this.label169.TabIndex = 8;
            this.label169.Text = "15";
            // 
            // _module27
            // 
            this._module27.Location = new System.Drawing.Point(200, 18);
            this._module27.Name = "_module27";
            this._module27.Size = new System.Drawing.Size(13, 13);
            this._module27.State = BEMN.Forms.LedState.Off;
            this._module27.TabIndex = 17;
            // 
            // _module8
            // 
            this._module8.Location = new System.Drawing.Point(71, 57);
            this._module8.Name = "_module8";
            this._module8.Size = new System.Drawing.Size(13, 13);
            this._module8.State = BEMN.Forms.LedState.Off;
            this._module8.TabIndex = 21;
            // 
            // label232
            // 
            this.label232.AutoSize = true;
            this.label232.Location = new System.Drawing.Point(219, 18);
            this.label232.Name = "label232";
            this.label232.Size = new System.Drawing.Size(19, 13);
            this.label232.TabIndex = 16;
            this.label232.Text = "27";
            // 
            // _module14
            // 
            this._module14.Location = new System.Drawing.Point(112, 75);
            this._module14.Name = "_module14";
            this._module14.Size = new System.Drawing.Size(13, 13);
            this._module14.State = BEMN.Forms.LedState.Off;
            this._module14.TabIndex = 7;
            // 
            // _module26
            // 
            this._module26.Location = new System.Drawing.Point(157, 151);
            this._module26.Name = "_module26";
            this._module26.Size = new System.Drawing.Size(13, 13);
            this._module26.State = BEMN.Forms.LedState.Off;
            this._module26.TabIndex = 15;
            // 
            // label176
            // 
            this.label176.AutoSize = true;
            this.label176.Location = new System.Drawing.Point(25, 19);
            this.label176.Name = "label176";
            this.label176.Size = new System.Drawing.Size(38, 13);
            this.label176.TabIndex = 0;
            this.label176.Text = "1(Вкл)";
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Location = new System.Drawing.Point(176, 151);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(19, 13);
            this.label127.TabIndex = 14;
            this.label127.Text = "26";
            // 
            // label170
            // 
            this.label170.AutoSize = true;
            this.label170.Location = new System.Drawing.Point(131, 75);
            this.label170.Name = "label170";
            this.label170.Size = new System.Drawing.Size(19, 13);
            this.label170.TabIndex = 6;
            this.label170.Text = "14";
            // 
            // _module25
            // 
            this._module25.Location = new System.Drawing.Point(157, 132);
            this._module25.Name = "_module25";
            this._module25.Size = new System.Drawing.Size(13, 13);
            this._module25.State = BEMN.Forms.LedState.Off;
            this._module25.TabIndex = 13;
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Location = new System.Drawing.Point(176, 132);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(19, 13);
            this.label128.TabIndex = 12;
            this.label128.Text = "25";
            // 
            // label166
            // 
            this.label166.AutoSize = true;
            this.label166.Location = new System.Drawing.Point(90, 57);
            this.label166.Name = "label166";
            this.label166.Size = new System.Drawing.Size(13, 13);
            this.label166.TabIndex = 20;
            this.label166.Text = "8";
            // 
            // _module13
            // 
            this._module13.Location = new System.Drawing.Point(112, 57);
            this._module13.Name = "_module13";
            this._module13.Size = new System.Drawing.Size(13, 13);
            this._module13.State = BEMN.Forms.LedState.Off;
            this._module13.TabIndex = 5;
            // 
            // _module24
            // 
            this._module24.Location = new System.Drawing.Point(157, 113);
            this._module24.Name = "_module24";
            this._module24.Size = new System.Drawing.Size(13, 13);
            this._module24.State = BEMN.Forms.LedState.Off;
            this._module24.TabIndex = 11;
            // 
            // _module1
            // 
            this._module1.Location = new System.Drawing.Point(6, 19);
            this._module1.Name = "_module1";
            this._module1.Size = new System.Drawing.Size(13, 13);
            this._module1.State = BEMN.Forms.LedState.Off;
            this._module1.TabIndex = 1;
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Location = new System.Drawing.Point(176, 113);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(19, 13);
            this.label129.TabIndex = 10;
            this.label129.Text = "24";
            // 
            // label171
            // 
            this.label171.AutoSize = true;
            this.label171.Location = new System.Drawing.Point(131, 57);
            this.label171.Name = "label171";
            this.label171.Size = new System.Drawing.Size(19, 13);
            this.label171.TabIndex = 4;
            this.label171.Text = "13";
            // 
            // _module23
            // 
            this._module23.Location = new System.Drawing.Point(157, 94);
            this._module23.Name = "_module23";
            this._module23.Size = new System.Drawing.Size(13, 13);
            this._module23.State = BEMN.Forms.LedState.Off;
            this._module23.TabIndex = 9;
            // 
            // _module7
            // 
            this._module7.Location = new System.Drawing.Point(71, 38);
            this._module7.Name = "_module7";
            this._module7.Size = new System.Drawing.Size(13, 13);
            this._module7.State = BEMN.Forms.LedState.Off;
            this._module7.TabIndex = 19;
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Location = new System.Drawing.Point(176, 94);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(19, 13);
            this.label130.TabIndex = 8;
            this.label130.Text = "23";
            // 
            // _module12
            // 
            this._module12.Location = new System.Drawing.Point(112, 38);
            this._module12.Name = "_module12";
            this._module12.Size = new System.Drawing.Size(13, 13);
            this._module12.State = BEMN.Forms.LedState.Off;
            this._module12.TabIndex = 3;
            // 
            // _module22
            // 
            this._module22.Location = new System.Drawing.Point(157, 75);
            this._module22.Name = "_module22";
            this._module22.Size = new System.Drawing.Size(13, 13);
            this._module22.State = BEMN.Forms.LedState.Off;
            this._module22.TabIndex = 7;
            // 
            // label175
            // 
            this.label175.AutoSize = true;
            this.label175.Location = new System.Drawing.Point(25, 38);
            this.label175.Name = "label175";
            this.label175.Size = new System.Drawing.Size(44, 13);
            this.label175.TabIndex = 2;
            this.label175.Text = "2(Откл)";
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Location = new System.Drawing.Point(176, 75);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(19, 13);
            this.label131.TabIndex = 6;
            this.label131.Text = "22";
            // 
            // label177
            // 
            this.label177.AutoSize = true;
            this.label177.Location = new System.Drawing.Point(131, 38);
            this.label177.Name = "label177";
            this.label177.Size = new System.Drawing.Size(19, 13);
            this.label177.TabIndex = 2;
            this.label177.Text = "12";
            // 
            // _module21
            // 
            this._module21.Location = new System.Drawing.Point(157, 56);
            this._module21.Name = "_module21";
            this._module21.Size = new System.Drawing.Size(13, 13);
            this._module21.State = BEMN.Forms.LedState.Off;
            this._module21.TabIndex = 5;
            // 
            // label167
            // 
            this.label167.AutoSize = true;
            this.label167.Location = new System.Drawing.Point(90, 38);
            this.label167.Name = "label167";
            this.label167.Size = new System.Drawing.Size(13, 13);
            this.label167.TabIndex = 18;
            this.label167.Text = "7";
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.Location = new System.Drawing.Point(176, 56);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(19, 13);
            this.label132.TabIndex = 4;
            this.label132.Text = "21";
            // 
            // _module11
            // 
            this._module11.Location = new System.Drawing.Point(112, 19);
            this._module11.Name = "_module11";
            this._module11.Size = new System.Drawing.Size(13, 13);
            this._module11.State = BEMN.Forms.LedState.Off;
            this._module11.TabIndex = 1;
            // 
            // _module20
            // 
            this._module20.Location = new System.Drawing.Point(157, 37);
            this._module20.Name = "_module20";
            this._module20.Size = new System.Drawing.Size(13, 13);
            this._module20.State = BEMN.Forms.LedState.Off;
            this._module20.TabIndex = 3;
            // 
            // label178
            // 
            this.label178.AutoSize = true;
            this.label178.Location = new System.Drawing.Point(131, 19);
            this.label178.Name = "label178";
            this.label178.Size = new System.Drawing.Size(19, 13);
            this.label178.TabIndex = 0;
            this.label178.Text = "11";
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.Location = new System.Drawing.Point(176, 37);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(19, 13);
            this.label133.TabIndex = 2;
            this.label133.Text = "20";
            // 
            // _module2
            // 
            this._module2.Location = new System.Drawing.Point(6, 38);
            this._module2.Name = "_module2";
            this._module2.Size = new System.Drawing.Size(13, 13);
            this._module2.State = BEMN.Forms.LedState.Off;
            this._module2.TabIndex = 3;
            // 
            // _module19
            // 
            this._module19.Location = new System.Drawing.Point(157, 18);
            this._module19.Name = "_module19";
            this._module19.Size = new System.Drawing.Size(13, 13);
            this._module19.State = BEMN.Forms.LedState.Off;
            this._module19.TabIndex = 1;
            // 
            // label134
            // 
            this.label134.AutoSize = true;
            this.label134.Location = new System.Drawing.Point(176, 18);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(19, 13);
            this.label134.TabIndex = 0;
            this.label134.Text = "19";
            // 
            // _module6
            // 
            this._module6.Location = new System.Drawing.Point(71, 19);
            this._module6.Name = "_module6";
            this._module6.Size = new System.Drawing.Size(13, 13);
            this._module6.State = BEMN.Forms.LedState.Off;
            this._module6.TabIndex = 17;
            // 
            // label168
            // 
            this.label168.AutoSize = true;
            this.label168.Location = new System.Drawing.Point(90, 19);
            this.label168.Name = "label168";
            this.label168.Size = new System.Drawing.Size(13, 13);
            this.label168.TabIndex = 16;
            this.label168.Text = "6";
            // 
            // label174
            // 
            this.label174.AutoSize = true;
            this.label174.Location = new System.Drawing.Point(25, 57);
            this.label174.Name = "label174";
            this.label174.Size = new System.Drawing.Size(13, 13);
            this.label174.TabIndex = 4;
            this.label174.Text = "3";
            // 
            // _module3
            // 
            this._module3.Location = new System.Drawing.Point(6, 57);
            this._module3.Name = "_module3";
            this._module3.Size = new System.Drawing.Size(13, 13);
            this._module3.State = BEMN.Forms.LedState.Off;
            this._module3.TabIndex = 5;
            // 
            // label173
            // 
            this.label173.AutoSize = true;
            this.label173.Location = new System.Drawing.Point(25, 76);
            this.label173.Name = "label173";
            this.label173.Size = new System.Drawing.Size(13, 13);
            this.label173.TabIndex = 6;
            this.label173.Text = "4";
            // 
            // label172
            // 
            this.label172.AutoSize = true;
            this.label172.Location = new System.Drawing.Point(25, 95);
            this.label172.Name = "label172";
            this.label172.Size = new System.Drawing.Size(13, 13);
            this.label172.TabIndex = 8;
            this.label172.Text = "5";
            // 
            // _module4
            // 
            this._module4.Location = new System.Drawing.Point(6, 76);
            this._module4.Name = "_module4";
            this._module4.Size = new System.Drawing.Size(13, 13);
            this._module4.State = BEMN.Forms.LedState.Off;
            this._module4.TabIndex = 7;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this._vls16);
            this.groupBox10.Controls.Add(this.label63);
            this.groupBox10.Controls.Add(this._vls15);
            this.groupBox10.Controls.Add(this.label64);
            this.groupBox10.Controls.Add(this._vls14);
            this.groupBox10.Controls.Add(this.label65);
            this.groupBox10.Controls.Add(this._vls13);
            this.groupBox10.Controls.Add(this.label66);
            this.groupBox10.Controls.Add(this._vls12);
            this.groupBox10.Controls.Add(this.label67);
            this.groupBox10.Controls.Add(this._vls11);
            this.groupBox10.Controls.Add(this.label68);
            this.groupBox10.Controls.Add(this._vls10);
            this.groupBox10.Controls.Add(this.label69);
            this.groupBox10.Controls.Add(this._vls9);
            this.groupBox10.Controls.Add(this.label70);
            this.groupBox10.Controls.Add(this._vls8);
            this.groupBox10.Controls.Add(this.label71);
            this.groupBox10.Controls.Add(this._vls7);
            this.groupBox10.Controls.Add(this.label72);
            this.groupBox10.Controls.Add(this._vls6);
            this.groupBox10.Controls.Add(this.label73);
            this.groupBox10.Controls.Add(this._vls5);
            this.groupBox10.Controls.Add(this.label74);
            this.groupBox10.Controls.Add(this._vls4);
            this.groupBox10.Controls.Add(this.label75);
            this.groupBox10.Controls.Add(this._vls3);
            this.groupBox10.Controls.Add(this.label76);
            this.groupBox10.Controls.Add(this._vls2);
            this.groupBox10.Controls.Add(this.label77);
            this.groupBox10.Controls.Add(this._vls1);
            this.groupBox10.Controls.Add(this.label78);
            this.groupBox10.Location = new System.Drawing.Point(443, 182);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(141, 191);
            this.groupBox10.TabIndex = 3;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Выходные логические сигналы ВЛС";
            // 
            // _vls16
            // 
            this._vls16.Location = new System.Drawing.Point(71, 166);
            this._vls16.Name = "_vls16";
            this._vls16.Size = new System.Drawing.Size(13, 13);
            this._vls16.State = BEMN.Forms.LedState.Off;
            this._vls16.TabIndex = 31;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(90, 166);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(41, 13);
            this.label63.TabIndex = 30;
            this.label63.Text = "ВЛС16";
            // 
            // _vls15
            // 
            this._vls15.Location = new System.Drawing.Point(71, 147);
            this._vls15.Name = "_vls15";
            this._vls15.Size = new System.Drawing.Size(13, 13);
            this._vls15.State = BEMN.Forms.LedState.Off;
            this._vls15.TabIndex = 29;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(90, 147);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(41, 13);
            this.label64.TabIndex = 28;
            this.label64.Text = "ВЛС15";
            // 
            // _vls14
            // 
            this._vls14.Location = new System.Drawing.Point(71, 128);
            this._vls14.Name = "_vls14";
            this._vls14.Size = new System.Drawing.Size(13, 13);
            this._vls14.State = BEMN.Forms.LedState.Off;
            this._vls14.TabIndex = 27;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(90, 128);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(41, 13);
            this.label65.TabIndex = 26;
            this.label65.Text = "ВЛС14";
            // 
            // _vls13
            // 
            this._vls13.Location = new System.Drawing.Point(71, 109);
            this._vls13.Name = "_vls13";
            this._vls13.Size = new System.Drawing.Size(13, 13);
            this._vls13.State = BEMN.Forms.LedState.Off;
            this._vls13.TabIndex = 25;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(90, 109);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(41, 13);
            this.label66.TabIndex = 24;
            this.label66.Text = "ВЛС13";
            // 
            // _vls12
            // 
            this._vls12.Location = new System.Drawing.Point(71, 90);
            this._vls12.Name = "_vls12";
            this._vls12.Size = new System.Drawing.Size(13, 13);
            this._vls12.State = BEMN.Forms.LedState.Off;
            this._vls12.TabIndex = 23;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(90, 90);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(41, 13);
            this.label67.TabIndex = 22;
            this.label67.Text = "ВЛС12";
            // 
            // _vls11
            // 
            this._vls11.Location = new System.Drawing.Point(71, 71);
            this._vls11.Name = "_vls11";
            this._vls11.Size = new System.Drawing.Size(13, 13);
            this._vls11.State = BEMN.Forms.LedState.Off;
            this._vls11.TabIndex = 21;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(90, 71);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(41, 13);
            this.label68.TabIndex = 20;
            this.label68.Text = "ВЛС11";
            // 
            // _vls10
            // 
            this._vls10.Location = new System.Drawing.Point(71, 52);
            this._vls10.Name = "_vls10";
            this._vls10.Size = new System.Drawing.Size(13, 13);
            this._vls10.State = BEMN.Forms.LedState.Off;
            this._vls10.TabIndex = 19;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(90, 52);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(41, 13);
            this.label69.TabIndex = 18;
            this.label69.Text = "ВЛС10";
            // 
            // _vls9
            // 
            this._vls9.Location = new System.Drawing.Point(71, 33);
            this._vls9.Name = "_vls9";
            this._vls9.Size = new System.Drawing.Size(13, 13);
            this._vls9.State = BEMN.Forms.LedState.Off;
            this._vls9.TabIndex = 17;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(90, 33);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(35, 13);
            this.label70.TabIndex = 16;
            this.label70.Text = "ВЛС9";
            // 
            // _vls8
            // 
            this._vls8.Location = new System.Drawing.Point(6, 166);
            this._vls8.Name = "_vls8";
            this._vls8.Size = new System.Drawing.Size(13, 13);
            this._vls8.State = BEMN.Forms.LedState.Off;
            this._vls8.TabIndex = 15;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(25, 166);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(35, 13);
            this.label71.TabIndex = 14;
            this.label71.Text = "ВЛС8";
            // 
            // _vls7
            // 
            this._vls7.Location = new System.Drawing.Point(6, 147);
            this._vls7.Name = "_vls7";
            this._vls7.Size = new System.Drawing.Size(13, 13);
            this._vls7.State = BEMN.Forms.LedState.Off;
            this._vls7.TabIndex = 13;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(25, 147);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(35, 13);
            this.label72.TabIndex = 12;
            this.label72.Text = "ВЛС7";
            // 
            // _vls6
            // 
            this._vls6.Location = new System.Drawing.Point(6, 128);
            this._vls6.Name = "_vls6";
            this._vls6.Size = new System.Drawing.Size(13, 13);
            this._vls6.State = BEMN.Forms.LedState.Off;
            this._vls6.TabIndex = 11;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(25, 128);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(35, 13);
            this.label73.TabIndex = 10;
            this.label73.Text = "ВЛС6";
            // 
            // _vls5
            // 
            this._vls5.Location = new System.Drawing.Point(6, 109);
            this._vls5.Name = "_vls5";
            this._vls5.Size = new System.Drawing.Size(13, 13);
            this._vls5.State = BEMN.Forms.LedState.Off;
            this._vls5.TabIndex = 9;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(25, 109);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(35, 13);
            this.label74.TabIndex = 8;
            this.label74.Text = "ВЛС5";
            // 
            // _vls4
            // 
            this._vls4.Location = new System.Drawing.Point(6, 90);
            this._vls4.Name = "_vls4";
            this._vls4.Size = new System.Drawing.Size(13, 13);
            this._vls4.State = BEMN.Forms.LedState.Off;
            this._vls4.TabIndex = 7;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(25, 90);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(35, 13);
            this.label75.TabIndex = 6;
            this.label75.Text = "ВЛС4";
            // 
            // _vls3
            // 
            this._vls3.Location = new System.Drawing.Point(6, 71);
            this._vls3.Name = "_vls3";
            this._vls3.Size = new System.Drawing.Size(13, 13);
            this._vls3.State = BEMN.Forms.LedState.Off;
            this._vls3.TabIndex = 5;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(25, 71);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(35, 13);
            this.label76.TabIndex = 4;
            this.label76.Text = "ВЛС3";
            // 
            // _vls2
            // 
            this._vls2.Location = new System.Drawing.Point(6, 52);
            this._vls2.Name = "_vls2";
            this._vls2.Size = new System.Drawing.Size(13, 13);
            this._vls2.State = BEMN.Forms.LedState.Off;
            this._vls2.TabIndex = 3;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(25, 52);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(35, 13);
            this.label77.TabIndex = 2;
            this.label77.Text = "ВЛС2";
            // 
            // _vls1
            // 
            this._vls1.Location = new System.Drawing.Point(6, 33);
            this._vls1.Name = "_vls1";
            this._vls1.Size = new System.Drawing.Size(13, 13);
            this._vls1.State = BEMN.Forms.LedState.Off;
            this._vls1.TabIndex = 1;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(25, 33);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(35, 13);
            this.label78.TabIndex = 0;
            this.label78.Text = "ВЛС1";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this._ls16);
            this.groupBox9.Controls.Add(this.label47);
            this.groupBox9.Controls.Add(this._ls15);
            this.groupBox9.Controls.Add(this.label48);
            this.groupBox9.Controls.Add(this._ls14);
            this.groupBox9.Controls.Add(this.label49);
            this.groupBox9.Controls.Add(this._ls13);
            this.groupBox9.Controls.Add(this.label50);
            this.groupBox9.Controls.Add(this._ls12);
            this.groupBox9.Controls.Add(this.label51);
            this.groupBox9.Controls.Add(this._ls11);
            this.groupBox9.Controls.Add(this.label52);
            this.groupBox9.Controls.Add(this._ls10);
            this.groupBox9.Controls.Add(this.label53);
            this.groupBox9.Controls.Add(this._ls9);
            this.groupBox9.Controls.Add(this.label54);
            this.groupBox9.Controls.Add(this._ls8);
            this.groupBox9.Controls.Add(this.label55);
            this.groupBox9.Controls.Add(this._ls7);
            this.groupBox9.Controls.Add(this.label56);
            this.groupBox9.Controls.Add(this._ls6);
            this.groupBox9.Controls.Add(this.label57);
            this.groupBox9.Controls.Add(this._ls5);
            this.groupBox9.Controls.Add(this.label58);
            this.groupBox9.Controls.Add(this._ls4);
            this.groupBox9.Controls.Add(this.label59);
            this.groupBox9.Controls.Add(this._ls3);
            this.groupBox9.Controls.Add(this.label60);
            this.groupBox9.Controls.Add(this._ls2);
            this.groupBox9.Controls.Add(this.label61);
            this.groupBox9.Controls.Add(this._ls1);
            this.groupBox9.Controls.Add(this.label62);
            this.groupBox9.Location = new System.Drawing.Point(296, 182);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(141, 191);
            this.groupBox9.TabIndex = 2;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Входные логические сигналы ЛС";
            // 
            // _ls16
            // 
            this._ls16.Location = new System.Drawing.Point(71, 166);
            this._ls16.Name = "_ls16";
            this._ls16.Size = new System.Drawing.Size(13, 13);
            this._ls16.State = BEMN.Forms.LedState.Off;
            this._ls16.TabIndex = 31;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(90, 166);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(34, 13);
            this.label47.TabIndex = 30;
            this.label47.Text = "ЛС16";
            // 
            // _ls15
            // 
            this._ls15.Location = new System.Drawing.Point(71, 147);
            this._ls15.Name = "_ls15";
            this._ls15.Size = new System.Drawing.Size(13, 13);
            this._ls15.State = BEMN.Forms.LedState.Off;
            this._ls15.TabIndex = 29;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(90, 147);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(34, 13);
            this.label48.TabIndex = 28;
            this.label48.Text = "ЛС15";
            // 
            // _ls14
            // 
            this._ls14.Location = new System.Drawing.Point(71, 128);
            this._ls14.Name = "_ls14";
            this._ls14.Size = new System.Drawing.Size(13, 13);
            this._ls14.State = BEMN.Forms.LedState.Off;
            this._ls14.TabIndex = 27;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(90, 128);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(34, 13);
            this.label49.TabIndex = 26;
            this.label49.Text = "ЛС14";
            // 
            // _ls13
            // 
            this._ls13.Location = new System.Drawing.Point(71, 109);
            this._ls13.Name = "_ls13";
            this._ls13.Size = new System.Drawing.Size(13, 13);
            this._ls13.State = BEMN.Forms.LedState.Off;
            this._ls13.TabIndex = 25;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(90, 109);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(34, 13);
            this.label50.TabIndex = 24;
            this.label50.Text = "ЛС13";
            // 
            // _ls12
            // 
            this._ls12.Location = new System.Drawing.Point(71, 90);
            this._ls12.Name = "_ls12";
            this._ls12.Size = new System.Drawing.Size(13, 13);
            this._ls12.State = BEMN.Forms.LedState.Off;
            this._ls12.TabIndex = 23;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(90, 90);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(34, 13);
            this.label51.TabIndex = 22;
            this.label51.Text = "ЛС12";
            // 
            // _ls11
            // 
            this._ls11.Location = new System.Drawing.Point(71, 71);
            this._ls11.Name = "_ls11";
            this._ls11.Size = new System.Drawing.Size(13, 13);
            this._ls11.State = BEMN.Forms.LedState.Off;
            this._ls11.TabIndex = 21;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(90, 71);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(34, 13);
            this.label52.TabIndex = 20;
            this.label52.Text = "ЛС11";
            // 
            // _ls10
            // 
            this._ls10.Location = new System.Drawing.Point(71, 52);
            this._ls10.Name = "_ls10";
            this._ls10.Size = new System.Drawing.Size(13, 13);
            this._ls10.State = BEMN.Forms.LedState.Off;
            this._ls10.TabIndex = 19;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(90, 52);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(34, 13);
            this.label53.TabIndex = 18;
            this.label53.Text = "ЛС10";
            // 
            // _ls9
            // 
            this._ls9.Location = new System.Drawing.Point(71, 33);
            this._ls9.Name = "_ls9";
            this._ls9.Size = new System.Drawing.Size(13, 13);
            this._ls9.State = BEMN.Forms.LedState.Off;
            this._ls9.TabIndex = 17;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(90, 33);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(28, 13);
            this.label54.TabIndex = 16;
            this.label54.Text = "ЛС9";
            // 
            // _ls8
            // 
            this._ls8.Location = new System.Drawing.Point(6, 166);
            this._ls8.Name = "_ls8";
            this._ls8.Size = new System.Drawing.Size(13, 13);
            this._ls8.State = BEMN.Forms.LedState.Off;
            this._ls8.TabIndex = 15;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(25, 166);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(28, 13);
            this.label55.TabIndex = 14;
            this.label55.Text = "ЛС8";
            // 
            // _ls7
            // 
            this._ls7.Location = new System.Drawing.Point(6, 147);
            this._ls7.Name = "_ls7";
            this._ls7.Size = new System.Drawing.Size(13, 13);
            this._ls7.State = BEMN.Forms.LedState.Off;
            this._ls7.TabIndex = 13;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(25, 147);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(28, 13);
            this.label56.TabIndex = 12;
            this.label56.Text = "ЛС7";
            // 
            // _ls6
            // 
            this._ls6.Location = new System.Drawing.Point(6, 128);
            this._ls6.Name = "_ls6";
            this._ls6.Size = new System.Drawing.Size(13, 13);
            this._ls6.State = BEMN.Forms.LedState.Off;
            this._ls6.TabIndex = 11;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(25, 128);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(28, 13);
            this.label57.TabIndex = 10;
            this.label57.Text = "ЛС6";
            // 
            // _ls5
            // 
            this._ls5.Location = new System.Drawing.Point(6, 109);
            this._ls5.Name = "_ls5";
            this._ls5.Size = new System.Drawing.Size(13, 13);
            this._ls5.State = BEMN.Forms.LedState.Off;
            this._ls5.TabIndex = 9;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(25, 109);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(28, 13);
            this.label58.TabIndex = 8;
            this.label58.Text = "ЛС5";
            // 
            // _ls4
            // 
            this._ls4.Location = new System.Drawing.Point(6, 90);
            this._ls4.Name = "_ls4";
            this._ls4.Size = new System.Drawing.Size(13, 13);
            this._ls4.State = BEMN.Forms.LedState.Off;
            this._ls4.TabIndex = 7;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(25, 90);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(28, 13);
            this.label59.TabIndex = 6;
            this.label59.Text = "ЛС4";
            // 
            // _ls3
            // 
            this._ls3.Location = new System.Drawing.Point(6, 71);
            this._ls3.Name = "_ls3";
            this._ls3.Size = new System.Drawing.Size(13, 13);
            this._ls3.State = BEMN.Forms.LedState.Off;
            this._ls3.TabIndex = 5;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(25, 71);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(28, 13);
            this.label60.TabIndex = 4;
            this.label60.Text = "ЛС3";
            // 
            // _ls2
            // 
            this._ls2.Location = new System.Drawing.Point(6, 52);
            this._ls2.Name = "_ls2";
            this._ls2.Size = new System.Drawing.Size(13, 13);
            this._ls2.State = BEMN.Forms.LedState.Off;
            this._ls2.TabIndex = 3;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(25, 52);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(28, 13);
            this.label61.TabIndex = 2;
            this.label61.Text = "ЛС2";
            // 
            // _ls1
            // 
            this._ls1.Location = new System.Drawing.Point(6, 33);
            this._ls1.Name = "_ls1";
            this._ls1.Size = new System.Drawing.Size(13, 13);
            this._ls1.State = BEMN.Forms.LedState.Off;
            this._ls1.TabIndex = 1;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(25, 33);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(28, 13);
            this.label62.TabIndex = 0;
            this.label62.Text = "ЛС1";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this._k2);
            this.groupBox6.Controls.Add(this.label95);
            this.groupBox6.Controls.Add(this._k1);
            this.groupBox6.Controls.Add(this.label97);
            this.groupBox6.Controls.Add(this._d72);
            this.groupBox6.Controls.Add(this.label9);
            this.groupBox6.Controls.Add(this._d71);
            this.groupBox6.Controls.Add(this.label10);
            this.groupBox6.Controls.Add(this._d70);
            this.groupBox6.Controls.Add(this.label11);
            this.groupBox6.Controls.Add(this._d69);
            this.groupBox6.Controls.Add(this.label13);
            this.groupBox6.Controls.Add(this._d68);
            this.groupBox6.Controls.Add(this.label14);
            this.groupBox6.Controls.Add(this._d67);
            this.groupBox6.Controls.Add(this.label15);
            this.groupBox6.Controls.Add(this._d66);
            this.groupBox6.Controls.Add(this.label16);
            this.groupBox6.Controls.Add(this._d65);
            this.groupBox6.Controls.Add(this.label17);
            this.groupBox6.Controls.Add(this._d64);
            this.groupBox6.Controls.Add(this.label19);
            this.groupBox6.Controls.Add(this._d63);
            this.groupBox6.Controls.Add(this.label80);
            this.groupBox6.Controls.Add(this._d62);
            this.groupBox6.Controls.Add(this.label81);
            this.groupBox6.Controls.Add(this._d61);
            this.groupBox6.Controls.Add(this.label82);
            this.groupBox6.Controls.Add(this._d60);
            this.groupBox6.Controls.Add(this.label83);
            this.groupBox6.Controls.Add(this._d59);
            this.groupBox6.Controls.Add(this.label84);
            this.groupBox6.Controls.Add(this._d58);
            this.groupBox6.Controls.Add(this.label85);
            this.groupBox6.Controls.Add(this._d57);
            this.groupBox6.Controls.Add(this.label86);
            this.groupBox6.Controls.Add(this._d56);
            this.groupBox6.Controls.Add(this.label87);
            this.groupBox6.Controls.Add(this._d55);
            this.groupBox6.Controls.Add(this.label88);
            this.groupBox6.Controls.Add(this._d54);
            this.groupBox6.Controls.Add(this.label89);
            this.groupBox6.Controls.Add(this._d53);
            this.groupBox6.Controls.Add(this.label90);
            this.groupBox6.Controls.Add(this._d52);
            this.groupBox6.Controls.Add(this.label91);
            this.groupBox6.Controls.Add(this._d51);
            this.groupBox6.Controls.Add(this.label92);
            this.groupBox6.Controls.Add(this._d50);
            this.groupBox6.Controls.Add(this.label93);
            this.groupBox6.Controls.Add(this._d49);
            this.groupBox6.Controls.Add(this.label94);
            this.groupBox6.Controls.Add(this._d48);
            this.groupBox6.Controls.Add(this.label1);
            this.groupBox6.Controls.Add(this._d47);
            this.groupBox6.Controls.Add(this.label2);
            this.groupBox6.Controls.Add(this._d46);
            this.groupBox6.Controls.Add(this.label3);
            this.groupBox6.Controls.Add(this._d45);
            this.groupBox6.Controls.Add(this.label4);
            this.groupBox6.Controls.Add(this._d44);
            this.groupBox6.Controls.Add(this.label5);
            this.groupBox6.Controls.Add(this._d43);
            this.groupBox6.Controls.Add(this.label6);
            this.groupBox6.Controls.Add(this._d42);
            this.groupBox6.Controls.Add(this.label7);
            this.groupBox6.Controls.Add(this._d41);
            this.groupBox6.Controls.Add(this.label8);
            this.groupBox6.Controls.Add(this._d40);
            this.groupBox6.Controls.Add(this._d24);
            this.groupBox6.Controls.Add(this.label233);
            this.groupBox6.Controls.Add(this._d8);
            this.groupBox6.Controls.Add(this._d39);
            this.groupBox6.Controls.Add(this.label39);
            this.groupBox6.Controls.Add(this.label234);
            this.groupBox6.Controls.Add(this._d38);
            this.groupBox6.Controls.Add(this._d23);
            this.groupBox6.Controls.Add(this.label235);
            this.groupBox6.Controls.Add(this.label30);
            this.groupBox6.Controls.Add(this._d37);
            this.groupBox6.Controls.Add(this.label40);
            this.groupBox6.Controls.Add(this.label236);
            this.groupBox6.Controls.Add(this._d22);
            this.groupBox6.Controls.Add(this._d36);
            this.groupBox6.Controls.Add(this._d7);
            this.groupBox6.Controls.Add(this.label271);
            this.groupBox6.Controls.Add(this.label41);
            this.groupBox6.Controls.Add(this._d35);
            this.groupBox6.Controls.Add(this.label29);
            this.groupBox6.Controls.Add(this.label272);
            this.groupBox6.Controls.Add(this._d21);
            this.groupBox6.Controls.Add(this._d34);
            this.groupBox6.Controls.Add(this._d1);
            this.groupBox6.Controls.Add(this.label273);
            this.groupBox6.Controls.Add(this.label42);
            this.groupBox6.Controls.Add(this._d33);
            this.groupBox6.Controls.Add(this.label274);
            this.groupBox6.Controls.Add(this._d6);
            this.groupBox6.Controls.Add(this._d20);
            this.groupBox6.Controls.Add(this._d32);
            this.groupBox6.Controls.Add(this.label23);
            this.groupBox6.Controls.Add(this.label275);
            this.groupBox6.Controls.Add(this.label43);
            this.groupBox6.Controls.Add(this._d31);
            this.groupBox6.Controls.Add(this.label28);
            this.groupBox6.Controls.Add(this.label276);
            this.groupBox6.Controls.Add(this._d19);
            this.groupBox6.Controls.Add(this._d30);
            this.groupBox6.Controls.Add(this.label24);
            this.groupBox6.Controls.Add(this.label277);
            this.groupBox6.Controls.Add(this.label44);
            this.groupBox6.Controls.Add(this._d29);
            this.groupBox6.Controls.Add(this._d5);
            this.groupBox6.Controls.Add(this.label278);
            this.groupBox6.Controls.Add(this._d18);
            this.groupBox6.Controls.Add(this._d28);
            this.groupBox6.Controls.Add(this._d2);
            this.groupBox6.Controls.Add(this.label279);
            this.groupBox6.Controls.Add(this.label45);
            this.groupBox6.Controls.Add(this._d27);
            this.groupBox6.Controls.Add(this.label27);
            this.groupBox6.Controls.Add(this.label280);
            this.groupBox6.Controls.Add(this._d17);
            this.groupBox6.Controls.Add(this._d26);
            this.groupBox6.Controls.Add(this.label46);
            this.groupBox6.Controls.Add(this.label281);
            this.groupBox6.Controls.Add(this.label25);
            this.groupBox6.Controls.Add(this._d25);
            this.groupBox6.Controls.Add(this.label282);
            this.groupBox6.Controls.Add(this._d4);
            this.groupBox6.Controls.Add(this._d16);
            this.groupBox6.Controls.Add(this._d3);
            this.groupBox6.Controls.Add(this.label31);
            this.groupBox6.Controls.Add(this.label26);
            this.groupBox6.Controls.Add(this._d15);
            this.groupBox6.Controls.Add(this._d9);
            this.groupBox6.Controls.Add(this.label32);
            this.groupBox6.Controls.Add(this.label38);
            this.groupBox6.Controls.Add(this._d14);
            this.groupBox6.Controls.Add(this.label37);
            this.groupBox6.Controls.Add(this.label33);
            this.groupBox6.Controls.Add(this._d10);
            this.groupBox6.Controls.Add(this._d13);
            this.groupBox6.Controls.Add(this.label36);
            this.groupBox6.Controls.Add(this.label34);
            this.groupBox6.Controls.Add(this._d11);
            this.groupBox6.Controls.Add(this._d12);
            this.groupBox6.Controls.Add(this.label35);
            this.groupBox6.Location = new System.Drawing.Point(390, 6);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(519, 170);
            this.groupBox6.TabIndex = 0;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Дискретные входы";
            // 
            // _k2
            // 
            this._k2.Location = new System.Drawing.Point(469, 38);
            this._k2.Name = "_k2";
            this._k2.Size = new System.Drawing.Size(13, 13);
            this._k2.State = BEMN.Forms.LedState.Off;
            this._k2.TabIndex = 99;
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(488, 38);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(20, 13);
            this.label95.TabIndex = 98;
            this.label95.Text = "K2";
            // 
            // _k1
            // 
            this._k1.Location = new System.Drawing.Point(469, 19);
            this._k1.Name = "_k1";
            this._k1.Size = new System.Drawing.Size(13, 13);
            this._k1.State = BEMN.Forms.LedState.Off;
            this._k1.TabIndex = 97;
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(488, 19);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(20, 13);
            this.label97.TabIndex = 96;
            this.label97.Text = "K1";
            // 
            // _d72
            // 
            this._d72.Location = new System.Drawing.Point(420, 152);
            this._d72.Name = "_d72";
            this._d72.Size = new System.Drawing.Size(13, 13);
            this._d72.State = BEMN.Forms.LedState.Off;
            this._d72.TabIndex = 95;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(439, 152);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(28, 13);
            this.label9.TabIndex = 94;
            this.label9.Text = "Д72";
            // 
            // _d71
            // 
            this._d71.Location = new System.Drawing.Point(420, 133);
            this._d71.Name = "_d71";
            this._d71.Size = new System.Drawing.Size(13, 13);
            this._d71.State = BEMN.Forms.LedState.Off;
            this._d71.TabIndex = 93;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(439, 133);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(28, 13);
            this.label10.TabIndex = 92;
            this.label10.Text = "Д71";
            // 
            // _d70
            // 
            this._d70.Location = new System.Drawing.Point(420, 114);
            this._d70.Name = "_d70";
            this._d70.Size = new System.Drawing.Size(13, 13);
            this._d70.State = BEMN.Forms.LedState.Off;
            this._d70.TabIndex = 91;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(439, 114);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(28, 13);
            this.label11.TabIndex = 90;
            this.label11.Text = "Д70";
            // 
            // _d69
            // 
            this._d69.Location = new System.Drawing.Point(420, 95);
            this._d69.Name = "_d69";
            this._d69.Size = new System.Drawing.Size(13, 13);
            this._d69.State = BEMN.Forms.LedState.Off;
            this._d69.TabIndex = 89;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(439, 95);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(28, 13);
            this.label13.TabIndex = 88;
            this.label13.Text = "Д69";
            // 
            // _d68
            // 
            this._d68.Location = new System.Drawing.Point(420, 76);
            this._d68.Name = "_d68";
            this._d68.Size = new System.Drawing.Size(13, 13);
            this._d68.State = BEMN.Forms.LedState.Off;
            this._d68.TabIndex = 87;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(439, 76);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(28, 13);
            this.label14.TabIndex = 86;
            this.label14.Text = "Д68";
            // 
            // _d67
            // 
            this._d67.Location = new System.Drawing.Point(420, 57);
            this._d67.Name = "_d67";
            this._d67.Size = new System.Drawing.Size(13, 13);
            this._d67.State = BEMN.Forms.LedState.Off;
            this._d67.TabIndex = 85;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(439, 57);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(28, 13);
            this.label15.TabIndex = 84;
            this.label15.Text = "Д67";
            // 
            // _d66
            // 
            this._d66.Location = new System.Drawing.Point(420, 38);
            this._d66.Name = "_d66";
            this._d66.Size = new System.Drawing.Size(13, 13);
            this._d66.State = BEMN.Forms.LedState.Off;
            this._d66.TabIndex = 83;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(439, 38);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(28, 13);
            this.label16.TabIndex = 82;
            this.label16.Text = "Д66";
            // 
            // _d65
            // 
            this._d65.Location = new System.Drawing.Point(420, 19);
            this._d65.Name = "_d65";
            this._d65.Size = new System.Drawing.Size(13, 13);
            this._d65.State = BEMN.Forms.LedState.Off;
            this._d65.TabIndex = 81;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(439, 19);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(28, 13);
            this.label17.TabIndex = 80;
            this.label17.Text = "Д65";
            // 
            // _d64
            // 
            this._d64.Location = new System.Drawing.Point(368, 152);
            this._d64.Name = "_d64";
            this._d64.Size = new System.Drawing.Size(13, 13);
            this._d64.State = BEMN.Forms.LedState.Off;
            this._d64.TabIndex = 79;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(387, 152);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(28, 13);
            this.label19.TabIndex = 78;
            this.label19.Text = "Д64";
            // 
            // _d63
            // 
            this._d63.Location = new System.Drawing.Point(368, 133);
            this._d63.Name = "_d63";
            this._d63.Size = new System.Drawing.Size(13, 13);
            this._d63.State = BEMN.Forms.LedState.Off;
            this._d63.TabIndex = 77;
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(387, 133);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(28, 13);
            this.label80.TabIndex = 76;
            this.label80.Text = "Д63";
            // 
            // _d62
            // 
            this._d62.Location = new System.Drawing.Point(368, 114);
            this._d62.Name = "_d62";
            this._d62.Size = new System.Drawing.Size(13, 13);
            this._d62.State = BEMN.Forms.LedState.Off;
            this._d62.TabIndex = 75;
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(387, 114);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(28, 13);
            this.label81.TabIndex = 74;
            this.label81.Text = "Д62";
            // 
            // _d61
            // 
            this._d61.Location = new System.Drawing.Point(368, 95);
            this._d61.Name = "_d61";
            this._d61.Size = new System.Drawing.Size(13, 13);
            this._d61.State = BEMN.Forms.LedState.Off;
            this._d61.TabIndex = 73;
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(387, 95);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(28, 13);
            this.label82.TabIndex = 72;
            this.label82.Text = "Д61";
            // 
            // _d60
            // 
            this._d60.Location = new System.Drawing.Point(368, 76);
            this._d60.Name = "_d60";
            this._d60.Size = new System.Drawing.Size(13, 13);
            this._d60.State = BEMN.Forms.LedState.Off;
            this._d60.TabIndex = 71;
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(387, 76);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(28, 13);
            this.label83.TabIndex = 70;
            this.label83.Text = "Д60";
            // 
            // _d59
            // 
            this._d59.Location = new System.Drawing.Point(368, 57);
            this._d59.Name = "_d59";
            this._d59.Size = new System.Drawing.Size(13, 13);
            this._d59.State = BEMN.Forms.LedState.Off;
            this._d59.TabIndex = 69;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(387, 57);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(28, 13);
            this.label84.TabIndex = 68;
            this.label84.Text = "Д59";
            // 
            // _d58
            // 
            this._d58.Location = new System.Drawing.Point(368, 38);
            this._d58.Name = "_d58";
            this._d58.Size = new System.Drawing.Size(13, 13);
            this._d58.State = BEMN.Forms.LedState.Off;
            this._d58.TabIndex = 67;
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(387, 38);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(28, 13);
            this.label85.TabIndex = 66;
            this.label85.Text = "Д58";
            // 
            // _d57
            // 
            this._d57.Location = new System.Drawing.Point(368, 19);
            this._d57.Name = "_d57";
            this._d57.Size = new System.Drawing.Size(13, 13);
            this._d57.State = BEMN.Forms.LedState.Off;
            this._d57.TabIndex = 65;
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(387, 19);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(28, 13);
            this.label86.TabIndex = 64;
            this.label86.Text = "Д57";
            // 
            // _d56
            // 
            this._d56.Location = new System.Drawing.Point(315, 152);
            this._d56.Name = "_d56";
            this._d56.Size = new System.Drawing.Size(13, 13);
            this._d56.State = BEMN.Forms.LedState.Off;
            this._d56.TabIndex = 63;
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(334, 152);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(28, 13);
            this.label87.TabIndex = 62;
            this.label87.Text = "Д56";
            // 
            // _d55
            // 
            this._d55.Location = new System.Drawing.Point(315, 133);
            this._d55.Name = "_d55";
            this._d55.Size = new System.Drawing.Size(13, 13);
            this._d55.State = BEMN.Forms.LedState.Off;
            this._d55.TabIndex = 61;
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(334, 133);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(28, 13);
            this.label88.TabIndex = 60;
            this.label88.Text = "Д55";
            // 
            // _d54
            // 
            this._d54.Location = new System.Drawing.Point(315, 114);
            this._d54.Name = "_d54";
            this._d54.Size = new System.Drawing.Size(13, 13);
            this._d54.State = BEMN.Forms.LedState.Off;
            this._d54.TabIndex = 59;
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(334, 114);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(28, 13);
            this.label89.TabIndex = 58;
            this.label89.Text = "Д54";
            // 
            // _d53
            // 
            this._d53.Location = new System.Drawing.Point(315, 95);
            this._d53.Name = "_d53";
            this._d53.Size = new System.Drawing.Size(13, 13);
            this._d53.State = BEMN.Forms.LedState.Off;
            this._d53.TabIndex = 57;
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Location = new System.Drawing.Point(334, 95);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(28, 13);
            this.label90.TabIndex = 56;
            this.label90.Text = "Д53";
            // 
            // _d52
            // 
            this._d52.Location = new System.Drawing.Point(315, 76);
            this._d52.Name = "_d52";
            this._d52.Size = new System.Drawing.Size(13, 13);
            this._d52.State = BEMN.Forms.LedState.Off;
            this._d52.TabIndex = 55;
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Location = new System.Drawing.Point(334, 76);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(28, 13);
            this.label91.TabIndex = 54;
            this.label91.Text = "Д52";
            // 
            // _d51
            // 
            this._d51.Location = new System.Drawing.Point(315, 57);
            this._d51.Name = "_d51";
            this._d51.Size = new System.Drawing.Size(13, 13);
            this._d51.State = BEMN.Forms.LedState.Off;
            this._d51.TabIndex = 53;
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Location = new System.Drawing.Point(334, 57);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(28, 13);
            this.label92.TabIndex = 52;
            this.label92.Text = "Д51";
            // 
            // _d50
            // 
            this._d50.Location = new System.Drawing.Point(315, 38);
            this._d50.Name = "_d50";
            this._d50.Size = new System.Drawing.Size(13, 13);
            this._d50.State = BEMN.Forms.LedState.Off;
            this._d50.TabIndex = 51;
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Location = new System.Drawing.Point(334, 38);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(28, 13);
            this.label93.TabIndex = 50;
            this.label93.Text = "Д50";
            // 
            // _d49
            // 
            this._d49.Location = new System.Drawing.Point(315, 19);
            this._d49.Name = "_d49";
            this._d49.Size = new System.Drawing.Size(13, 13);
            this._d49.State = BEMN.Forms.LedState.Off;
            this._d49.TabIndex = 49;
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Location = new System.Drawing.Point(334, 19);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(28, 13);
            this.label94.TabIndex = 48;
            this.label94.Text = "Д49";
            // 
            // _d48
            // 
            this._d48.Location = new System.Drawing.Point(264, 152);
            this._d48.Name = "_d48";
            this._d48.Size = new System.Drawing.Size(13, 13);
            this._d48.State = BEMN.Forms.LedState.Off;
            this._d48.TabIndex = 47;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(283, 152);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 13);
            this.label1.TabIndex = 46;
            this.label1.Text = "Д48";
            // 
            // _d47
            // 
            this._d47.Location = new System.Drawing.Point(264, 133);
            this._d47.Name = "_d47";
            this._d47.Size = new System.Drawing.Size(13, 13);
            this._d47.State = BEMN.Forms.LedState.Off;
            this._d47.TabIndex = 45;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(283, 133);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 13);
            this.label2.TabIndex = 44;
            this.label2.Text = "Д47";
            // 
            // _d46
            // 
            this._d46.Location = new System.Drawing.Point(264, 114);
            this._d46.Name = "_d46";
            this._d46.Size = new System.Drawing.Size(13, 13);
            this._d46.State = BEMN.Forms.LedState.Off;
            this._d46.TabIndex = 43;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(283, 114);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 13);
            this.label3.TabIndex = 42;
            this.label3.Text = "Д46";
            // 
            // _d45
            // 
            this._d45.Location = new System.Drawing.Point(264, 95);
            this._d45.Name = "_d45";
            this._d45.Size = new System.Drawing.Size(13, 13);
            this._d45.State = BEMN.Forms.LedState.Off;
            this._d45.TabIndex = 41;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(283, 95);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(28, 13);
            this.label4.TabIndex = 40;
            this.label4.Text = "Д45";
            // 
            // _d44
            // 
            this._d44.Location = new System.Drawing.Point(264, 76);
            this._d44.Name = "_d44";
            this._d44.Size = new System.Drawing.Size(13, 13);
            this._d44.State = BEMN.Forms.LedState.Off;
            this._d44.TabIndex = 39;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(283, 76);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(28, 13);
            this.label5.TabIndex = 38;
            this.label5.Text = "Д44";
            // 
            // _d43
            // 
            this._d43.Location = new System.Drawing.Point(264, 57);
            this._d43.Name = "_d43";
            this._d43.Size = new System.Drawing.Size(13, 13);
            this._d43.State = BEMN.Forms.LedState.Off;
            this._d43.TabIndex = 37;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(283, 57);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(28, 13);
            this.label6.TabIndex = 36;
            this.label6.Text = "Д43";
            // 
            // _d42
            // 
            this._d42.Location = new System.Drawing.Point(264, 38);
            this._d42.Name = "_d42";
            this._d42.Size = new System.Drawing.Size(13, 13);
            this._d42.State = BEMN.Forms.LedState.Off;
            this._d42.TabIndex = 35;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(283, 38);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(28, 13);
            this.label7.TabIndex = 34;
            this.label7.Text = "Д42";
            // 
            // _d41
            // 
            this._d41.Location = new System.Drawing.Point(264, 19);
            this._d41.Name = "_d41";
            this._d41.Size = new System.Drawing.Size(13, 13);
            this._d41.State = BEMN.Forms.LedState.Off;
            this._d41.TabIndex = 33;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(283, 19);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(28, 13);
            this.label8.TabIndex = 32;
            this.label8.Text = "Д41";
            // 
            // _d40
            // 
            this._d40.Location = new System.Drawing.Point(212, 152);
            this._d40.Name = "_d40";
            this._d40.Size = new System.Drawing.Size(13, 13);
            this._d40.State = BEMN.Forms.LedState.Off;
            this._d40.TabIndex = 31;
            // 
            // _d24
            // 
            this._d24.Location = new System.Drawing.Point(106, 152);
            this._d24.Name = "_d24";
            this._d24.Size = new System.Drawing.Size(13, 13);
            this._d24.State = BEMN.Forms.LedState.Off;
            this._d24.TabIndex = 31;
            // 
            // label233
            // 
            this.label233.AutoSize = true;
            this.label233.Location = new System.Drawing.Point(231, 152);
            this.label233.Name = "label233";
            this.label233.Size = new System.Drawing.Size(28, 13);
            this.label233.TabIndex = 30;
            this.label233.Text = "Д40";
            // 
            // _d8
            // 
            this._d8.Location = new System.Drawing.Point(6, 152);
            this._d8.Name = "_d8";
            this._d8.Size = new System.Drawing.Size(13, 13);
            this._d8.State = BEMN.Forms.LedState.Off;
            this._d8.TabIndex = 15;
            // 
            // _d39
            // 
            this._d39.Location = new System.Drawing.Point(212, 133);
            this._d39.Name = "_d39";
            this._d39.Size = new System.Drawing.Size(13, 13);
            this._d39.State = BEMN.Forms.LedState.Off;
            this._d39.TabIndex = 29;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(125, 152);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(28, 13);
            this.label39.TabIndex = 30;
            this.label39.Text = "Д24";
            // 
            // label234
            // 
            this.label234.AutoSize = true;
            this.label234.Location = new System.Drawing.Point(231, 133);
            this.label234.Name = "label234";
            this.label234.Size = new System.Drawing.Size(28, 13);
            this.label234.TabIndex = 28;
            this.label234.Text = "Д39";
            // 
            // _d38
            // 
            this._d38.Location = new System.Drawing.Point(212, 114);
            this._d38.Name = "_d38";
            this._d38.Size = new System.Drawing.Size(13, 13);
            this._d38.State = BEMN.Forms.LedState.Off;
            this._d38.TabIndex = 27;
            // 
            // _d23
            // 
            this._d23.Location = new System.Drawing.Point(106, 133);
            this._d23.Name = "_d23";
            this._d23.Size = new System.Drawing.Size(13, 13);
            this._d23.State = BEMN.Forms.LedState.Off;
            this._d23.TabIndex = 29;
            // 
            // label235
            // 
            this.label235.AutoSize = true;
            this.label235.Location = new System.Drawing.Point(231, 114);
            this.label235.Name = "label235";
            this.label235.Size = new System.Drawing.Size(28, 13);
            this.label235.TabIndex = 26;
            this.label235.Text = "Д38";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(25, 152);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(22, 13);
            this.label30.TabIndex = 14;
            this.label30.Text = "Д8";
            // 
            // _d37
            // 
            this._d37.Location = new System.Drawing.Point(212, 95);
            this._d37.Name = "_d37";
            this._d37.Size = new System.Drawing.Size(13, 13);
            this._d37.State = BEMN.Forms.LedState.Off;
            this._d37.TabIndex = 25;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(125, 133);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(28, 13);
            this.label40.TabIndex = 28;
            this.label40.Text = "Д23";
            // 
            // label236
            // 
            this.label236.AutoSize = true;
            this.label236.Location = new System.Drawing.Point(231, 95);
            this.label236.Name = "label236";
            this.label236.Size = new System.Drawing.Size(28, 13);
            this.label236.TabIndex = 24;
            this.label236.Text = "Д37";
            // 
            // _d22
            // 
            this._d22.Location = new System.Drawing.Point(106, 114);
            this._d22.Name = "_d22";
            this._d22.Size = new System.Drawing.Size(13, 13);
            this._d22.State = BEMN.Forms.LedState.Off;
            this._d22.TabIndex = 27;
            // 
            // _d36
            // 
            this._d36.Location = new System.Drawing.Point(212, 76);
            this._d36.Name = "_d36";
            this._d36.Size = new System.Drawing.Size(13, 13);
            this._d36.State = BEMN.Forms.LedState.Off;
            this._d36.TabIndex = 23;
            // 
            // _d7
            // 
            this._d7.Location = new System.Drawing.Point(6, 133);
            this._d7.Name = "_d7";
            this._d7.Size = new System.Drawing.Size(13, 13);
            this._d7.State = BEMN.Forms.LedState.Off;
            this._d7.TabIndex = 13;
            // 
            // label271
            // 
            this.label271.AutoSize = true;
            this.label271.Location = new System.Drawing.Point(231, 76);
            this.label271.Name = "label271";
            this.label271.Size = new System.Drawing.Size(28, 13);
            this.label271.TabIndex = 22;
            this.label271.Text = "Д36";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(125, 114);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(28, 13);
            this.label41.TabIndex = 26;
            this.label41.Text = "Д22";
            // 
            // _d35
            // 
            this._d35.Location = new System.Drawing.Point(212, 57);
            this._d35.Name = "_d35";
            this._d35.Size = new System.Drawing.Size(13, 13);
            this._d35.State = BEMN.Forms.LedState.Off;
            this._d35.TabIndex = 21;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(25, 133);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(22, 13);
            this.label29.TabIndex = 12;
            this.label29.Text = "Д7";
            // 
            // label272
            // 
            this.label272.AutoSize = true;
            this.label272.Location = new System.Drawing.Point(231, 57);
            this.label272.Name = "label272";
            this.label272.Size = new System.Drawing.Size(28, 13);
            this.label272.TabIndex = 20;
            this.label272.Text = "Д35";
            // 
            // _d21
            // 
            this._d21.Location = new System.Drawing.Point(106, 95);
            this._d21.Name = "_d21";
            this._d21.Size = new System.Drawing.Size(13, 13);
            this._d21.State = BEMN.Forms.LedState.Off;
            this._d21.TabIndex = 25;
            // 
            // _d34
            // 
            this._d34.Location = new System.Drawing.Point(212, 38);
            this._d34.Name = "_d34";
            this._d34.Size = new System.Drawing.Size(13, 13);
            this._d34.State = BEMN.Forms.LedState.Off;
            this._d34.TabIndex = 19;
            // 
            // _d1
            // 
            this._d1.Location = new System.Drawing.Point(6, 19);
            this._d1.Name = "_d1";
            this._d1.Size = new System.Drawing.Size(13, 13);
            this._d1.State = BEMN.Forms.LedState.Off;
            this._d1.TabIndex = 1;
            // 
            // label273
            // 
            this.label273.AutoSize = true;
            this.label273.Location = new System.Drawing.Point(231, 38);
            this.label273.Name = "label273";
            this.label273.Size = new System.Drawing.Size(28, 13);
            this.label273.TabIndex = 18;
            this.label273.Text = "Д34";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(125, 95);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(28, 13);
            this.label42.TabIndex = 24;
            this.label42.Text = "Д21";
            // 
            // _d33
            // 
            this._d33.Location = new System.Drawing.Point(212, 19);
            this._d33.Name = "_d33";
            this._d33.Size = new System.Drawing.Size(13, 13);
            this._d33.State = BEMN.Forms.LedState.Off;
            this._d33.TabIndex = 17;
            // 
            // label274
            // 
            this.label274.AutoSize = true;
            this.label274.Location = new System.Drawing.Point(231, 19);
            this.label274.Name = "label274";
            this.label274.Size = new System.Drawing.Size(28, 13);
            this.label274.TabIndex = 16;
            this.label274.Text = "Д33";
            // 
            // _d6
            // 
            this._d6.Location = new System.Drawing.Point(6, 114);
            this._d6.Name = "_d6";
            this._d6.Size = new System.Drawing.Size(13, 13);
            this._d6.State = BEMN.Forms.LedState.Off;
            this._d6.TabIndex = 11;
            // 
            // _d20
            // 
            this._d20.Location = new System.Drawing.Point(106, 76);
            this._d20.Name = "_d20";
            this._d20.Size = new System.Drawing.Size(13, 13);
            this._d20.State = BEMN.Forms.LedState.Off;
            this._d20.TabIndex = 23;
            // 
            // _d32
            // 
            this._d32.Location = new System.Drawing.Point(159, 152);
            this._d32.Name = "_d32";
            this._d32.Size = new System.Drawing.Size(13, 13);
            this._d32.State = BEMN.Forms.LedState.Off;
            this._d32.TabIndex = 15;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(25, 19);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(22, 13);
            this.label23.TabIndex = 0;
            this.label23.Text = "Д1";
            // 
            // label275
            // 
            this.label275.AutoSize = true;
            this.label275.Location = new System.Drawing.Point(178, 152);
            this.label275.Name = "label275";
            this.label275.Size = new System.Drawing.Size(28, 13);
            this.label275.TabIndex = 14;
            this.label275.Text = "Д32";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(125, 76);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(28, 13);
            this.label43.TabIndex = 22;
            this.label43.Text = "Д20";
            // 
            // _d31
            // 
            this._d31.Location = new System.Drawing.Point(159, 133);
            this._d31.Name = "_d31";
            this._d31.Size = new System.Drawing.Size(13, 13);
            this._d31.State = BEMN.Forms.LedState.Off;
            this._d31.TabIndex = 13;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(25, 114);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(22, 13);
            this.label28.TabIndex = 10;
            this.label28.Text = "Д6";
            // 
            // label276
            // 
            this.label276.AutoSize = true;
            this.label276.Location = new System.Drawing.Point(178, 133);
            this.label276.Name = "label276";
            this.label276.Size = new System.Drawing.Size(28, 13);
            this.label276.TabIndex = 12;
            this.label276.Text = "Д31";
            // 
            // _d19
            // 
            this._d19.Location = new System.Drawing.Point(106, 57);
            this._d19.Name = "_d19";
            this._d19.Size = new System.Drawing.Size(13, 13);
            this._d19.State = BEMN.Forms.LedState.Off;
            this._d19.TabIndex = 21;
            // 
            // _d30
            // 
            this._d30.Location = new System.Drawing.Point(159, 114);
            this._d30.Name = "_d30";
            this._d30.Size = new System.Drawing.Size(13, 13);
            this._d30.State = BEMN.Forms.LedState.Off;
            this._d30.TabIndex = 11;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(25, 38);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(22, 13);
            this.label24.TabIndex = 2;
            this.label24.Text = "Д2";
            // 
            // label277
            // 
            this.label277.AutoSize = true;
            this.label277.Location = new System.Drawing.Point(178, 114);
            this.label277.Name = "label277";
            this.label277.Size = new System.Drawing.Size(28, 13);
            this.label277.TabIndex = 10;
            this.label277.Text = "Д30";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(125, 57);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(28, 13);
            this.label44.TabIndex = 20;
            this.label44.Text = "Д19";
            // 
            // _d29
            // 
            this._d29.Location = new System.Drawing.Point(159, 95);
            this._d29.Name = "_d29";
            this._d29.Size = new System.Drawing.Size(13, 13);
            this._d29.State = BEMN.Forms.LedState.Off;
            this._d29.TabIndex = 9;
            // 
            // _d5
            // 
            this._d5.Location = new System.Drawing.Point(6, 95);
            this._d5.Name = "_d5";
            this._d5.Size = new System.Drawing.Size(13, 13);
            this._d5.State = BEMN.Forms.LedState.Off;
            this._d5.TabIndex = 9;
            // 
            // label278
            // 
            this.label278.AutoSize = true;
            this.label278.Location = new System.Drawing.Point(178, 95);
            this.label278.Name = "label278";
            this.label278.Size = new System.Drawing.Size(28, 13);
            this.label278.TabIndex = 8;
            this.label278.Text = "Д29";
            // 
            // _d18
            // 
            this._d18.Location = new System.Drawing.Point(106, 38);
            this._d18.Name = "_d18";
            this._d18.Size = new System.Drawing.Size(13, 13);
            this._d18.State = BEMN.Forms.LedState.Off;
            this._d18.TabIndex = 19;
            // 
            // _d28
            // 
            this._d28.Location = new System.Drawing.Point(159, 76);
            this._d28.Name = "_d28";
            this._d28.Size = new System.Drawing.Size(13, 13);
            this._d28.State = BEMN.Forms.LedState.Off;
            this._d28.TabIndex = 7;
            // 
            // _d2
            // 
            this._d2.Location = new System.Drawing.Point(6, 38);
            this._d2.Name = "_d2";
            this._d2.Size = new System.Drawing.Size(13, 13);
            this._d2.State = BEMN.Forms.LedState.Off;
            this._d2.TabIndex = 3;
            // 
            // label279
            // 
            this.label279.AutoSize = true;
            this.label279.Location = new System.Drawing.Point(178, 76);
            this.label279.Name = "label279";
            this.label279.Size = new System.Drawing.Size(28, 13);
            this.label279.TabIndex = 6;
            this.label279.Text = "Д28";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(125, 38);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(28, 13);
            this.label45.TabIndex = 18;
            this.label45.Text = "Д18";
            // 
            // _d27
            // 
            this._d27.Location = new System.Drawing.Point(159, 57);
            this._d27.Name = "_d27";
            this._d27.Size = new System.Drawing.Size(13, 13);
            this._d27.State = BEMN.Forms.LedState.Off;
            this._d27.TabIndex = 5;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(25, 95);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(22, 13);
            this.label27.TabIndex = 8;
            this.label27.Text = "Д5";
            // 
            // label280
            // 
            this.label280.AutoSize = true;
            this.label280.Location = new System.Drawing.Point(178, 57);
            this.label280.Name = "label280";
            this.label280.Size = new System.Drawing.Size(28, 13);
            this.label280.TabIndex = 4;
            this.label280.Text = "Д27";
            // 
            // _d17
            // 
            this._d17.Location = new System.Drawing.Point(106, 19);
            this._d17.Name = "_d17";
            this._d17.Size = new System.Drawing.Size(13, 13);
            this._d17.State = BEMN.Forms.LedState.Off;
            this._d17.TabIndex = 17;
            // 
            // _d26
            // 
            this._d26.Location = new System.Drawing.Point(159, 38);
            this._d26.Name = "_d26";
            this._d26.Size = new System.Drawing.Size(13, 13);
            this._d26.State = BEMN.Forms.LedState.Off;
            this._d26.TabIndex = 3;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(125, 19);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(28, 13);
            this.label46.TabIndex = 16;
            this.label46.Text = "Д17";
            // 
            // label281
            // 
            this.label281.AutoSize = true;
            this.label281.Location = new System.Drawing.Point(178, 38);
            this.label281.Name = "label281";
            this.label281.Size = new System.Drawing.Size(28, 13);
            this.label281.TabIndex = 2;
            this.label281.Text = "Д26";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(25, 57);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(22, 13);
            this.label25.TabIndex = 4;
            this.label25.Text = "Д3";
            // 
            // _d25
            // 
            this._d25.Location = new System.Drawing.Point(159, 19);
            this._d25.Name = "_d25";
            this._d25.Size = new System.Drawing.Size(13, 13);
            this._d25.State = BEMN.Forms.LedState.Off;
            this._d25.TabIndex = 1;
            // 
            // label282
            // 
            this.label282.AutoSize = true;
            this.label282.Location = new System.Drawing.Point(178, 19);
            this.label282.Name = "label282";
            this.label282.Size = new System.Drawing.Size(28, 13);
            this.label282.TabIndex = 0;
            this.label282.Text = "Д25";
            // 
            // _d4
            // 
            this._d4.Location = new System.Drawing.Point(6, 76);
            this._d4.Name = "_d4";
            this._d4.Size = new System.Drawing.Size(13, 13);
            this._d4.State = BEMN.Forms.LedState.Off;
            this._d4.TabIndex = 7;
            // 
            // _d16
            // 
            this._d16.Location = new System.Drawing.Point(53, 152);
            this._d16.Name = "_d16";
            this._d16.Size = new System.Drawing.Size(13, 13);
            this._d16.State = BEMN.Forms.LedState.Off;
            this._d16.TabIndex = 15;
            // 
            // _d3
            // 
            this._d3.Location = new System.Drawing.Point(6, 57);
            this._d3.Name = "_d3";
            this._d3.Size = new System.Drawing.Size(13, 13);
            this._d3.State = BEMN.Forms.LedState.Off;
            this._d3.TabIndex = 5;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(72, 152);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(28, 13);
            this.label31.TabIndex = 14;
            this.label31.Text = "Д16";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(25, 76);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(22, 13);
            this.label26.TabIndex = 6;
            this.label26.Text = "Д4";
            // 
            // _d15
            // 
            this._d15.Location = new System.Drawing.Point(53, 133);
            this._d15.Name = "_d15";
            this._d15.Size = new System.Drawing.Size(13, 13);
            this._d15.State = BEMN.Forms.LedState.Off;
            this._d15.TabIndex = 13;
            // 
            // _d9
            // 
            this._d9.Location = new System.Drawing.Point(53, 19);
            this._d9.Name = "_d9";
            this._d9.Size = new System.Drawing.Size(13, 13);
            this._d9.State = BEMN.Forms.LedState.Off;
            this._d9.TabIndex = 1;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(72, 133);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(28, 13);
            this.label32.TabIndex = 12;
            this.label32.Text = "Д15";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(72, 19);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(22, 13);
            this.label38.TabIndex = 0;
            this.label38.Text = "Д9";
            // 
            // _d14
            // 
            this._d14.Location = new System.Drawing.Point(53, 114);
            this._d14.Name = "_d14";
            this._d14.Size = new System.Drawing.Size(13, 13);
            this._d14.State = BEMN.Forms.LedState.Off;
            this._d14.TabIndex = 11;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(72, 38);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(28, 13);
            this.label37.TabIndex = 2;
            this.label37.Text = "Д10";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(72, 114);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(28, 13);
            this.label33.TabIndex = 10;
            this.label33.Text = "Д14";
            // 
            // _d10
            // 
            this._d10.Location = new System.Drawing.Point(53, 38);
            this._d10.Name = "_d10";
            this._d10.Size = new System.Drawing.Size(13, 13);
            this._d10.State = BEMN.Forms.LedState.Off;
            this._d10.TabIndex = 3;
            // 
            // _d13
            // 
            this._d13.Location = new System.Drawing.Point(53, 95);
            this._d13.Name = "_d13";
            this._d13.Size = new System.Drawing.Size(13, 13);
            this._d13.State = BEMN.Forms.LedState.Off;
            this._d13.TabIndex = 9;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(72, 57);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(28, 13);
            this.label36.TabIndex = 4;
            this.label36.Text = "Д11";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(72, 95);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(28, 13);
            this.label34.TabIndex = 8;
            this.label34.Text = "Д13";
            // 
            // _d11
            // 
            this._d11.Location = new System.Drawing.Point(53, 57);
            this._d11.Name = "_d11";
            this._d11.Size = new System.Drawing.Size(13, 13);
            this._d11.State = BEMN.Forms.LedState.Off;
            this._d11.TabIndex = 5;
            // 
            // _d12
            // 
            this._d12.Location = new System.Drawing.Point(53, 76);
            this._d12.Name = "_d12";
            this._d12.Size = new System.Drawing.Size(13, 13);
            this._d12.State = BEMN.Forms.LedState.Off;
            this._d12.TabIndex = 7;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(72, 76);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(28, 13);
            this.label35.TabIndex = 6;
            this.label35.Text = "Д12";
            // 
            // _dataBaseTabControl
            // 
            this._dataBaseTabControl.Controls.Add(this._discretTabPage1);
            this._dataBaseTabControl.Controls.Add(this._controlSignalsTabPage);
            this._dataBaseTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._dataBaseTabControl.Location = new System.Drawing.Point(0, 0);
            this._dataBaseTabControl.Name = "_dataBaseTabControl";
            this._dataBaseTabControl.SelectedIndex = 0;
            this._dataBaseTabControl.Size = new System.Drawing.Size(941, 625);
            this._dataBaseTabControl.TabIndex = 1;
            // 
            // MeasuringForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(941, 625);
            this.Controls.Add(this._dataBaseTabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MeasuringForm";
            this.Text = "Mr761OBRMeasuringForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MeasuringForm_FormClosing);
            this.Load += new System.EventHandler(this.MeasuringForm_Load);
            this._controlSignalsTabPage.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox28.ResumeLayout(false);
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.groupBox15.ResumeLayout(false);
            this.groupBox27.ResumeLayout(false);
            this.groupBox27.PerformLayout();
            this.groupBox40.ResumeLayout(false);
            this.groupBox40.PerformLayout();
            this._discretTabPage1.ResumeLayout(false);
            this.groupBox49.ResumeLayout(false);
            this.groupBox49.PerformLayout();
            this.groupBox38.ResumeLayout(false);
            this.groupBox38.PerformLayout();
            this.groupBox39.ResumeLayout(false);
            this.groupBox39.PerformLayout();
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.groupBox21.ResumeLayout(false);
            this.groupBox21.PerformLayout();
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this._dataBaseTabControl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage _controlSignalsTabPage;
        private System.Windows.Forms.GroupBox groupBox28;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.Label _currenGroupLabel;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.ComboBox _groupCombo;
        private System.Windows.Forms.Button _switchGroupButton;
        private System.Windows.Forms.GroupBox groupBox27;
        private System.Windows.Forms.GroupBox groupBox40;
        private Forms.LedControl _logicState;
        private System.Windows.Forms.Button stopLogic;
        private System.Windows.Forms.Button startLogic;
        private System.Windows.Forms.Label label314;
        private System.Windows.Forms.Button _breakerOffBut;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Button _breakerOnBut;
        private System.Windows.Forms.Label label101;
        private Forms.LedControl _switchOn;
        private Forms.LedControl _switchOff;
        private System.Windows.Forms.Button _startOscBtn;
        private Forms.LedControl _availabilityFaultSystemJournal;
        private Forms.LedControl _newRecordOscJournal;
        private Forms.LedControl _newRecordAlarmJournal;
        private Forms.LedControl _newRecordSystemJournal;
        private System.Windows.Forms.Button _resetAnButton;
        private System.Windows.Forms.Button _resetAvailabilityFaultSystemJournalButton;
        private System.Windows.Forms.Button _resetOscJournalButton;
        private System.Windows.Forms.Button _resetAlarmJournalButton;
        private System.Windows.Forms.Button _resetSystemJournalButton;
        private System.Windows.Forms.Label label227;
        private System.Windows.Forms.Label label225;
        private System.Windows.Forms.Label label226;
        private System.Windows.Forms.Label label231;
        private System.Windows.Forms.TabPage _discretTabPage1;
        private System.Windows.Forms.GroupBox groupBox14;
        private Forms.LedControl _fault;
        private System.Windows.Forms.Label label210;
        private Forms.LedControl _faultOff;
        private System.Windows.Forms.Label label21;
        private Forms.LedControl _acceleration;
        private System.Windows.Forms.Label _auto4Led;
        private Forms.LedControl _signaling;
        private System.Windows.Forms.Label _auto2Led;
        private System.Windows.Forms.GroupBox groupBox21;
        private Forms.LedControl _ssl32;
        private System.Windows.Forms.Label label237;
        private Forms.LedControl _ssl31;
        private System.Windows.Forms.Label label238;
        private Forms.LedControl _ssl30;
        private System.Windows.Forms.Label label239;
        private Forms.LedControl _ssl29;
        private System.Windows.Forms.Label label240;
        private Forms.LedControl _ssl28;
        private System.Windows.Forms.Label label241;
        private Forms.LedControl _ssl27;
        private System.Windows.Forms.Label label242;
        private Forms.LedControl _ssl26;
        private System.Windows.Forms.Label label243;
        private Forms.LedControl _ssl25;
        private System.Windows.Forms.Label label244;
        private Forms.LedControl _ssl24;
        private System.Windows.Forms.Label label245;
        private Forms.LedControl _ssl23;
        private System.Windows.Forms.Label label246;
        private Forms.LedControl _ssl22;
        private System.Windows.Forms.Label label247;
        private Forms.LedControl _ssl21;
        private System.Windows.Forms.Label label248;
        private Forms.LedControl _ssl20;
        private System.Windows.Forms.Label label249;
        private Forms.LedControl _ssl19;
        private System.Windows.Forms.Label label250;
        private Forms.LedControl _ssl18;
        private System.Windows.Forms.Label label251;
        private Forms.LedControl _ssl17;
        private System.Windows.Forms.Label label252;
        private Forms.LedControl _ssl16;
        private System.Windows.Forms.Label label253;
        private Forms.LedControl _ssl15;
        private System.Windows.Forms.Label label254;
        private Forms.LedControl _ssl14;
        private System.Windows.Forms.Label label255;
        private Forms.LedControl _ssl13;
        private System.Windows.Forms.Label label256;
        private Forms.LedControl _ssl12;
        private System.Windows.Forms.Label label257;
        private Forms.LedControl _ssl11;
        private System.Windows.Forms.Label label258;
        private Forms.LedControl _ssl10;
        private System.Windows.Forms.Label label259;
        private Forms.LedControl _ssl9;
        private System.Windows.Forms.Label label260;
        private Forms.LedControl _ssl8;
        private System.Windows.Forms.Label label261;
        private Forms.LedControl _ssl7;
        private System.Windows.Forms.Label label262;
        private Forms.LedControl _ssl6;
        private System.Windows.Forms.Label label263;
        private Forms.LedControl _ssl5;
        private System.Windows.Forms.Label label264;
        private Forms.LedControl _ssl4;
        private System.Windows.Forms.Label label265;
        private Forms.LedControl _ssl3;
        private System.Windows.Forms.Label label266;
        private Forms.LedControl _ssl2;
        private System.Windows.Forms.Label label267;
        private Forms.LedControl _ssl1;
        private System.Windows.Forms.Label label268;
        private System.Windows.Forms.GroupBox groupBox18;
        private Forms.Diod diod12;
        private Forms.Diod diod11;
        private Forms.Diod diod10;
        private Forms.Diod diod9;
        private Forms.Diod diod8;
        private Forms.Diod diod7;
        private Forms.Diod diod6;
        private Forms.Diod diod5;
        private Forms.Diod diod4;
        private Forms.Diod diod3;
        private Forms.Diod diod2;
        private Forms.Diod diod1;
        private System.Windows.Forms.Label label190;
        private System.Windows.Forms.Label label189;
        private System.Windows.Forms.Label label179;
        private System.Windows.Forms.Label label180;
        private System.Windows.Forms.Label label181;
        private System.Windows.Forms.Label label182;
        private System.Windows.Forms.Label label183;
        private System.Windows.Forms.Label label184;
        private System.Windows.Forms.Label label185;
        private System.Windows.Forms.Label label186;
        private System.Windows.Forms.Label label187;
        private System.Windows.Forms.Label label188;
        private System.Windows.Forms.GroupBox groupBox17;
        private Forms.LedControl _module34;
        private System.Windows.Forms.Label label18;
        private Forms.LedControl _module33;
        private System.Windows.Forms.Label label20;
        private Forms.LedControl _module18;
        private Forms.LedControl _module10;
        private System.Windows.Forms.Label label161;
        private Forms.LedControl _module32;
        private Forms.LedControl _module17;
        private System.Windows.Forms.Label label138;
        private System.Windows.Forms.Label label162;
        private Forms.LedControl _module31;
        private System.Windows.Forms.Label label164;
        private System.Windows.Forms.Label label139;
        private Forms.LedControl _module9;
        private Forms.LedControl _module30;
        private Forms.LedControl _module16;
        private System.Windows.Forms.Label label140;
        private System.Windows.Forms.Label label165;
        private Forms.LedControl _module29;
        private System.Windows.Forms.Label label163;
        private System.Windows.Forms.Label label141;
        private Forms.LedControl _module15;
        private Forms.LedControl _module28;
        private Forms.LedControl _module5;
        private System.Windows.Forms.Label label142;
        private System.Windows.Forms.Label label169;
        private Forms.LedControl _module27;
        private Forms.LedControl _module8;
        private System.Windows.Forms.Label label232;
        private Forms.LedControl _module14;
        private Forms.LedControl _module26;
        private System.Windows.Forms.Label label176;
        private System.Windows.Forms.Label label127;
        private System.Windows.Forms.Label label170;
        private Forms.LedControl _module25;
        private System.Windows.Forms.Label label128;
        private System.Windows.Forms.Label label166;
        private Forms.LedControl _module13;
        private Forms.LedControl _module24;
        private Forms.LedControl _module1;
        private System.Windows.Forms.Label label129;
        private System.Windows.Forms.Label label171;
        private Forms.LedControl _module23;
        private Forms.LedControl _module7;
        private System.Windows.Forms.Label label130;
        private Forms.LedControl _module12;
        private Forms.LedControl _module22;
        private System.Windows.Forms.Label label175;
        private System.Windows.Forms.Label label131;
        private System.Windows.Forms.Label label177;
        private Forms.LedControl _module21;
        private System.Windows.Forms.Label label167;
        private System.Windows.Forms.Label label132;
        private Forms.LedControl _module11;
        private Forms.LedControl _module20;
        private System.Windows.Forms.Label label178;
        private System.Windows.Forms.Label label133;
        private Forms.LedControl _module2;
        private Forms.LedControl _module19;
        private System.Windows.Forms.Label label134;
        private Forms.LedControl _module6;
        private System.Windows.Forms.Label label168;
        private System.Windows.Forms.Label label174;
        private Forms.LedControl _module3;
        private System.Windows.Forms.Label label173;
        private System.Windows.Forms.Label label172;
        private Forms.LedControl _module4;
        private System.Windows.Forms.GroupBox groupBox10;
        private Forms.LedControl _vls16;
        private System.Windows.Forms.Label label63;
        private Forms.LedControl _vls15;
        private System.Windows.Forms.Label label64;
        private Forms.LedControl _vls14;
        private System.Windows.Forms.Label label65;
        private Forms.LedControl _vls13;
        private System.Windows.Forms.Label label66;
        private Forms.LedControl _vls12;
        private System.Windows.Forms.Label label67;
        private Forms.LedControl _vls11;
        private System.Windows.Forms.Label label68;
        private Forms.LedControl _vls10;
        private System.Windows.Forms.Label label69;
        private Forms.LedControl _vls9;
        private System.Windows.Forms.Label label70;
        private Forms.LedControl _vls8;
        private System.Windows.Forms.Label label71;
        private Forms.LedControl _vls7;
        private System.Windows.Forms.Label label72;
        private Forms.LedControl _vls6;
        private System.Windows.Forms.Label label73;
        private Forms.LedControl _vls5;
        private System.Windows.Forms.Label label74;
        private Forms.LedControl _vls4;
        private System.Windows.Forms.Label label75;
        private Forms.LedControl _vls3;
        private System.Windows.Forms.Label label76;
        private Forms.LedControl _vls2;
        private System.Windows.Forms.Label label77;
        private Forms.LedControl _vls1;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.GroupBox groupBox9;
        private Forms.LedControl _ls16;
        private System.Windows.Forms.Label label47;
        private Forms.LedControl _ls15;
        private System.Windows.Forms.Label label48;
        private Forms.LedControl _ls14;
        private System.Windows.Forms.Label label49;
        private Forms.LedControl _ls13;
        private System.Windows.Forms.Label label50;
        private Forms.LedControl _ls12;
        private System.Windows.Forms.Label label51;
        private Forms.LedControl _ls11;
        private System.Windows.Forms.Label label52;
        private Forms.LedControl _ls10;
        private System.Windows.Forms.Label label53;
        private Forms.LedControl _ls9;
        private System.Windows.Forms.Label label54;
        private Forms.LedControl _ls8;
        private System.Windows.Forms.Label label55;
        private Forms.LedControl _ls7;
        private System.Windows.Forms.Label label56;
        private Forms.LedControl _ls6;
        private System.Windows.Forms.Label label57;
        private Forms.LedControl _ls5;
        private System.Windows.Forms.Label label58;
        private Forms.LedControl _ls4;
        private System.Windows.Forms.Label label59;
        private Forms.LedControl _ls3;
        private System.Windows.Forms.Label label60;
        private Forms.LedControl _ls2;
        private System.Windows.Forms.Label label61;
        private Forms.LedControl _ls1;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.GroupBox groupBox6;
        private Forms.LedControl _d40;
        private Forms.LedControl _d24;
        private System.Windows.Forms.Label label233;
        private Forms.LedControl _d8;
        private Forms.LedControl _d39;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label234;
        private Forms.LedControl _d38;
        private Forms.LedControl _d23;
        private System.Windows.Forms.Label label235;
        private System.Windows.Forms.Label label30;
        private Forms.LedControl _d37;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label236;
        private Forms.LedControl _d22;
        private Forms.LedControl _d36;
        private Forms.LedControl _d7;
        private System.Windows.Forms.Label label271;
        private System.Windows.Forms.Label label41;
        private Forms.LedControl _d35;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label272;
        private Forms.LedControl _d21;
        private Forms.LedControl _d34;
        private Forms.LedControl _d1;
        private System.Windows.Forms.Label label273;
        private System.Windows.Forms.Label label42;
        private Forms.LedControl _d33;
        private System.Windows.Forms.Label label274;
        private Forms.LedControl _d6;
        private Forms.LedControl _d20;
        private Forms.LedControl _d32;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label275;
        private System.Windows.Forms.Label label43;
        private Forms.LedControl _d31;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label276;
        private Forms.LedControl _d19;
        private Forms.LedControl _d30;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label277;
        private System.Windows.Forms.Label label44;
        private Forms.LedControl _d29;
        private Forms.LedControl _d5;
        private System.Windows.Forms.Label label278;
        private Forms.LedControl _d18;
        private Forms.LedControl _d28;
        private Forms.LedControl _d2;
        private System.Windows.Forms.Label label279;
        private System.Windows.Forms.Label label45;
        private Forms.LedControl _d27;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label280;
        private Forms.LedControl _d17;
        private Forms.LedControl _d26;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label281;
        private System.Windows.Forms.Label label25;
        private Forms.LedControl _d25;
        private System.Windows.Forms.Label label282;
        private Forms.LedControl _d4;
        private Forms.LedControl _d16;
        private Forms.LedControl _d3;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label26;
        private Forms.LedControl _d15;
        private Forms.LedControl _d9;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label38;
        private Forms.LedControl _d14;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label33;
        private Forms.LedControl _d10;
        private Forms.LedControl _d13;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label34;
        private Forms.LedControl _d11;
        private Forms.LedControl _d12;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TabControl _dataBaseTabControl;
        private Forms.LedControl _d72;
        private System.Windows.Forms.Label label9;
        private Forms.LedControl _d71;
        private System.Windows.Forms.Label label10;
        private Forms.LedControl _d70;
        private System.Windows.Forms.Label label11;
        private Forms.LedControl _d69;
        private System.Windows.Forms.Label label13;
        private Forms.LedControl _d68;
        private System.Windows.Forms.Label label14;
        private Forms.LedControl _d67;
        private System.Windows.Forms.Label label15;
        private Forms.LedControl _d66;
        private System.Windows.Forms.Label label16;
        private Forms.LedControl _d65;
        private System.Windows.Forms.Label label17;
        private Forms.LedControl _d64;
        private System.Windows.Forms.Label label19;
        private Forms.LedControl _d63;
        private System.Windows.Forms.Label label80;
        private Forms.LedControl _d62;
        private System.Windows.Forms.Label label81;
        private Forms.LedControl _d61;
        private System.Windows.Forms.Label label82;
        private Forms.LedControl _d60;
        private System.Windows.Forms.Label label83;
        private Forms.LedControl _d59;
        private System.Windows.Forms.Label label84;
        private Forms.LedControl _d58;
        private System.Windows.Forms.Label label85;
        private Forms.LedControl _d57;
        private System.Windows.Forms.Label label86;
        private Forms.LedControl _d56;
        private System.Windows.Forms.Label label87;
        private Forms.LedControl _d55;
        private System.Windows.Forms.Label label88;
        private Forms.LedControl _d54;
        private System.Windows.Forms.Label label89;
        private Forms.LedControl _d53;
        private System.Windows.Forms.Label label90;
        private Forms.LedControl _d52;
        private System.Windows.Forms.Label label91;
        private Forms.LedControl _d51;
        private System.Windows.Forms.Label label92;
        private Forms.LedControl _d50;
        private System.Windows.Forms.Label label93;
        private Forms.LedControl _d49;
        private System.Windows.Forms.Label label94;
        private Forms.LedControl _d48;
        private System.Windows.Forms.Label label1;
        private Forms.LedControl _d47;
        private System.Windows.Forms.Label label2;
        private Forms.LedControl _d46;
        private System.Windows.Forms.Label label3;
        private Forms.LedControl _d45;
        private System.Windows.Forms.Label label4;
        private Forms.LedControl _d44;
        private System.Windows.Forms.Label label5;
        private Forms.LedControl _d43;
        private System.Windows.Forms.Label label6;
        private Forms.LedControl _d42;
        private System.Windows.Forms.Label label7;
        private Forms.LedControl _d41;
        private System.Windows.Forms.Label label8;
        private Forms.LedControl _k2;
        private System.Windows.Forms.Label label95;
        private Forms.LedControl _k1;
        private System.Windows.Forms.Label label97;
        private Forms.DateTimeControl _dateTimeControl;
        private System.Windows.Forms.GroupBox groupBox49;
        private System.Windows.Forms.Label label347;
        private Forms.LedControl _faultDisable2;
        private System.Windows.Forms.Label label346;
        private System.Windows.Forms.Label label345;
        private System.Windows.Forms.Label label311;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label22;
        private Forms.LedControl _faultDisable1;
        private Forms.LedControl _faultSwithON;
        private Forms.LedControl _faultManage;
        private Forms.LedControl _faultBlockCon;
        private Forms.LedControl _faultOut;
        private System.Windows.Forms.GroupBox groupBox38;
        private System.Windows.Forms.GroupBox groupBox39;
        private Forms.LedControl _fSpl1;
        private System.Windows.Forms.Label label315;
        private System.Windows.Forms.Label label316;
        private Forms.LedControl _fSpl2;
        private System.Windows.Forms.Label label317;
        private Forms.LedControl _fSpl3;
        private Forms.LedControl _fSpl5;
        private System.Windows.Forms.Label label320;
        private System.Windows.Forms.Label label319;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.Label label344;
        private Forms.LedControl _faultSwitchOff;
        private Forms.LedControl _faultAlarmJournal;
        private System.Windows.Forms.Label label192;
        private Forms.LedControl _faultOsc;
        private System.Windows.Forms.Label label193;
        private Forms.LedControl _faultModule5;
        private Forms.LedControl _faultSystemJournal;
        private System.Windows.Forms.Label label200;
        private System.Windows.Forms.Label label194;
        private Forms.LedControl _faultGroupsOfSetpoints;
        private System.Windows.Forms.Label label201;
        private Forms.LedControl _faultModule4;
        private Forms.LedControl _faultLogic;
        private System.Windows.Forms.Label label79;
        private Forms.LedControl _faultSetpoints;
        private System.Windows.Forms.Label label202;
        private System.Windows.Forms.Label label195;
        private Forms.LedControl _faultPass;
        private System.Windows.Forms.Label label203;
        private Forms.LedControl _faultModule3;
        private System.Windows.Forms.Label label196;
        private Forms.LedControl _faultSoftware;
        private System.Windows.Forms.Label label205;
        private Forms.LedControl _faultModule2;
        private Forms.LedControl _faultHardware;
        private System.Windows.Forms.Label label206;
        private System.Windows.Forms.Label label197;
        private Forms.LedControl _faultModule1;
        private System.Windows.Forms.Label label198;
        private System.Windows.Forms.GroupBox groupBox12;
        private Forms.LedControl _vz16;
        private System.Windows.Forms.Label label111;
        private Forms.LedControl _vz15;
        private System.Windows.Forms.Label label112;
        private Forms.LedControl _vz14;
        private System.Windows.Forms.Label label113;
        private Forms.LedControl _vz13;
        private System.Windows.Forms.Label label114;
        private Forms.LedControl _vz12;
        private System.Windows.Forms.Label label115;
        private Forms.LedControl _vz11;
        private System.Windows.Forms.Label label116;
        private Forms.LedControl _vz10;
        private System.Windows.Forms.Label label117;
        private Forms.LedControl _vz9;
        private System.Windows.Forms.Label label118;
        private Forms.LedControl _vz8;
        private System.Windows.Forms.Label label119;
        private Forms.LedControl _vz7;
        private System.Windows.Forms.Label label120;
        private Forms.LedControl _vz6;
        private System.Windows.Forms.Label label121;
        private Forms.LedControl _vz5;
        private System.Windows.Forms.Label label122;
        private Forms.LedControl _vz4;
        private System.Windows.Forms.Label label123;
        private Forms.LedControl _vz3;
        private System.Windows.Forms.Label label124;
        private Forms.LedControl _vz2;
        private System.Windows.Forms.Label label125;
        private Forms.LedControl _vz1;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button _commandBtn;
        private System.Windows.Forms.ComboBox _commandComboBox;
    }
}