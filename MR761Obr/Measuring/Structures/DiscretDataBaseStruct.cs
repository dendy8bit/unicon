﻿using System.Collections.Generic;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.MR761Obr.Measuring.Structures
{
    public class DiscretDataBaseStruct : StructBase
    {
        #region [Constants]
        private const int BASE_SIZE = 22;
        private const int ALARM_SIZE = 16;
        #endregion [Constants]


        #region [Private fields]
        [Layout(0, Count = BASE_SIZE)] private ushort[] _base;      //бд общаяя
        [Layout(1, Count = ALARM_SIZE)] private ushort[] _alarm;    //БД неисправностей
        #endregion [Private fields]

        
        #region [Properties]
        /// <summary>
        /// Дискретные входы
        /// </summary>
        public bool[] DiscretInputs
        {
            get
            {
                List<bool> retDiscr = new List<bool>();
                retDiscr.AddRange(Common.GetBitsArray(this._base, 0, 71));
                retDiscr.AddRange(Common.GetBitsArray(this._base, 174, 175));
                return retDiscr.ToArray();
            }
        }
        /// <summary>
        /// Входные ЛС
        /// </summary>
        public bool[] InputsLogicSignals
        {
            get
            {
                return Common.GetBitsArray(this._base, 72, 87);
            }
        }
        /// <summary>
        /// Выходные ЛС
        /// </summary>
        public bool[] OutputLogicSignals
        {
            get { return Common.GetBitsArray(this._base, 88, 103); }
        }
        /// <summary>
        /// Свободная логика
        /// </summary>
        public bool[] FreeLogic
        {
            get { return Common.GetBitsArray(this._base, 104, 135); }
        }  
        /// <summary>
        /// Состояния 
        /// </summary>
        public bool[] StateAndApv
        {
            get { return Common.GetBitsArray(this._base, 168, 171); }
        }

        /// <summary>
        /// Повр. фаз и качание
        /// </summary>
        public bool[] PhaseAndSw
        {
            get { return Common.GetBitsArray(this._base, 216, 221); }
        }
        /// <summary>
        /// Внешние защиты
        /// </summary>
        public bool[] ExternalDefenses
        {
            get { return Common.GetBitsArray(this._base, 240, 255); }
        }
        /// <summary>
        /// Реле
        /// </summary>
        public bool[] Relays
        {
            get { return Common.GetBitsArray(this._base, 256, 289); }
        }
        /// <summary>
        /// Индикаторы
        /// </summary>
        public List<bool[]> Indicators
        {
            get
            {
                bool[] allIndArray = Common.GetBitsArray(this._base, 290, 313); // все подряд биты
                List<bool[]> retList = new List<bool[]>();
                for (int i = 0; i < allIndArray.Length; i=i+2)    // выделяем попарно состояния одного диода
                {                                                   
                    retList.Add(new[] {allIndArray[i], allIndArray[i + 1]});// (bool зеленый, bool красный)
                }
                return retList;
            }
        }
        /// <summary>
        /// Контроль
        /// </summary>
        public bool[] ControlSignals
        {
            get { return Common.GetBitsArray(this._base, 316, 324); }
        }
        /// <summary>
        /// Неисправности общие
        /// </summary>
        public bool[] Faults1
        {
            get { return Common.GetBitsArray(this._alarm, 0, 17); }
        }
        /// <summary>
        /// Неисправности выключателя
        /// </summary>
        public bool[] Faults2
        {
            get { return Common.GetBitsArray(this._alarm, 18, 25); }
        }
        
        List<bool> _logicList = new List<bool>();
        /// <summary>
        /// Неисправность логики
        /// </summary>
        public bool[] FaultLogicErr
        {
            get
            {
                this._logicList.Clear();
                this._logicList.AddRange(Common.GetBitsArray(this._alarm, 26, 30));
                this._logicList.RemoveAt(3);
                return this._logicList.ToArray();
            }
        }
        #endregion [Properties]
    }
}
