using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using BEMN.MBServer;
using BEMN.MR761Obr.Configuration.Structures.Oscope;
using BEMN.MR761Obr.Osc.Structures;

namespace BEMN.MR761Obr.Osc.HelpClasses
{
    /// <summary>
    /// ��������� �������� ������������
    /// </summary>
    public class CountingList
    {
        #region [Constants]
        /// <summary>
        /// ������ ������ �������(� ������)
        /// </summary>
        public const int COUNTING_SIZE = 16;
        /// <summary>
        /// ���-�� �������
        /// </summary>
        public const int DISCRETS_COUNT = 72;
        /// <summary>
        /// ���-�� �������
        /// </summary>
        public const int CHANNELS_COUNT = 56;

        #endregion [Constants]


        #region [Private fields]
        private readonly ushort[][] _countingArray;
        private ChannelWithBase[] _�hannelWithBase;
        private string _hdrString;
        private int _count;
        private int _alarm;
        private OscJournalStruct _oscJournalStruct;
        

        #endregion [Private fields]

        /// <summary>
        /// ������
        /// </summary>
        public ushort[][] Channels { get; set; }

        /// <summary>
        /// ��������
        /// </summary>
        public ushort[][] Discrets { get; set; }

        /// <summary>
        /// ����� ���������� ��������
        /// </summary>
        public int Count
        {
            get { return this._count; }
        }
        /// <summary>
        /// ������("���� �����������") � ��������
        /// </summary>
        public int Alarm
        {
            get { return this._alarm; }
        }

        public OscJournalStruct OscJournalStruct
        {
            get { return this._oscJournalStruct; }
        }

        public string HdrString
        {
            get { return this._hdrString; }
        }

        #region [Ctor's]
        public CountingList(ushort[] pageValue, OscJournalStruct oscJournalStruct, ChannelWithBase[] channelWithBase)
        {
            this._�hannelWithBase = channelWithBase;
            this._oscJournalStruct = oscJournalStruct;

            this._alarm = this.OscJournalStruct.Len - this.OscJournalStruct.After;

            ushort groupIndex = oscJournalStruct.GroupIndex;
            this._countingArray = new ushort[COUNTING_SIZE][];
            //����� ���������� ��������
            this._count = pageValue.Length / COUNTING_SIZE;
            //������������� �������
            for (int i = 0; i < COUNTING_SIZE; i++)
            {
                this._countingArray[i] = new ushort[this.Count];
            }
            int m = 0;
            int n = 0;
            foreach (ushort value in pageValue)
            {
                this._countingArray[n][m] = value;
                n++;
                if (n != COUNTING_SIZE) continue;
                m++;
                n = 0;
            }
            //��������
            ushort[][] d41To56 = this.DiscretArrayInit(this._countingArray[7]);
            ushort[][] d57To72 = this.DiscretArrayInit(this._countingArray[8]);
            ushort[][] d8To24 = this.DiscretArrayInit(this._countingArray[9]);
            ushort[][] d25To40 = this.DiscretArrayInit(this._countingArray[10]);
            ushort[][] d1To8AndC1To8 = this.DiscretArrayInit(this._countingArray[11]);

            ushort[][] c9To24 = this.DiscretArrayInit(this._countingArray[12]);
            ushort[][] c25To40 = this.DiscretArrayInit(this._countingArray[13]);
            ushort[][] c41To56 = this.DiscretArrayInit(this._countingArray[14]);

            List<ushort[]> dicrets = new List<ushort[]>(DISCRETS_COUNT);
            dicrets.AddRange(d1To8AndC1To8.Take(8));
            dicrets.AddRange(d8To24);
            dicrets.AddRange(d25To40);
            dicrets.AddRange(d41To56);
            dicrets.AddRange(d57To72);
          
            this.Discrets = dicrets.ToArray();
            //������
            List<ushort[]> channels = new List<ushort[]>(CHANNELS_COUNT);
            channels.AddRange(d1To8AndC1To8.Skip(8));
            channels.AddRange(c9To24);
            channels.AddRange(c25To40);
            channels.AddRange(c41To56);
            this.Channels = channels.ToArray();
            this._hdrString = string.Format("�� 761 ��� {0} ������� - {1}, ��. ������� �{2}", 
                this.ConvertDateTime(oscJournalStruct.GetFormattedDateTime), oscJournalStruct.Stage, groupIndex + 1);
        }

        private string ConvertDateTime(string sourse)
        {
            string[] res = sourse.Split(new[] { '/', ',', ':', '.', ',' }, StringSplitOptions.RemoveEmptyEntries);
            return string.Format("{0}.{1}.{2} {3}:{4}:{5}.{6}", res[0], res[1], res[2], res[3], res[4],res[5], res[6]);
        }
        
        private CountingList(int count)
        {
            this.Discrets = new ushort[DISCRETS_COUNT][];
            this.Channels = new ushort[CHANNELS_COUNT][];
            this._count = count;
        }
        #endregion [Ctor's]

        /// <summary>
        /// ���������� ������ ����� � ��������������� ������ ���(�������� 0/1) 
        /// </summary>
        /// <param name="sourseArray">������ �������� ���</param>
        /// <returns></returns>
        private ushort[][] DiscretArrayInit(ushort[] sourseArray)
        {
            ushort[][] result = new ushort[16][];
            for (int i = 0; i < 16; i++)
            {
                result[i] = new ushort[sourseArray.Length];
            }

            for (int i = 0; i < sourseArray.Length; i++)
            {
                for (int j = 0; j < 16; j++)
                {
                    result[j][i] = (ushort)(Common.GetBit(sourseArray[i], j) ? 1 : 0);
                }
            }

            return result;
        }
        
        public void Save(string filePath)
        {
            string hdrPath = Path.ChangeExtension(filePath, "hdr");
            using (StreamWriter hdrFile = new StreamWriter(hdrPath))
            {
                hdrFile.WriteLine(this.HdrString);
                for (int i = 0; i < this._�hannelWithBase.Length; i++)
                {
                    hdrFile.WriteLine("K{0} = {1}, {2}", i + 1, this._�hannelWithBase[i].Channel, this._�hannelWithBase[i].Base);
                }
                hdrFile.WriteLine(1251);
            }
            string cgfPath = Path.ChangeExtension(filePath, "cfg");
            using (StreamWriter cgfFile = new StreamWriter(cgfPath, false, Encoding.GetEncoding(1251)))
            {
                cgfFile.WriteLine("MP761���,1");
                cgfFile.WriteLine("128,0A,128D");
                int index = 1;
                
                for (int i = 0; i < this.Discrets.Length; i++)
                {
                    cgfFile.WriteLine("{0},D{1},0", index, i + 1);
                    index++;
                }
                for (int i = 0; i < this.Channels.Length; i++)
                {
                    try
                    {
                        cgfFile.WriteLine("{0},K{1} ({2}),0", index, i + 1,  this._�hannelWithBase[i].ChannelStr);
                        index++;
                    }
                    catch (Exception)
                    {
                        cgfFile.WriteLine("{0},K{1} ({2}),0", index, i + 1, "Error");
                        index++;
                    }
                }

                cgfFile.WriteLine("50");
                cgfFile.WriteLine("1");
                cgfFile.WriteLine("1000,{0}", this.OscJournalStruct.Len);

                cgfFile.WriteLine(this.OscJournalStruct.GetFormattedDateTime);
                cgfFile.WriteLine(this.OscJournalStruct.GetFormattedDateTimeAlarm(this.Alarm));
                cgfFile.WriteLine("ASCII");
            }

            string datPath = Path.ChangeExtension(filePath, "dat");
            using (StreamWriter datFile = new StreamWriter(datPath))
            {
                for (int i = 0; i < this._count; i++)
                {
                    
                    string writeStr = string.Format("{0:D6},{1:D6},", i, i * 1000);
                    writeStr = this.Discrets.Aggregate(writeStr, (current, d) => current + string.Format("{0},", d[i]));
                    for (int j = 0; j < this.Channels.Length;j++)
                    {
                        writeStr += string.Format(j < this.Channels.Length - 1 ? "{0}," : "{0}", this.Channels[j][i]);
                    }
                    datFile.WriteLine(writeStr);
                }
            }
        }

        public  bool IsLoad { get; private set; }
        public string FilePath { get; private set; }
        public static CountingList Load(string filePath)
        {
            string hdrPath = Path.ChangeExtension(filePath, "hdr");
            string[] hdrStrings = File.ReadAllLines(hdrPath);
            string hdrString = hdrStrings[0];

            ChannelWithBase[] channelWithBase = new ChannelWithBase[CHANNELS_COUNT];
            for (int i = 0; i < CHANNELS_COUNT; i++)
            {
                string[] channelAndBase =
                    hdrStrings[1 + i].Split(new[] {'=', ' '}, StringSplitOptions.RemoveEmptyEntries)[1]
                        .Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries);
                if (channelAndBase.Length > 1)
                {
                    channelWithBase[i] = new ChannelWithBase
                    {
                        Channel = Convert.ToUInt16(channelAndBase[0]),
                        Base = Convert.ToByte(channelAndBase[1])
                    };
                }
                else
                {
                    channelWithBase[i] = new ChannelWithBase
                    {
                        Channel = Convert.ToUInt16(channelAndBase[0]),
                        Base = 0
                    };
                }
            }

            string cgfPath = Path.ChangeExtension(filePath, "cfg");
            string[] cfgStrings = File.ReadAllLines(cgfPath);
            
            int counts = int.Parse(cfgStrings[132].Replace("1000,", string.Empty));
            int alarm = 0;
            try
            {
                string startTime = cfgStrings[133].Split(',')[1];
                string runTime = cfgStrings[134].Split(',')[1];

                TimeSpan a = DateTime.Parse(runTime) - DateTime.Parse(startTime);
                alarm = (int)a.TotalMilliseconds;
            }
            catch (Exception)
            {
                // ignored
            }
            CountingList result = new CountingList(counts) { _alarm = alarm };
            string datPath = Path.ChangeExtension(filePath, "dat");
            string[] datStrings = File.ReadAllLines(datPath);

            string dataPattern =
                @"^\d+\,\d+,(?<D1>\d+),(?<D2>\d+),(?<D3>\d+),(?<D4>\d+),(?<D5>\d+),(?<D6>\d+),(?<D7>\d+),(?<D8>\d+),(?<D9>\d+),"+
              @"(?<D10>\d+),(?<D11>\d+),(?<D12>\d+),(?<D13>\d+),(?<D14>\d+),(?<D15>\d+),(?<D16>\d+),(?<D17>\d+),(?<D18>\d+),(?<D19>\d+),"+
              @"(?<D20>\d+),(?<D21>\d+),(?<D22>\d+),(?<D23>\d+),(?<D24>\d+),(?<D25>\d+),(?<D26>\d+),(?<D27>\d+),(?<D28>\d+),(?<D29>\d+),"+
              @"(?<D30>\d+),(?<D31>\d+),(?<D32>\d+),(?<D33>\d+),(?<D34>\d+),(?<D35>\d+),(?<D36>\d+),(?<D37>\d+),(?<D38>\d+),(?<D39>\d+),"+
              @"(?<D40>\d+),(?<D41>\d+),(?<D42>\d+),(?<D43>\d+),(?<D44>\d+),(?<D45>\d+),(?<D46>\d+),(?<D47>\d+),(?<D48>\d+),(?<D49>\d+),"+
              @"(?<D50>\d+),(?<D51>\d+),(?<D52>\d+),(?<D53>\d+),(?<D54>\d+),(?<D55>\d+),(?<D56>\d+),(?<D57>\d+),(?<D58>\d+),(?<D59>\d+),"+
              @"(?<D60>\d+),(?<D61>\d+),(?<D62>\d+),(?<D63>\d+),(?<D64>\d+),(?<D65>\d+),(?<D66>\d+),(?<D67>\d+),(?<D68>\d+),(?<D69>\d+),"+
              @"(?<D70>\d+),(?<D71>\d+),(?<D72>\d+),(?<C1>\d+),(?<C2>\d+),(?<C3>\d+),(?<C4>\d+),(?<C5>\d+),(?<C6>\d+),(?<C7>\d+),(?<C8>\d+),(?<C9>\d+),"+
              @"(?<C10>\d+),(?<C11>\d+),(?<C12>\d+),(?<C13>\d+),(?<C14>\d+),(?<C15>\d+),(?<C16>\d+),(?<C17>\d+),(?<C18>\d+),(?<C19>\d+),"+
              @"(?<C20>\d+),(?<C21>\d+),(?<C22>\d+),(?<C23>\d+),(?<C24>\d+),(?<C25>\d+),(?<C26>\d+),(?<C27>\d+),(?<C28>\d+),(?<C29>\d+),"+
              @"(?<C30>\d+),(?<C31>\d+),(?<C32>\d+),(?<C33>\d+),(?<C34>\d+),(?<C35>\d+),(?<C36>\d+),(?<C37>\d+),(?<C38>\d+),(?<C39>\d+),"+
              @"(?<C40>\d+),(?<C41>\d+),(?<C42>\d+),(?<C43>\d+),(?<C44>\d+),(?<C45>\d+),(?<C46>\d+),(?<C47>\d+),(?<C48>\d+),(?<C49>\d+),"+
              @"(?<C50>\d+),(?<C51>\d+),(?<C52>\d+),(?<C53>\d+),(?<C54>\d+),(?<C55>\d+),(?<C56>\d+)";
            Regex dataRegex = new Regex(dataPattern);
            ushort[][] discrets = new ushort[DISCRETS_COUNT][];
            ushort[][] channels = new ushort[CHANNELS_COUNT][];
            
            for (int i = 0; i < discrets.Length; i++)
            {
                discrets[i] = new ushort[counts];
            }

            for (int i = 0; i < channels.Length; i++)
            {
                channels[i] = new ushort[counts];
            }

            for (int i = 0; i < datStrings.Length; i++)
            {
                for (int j = 0; j < DISCRETS_COUNT; j++)
                {
                    discrets[j][i] = ushort.Parse(dataRegex.Match(datStrings[i]).Groups["D" + (j + 1)].Value);
                }

                for (int j = 0; j < CHANNELS_COUNT; j++)
                {
                    channels[j][i] = ushort.Parse(dataRegex.Match(datStrings[i]).Groups["C" + (j + 1)].Value);
                }      
            }
            
            result.Channels = channels;
            result.Discrets = discrets;
            result._�hannelWithBase = channelWithBase;
            result._hdrString = hdrString;
            result.IsLoad = true;
            result.FilePath = filePath;
            return result;
        }
    }
}
