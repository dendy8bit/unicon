﻿using System;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.MemoryEntityClasses.FileOperations;
using BEMN.Devices.Structures;
using BEMN.Forms.Export;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR761Obr.Properties;
using BEMN.MR761Obr.SystemJournal.Structures;
using SchemeEditorSystem.ResourceLibs;

namespace BEMN.MR761Obr.SystemJournal
{
    public partial class SystemJournalForm : Form, IFormView
    {
        #region [Constants]
        private const string RECORDS_IN_JOURNAL = "Записей в журнале - {0}";
        private const string JOURNAL_SAVED = "Журнал сохранён";
        private const string READ_FAIL = "Невозможно прочитать журнал";
        private const string TABLE_NAME_SYS = "МР761_обр_журнал_системы";
        private const string NUMBER_SYS = "Номер";
        private const string TIME_SYS = "Время";
        private const string MESSAGE_SYS = "Сообщение";
        private const string SYSTEM_JOURNAL = "Журнал системы";
        private const string READ_SYSTEM_JOURNAL = "Идет чтение журнала системы";
        private const string READING_LIST_FILE = "Идет чтение файла списка подписей сигналов ЖС СПЛ";
        private const string FAIL_READ =
            "Невозможно прочитать список подписей сигналов ЖC СПЛ. Списки будут сформированы по умолчанию.";
        private const string LIST_FILE_NAME = "jlist.xml";

        #endregion [Constants]

        #region [Private fields]
        private readonly MemoryEntity<OneWordStruct> _setPageSystemJournal;
        private readonly MemoryEntity<SystemJournalStruct> _systemJournal;
        private DataTable _dataTable;
        private int _recordNumber;
        private int _failCounter;
        private Mr761ObrDevice _device;
        private FileDriver _fileDriver;
        #endregion [Private fields]

        public SystemJournalForm()
        {
            this.InitializeComponent();
        }

        public SystemJournalForm(Mr761ObrDevice device)
        {
            this.InitializeComponent();
            this._device = device; 

            this._systemJournal = device.SystemJournal;
            this._systemJournal.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, ReadRecord);
            this._systemJournal.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                if (this._failCounter < 80)
                {
                    this._failCounter++;
                }
                else
                {
                    this._systemJournal.RemoveStructQueries();
                    this._failCounter = 0;
                    this.ButtonsEnabled = true;
                }
            });

            this._setPageSystemJournal = device.RefreshSysJournal;
            this._setPageSystemJournal.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, ReadJournalRecord);
            this._setPageSystemJournal.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, FailReadJournal);

            this._fileDriver = new FileDriver(this._device, this);
        }

        #region [IFormView Members]
        public Type FormDevice
        {
            get { return typeof(Mr761ObrDevice); }
        }

        public bool Multishow { get; private set; }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public Type ClassType
        {
            get { return typeof(SystemJournalForm); }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.js; }
        }

        public string NodeName
        {
            get { return SYSTEM_JOURNAL; }
        }
        #endregion [IFormView Members]


        #region [Properties]
        private bool ButtonsEnabled
        {
            set
            {
                this._readJournalButton.Enabled = this._saveJournalButton.Enabled =
                    this._loadJournalButton.Enabled = this._exportButton.Enabled = value;
            }
        }

        /// <summary>
        /// Счётчик сообщений
        /// </summary>
        public int RecordNumber
        {
            get { return this._recordNumber; }
            set
            {
                this._recordNumber = value;
                this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._recordNumber);
                this.statusStrip1.Update();
            }
        }
        #endregion [Properties]


        #region [Help members]
        private void FailReadJournal()
        {
            this.RecordNumber = 0;
            this._statusLabel.Text = READ_FAIL;
            this.ButtonsEnabled = true;
        }

        private void ReadJournalRecord()
        {
            this._systemJournal.LoadStruct();
        }

        private void ReadRecord()
        {
            if (!this._systemJournal.Value.IsEmpty)
            {
                this.RecordNumber++;
                this._failCounter = 0;
                this._dataTable.Rows.Add(new object[]
                    {
                        this.RecordNumber.ToString(CultureInfo.InvariantCulture),
                        this._systemJournal.Value.GetRecordTime,
                        this._systemJournal.Value.GetRecordMessage
                    });

                this._systemJournalGrid.DataSource = this._dataTable;
                SavePageNumber();
            }
            else
            {
                this.ButtonsEnabled = true;
            }
        }

        private void SavePageNumber()
        {
            this._setPageSystemJournal.Value.Word = (ushort)this._recordNumber;
            this._setPageSystemJournal.SaveStruct(TimeSpan.FromMilliseconds(100));
        }

        private DataTable GetJournalDataTable()
        {
            var table = new DataTable(TABLE_NAME_SYS);
            table.Columns.Add(NUMBER_SYS);
            table.Columns.Add(TIME_SYS);
            table.Columns.Add(MESSAGE_SYS);
            return table;
        }

        private void SaveJournalToFile()
        {
            if (DialogResult.OK == this._saveJournalDialog.ShowDialog())
            {
                this._dataTable.WriteXml(this._saveJournalDialog.FileName);
                this._statusLabel.Text = JOURNAL_SAVED;
            }
        }

        private void LoadJournalFromFile()
        {
            if (DialogResult.OK == this._openJournalDialog.ShowDialog())
            {
                this._dataTable.Clear();
                this._dataTable.ReadXml(this._openJournalDialog.FileName);
            }
            this.RecordNumber = this._systemJournalGrid.Rows.Count;
        }
        #endregion [Help members]


        #region [Events Handlers]
        private void SystemJournalForm_Load(object sender, EventArgs e)
        {
            this._dataTable = this.GetJournalDataTable();
            this._systemJournalGrid.DataSource = this._dataTable;
            if (Device.AutoloadConfig && this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._fileDriver.ReadFile(this.OnListRead, LIST_FILE_NAME);
                this._dataTable.Clear();
                this._statusLabel.Text = READING_LIST_FILE;
                this.ButtonsEnabled = false;
            }
        }

        private void OnListRead(byte[] readBytes, string mes)
        {
            try
            {
                if (readBytes != null && readBytes.Length != 0 && mes == "Операция успешно выполнена")
                {
                    using (StreamReader streamReader = new StreamReader(new MemoryStream(readBytes),
                        Encoding.GetEncoding("windows-1251")))
                    {
                        using (XmlTextReader reader = new XmlTextReader(streamReader))
                        {
                            XmlRootAttribute root = new XmlRootAttribute("MR761OBR");//new XmlRootAttribute(DEVICE_NAME);
                            XmlSerializer serializer = new XmlSerializer(typeof(ListsOfJournals), root);
                            ListsOfJournals lists = (ListsOfJournals)serializer.Deserialize(reader);
                            this._systemJournal.Value.MessagesList = lists.SysJournal.MessagesList;
                        }
                    }
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception)
            {
                MessageBox.Show(FAIL_READ, "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this._systemJournal.Value.MessagesList = PropFormsStrings.GetNewJournalList();
                //todo доделать
                this._statusLabel.Text = READ_SYSTEM_JOURNAL;
            }
            this.StartRead();
        }

        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._recordNumber = 0;
            this._dataTable.Rows.Clear();
            this._statusLabel.Text = "Чтение журнала системы";

            this.SavePageNumber();
        }

        private void _readJournalButton_Click(object sender, EventArgs e)
        {
            if (Device.AutoloadConfig && this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._fileDriver.ReadFile(this.OnListRead, LIST_FILE_NAME);
                this._dataTable.Clear();
                this._statusLabel.Text = READING_LIST_FILE;
                this.ButtonsEnabled = false;
            }
        }

        private void _saveJournalButton_Click(object sender, EventArgs e)
        {
            this.SaveJournalToFile();
        }

        private void _loadJournalButton_Click(object sender, EventArgs e)
        {
            this.LoadJournalFromFile();
        }
        #endregion [Events Handlers]

        private void _exportButton_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == this._saveJournalHtmlDialog.ShowDialog())
            {
                HtmlExport.Export(this._dataTable, this._saveJournalHtmlDialog.FileName, Resources.MR731SJ);
                this._statusLabel.Text = JOURNAL_SAVED;
            }
        }
    }
}
