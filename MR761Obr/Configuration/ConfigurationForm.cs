﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms.Export;
using BEMN.Forms.ValidatingClasses.ControlsSupportClasses;
using BEMN.Forms.ValidatingClasses.New.ColumnsInfos;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Forms.ValidatingClasses.New.Validators.TurnOff;
using BEMN.Forms.ValidatingClasses.Rules;
using BEMN.Forms.ValidatingClasses.Rules.Ushort;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR761Obr.Configuration.Structures;
using BEMN.MR761Obr.Configuration.Structures.ConfigSystem;
using BEMN.MR761Obr.Configuration.Structures.External;
using BEMN.MR761Obr.Configuration.Structures.InputSignals;
using BEMN.MR761Obr.Configuration.Structures.Ls;
using BEMN.MR761Obr.Configuration.Structures.Oscope;
using BEMN.MR761Obr.Configuration.Structures.RelayInd;
using BEMN.MR761Obr.Configuration.Structures.Switch;
using BEMN.MR761Obr.Configuration.Structures.Vls;
using BEMN.MR761Obr.Properties;

namespace BEMN.MR761Obr.Configuration
{
    public partial class ConfigurationForm : Form, IFormView
    {
        #region [Constants]
        private const int OSC_SIZE = 54528 * 2;
        private const string READING_CONFIG = "Чтение конфигурации";
        private const string READ_OK = "Конфигурация прочитана";
        private const string READ_FAIL = "Конфигурация не может быть прочитана";
        private const string WRITING_CONFIG = "Идет запись конфигурации";
        private const string WRITE_OK = "Конфигурация записана в устройство";
        private const string WRITE_FAIL = "Конфигурация не может быть записана";
        private const string FILE_SAVE_FAIL = "Невозможно сохранить файл";
        private const string FILE_LOAD_FAIL = "Невозможно загрузить файл";
        private const string XML_HEAD = "MR761OBR_SET_POINTS";
        private const string INVALID_PORT = "Порт недоступен.";
        
        private double _version;
        #endregion [Constants]

        #region [Private fields]

        private readonly MemoryEntity<ConfigurationStruct> _configuration;
        /// <summary>
        /// Текущие уставки
        /// </summary>
        private ConfigurationStruct _currentSetpointsStruct;
        // Массивы контролов для ВЛС
        private CheckedListBox[] _vlsBoxesGr1;
        // Массив контролов для ЛС
        private DataGridView[] _lsBoxesGr1;
        private readonly Mr761ObrDevice _device;

        #endregion [Private fields]

        #region [Валидаторы]

        #region Уставки группы

        /// <summary>
        /// External
        /// </summary>
        private NewDgwValidatior<AllDefenseExternalStruct, DefenseExternalStruct> _externalValidatorGr1;
        // ЛС
        private NewDgwValidatior<InputLogicStruct>[] _inputLogicValidatorGr1;
        private StructUnion<AllInputLogicStruct> _inputLogicUnionGr1;
        // ВЛС
        private NewCheckedListBoxValidator<OutputLogicStruct>[] _vlsValidatorGr1;
        private StructUnion<AllOutputLogicSignalStruct> _vlsUnionGr1;
        
        private StructUnion<GroupSetpointStruct> _setpointUnionV1;
        private SetpointsValidator<AllGroupSetpointStruct, GroupSetpointStruct> _setpointsValidatorV1;
        #endregion Уставки

        // Валидатор выключателя
        private NewStructValidator<SwitchStruct> _switchValidator;
        // Валидатор входных сигналов
        private NewStructValidator<InputSignalStruct> _inputSignalsValidator;
        // Осциллограф
        private NewStructValidator<OscopeConfigStruct> _oscopeConfigValidator;
        private DgvValidatorWithDepend<OscopeAllChannelsStruct, ChannelWithBase> _channelsValidator;
        private StructUnion<OscopeStruct> _oscopeUnion;
        

        // Реле и индикаторы
        private NewDgwValidatior<AllReleOutputStruct, ReleOutputStruct> _releyValidator;
        private NewDgwValidatior<AllIndicatorsStruct, IndicatorsStruct> _indicatorValidator;
        private NewStructValidator<FaultStruct> _faultValidator;
        private StructUnion<AutomaticsParametersStruct> _automaticsParametersUnion;
        private NewStructValidator<ConfigAddStruct> _configAddValidator;
        
        private StructUnion<ConfigurationStruct> _configurationValidator;
        private NewStructValidator<ChannelStruct> _startOscChannelValidator;
        private NewStructValidator<ConfigIPAddress> _ethernetValidator;

        #endregion [Валидаторы]

        #region [Ctor's]

        public ConfigurationForm()
        {
            this.InitializeComponent();
        }

        public ConfigurationForm(Mr761ObrDevice device)
        {
            this.InitializeComponent();
            this._device = device;
            this._version = Common.VersionConverter(_device.DeviceVersion);
            this._configuration = device.Configuration;
            this._configuration.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationReadOk);
            this._configuration.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationWriteOk);
            this._configuration.ReadOk += HandlerHelper.CreateHandler(this, this._progressBar.PerformStep);
            this._configuration.WriteOk += HandlerHelper.CreateHandler(this, this._progressBar.PerformStep);
            this._configuration.ReadFail += HandlerHelper.CreateHandler(this, () =>
            {
                this._configuration.RemoveStructQueries();
                this.ConfigurationReadFail();
            });
            this._configuration.WriteFail += HandlerHelper.CreateHandler(this, () =>
            {
                this._configuration.RemoveStructQueries();
                this.ConfigurationWriteFail();
            });
            this._progressBar.Maximum = this._configuration.Slots.Count;
            this._currentSetpointsStruct = new ConfigurationStruct();
            this.Init();
        }

        private void Init()
        {
            this._copySetpoinsGroupComboBox.DataSource = StringsConfig.CopyGroupsNames;

            #region Уставки
            // Внешние защиты
            this._externalValidatorGr1 = new NewDgwValidatior<AllDefenseExternalStruct, DefenseExternalStruct>
                (
                this._externalDefensesGr1,
                16,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.ExternalStages, ColumnsType.NAME), //0
                new ColumnInfoCombo(StringsConfig.DefenseModes),
                new ColumnInfoCombo(StringsConfig.ExternalDafenseSrab),
                new ColumnInfoText(RulesContainer.TimeRule), //3
                new ColumnInfoText(RulesContainer.TimeRule), //4
                new ColumnInfoCombo(StringsConfig.ExternalDafenseSrab),//5
                new ColumnInfoCheck(), //6
                new ColumnInfoCombo(StringsConfig.ExternalDafenseSrab), //7
                new ColumnInfoCombo(StringsConfig.OscModes),//8
                new ColumnInfoCheck(),//9
                new ColumnInfoCheck()
                )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._externalDefensesGr1,
                        new TurnOffRule(1, StringsConfig.DefenseModes[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10)
                        )
                }
            };
            // ЛС
            this._lsBoxesGr1 = new DataGridView[]
            {
                this._lsAndDgv1Gr1, this._lsAndDgv2Gr1, this._lsAndDgv3Gr1, this._lsAndDgv4Gr1, this._lsAndDgv5Gr1, this._lsAndDgv6Gr1, this._lsAndDgv7Gr1,
                this._lsAndDgv8Gr1,
                this._lsOrDgv1Gr1, this._lsOrDgv2Gr1, this._lsOrDgv3Gr1, this._lsOrDgv4Gr1, this._lsOrDgv5Gr1, this._lsOrDgv6Gr1, this._lsOrDgv7Gr1,
                this._lsOrDgv8Gr1
            };

            this._inputLogicValidatorGr1 = new NewDgwValidatior<InputLogicStruct>[AllInputLogicStruct.LOGIC_COUNT];
            for (int i = 0; i < AllInputLogicStruct.LOGIC_COUNT; i++)
            {
                this._inputLogicValidatorGr1[i] = new NewDgwValidatior<InputLogicStruct>
                    (
                    this._lsBoxesGr1[i],
                    InputLogicStruct.DISCRETS_COUNT,
                    this._toolTip,
                    new ColumnInfoCombo(StringsConfig.LsSignals, ColumnsType.NAME),
                    new ColumnInfoCombo(StringsConfig.LsState)
                    );
            }
            this._inputLogicUnionGr1 = new StructUnion<AllInputLogicStruct>(this._inputLogicValidatorGr1);

            // ВЛС
            this._vlsBoxesGr1 = new CheckedListBox[]
                {
                    this.VLSclb1Gr1, this.VLSclb2Gr1, this.VLSclb3Gr1, this.VLSclb4Gr1, this.VLSclb5Gr1, this.VLSclb6Gr1, this.VLSclb7Gr1, this.VLSclb8Gr1,
                    this.VLSclb9Gr1, this.VLSclb10Gr1, this.VLSclb11Gr1, this.VLSclb12Gr1, this.VLSclb13Gr1, this.VLSclb14Gr1, this.VLSclb15Gr1,
                    this.VLSclb16Gr1
                };
            this._vlsValidatorGr1 = new NewCheckedListBoxValidator<OutputLogicStruct>[AllOutputLogicSignalStruct.LOGIC_COUNT];
            for (int i = 0; i < AllOutputLogicSignalStruct.LOGIC_COUNT; i++)
            {
                this._vlsValidatorGr1[i] = new NewCheckedListBoxValidator<OutputLogicStruct>(this._vlsBoxesGr1[i], StringsConfig.VlsSignals);
            }
            this._vlsUnionGr1 = new StructUnion<AllOutputLogicSignalStruct>(this._vlsValidatorGr1);
            
            this._setpointUnionV1 = new StructUnion<GroupSetpointStruct>
                (
                this._externalValidatorGr1,
                this._inputLogicUnionGr1,
                this._vlsUnionGr1
                );
            this._setpointsValidatorV1 = new SetpointsValidator<AllGroupSetpointStruct, GroupSetpointStruct>
                (new ComboboxSelector(this._setpointsComboBox, StringsConfig.GroupsNames), this._setpointUnionV1);
            
            #endregion Уставки

            // Выключатель
            this._switchValidator = new NewStructValidator<SwitchStruct>
                (
                this._toolTip,
                   new ControlInfoCombo(this._switchOff, StringsConfig.SwitchSignals),
                   new ControlInfoCombo(this._switchOn, StringsConfig.SwitchSignals),
                   new ControlInfoCombo(this._switchError, StringsConfig.SwitchSignals),
                   new ControlInfoCombo(this._switchBlock, StringsConfig.SwitchSignals),
                   new ControlInfoText(this._switchImp, RulesContainer.TimeRule),
                   new ControlInfoText(this._switchTUskor, RulesContainer.TimeRule),
                   new ControlInfoCombo(this._switchKontCep, StringsConfig.OffOn),
                   new ControlInfoCombo(this._controlSolenoidCombo, StringsConfig.SwitchSignals),
                   new ControlInfoCombo(this._switchKeyOn, StringsConfig.SwitchSignals),
                   new ControlInfoCombo(this._switchKeyOff, StringsConfig.SwitchSignals),
                   new ControlInfoCombo(this._switchVneshOn, StringsConfig.SwitchSignals),
                   new ControlInfoCombo(this._switchVneshOff, StringsConfig.SwitchSignals),
                   new ControlInfoCombo(this._switchButtons, StringsConfig.ForbiddenAllowed),
                   new ControlInfoCombo(this._switchKey, StringsConfig.ControlForbidden),
                   new ControlInfoCombo(this._switchVnesh, StringsConfig.ControlForbidden),
                   new ControlInfoCombo(this._switchSDTU, StringsConfig.ForbiddenAllowed),
                   new ControlInfoCombo(this._comandOtkl,StringsConfig.ImpDlit)
                   );  

            // Входные сигналы
            this._inputSignalsValidator = new NewStructValidator<InputSignalStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this._grUst1ComboBox, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._grUst2ComboBox, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._grUst3ComboBox, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._grUst4ComboBox, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._grUst5ComboBox, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._grUst6ComboBox, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._indComboBox, StringsConfig.SwitchSignals)
                );

            this._oscopeConfigValidator = new NewStructValidator<OscopeConfigStruct>
                     (
                     this._toolTip,
                     new ControlInfoText(this._oscWriteLength, RulesContainer.UshortTo100),
                     new ControlInfoCombo(this._oscFix, StringsConfig.OscFixation),
                     new ControlInfoCombo(this._oscLength, StringsConfig.OscCount)
                     );

            if (_version >= 1.01)
            {
                 this._ethernetValidator = new NewStructValidator<ConfigIPAddress>(
                    this._toolTip,
                    new ControlInfoText(this._ipLo1, new CustomUshortRule(0, 255)),
                    new ControlInfoText(this._ipLo2, new CustomUshortRule(0, 255)),
                    new ControlInfoText(this._ipHi1, new CustomUshortRule(0, 255)),
                    new ControlInfoText(this._ipHi2, new CustomUshortRule(0, 255)));
            }
           

            Func<string, List<string>> func = str =>
            {
                if(string.IsNullOrEmpty(str)) return new List<string>();
                int index = StringsConfig.OscBases.IndexOf(str);
                return index != -1 ? StringsConfig.OscChannelSignals[index] : new List<string>();  
            };

            this._channelsValidator = new DgvValidatorWithDepend<OscopeAllChannelsStruct, ChannelWithBase>
                (
                this._oscChannelsGrid,
                OscopeAllChannelsStruct.KANAL_COUNT,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.OscChannelNames, ColumnsType.NAME),
                new ColumnInfoComboControl(StringsConfig.OscBases, 2),
                new ColumnInfoComboDependent(func, 1)
                );

            this._startOscChannelValidator = new NewStructValidator<ChannelStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this.oscStartCb, StringsConfig.RelaySignals)
                );

            this._oscopeUnion = new StructUnion<OscopeStruct>(this._oscopeConfigValidator, this._channelsValidator, this._startOscChannelValidator);


            #region [Реле и Индикаторы]

            this._releyValidator = new NewDgwValidatior<AllReleOutputStruct, ReleOutputStruct>
                (
                this._outputReleGrid,
                AllReleOutputStruct.RELAY_COUNT,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.RelayNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.ReleyType),
                new ColumnInfoCombo(StringsConfig.RelaySignals),
                new ColumnInfoText(RulesContainer.TimeRule)
                );

            this._indicatorValidator = new NewDgwValidatior<AllIndicatorsStruct, IndicatorsStruct>
                (
                this._outputIndicatorsGrid,
                AllIndicatorsStruct.INDICATORS_COUNT,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.IndNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.ReleyType),
                new ColumnInfoCombo(StringsConfig.RelaySignals),
                new ColumnInfoCombo(StringsConfig.RelaySignals),
                new ColumnInfoCombo(StringsConfig.Mode)
                );

            this._faultValidator = new NewStructValidator<FaultStruct>
                (
                this._toolTip,
                new ControlInfoCheck(this._fault1CheckBox),
                new ControlInfoCheck(this._fault2CheckBox),
                new ControlInfoCheck(this._fault3CheckBox),
                new ControlInfoCheck(this._fault4CheckBox),
                new ControlInfoText(this._impTB, RulesContainer.TimeRule)
                );

            this._automaticsParametersUnion = new StructUnion<AutomaticsParametersStruct>
                (this._releyValidator, this._indicatorValidator, this._faultValidator);

            #endregion [Реле и Индикаторы]

            this._configAddValidator = new NewStructValidator<ConfigAddStruct>
                (this._toolTip,
                new ControlInfoCheck(this._resetSystemCheckBox),
                new ControlInfoCheck(this._resetAlarmCheckBox)
                );

            if (_version >= 1.01)
            {
                this._configurationValidator = new StructUnion<ConfigurationStruct>
                (
                    this._setpointsValidatorV1,
                    this._switchValidator,
                    this._inputSignalsValidator,
                    this._oscopeUnion,
                    this._automaticsParametersUnion,
                    this._ethernetValidator,
                    this._configAddValidator
                );
            }
            else
            {
                this._configurationTabControl.TabPages.Remove(this._ethernetPage);
                this._configurationValidator = new StructUnion<ConfigurationStruct>
                (
                    this._setpointsValidatorV1,
                    this._switchValidator,
                    this._inputSignalsValidator,
                    this._oscopeUnion,
                    this._automaticsParametersUnion,
                    this._configAddValidator
                );
            }
            
        }

        #endregion [Ctor's]
        
        #region [MemoryEntity Events Handlers]

        /// <summary>
        /// Конфигурация успешно прочитана
        /// </summary>
        private void ConfigurationReadOk()
        {
            this.IsProcess = false;
            this._progressBar.Value = this._progressBar.Maximum;
            this._statusLabel.Text = READ_OK;
            this._currentSetpointsStruct = this._configuration.Value;
            this.ShowConfiguration();
            MessageBox.Show(READ_OK);
        }

        /// <summary>
        /// Ошибка чтения конфигурации
        /// </summary>
        private void ConfigurationReadFail()
        {
            this.IsProcess = false;
            this._progressBar.Value = this._progressBar.Maximum;
            this._statusLabel.Text = READ_FAIL;
            MessageBox.Show(READ_FAIL);
        }

        /// <summary>
        /// Конфигурация успешно записана
        /// </summary>
        private void ConfigurationWriteOk()
        {
            this.IsProcess = false;
            this._progressBar.Value = this._progressBar.Maximum;
            this._statusLabel.Text = WRITE_OK;
            this._device.SetBit(this._device.DeviceNumber, 0xd00, true, "Сохранить конфигурацию", this._device);
            MessageBox.Show(WRITE_OK);
        }

        /// <summary>
        /// Ошибка записи конфигурации
        /// </summary>
        private void ConfigurationWriteFail()
        {
            this.IsProcess = false;
            this._progressBar.Value = this._progressBar.Maximum;
            this._statusLabel.Text = WRITE_FAIL;
            MessageBox.Show(WRITE_FAIL);
        }

        #endregion [MemoryEntity Events Handlers]
        
        #region [Help members]
        
        /// <summary>
        /// Выводит все данные на экран
        /// </summary>
        private void ShowConfiguration()
        {
            this._configurationValidator.Set(this._currentSetpointsStruct);
        }

        private bool IsProcess
        {
            set
            {
                this._readConfigBut.Enabled = !value;
                this._writeConfigBut.Enabled = !value;
                this._resetSetpointsButton.Enabled = !value;
                this._loadConfigBut.Enabled = !value;
                this._saveConfigBut.Enabled = !value;
                this._saveToXmlButton.Enabled = !value;
            }
        }

        /// <summary>
        /// Читает все данные с экрана
        /// </summary>
        private bool WriteConfiguration()
        {
            this.IsProcess = true;
            string message;
            this._statusLabel.Text = "Проверка уставок";
            this.statusStrip1.Update();
            if (this._configurationValidator.Check(out message, true))
            {
                this._statusLabel.Text = WRITING_CONFIG;
                this._currentSetpointsStruct = this._configurationValidator.Get();
                this._configuration.Value = this._currentSetpointsStruct;
                return true;
            }
            MessageBox.Show("Обнаружены неккоректные уставки. Конфигурация не может быть записана.");
            this.IsProcess = false;
            return false;
        }

        /// <summary>
        /// Запуск чтения конфигурации
        /// </summary>
        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.IsProcess = true;
            this._statusLabel.Text = READING_CONFIG;
            this._progressBar.Value = 0;
            this._configuration.LoadStruct();
        }

        #endregion [Help members]
        
        #region [Event handlers]

        private void ConfigurationForm_Load(object sender, EventArgs e)
        {
            if (Device.AutoloadConfig)
            {
                this.StartRead();
            }
            this.ResetSetpoints();
        }

        private void ConfigurationForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(this._configuration != null) this._configuration.RemoveStructQueries();
        }
        
        private void _readConfigBut_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void _writeConfigBut_Click(object sender, EventArgs e)
        {
            if (TextboxSupport.PassedValidation)
            {
                this.WriteConfig();
            }
            else
            {
                MessageBox.Show("На одной из открытых форм поле(я) для ввода заполнено(ы) неверно. Действие не будет выполнено.", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            
        }

        private void WriteConfig()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            //if (this._device.MB.IsPortInvalid)
            //{
            //    MessageBox.Show(INVALID_PORT);
            //    return;
            //}
            DialogResult result = MessageBox.Show(string.Format("Записать конфигурацию МР 761 ОБР №{0}?", this._device.DeviceNumber),
                "Запись", MessageBoxButtons.YesNo);
            if (result != DialogResult.Yes) return;
            this._progressBar.Value = 0;
            if (!this.WriteConfiguration()) return;
            this._configuration.SaveStruct();
        }
        private void _oscLength_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox comboBox = sender as ComboBox;
            if (comboBox == null) return;
            int ind = comboBox.SelectedIndex;

            this._oscSizeTextBox.Text = (OSC_SIZE/(ind + 2)).ToString();
        }

        private void _resetSetpointsButton_Click(object sender, EventArgs e)
        {
            this.ResetSetpoints();
        }

        private void ConfigurationForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers != Keys.Control) return;
            switch (e.KeyCode)
            {
                case Keys.W:
                    this.WriteConfig();
                    break;
                case Keys.R:
                    this.StartRead();
                    break;
                case Keys.S:
                    this.SaveInFile();
                    break;
                case Keys.O:
                    this.ReadFromFile();
                    break;
            }
            e.Handled = true;
        }

        private void contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ((ContextMenuStrip)sender).Close();
            if (e.ClickedItem == this.readFromDeviceItem)
            {
                this.StartRead();
                return;
            }
            if (e.ClickedItem == this.writeToDeviceItem)
            {
                this.WriteConfig();
                return;
            }
            if (e.ClickedItem == this.readFromFileItem)
            {
                this.ReadFromFile();
                return;
            }
            if (e.ClickedItem == this.writeToFileItem)
            {
                this.SaveInFile();
                return;
            }
            if (e.ClickedItem == this.writeToHtmlItem)
            {
                this.SaveToHtml();
            }
        }

        private void contextMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.readFromDeviceItem.Enabled = this.writeToDeviceItem.Enabled = this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode;
        }
        #endregion [Event handlers]

        #region [Save/Load File Members]

        private void ResetSetpoints()
        {

            this._configurationValidator.Reset();
        }
        
        private void _saveConfigBut_Click(object sender, EventArgs e)
        {
            if (TextboxSupport.PassedValidation)
            {
                this.SaveInFile();
            }
            else
            {
                MessageBox.Show("На одной из открытых форм поле(я) для ввода заполнено(ы) неверно. Действие не будет выполнено.", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            
        }

        private void SaveInFile()
        {
            this._saveConfigurationDlg.FileName = string.Format("МР761ОБР_Уставки_версия {0:F1}.xml", this._device.DeviceVersion);
            if (DialogResult.OK != this._saveConfigurationDlg.ShowDialog()) return;
            if (this.WriteConfiguration())
                this.Serialize(this._saveConfigurationDlg.FileName);
        }
        private void _loadConfigBut_Click(object sender, EventArgs e)
        {
            this.ReadFromFile();
        }

        private void ReadFromFile()
        {
            if (DialogResult.OK == this._openConfigurationDlg.ShowDialog())
            {
                this.Deserialize(this._openConfigurationDlg.FileName);
            }
        }
        /// <summary>
        /// Сохранение конфигурации в файл
        /// </summary>
        /// <param name="binFileName">Имя файла</param>
        public void Serialize(string binFileName)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.AppendChild(doc.CreateElement("Mr761ObrDevice"));
                ushort[] values = this._currentSetpointsStruct.GetValues();
                XmlElement element = doc.CreateElement(XML_HEAD);
                element.InnerText = Convert.ToBase64String(Common.TOBYTES(values, false));
                if (doc.DocumentElement == null)
                {
                    throw new NullReferenceException();
                }
                doc.DocumentElement.AppendChild(element);
                doc.Save(binFileName);
                this._statusLabel.Text = string.Format("Файл {0} успешно сохранён", binFileName);
                this.IsProcess = false;
            }
            catch
            {
                MessageBox.Show(FILE_SAVE_FAIL);
                this.IsProcess = false;
            }
        }

        /// <summary>
        /// Загрузка конфигурации из файла
        /// </summary>
        /// <param name="binFileName">Имя файла</param>
        public void Deserialize(string binFileName)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(binFileName);

                XmlNode a = doc.FirstChild.SelectSingleNode(XML_HEAD);
                if (a == null)
                    throw new NullReferenceException();

                byte[] values = Convert.FromBase64String(a.InnerText);
                this._currentSetpointsStruct.InitStruct(values);
                this.ShowConfiguration();
                this._statusLabel.Text = string.Format("Файл {0} успешно загружен", binFileName);
            }
            catch
            {
                MessageBox.Show(FILE_LOAD_FAIL);
            }
        }

        private void _saveToXmlButton_Click(object sender, EventArgs e)
        {
            if (TextboxSupport.PassedValidation)
            {
                this.SaveToHtml();
            }
            else
            {
                MessageBox.Show("На одной из открытых форм поле(я) для ввода заполнено(ы) неверно. Действие не будет выполнено.", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void SaveToHtml()
        {
            try
            {
                if (this.WriteConfiguration())
                {
                    this._statusLabel.Text = "Идет запись конфигурации в HTML";
                    ExportGroupForm exportGroup = new ExportGroupForm();
                    if (exportGroup.ShowDialog() != DialogResult.OK)
                    {
                        this.IsProcess = false;
                        this._statusLabel.Text = string.Empty;
                        return;
                    }
                    this._currentSetpointsStruct.DeviceVersion = this._device.DeviceVersion;
                    this._currentSetpointsStruct.DeviceNumber = this._device.DeviceNumber.ToString();
                    this._currentSetpointsStruct.SizeOsc = this._oscSizeTextBox.Text;
                    this._currentSetpointsStruct.Group = (int) exportGroup.SelectedGroup;
                    this.SaveToHtml(_currentSetpointsStruct, exportGroup.SelectedGroup);

                    //this._statusLabel.Text = HtmlExport.ExportGroupMain(Resources.MR771Group1, Resources.MR771Group2,
                    //    Resources.MR771Group3, Resources.MR771Group4, Resources.MR771Group5,
                    //    Resources.MR771Group6, this._currentSetpointsStruct, "МР761ОБР", this._device.DeviceVersion);
                }
                this.IsProcess = false;

            }
            catch (Exception)
            {
                MessageBox.Show("Ошибка сохранения");
            }
        }

        private void SaveToHtml(StructBase str, ExportStruct group)
        {
            string fileName;
            if (group == ExportStruct.ALL)
            {
                fileName = string.Format("{0} Уставки версия {1} все группы", "МР761OBR", this._device.DeviceVersion);
                this._statusLabel.Text = HtmlExport.ExportGroupMain(Resources.MR761OBRAll, fileName, str);
            }
            else
            {
                fileName = string.Format("{0} Уставки версия {1} группа{2}", "МР761OBR", this._device.DeviceVersion, (int)group);
                this._statusLabel.Text = HtmlExport.ExportGroupMain(Resources.MR761OBRGroup, fileName, str);
            }
        }

        #endregion [Save/Load File Members]

        #region [Switch setpoints]

        private void _applyCopySetpoinsButton_Click(object sender, EventArgs e)
        {
            string message;
            if (this._setpointUnionV1.Check(out message, true))
            {
                GroupSetpointStruct[] allSetpoints =
                    this.CopySetpoints<AllGroupSetpointStruct, GroupSetpointStruct>(this._setpointUnionV1,
                        this._setpointsValidatorV1);
                this._currentSetpointsStruct.AllGroupSetpoints.Setpoints = allSetpoints;
                this._setpointsValidatorV1.Set(this._currentSetpointsStruct.AllGroupSetpoints);
                MessageBox.Show("Копирование завершено");
            }
            else
            {
                MessageBox.Show("Обнаружены некорректные уставки");
            }
        }

        private T2[] CopySetpoints<T1, T2>(IValidator union, IValidator setpointValidator)
            where T1 : StructBase, ISetpointContainer<T2> where T2 : StructBase
        {
            T2 temp = (T2) union.Get();
            T2[] allSetpoints = ((T1) setpointValidator.Get()).Setpoints;
            if ((string) this._copySetpoinsGroupComboBox.SelectedItem == StringsConfig.CopyGroupsNames.Last())
            {
                for (int i = 0; i < allSetpoints.Length; i++)
                {
                    allSetpoints[i] = temp.Clone<T2>();
                }
            }
            else
            {
                allSetpoints[this._copySetpoinsGroupComboBox.SelectedIndex] = temp.Clone<T2>();
                allSetpoints[this._setpointsComboBox.SelectedIndex] = temp.Clone<T2>();
            }
            return allSetpoints;
        }

        #endregion

        #region [IFormView Members]

        public Type FormDevice
        {
            get { return typeof(Mr761ObrDevice); }
        }

        public bool Multishow { get; private set; }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public Type ClassType
        {
            get { return typeof(ConfigurationForm); }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Properties.Resources.config.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Конфигурация"; }
        }

        #endregion [IFormView Members]
    }
}
