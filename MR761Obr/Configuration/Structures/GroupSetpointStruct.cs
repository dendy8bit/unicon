﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MR761Obr.Configuration.Structures.External;
using BEMN.MR761Obr.Configuration.Structures.Ls;
using BEMN.MR761Obr.Configuration.Structures.Vls;

namespace BEMN.MR761Obr.Configuration.Structures
{
    public class GroupSetpointStruct : StructBase
    {
        #region Fields

        [Layout(0)] private AllDefenseExternalStruct _allDefenseExternal;
        [Layout(1)] private AllInputLogicStruct _inputLogicSignal;
        [Layout(2)] private AllOutputLogicSignalStruct _outputLogicSignal;
        #endregion

        #region Property
        [BindingProperty(0)]
        public AllDefenseExternalStruct DefensesSetpoints
        {
            get { return this._allDefenseExternal; }
            set { this._allDefenseExternal = value; }
        }

        [BindingProperty(1)]
        public AllInputLogicStruct InputLogicSignal
        {
            get { return this._inputLogicSignal; }
            set { this._inputLogicSignal = value; }
        }

        [BindingProperty(2)]
        public AllOutputLogicSignalStruct OutputLogicSignal
        {
            get { return this._outputLogicSignal; }
            set { this._outputLogicSignal = value; }
        }
        #endregion Property
    }
}
