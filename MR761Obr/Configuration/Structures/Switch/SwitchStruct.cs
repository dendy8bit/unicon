﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR761Obr.Configuration.Structures.Switch
{
    /// <summary>
    /// Конфигурациия выключателя
    /// </summary>
    public class SwitchStruct : StructBase
    {
        #region [Private fields]

        //так надо
        //конфигурация: управления выключателем
        //бит 0001 - управление от кнопок (0-запрещено, 1-разрешено)
        //бит 0002 - внешнее управление (0-контроль, 1-разрешено)
        //бит 0004 - управление от ключа (0-контроль, 1-разрешено)
        //бит 0008 - управление по СДТУ (0-запрещено, 1-разрешено)
        //бит 0080 - выключатель (0-нет, 1-есть)
        [Layout(0)] private ushort _config;
        [Layout(1)] private ushort _on; //вход положение включено
        [Layout(2)] private ushort _off; //вход положение выключено
        [Layout(3)] private ushort _err; //вход неисправность выключателя
        [Layout(4)] private ushort _block; //вход блокировка включения
        [Layout(5)] private ushort _timeUrov; //время УРОВ
        [Layout(6)] private ushort _urov; //ток УРОВ
        [Layout(7)] private ushort _timeImp; //время импульса сигнала управления
        [Layout(8)] private ushort _timeAcc; //длительность включения (время ускорения)
        [Layout(9)] private ushort _ccde; //для контроля цепей включения отключения
        //0-выведено
        //1-введено
        [Layout(10)] private ushort _keyOn; //вход ключ включить
        [Layout(11)] private ushort _keyOff; //вход ключ выключить
        [Layout(12)] private ushort _extOn; //вход внеш. включить
        [Layout(13)] private ushort _extOff; //вход внеш. выключить 
        [Layout(14)] private ushort _ccdeinp; //для контроля второго соленоида отключения
        [Layout(15)] private ushort _res;
        #endregion [Private fields]


        #region [Properties]
        
        /// <summary>
        /// Отключено
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "Отключено")]
        public string OffXml
        {
            get { return Validator.Get(this._off,StringsConfig.SwitchSignals); }
            set { this._off =Validator.Set(value,StringsConfig.SwitchSignals) ; }
        }
        
        /// <summary>
        /// Включено
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "Включено")]
        public string OnXml
        {
            get { return Validator.Get(this._on,StringsConfig.SwitchSignals); }
            set { this._on = Validator.Set(value,StringsConfig.SwitchSignals); }
        }

        /// <summary>
        /// Ошибка
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "Ошибка")]
        public string ErrorXml
        {
            get { return Validator.Get(this._err,StringsConfig.SwitchSignals); }
            set { this._err =Validator.Set(value,StringsConfig.SwitchSignals); }
        }

        /// <summary>
        /// Блокировка
        /// </summary>
        [BindingProperty(3)]
        [XmlElement(ElementName = "Блокировка")]
        public string BlockingXml
        {
            get { return Validator.Get(this._block,StringsConfig.SwitchSignals) ; }
            set { this._block =Validator.Set(value,StringsConfig.SwitchSignals) ; }
        }
        /// <summary>
        /// Импульс
        /// </summary>
        [BindingProperty(4)]
        [XmlElement(ElementName = "Импульс")]
        public int Pulse
        {
            get { return ValuesConverterCommon.GetWaitTime(this._timeImp); }
            set { this._timeImp = ValuesConverterCommon.SetWaitTime(value); }
        }

        /// <summary>
        /// Ускорение
        /// </summary>
        [BindingProperty(5)]
        [XmlElement(ElementName = "Ускорение")]
        public int Acceleration
        {
            get { return ValuesConverterCommon.GetWaitTime(this._timeAcc); }
            set { this._timeAcc = ValuesConverterCommon.SetWaitTime(value); }
        }

        /// <summary>
        ///  контроля цепей включения отключения
        /// </summary>
        [BindingProperty(6)]
        [XmlElement(ElementName = "контроля_цепей_включения_отключения")]
        public string ControlXml
        {
            get { return Validator.Get(this._ccde,StringsConfig.OffOn) ; }
            set { this._ccde = Validator.Set(value,StringsConfig.OffOn); }
        }

        /// <summary>
        /// контроля цепей второго соленоида отключения
        /// </summary>
        [BindingProperty(7)]
        [XmlElement(ElementName = "контроля_цепей_второго_соленоида_отключения")]
        public string ControlSolenoid
        {
            get { return Validator.Get(this._ccdeinp,StringsConfig.SwitchSignals) ; }
            set { this._ccdeinp = Validator.Set(value,StringsConfig.SwitchSignals); }
        }

        /// <summary>
        /// ключ ВКЛ
        /// </summary>
        [BindingProperty(8)]
        [XmlElement(ElementName = "ключ_ВКЛ")]
        public string KeyOnXml
        {
            get { return Validator.Get(this._keyOn,StringsConfig.SwitchSignals) ; }
            set { this._keyOn =Validator.Set(value,StringsConfig.SwitchSignals); }
        }

        /// <summary>
        /// ключ ВЫКЛ
        /// </summary>
        [BindingProperty(9)]
        [XmlElement(ElementName = "ключ_ВЫКЛ")]
        public string KeyOffXml
        {
            get { return Validator.Get(this._keyOff, StringsConfig.SwitchSignals); }
            set { this._keyOff =Validator.Set(value,StringsConfig.SwitchSignals) ; }
        }

        /// <summary>
        /// вход внеш. включить
        /// </summary>
        [BindingProperty(10)]
        [XmlElement(ElementName = "вход_внеш_включить")]
        public string InputOnXml
        {
            get { return Validator.Get(this._extOn,StringsConfig.SwitchSignals) ; }
            set { this._extOn = Validator.Set(value,StringsConfig.SwitchSignals) ; }
        }

        /// <summary>
        /// вход внеш. выключить
        /// </summary>
        [BindingProperty(11)]
        [XmlElement(ElementName = "вход_внеш_выключить")]
        public string InputOffXml
        {
            get { return Validator.Get(this._extOff,StringsConfig.SwitchSignals); }
            set { this._extOff =Validator.Set(value,StringsConfig.SwitchSignals); }
        }

        /// <summary>
        /// Меню
        /// </summary>
        [BindingProperty(12)]
        [XmlElement(ElementName = "Меню")]
        public string MenuXml
        {
            get { return Validator.Get(this._config, StringsConfig.ForbiddenAllowed, 0); }
            set { this._config = Validator.Set(value, StringsConfig.ForbiddenAllowed, this._config, 0); }
        }

        /// <summary>
        /// Ключ
        /// </summary>
        [BindingProperty(13)]
        [XmlElement(ElementName = "Ключ")]
        public string KeyXml
        {
            get { return Validator.Get(this._config, StringsConfig.ControlForbidden, 1); }
            set { this._config = Validator.Set(value, StringsConfig.ControlForbidden, this._config, 1); }
        }

        /// <summary>
        /// Внешнее
        /// </summary>
        [BindingProperty(14)]
        [XmlElement(ElementName = "Внешнее")]
        public string ExternalXml
        {
            get { return Validator.Get(this._config, StringsConfig.ControlForbidden, 2); }
            set { this._config = Validator.Set(value, StringsConfig.ControlForbidden, this._config, 2); }
        }
        
        /// <summary>
        /// СДТУ
        /// </summary>
        [BindingProperty(15)]
        [XmlElement(ElementName = "СДТУ")]
        public string SdtuXml
        {
            get { return Validator.Get(this._config, StringsConfig.ForbiddenAllowed, 3); }
            set { this._config = Validator.Set(value, StringsConfig.ForbiddenAllowed, this._config, 3); }
        }

        [BindingProperty(16)]
        [XmlElement(ElementName = "Команда_отключения")]
        public string ImpDlit
        {
            get { return Validator.Get(this._config, StringsConfig.ImpDlit, 4); }
            set { this._config = Validator.Set(value, StringsConfig.ImpDlit, this._config, 4); }
        }
        #endregion [Properties]
    }
}
