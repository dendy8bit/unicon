﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;

namespace BEMN.MR761Obr.Configuration.Structures
{
    public class AllGroupSetpointStruct : StructBase, ISetpointContainer<GroupSetpointStruct>
    {
        private const int GROUP_COUNT = 6;

        [Layout(0, Count = GROUP_COUNT)] private GroupSetpointStruct[] _groupSetpoints;

        public GroupSetpointStruct[] Setpoints
        {
            get
            {
                var res = new GroupSetpointStruct[GROUP_COUNT];
                for (int i = 0; i < GROUP_COUNT; i++)
                {
                    res[i] = this._groupSetpoints[i].Clone<GroupSetpointStruct>();
                }
                return res;
            }
            set
            {
                for (int i = 0; i < GROUP_COUNT; i++)
                {
                    this._groupSetpoints[i] = value[i];
                }
            }
        }

        [XmlElement(ElementName = "Группа1")]
        public GroupSetpointStruct Group1
        {
            get
            {
                return this._groupSetpoints[0].Clone<GroupSetpointStruct>();
            }
            set { }
        }

        [XmlElement(ElementName = "Группа2")]
        public GroupSetpointStruct Group2
        {
            get
            {
                return this._groupSetpoints[1].Clone<GroupSetpointStruct>();
            }
            set { }
        }

        [XmlElement(ElementName = "Группа3")]
        public GroupSetpointStruct Group3
        {
            get
            {
                return this._groupSetpoints[2].Clone<GroupSetpointStruct>();
            }
            set { }
        }

        [XmlElement(ElementName = "Группа4")]
        public GroupSetpointStruct Group4
        {
            get
            {
                return this._groupSetpoints[3].Clone<GroupSetpointStruct>();
            }
            set { }
        }

        [XmlElement(ElementName = "Группа5")]
        public GroupSetpointStruct Group5
        {
            get
            {
                return this._groupSetpoints[4].Clone<GroupSetpointStruct>();
            }
            set { }
        }

        [XmlElement(ElementName = "Группа6")]
        public GroupSetpointStruct Group6
        {
            get
            {
                return this._groupSetpoints[5].Clone<GroupSetpointStruct>();
            }
            set { }
        }
    }
}
