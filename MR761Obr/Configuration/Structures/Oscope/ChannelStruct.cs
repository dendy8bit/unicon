﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR761Obr.Configuration.Structures.Oscope
{
    [XmlType(TypeName = "Один_канал")]
    public class ChannelStruct : StructBase
    {
        [Layout(0)] private ushort _channel;
        private ushort _base;

        [BindingProperty(0)]
        [XmlAttribute(AttributeName = "Канал")]
        public string ChannelStr
        {
            get
            {
                return Validator.Get(this._channel, StringsConfig.RelaySignals);
            }
            set
            {
                this._channel = Validator.Set(value, StringsConfig.RelaySignals);
            }
        }

        public ushort ChannelValue
        {
            get{return this._channel;}
        }

        public void SetBase(ushort oscBase)
        {
            this._base = oscBase;
        }

        public bool[] GetBase()
        {
            return new[] {Common.GetBit(this._base, 1), Common.GetBit(this._base, 0)};
        }
    }
}
