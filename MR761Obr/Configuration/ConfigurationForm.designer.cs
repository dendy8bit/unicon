﻿namespace BEMN.MR761Obr.Configuration
{
    partial class ConfigurationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle41 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle42 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle43 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle44 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle45 = new System.Windows.Forms.DataGridViewCellStyle();
            this._configurationTabControl = new System.Windows.Forms.TabControl();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.readFromDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readFromFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToHtmlItem = new System.Windows.Forms.ToolStripMenuItem();
            this._setpointPage = new System.Windows.Forms.TabPage();
            this.groupBox24 = new System.Windows.Forms.GroupBox();
            this._applyCopySetpoinsButton = new System.Windows.Forms.Button();
            this._copySetpoinsGroupComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._setpointsTab = new System.Windows.Forms.TabControl();
            this._defExtGr1 = new System.Windows.Forms.TabPage();
            this._externalDefensesGr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewComboBoxColumn4 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn5 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewCheckBoxColumn4 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn5 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._logicSignalGr1 = new System.Windows.Forms.TabPage();
            this.groupBox49 = new System.Windows.Forms.GroupBox();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.VLS1 = new System.Windows.Forms.TabPage();
            this.VLSclb1Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS2 = new System.Windows.Forms.TabPage();
            this.VLSclb2Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS3 = new System.Windows.Forms.TabPage();
            this.VLSclb3Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS4 = new System.Windows.Forms.TabPage();
            this.VLSclb4Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS5 = new System.Windows.Forms.TabPage();
            this.VLSclb5Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS6 = new System.Windows.Forms.TabPage();
            this.VLSclb6Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS7 = new System.Windows.Forms.TabPage();
            this.VLSclb7Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS8 = new System.Windows.Forms.TabPage();
            this.VLSclb8Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS9 = new System.Windows.Forms.TabPage();
            this.VLSclb9Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS10 = new System.Windows.Forms.TabPage();
            this.VLSclb10Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS11 = new System.Windows.Forms.TabPage();
            this.VLSclb11Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS12 = new System.Windows.Forms.TabPage();
            this.VLSclb12Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS13 = new System.Windows.Forms.TabPage();
            this.VLSclb13Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS14 = new System.Windows.Forms.TabPage();
            this.VLSclb14Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS15 = new System.Windows.Forms.TabPage();
            this.VLSclb15Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS16 = new System.Windows.Forms.TabPage();
            this.VLSclb16Gr1 = new System.Windows.Forms.CheckedListBox();
            this.groupBox26 = new System.Windows.Forms.GroupBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage31 = new System.Windows.Forms.TabPage();
            this._lsOrDgv1Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn15 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage32 = new System.Windows.Forms.TabPage();
            this._lsOrDgv2Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn16 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage33 = new System.Windows.Forms.TabPage();
            this._lsOrDgv3Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn17 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage34 = new System.Windows.Forms.TabPage();
            this._lsOrDgv4Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn18 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage35 = new System.Windows.Forms.TabPage();
            this._lsOrDgv5Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn19 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage36 = new System.Windows.Forms.TabPage();
            this._lsOrDgv6Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn20 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage37 = new System.Windows.Forms.TabPage();
            this._lsOrDgv7Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn21 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage38 = new System.Windows.Forms.TabPage();
            this._lsOrDgv8Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn22 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.groupBox45 = new System.Windows.Forms.GroupBox();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPage39 = new System.Windows.Forms.TabPage();
            this._lsAndDgv1Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn23 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage40 = new System.Windows.Forms.TabPage();
            this._lsAndDgv2Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn24 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage41 = new System.Windows.Forms.TabPage();
            this._lsAndDgv3Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn25 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage42 = new System.Windows.Forms.TabPage();
            this._lsAndDgv4Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn26 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage43 = new System.Windows.Forms.TabPage();
            this._lsAndDgv5Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn27 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage44 = new System.Windows.Forms.TabPage();
            this._lsAndDgv6Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn28 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage45 = new System.Windows.Forms.TabPage();
            this._lsAndDgv7Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn29 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage46 = new System.Windows.Forms.TabPage();
            this._lsAndDgv8Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn30 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this._setpointsComboBox = new System.Windows.Forms.ComboBox();
            this._inputSignalsPage = new System.Windows.Forms.TabPage();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this._indComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.label80 = new System.Windows.Forms.Label();
            this._grUst6ComboBox = new System.Windows.Forms.ComboBox();
            this.label79 = new System.Windows.Forms.Label();
            this._grUst5ComboBox = new System.Windows.Forms.ComboBox();
            this.label78 = new System.Windows.Forms.Label();
            this._grUst4ComboBox = new System.Windows.Forms.ComboBox();
            this.label77 = new System.Windows.Forms.Label();
            this._grUst3ComboBox = new System.Windows.Forms.ComboBox();
            this.label76 = new System.Windows.Forms.Label();
            this._grUst2ComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this._grUst1ComboBox = new System.Windows.Forms.ComboBox();
            this._outputSignalsPage = new System.Windows.Forms.TabPage();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this._resetAlarmCheckBox = new System.Windows.Forms.CheckBox();
            this._resetSystemCheckBox = new System.Windows.Forms.CheckBox();
            this.label110 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.groupBox31 = new System.Windows.Forms.GroupBox();
            this.label121 = new System.Windows.Forms.Label();
            this._fault4CheckBox = new System.Windows.Forms.CheckBox();
            this._fault3CheckBox = new System.Windows.Forms.CheckBox();
            this._fault2CheckBox = new System.Windows.Forms.CheckBox();
            this.label111 = new System.Windows.Forms.Label();
            this._fault1CheckBox = new System.Windows.Forms.CheckBox();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this._impTB = new System.Windows.Forms.MaskedTextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.groupBox175 = new System.Windows.Forms.GroupBox();
            this._outputIndicatorsGrid = new System.Windows.Forms.DataGridView();
            this._outIndNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._outIndTypeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._outIndSignalCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._out1IndSignalCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._outIndSignal2Col = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.groupBox176 = new System.Windows.Forms.GroupBox();
            this._outputReleGrid = new System.Windows.Forms.DataGridView();
            this._releNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._releTypeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._releSignalCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._releWaitCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._systemPage = new System.Windows.Forms.TabPage();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this._oscChannelsGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn6 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn7 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.oscStartCb = new System.Windows.Forms.ComboBox();
            this.label65 = new System.Windows.Forms.Label();
            this._oscSizeTextBox = new System.Windows.Forms.MaskedTextBox();
            this._oscWriteLength = new System.Windows.Forms.MaskedTextBox();
            this._oscFix = new System.Windows.Forms.ComboBox();
            this._oscLength = new System.Windows.Forms.ComboBox();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this._automatPage = new System.Windows.Forms.TabPage();
            this.groupBox32 = new System.Windows.Forms.GroupBox();
            this.label96 = new System.Windows.Forms.Label();
            this._comandOtkl = new System.Windows.Forms.ComboBox();
            this._controlSolenoidCombo = new System.Windows.Forms.ComboBox();
            this._switchKontCep = new System.Windows.Forms.ComboBox();
            this._switchTUskor = new System.Windows.Forms.MaskedTextBox();
            this._switchImp = new System.Windows.Forms.MaskedTextBox();
            this._switchBlock = new System.Windows.Forms.ComboBox();
            this._switchError = new System.Windows.Forms.ComboBox();
            this._switchOn = new System.Windows.Forms.ComboBox();
            this.label51 = new System.Windows.Forms.Label();
            this._switchOff = new System.Windows.Forms.ComboBox();
            this.label97 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.groupBox33 = new System.Windows.Forms.GroupBox();
            this._switchSDTU = new System.Windows.Forms.ComboBox();
            this._switchVnesh = new System.Windows.Forms.ComboBox();
            this._switchKey = new System.Windows.Forms.ComboBox();
            this._switchButtons = new System.Windows.Forms.ComboBox();
            this._switchVneshOff = new System.Windows.Forms.ComboBox();
            this._switchVneshOn = new System.Windows.Forms.ComboBox();
            this._switchKeyOff = new System.Windows.Forms.ComboBox();
            this._switchKeyOn = new System.Windows.Forms.ComboBox();
            this.label101 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this._ethernetPage = new System.Windows.Forms.TabPage();
            this.groupBox51 = new System.Windows.Forms.GroupBox();
            this._ipLo1 = new System.Windows.Forms.MaskedTextBox();
            this._ipLo2 = new System.Windows.Forms.MaskedTextBox();
            this._ipHi1 = new System.Windows.Forms.MaskedTextBox();
            this._ipHi2 = new System.Windows.Forms.MaskedTextBox();
            this.label214 = new System.Windows.Forms.Label();
            this.label213 = new System.Windows.Forms.Label();
            this.label212 = new System.Windows.Forms.Label();
            this.label179 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this._saveToXmlButton = new System.Windows.Forms.Button();
            this._resetSetpointsButton = new System.Windows.Forms.Button();
            this._writeConfigBut = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this._progressBar = new System.Windows.Forms.ToolStripProgressBar();
            this._statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._readConfigBut = new System.Windows.Forms.Button();
            this._saveConfigBut = new System.Windows.Forms.Button();
            this._loadConfigBut = new System.Windows.Forms.Button();
            this._saveConfigurationDlg = new System.Windows.Forms.SaveFileDialog();
            this._openConfigurationDlg = new System.Windows.Forms.OpenFileDialog();
            this._toolTip = new System.Windows.Forms.ToolTip(this.components);
            this._saveXmlDialog = new System.Windows.Forms.SaveFileDialog();
            this._externalDifSbrosColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._externalDifAPVRetColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._externalDifAPVColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._externalDifUROVColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._externalDifOscColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._externalDifBlockingColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._externalDifVozvrYNColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._externalDifVozvrColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._externalDifTvzColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._externalDifTsrColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._externalDifSrabColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._externalDifModesColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._externalDifStageColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this._configurationTabControl.SuspendLayout();
            this.contextMenu.SuspendLayout();
            this._setpointPage.SuspendLayout();
            this.groupBox24.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this._setpointsTab.SuspendLayout();
            this._defExtGr1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._externalDefensesGr1)).BeginInit();
            this._logicSignalGr1.SuspendLayout();
            this.groupBox49.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.VLS1.SuspendLayout();
            this.VLS2.SuspendLayout();
            this.VLS3.SuspendLayout();
            this.VLS4.SuspendLayout();
            this.VLS5.SuspendLayout();
            this.VLS6.SuspendLayout();
            this.VLS7.SuspendLayout();
            this.VLS8.SuspendLayout();
            this.VLS9.SuspendLayout();
            this.VLS10.SuspendLayout();
            this.VLS11.SuspendLayout();
            this.VLS12.SuspendLayout();
            this.VLS13.SuspendLayout();
            this.VLS14.SuspendLayout();
            this.VLS15.SuspendLayout();
            this.VLS16.SuspendLayout();
            this.groupBox26.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage31.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv1Gr1)).BeginInit();
            this.tabPage32.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv2Gr1)).BeginInit();
            this.tabPage33.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv3Gr1)).BeginInit();
            this.tabPage34.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv4Gr1)).BeginInit();
            this.tabPage35.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv5Gr1)).BeginInit();
            this.tabPage36.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv6Gr1)).BeginInit();
            this.tabPage37.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv7Gr1)).BeginInit();
            this.tabPage38.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv8Gr1)).BeginInit();
            this.groupBox45.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabPage39.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv1Gr1)).BeginInit();
            this.tabPage40.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv2Gr1)).BeginInit();
            this.tabPage41.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv3Gr1)).BeginInit();
            this.tabPage42.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv4Gr1)).BeginInit();
            this.tabPage43.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv5Gr1)).BeginInit();
            this.tabPage44.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv6Gr1)).BeginInit();
            this.tabPage45.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv7Gr1)).BeginInit();
            this.tabPage46.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv8Gr1)).BeginInit();
            this.groupBox7.SuspendLayout();
            this._inputSignalsPage.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this._outputSignalsPage.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox31.SuspendLayout();
            this.groupBox175.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._outputIndicatorsGrid)).BeginInit();
            this.groupBox176.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._outputReleGrid)).BeginInit();
            this._systemPage.SuspendLayout();
            this.groupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._oscChannelsGrid)).BeginInit();
            this.groupBox3.SuspendLayout();
            this._automatPage.SuspendLayout();
            this.groupBox32.SuspendLayout();
            this.groupBox33.SuspendLayout();
            this._ethernetPage.SuspendLayout();
            this.groupBox51.SuspendLayout();
            this.panel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _configurationTabControl
            // 
            this._configurationTabControl.ContextMenuStrip = this.contextMenu;
            this._configurationTabControl.Controls.Add(this._setpointPage);
            this._configurationTabControl.Controls.Add(this._inputSignalsPage);
            this._configurationTabControl.Controls.Add(this._outputSignalsPage);
            this._configurationTabControl.Controls.Add(this._systemPage);
            this._configurationTabControl.Controls.Add(this._automatPage);
            this._configurationTabControl.Controls.Add(this._ethernetPage);
            this._configurationTabControl.Dock = System.Windows.Forms.DockStyle.Top;
            this._configurationTabControl.Location = new System.Drawing.Point(0, 0);
            this._configurationTabControl.MinimumSize = new System.Drawing.Size(820, 579);
            this._configurationTabControl.Name = "_configurationTabControl";
            this._configurationTabControl.SelectedIndex = 0;
            this._configurationTabControl.Size = new System.Drawing.Size(984, 579);
            this._configurationTabControl.TabIndex = 31;
            // 
            // contextMenu
            // 
            this.contextMenu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.readFromDeviceItem,
            this.writeToDeviceItem,
            this.readFromFileItem,
            this.writeToFileItem,
            this.writeToHtmlItem});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(213, 114);
            this.contextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenu_Opening);
            this.contextMenu.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.contextMenu_ItemClicked);
            // 
            // readFromDeviceItem
            // 
            this.readFromDeviceItem.Name = "readFromDeviceItem";
            this.readFromDeviceItem.Size = new System.Drawing.Size(212, 22);
            this.readFromDeviceItem.Text = "Прочитать из устройства";
            // 
            // writeToDeviceItem
            // 
            this.writeToDeviceItem.Name = "writeToDeviceItem";
            this.writeToDeviceItem.Size = new System.Drawing.Size(212, 22);
            this.writeToDeviceItem.Text = "Записать в устройство";
            // 
            // readFromFileItem
            // 
            this.readFromFileItem.Name = "readFromFileItem";
            this.readFromFileItem.Size = new System.Drawing.Size(212, 22);
            this.readFromFileItem.Text = "Загрузить из файла";
            // 
            // writeToFileItem
            // 
            this.writeToFileItem.Name = "writeToFileItem";
            this.writeToFileItem.Size = new System.Drawing.Size(212, 22);
            this.writeToFileItem.Text = "Сохранить в файл";
            // 
            // writeToHtmlItem
            // 
            this.writeToHtmlItem.Name = "writeToHtmlItem";
            this.writeToHtmlItem.Size = new System.Drawing.Size(212, 22);
            this.writeToHtmlItem.Text = "Сохранить в HTML";
            // 
            // _setpointPage
            // 
            this._setpointPage.Controls.Add(this.groupBox24);
            this._setpointPage.Controls.Add(this.groupBox1);
            this._setpointPage.Controls.Add(this.groupBox7);
            this._setpointPage.Location = new System.Drawing.Point(4, 22);
            this._setpointPage.Name = "_setpointPage";
            this._setpointPage.Size = new System.Drawing.Size(976, 553);
            this._setpointPage.TabIndex = 4;
            this._setpointPage.Text = "Уставки";
            this._setpointPage.UseVisualStyleBackColor = true;
            // 
            // groupBox24
            // 
            this.groupBox24.Controls.Add(this._applyCopySetpoinsButton);
            this.groupBox24.Controls.Add(this._copySetpoinsGroupComboBox);
            this.groupBox24.Location = new System.Drawing.Point(112, 3);
            this.groupBox24.Name = "groupBox24";
            this.groupBox24.Size = new System.Drawing.Size(174, 54);
            this.groupBox24.TabIndex = 3;
            this.groupBox24.TabStop = false;
            this.groupBox24.Text = "Копировать в";
            // 
            // _applyCopySetpoinsButton
            // 
            this._applyCopySetpoinsButton.Location = new System.Drawing.Point(95, 17);
            this._applyCopySetpoinsButton.Name = "_applyCopySetpoinsButton";
            this._applyCopySetpoinsButton.Size = new System.Drawing.Size(75, 23);
            this._applyCopySetpoinsButton.TabIndex = 1;
            this._applyCopySetpoinsButton.Text = "Применить";
            this._applyCopySetpoinsButton.UseVisualStyleBackColor = true;
            this._applyCopySetpoinsButton.Click += new System.EventHandler(this._applyCopySetpoinsButton_Click);
            // 
            // _copySetpoinsGroupComboBox
            // 
            this._copySetpoinsGroupComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._copySetpoinsGroupComboBox.FormattingEnabled = true;
            this._copySetpoinsGroupComboBox.Location = new System.Drawing.Point(6, 19);
            this._copySetpoinsGroupComboBox.Name = "_copySetpoinsGroupComboBox";
            this._copySetpoinsGroupComboBox.Size = new System.Drawing.Size(83, 21);
            this._copySetpoinsGroupComboBox.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this._setpointsTab);
            this.groupBox1.Location = new System.Drawing.Point(3, 59);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(970, 491);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            // 
            // _setpointsTab
            // 
            this._setpointsTab.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this._setpointsTab.Controls.Add(this._defExtGr1);
            this._setpointsTab.Controls.Add(this._logicSignalGr1);
            this._setpointsTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this._setpointsTab.Location = new System.Drawing.Point(3, 16);
            this._setpointsTab.Name = "_setpointsTab";
            this._setpointsTab.SelectedIndex = 0;
            this._setpointsTab.Size = new System.Drawing.Size(964, 472);
            this._setpointsTab.TabIndex = 2;
            // 
            // _defExtGr1
            // 
            this._defExtGr1.Controls.Add(this._externalDefensesGr1);
            this._defExtGr1.Location = new System.Drawing.Point(4, 25);
            this._defExtGr1.Name = "_defExtGr1";
            this._defExtGr1.Size = new System.Drawing.Size(956, 443);
            this._defExtGr1.TabIndex = 8;
            this._defExtGr1.Text = "Внешние";
            this._defExtGr1.UseVisualStyleBackColor = true;
            // 
            // _externalDefensesGr1
            // 
            this._externalDefensesGr1.AllowUserToAddRows = false;
            this._externalDefensesGr1.AllowUserToDeleteRows = false;
            this._externalDefensesGr1.AllowUserToResizeColumns = false;
            this._externalDefensesGr1.AllowUserToResizeRows = false;
            this._externalDefensesGr1.BackgroundColor = System.Drawing.Color.White;
            this._externalDefensesGr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._externalDefensesGr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewComboBoxColumn1,
            this.dataGridViewComboBoxColumn2,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewComboBoxColumn3,
            this.dataGridViewCheckBoxColumn1,
            this.dataGridViewComboBoxColumn4,
            this.dataGridViewComboBoxColumn5,
            this.dataGridViewCheckBoxColumn4,
            this.dataGridViewCheckBoxColumn5});
            this._externalDefensesGr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._externalDefensesGr1.Location = new System.Drawing.Point(0, 0);
            this._externalDefensesGr1.Name = "_externalDefensesGr1";
            this._externalDefensesGr1.RowHeadersVisible = false;
            this._externalDefensesGr1.RowHeadersWidth = 51;
            this._externalDefensesGr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._externalDefensesGr1.RowTemplate.Height = 24;
            this._externalDefensesGr1.ShowCellErrors = false;
            this._externalDefensesGr1.ShowRowErrors = false;
            this._externalDefensesGr1.Size = new System.Drawing.Size(956, 443);
            this._externalDefensesGr1.TabIndex = 5;
            // 
            // dataGridViewTextBoxColumn1
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewTextBoxColumn1.Frozen = true;
            this.dataGridViewTextBoxColumn1.HeaderText = "Ступень";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn1.Width = 125;
            // 
            // dataGridViewComboBoxColumn1
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewComboBoxColumn1.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn1.HeaderText = "Состояние";
            this.dataGridViewComboBoxColumn1.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            this.dataGridViewComboBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn1.Width = 110;
            // 
            // dataGridViewComboBoxColumn2
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn2.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewComboBoxColumn2.HeaderText = "Сигнал срабатывания";
            this.dataGridViewComboBoxColumn2.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn2.Name = "dataGridViewComboBoxColumn2";
            this.dataGridViewComboBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn2.Width = 130;
            // 
            // dataGridViewTextBoxColumn2
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewTextBoxColumn2.HeaderText = "tср [мс]";
            this.dataGridViewTextBoxColumn2.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn2.Width = 70;
            // 
            // dataGridViewTextBoxColumn3
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridViewTextBoxColumn3.HeaderText = "tвз [мс]";
            this.dataGridViewTextBoxColumn3.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn3.Width = 70;
            // 
            // dataGridViewComboBoxColumn3
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn3.DefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridViewComboBoxColumn3.HeaderText = "Сигнал возврат";
            this.dataGridViewComboBoxColumn3.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn3.Name = "dataGridViewComboBoxColumn3";
            this.dataGridViewComboBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn3.Width = 125;
            // 
            // dataGridViewCheckBoxColumn1
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.NullValue = false;
            this.dataGridViewCheckBoxColumn1.DefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewCheckBoxColumn1.HeaderText = "Возврат";
            this.dataGridViewCheckBoxColumn1.MinimumWidth = 6;
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn1.Width = 65;
            // 
            // dataGridViewComboBoxColumn4
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn4.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridViewComboBoxColumn4.HeaderText = "Сигнал блокировки";
            this.dataGridViewComboBoxColumn4.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn4.Name = "dataGridViewComboBoxColumn4";
            this.dataGridViewComboBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn4.Width = 120;
            // 
            // dataGridViewComboBoxColumn5
            // 
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn5.DefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewComboBoxColumn5.HeaderText = "Осциллограф";
            this.dataGridViewComboBoxColumn5.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn5.Name = "dataGridViewComboBoxColumn5";
            this.dataGridViewComboBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn5.Width = 110;
            // 
            // dataGridViewCheckBoxColumn4
            // 
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle10.NullValue = false;
            this.dataGridViewCheckBoxColumn4.DefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridViewCheckBoxColumn4.HeaderText = "АПВ возвр.";
            this.dataGridViewCheckBoxColumn4.MinimumWidth = 6;
            this.dataGridViewCheckBoxColumn4.Name = "dataGridViewCheckBoxColumn4";
            this.dataGridViewCheckBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn4.Width = 75;
            // 
            // dataGridViewCheckBoxColumn5
            // 
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle11.NullValue = false;
            this.dataGridViewCheckBoxColumn5.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridViewCheckBoxColumn5.HeaderText = "Сброс";
            this.dataGridViewCheckBoxColumn5.MinimumWidth = 6;
            this.dataGridViewCheckBoxColumn5.Name = "dataGridViewCheckBoxColumn5";
            this.dataGridViewCheckBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn5.Width = 50;
            // 
            // _logicSignalGr1
            // 
            this._logicSignalGr1.AutoScroll = true;
            this._logicSignalGr1.Controls.Add(this.groupBox49);
            this._logicSignalGr1.Controls.Add(this.groupBox26);
            this._logicSignalGr1.Controls.Add(this.groupBox45);
            this._logicSignalGr1.Location = new System.Drawing.Point(4, 25);
            this._logicSignalGr1.Name = "_logicSignalGr1";
            this._logicSignalGr1.Size = new System.Drawing.Size(956, 445);
            this._logicSignalGr1.TabIndex = 14;
            this._logicSignalGr1.Text = "Логические сигналы";
            this._logicSignalGr1.UseVisualStyleBackColor = true;
            // 
            // groupBox49
            // 
            this.groupBox49.Controls.Add(this.tabControl2);
            this.groupBox49.Location = new System.Drawing.Point(406, 4);
            this.groupBox49.Name = "groupBox49";
            this.groupBox49.Size = new System.Drawing.Size(409, 436);
            this.groupBox49.TabIndex = 6;
            this.groupBox49.TabStop = false;
            this.groupBox49.Text = "ВЛС";
            // 
            // tabControl2
            // 
            this.tabControl2.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabControl2.Controls.Add(this.VLS1);
            this.tabControl2.Controls.Add(this.VLS2);
            this.tabControl2.Controls.Add(this.VLS3);
            this.tabControl2.Controls.Add(this.VLS4);
            this.tabControl2.Controls.Add(this.VLS5);
            this.tabControl2.Controls.Add(this.VLS6);
            this.tabControl2.Controls.Add(this.VLS7);
            this.tabControl2.Controls.Add(this.VLS8);
            this.tabControl2.Controls.Add(this.VLS9);
            this.tabControl2.Controls.Add(this.VLS10);
            this.tabControl2.Controls.Add(this.VLS11);
            this.tabControl2.Controls.Add(this.VLS12);
            this.tabControl2.Controls.Add(this.VLS13);
            this.tabControl2.Controls.Add(this.VLS14);
            this.tabControl2.Controls.Add(this.VLS15);
            this.tabControl2.Controls.Add(this.VLS16);
            this.tabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl2.Location = new System.Drawing.Point(3, 16);
            this.tabControl2.Multiline = true;
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(403, 417);
            this.tabControl2.TabIndex = 0;
            // 
            // VLS1
            // 
            this.VLS1.Controls.Add(this.VLSclb1Gr1);
            this.VLS1.Location = new System.Drawing.Point(4, 49);
            this.VLS1.Name = "VLS1";
            this.VLS1.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.VLS1.Size = new System.Drawing.Size(395, 364);
            this.VLS1.TabIndex = 0;
            this.VLS1.Text = "ВЛС 1";
            this.VLS1.UseVisualStyleBackColor = true;
            // 
            // VLSclb1Gr1
            // 
            this.VLSclb1Gr1.CheckOnClick = true;
            this.VLSclb1Gr1.FormattingEnabled = true;
            this.VLSclb1Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb1Gr1.Name = "VLSclb1Gr1";
            this.VLSclb1Gr1.ScrollAlwaysVisible = true;
            this.VLSclb1Gr1.Size = new System.Drawing.Size(191, 289);
            this.VLSclb1Gr1.TabIndex = 6;
            // 
            // VLS2
            // 
            this.VLS2.Controls.Add(this.VLSclb2Gr1);
            this.VLS2.Location = new System.Drawing.Point(4, 49);
            this.VLS2.Name = "VLS2";
            this.VLS2.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.VLS2.Size = new System.Drawing.Size(395, 366);
            this.VLS2.TabIndex = 1;
            this.VLS2.Text = "ВЛС 2";
            this.VLS2.UseVisualStyleBackColor = true;
            // 
            // VLSclb2Gr1
            // 
            this.VLSclb2Gr1.CheckOnClick = true;
            this.VLSclb2Gr1.FormattingEnabled = true;
            this.VLSclb2Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb2Gr1.Name = "VLSclb2Gr1";
            this.VLSclb2Gr1.ScrollAlwaysVisible = true;
            this.VLSclb2Gr1.Size = new System.Drawing.Size(184, 289);
            this.VLSclb2Gr1.TabIndex = 6;
            // 
            // VLS3
            // 
            this.VLS3.Controls.Add(this.VLSclb3Gr1);
            this.VLS3.Location = new System.Drawing.Point(4, 49);
            this.VLS3.Name = "VLS3";
            this.VLS3.Size = new System.Drawing.Size(395, 366);
            this.VLS3.TabIndex = 2;
            this.VLS3.Text = "ВЛС   3";
            this.VLS3.UseVisualStyleBackColor = true;
            // 
            // VLSclb3Gr1
            // 
            this.VLSclb3Gr1.CheckOnClick = true;
            this.VLSclb3Gr1.FormattingEnabled = true;
            this.VLSclb3Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb3Gr1.Name = "VLSclb3Gr1";
            this.VLSclb3Gr1.ScrollAlwaysVisible = true;
            this.VLSclb3Gr1.Size = new System.Drawing.Size(184, 289);
            this.VLSclb3Gr1.TabIndex = 6;
            // 
            // VLS4
            // 
            this.VLS4.Controls.Add(this.VLSclb4Gr1);
            this.VLS4.Location = new System.Drawing.Point(4, 49);
            this.VLS4.Name = "VLS4";
            this.VLS4.Size = new System.Drawing.Size(395, 366);
            this.VLS4.TabIndex = 3;
            this.VLS4.Text = "ВЛС 4";
            this.VLS4.UseVisualStyleBackColor = true;
            // 
            // VLSclb4Gr1
            // 
            this.VLSclb4Gr1.CheckOnClick = true;
            this.VLSclb4Gr1.FormattingEnabled = true;
            this.VLSclb4Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb4Gr1.Name = "VLSclb4Gr1";
            this.VLSclb4Gr1.ScrollAlwaysVisible = true;
            this.VLSclb4Gr1.Size = new System.Drawing.Size(184, 289);
            this.VLSclb4Gr1.TabIndex = 6;
            // 
            // VLS5
            // 
            this.VLS5.Controls.Add(this.VLSclb5Gr1);
            this.VLS5.Location = new System.Drawing.Point(4, 49);
            this.VLS5.Name = "VLS5";
            this.VLS5.Size = new System.Drawing.Size(395, 366);
            this.VLS5.TabIndex = 4;
            this.VLS5.Text = "ВЛС   5";
            this.VLS5.UseVisualStyleBackColor = true;
            // 
            // VLSclb5Gr1
            // 
            this.VLSclb5Gr1.CheckOnClick = true;
            this.VLSclb5Gr1.FormattingEnabled = true;
            this.VLSclb5Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb5Gr1.Name = "VLSclb5Gr1";
            this.VLSclb5Gr1.ScrollAlwaysVisible = true;
            this.VLSclb5Gr1.Size = new System.Drawing.Size(184, 289);
            this.VLSclb5Gr1.TabIndex = 6;
            // 
            // VLS6
            // 
            this.VLS6.Controls.Add(this.VLSclb6Gr1);
            this.VLS6.Location = new System.Drawing.Point(4, 49);
            this.VLS6.Name = "VLS6";
            this.VLS6.Size = new System.Drawing.Size(395, 366);
            this.VLS6.TabIndex = 5;
            this.VLS6.Text = "ВЛС  6";
            this.VLS6.UseVisualStyleBackColor = true;
            // 
            // VLSclb6Gr1
            // 
            this.VLSclb6Gr1.CheckOnClick = true;
            this.VLSclb6Gr1.FormattingEnabled = true;
            this.VLSclb6Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb6Gr1.Name = "VLSclb6Gr1";
            this.VLSclb6Gr1.ScrollAlwaysVisible = true;
            this.VLSclb6Gr1.Size = new System.Drawing.Size(184, 289);
            this.VLSclb6Gr1.TabIndex = 6;
            // 
            // VLS7
            // 
            this.VLS7.Controls.Add(this.VLSclb7Gr1);
            this.VLS7.Location = new System.Drawing.Point(4, 49);
            this.VLS7.Name = "VLS7";
            this.VLS7.Size = new System.Drawing.Size(395, 366);
            this.VLS7.TabIndex = 6;
            this.VLS7.Text = "ВЛС 7";
            this.VLS7.UseVisualStyleBackColor = true;
            // 
            // VLSclb7Gr1
            // 
            this.VLSclb7Gr1.CheckOnClick = true;
            this.VLSclb7Gr1.FormattingEnabled = true;
            this.VLSclb7Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb7Gr1.Name = "VLSclb7Gr1";
            this.VLSclb7Gr1.ScrollAlwaysVisible = true;
            this.VLSclb7Gr1.Size = new System.Drawing.Size(184, 289);
            this.VLSclb7Gr1.TabIndex = 6;
            // 
            // VLS8
            // 
            this.VLS8.Controls.Add(this.VLSclb8Gr1);
            this.VLS8.Location = new System.Drawing.Point(4, 49);
            this.VLS8.Name = "VLS8";
            this.VLS8.Size = new System.Drawing.Size(395, 366);
            this.VLS8.TabIndex = 7;
            this.VLS8.Text = "ВЛС   8";
            this.VLS8.UseVisualStyleBackColor = true;
            // 
            // VLSclb8Gr1
            // 
            this.VLSclb8Gr1.CheckOnClick = true;
            this.VLSclb8Gr1.FormattingEnabled = true;
            this.VLSclb8Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb8Gr1.Name = "VLSclb8Gr1";
            this.VLSclb8Gr1.ScrollAlwaysVisible = true;
            this.VLSclb8Gr1.Size = new System.Drawing.Size(184, 289);
            this.VLSclb8Gr1.TabIndex = 6;
            // 
            // VLS9
            // 
            this.VLS9.Controls.Add(this.VLSclb9Gr1);
            this.VLS9.Location = new System.Drawing.Point(4, 49);
            this.VLS9.Name = "VLS9";
            this.VLS9.Size = new System.Drawing.Size(395, 366);
            this.VLS9.TabIndex = 8;
            this.VLS9.Text = "ВЛС9";
            this.VLS9.UseVisualStyleBackColor = true;
            // 
            // VLSclb9Gr1
            // 
            this.VLSclb9Gr1.CheckOnClick = true;
            this.VLSclb9Gr1.FormattingEnabled = true;
            this.VLSclb9Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb9Gr1.Name = "VLSclb9Gr1";
            this.VLSclb9Gr1.ScrollAlwaysVisible = true;
            this.VLSclb9Gr1.Size = new System.Drawing.Size(184, 289);
            this.VLSclb9Gr1.TabIndex = 6;
            // 
            // VLS10
            // 
            this.VLS10.Controls.Add(this.VLSclb10Gr1);
            this.VLS10.Location = new System.Drawing.Point(4, 49);
            this.VLS10.Name = "VLS10";
            this.VLS10.Size = new System.Drawing.Size(395, 366);
            this.VLS10.TabIndex = 9;
            this.VLS10.Text = "ВЛС10";
            this.VLS10.UseVisualStyleBackColor = true;
            // 
            // VLSclb10Gr1
            // 
            this.VLSclb10Gr1.CheckOnClick = true;
            this.VLSclb10Gr1.FormattingEnabled = true;
            this.VLSclb10Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb10Gr1.Name = "VLSclb10Gr1";
            this.VLSclb10Gr1.ScrollAlwaysVisible = true;
            this.VLSclb10Gr1.Size = new System.Drawing.Size(184, 289);
            this.VLSclb10Gr1.TabIndex = 6;
            // 
            // VLS11
            // 
            this.VLS11.Controls.Add(this.VLSclb11Gr1);
            this.VLS11.Location = new System.Drawing.Point(4, 49);
            this.VLS11.Name = "VLS11";
            this.VLS11.Size = new System.Drawing.Size(395, 366);
            this.VLS11.TabIndex = 10;
            this.VLS11.Text = "ВЛС11";
            this.VLS11.UseVisualStyleBackColor = true;
            // 
            // VLSclb11Gr1
            // 
            this.VLSclb11Gr1.CheckOnClick = true;
            this.VLSclb11Gr1.FormattingEnabled = true;
            this.VLSclb11Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb11Gr1.Name = "VLSclb11Gr1";
            this.VLSclb11Gr1.ScrollAlwaysVisible = true;
            this.VLSclb11Gr1.Size = new System.Drawing.Size(184, 289);
            this.VLSclb11Gr1.TabIndex = 6;
            // 
            // VLS12
            // 
            this.VLS12.Controls.Add(this.VLSclb12Gr1);
            this.VLS12.Location = new System.Drawing.Point(4, 49);
            this.VLS12.Name = "VLS12";
            this.VLS12.Size = new System.Drawing.Size(395, 366);
            this.VLS12.TabIndex = 11;
            this.VLS12.Text = "ВЛС12";
            this.VLS12.UseVisualStyleBackColor = true;
            // 
            // VLSclb12Gr1
            // 
            this.VLSclb12Gr1.CheckOnClick = true;
            this.VLSclb12Gr1.FormattingEnabled = true;
            this.VLSclb12Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb12Gr1.Name = "VLSclb12Gr1";
            this.VLSclb12Gr1.ScrollAlwaysVisible = true;
            this.VLSclb12Gr1.Size = new System.Drawing.Size(184, 289);
            this.VLSclb12Gr1.TabIndex = 6;
            // 
            // VLS13
            // 
            this.VLS13.Controls.Add(this.VLSclb13Gr1);
            this.VLS13.Location = new System.Drawing.Point(4, 49);
            this.VLS13.Name = "VLS13";
            this.VLS13.Size = new System.Drawing.Size(395, 366);
            this.VLS13.TabIndex = 12;
            this.VLS13.Text = "ВЛС13";
            this.VLS13.UseVisualStyleBackColor = true;
            // 
            // VLSclb13Gr1
            // 
            this.VLSclb13Gr1.CheckOnClick = true;
            this.VLSclb13Gr1.FormattingEnabled = true;
            this.VLSclb13Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb13Gr1.Name = "VLSclb13Gr1";
            this.VLSclb13Gr1.ScrollAlwaysVisible = true;
            this.VLSclb13Gr1.Size = new System.Drawing.Size(184, 289);
            this.VLSclb13Gr1.TabIndex = 6;
            // 
            // VLS14
            // 
            this.VLS14.Controls.Add(this.VLSclb14Gr1);
            this.VLS14.Location = new System.Drawing.Point(4, 49);
            this.VLS14.Name = "VLS14";
            this.VLS14.Size = new System.Drawing.Size(395, 366);
            this.VLS14.TabIndex = 13;
            this.VLS14.Text = "ВЛС14";
            this.VLS14.UseVisualStyleBackColor = true;
            // 
            // VLSclb14Gr1
            // 
            this.VLSclb14Gr1.CheckOnClick = true;
            this.VLSclb14Gr1.FormattingEnabled = true;
            this.VLSclb14Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb14Gr1.Name = "VLSclb14Gr1";
            this.VLSclb14Gr1.ScrollAlwaysVisible = true;
            this.VLSclb14Gr1.Size = new System.Drawing.Size(184, 289);
            this.VLSclb14Gr1.TabIndex = 6;
            // 
            // VLS15
            // 
            this.VLS15.Controls.Add(this.VLSclb15Gr1);
            this.VLS15.Location = new System.Drawing.Point(4, 49);
            this.VLS15.Name = "VLS15";
            this.VLS15.Size = new System.Drawing.Size(395, 366);
            this.VLS15.TabIndex = 14;
            this.VLS15.Text = "ВЛС15";
            this.VLS15.UseVisualStyleBackColor = true;
            // 
            // VLSclb15Gr1
            // 
            this.VLSclb15Gr1.CheckOnClick = true;
            this.VLSclb15Gr1.FormattingEnabled = true;
            this.VLSclb15Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb15Gr1.Name = "VLSclb15Gr1";
            this.VLSclb15Gr1.ScrollAlwaysVisible = true;
            this.VLSclb15Gr1.Size = new System.Drawing.Size(184, 289);
            this.VLSclb15Gr1.TabIndex = 6;
            // 
            // VLS16
            // 
            this.VLS16.Controls.Add(this.VLSclb16Gr1);
            this.VLS16.Location = new System.Drawing.Point(4, 49);
            this.VLS16.Name = "VLS16";
            this.VLS16.Size = new System.Drawing.Size(395, 366);
            this.VLS16.TabIndex = 15;
            this.VLS16.Text = "ВЛС16";
            this.VLS16.UseVisualStyleBackColor = true;
            // 
            // VLSclb16Gr1
            // 
            this.VLSclb16Gr1.CheckOnClick = true;
            this.VLSclb16Gr1.FormattingEnabled = true;
            this.VLSclb16Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb16Gr1.Name = "VLSclb16Gr1";
            this.VLSclb16Gr1.ScrollAlwaysVisible = true;
            this.VLSclb16Gr1.Size = new System.Drawing.Size(184, 289);
            this.VLSclb16Gr1.TabIndex = 6;
            // 
            // groupBox26
            // 
            this.groupBox26.Controls.Add(this.tabControl1);
            this.groupBox26.Location = new System.Drawing.Point(207, 3);
            this.groupBox26.Name = "groupBox26";
            this.groupBox26.Size = new System.Drawing.Size(193, 437);
            this.groupBox26.TabIndex = 2;
            this.groupBox26.TabStop = false;
            this.groupBox26.Text = "Логические сигналы ИЛИ";
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabControl1.Controls.Add(this.tabPage31);
            this.tabControl1.Controls.Add(this.tabPage32);
            this.tabControl1.Controls.Add(this.tabPage33);
            this.tabControl1.Controls.Add(this.tabPage34);
            this.tabControl1.Controls.Add(this.tabPage35);
            this.tabControl1.Controls.Add(this.tabPage36);
            this.tabControl1.Controls.Add(this.tabPage37);
            this.tabControl1.Controls.Add(this.tabPage38);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(3, 16);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(187, 418);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPage31
            // 
            this.tabPage31.Controls.Add(this._lsOrDgv1Gr1);
            this.tabPage31.Location = new System.Drawing.Point(4, 49);
            this.tabPage31.Name = "tabPage31";
            this.tabPage31.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage31.Size = new System.Drawing.Size(179, 365);
            this.tabPage31.TabIndex = 0;
            this.tabPage31.Text = "ЛС9";
            this.tabPage31.UseVisualStyleBackColor = true;
            // 
            // _lsOrDgv1Gr1
            // 
            this._lsOrDgv1Gr1.AllowUserToAddRows = false;
            this._lsOrDgv1Gr1.AllowUserToDeleteRows = false;
            this._lsOrDgv1Gr1.AllowUserToResizeColumns = false;
            this._lsOrDgv1Gr1.AllowUserToResizeRows = false;
            this._lsOrDgv1Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsOrDgv1Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsOrDgv1Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsOrDgv1Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewComboBoxColumn15});
            this._lsOrDgv1Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsOrDgv1Gr1.MultiSelect = false;
            this._lsOrDgv1Gr1.Name = "_lsOrDgv1Gr1";
            this._lsOrDgv1Gr1.RowHeadersVisible = false;
            this._lsOrDgv1Gr1.RowHeadersWidth = 51;
            this._lsOrDgv1Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsOrDgv1Gr1.RowsDefaultCellStyle = dataGridViewCellStyle12;
            this._lsOrDgv1Gr1.RowTemplate.Height = 20;
            this._lsOrDgv1Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsOrDgv1Gr1.ShowCellErrors = false;
            this._lsOrDgv1Gr1.ShowCellToolTips = false;
            this._lsOrDgv1Gr1.ShowEditingIcon = false;
            this._lsOrDgv1Gr1.ShowRowErrors = false;
            this._lsOrDgv1Gr1.Size = new System.Drawing.Size(179, 365);
            this._lsOrDgv1Gr1.TabIndex = 2;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.HeaderText = "№";
            this.dataGridViewTextBoxColumn15.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            this.dataGridViewTextBoxColumn15.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn15.Width = 24;
            // 
            // dataGridViewComboBoxColumn15
            // 
            this.dataGridViewComboBoxColumn15.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn15.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn15.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn15.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn15.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn15.Name = "dataGridViewComboBoxColumn15";
            // 
            // tabPage32
            // 
            this.tabPage32.Controls.Add(this._lsOrDgv2Gr1);
            this.tabPage32.Location = new System.Drawing.Point(4, 49);
            this.tabPage32.Name = "tabPage32";
            this.tabPage32.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage32.Size = new System.Drawing.Size(179, 367);
            this.tabPage32.TabIndex = 1;
            this.tabPage32.Text = "ЛС10";
            this.tabPage32.UseVisualStyleBackColor = true;
            // 
            // _lsOrDgv2Gr1
            // 
            this._lsOrDgv2Gr1.AllowUserToAddRows = false;
            this._lsOrDgv2Gr1.AllowUserToDeleteRows = false;
            this._lsOrDgv2Gr1.AllowUserToResizeColumns = false;
            this._lsOrDgv2Gr1.AllowUserToResizeRows = false;
            this._lsOrDgv2Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsOrDgv2Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsOrDgv2Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsOrDgv2Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewComboBoxColumn16});
            this._lsOrDgv2Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsOrDgv2Gr1.MultiSelect = false;
            this._lsOrDgv2Gr1.Name = "_lsOrDgv2Gr1";
            this._lsOrDgv2Gr1.RowHeadersVisible = false;
            this._lsOrDgv2Gr1.RowHeadersWidth = 51;
            this._lsOrDgv2Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsOrDgv2Gr1.RowsDefaultCellStyle = dataGridViewCellStyle13;
            this._lsOrDgv2Gr1.RowTemplate.Height = 20;
            this._lsOrDgv2Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsOrDgv2Gr1.ShowCellErrors = false;
            this._lsOrDgv2Gr1.ShowCellToolTips = false;
            this._lsOrDgv2Gr1.ShowEditingIcon = false;
            this._lsOrDgv2Gr1.ShowRowErrors = false;
            this._lsOrDgv2Gr1.Size = new System.Drawing.Size(179, 365);
            this._lsOrDgv2Gr1.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.HeaderText = "№";
            this.dataGridViewTextBoxColumn16.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            this.dataGridViewTextBoxColumn16.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn16.Width = 24;
            // 
            // dataGridViewComboBoxColumn16
            // 
            this.dataGridViewComboBoxColumn16.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn16.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn16.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn16.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn16.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn16.Name = "dataGridViewComboBoxColumn16";
            // 
            // tabPage33
            // 
            this.tabPage33.Controls.Add(this._lsOrDgv3Gr1);
            this.tabPage33.Location = new System.Drawing.Point(4, 49);
            this.tabPage33.Name = "tabPage33";
            this.tabPage33.Size = new System.Drawing.Size(179, 367);
            this.tabPage33.TabIndex = 2;
            this.tabPage33.Text = "ЛС11";
            this.tabPage33.UseVisualStyleBackColor = true;
            // 
            // _lsOrDgv3Gr1
            // 
            this._lsOrDgv3Gr1.AllowUserToAddRows = false;
            this._lsOrDgv3Gr1.AllowUserToDeleteRows = false;
            this._lsOrDgv3Gr1.AllowUserToResizeColumns = false;
            this._lsOrDgv3Gr1.AllowUserToResizeRows = false;
            this._lsOrDgv3Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsOrDgv3Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsOrDgv3Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsOrDgv3Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn18,
            this.dataGridViewComboBoxColumn17});
            this._lsOrDgv3Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsOrDgv3Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsOrDgv3Gr1.MultiSelect = false;
            this._lsOrDgv3Gr1.Name = "_lsOrDgv3Gr1";
            this._lsOrDgv3Gr1.RowHeadersVisible = false;
            this._lsOrDgv3Gr1.RowHeadersWidth = 51;
            this._lsOrDgv3Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsOrDgv3Gr1.RowsDefaultCellStyle = dataGridViewCellStyle14;
            this._lsOrDgv3Gr1.RowTemplate.Height = 20;
            this._lsOrDgv3Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsOrDgv3Gr1.ShowCellErrors = false;
            this._lsOrDgv3Gr1.ShowCellToolTips = false;
            this._lsOrDgv3Gr1.ShowEditingIcon = false;
            this._lsOrDgv3Gr1.ShowRowErrors = false;
            this._lsOrDgv3Gr1.Size = new System.Drawing.Size(179, 367);
            this._lsOrDgv3Gr1.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.HeaderText = "№";
            this.dataGridViewTextBoxColumn18.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            this.dataGridViewTextBoxColumn18.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn18.Width = 24;
            // 
            // dataGridViewComboBoxColumn17
            // 
            this.dataGridViewComboBoxColumn17.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn17.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn17.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn17.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn17.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn17.Name = "dataGridViewComboBoxColumn17";
            // 
            // tabPage34
            // 
            this.tabPage34.Controls.Add(this._lsOrDgv4Gr1);
            this.tabPage34.Location = new System.Drawing.Point(4, 49);
            this.tabPage34.Name = "tabPage34";
            this.tabPage34.Size = new System.Drawing.Size(179, 367);
            this.tabPage34.TabIndex = 3;
            this.tabPage34.Text = "ЛС12";
            this.tabPage34.UseVisualStyleBackColor = true;
            // 
            // _lsOrDgv4Gr1
            // 
            this._lsOrDgv4Gr1.AllowUserToAddRows = false;
            this._lsOrDgv4Gr1.AllowUserToDeleteRows = false;
            this._lsOrDgv4Gr1.AllowUserToResizeColumns = false;
            this._lsOrDgv4Gr1.AllowUserToResizeRows = false;
            this._lsOrDgv4Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsOrDgv4Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsOrDgv4Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsOrDgv4Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn19,
            this.dataGridViewComboBoxColumn18});
            this._lsOrDgv4Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsOrDgv4Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsOrDgv4Gr1.MultiSelect = false;
            this._lsOrDgv4Gr1.Name = "_lsOrDgv4Gr1";
            this._lsOrDgv4Gr1.RowHeadersVisible = false;
            this._lsOrDgv4Gr1.RowHeadersWidth = 51;
            this._lsOrDgv4Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsOrDgv4Gr1.RowsDefaultCellStyle = dataGridViewCellStyle15;
            this._lsOrDgv4Gr1.RowTemplate.Height = 20;
            this._lsOrDgv4Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsOrDgv4Gr1.ShowCellErrors = false;
            this._lsOrDgv4Gr1.ShowCellToolTips = false;
            this._lsOrDgv4Gr1.ShowEditingIcon = false;
            this._lsOrDgv4Gr1.ShowRowErrors = false;
            this._lsOrDgv4Gr1.Size = new System.Drawing.Size(179, 367);
            this._lsOrDgv4Gr1.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.HeaderText = "№";
            this.dataGridViewTextBoxColumn19.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.ReadOnly = true;
            this.dataGridViewTextBoxColumn19.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn19.Width = 24;
            // 
            // dataGridViewComboBoxColumn18
            // 
            this.dataGridViewComboBoxColumn18.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn18.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn18.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn18.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn18.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn18.Name = "dataGridViewComboBoxColumn18";
            // 
            // tabPage35
            // 
            this.tabPage35.Controls.Add(this._lsOrDgv5Gr1);
            this.tabPage35.Location = new System.Drawing.Point(4, 49);
            this.tabPage35.Name = "tabPage35";
            this.tabPage35.Size = new System.Drawing.Size(179, 367);
            this.tabPage35.TabIndex = 4;
            this.tabPage35.Text = "ЛС13";
            this.tabPage35.UseVisualStyleBackColor = true;
            // 
            // _lsOrDgv5Gr1
            // 
            this._lsOrDgv5Gr1.AllowUserToAddRows = false;
            this._lsOrDgv5Gr1.AllowUserToDeleteRows = false;
            this._lsOrDgv5Gr1.AllowUserToResizeColumns = false;
            this._lsOrDgv5Gr1.AllowUserToResizeRows = false;
            this._lsOrDgv5Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsOrDgv5Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsOrDgv5Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsOrDgv5Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn20,
            this.dataGridViewComboBoxColumn19});
            this._lsOrDgv5Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsOrDgv5Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsOrDgv5Gr1.MultiSelect = false;
            this._lsOrDgv5Gr1.Name = "_lsOrDgv5Gr1";
            this._lsOrDgv5Gr1.RowHeadersVisible = false;
            this._lsOrDgv5Gr1.RowHeadersWidth = 51;
            this._lsOrDgv5Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsOrDgv5Gr1.RowsDefaultCellStyle = dataGridViewCellStyle16;
            this._lsOrDgv5Gr1.RowTemplate.Height = 20;
            this._lsOrDgv5Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsOrDgv5Gr1.ShowCellErrors = false;
            this._lsOrDgv5Gr1.ShowCellToolTips = false;
            this._lsOrDgv5Gr1.ShowEditingIcon = false;
            this._lsOrDgv5Gr1.ShowRowErrors = false;
            this._lsOrDgv5Gr1.Size = new System.Drawing.Size(179, 367);
            this._lsOrDgv5Gr1.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn20
            // 
            this.dataGridViewTextBoxColumn20.HeaderText = "№";
            this.dataGridViewTextBoxColumn20.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.ReadOnly = true;
            this.dataGridViewTextBoxColumn20.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn20.Width = 24;
            // 
            // dataGridViewComboBoxColumn19
            // 
            this.dataGridViewComboBoxColumn19.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn19.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn19.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn19.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn19.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn19.Name = "dataGridViewComboBoxColumn19";
            // 
            // tabPage36
            // 
            this.tabPage36.Controls.Add(this._lsOrDgv6Gr1);
            this.tabPage36.Location = new System.Drawing.Point(4, 49);
            this.tabPage36.Name = "tabPage36";
            this.tabPage36.Size = new System.Drawing.Size(179, 367);
            this.tabPage36.TabIndex = 5;
            this.tabPage36.Text = "ЛС14";
            this.tabPage36.UseVisualStyleBackColor = true;
            // 
            // _lsOrDgv6Gr1
            // 
            this._lsOrDgv6Gr1.AllowUserToAddRows = false;
            this._lsOrDgv6Gr1.AllowUserToDeleteRows = false;
            this._lsOrDgv6Gr1.AllowUserToResizeColumns = false;
            this._lsOrDgv6Gr1.AllowUserToResizeRows = false;
            this._lsOrDgv6Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsOrDgv6Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsOrDgv6Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsOrDgv6Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn21,
            this.dataGridViewComboBoxColumn20});
            this._lsOrDgv6Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsOrDgv6Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsOrDgv6Gr1.MultiSelect = false;
            this._lsOrDgv6Gr1.Name = "_lsOrDgv6Gr1";
            this._lsOrDgv6Gr1.RowHeadersVisible = false;
            this._lsOrDgv6Gr1.RowHeadersWidth = 51;
            this._lsOrDgv6Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsOrDgv6Gr1.RowsDefaultCellStyle = dataGridViewCellStyle17;
            this._lsOrDgv6Gr1.RowTemplate.Height = 20;
            this._lsOrDgv6Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsOrDgv6Gr1.ShowCellErrors = false;
            this._lsOrDgv6Gr1.ShowCellToolTips = false;
            this._lsOrDgv6Gr1.ShowEditingIcon = false;
            this._lsOrDgv6Gr1.ShowRowErrors = false;
            this._lsOrDgv6Gr1.Size = new System.Drawing.Size(179, 367);
            this._lsOrDgv6Gr1.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn21
            // 
            this.dataGridViewTextBoxColumn21.HeaderText = "№";
            this.dataGridViewTextBoxColumn21.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.ReadOnly = true;
            this.dataGridViewTextBoxColumn21.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn21.Width = 24;
            // 
            // dataGridViewComboBoxColumn20
            // 
            this.dataGridViewComboBoxColumn20.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn20.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn20.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn20.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn20.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn20.Name = "dataGridViewComboBoxColumn20";
            // 
            // tabPage37
            // 
            this.tabPage37.Controls.Add(this._lsOrDgv7Gr1);
            this.tabPage37.Location = new System.Drawing.Point(4, 49);
            this.tabPage37.Name = "tabPage37";
            this.tabPage37.Size = new System.Drawing.Size(179, 367);
            this.tabPage37.TabIndex = 6;
            this.tabPage37.Text = "ЛС15";
            this.tabPage37.UseVisualStyleBackColor = true;
            // 
            // _lsOrDgv7Gr1
            // 
            this._lsOrDgv7Gr1.AllowUserToAddRows = false;
            this._lsOrDgv7Gr1.AllowUserToDeleteRows = false;
            this._lsOrDgv7Gr1.AllowUserToResizeColumns = false;
            this._lsOrDgv7Gr1.AllowUserToResizeRows = false;
            this._lsOrDgv7Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsOrDgv7Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsOrDgv7Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsOrDgv7Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn22,
            this.dataGridViewComboBoxColumn21});
            this._lsOrDgv7Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsOrDgv7Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsOrDgv7Gr1.MultiSelect = false;
            this._lsOrDgv7Gr1.Name = "_lsOrDgv7Gr1";
            this._lsOrDgv7Gr1.RowHeadersVisible = false;
            this._lsOrDgv7Gr1.RowHeadersWidth = 51;
            this._lsOrDgv7Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsOrDgv7Gr1.RowsDefaultCellStyle = dataGridViewCellStyle18;
            this._lsOrDgv7Gr1.RowTemplate.Height = 20;
            this._lsOrDgv7Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsOrDgv7Gr1.ShowCellErrors = false;
            this._lsOrDgv7Gr1.ShowCellToolTips = false;
            this._lsOrDgv7Gr1.ShowEditingIcon = false;
            this._lsOrDgv7Gr1.ShowRowErrors = false;
            this._lsOrDgv7Gr1.Size = new System.Drawing.Size(179, 367);
            this._lsOrDgv7Gr1.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn22
            // 
            this.dataGridViewTextBoxColumn22.HeaderText = "№";
            this.dataGridViewTextBoxColumn22.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.ReadOnly = true;
            this.dataGridViewTextBoxColumn22.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn22.Width = 24;
            // 
            // dataGridViewComboBoxColumn21
            // 
            this.dataGridViewComboBoxColumn21.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn21.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn21.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn21.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn21.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn21.Name = "dataGridViewComboBoxColumn21";
            // 
            // tabPage38
            // 
            this.tabPage38.Controls.Add(this._lsOrDgv8Gr1);
            this.tabPage38.Location = new System.Drawing.Point(4, 49);
            this.tabPage38.Name = "tabPage38";
            this.tabPage38.Size = new System.Drawing.Size(179, 367);
            this.tabPage38.TabIndex = 7;
            this.tabPage38.Text = "ЛС16";
            this.tabPage38.UseVisualStyleBackColor = true;
            // 
            // _lsOrDgv8Gr1
            // 
            this._lsOrDgv8Gr1.AllowUserToAddRows = false;
            this._lsOrDgv8Gr1.AllowUserToDeleteRows = false;
            this._lsOrDgv8Gr1.AllowUserToResizeColumns = false;
            this._lsOrDgv8Gr1.AllowUserToResizeRows = false;
            this._lsOrDgv8Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsOrDgv8Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsOrDgv8Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsOrDgv8Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn23,
            this.dataGridViewComboBoxColumn22});
            this._lsOrDgv8Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsOrDgv8Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsOrDgv8Gr1.MultiSelect = false;
            this._lsOrDgv8Gr1.Name = "_lsOrDgv8Gr1";
            this._lsOrDgv8Gr1.RowHeadersVisible = false;
            this._lsOrDgv8Gr1.RowHeadersWidth = 51;
            this._lsOrDgv8Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsOrDgv8Gr1.RowsDefaultCellStyle = dataGridViewCellStyle19;
            this._lsOrDgv8Gr1.RowTemplate.Height = 20;
            this._lsOrDgv8Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsOrDgv8Gr1.ShowCellErrors = false;
            this._lsOrDgv8Gr1.ShowCellToolTips = false;
            this._lsOrDgv8Gr1.ShowEditingIcon = false;
            this._lsOrDgv8Gr1.ShowRowErrors = false;
            this._lsOrDgv8Gr1.Size = new System.Drawing.Size(179, 367);
            this._lsOrDgv8Gr1.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn23
            // 
            this.dataGridViewTextBoxColumn23.HeaderText = "№";
            this.dataGridViewTextBoxColumn23.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            this.dataGridViewTextBoxColumn23.ReadOnly = true;
            this.dataGridViewTextBoxColumn23.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn23.Width = 24;
            // 
            // dataGridViewComboBoxColumn22
            // 
            this.dataGridViewComboBoxColumn22.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn22.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn22.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn22.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn22.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn22.Name = "dataGridViewComboBoxColumn22";
            // 
            // groupBox45
            // 
            this.groupBox45.Controls.Add(this.tabControl);
            this.groupBox45.Location = new System.Drawing.Point(8, 3);
            this.groupBox45.Name = "groupBox45";
            this.groupBox45.Size = new System.Drawing.Size(193, 437);
            this.groupBox45.TabIndex = 0;
            this.groupBox45.TabStop = false;
            this.groupBox45.Text = "Логические сигналы И";
            // 
            // tabControl
            // 
            this.tabControl.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabControl.Controls.Add(this.tabPage39);
            this.tabControl.Controls.Add(this.tabPage40);
            this.tabControl.Controls.Add(this.tabPage41);
            this.tabControl.Controls.Add(this.tabPage42);
            this.tabControl.Controls.Add(this.tabPage43);
            this.tabControl.Controls.Add(this.tabPage44);
            this.tabControl.Controls.Add(this.tabPage45);
            this.tabControl.Controls.Add(this.tabPage46);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(3, 16);
            this.tabControl.Multiline = true;
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(187, 418);
            this.tabControl.TabIndex = 2;
            // 
            // tabPage39
            // 
            this.tabPage39.Controls.Add(this._lsAndDgv1Gr1);
            this.tabPage39.Location = new System.Drawing.Point(4, 49);
            this.tabPage39.Name = "tabPage39";
            this.tabPage39.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage39.Size = new System.Drawing.Size(179, 365);
            this.tabPage39.TabIndex = 0;
            this.tabPage39.Text = "ЛС1";
            this.tabPage39.UseVisualStyleBackColor = true;
            // 
            // _lsAndDgv1Gr1
            // 
            this._lsAndDgv1Gr1.AllowUserToAddRows = false;
            this._lsAndDgv1Gr1.AllowUserToDeleteRows = false;
            this._lsAndDgv1Gr1.AllowUserToResizeColumns = false;
            this._lsAndDgv1Gr1.AllowUserToResizeRows = false;
            this._lsAndDgv1Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsAndDgv1Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsAndDgv1Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsAndDgv1Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn24,
            this.dataGridViewComboBoxColumn23});
            this._lsAndDgv1Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsAndDgv1Gr1.MultiSelect = false;
            this._lsAndDgv1Gr1.Name = "_lsAndDgv1Gr1";
            this._lsAndDgv1Gr1.RowHeadersVisible = false;
            this._lsAndDgv1Gr1.RowHeadersWidth = 51;
            this._lsAndDgv1Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsAndDgv1Gr1.RowsDefaultCellStyle = dataGridViewCellStyle20;
            this._lsAndDgv1Gr1.RowTemplate.Height = 20;
            this._lsAndDgv1Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsAndDgv1Gr1.ShowCellErrors = false;
            this._lsAndDgv1Gr1.ShowCellToolTips = false;
            this._lsAndDgv1Gr1.ShowEditingIcon = false;
            this._lsAndDgv1Gr1.ShowRowErrors = false;
            this._lsAndDgv1Gr1.Size = new System.Drawing.Size(179, 365);
            this._lsAndDgv1Gr1.TabIndex = 2;
            // 
            // dataGridViewTextBoxColumn24
            // 
            this.dataGridViewTextBoxColumn24.HeaderText = "№";
            this.dataGridViewTextBoxColumn24.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            this.dataGridViewTextBoxColumn24.ReadOnly = true;
            this.dataGridViewTextBoxColumn24.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn24.Width = 24;
            // 
            // dataGridViewComboBoxColumn23
            // 
            this.dataGridViewComboBoxColumn23.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn23.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn23.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn23.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn23.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn23.Name = "dataGridViewComboBoxColumn23";
            // 
            // tabPage40
            // 
            this.tabPage40.Controls.Add(this._lsAndDgv2Gr1);
            this.tabPage40.Location = new System.Drawing.Point(4, 49);
            this.tabPage40.Name = "tabPage40";
            this.tabPage40.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage40.Size = new System.Drawing.Size(179, 367);
            this.tabPage40.TabIndex = 1;
            this.tabPage40.Text = "ЛС2";
            this.tabPage40.UseVisualStyleBackColor = true;
            // 
            // _lsAndDgv2Gr1
            // 
            this._lsAndDgv2Gr1.AllowUserToAddRows = false;
            this._lsAndDgv2Gr1.AllowUserToDeleteRows = false;
            this._lsAndDgv2Gr1.AllowUserToResizeColumns = false;
            this._lsAndDgv2Gr1.AllowUserToResizeRows = false;
            this._lsAndDgv2Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsAndDgv2Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsAndDgv2Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsAndDgv2Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn25,
            this.dataGridViewComboBoxColumn24});
            this._lsAndDgv2Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsAndDgv2Gr1.MultiSelect = false;
            this._lsAndDgv2Gr1.Name = "_lsAndDgv2Gr1";
            this._lsAndDgv2Gr1.RowHeadersVisible = false;
            this._lsAndDgv2Gr1.RowHeadersWidth = 51;
            this._lsAndDgv2Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsAndDgv2Gr1.RowsDefaultCellStyle = dataGridViewCellStyle21;
            this._lsAndDgv2Gr1.RowTemplate.Height = 20;
            this._lsAndDgv2Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsAndDgv2Gr1.ShowCellErrors = false;
            this._lsAndDgv2Gr1.ShowCellToolTips = false;
            this._lsAndDgv2Gr1.ShowEditingIcon = false;
            this._lsAndDgv2Gr1.ShowRowErrors = false;
            this._lsAndDgv2Gr1.Size = new System.Drawing.Size(179, 365);
            this._lsAndDgv2Gr1.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn25
            // 
            this.dataGridViewTextBoxColumn25.HeaderText = "№";
            this.dataGridViewTextBoxColumn25.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn25.Name = "dataGridViewTextBoxColumn25";
            this.dataGridViewTextBoxColumn25.ReadOnly = true;
            this.dataGridViewTextBoxColumn25.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn25.Width = 24;
            // 
            // dataGridViewComboBoxColumn24
            // 
            this.dataGridViewComboBoxColumn24.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn24.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn24.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn24.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn24.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn24.Name = "dataGridViewComboBoxColumn24";
            // 
            // tabPage41
            // 
            this.tabPage41.Controls.Add(this._lsAndDgv3Gr1);
            this.tabPage41.Location = new System.Drawing.Point(4, 49);
            this.tabPage41.Name = "tabPage41";
            this.tabPage41.Size = new System.Drawing.Size(179, 367);
            this.tabPage41.TabIndex = 2;
            this.tabPage41.Text = "ЛС3";
            this.tabPage41.UseVisualStyleBackColor = true;
            // 
            // _lsAndDgv3Gr1
            // 
            this._lsAndDgv3Gr1.AllowUserToAddRows = false;
            this._lsAndDgv3Gr1.AllowUserToDeleteRows = false;
            this._lsAndDgv3Gr1.AllowUserToResizeColumns = false;
            this._lsAndDgv3Gr1.AllowUserToResizeRows = false;
            this._lsAndDgv3Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsAndDgv3Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsAndDgv3Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsAndDgv3Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn26,
            this.dataGridViewComboBoxColumn25});
            this._lsAndDgv3Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsAndDgv3Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsAndDgv3Gr1.MultiSelect = false;
            this._lsAndDgv3Gr1.Name = "_lsAndDgv3Gr1";
            this._lsAndDgv3Gr1.RowHeadersVisible = false;
            this._lsAndDgv3Gr1.RowHeadersWidth = 51;
            this._lsAndDgv3Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsAndDgv3Gr1.RowsDefaultCellStyle = dataGridViewCellStyle22;
            this._lsAndDgv3Gr1.RowTemplate.Height = 20;
            this._lsAndDgv3Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsAndDgv3Gr1.ShowCellErrors = false;
            this._lsAndDgv3Gr1.ShowCellToolTips = false;
            this._lsAndDgv3Gr1.ShowEditingIcon = false;
            this._lsAndDgv3Gr1.ShowRowErrors = false;
            this._lsAndDgv3Gr1.Size = new System.Drawing.Size(179, 367);
            this._lsAndDgv3Gr1.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn26
            // 
            this.dataGridViewTextBoxColumn26.HeaderText = "№";
            this.dataGridViewTextBoxColumn26.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn26.Name = "dataGridViewTextBoxColumn26";
            this.dataGridViewTextBoxColumn26.ReadOnly = true;
            this.dataGridViewTextBoxColumn26.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn26.Width = 24;
            // 
            // dataGridViewComboBoxColumn25
            // 
            this.dataGridViewComboBoxColumn25.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn25.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn25.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn25.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn25.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn25.Name = "dataGridViewComboBoxColumn25";
            // 
            // tabPage42
            // 
            this.tabPage42.Controls.Add(this._lsAndDgv4Gr1);
            this.tabPage42.Location = new System.Drawing.Point(4, 49);
            this.tabPage42.Name = "tabPage42";
            this.tabPage42.Size = new System.Drawing.Size(179, 367);
            this.tabPage42.TabIndex = 3;
            this.tabPage42.Text = "ЛС4";
            this.tabPage42.UseVisualStyleBackColor = true;
            // 
            // _lsAndDgv4Gr1
            // 
            this._lsAndDgv4Gr1.AllowUserToAddRows = false;
            this._lsAndDgv4Gr1.AllowUserToDeleteRows = false;
            this._lsAndDgv4Gr1.AllowUserToResizeColumns = false;
            this._lsAndDgv4Gr1.AllowUserToResizeRows = false;
            this._lsAndDgv4Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsAndDgv4Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsAndDgv4Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsAndDgv4Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn27,
            this.dataGridViewComboBoxColumn26});
            this._lsAndDgv4Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsAndDgv4Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsAndDgv4Gr1.MultiSelect = false;
            this._lsAndDgv4Gr1.Name = "_lsAndDgv4Gr1";
            this._lsAndDgv4Gr1.RowHeadersVisible = false;
            this._lsAndDgv4Gr1.RowHeadersWidth = 51;
            this._lsAndDgv4Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsAndDgv4Gr1.RowsDefaultCellStyle = dataGridViewCellStyle23;
            this._lsAndDgv4Gr1.RowTemplate.Height = 20;
            this._lsAndDgv4Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsAndDgv4Gr1.ShowCellErrors = false;
            this._lsAndDgv4Gr1.ShowCellToolTips = false;
            this._lsAndDgv4Gr1.ShowEditingIcon = false;
            this._lsAndDgv4Gr1.ShowRowErrors = false;
            this._lsAndDgv4Gr1.Size = new System.Drawing.Size(179, 367);
            this._lsAndDgv4Gr1.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn27
            // 
            this.dataGridViewTextBoxColumn27.HeaderText = "№";
            this.dataGridViewTextBoxColumn27.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn27.Name = "dataGridViewTextBoxColumn27";
            this.dataGridViewTextBoxColumn27.ReadOnly = true;
            this.dataGridViewTextBoxColumn27.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn27.Width = 24;
            // 
            // dataGridViewComboBoxColumn26
            // 
            this.dataGridViewComboBoxColumn26.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn26.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn26.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn26.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn26.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn26.Name = "dataGridViewComboBoxColumn26";
            // 
            // tabPage43
            // 
            this.tabPage43.Controls.Add(this._lsAndDgv5Gr1);
            this.tabPage43.Location = new System.Drawing.Point(4, 49);
            this.tabPage43.Name = "tabPage43";
            this.tabPage43.Size = new System.Drawing.Size(179, 367);
            this.tabPage43.TabIndex = 4;
            this.tabPage43.Text = "ЛС5";
            this.tabPage43.UseVisualStyleBackColor = true;
            // 
            // _lsAndDgv5Gr1
            // 
            this._lsAndDgv5Gr1.AllowUserToAddRows = false;
            this._lsAndDgv5Gr1.AllowUserToDeleteRows = false;
            this._lsAndDgv5Gr1.AllowUserToResizeColumns = false;
            this._lsAndDgv5Gr1.AllowUserToResizeRows = false;
            this._lsAndDgv5Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsAndDgv5Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsAndDgv5Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsAndDgv5Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn28,
            this.dataGridViewComboBoxColumn27});
            this._lsAndDgv5Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsAndDgv5Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsAndDgv5Gr1.MultiSelect = false;
            this._lsAndDgv5Gr1.Name = "_lsAndDgv5Gr1";
            this._lsAndDgv5Gr1.RowHeadersVisible = false;
            this._lsAndDgv5Gr1.RowHeadersWidth = 51;
            this._lsAndDgv5Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsAndDgv5Gr1.RowsDefaultCellStyle = dataGridViewCellStyle24;
            this._lsAndDgv5Gr1.RowTemplate.Height = 20;
            this._lsAndDgv5Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsAndDgv5Gr1.ShowCellErrors = false;
            this._lsAndDgv5Gr1.ShowCellToolTips = false;
            this._lsAndDgv5Gr1.ShowEditingIcon = false;
            this._lsAndDgv5Gr1.ShowRowErrors = false;
            this._lsAndDgv5Gr1.Size = new System.Drawing.Size(179, 367);
            this._lsAndDgv5Gr1.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn28
            // 
            this.dataGridViewTextBoxColumn28.HeaderText = "№";
            this.dataGridViewTextBoxColumn28.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn28.Name = "dataGridViewTextBoxColumn28";
            this.dataGridViewTextBoxColumn28.ReadOnly = true;
            this.dataGridViewTextBoxColumn28.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn28.Width = 24;
            // 
            // dataGridViewComboBoxColumn27
            // 
            this.dataGridViewComboBoxColumn27.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn27.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn27.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn27.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn27.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn27.Name = "dataGridViewComboBoxColumn27";
            // 
            // tabPage44
            // 
            this.tabPage44.Controls.Add(this._lsAndDgv6Gr1);
            this.tabPage44.Location = new System.Drawing.Point(4, 49);
            this.tabPage44.Name = "tabPage44";
            this.tabPage44.Size = new System.Drawing.Size(179, 367);
            this.tabPage44.TabIndex = 5;
            this.tabPage44.Text = "ЛС6";
            this.tabPage44.UseVisualStyleBackColor = true;
            // 
            // _lsAndDgv6Gr1
            // 
            this._lsAndDgv6Gr1.AllowUserToAddRows = false;
            this._lsAndDgv6Gr1.AllowUserToDeleteRows = false;
            this._lsAndDgv6Gr1.AllowUserToResizeColumns = false;
            this._lsAndDgv6Gr1.AllowUserToResizeRows = false;
            this._lsAndDgv6Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsAndDgv6Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsAndDgv6Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsAndDgv6Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn29,
            this.dataGridViewComboBoxColumn28});
            this._lsAndDgv6Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsAndDgv6Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsAndDgv6Gr1.MultiSelect = false;
            this._lsAndDgv6Gr1.Name = "_lsAndDgv6Gr1";
            this._lsAndDgv6Gr1.RowHeadersVisible = false;
            this._lsAndDgv6Gr1.RowHeadersWidth = 51;
            this._lsAndDgv6Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsAndDgv6Gr1.RowsDefaultCellStyle = dataGridViewCellStyle25;
            this._lsAndDgv6Gr1.RowTemplate.Height = 20;
            this._lsAndDgv6Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsAndDgv6Gr1.ShowCellErrors = false;
            this._lsAndDgv6Gr1.ShowCellToolTips = false;
            this._lsAndDgv6Gr1.ShowEditingIcon = false;
            this._lsAndDgv6Gr1.ShowRowErrors = false;
            this._lsAndDgv6Gr1.Size = new System.Drawing.Size(179, 367);
            this._lsAndDgv6Gr1.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn29
            // 
            this.dataGridViewTextBoxColumn29.HeaderText = "№";
            this.dataGridViewTextBoxColumn29.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn29.Name = "dataGridViewTextBoxColumn29";
            this.dataGridViewTextBoxColumn29.ReadOnly = true;
            this.dataGridViewTextBoxColumn29.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn29.Width = 24;
            // 
            // dataGridViewComboBoxColumn28
            // 
            this.dataGridViewComboBoxColumn28.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn28.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn28.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn28.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn28.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn28.Name = "dataGridViewComboBoxColumn28";
            // 
            // tabPage45
            // 
            this.tabPage45.Controls.Add(this._lsAndDgv7Gr1);
            this.tabPage45.Location = new System.Drawing.Point(4, 49);
            this.tabPage45.Name = "tabPage45";
            this.tabPage45.Size = new System.Drawing.Size(179, 367);
            this.tabPage45.TabIndex = 6;
            this.tabPage45.Text = "ЛС7";
            this.tabPage45.UseVisualStyleBackColor = true;
            // 
            // _lsAndDgv7Gr1
            // 
            this._lsAndDgv7Gr1.AllowUserToAddRows = false;
            this._lsAndDgv7Gr1.AllowUserToDeleteRows = false;
            this._lsAndDgv7Gr1.AllowUserToResizeColumns = false;
            this._lsAndDgv7Gr1.AllowUserToResizeRows = false;
            this._lsAndDgv7Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsAndDgv7Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsAndDgv7Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsAndDgv7Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn30,
            this.dataGridViewComboBoxColumn29});
            this._lsAndDgv7Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsAndDgv7Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsAndDgv7Gr1.MultiSelect = false;
            this._lsAndDgv7Gr1.Name = "_lsAndDgv7Gr1";
            this._lsAndDgv7Gr1.RowHeadersVisible = false;
            this._lsAndDgv7Gr1.RowHeadersWidth = 51;
            this._lsAndDgv7Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsAndDgv7Gr1.RowsDefaultCellStyle = dataGridViewCellStyle26;
            this._lsAndDgv7Gr1.RowTemplate.Height = 20;
            this._lsAndDgv7Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsAndDgv7Gr1.ShowCellErrors = false;
            this._lsAndDgv7Gr1.ShowCellToolTips = false;
            this._lsAndDgv7Gr1.ShowEditingIcon = false;
            this._lsAndDgv7Gr1.ShowRowErrors = false;
            this._lsAndDgv7Gr1.Size = new System.Drawing.Size(179, 367);
            this._lsAndDgv7Gr1.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn30
            // 
            this.dataGridViewTextBoxColumn30.HeaderText = "№";
            this.dataGridViewTextBoxColumn30.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn30.Name = "dataGridViewTextBoxColumn30";
            this.dataGridViewTextBoxColumn30.ReadOnly = true;
            this.dataGridViewTextBoxColumn30.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn30.Width = 24;
            // 
            // dataGridViewComboBoxColumn29
            // 
            this.dataGridViewComboBoxColumn29.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn29.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn29.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn29.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn29.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn29.Name = "dataGridViewComboBoxColumn29";
            // 
            // tabPage46
            // 
            this.tabPage46.Controls.Add(this._lsAndDgv8Gr1);
            this.tabPage46.Location = new System.Drawing.Point(4, 49);
            this.tabPage46.Name = "tabPage46";
            this.tabPage46.Size = new System.Drawing.Size(179, 367);
            this.tabPage46.TabIndex = 7;
            this.tabPage46.Text = "ЛС8";
            this.tabPage46.UseVisualStyleBackColor = true;
            // 
            // _lsAndDgv8Gr1
            // 
            this._lsAndDgv8Gr1.AllowUserToAddRows = false;
            this._lsAndDgv8Gr1.AllowUserToDeleteRows = false;
            this._lsAndDgv8Gr1.AllowUserToResizeColumns = false;
            this._lsAndDgv8Gr1.AllowUserToResizeRows = false;
            this._lsAndDgv8Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsAndDgv8Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsAndDgv8Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsAndDgv8Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn31,
            this.dataGridViewComboBoxColumn30});
            this._lsAndDgv8Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsAndDgv8Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsAndDgv8Gr1.MultiSelect = false;
            this._lsAndDgv8Gr1.Name = "_lsAndDgv8Gr1";
            this._lsAndDgv8Gr1.RowHeadersVisible = false;
            this._lsAndDgv8Gr1.RowHeadersWidth = 51;
            this._lsAndDgv8Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsAndDgv8Gr1.RowsDefaultCellStyle = dataGridViewCellStyle27;
            this._lsAndDgv8Gr1.RowTemplate.Height = 20;
            this._lsAndDgv8Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsAndDgv8Gr1.ShowCellErrors = false;
            this._lsAndDgv8Gr1.ShowCellToolTips = false;
            this._lsAndDgv8Gr1.ShowEditingIcon = false;
            this._lsAndDgv8Gr1.ShowRowErrors = false;
            this._lsAndDgv8Gr1.Size = new System.Drawing.Size(179, 367);
            this._lsAndDgv8Gr1.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn31
            // 
            this.dataGridViewTextBoxColumn31.HeaderText = "№";
            this.dataGridViewTextBoxColumn31.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn31.Name = "dataGridViewTextBoxColumn31";
            this.dataGridViewTextBoxColumn31.ReadOnly = true;
            this.dataGridViewTextBoxColumn31.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn31.Width = 24;
            // 
            // dataGridViewComboBoxColumn30
            // 
            this.dataGridViewComboBoxColumn30.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn30.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn30.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn30.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn30.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn30.Name = "dataGridViewComboBoxColumn30";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this._setpointsComboBox);
            this.groupBox7.Location = new System.Drawing.Point(3, 3);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(103, 54);
            this.groupBox7.TabIndex = 2;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Группы уставок";
            // 
            // _setpointsComboBox
            // 
            this._setpointsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._setpointsComboBox.FormattingEnabled = true;
            this._setpointsComboBox.Location = new System.Drawing.Point(6, 19);
            this._setpointsComboBox.Name = "_setpointsComboBox";
            this._setpointsComboBox.Size = new System.Drawing.Size(88, 21);
            this._setpointsComboBox.TabIndex = 0;
            // 
            // _inputSignalsPage
            // 
            this._inputSignalsPage.Controls.Add(this.groupBox18);
            this._inputSignalsPage.Controls.Add(this.groupBox15);
            this._inputSignalsPage.Location = new System.Drawing.Point(4, 22);
            this._inputSignalsPage.Name = "_inputSignalsPage";
            this._inputSignalsPage.Size = new System.Drawing.Size(976, 553);
            this._inputSignalsPage.TabIndex = 7;
            this._inputSignalsPage.Text = "Входные сигналы";
            this._inputSignalsPage.UseVisualStyleBackColor = true;
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this._indComboBox);
            this.groupBox18.Location = new System.Drawing.Point(8, 193);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(254, 52);
            this.groupBox18.TabIndex = 4;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "Сброс блинкеров";
            // 
            // _indComboBox
            // 
            this._indComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._indComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._indComboBox.FormattingEnabled = true;
            this._indComboBox.Location = new System.Drawing.Point(76, 19);
            this._indComboBox.Name = "_indComboBox";
            this._indComboBox.Size = new System.Drawing.Size(167, 21);
            this._indComboBox.TabIndex = 0;
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.label80);
            this.groupBox15.Controls.Add(this._grUst6ComboBox);
            this.groupBox15.Controls.Add(this.label79);
            this.groupBox15.Controls.Add(this._grUst5ComboBox);
            this.groupBox15.Controls.Add(this.label78);
            this.groupBox15.Controls.Add(this._grUst4ComboBox);
            this.groupBox15.Controls.Add(this.label77);
            this.groupBox15.Controls.Add(this._grUst3ComboBox);
            this.groupBox15.Controls.Add(this.label76);
            this.groupBox15.Controls.Add(this._grUst2ComboBox);
            this.groupBox15.Controls.Add(this.label1);
            this.groupBox15.Controls.Add(this._grUst1ComboBox);
            this.groupBox15.Location = new System.Drawing.Point(8, 3);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(254, 184);
            this.groupBox15.TabIndex = 3;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Группы уставок";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(6, 156);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(51, 13);
            this.label80.TabIndex = 11;
            this.label80.Text = "Группа 6";
            // 
            // _grUst6ComboBox
            // 
            this._grUst6ComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._grUst6ComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._grUst6ComboBox.FormattingEnabled = true;
            this._grUst6ComboBox.Location = new System.Drawing.Point(76, 153);
            this._grUst6ComboBox.Name = "_grUst6ComboBox";
            this._grUst6ComboBox.Size = new System.Drawing.Size(167, 21);
            this._grUst6ComboBox.TabIndex = 10;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(6, 129);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(51, 13);
            this.label79.TabIndex = 9;
            this.label79.Text = "Группа 5";
            // 
            // _grUst5ComboBox
            // 
            this._grUst5ComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._grUst5ComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._grUst5ComboBox.FormattingEnabled = true;
            this._grUst5ComboBox.Location = new System.Drawing.Point(76, 126);
            this._grUst5ComboBox.Name = "_grUst5ComboBox";
            this._grUst5ComboBox.Size = new System.Drawing.Size(167, 21);
            this._grUst5ComboBox.TabIndex = 8;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(6, 102);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(51, 13);
            this.label78.TabIndex = 7;
            this.label78.Text = "Группа 4";
            // 
            // _grUst4ComboBox
            // 
            this._grUst4ComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._grUst4ComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._grUst4ComboBox.FormattingEnabled = true;
            this._grUst4ComboBox.Location = new System.Drawing.Point(76, 99);
            this._grUst4ComboBox.Name = "_grUst4ComboBox";
            this._grUst4ComboBox.Size = new System.Drawing.Size(167, 21);
            this._grUst4ComboBox.TabIndex = 6;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(6, 75);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(51, 13);
            this.label77.TabIndex = 5;
            this.label77.Text = "Группа 3";
            // 
            // _grUst3ComboBox
            // 
            this._grUst3ComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._grUst3ComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._grUst3ComboBox.FormattingEnabled = true;
            this._grUst3ComboBox.Location = new System.Drawing.Point(76, 72);
            this._grUst3ComboBox.Name = "_grUst3ComboBox";
            this._grUst3ComboBox.Size = new System.Drawing.Size(167, 21);
            this._grUst3ComboBox.TabIndex = 4;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(6, 48);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(51, 13);
            this.label76.TabIndex = 3;
            this.label76.Text = "Группа 2";
            // 
            // _grUst2ComboBox
            // 
            this._grUst2ComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._grUst2ComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._grUst2ComboBox.FormattingEnabled = true;
            this._grUst2ComboBox.Location = new System.Drawing.Point(76, 45);
            this._grUst2ComboBox.Name = "_grUst2ComboBox";
            this._grUst2ComboBox.Size = new System.Drawing.Size(167, 21);
            this._grUst2ComboBox.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Группа 1";
            // 
            // _grUst1ComboBox
            // 
            this._grUst1ComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._grUst1ComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._grUst1ComboBox.FormattingEnabled = true;
            this._grUst1ComboBox.Location = new System.Drawing.Point(76, 18);
            this._grUst1ComboBox.Name = "_grUst1ComboBox";
            this._grUst1ComboBox.Size = new System.Drawing.Size(167, 21);
            this._grUst1ComboBox.TabIndex = 0;
            // 
            // _outputSignalsPage
            // 
            this._outputSignalsPage.Controls.Add(this.groupBox13);
            this._outputSignalsPage.Controls.Add(this.groupBox31);
            this._outputSignalsPage.Controls.Add(this.groupBox175);
            this._outputSignalsPage.Controls.Add(this.groupBox176);
            this._outputSignalsPage.Location = new System.Drawing.Point(4, 22);
            this._outputSignalsPage.Name = "_outputSignalsPage";
            this._outputSignalsPage.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this._outputSignalsPage.Size = new System.Drawing.Size(976, 553);
            this._outputSignalsPage.TabIndex = 10;
            this._outputSignalsPage.Text = "Выходные сигналы";
            this._outputSignalsPage.UseVisualStyleBackColor = true;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this._resetAlarmCheckBox);
            this.groupBox13.Controls.Add(this._resetSystemCheckBox);
            this.groupBox13.Controls.Add(this.label110);
            this.groupBox13.Controls.Add(this.label52);
            this.groupBox13.Location = new System.Drawing.Point(407, 342);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(215, 143);
            this.groupBox13.TabIndex = 22;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Сброс индикаторов";
            // 
            // _resetAlarmCheckBox
            // 
            this._resetAlarmCheckBox.AutoSize = true;
            this._resetAlarmCheckBox.Location = new System.Drawing.Point(185, 46);
            this._resetAlarmCheckBox.Name = "_resetAlarmCheckBox";
            this._resetAlarmCheckBox.Size = new System.Drawing.Size(15, 14);
            this._resetAlarmCheckBox.TabIndex = 21;
            this._resetAlarmCheckBox.UseVisualStyleBackColor = true;
            // 
            // _resetSystemCheckBox
            // 
            this._resetSystemCheckBox.AutoSize = true;
            this._resetSystemCheckBox.Location = new System.Drawing.Point(185, 21);
            this._resetSystemCheckBox.Name = "_resetSystemCheckBox";
            this._resetSystemCheckBox.Size = new System.Drawing.Size(15, 14);
            this._resetSystemCheckBox.TabIndex = 19;
            this._resetSystemCheckBox.UseVisualStyleBackColor = true;
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Location = new System.Drawing.Point(6, 45);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(152, 13);
            this.label110.TabIndex = 20;
            this.label110.Text = "2. По входу в журнал аварий";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(6, 22);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(161, 13);
            this.label52.TabIndex = 19;
            this.label52.Text = "1. По входу в журнал системы";
            // 
            // groupBox31
            // 
            this.groupBox31.Controls.Add(this.label121);
            this.groupBox31.Controls.Add(this._fault4CheckBox);
            this.groupBox31.Controls.Add(this._fault3CheckBox);
            this.groupBox31.Controls.Add(this._fault2CheckBox);
            this.groupBox31.Controls.Add(this.label111);
            this.groupBox31.Controls.Add(this._fault1CheckBox);
            this.groupBox31.Controls.Add(this.label44);
            this.groupBox31.Controls.Add(this.label45);
            this.groupBox31.Controls.Add(this._impTB);
            this.groupBox31.Controls.Add(this.label46);
            this.groupBox31.Location = new System.Drawing.Point(628, 342);
            this.groupBox31.Name = "groupBox31";
            this.groupBox31.Size = new System.Drawing.Size(323, 143);
            this.groupBox31.TabIndex = 21;
            this.groupBox31.TabStop = false;
            this.groupBox31.Text = "Реле неисправность";
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Location = new System.Drawing.Point(6, 94);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(136, 13);
            this.label121.TabIndex = 19;
            this.label121.Text = "4. Неисправность логики";
            // 
            // _fault4CheckBox
            // 
            this._fault4CheckBox.AutoSize = true;
            this._fault4CheckBox.Location = new System.Drawing.Point(181, 93);
            this._fault4CheckBox.Name = "_fault4CheckBox";
            this._fault4CheckBox.Size = new System.Drawing.Size(15, 14);
            this._fault4CheckBox.TabIndex = 18;
            this._fault4CheckBox.UseVisualStyleBackColor = true;
            // 
            // _fault3CheckBox
            // 
            this._fault3CheckBox.AutoSize = true;
            this._fault3CheckBox.Location = new System.Drawing.Point(181, 69);
            this._fault3CheckBox.Name = "_fault3CheckBox";
            this._fault3CheckBox.Size = new System.Drawing.Size(15, 14);
            this._fault3CheckBox.TabIndex = 17;
            this._fault3CheckBox.UseVisualStyleBackColor = true;
            // 
            // _fault2CheckBox
            // 
            this._fault2CheckBox.AutoSize = true;
            this._fault2CheckBox.Location = new System.Drawing.Point(181, 45);
            this._fault2CheckBox.Name = "_fault2CheckBox";
            this._fault2CheckBox.Size = new System.Drawing.Size(15, 14);
            this._fault2CheckBox.TabIndex = 16;
            this._fault2CheckBox.UseVisualStyleBackColor = true;
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Location = new System.Drawing.Point(6, 70);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(169, 13);
            this.label111.TabIndex = 14;
            this.label111.Text = "3. Неисправность выключателя";
            // 
            // _fault1CheckBox
            // 
            this._fault1CheckBox.AutoSize = true;
            this._fault1CheckBox.Location = new System.Drawing.Point(181, 21);
            this._fault1CheckBox.Name = "_fault1CheckBox";
            this._fault1CheckBox.Size = new System.Drawing.Size(15, 14);
            this._fault1CheckBox.TabIndex = 15;
            this._fault1CheckBox.UseVisualStyleBackColor = true;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(6, 46);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(170, 13);
            this.label44.TabIndex = 12;
            this.label44.Text = "2. Программная неисправность";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(6, 22);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(159, 13);
            this.label45.TabIndex = 11;
            this.label45.Text = "1. Аппаратная неисправность";
            // 
            // _impTB
            // 
            this._impTB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._impTB.Location = new System.Drawing.Point(92, 113);
            this._impTB.Name = "_impTB";
            this._impTB.Size = new System.Drawing.Size(104, 20);
            this._impTB.TabIndex = 7;
            this._impTB.Text = "0";
            this._impTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(7, 115);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(64, 13);
            this.label46.TabIndex = 3;
            this.label46.Text = "Твозвр, мс";
            // 
            // groupBox175
            // 
            this.groupBox175.Controls.Add(this._outputIndicatorsGrid);
            this.groupBox175.Location = new System.Drawing.Point(407, 6);
            this.groupBox175.Name = "groupBox175";
            this.groupBox175.Size = new System.Drawing.Size(544, 330);
            this.groupBox175.TabIndex = 20;
            this.groupBox175.TabStop = false;
            this.groupBox175.Text = "Индикаторы";
            // 
            // _outputIndicatorsGrid
            // 
            this._outputIndicatorsGrid.AllowUserToAddRows = false;
            this._outputIndicatorsGrid.AllowUserToDeleteRows = false;
            this._outputIndicatorsGrid.AllowUserToResizeColumns = false;
            this._outputIndicatorsGrid.AllowUserToResizeRows = false;
            this._outputIndicatorsGrid.BackgroundColor = System.Drawing.Color.White;
            this._outputIndicatorsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._outputIndicatorsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._outIndNumberCol,
            this._outIndTypeCol,
            this._outIndSignalCol,
            this._out1IndSignalCol,
            this._outIndSignal2Col});
            this._outputIndicatorsGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._outputIndicatorsGrid.Location = new System.Drawing.Point(3, 16);
            this._outputIndicatorsGrid.Name = "_outputIndicatorsGrid";
            this._outputIndicatorsGrid.RowHeadersVisible = false;
            this._outputIndicatorsGrid.RowHeadersWidth = 51;
            this._outputIndicatorsGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._outputIndicatorsGrid.RowsDefaultCellStyle = dataGridViewCellStyle28;
            this._outputIndicatorsGrid.RowTemplate.Height = 24;
            this._outputIndicatorsGrid.ShowCellErrors = false;
            this._outputIndicatorsGrid.ShowRowErrors = false;
            this._outputIndicatorsGrid.Size = new System.Drawing.Size(538, 311);
            this._outputIndicatorsGrid.TabIndex = 0;
            // 
            // _outIndNumberCol
            // 
            this._outIndNumberCol.HeaderText = "№";
            this._outIndNumberCol.MinimumWidth = 6;
            this._outIndNumberCol.Name = "_outIndNumberCol";
            this._outIndNumberCol.ReadOnly = true;
            this._outIndNumberCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._outIndNumberCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._outIndNumberCol.Width = 25;
            // 
            // _outIndTypeCol
            // 
            this._outIndTypeCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._outIndTypeCol.HeaderText = "Тип";
            this._outIndTypeCol.MinimumWidth = 6;
            this._outIndTypeCol.Name = "_outIndTypeCol";
            this._outIndTypeCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._outIndTypeCol.Width = 125;
            // 
            // _outIndSignalCol
            // 
            this._outIndSignalCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._outIndSignalCol.HeaderText = "Сигнал \"зеленый\"";
            this._outIndSignalCol.MinimumWidth = 6;
            this._outIndSignalCol.Name = "_outIndSignalCol";
            this._outIndSignalCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._outIndSignalCol.Width = 130;
            // 
            // _out1IndSignalCol
            // 
            this._out1IndSignalCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._out1IndSignalCol.HeaderText = "Сигнал \"красный\"";
            this._out1IndSignalCol.MinimumWidth = 6;
            this._out1IndSignalCol.Name = "_out1IndSignalCol";
            this._out1IndSignalCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._out1IndSignalCol.Width = 130;
            // 
            // _outIndSignal2Col
            // 
            this._outIndSignal2Col.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._outIndSignal2Col.HeaderText = "Режим работы";
            this._outIndSignal2Col.MinimumWidth = 6;
            this._outIndSignal2Col.Name = "_outIndSignal2Col";
            this._outIndSignal2Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._outIndSignal2Col.Width = 150;
            // 
            // groupBox176
            // 
            this.groupBox176.Controls.Add(this._outputReleGrid);
            this.groupBox176.Location = new System.Drawing.Point(8, 6);
            this.groupBox176.Name = "groupBox176";
            this.groupBox176.Size = new System.Drawing.Size(393, 547);
            this.groupBox176.TabIndex = 19;
            this.groupBox176.TabStop = false;
            this.groupBox176.Text = "Выходные реле";
            // 
            // _outputReleGrid
            // 
            this._outputReleGrid.AllowUserToAddRows = false;
            this._outputReleGrid.AllowUserToDeleteRows = false;
            this._outputReleGrid.AllowUserToResizeColumns = false;
            this._outputReleGrid.AllowUserToResizeRows = false;
            this._outputReleGrid.BackgroundColor = System.Drawing.Color.White;
            this._outputReleGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._outputReleGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._releNumberCol,
            this._releTypeCol,
            this._releSignalCol,
            this._releWaitCol});
            this._outputReleGrid.Location = new System.Drawing.Point(9, 13);
            this._outputReleGrid.Name = "_outputReleGrid";
            this._outputReleGrid.RowHeadersVisible = false;
            this._outputReleGrid.RowHeadersWidth = 51;
            this._outputReleGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._outputReleGrid.RowsDefaultCellStyle = dataGridViewCellStyle29;
            this._outputReleGrid.RowTemplate.Height = 24;
            this._outputReleGrid.ShowCellErrors = false;
            this._outputReleGrid.ShowRowErrors = false;
            this._outputReleGrid.Size = new System.Drawing.Size(376, 526);
            this._outputReleGrid.TabIndex = 0;
            // 
            // _releNumberCol
            // 
            this._releNumberCol.HeaderText = "№";
            this._releNumberCol.MinimumWidth = 6;
            this._releNumberCol.Name = "_releNumberCol";
            this._releNumberCol.ReadOnly = true;
            this._releNumberCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._releNumberCol.Width = 25;
            // 
            // _releTypeCol
            // 
            this._releTypeCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._releTypeCol.HeaderText = "Тип";
            this._releTypeCol.MinimumWidth = 6;
            this._releTypeCol.Name = "_releTypeCol";
            this._releTypeCol.Width = 120;
            // 
            // _releSignalCol
            // 
            this._releSignalCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._releSignalCol.HeaderText = "Сигнал";
            this._releSignalCol.MinimumWidth = 6;
            this._releSignalCol.Name = "_releSignalCol";
            this._releSignalCol.Width = 120;
            // 
            // _releWaitCol
            // 
            this._releWaitCol.HeaderText = "Твозвр, мс";
            this._releWaitCol.MinimumWidth = 6;
            this._releWaitCol.Name = "_releWaitCol";
            this._releWaitCol.Width = 90;
            // 
            // _systemPage
            // 
            this._systemPage.Controls.Add(this.groupBox11);
            this._systemPage.Controls.Add(this.groupBox3);
            this._systemPage.Location = new System.Drawing.Point(4, 22);
            this._systemPage.Name = "_systemPage";
            this._systemPage.Size = new System.Drawing.Size(976, 553);
            this._systemPage.TabIndex = 8;
            this._systemPage.Text = "Осциллограф";
            this._systemPage.UseVisualStyleBackColor = true;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this._oscChannelsGrid);
            this.groupBox11.Location = new System.Drawing.Point(291, 3);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(306, 547);
            this.groupBox11.TabIndex = 4;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Программируемые дискретные каналы";
            // 
            // _oscChannelsGrid
            // 
            this._oscChannelsGrid.AllowUserToAddRows = false;
            this._oscChannelsGrid.AllowUserToDeleteRows = false;
            this._oscChannelsGrid.AllowUserToResizeColumns = false;
            this._oscChannelsGrid.AllowUserToResizeRows = false;
            this._oscChannelsGrid.BackgroundColor = System.Drawing.Color.White;
            this._oscChannelsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._oscChannelsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewComboBoxColumn6,
            this.dataGridViewComboBoxColumn7});
            this._oscChannelsGrid.Location = new System.Drawing.Point(6, 19);
            this._oscChannelsGrid.Name = "_oscChannelsGrid";
            this._oscChannelsGrid.RowHeadersVisible = false;
            this._oscChannelsGrid.RowHeadersWidth = 51;
            this._oscChannelsGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._oscChannelsGrid.RowTemplate.Height = 24;
            this._oscChannelsGrid.ShowCellErrors = false;
            this._oscChannelsGrid.ShowRowErrors = false;
            this._oscChannelsGrid.Size = new System.Drawing.Size(293, 522);
            this._oscChannelsGrid.TabIndex = 28;
            // 
            // dataGridViewTextBoxColumn4
            // 
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle30.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle30.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle30.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle30.SelectionForeColor = System.Drawing.Color.White;
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle30;
            this.dataGridViewTextBoxColumn4.Frozen = true;
            this.dataGridViewTextBoxColumn4.HeaderText = "Канал";
            this.dataGridViewTextBoxColumn4.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn4.Width = 70;
            // 
            // dataGridViewComboBoxColumn6
            // 
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dataGridViewComboBoxColumn6.DefaultCellStyle = dataGridViewCellStyle31;
            this.dataGridViewComboBoxColumn6.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn6.HeaderText = "База";
            this.dataGridViewComboBoxColumn6.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn6.Name = "dataGridViewComboBoxColumn6";
            this.dataGridViewComboBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn6.Width = 50;
            // 
            // dataGridViewComboBoxColumn7
            // 
            this.dataGridViewComboBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dataGridViewComboBoxColumn7.DefaultCellStyle = dataGridViewCellStyle32;
            this.dataGridViewComboBoxColumn7.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn7.HeaderText = "Сигнал";
            this.dataGridViewComboBoxColumn7.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn7.Name = "dataGridViewComboBoxColumn7";
            this.dataGridViewComboBoxColumn7.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.oscStartCb);
            this.groupBox3.Controls.Add(this.label65);
            this.groupBox3.Controls.Add(this._oscSizeTextBox);
            this.groupBox3.Controls.Add(this._oscWriteLength);
            this.groupBox3.Controls.Add(this._oscFix);
            this.groupBox3.Controls.Add(this._oscLength);
            this.groupBox3.Controls.Add(this.label41);
            this.groupBox3.Controls.Add(this.label42);
            this.groupBox3.Controls.Add(this.label43);
            this.groupBox3.Location = new System.Drawing.Point(8, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(277, 124);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Осциллограф";
            // 
            // oscStartCb
            // 
            this.oscStartCb.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.oscStartCb.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.oscStartCb.FormattingEnabled = true;
            this.oscStartCb.Location = new System.Drawing.Point(134, 91);
            this.oscStartCb.Name = "oscStartCb";
            this.oscStartCb.Size = new System.Drawing.Size(121, 21);
            this.oscStartCb.TabIndex = 30;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(18, 94);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(87, 13);
            this.label65.TabIndex = 29;
            this.label65.Text = "Вход пуска осц.";
            // 
            // _oscSizeTextBox
            // 
            this._oscSizeTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._oscSizeTextBox.Location = new System.Drawing.Point(184, 23);
            this._oscSizeTextBox.Name = "_oscSizeTextBox";
            this._oscSizeTextBox.ReadOnly = true;
            this._oscSizeTextBox.Size = new System.Drawing.Size(71, 20);
            this._oscSizeTextBox.TabIndex = 28;
            this._oscSizeTextBox.Tag = "3000000";
            this._oscSizeTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _oscWriteLength
            // 
            this._oscWriteLength.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._oscWriteLength.Location = new System.Drawing.Point(134, 44);
            this._oscWriteLength.Name = "_oscWriteLength";
            this._oscWriteLength.Size = new System.Drawing.Size(121, 20);
            this._oscWriteLength.TabIndex = 23;
            this._oscWriteLength.Tag = "3000000";
            this._oscWriteLength.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _oscFix
            // 
            this._oscFix.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._oscFix.FormattingEnabled = true;
            this._oscFix.Location = new System.Drawing.Point(134, 64);
            this._oscFix.Name = "_oscFix";
            this._oscFix.Size = new System.Drawing.Size(121, 21);
            this._oscFix.TabIndex = 13;
            // 
            // _oscLength
            // 
            this._oscLength.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._oscLength.FormattingEnabled = true;
            this._oscLength.Location = new System.Drawing.Point(134, 23);
            this._oscLength.Name = "_oscLength";
            this._oscLength.Size = new System.Drawing.Size(44, 21);
            this._oscLength.TabIndex = 12;
            this._oscLength.SelectedIndexChanged += new System.EventHandler(this._oscLength_SelectedIndexChanged);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(18, 67);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(66, 13);
            this.label41.TabIndex = 2;
            this.label41.Text = "Фиксац. по";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(18, 46);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(113, 13);
            this.label42.TabIndex = 1;
            this.label42.Text = "Длит. предзаписи, %";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(18, 26);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(66, 13);
            this.label43.TabIndex = 0;
            this.label43.Text = "Размер, мс";
            // 
            // _automatPage
            // 
            this._automatPage.Controls.Add(this.groupBox32);
            this._automatPage.Controls.Add(this.groupBox33);
            this._automatPage.Location = new System.Drawing.Point(4, 22);
            this._automatPage.Name = "_automatPage";
            this._automatPage.Size = new System.Drawing.Size(976, 553);
            this._automatPage.TabIndex = 9;
            this._automatPage.Text = "Выключатель";
            this._automatPage.UseVisualStyleBackColor = true;
            // 
            // groupBox32
            // 
            this.groupBox32.Controls.Add(this.label96);
            this.groupBox32.Controls.Add(this._comandOtkl);
            this.groupBox32.Controls.Add(this._controlSolenoidCombo);
            this.groupBox32.Controls.Add(this._switchKontCep);
            this.groupBox32.Controls.Add(this._switchTUskor);
            this.groupBox32.Controls.Add(this._switchImp);
            this.groupBox32.Controls.Add(this._switchBlock);
            this.groupBox32.Controls.Add(this._switchError);
            this.groupBox32.Controls.Add(this._switchOn);
            this.groupBox32.Controls.Add(this.label51);
            this.groupBox32.Controls.Add(this._switchOff);
            this.groupBox32.Controls.Add(this.label97);
            this.groupBox32.Controls.Add(this.label98);
            this.groupBox32.Controls.Add(this.label99);
            this.groupBox32.Controls.Add(this.label93);
            this.groupBox32.Controls.Add(this.label90);
            this.groupBox32.Controls.Add(this.label89);
            this.groupBox32.Controls.Add(this.label88);
            this.groupBox32.Location = new System.Drawing.Point(8, 3);
            this.groupBox32.Name = "groupBox32";
            this.groupBox32.Size = new System.Drawing.Size(253, 228);
            this.groupBox32.TabIndex = 14;
            this.groupBox32.TabStop = false;
            this.groupBox32.Text = "Выключатель";
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(6, 117);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(115, 13);
            this.label96.TabIndex = 16;
            this.label96.Text = "Команда отключения";
            // 
            // _comandOtkl
            // 
            this._comandOtkl.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._comandOtkl.FormattingEnabled = true;
            this._comandOtkl.Location = new System.Drawing.Point(142, 114);
            this._comandOtkl.Name = "_comandOtkl";
            this._comandOtkl.Size = new System.Drawing.Size(105, 21);
            this._comandOtkl.TabIndex = 27;
            // 
            // _controlSolenoidCombo
            // 
            this._controlSolenoidCombo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._controlSolenoidCombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._controlSolenoidCombo.FormattingEnabled = true;
            this._controlSolenoidCombo.Location = new System.Drawing.Point(142, 186);
            this._controlSolenoidCombo.Name = "_controlSolenoidCombo";
            this._controlSolenoidCombo.Size = new System.Drawing.Size(105, 21);
            this._controlSolenoidCombo.TabIndex = 26;
            // 
            // _switchKontCep
            // 
            this._switchKontCep.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switchKontCep.FormattingEnabled = true;
            this._switchKontCep.Location = new System.Drawing.Point(142, 153);
            this._switchKontCep.Name = "_switchKontCep";
            this._switchKontCep.Size = new System.Drawing.Size(105, 21);
            this._switchKontCep.TabIndex = 26;
            // 
            // _switchTUskor
            // 
            this._switchTUskor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._switchTUskor.Location = new System.Drawing.Point(142, 134);
            this._switchTUskor.Name = "_switchTUskor";
            this._switchTUskor.Size = new System.Drawing.Size(105, 20);
            this._switchTUskor.TabIndex = 25;
            this._switchTUskor.Tag = "3276700";
            this._switchTUskor.Text = "20";
            this._switchTUskor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _switchImp
            // 
            this._switchImp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._switchImp.Location = new System.Drawing.Point(142, 95);
            this._switchImp.Name = "_switchImp";
            this._switchImp.Size = new System.Drawing.Size(105, 20);
            this._switchImp.TabIndex = 24;
            this._switchImp.Tag = "3276700";
            this._switchImp.Text = "0";
            this._switchImp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _switchBlock
            // 
            this._switchBlock.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._switchBlock.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._switchBlock.BackColor = System.Drawing.SystemColors.Window;
            this._switchBlock.FormattingEnabled = true;
            this._switchBlock.Location = new System.Drawing.Point(142, 75);
            this._switchBlock.Name = "_switchBlock";
            this._switchBlock.Size = new System.Drawing.Size(105, 21);
            this._switchBlock.TabIndex = 21;
            // 
            // _switchError
            // 
            this._switchError.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._switchError.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._switchError.FormattingEnabled = true;
            this._switchError.Location = new System.Drawing.Point(142, 55);
            this._switchError.Name = "_switchError";
            this._switchError.Size = new System.Drawing.Size(105, 21);
            this._switchError.TabIndex = 20;
            // 
            // _switchOn
            // 
            this._switchOn.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._switchOn.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._switchOn.FormattingEnabled = true;
            this._switchOn.Location = new System.Drawing.Point(142, 35);
            this._switchOn.Name = "_switchOn";
            this._switchOn.Size = new System.Drawing.Size(105, 21);
            this._switchOn.TabIndex = 19;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(6, 184);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(124, 26);
            this.label51.TabIndex = 17;
            this.label51.Text = "Контроль второго\r\nсоленоида отключения";
            // 
            // _switchOff
            // 
            this._switchOff.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._switchOff.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._switchOff.FormattingEnabled = true;
            this._switchOff.Location = new System.Drawing.Point(142, 15);
            this._switchOff.Name = "_switchOff";
            this._switchOff.Size = new System.Drawing.Size(105, 21);
            this._switchOff.TabIndex = 18;
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(6, 154);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(88, 26);
            this.label97.TabIndex = 17;
            this.label97.Text = "Контроль цепей\r\nуправления";
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(6, 136);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(59, 13);
            this.label98.TabIndex = 16;
            this.label98.Text = "tускор, мс";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(6, 98);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(139, 13);
            this.label99.TabIndex = 15;
            this.label99.Text = "Импульс сигнала упр., мс";
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Location = new System.Drawing.Point(6, 78);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(94, 13);
            this.label93.TabIndex = 12;
            this.label93.Text = "Вход блокировки";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Location = new System.Drawing.Point(6, 58);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(111, 13);
            this.label90.TabIndex = 11;
            this.label90.Text = "Вход неисправности";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(6, 38);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(124, 13);
            this.label89.TabIndex = 10;
            this.label89.Text = "Состояние \"Включено\"";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(6, 18);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(130, 13);
            this.label88.TabIndex = 9;
            this.label88.Text = "Состояние \"Отключено\"";
            // 
            // groupBox33
            // 
            this.groupBox33.Controls.Add(this._switchSDTU);
            this.groupBox33.Controls.Add(this._switchVnesh);
            this.groupBox33.Controls.Add(this._switchKey);
            this.groupBox33.Controls.Add(this._switchButtons);
            this.groupBox33.Controls.Add(this._switchVneshOff);
            this.groupBox33.Controls.Add(this._switchVneshOn);
            this.groupBox33.Controls.Add(this._switchKeyOff);
            this.groupBox33.Controls.Add(this._switchKeyOn);
            this.groupBox33.Controls.Add(this.label101);
            this.groupBox33.Controls.Add(this.label102);
            this.groupBox33.Controls.Add(this.label103);
            this.groupBox33.Controls.Add(this.label104);
            this.groupBox33.Controls.Add(this.label105);
            this.groupBox33.Controls.Add(this.label106);
            this.groupBox33.Controls.Add(this.label107);
            this.groupBox33.Controls.Add(this.label108);
            this.groupBox33.Location = new System.Drawing.Point(8, 237);
            this.groupBox33.Name = "groupBox33";
            this.groupBox33.Size = new System.Drawing.Size(253, 184);
            this.groupBox33.TabIndex = 15;
            this.groupBox33.TabStop = false;
            this.groupBox33.Text = "Управление";
            // 
            // _switchSDTU
            // 
            this._switchSDTU.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switchSDTU.FormattingEnabled = true;
            this._switchSDTU.Location = new System.Drawing.Point(142, 154);
            this._switchSDTU.Name = "_switchSDTU";
            this._switchSDTU.Size = new System.Drawing.Size(105, 21);
            this._switchSDTU.TabIndex = 34;
            // 
            // _switchVnesh
            // 
            this._switchVnesh.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switchVnesh.FormattingEnabled = true;
            this._switchVnesh.Location = new System.Drawing.Point(142, 134);
            this._switchVnesh.Name = "_switchVnesh";
            this._switchVnesh.Size = new System.Drawing.Size(105, 21);
            this._switchVnesh.TabIndex = 33;
            // 
            // _switchKey
            // 
            this._switchKey.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switchKey.FormattingEnabled = true;
            this._switchKey.Location = new System.Drawing.Point(142, 115);
            this._switchKey.Name = "_switchKey";
            this._switchKey.Size = new System.Drawing.Size(105, 21);
            this._switchKey.TabIndex = 32;
            // 
            // _switchButtons
            // 
            this._switchButtons.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switchButtons.FormattingEnabled = true;
            this._switchButtons.Location = new System.Drawing.Point(142, 95);
            this._switchButtons.Name = "_switchButtons";
            this._switchButtons.Size = new System.Drawing.Size(105, 21);
            this._switchButtons.TabIndex = 31;
            // 
            // _switchVneshOff
            // 
            this._switchVneshOff.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._switchVneshOff.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._switchVneshOff.FormattingEnabled = true;
            this._switchVneshOff.Location = new System.Drawing.Point(142, 75);
            this._switchVneshOff.Name = "_switchVneshOff";
            this._switchVneshOff.Size = new System.Drawing.Size(105, 21);
            this._switchVneshOff.TabIndex = 30;
            // 
            // _switchVneshOn
            // 
            this._switchVneshOn.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._switchVneshOn.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._switchVneshOn.FormattingEnabled = true;
            this._switchVneshOn.Location = new System.Drawing.Point(142, 55);
            this._switchVneshOn.Name = "_switchVneshOn";
            this._switchVneshOn.Size = new System.Drawing.Size(105, 21);
            this._switchVneshOn.TabIndex = 29;
            // 
            // _switchKeyOff
            // 
            this._switchKeyOff.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._switchKeyOff.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._switchKeyOff.FormattingEnabled = true;
            this._switchKeyOff.Location = new System.Drawing.Point(142, 35);
            this._switchKeyOff.Name = "_switchKeyOff";
            this._switchKeyOff.Size = new System.Drawing.Size(105, 21);
            this._switchKeyOff.TabIndex = 28;
            // 
            // _switchKeyOn
            // 
            this._switchKeyOn.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._switchKeyOn.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._switchKeyOn.FormattingEnabled = true;
            this._switchKeyOn.Location = new System.Drawing.Point(142, 15);
            this._switchKeyOn.Name = "_switchKeyOn";
            this._switchKeyOn.Size = new System.Drawing.Size(105, 21);
            this._switchKeyOn.TabIndex = 27;
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(6, 157);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(54, 13);
            this.label101.TabIndex = 25;
            this.label101.Text = "От СДТУ";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Location = new System.Drawing.Point(6, 137);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(52, 13);
            this.label102.TabIndex = 24;
            this.label102.Text = "Внешнее";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(6, 118);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(54, 13);
            this.label103.TabIndex = 23;
            this.label103.Text = "От ключа";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Location = new System.Drawing.Point(6, 98);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(96, 13);
            this.label104.TabIndex = 22;
            this.label104.Text = "От кнопок пульта";
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Location = new System.Drawing.Point(6, 78);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(108, 13);
            this.label105.TabIndex = 21;
            this.label105.Text = "Внешнее отключить";
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Location = new System.Drawing.Point(6, 58);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(103, 13);
            this.label106.TabIndex = 20;
            this.label106.Text = "Внешнее включить";
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Location = new System.Drawing.Point(6, 38);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(89, 13);
            this.label107.TabIndex = 19;
            this.label107.Text = "Ключ отключить";
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Location = new System.Drawing.Point(6, 18);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(84, 13);
            this.label108.TabIndex = 18;
            this.label108.Text = "Ключ включить";
            // 
            // _ethernetPage
            // 
            this._ethernetPage.Controls.Add(this.groupBox51);
            this._ethernetPage.Location = new System.Drawing.Point(4, 22);
            this._ethernetPage.Name = "_ethernetPage";
            this._ethernetPage.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this._ethernetPage.Size = new System.Drawing.Size(976, 553);
            this._ethernetPage.TabIndex = 11;
            this._ethernetPage.Text = "Конфигурация Ethernet";
            this._ethernetPage.UseVisualStyleBackColor = true;
            // 
            // groupBox51
            // 
            this.groupBox51.Controls.Add(this._ipLo1);
            this.groupBox51.Controls.Add(this._ipLo2);
            this.groupBox51.Controls.Add(this._ipHi1);
            this.groupBox51.Controls.Add(this._ipHi2);
            this.groupBox51.Controls.Add(this.label214);
            this.groupBox51.Controls.Add(this.label213);
            this.groupBox51.Controls.Add(this.label212);
            this.groupBox51.Controls.Add(this.label179);
            this.groupBox51.Location = new System.Drawing.Point(3, 3);
            this.groupBox51.Name = "groupBox51";
            this.groupBox51.Size = new System.Drawing.Size(261, 53);
            this.groupBox51.TabIndex = 1;
            this.groupBox51.TabStop = false;
            this.groupBox51.Text = "Конфигурация Ethernet";
            // 
            // _ipLo1
            // 
            this._ipLo1.Location = new System.Drawing.Point(215, 19);
            this._ipLo1.Name = "_ipLo1";
            this._ipLo1.Size = new System.Drawing.Size(38, 20);
            this._ipLo1.TabIndex = 3;
            // 
            // _ipLo2
            // 
            this._ipLo2.Location = new System.Drawing.Point(163, 19);
            this._ipLo2.Name = "_ipLo2";
            this._ipLo2.Size = new System.Drawing.Size(38, 20);
            this._ipLo2.TabIndex = 2;
            // 
            // _ipHi1
            // 
            this._ipHi1.Location = new System.Drawing.Point(112, 19);
            this._ipHi1.Name = "_ipHi1";
            this._ipHi1.Size = new System.Drawing.Size(38, 20);
            this._ipHi1.TabIndex = 1;
            // 
            // _ipHi2
            // 
            this._ipHi2.Location = new System.Drawing.Point(60, 19);
            this._ipHi2.Name = "_ipHi2";
            this._ipHi2.Size = new System.Drawing.Size(38, 20);
            this._ipHi2.TabIndex = 0;
            // 
            // label214
            // 
            this.label214.AutoSize = true;
            this.label214.Location = new System.Drawing.Point(152, 26);
            this.label214.Name = "label214";
            this.label214.Size = new System.Drawing.Size(10, 13);
            this.label214.TabIndex = 9;
            this.label214.Text = ".";
            // 
            // label213
            // 
            this.label213.AutoSize = true;
            this.label213.Location = new System.Drawing.Point(203, 26);
            this.label213.Name = "label213";
            this.label213.Size = new System.Drawing.Size(10, 13);
            this.label213.TabIndex = 9;
            this.label213.Text = ".";
            // 
            // label212
            // 
            this.label212.AutoSize = true;
            this.label212.Location = new System.Drawing.Point(100, 26);
            this.label212.Name = "label212";
            this.label212.Size = new System.Drawing.Size(10, 13);
            this.label212.TabIndex = 9;
            this.label212.Text = ".";
            // 
            // label179
            // 
            this.label179.AutoSize = true;
            this.label179.Location = new System.Drawing.Point(6, 22);
            this.label179.Name = "label179";
            this.label179.Size = new System.Drawing.Size(50, 13);
            this.label179.TabIndex = 9;
            this.label179.Text = "IP-адрес";
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Location = new System.Drawing.Point(6, 116);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(169, 13);
            this.label109.TabIndex = 19;
            this.label109.Text = "5. Неисправность выключателя";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this._saveToXmlButton);
            this.panel1.Controls.Add(this._resetSetpointsButton);
            this.panel1.Controls.Add(this._writeConfigBut);
            this.panel1.Controls.Add(this.statusStrip1);
            this.panel1.Controls.Add(this._readConfigBut);
            this.panel1.Controls.Add(this._saveConfigBut);
            this.panel1.Controls.Add(this._loadConfigBut);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 582);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(984, 52);
            this.panel1.TabIndex = 33;
            // 
            // _saveToXmlButton
            // 
            this._saveToXmlButton.AllowDrop = true;
            this._saveToXmlButton.AutoSize = true;
            this._saveToXmlButton.Location = new System.Drawing.Point(868, 5);
            this._saveToXmlButton.Name = "_saveToXmlButton";
            this._saveToXmlButton.Size = new System.Drawing.Size(112, 23);
            this._saveToXmlButton.TabIndex = 0;
            this._saveToXmlButton.Text = "Сохранить в HTML";
            this._saveToXmlButton.UseVisualStyleBackColor = true;
            this._saveToXmlButton.Click += new System.EventHandler(this._saveToXmlButton_Click);
            // 
            // _resetSetpointsButton
            // 
            this._resetSetpointsButton.AutoSize = true;
            this._resetSetpointsButton.Location = new System.Drawing.Point(294, 5);
            this._resetSetpointsButton.Name = "_resetSetpointsButton";
            this._resetSetpointsButton.Size = new System.Drawing.Size(108, 23);
            this._resetSetpointsButton.TabIndex = 3;
            this._resetSetpointsButton.Text = "Обнулить уставки";
            this._resetSetpointsButton.UseVisualStyleBackColor = true;
            this._resetSetpointsButton.Click += new System.EventHandler(this._resetSetpointsButton_Click);
            // 
            // _writeConfigBut
            // 
            this._writeConfigBut.AutoSize = true;
            this._writeConfigBut.Location = new System.Drawing.Point(154, 5);
            this._writeConfigBut.Name = "_writeConfigBut";
            this._writeConfigBut.Size = new System.Drawing.Size(134, 23);
            this._writeConfigBut.TabIndex = 4;
            this._writeConfigBut.Text = "Записать в устройство";
            this.toolTip1.SetToolTip(this._writeConfigBut, "Записать конфигурацию в устройство (CTRL+W)");
            this._writeConfigBut.UseVisualStyleBackColor = true;
            this._writeConfigBut.Click += new System.EventHandler(this._writeConfigBut_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this._progressBar,
            this._statusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 30);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.statusStrip1.Size = new System.Drawing.Size(984, 22);
            this.statusStrip1.TabIndex = 26;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // _progressBar
            // 
            this._progressBar.Maximum = 90;
            this._progressBar.Name = "_progressBar";
            this._progressBar.Size = new System.Drawing.Size(100, 16);
            this._progressBar.Step = 1;
            // 
            // _statusLabel
            // 
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // _readConfigBut
            // 
            this._readConfigBut.AutoSize = true;
            this._readConfigBut.Location = new System.Drawing.Point(3, 5);
            this._readConfigBut.Name = "_readConfigBut";
            this._readConfigBut.Size = new System.Drawing.Size(145, 23);
            this._readConfigBut.TabIndex = 5;
            this._readConfigBut.Text = "Прочитать из устройства";
            this.toolTip1.SetToolTip(this._readConfigBut, "Прочитать конфигурацию из устройства (CTRL+R)");
            this._readConfigBut.UseVisualStyleBackColor = true;
            this._readConfigBut.Click += new System.EventHandler(this._readConfigBut_Click);
            // 
            // _saveConfigBut
            // 
            this._saveConfigBut.AllowDrop = true;
            this._saveConfigBut.AutoSize = true;
            this._saveConfigBut.Location = new System.Drawing.Point(754, 5);
            this._saveConfigBut.Name = "_saveConfigBut";
            this._saveConfigBut.Size = new System.Drawing.Size(108, 23);
            this._saveConfigBut.TabIndex = 1;
            this._saveConfigBut.Text = "Сохранить в файл";
            this.toolTip1.SetToolTip(this._saveConfigBut, "Сохранить конфигурацию в файл (CTRL+S)");
            this._saveConfigBut.UseVisualStyleBackColor = true;
            this._saveConfigBut.Click += new System.EventHandler(this._saveConfigBut_Click);
            // 
            // _loadConfigBut
            // 
            this._loadConfigBut.AutoSize = true;
            this._loadConfigBut.Location = new System.Drawing.Point(629, 5);
            this._loadConfigBut.Name = "_loadConfigBut";
            this._loadConfigBut.Size = new System.Drawing.Size(119, 23);
            this._loadConfigBut.TabIndex = 2;
            this._loadConfigBut.Text = "Загрузить из файла";
            this.toolTip1.SetToolTip(this._loadConfigBut, "Загрузить конфигурацию из файла (CTRL+O)");
            this._loadConfigBut.UseVisualStyleBackColor = true;
            this._loadConfigBut.Click += new System.EventHandler(this._loadConfigBut_Click);
            // 
            // _saveConfigurationDlg
            // 
            this._saveConfigurationDlg.DefaultExt = "xml";
            this._saveConfigurationDlg.Filter = "Уставки МР761 ОБР (*.xml) | *.xml";
            this._saveConfigurationDlg.Title = "Сохранить  уставки для МР761 ОБР";
            // 
            // _openConfigurationDlg
            // 
            this._openConfigurationDlg.DefaultExt = "xml";
            this._openConfigurationDlg.Filter = "Уставки МР761 ОБР (*.xml) | *.xml";
            this._openConfigurationDlg.RestoreDirectory = true;
            this._openConfigurationDlg.Title = "Открыть уставки для МР761 ОБР";
            // 
            // _saveXmlDialog
            // 
            this._saveXmlDialog.Filter = "(*.html) | *.html";
            // 
            // _externalDifSbrosColumn
            // 
            dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle33.NullValue = false;
            this._externalDifSbrosColumn.DefaultCellStyle = dataGridViewCellStyle33;
            this._externalDifSbrosColumn.HeaderText = "Сброс";
            this._externalDifSbrosColumn.MinimumWidth = 6;
            this._externalDifSbrosColumn.Name = "_externalDifSbrosColumn";
            this._externalDifSbrosColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifSbrosColumn.Width = 50;
            // 
            // _externalDifAPVRetColumn
            // 
            dataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle34.NullValue = false;
            this._externalDifAPVRetColumn.DefaultCellStyle = dataGridViewCellStyle34;
            this._externalDifAPVRetColumn.HeaderText = "АПВ возвр.";
            this._externalDifAPVRetColumn.MinimumWidth = 6;
            this._externalDifAPVRetColumn.Name = "_externalDifAPVRetColumn";
            this._externalDifAPVRetColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifAPVRetColumn.Width = 75;
            // 
            // _externalDifAPVColumn
            // 
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle35.NullValue = false;
            this._externalDifAPVColumn.DefaultCellStyle = dataGridViewCellStyle35;
            this._externalDifAPVColumn.HeaderText = "АПВ";
            this._externalDifAPVColumn.MinimumWidth = 6;
            this._externalDifAPVColumn.Name = "_externalDifAPVColumn";
            this._externalDifAPVColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifAPVColumn.Width = 50;
            // 
            // _externalDifUROVColumn
            // 
            dataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle36.NullValue = false;
            this._externalDifUROVColumn.DefaultCellStyle = dataGridViewCellStyle36;
            this._externalDifUROVColumn.HeaderText = "УРОВ";
            this._externalDifUROVColumn.MinimumWidth = 6;
            this._externalDifUROVColumn.Name = "_externalDifUROVColumn";
            this._externalDifUROVColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifUROVColumn.Width = 50;
            // 
            // _externalDifOscColumn
            // 
            dataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifOscColumn.DefaultCellStyle = dataGridViewCellStyle37;
            this._externalDifOscColumn.HeaderText = "Осциллограф";
            this._externalDifOscColumn.MinimumWidth = 6;
            this._externalDifOscColumn.Name = "_externalDifOscColumn";
            this._externalDifOscColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifOscColumn.Width = 110;
            // 
            // _externalDifBlockingColumn
            // 
            dataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifBlockingColumn.DefaultCellStyle = dataGridViewCellStyle38;
            this._externalDifBlockingColumn.HeaderText = "Сигнал блокировки";
            this._externalDifBlockingColumn.MinimumWidth = 6;
            this._externalDifBlockingColumn.Name = "_externalDifBlockingColumn";
            this._externalDifBlockingColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifBlockingColumn.Width = 120;
            // 
            // _externalDifVozvrYNColumn
            // 
            dataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle39.NullValue = false;
            this._externalDifVozvrYNColumn.DefaultCellStyle = dataGridViewCellStyle39;
            this._externalDifVozvrYNColumn.HeaderText = "Возврат";
            this._externalDifVozvrYNColumn.MinimumWidth = 6;
            this._externalDifVozvrYNColumn.Name = "_externalDifVozvrYNColumn";
            this._externalDifVozvrYNColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifVozvrYNColumn.Width = 65;
            // 
            // _externalDifVozvrColumn
            // 
            dataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifVozvrColumn.DefaultCellStyle = dataGridViewCellStyle40;
            this._externalDifVozvrColumn.HeaderText = "Сигнал возврат";
            this._externalDifVozvrColumn.MinimumWidth = 6;
            this._externalDifVozvrColumn.Name = "_externalDifVozvrColumn";
            this._externalDifVozvrColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifVozvrColumn.Width = 125;
            // 
            // _externalDifTvzColumn
            // 
            dataGridViewCellStyle41.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifTvzColumn.DefaultCellStyle = dataGridViewCellStyle41;
            this._externalDifTvzColumn.HeaderText = "tвз [мс]";
            this._externalDifTvzColumn.MinimumWidth = 6;
            this._externalDifTvzColumn.Name = "_externalDifTvzColumn";
            this._externalDifTvzColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifTvzColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._externalDifTvzColumn.Width = 70;
            // 
            // _externalDifTsrColumn
            // 
            dataGridViewCellStyle42.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifTsrColumn.DefaultCellStyle = dataGridViewCellStyle42;
            this._externalDifTsrColumn.HeaderText = "tср [мс]";
            this._externalDifTsrColumn.MinimumWidth = 6;
            this._externalDifTsrColumn.Name = "_externalDifTsrColumn";
            this._externalDifTsrColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifTsrColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._externalDifTsrColumn.Width = 70;
            // 
            // _externalDifSrabColumn
            // 
            dataGridViewCellStyle43.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifSrabColumn.DefaultCellStyle = dataGridViewCellStyle43;
            this._externalDifSrabColumn.HeaderText = "Сигнал срабатывания";
            this._externalDifSrabColumn.MinimumWidth = 6;
            this._externalDifSrabColumn.Name = "_externalDifSrabColumn";
            this._externalDifSrabColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifSrabColumn.Width = 130;
            // 
            // _externalDifModesColumn
            // 
            dataGridViewCellStyle44.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifModesColumn.DefaultCellStyle = dataGridViewCellStyle44;
            this._externalDifModesColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._externalDifModesColumn.HeaderText = "Состояние";
            this._externalDifModesColumn.MinimumWidth = 6;
            this._externalDifModesColumn.Name = "_externalDifModesColumn";
            this._externalDifModesColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifModesColumn.Width = 110;
            // 
            // _externalDifStageColumn
            // 
            dataGridViewCellStyle45.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle45.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle45.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle45.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle45.SelectionForeColor = System.Drawing.Color.White;
            this._externalDifStageColumn.DefaultCellStyle = dataGridViewCellStyle45;
            this._externalDifStageColumn.Frozen = true;
            this._externalDifStageColumn.HeaderText = "Ступень";
            this._externalDifStageColumn.MinimumWidth = 6;
            this._externalDifStageColumn.Name = "_externalDifStageColumn";
            this._externalDifStageColumn.ReadOnly = true;
            this._externalDifStageColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifStageColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._externalDifStageColumn.Width = 125;
            // 
            // ConfigurationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 634);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this._configurationTabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(900, 669);
            this.Name = "ConfigurationForm";
            this.Text = "ConfigurationForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ConfigurationForm_FormClosing);
            this.Load += new System.EventHandler(this.ConfigurationForm_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ConfigurationForm_KeyUp);
            this._configurationTabControl.ResumeLayout(false);
            this.contextMenu.ResumeLayout(false);
            this._setpointPage.ResumeLayout(false);
            this.groupBox24.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this._setpointsTab.ResumeLayout(false);
            this._defExtGr1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._externalDefensesGr1)).EndInit();
            this._logicSignalGr1.ResumeLayout(false);
            this.groupBox49.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.VLS1.ResumeLayout(false);
            this.VLS2.ResumeLayout(false);
            this.VLS3.ResumeLayout(false);
            this.VLS4.ResumeLayout(false);
            this.VLS5.ResumeLayout(false);
            this.VLS6.ResumeLayout(false);
            this.VLS7.ResumeLayout(false);
            this.VLS8.ResumeLayout(false);
            this.VLS9.ResumeLayout(false);
            this.VLS10.ResumeLayout(false);
            this.VLS11.ResumeLayout(false);
            this.VLS12.ResumeLayout(false);
            this.VLS13.ResumeLayout(false);
            this.VLS14.ResumeLayout(false);
            this.VLS15.ResumeLayout(false);
            this.VLS16.ResumeLayout(false);
            this.groupBox26.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage31.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv1Gr1)).EndInit();
            this.tabPage32.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv2Gr1)).EndInit();
            this.tabPage33.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv3Gr1)).EndInit();
            this.tabPage34.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv4Gr1)).EndInit();
            this.tabPage35.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv5Gr1)).EndInit();
            this.tabPage36.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv6Gr1)).EndInit();
            this.tabPage37.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv7Gr1)).EndInit();
            this.tabPage38.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv8Gr1)).EndInit();
            this.groupBox45.ResumeLayout(false);
            this.tabControl.ResumeLayout(false);
            this.tabPage39.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv1Gr1)).EndInit();
            this.tabPage40.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv2Gr1)).EndInit();
            this.tabPage41.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv3Gr1)).EndInit();
            this.tabPage42.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv4Gr1)).EndInit();
            this.tabPage43.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv5Gr1)).EndInit();
            this.tabPage44.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv6Gr1)).EndInit();
            this.tabPage45.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv7Gr1)).EndInit();
            this.tabPage46.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv8Gr1)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this._inputSignalsPage.ResumeLayout(false);
            this.groupBox18.ResumeLayout(false);
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this._outputSignalsPage.ResumeLayout(false);
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox31.ResumeLayout(false);
            this.groupBox31.PerformLayout();
            this.groupBox175.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._outputIndicatorsGrid)).EndInit();
            this.groupBox176.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._outputReleGrid)).EndInit();
            this._systemPage.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._oscChannelsGrid)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this._automatPage.ResumeLayout(false);
            this.groupBox32.ResumeLayout(false);
            this.groupBox32.PerformLayout();
            this.groupBox33.ResumeLayout(false);
            this.groupBox33.PerformLayout();
            this._ethernetPage.ResumeLayout(false);
            this.groupBox51.ResumeLayout(false);
            this.groupBox51.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl _configurationTabControl;
        private System.Windows.Forms.TabPage _inputSignalsPage;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.ComboBox _indComboBox;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.ComboBox _grUst1ComboBox;
        private System.Windows.Forms.TabPage _setpointPage;
        private System.Windows.Forms.TabPage _systemPage;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.MaskedTextBox _oscWriteLength;
        private System.Windows.Forms.ComboBox _oscFix;
        private System.Windows.Forms.ComboBox _oscLength;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TabPage _automatPage;
        private System.Windows.Forms.GroupBox groupBox32;
        private System.Windows.Forms.ComboBox _switchKontCep;
        private System.Windows.Forms.MaskedTextBox _switchTUskor;
        private System.Windows.Forms.MaskedTextBox _switchImp;
        private System.Windows.Forms.ComboBox _switchBlock;
        private System.Windows.Forms.ComboBox _switchError;
        private System.Windows.Forms.ComboBox _switchOn;
        private System.Windows.Forms.ComboBox _switchOff;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.GroupBox groupBox33;
        private System.Windows.Forms.ComboBox _switchSDTU;
        private System.Windows.Forms.ComboBox _switchVnesh;
        private System.Windows.Forms.ComboBox _switchKey;
        private System.Windows.Forms.ComboBox _switchButtons;
        private System.Windows.Forms.ComboBox _switchVneshOff;
        private System.Windows.Forms.ComboBox _switchVneshOn;
        private System.Windows.Forms.ComboBox _switchKeyOff;
        private System.Windows.Forms.ComboBox _switchKeyOn;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button _writeConfigBut;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripProgressBar _progressBar;
        private System.Windows.Forms.Button _readConfigBut;
        private System.Windows.Forms.Button _saveConfigBut;
        private System.Windows.Forms.Button _loadConfigBut;
        private System.Windows.Forms.SaveFileDialog _saveConfigurationDlg;
        private System.Windows.Forms.OpenFileDialog _openConfigurationDlg;
        private System.Windows.Forms.ToolStripStatusLabel _statusLabel;
        private System.Windows.Forms.MaskedTextBox _oscSizeTextBox;
        private System.Windows.Forms.ToolTip _toolTip;
        private System.Windows.Forms.Button _resetSetpointsButton;
        private System.Windows.Forms.Button _saveToXmlButton;
        private System.Windows.Forms.SaveFileDialog _saveXmlDialog;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.ComboBox _grUst6ComboBox;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.ComboBox _grUst5ComboBox;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.ComboBox _grUst4ComboBox;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.ComboBox _grUst3ComboBox;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.ComboBox _grUst2ComboBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.ComboBox _setpointsComboBox;
        private System.Windows.Forms.TabPage _outputSignalsPage;
        private System.Windows.Forms.GroupBox groupBox31;
        private System.Windows.Forms.CheckBox _fault4CheckBox;
        private System.Windows.Forms.CheckBox _fault3CheckBox;
        private System.Windows.Forms.CheckBox _fault2CheckBox;
        private System.Windows.Forms.CheckBox _fault1CheckBox;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.MaskedTextBox _impTB;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.GroupBox groupBox175;
        private System.Windows.Forms.DataGridView _outputIndicatorsGrid;
        private System.Windows.Forms.GroupBox groupBox176;
        private System.Windows.Forms.DataGridView _outputReleGrid;
        private System.Windows.Forms.GroupBox groupBox24;
        private System.Windows.Forms.Button _applyCopySetpoinsButton;
        private System.Windows.Forms.ComboBox _copySetpoinsGroupComboBox;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ComboBox _controlSolenoidCombo;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.DataGridViewTextBoxColumn _outIndNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _outIndTypeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _outIndSignalCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _out1IndSignalCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _outIndSignal2Col;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.ComboBox oscStartCb;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.ComboBox _comandOtkl;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.CheckBox _resetAlarmCheckBox;
        private System.Windows.Forms.CheckBox _resetSystemCheckBox;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.TabControl _setpointsTab;
        private System.Windows.Forms.TabPage _logicSignalGr1;
        private System.Windows.Forms.GroupBox groupBox49;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage VLS1;
        private System.Windows.Forms.CheckedListBox VLSclb1Gr1;
        private System.Windows.Forms.TabPage VLS2;
        private System.Windows.Forms.CheckedListBox VLSclb2Gr1;
        private System.Windows.Forms.TabPage VLS3;
        private System.Windows.Forms.CheckedListBox VLSclb3Gr1;
        private System.Windows.Forms.TabPage VLS4;
        private System.Windows.Forms.CheckedListBox VLSclb4Gr1;
        private System.Windows.Forms.TabPage VLS5;
        private System.Windows.Forms.CheckedListBox VLSclb5Gr1;
        private System.Windows.Forms.TabPage VLS6;
        private System.Windows.Forms.CheckedListBox VLSclb6Gr1;
        private System.Windows.Forms.TabPage VLS7;
        private System.Windows.Forms.CheckedListBox VLSclb7Gr1;
        private System.Windows.Forms.TabPage VLS8;
        private System.Windows.Forms.CheckedListBox VLSclb8Gr1;
        private System.Windows.Forms.TabPage VLS9;
        private System.Windows.Forms.CheckedListBox VLSclb9Gr1;
        private System.Windows.Forms.TabPage VLS10;
        private System.Windows.Forms.CheckedListBox VLSclb10Gr1;
        private System.Windows.Forms.TabPage VLS11;
        private System.Windows.Forms.CheckedListBox VLSclb11Gr1;
        private System.Windows.Forms.TabPage VLS12;
        private System.Windows.Forms.CheckedListBox VLSclb12Gr1;
        private System.Windows.Forms.TabPage VLS13;
        private System.Windows.Forms.CheckedListBox VLSclb13Gr1;
        private System.Windows.Forms.TabPage VLS14;
        private System.Windows.Forms.CheckedListBox VLSclb14Gr1;
        private System.Windows.Forms.TabPage VLS15;
        private System.Windows.Forms.CheckedListBox VLSclb15Gr1;
        private System.Windows.Forms.TabPage VLS16;
        private System.Windows.Forms.CheckedListBox VLSclb16Gr1;
        private System.Windows.Forms.GroupBox groupBox26;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage31;
        private System.Windows.Forms.DataGridView _lsOrDgv1Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn15;
        private System.Windows.Forms.TabPage tabPage32;
        private System.Windows.Forms.DataGridView _lsOrDgv2Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn16;
        private System.Windows.Forms.TabPage tabPage33;
        private System.Windows.Forms.TabPage tabPage34;
        private System.Windows.Forms.DataGridView _lsOrDgv4Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn18;
        private System.Windows.Forms.TabPage tabPage35;
        private System.Windows.Forms.DataGridView _lsOrDgv5Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn19;
        private System.Windows.Forms.TabPage tabPage36;
        private System.Windows.Forms.DataGridView _lsOrDgv6Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn20;
        private System.Windows.Forms.TabPage tabPage37;
        private System.Windows.Forms.DataGridView _lsOrDgv7Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn21;
        private System.Windows.Forms.TabPage tabPage38;
        private System.Windows.Forms.DataGridView _lsOrDgv8Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn22;
        private System.Windows.Forms.GroupBox groupBox45;
        private System.Windows.Forms.TabPage _defExtGr1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _externalDifSbrosColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _externalDifAPVRetColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _externalDifAPVColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _externalDifUROVColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _externalDifOscColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _externalDifBlockingColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _externalDifVozvrYNColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _externalDifVozvrColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _externalDifTvzColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _externalDifTsrColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _externalDifSrabColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _externalDifModesColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _externalDifStageColumn;
        private System.Windows.Forms.DataGridView _externalDefensesGr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn4;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn5;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn4;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn5;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPage39;
        private System.Windows.Forms.TabPage tabPage40;
        private System.Windows.Forms.DataGridView _lsAndDgv2Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn24;
        private System.Windows.Forms.TabPage tabPage41;
        private System.Windows.Forms.TabPage tabPage42;
        private System.Windows.Forms.DataGridView _lsAndDgv4Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn27;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn26;
        private System.Windows.Forms.TabPage tabPage43;
        private System.Windows.Forms.DataGridView _lsAndDgv5Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn28;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn27;
        private System.Windows.Forms.TabPage tabPage44;
        private System.Windows.Forms.DataGridView _lsAndDgv6Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn29;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn28;
        private System.Windows.Forms.TabPage tabPage45;
        private System.Windows.Forms.DataGridView _lsAndDgv7Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn30;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn29;
        private System.Windows.Forms.TabPage tabPage46;
        private System.Windows.Forms.DataGridView _lsAndDgv8Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn31;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn30;
        private System.Windows.Forms.DataGridView _lsOrDgv3Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn17;
        private System.Windows.Forms.DataGridView _lsAndDgv1Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn23;
        private System.Windows.Forms.DataGridView _lsAndDgv3Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn26;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn25;
        private System.Windows.Forms.DataGridView _oscChannelsGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn6;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn7;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem readFromDeviceItem;
        private System.Windows.Forms.ToolStripMenuItem writeToDeviceItem;
        private System.Windows.Forms.ToolStripMenuItem readFromFileItem;
        private System.Windows.Forms.ToolStripMenuItem writeToFileItem;
        private System.Windows.Forms.ToolStripMenuItem writeToHtmlItem;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.TabPage _ethernetPage;
        private System.Windows.Forms.GroupBox groupBox51;
        private System.Windows.Forms.MaskedTextBox _ipLo1;
        private System.Windows.Forms.MaskedTextBox _ipLo2;
        private System.Windows.Forms.MaskedTextBox _ipHi1;
        private System.Windows.Forms.MaskedTextBox _ipHi2;
        private System.Windows.Forms.Label label214;
        private System.Windows.Forms.Label label213;
        private System.Windows.Forms.Label label212;
        private System.Windows.Forms.Label label179;
        private System.Windows.Forms.DataGridViewTextBoxColumn _releNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _releTypeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _releSignalCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _releWaitCol;
    }
}