﻿using System.Collections.Generic;

namespace BEMN.MR761Obr.AlarmJournal
{
    public static class AjStrings
    {
        /// <summary>
        /// Группа уставок(Журнал аварий)
        /// </summary>
        public static List<string> AlarmJournalSetpointsGroup
        {
            get
            {
                return new List<string>
                    {
                        "Группа 1",
                        "Группа 2",
                        "Группа 3",
                        "Группа 4",
                        "Группа 5",
                        "Группа 6"
                    };
            }
        }
        /// <summary>
        /// Сработавшая защита
        /// </summary>
        public static List<string> TriggeredDefense
        {
            get
            {
                return new List<string>
                    {
                        "ВНЕШ. 1",
                        "ВНЕШ. 2",
                        "ВНЕШ. 3",
                        "ВНЕШ. 4",
                        "ВНЕШ. 5",
                        "ВНЕШ. 6",
                        "ВНЕШ. 7",
                        "ВНЕШ. 8",
                        "ВНЕШ. 9",
                        "ВНЕШ. 10",
                        "ВНЕШ. 11",
                        "ВНЕШ. 12",
                        "ВНЕШ. 13",
                        "ВНЕШ. 14",
                        "ВНЕШ. 15",
                        "ВНЕШ. 16",
                        "ЖА СПЛ",
                        "",
                        "ПУСК ОСЦИЛЛОГРАФА ОТ ДИСКР. СИГНАЛА",
                        "МЕНЮ: ПУСК ОСЦИЛЛОГРАФА",
                        "СДТУ: ПУСК ОСЦИЛЛОГРАФА"
                    };
            }
        }
        /// <summary>
        /// Сообщение
        /// </summary>
        public static List<string> Message
        {
            get
            {
                return new List<string>
                    {
                        "ОШИБКА СООБЩЕНИЯ",
                        "СИГНАЛ-ЦИЯ",
                        "РАБОТА",
                        "ОТКЛЮЧЕНИЕ",
                        "НЕУСП. АПВ",
                        "АВАРИЯ",
                        "ЛОГИКА",
                        "ОМП",
                        "СООБЩЕНИЕ",
                        "Резерв",
                        "СООБЩЕНИЕ СПЛ"
                    };
            }
        }
    }
}
