﻿namespace BEMN.MR761Obr.OBRNEW.Configuration
{
    partial class ConfigurationFormNew
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle41 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle42 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle43 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle44 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle45 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle46 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle47 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle48 = new System.Windows.Forms.DataGridViewCellStyle();
            this._configurationTabControl = new System.Windows.Forms.TabControl();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.readFromDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readFromFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetSetpointItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToHtmlItem = new System.Windows.Forms.ToolStripMenuItem();
            this._setpointPage = new System.Windows.Forms.TabPage();
            this.groupBox24 = new System.Windows.Forms.GroupBox();
            this._applyCopySetpoinsButton = new System.Windows.Forms.Button();
            this._copySetpoinsGroupComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._setpointsTab = new System.Windows.Forms.TabControl();
            this._defExtGr1 = new System.Windows.Forms.TabPage();
            this._externalDefensesGr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewComboBoxColumn4 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn5 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewCheckBoxColumn4 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn5 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._logicSignalGr1 = new System.Windows.Forms.TabPage();
            this.groupBox49 = new System.Windows.Forms.GroupBox();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.VLS1 = new System.Windows.Forms.TabPage();
            this.VLSclb1Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS2 = new System.Windows.Forms.TabPage();
            this.VLSclb2Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS3 = new System.Windows.Forms.TabPage();
            this.VLSclb3Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS4 = new System.Windows.Forms.TabPage();
            this.VLSclb4Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS5 = new System.Windows.Forms.TabPage();
            this.VLSclb5Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS6 = new System.Windows.Forms.TabPage();
            this.VLSclb6Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS7 = new System.Windows.Forms.TabPage();
            this.VLSclb7Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS8 = new System.Windows.Forms.TabPage();
            this.VLSclb8Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS9 = new System.Windows.Forms.TabPage();
            this.VLSclb9Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS10 = new System.Windows.Forms.TabPage();
            this.VLSclb10Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS11 = new System.Windows.Forms.TabPage();
            this.VLSclb11Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS12 = new System.Windows.Forms.TabPage();
            this.VLSclb12Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS13 = new System.Windows.Forms.TabPage();
            this.VLSclb13Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS14 = new System.Windows.Forms.TabPage();
            this.VLSclb14Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS15 = new System.Windows.Forms.TabPage();
            this.VLSclb15Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS16 = new System.Windows.Forms.TabPage();
            this.VLSclb16Gr1 = new System.Windows.Forms.CheckedListBox();
            this.groupBox26 = new System.Windows.Forms.GroupBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage31 = new System.Windows.Forms.TabPage();
            this._lsOrDgv1Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn15 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage32 = new System.Windows.Forms.TabPage();
            this._lsOrDgv2Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn16 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage33 = new System.Windows.Forms.TabPage();
            this._lsOrDgv3Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn17 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage34 = new System.Windows.Forms.TabPage();
            this._lsOrDgv4Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn18 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage35 = new System.Windows.Forms.TabPage();
            this._lsOrDgv5Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn19 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage36 = new System.Windows.Forms.TabPage();
            this._lsOrDgv6Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn20 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage37 = new System.Windows.Forms.TabPage();
            this._lsOrDgv7Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn21 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage38 = new System.Windows.Forms.TabPage();
            this._lsOrDgv8Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn22 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.groupBox45 = new System.Windows.Forms.GroupBox();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPage39 = new System.Windows.Forms.TabPage();
            this._lsAndDgv1Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn23 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage40 = new System.Windows.Forms.TabPage();
            this._lsAndDgv2Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn24 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage41 = new System.Windows.Forms.TabPage();
            this._lsAndDgv3Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn25 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage42 = new System.Windows.Forms.TabPage();
            this._lsAndDgv4Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn26 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage43 = new System.Windows.Forms.TabPage();
            this._lsAndDgv5Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn27 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage44 = new System.Windows.Forms.TabPage();
            this._lsAndDgv6Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn28 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage45 = new System.Windows.Forms.TabPage();
            this._lsAndDgv7Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn29 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage46 = new System.Windows.Forms.TabPage();
            this._lsAndDgv8Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn30 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this._setpointsComboBox = new System.Windows.Forms.ComboBox();
            this._automatPage = new System.Windows.Forms.TabPage();
            this.groupBox32 = new System.Windows.Forms.GroupBox();
            this._switchImp = new System.Windows.Forms.MaskedTextBox();
            this._controlSolenoidCombo = new System.Windows.Forms.ComboBox();
            this._switchYesNo = new System.Windows.Forms.CheckBox();
            this.label96 = new System.Windows.Forms.Label();
            this._switchBlock = new System.Windows.Forms.ComboBox();
            this.label99 = new System.Windows.Forms.Label();
            this._switchError = new System.Windows.Forms.ComboBox();
            this._comandOtkl = new System.Windows.Forms.ComboBox();
            this._switchOn = new System.Windows.Forms.ComboBox();
            this.label51 = new System.Windows.Forms.Label();
            this._switchOff = new System.Windows.Forms.ComboBox();
            this.label98 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this._switchKontCep = new System.Windows.Forms.ComboBox();
            this.label89 = new System.Windows.Forms.Label();
            this._switchTUskor = new System.Windows.Forms.MaskedTextBox();
            this.label88 = new System.Windows.Forms.Label();
            this.groupBox33 = new System.Windows.Forms.GroupBox();
            this._blockSDTU = new System.Windows.Forms.ComboBox();
            this._blockSDTUlabel = new System.Windows.Forms.Label();
            this._switchSDTU = new System.Windows.Forms.ComboBox();
            this._switchVnesh = new System.Windows.Forms.ComboBox();
            this._switchKey = new System.Windows.Forms.ComboBox();
            this._switchButtons = new System.Windows.Forms.ComboBox();
            this._switchVneshOff = new System.Windows.Forms.ComboBox();
            this._switchVneshOn = new System.Windows.Forms.ComboBox();
            this._switchKeyOff = new System.Windows.Forms.ComboBox();
            this._switchKeyOn = new System.Windows.Forms.ComboBox();
            this.label101 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this._inputSignalsPage = new System.Windows.Forms.TabPage();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this._indComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.label76 = new System.Windows.Forms.Label();
            this._grUst2ComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this._grUst1ComboBox = new System.Windows.Forms.ComboBox();
            this._outputSignalsPage = new System.Windows.Forms.TabPage();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this._resetAlarmCheckBox = new System.Windows.Forms.CheckBox();
            this._resetSystemCheckBox = new System.Windows.Forms.CheckBox();
            this.label110 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.groupBox31 = new System.Windows.Forms.GroupBox();
            this._fault4CheckBox = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this._fault3CheckBox = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this._fault2CheckBox = new System.Windows.Forms.CheckBox();
            this._fault1CheckBox = new System.Windows.Forms.CheckBox();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this._impTB = new System.Windows.Forms.MaskedTextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.groupBox175 = new System.Windows.Forms.GroupBox();
            this._outputIndicatorsGrid = new System.Windows.Forms.DataGridView();
            this._outIndNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._outIndTypeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._outIndSignalCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._out1IndSignalCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._outIndSignal2Col = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.groupBox176 = new System.Windows.Forms.GroupBox();
            this._outputReleGrid = new System.Windows.Forms.DataGridView();
            this._releNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._releTypeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._releSignalCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._releWaitCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._logicElement = new System.Windows.Forms.TabPage();
            this.groupBox55 = new System.Windows.Forms.GroupBox();
            this._virtualReleDataGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn52 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn68 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn69 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn53 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox53 = new System.Windows.Forms.GroupBox();
            this._rsTriggersDataGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn51 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn65 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn66 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn67 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._systemPage = new System.Windows.Forms.TabPage();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this._oscChannelsGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn6 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn7 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.oscStartCb = new System.Windows.Forms.ComboBox();
            this.label65 = new System.Windows.Forms.Label();
            this._oscSizeTextBox = new System.Windows.Forms.MaskedTextBox();
            this._oscWriteLength = new System.Windows.Forms.MaskedTextBox();
            this._oscFix = new System.Windows.Forms.ComboBox();
            this._oscLength = new System.Windows.Forms.ComboBox();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this._bgsGoose = new System.Windows.Forms.TabPage();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.goin64 = new System.Windows.Forms.ComboBox();
            this.goin48 = new System.Windows.Forms.ComboBox();
            this.goin32 = new System.Windows.Forms.ComboBox();
            this.label281 = new System.Windows.Forms.Label();
            this.label282 = new System.Windows.Forms.Label();
            this.label283 = new System.Windows.Forms.Label();
            this.goin63 = new System.Windows.Forms.ComboBox();
            this.goin47 = new System.Windows.Forms.ComboBox();
            this.goin31 = new System.Windows.Forms.ComboBox();
            this.label284 = new System.Windows.Forms.Label();
            this.label285 = new System.Windows.Forms.Label();
            this.label286 = new System.Windows.Forms.Label();
            this.goin62 = new System.Windows.Forms.ComboBox();
            this.goin46 = new System.Windows.Forms.ComboBox();
            this.goin30 = new System.Windows.Forms.ComboBox();
            this.label287 = new System.Windows.Forms.Label();
            this.label288 = new System.Windows.Forms.Label();
            this.label289 = new System.Windows.Forms.Label();
            this.goin61 = new System.Windows.Forms.ComboBox();
            this.goin45 = new System.Windows.Forms.ComboBox();
            this.goin29 = new System.Windows.Forms.ComboBox();
            this.label290 = new System.Windows.Forms.Label();
            this.label291 = new System.Windows.Forms.Label();
            this.label292 = new System.Windows.Forms.Label();
            this.goin60 = new System.Windows.Forms.ComboBox();
            this.goin44 = new System.Windows.Forms.ComboBox();
            this.goin28 = new System.Windows.Forms.ComboBox();
            this.label293 = new System.Windows.Forms.Label();
            this.label294 = new System.Windows.Forms.Label();
            this.label295 = new System.Windows.Forms.Label();
            this.goin59 = new System.Windows.Forms.ComboBox();
            this.goin43 = new System.Windows.Forms.ComboBox();
            this.goin27 = new System.Windows.Forms.ComboBox();
            this.label296 = new System.Windows.Forms.Label();
            this.label297 = new System.Windows.Forms.Label();
            this.label298 = new System.Windows.Forms.Label();
            this.goin58 = new System.Windows.Forms.ComboBox();
            this.goin42 = new System.Windows.Forms.ComboBox();
            this.goin26 = new System.Windows.Forms.ComboBox();
            this.label299 = new System.Windows.Forms.Label();
            this.label300 = new System.Windows.Forms.Label();
            this.label301 = new System.Windows.Forms.Label();
            this.goin57 = new System.Windows.Forms.ComboBox();
            this.goin41 = new System.Windows.Forms.ComboBox();
            this.goin25 = new System.Windows.Forms.ComboBox();
            this.label302 = new System.Windows.Forms.Label();
            this.label303 = new System.Windows.Forms.Label();
            this.label304 = new System.Windows.Forms.Label();
            this.goin56 = new System.Windows.Forms.ComboBox();
            this.goin40 = new System.Windows.Forms.ComboBox();
            this.goin24 = new System.Windows.Forms.ComboBox();
            this.label305 = new System.Windows.Forms.Label();
            this.label306 = new System.Windows.Forms.Label();
            this.label307 = new System.Windows.Forms.Label();
            this.goin55 = new System.Windows.Forms.ComboBox();
            this.goin39 = new System.Windows.Forms.ComboBox();
            this.goin23 = new System.Windows.Forms.ComboBox();
            this.label308 = new System.Windows.Forms.Label();
            this.label309 = new System.Windows.Forms.Label();
            this.label310 = new System.Windows.Forms.Label();
            this.goin54 = new System.Windows.Forms.ComboBox();
            this.goin38 = new System.Windows.Forms.ComboBox();
            this.goin22 = new System.Windows.Forms.ComboBox();
            this.label311 = new System.Windows.Forms.Label();
            this.label312 = new System.Windows.Forms.Label();
            this.label313 = new System.Windows.Forms.Label();
            this.goin53 = new System.Windows.Forms.ComboBox();
            this.goin37 = new System.Windows.Forms.ComboBox();
            this.goin21 = new System.Windows.Forms.ComboBox();
            this.label314 = new System.Windows.Forms.Label();
            this.label315 = new System.Windows.Forms.Label();
            this.label316 = new System.Windows.Forms.Label();
            this.goin52 = new System.Windows.Forms.ComboBox();
            this.goin36 = new System.Windows.Forms.ComboBox();
            this.goin20 = new System.Windows.Forms.ComboBox();
            this.label317 = new System.Windows.Forms.Label();
            this.label318 = new System.Windows.Forms.Label();
            this.label319 = new System.Windows.Forms.Label();
            this.goin51 = new System.Windows.Forms.ComboBox();
            this.goin35 = new System.Windows.Forms.ComboBox();
            this.goin19 = new System.Windows.Forms.ComboBox();
            this.label320 = new System.Windows.Forms.Label();
            this.label321 = new System.Windows.Forms.Label();
            this.label322 = new System.Windows.Forms.Label();
            this.goin50 = new System.Windows.Forms.ComboBox();
            this.goin34 = new System.Windows.Forms.ComboBox();
            this.goin18 = new System.Windows.Forms.ComboBox();
            this.label323 = new System.Windows.Forms.Label();
            this.label324 = new System.Windows.Forms.Label();
            this.label325 = new System.Windows.Forms.Label();
            this.goin49 = new System.Windows.Forms.ComboBox();
            this.label326 = new System.Windows.Forms.Label();
            this.goin33 = new System.Windows.Forms.ComboBox();
            this.label327 = new System.Windows.Forms.Label();
            this.goin17 = new System.Windows.Forms.ComboBox();
            this.label328 = new System.Windows.Forms.Label();
            this.goin16 = new System.Windows.Forms.ComboBox();
            this.label329 = new System.Windows.Forms.Label();
            this.goin15 = new System.Windows.Forms.ComboBox();
            this.label330 = new System.Windows.Forms.Label();
            this.goin14 = new System.Windows.Forms.ComboBox();
            this.label331 = new System.Windows.Forms.Label();
            this.goin13 = new System.Windows.Forms.ComboBox();
            this.label332 = new System.Windows.Forms.Label();
            this.goin12 = new System.Windows.Forms.ComboBox();
            this.label333 = new System.Windows.Forms.Label();
            this.goin11 = new System.Windows.Forms.ComboBox();
            this.label334 = new System.Windows.Forms.Label();
            this.goin10 = new System.Windows.Forms.ComboBox();
            this.label335 = new System.Windows.Forms.Label();
            this.goin9 = new System.Windows.Forms.ComboBox();
            this.label336 = new System.Windows.Forms.Label();
            this.goin8 = new System.Windows.Forms.ComboBox();
            this.label337 = new System.Windows.Forms.Label();
            this.goin7 = new System.Windows.Forms.ComboBox();
            this.label338 = new System.Windows.Forms.Label();
            this.goin6 = new System.Windows.Forms.ComboBox();
            this.label339 = new System.Windows.Forms.Label();
            this.goin5 = new System.Windows.Forms.ComboBox();
            this.label340 = new System.Windows.Forms.Label();
            this.goin4 = new System.Windows.Forms.ComboBox();
            this.label341 = new System.Windows.Forms.Label();
            this.goin3 = new System.Windows.Forms.ComboBox();
            this.label342 = new System.Windows.Forms.Label();
            this.goin2 = new System.Windows.Forms.ComboBox();
            this.label343 = new System.Windows.Forms.Label();
            this.goin1 = new System.Windows.Forms.ComboBox();
            this.label344 = new System.Windows.Forms.Label();
            this.label345 = new System.Windows.Forms.Label();
            this.operationBGS = new System.Windows.Forms.ComboBox();
            this.label36 = new System.Windows.Forms.Label();
            this.currentBGS = new System.Windows.Forms.ComboBox();
            this._ethernetPage = new System.Windows.Forms.TabPage();
            this.groupBox51 = new System.Windows.Forms.GroupBox();
            this._ipLo1 = new System.Windows.Forms.MaskedTextBox();
            this._ipLo2 = new System.Windows.Forms.MaskedTextBox();
            this._ipHi1 = new System.Windows.Forms.MaskedTextBox();
            this._ipHi2 = new System.Windows.Forms.MaskedTextBox();
            this.label214 = new System.Windows.Forms.Label();
            this.label213 = new System.Windows.Forms.Label();
            this.label212 = new System.Windows.Forms.Label();
            this.label179 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this._saveToXmlButton = new System.Windows.Forms.Button();
            this._resetSetpointsButton = new System.Windows.Forms.Button();
            this._writeConfigBut = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this._progressBar = new System.Windows.Forms.ToolStripProgressBar();
            this._statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._readConfigBut = new System.Windows.Forms.Button();
            this._saveConfigBut = new System.Windows.Forms.Button();
            this._loadConfigBut = new System.Windows.Forms.Button();
            this._saveConfigurationDlg = new System.Windows.Forms.SaveFileDialog();
            this._openConfigurationDlg = new System.Windows.Forms.OpenFileDialog();
            this._toolTip = new System.Windows.Forms.ToolTip(this.components);
            this._saveXmlDialog = new System.Windows.Forms.SaveFileDialog();
            this._externalDifSbrosColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._externalDifAPVRetColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._externalDifAPVColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._externalDifUROVColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._externalDifOscColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._externalDifBlockingColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._externalDifVozvrYNColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._externalDifVozvrColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._externalDifTvzColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._externalDifTsrColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._externalDifSrabColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._externalDifModesColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._externalDifStageColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this._configurationTabControl.SuspendLayout();
            this.contextMenu.SuspendLayout();
            this._setpointPage.SuspendLayout();
            this.groupBox24.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this._setpointsTab.SuspendLayout();
            this._defExtGr1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._externalDefensesGr1)).BeginInit();
            this._logicSignalGr1.SuspendLayout();
            this.groupBox49.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.VLS1.SuspendLayout();
            this.VLS2.SuspendLayout();
            this.VLS3.SuspendLayout();
            this.VLS4.SuspendLayout();
            this.VLS5.SuspendLayout();
            this.VLS6.SuspendLayout();
            this.VLS7.SuspendLayout();
            this.VLS8.SuspendLayout();
            this.VLS9.SuspendLayout();
            this.VLS10.SuspendLayout();
            this.VLS11.SuspendLayout();
            this.VLS12.SuspendLayout();
            this.VLS13.SuspendLayout();
            this.VLS14.SuspendLayout();
            this.VLS15.SuspendLayout();
            this.VLS16.SuspendLayout();
            this.groupBox26.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage31.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv1Gr1)).BeginInit();
            this.tabPage32.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv2Gr1)).BeginInit();
            this.tabPage33.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv3Gr1)).BeginInit();
            this.tabPage34.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv4Gr1)).BeginInit();
            this.tabPage35.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv5Gr1)).BeginInit();
            this.tabPage36.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv6Gr1)).BeginInit();
            this.tabPage37.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv7Gr1)).BeginInit();
            this.tabPage38.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv8Gr1)).BeginInit();
            this.groupBox45.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabPage39.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv1Gr1)).BeginInit();
            this.tabPage40.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv2Gr1)).BeginInit();
            this.tabPage41.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv3Gr1)).BeginInit();
            this.tabPage42.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv4Gr1)).BeginInit();
            this.tabPage43.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv5Gr1)).BeginInit();
            this.tabPage44.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv6Gr1)).BeginInit();
            this.tabPage45.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv7Gr1)).BeginInit();
            this.tabPage46.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv8Gr1)).BeginInit();
            this.groupBox7.SuspendLayout();
            this._automatPage.SuspendLayout();
            this.groupBox32.SuspendLayout();
            this.groupBox33.SuspendLayout();
            this._inputSignalsPage.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this._outputSignalsPage.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox31.SuspendLayout();
            this.groupBox175.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._outputIndicatorsGrid)).BeginInit();
            this.groupBox176.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._outputReleGrid)).BeginInit();
            this._logicElement.SuspendLayout();
            this.groupBox55.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._virtualReleDataGrid)).BeginInit();
            this.groupBox53.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._rsTriggersDataGrid)).BeginInit();
            this._systemPage.SuspendLayout();
            this.groupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._oscChannelsGrid)).BeginInit();
            this.groupBox3.SuspendLayout();
            this._bgsGoose.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox19.SuspendLayout();
            this._ethernetPage.SuspendLayout();
            this.groupBox51.SuspendLayout();
            this.panel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _configurationTabControl
            // 
            this._configurationTabControl.ContextMenuStrip = this.contextMenu;
            this._configurationTabControl.Controls.Add(this._setpointPage);
            this._configurationTabControl.Controls.Add(this._automatPage);
            this._configurationTabControl.Controls.Add(this._inputSignalsPage);
            this._configurationTabControl.Controls.Add(this._outputSignalsPage);
            this._configurationTabControl.Controls.Add(this._logicElement);
            this._configurationTabControl.Controls.Add(this._systemPage);
            this._configurationTabControl.Controls.Add(this._bgsGoose);
            this._configurationTabControl.Controls.Add(this._ethernetPage);
            this._configurationTabControl.Dock = System.Windows.Forms.DockStyle.Top;
            this._configurationTabControl.Location = new System.Drawing.Point(0, 0);
            this._configurationTabControl.MinimumSize = new System.Drawing.Size(820, 579);
            this._configurationTabControl.Name = "_configurationTabControl";
            this._configurationTabControl.SelectedIndex = 0;
            this._configurationTabControl.Size = new System.Drawing.Size(984, 579);
            this._configurationTabControl.TabIndex = 31;
            // 
            // contextMenu
            // 
            this.contextMenu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.readFromDeviceItem,
            this.writeToDeviceItem,
            this.readFromFileItem,
            this.resetSetpointItem,
            this.writeToFileItem,
            this.writeToHtmlItem});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(213, 136);
            this.contextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenu_Opening);
            this.contextMenu.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.contextMenu_ItemClicked);
            // 
            // readFromDeviceItem
            // 
            this.readFromDeviceItem.Name = "readFromDeviceItem";
            this.readFromDeviceItem.Size = new System.Drawing.Size(212, 22);
            this.readFromDeviceItem.Text = "Прочитать из устройства";
            // 
            // writeToDeviceItem
            // 
            this.writeToDeviceItem.Name = "writeToDeviceItem";
            this.writeToDeviceItem.Size = new System.Drawing.Size(212, 22);
            this.writeToDeviceItem.Text = "Записать в устройство";
            // 
            // readFromFileItem
            // 
            this.readFromFileItem.Name = "readFromFileItem";
            this.readFromFileItem.Size = new System.Drawing.Size(212, 22);
            this.readFromFileItem.Text = "Загрузить из файла";
            // 
            // resetSetpointItem
            // 
            this.resetSetpointItem.Name = "resetSetpointItem";
            this.resetSetpointItem.Size = new System.Drawing.Size(212, 22);
            this.resetSetpointItem.Text = "Обнулить уставки";
            // 
            // writeToFileItem
            // 
            this.writeToFileItem.Name = "writeToFileItem";
            this.writeToFileItem.Size = new System.Drawing.Size(212, 22);
            this.writeToFileItem.Text = "Сохранить в файл";
            // 
            // writeToHtmlItem
            // 
            this.writeToHtmlItem.Name = "writeToHtmlItem";
            this.writeToHtmlItem.Size = new System.Drawing.Size(212, 22);
            this.writeToHtmlItem.Text = "Сохранить в HTML";
            // 
            // _setpointPage
            // 
            this._setpointPage.Controls.Add(this.groupBox24);
            this._setpointPage.Controls.Add(this.groupBox1);
            this._setpointPage.Controls.Add(this.groupBox7);
            this._setpointPage.Location = new System.Drawing.Point(4, 22);
            this._setpointPage.Name = "_setpointPage";
            this._setpointPage.Size = new System.Drawing.Size(976, 553);
            this._setpointPage.TabIndex = 4;
            this._setpointPage.Text = "Уставки";
            this._setpointPage.UseVisualStyleBackColor = true;
            // 
            // groupBox24
            // 
            this.groupBox24.Controls.Add(this._applyCopySetpoinsButton);
            this.groupBox24.Controls.Add(this._copySetpoinsGroupComboBox);
            this.groupBox24.Location = new System.Drawing.Point(112, 3);
            this.groupBox24.Name = "groupBox24";
            this.groupBox24.Size = new System.Drawing.Size(174, 54);
            this.groupBox24.TabIndex = 3;
            this.groupBox24.TabStop = false;
            this.groupBox24.Text = "Копировать в";
            // 
            // _applyCopySetpoinsButton
            // 
            this._applyCopySetpoinsButton.Location = new System.Drawing.Point(95, 17);
            this._applyCopySetpoinsButton.Name = "_applyCopySetpoinsButton";
            this._applyCopySetpoinsButton.Size = new System.Drawing.Size(75, 23);
            this._applyCopySetpoinsButton.TabIndex = 1;
            this._applyCopySetpoinsButton.Text = "Применить";
            this._applyCopySetpoinsButton.UseVisualStyleBackColor = true;
            this._applyCopySetpoinsButton.Click += new System.EventHandler(this._applyCopySetpoinsButton_Click);
            // 
            // _copySetpoinsGroupComboBox
            // 
            this._copySetpoinsGroupComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._copySetpoinsGroupComboBox.FormattingEnabled = true;
            this._copySetpoinsGroupComboBox.Location = new System.Drawing.Point(6, 19);
            this._copySetpoinsGroupComboBox.Name = "_copySetpoinsGroupComboBox";
            this._copySetpoinsGroupComboBox.Size = new System.Drawing.Size(83, 21);
            this._copySetpoinsGroupComboBox.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this._setpointsTab);
            this.groupBox1.Location = new System.Drawing.Point(3, 59);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(970, 491);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            // 
            // _setpointsTab
            // 
            this._setpointsTab.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this._setpointsTab.Controls.Add(this._defExtGr1);
            this._setpointsTab.Controls.Add(this._logicSignalGr1);
            this._setpointsTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this._setpointsTab.Location = new System.Drawing.Point(3, 16);
            this._setpointsTab.Name = "_setpointsTab";
            this._setpointsTab.SelectedIndex = 0;
            this._setpointsTab.Size = new System.Drawing.Size(964, 472);
            this._setpointsTab.TabIndex = 2;
            // 
            // _defExtGr1
            // 
            this._defExtGr1.Controls.Add(this._externalDefensesGr1);
            this._defExtGr1.Location = new System.Drawing.Point(4, 25);
            this._defExtGr1.Name = "_defExtGr1";
            this._defExtGr1.Size = new System.Drawing.Size(956, 443);
            this._defExtGr1.TabIndex = 8;
            this._defExtGr1.Text = "Внешние";
            this._defExtGr1.UseVisualStyleBackColor = true;
            // 
            // _externalDefensesGr1
            // 
            this._externalDefensesGr1.AllowUserToAddRows = false;
            this._externalDefensesGr1.AllowUserToDeleteRows = false;
            this._externalDefensesGr1.AllowUserToResizeColumns = false;
            this._externalDefensesGr1.AllowUserToResizeRows = false;
            this._externalDefensesGr1.BackgroundColor = System.Drawing.Color.White;
            this._externalDefensesGr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._externalDefensesGr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewComboBoxColumn1,
            this.dataGridViewComboBoxColumn2,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewComboBoxColumn3,
            this.dataGridViewCheckBoxColumn1,
            this.dataGridViewComboBoxColumn4,
            this.dataGridViewComboBoxColumn5,
            this.dataGridViewCheckBoxColumn4,
            this.dataGridViewCheckBoxColumn5});
            this._externalDefensesGr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._externalDefensesGr1.Location = new System.Drawing.Point(0, 0);
            this._externalDefensesGr1.Name = "_externalDefensesGr1";
            this._externalDefensesGr1.RowHeadersVisible = false;
            this._externalDefensesGr1.RowHeadersWidth = 51;
            this._externalDefensesGr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._externalDefensesGr1.RowTemplate.Height = 24;
            this._externalDefensesGr1.ShowCellErrors = false;
            this._externalDefensesGr1.ShowRowErrors = false;
            this._externalDefensesGr1.Size = new System.Drawing.Size(956, 443);
            this._externalDefensesGr1.TabIndex = 5;
            // 
            // dataGridViewTextBoxColumn1
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewTextBoxColumn1.Frozen = true;
            this.dataGridViewTextBoxColumn1.HeaderText = "Ступень";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn1.Width = 125;
            // 
            // dataGridViewComboBoxColumn1
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewComboBoxColumn1.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn1.HeaderText = "Состояние";
            this.dataGridViewComboBoxColumn1.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            this.dataGridViewComboBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn1.Width = 110;
            // 
            // dataGridViewComboBoxColumn2
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn2.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewComboBoxColumn2.HeaderText = "Сигнал срабатывания";
            this.dataGridViewComboBoxColumn2.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn2.Name = "dataGridViewComboBoxColumn2";
            this.dataGridViewComboBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn2.Width = 130;
            // 
            // dataGridViewTextBoxColumn2
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewTextBoxColumn2.HeaderText = "tср [мс]";
            this.dataGridViewTextBoxColumn2.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn2.Width = 70;
            // 
            // dataGridViewTextBoxColumn3
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridViewTextBoxColumn3.HeaderText = "tвз [мс]";
            this.dataGridViewTextBoxColumn3.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn3.Width = 70;
            // 
            // dataGridViewComboBoxColumn3
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn3.DefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridViewComboBoxColumn3.HeaderText = "Сигнал возврат";
            this.dataGridViewComboBoxColumn3.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn3.Name = "dataGridViewComboBoxColumn3";
            this.dataGridViewComboBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn3.Width = 125;
            // 
            // dataGridViewCheckBoxColumn1
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.NullValue = false;
            this.dataGridViewCheckBoxColumn1.DefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewCheckBoxColumn1.HeaderText = "Возврат";
            this.dataGridViewCheckBoxColumn1.MinimumWidth = 6;
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn1.Width = 65;
            // 
            // dataGridViewComboBoxColumn4
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn4.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridViewComboBoxColumn4.HeaderText = "Сигнал блокировки";
            this.dataGridViewComboBoxColumn4.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn4.Name = "dataGridViewComboBoxColumn4";
            this.dataGridViewComboBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn4.Width = 120;
            // 
            // dataGridViewComboBoxColumn5
            // 
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn5.DefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewComboBoxColumn5.HeaderText = "Осциллограф";
            this.dataGridViewComboBoxColumn5.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn5.Name = "dataGridViewComboBoxColumn5";
            this.dataGridViewComboBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn5.Width = 110;
            // 
            // dataGridViewCheckBoxColumn4
            // 
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle10.NullValue = false;
            this.dataGridViewCheckBoxColumn4.DefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridViewCheckBoxColumn4.HeaderText = "АПВ возвр.";
            this.dataGridViewCheckBoxColumn4.MinimumWidth = 6;
            this.dataGridViewCheckBoxColumn4.Name = "dataGridViewCheckBoxColumn4";
            this.dataGridViewCheckBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn4.Width = 75;
            // 
            // dataGridViewCheckBoxColumn5
            // 
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle11.NullValue = false;
            this.dataGridViewCheckBoxColumn5.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridViewCheckBoxColumn5.HeaderText = "Сброс";
            this.dataGridViewCheckBoxColumn5.MinimumWidth = 6;
            this.dataGridViewCheckBoxColumn5.Name = "dataGridViewCheckBoxColumn5";
            this.dataGridViewCheckBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn5.Width = 50;
            // 
            // _logicSignalGr1
            // 
            this._logicSignalGr1.AutoScroll = true;
            this._logicSignalGr1.Controls.Add(this.groupBox49);
            this._logicSignalGr1.Controls.Add(this.groupBox26);
            this._logicSignalGr1.Controls.Add(this.groupBox45);
            this._logicSignalGr1.Location = new System.Drawing.Point(4, 25);
            this._logicSignalGr1.Name = "_logicSignalGr1";
            this._logicSignalGr1.Size = new System.Drawing.Size(956, 445);
            this._logicSignalGr1.TabIndex = 14;
            this._logicSignalGr1.Text = "Логические сигналы";
            this._logicSignalGr1.UseVisualStyleBackColor = true;
            // 
            // groupBox49
            // 
            this.groupBox49.Controls.Add(this.tabControl2);
            this.groupBox49.Location = new System.Drawing.Point(406, 4);
            this.groupBox49.Name = "groupBox49";
            this.groupBox49.Size = new System.Drawing.Size(409, 436);
            this.groupBox49.TabIndex = 6;
            this.groupBox49.TabStop = false;
            this.groupBox49.Text = "ВЛС";
            // 
            // tabControl2
            // 
            this.tabControl2.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabControl2.Controls.Add(this.VLS1);
            this.tabControl2.Controls.Add(this.VLS2);
            this.tabControl2.Controls.Add(this.VLS3);
            this.tabControl2.Controls.Add(this.VLS4);
            this.tabControl2.Controls.Add(this.VLS5);
            this.tabControl2.Controls.Add(this.VLS6);
            this.tabControl2.Controls.Add(this.VLS7);
            this.tabControl2.Controls.Add(this.VLS8);
            this.tabControl2.Controls.Add(this.VLS9);
            this.tabControl2.Controls.Add(this.VLS10);
            this.tabControl2.Controls.Add(this.VLS11);
            this.tabControl2.Controls.Add(this.VLS12);
            this.tabControl2.Controls.Add(this.VLS13);
            this.tabControl2.Controls.Add(this.VLS14);
            this.tabControl2.Controls.Add(this.VLS15);
            this.tabControl2.Controls.Add(this.VLS16);
            this.tabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl2.Location = new System.Drawing.Point(3, 16);
            this.tabControl2.Multiline = true;
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(403, 417);
            this.tabControl2.TabIndex = 0;
            // 
            // VLS1
            // 
            this.VLS1.Controls.Add(this.VLSclb1Gr1);
            this.VLS1.Location = new System.Drawing.Point(4, 49);
            this.VLS1.Name = "VLS1";
            this.VLS1.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.VLS1.Size = new System.Drawing.Size(395, 364);
            this.VLS1.TabIndex = 0;
            this.VLS1.Text = "ВЛС 1";
            this.VLS1.UseVisualStyleBackColor = true;
            // 
            // VLSclb1Gr1
            // 
            this.VLSclb1Gr1.CheckOnClick = true;
            this.VLSclb1Gr1.FormattingEnabled = true;
            this.VLSclb1Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb1Gr1.Name = "VLSclb1Gr1";
            this.VLSclb1Gr1.ScrollAlwaysVisible = true;
            this.VLSclb1Gr1.Size = new System.Drawing.Size(191, 289);
            this.VLSclb1Gr1.TabIndex = 6;
            // 
            // VLS2
            // 
            this.VLS2.Controls.Add(this.VLSclb2Gr1);
            this.VLS2.Location = new System.Drawing.Point(4, 49);
            this.VLS2.Name = "VLS2";
            this.VLS2.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.VLS2.Size = new System.Drawing.Size(395, 366);
            this.VLS2.TabIndex = 1;
            this.VLS2.Text = "ВЛС 2";
            this.VLS2.UseVisualStyleBackColor = true;
            // 
            // VLSclb2Gr1
            // 
            this.VLSclb2Gr1.CheckOnClick = true;
            this.VLSclb2Gr1.FormattingEnabled = true;
            this.VLSclb2Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb2Gr1.Name = "VLSclb2Gr1";
            this.VLSclb2Gr1.ScrollAlwaysVisible = true;
            this.VLSclb2Gr1.Size = new System.Drawing.Size(184, 289);
            this.VLSclb2Gr1.TabIndex = 6;
            // 
            // VLS3
            // 
            this.VLS3.Controls.Add(this.VLSclb3Gr1);
            this.VLS3.Location = new System.Drawing.Point(4, 49);
            this.VLS3.Name = "VLS3";
            this.VLS3.Size = new System.Drawing.Size(395, 366);
            this.VLS3.TabIndex = 2;
            this.VLS3.Text = "ВЛС   3";
            this.VLS3.UseVisualStyleBackColor = true;
            // 
            // VLSclb3Gr1
            // 
            this.VLSclb3Gr1.CheckOnClick = true;
            this.VLSclb3Gr1.FormattingEnabled = true;
            this.VLSclb3Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb3Gr1.Name = "VLSclb3Gr1";
            this.VLSclb3Gr1.ScrollAlwaysVisible = true;
            this.VLSclb3Gr1.Size = new System.Drawing.Size(184, 289);
            this.VLSclb3Gr1.TabIndex = 6;
            // 
            // VLS4
            // 
            this.VLS4.Controls.Add(this.VLSclb4Gr1);
            this.VLS4.Location = new System.Drawing.Point(4, 49);
            this.VLS4.Name = "VLS4";
            this.VLS4.Size = new System.Drawing.Size(395, 366);
            this.VLS4.TabIndex = 3;
            this.VLS4.Text = "ВЛС 4";
            this.VLS4.UseVisualStyleBackColor = true;
            // 
            // VLSclb4Gr1
            // 
            this.VLSclb4Gr1.CheckOnClick = true;
            this.VLSclb4Gr1.FormattingEnabled = true;
            this.VLSclb4Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb4Gr1.Name = "VLSclb4Gr1";
            this.VLSclb4Gr1.ScrollAlwaysVisible = true;
            this.VLSclb4Gr1.Size = new System.Drawing.Size(184, 289);
            this.VLSclb4Gr1.TabIndex = 6;
            // 
            // VLS5
            // 
            this.VLS5.Controls.Add(this.VLSclb5Gr1);
            this.VLS5.Location = new System.Drawing.Point(4, 49);
            this.VLS5.Name = "VLS5";
            this.VLS5.Size = new System.Drawing.Size(395, 366);
            this.VLS5.TabIndex = 4;
            this.VLS5.Text = "ВЛС   5";
            this.VLS5.UseVisualStyleBackColor = true;
            // 
            // VLSclb5Gr1
            // 
            this.VLSclb5Gr1.CheckOnClick = true;
            this.VLSclb5Gr1.FormattingEnabled = true;
            this.VLSclb5Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb5Gr1.Name = "VLSclb5Gr1";
            this.VLSclb5Gr1.ScrollAlwaysVisible = true;
            this.VLSclb5Gr1.Size = new System.Drawing.Size(184, 289);
            this.VLSclb5Gr1.TabIndex = 6;
            // 
            // VLS6
            // 
            this.VLS6.Controls.Add(this.VLSclb6Gr1);
            this.VLS6.Location = new System.Drawing.Point(4, 49);
            this.VLS6.Name = "VLS6";
            this.VLS6.Size = new System.Drawing.Size(395, 366);
            this.VLS6.TabIndex = 5;
            this.VLS6.Text = "ВЛС  6";
            this.VLS6.UseVisualStyleBackColor = true;
            // 
            // VLSclb6Gr1
            // 
            this.VLSclb6Gr1.CheckOnClick = true;
            this.VLSclb6Gr1.FormattingEnabled = true;
            this.VLSclb6Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb6Gr1.Name = "VLSclb6Gr1";
            this.VLSclb6Gr1.ScrollAlwaysVisible = true;
            this.VLSclb6Gr1.Size = new System.Drawing.Size(184, 289);
            this.VLSclb6Gr1.TabIndex = 6;
            // 
            // VLS7
            // 
            this.VLS7.Controls.Add(this.VLSclb7Gr1);
            this.VLS7.Location = new System.Drawing.Point(4, 49);
            this.VLS7.Name = "VLS7";
            this.VLS7.Size = new System.Drawing.Size(395, 366);
            this.VLS7.TabIndex = 6;
            this.VLS7.Text = "ВЛС 7";
            this.VLS7.UseVisualStyleBackColor = true;
            // 
            // VLSclb7Gr1
            // 
            this.VLSclb7Gr1.CheckOnClick = true;
            this.VLSclb7Gr1.FormattingEnabled = true;
            this.VLSclb7Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb7Gr1.Name = "VLSclb7Gr1";
            this.VLSclb7Gr1.ScrollAlwaysVisible = true;
            this.VLSclb7Gr1.Size = new System.Drawing.Size(184, 289);
            this.VLSclb7Gr1.TabIndex = 6;
            // 
            // VLS8
            // 
            this.VLS8.Controls.Add(this.VLSclb8Gr1);
            this.VLS8.Location = new System.Drawing.Point(4, 49);
            this.VLS8.Name = "VLS8";
            this.VLS8.Size = new System.Drawing.Size(395, 366);
            this.VLS8.TabIndex = 7;
            this.VLS8.Text = "ВЛС   8";
            this.VLS8.UseVisualStyleBackColor = true;
            // 
            // VLSclb8Gr1
            // 
            this.VLSclb8Gr1.CheckOnClick = true;
            this.VLSclb8Gr1.FormattingEnabled = true;
            this.VLSclb8Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb8Gr1.Name = "VLSclb8Gr1";
            this.VLSclb8Gr1.ScrollAlwaysVisible = true;
            this.VLSclb8Gr1.Size = new System.Drawing.Size(184, 289);
            this.VLSclb8Gr1.TabIndex = 6;
            // 
            // VLS9
            // 
            this.VLS9.Controls.Add(this.VLSclb9Gr1);
            this.VLS9.Location = new System.Drawing.Point(4, 49);
            this.VLS9.Name = "VLS9";
            this.VLS9.Size = new System.Drawing.Size(395, 366);
            this.VLS9.TabIndex = 8;
            this.VLS9.Text = "ВЛС9";
            this.VLS9.UseVisualStyleBackColor = true;
            // 
            // VLSclb9Gr1
            // 
            this.VLSclb9Gr1.CheckOnClick = true;
            this.VLSclb9Gr1.FormattingEnabled = true;
            this.VLSclb9Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb9Gr1.Name = "VLSclb9Gr1";
            this.VLSclb9Gr1.ScrollAlwaysVisible = true;
            this.VLSclb9Gr1.Size = new System.Drawing.Size(184, 289);
            this.VLSclb9Gr1.TabIndex = 6;
            // 
            // VLS10
            // 
            this.VLS10.Controls.Add(this.VLSclb10Gr1);
            this.VLS10.Location = new System.Drawing.Point(4, 49);
            this.VLS10.Name = "VLS10";
            this.VLS10.Size = new System.Drawing.Size(395, 366);
            this.VLS10.TabIndex = 9;
            this.VLS10.Text = "ВЛС10";
            this.VLS10.UseVisualStyleBackColor = true;
            // 
            // VLSclb10Gr1
            // 
            this.VLSclb10Gr1.CheckOnClick = true;
            this.VLSclb10Gr1.FormattingEnabled = true;
            this.VLSclb10Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb10Gr1.Name = "VLSclb10Gr1";
            this.VLSclb10Gr1.ScrollAlwaysVisible = true;
            this.VLSclb10Gr1.Size = new System.Drawing.Size(184, 289);
            this.VLSclb10Gr1.TabIndex = 6;
            // 
            // VLS11
            // 
            this.VLS11.Controls.Add(this.VLSclb11Gr1);
            this.VLS11.Location = new System.Drawing.Point(4, 49);
            this.VLS11.Name = "VLS11";
            this.VLS11.Size = new System.Drawing.Size(395, 366);
            this.VLS11.TabIndex = 10;
            this.VLS11.Text = "ВЛС11";
            this.VLS11.UseVisualStyleBackColor = true;
            // 
            // VLSclb11Gr1
            // 
            this.VLSclb11Gr1.CheckOnClick = true;
            this.VLSclb11Gr1.FormattingEnabled = true;
            this.VLSclb11Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb11Gr1.Name = "VLSclb11Gr1";
            this.VLSclb11Gr1.ScrollAlwaysVisible = true;
            this.VLSclb11Gr1.Size = new System.Drawing.Size(184, 289);
            this.VLSclb11Gr1.TabIndex = 6;
            // 
            // VLS12
            // 
            this.VLS12.Controls.Add(this.VLSclb12Gr1);
            this.VLS12.Location = new System.Drawing.Point(4, 49);
            this.VLS12.Name = "VLS12";
            this.VLS12.Size = new System.Drawing.Size(395, 366);
            this.VLS12.TabIndex = 11;
            this.VLS12.Text = "ВЛС12";
            this.VLS12.UseVisualStyleBackColor = true;
            // 
            // VLSclb12Gr1
            // 
            this.VLSclb12Gr1.CheckOnClick = true;
            this.VLSclb12Gr1.FormattingEnabled = true;
            this.VLSclb12Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb12Gr1.Name = "VLSclb12Gr1";
            this.VLSclb12Gr1.ScrollAlwaysVisible = true;
            this.VLSclb12Gr1.Size = new System.Drawing.Size(184, 289);
            this.VLSclb12Gr1.TabIndex = 6;
            // 
            // VLS13
            // 
            this.VLS13.Controls.Add(this.VLSclb13Gr1);
            this.VLS13.Location = new System.Drawing.Point(4, 49);
            this.VLS13.Name = "VLS13";
            this.VLS13.Size = new System.Drawing.Size(395, 366);
            this.VLS13.TabIndex = 12;
            this.VLS13.Text = "ВЛС13";
            this.VLS13.UseVisualStyleBackColor = true;
            // 
            // VLSclb13Gr1
            // 
            this.VLSclb13Gr1.CheckOnClick = true;
            this.VLSclb13Gr1.FormattingEnabled = true;
            this.VLSclb13Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb13Gr1.Name = "VLSclb13Gr1";
            this.VLSclb13Gr1.ScrollAlwaysVisible = true;
            this.VLSclb13Gr1.Size = new System.Drawing.Size(184, 289);
            this.VLSclb13Gr1.TabIndex = 6;
            // 
            // VLS14
            // 
            this.VLS14.Controls.Add(this.VLSclb14Gr1);
            this.VLS14.Location = new System.Drawing.Point(4, 49);
            this.VLS14.Name = "VLS14";
            this.VLS14.Size = new System.Drawing.Size(395, 366);
            this.VLS14.TabIndex = 13;
            this.VLS14.Text = "ВЛС14";
            this.VLS14.UseVisualStyleBackColor = true;
            // 
            // VLSclb14Gr1
            // 
            this.VLSclb14Gr1.CheckOnClick = true;
            this.VLSclb14Gr1.FormattingEnabled = true;
            this.VLSclb14Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb14Gr1.Name = "VLSclb14Gr1";
            this.VLSclb14Gr1.ScrollAlwaysVisible = true;
            this.VLSclb14Gr1.Size = new System.Drawing.Size(184, 289);
            this.VLSclb14Gr1.TabIndex = 6;
            // 
            // VLS15
            // 
            this.VLS15.Controls.Add(this.VLSclb15Gr1);
            this.VLS15.Location = new System.Drawing.Point(4, 49);
            this.VLS15.Name = "VLS15";
            this.VLS15.Size = new System.Drawing.Size(395, 366);
            this.VLS15.TabIndex = 14;
            this.VLS15.Text = "ВЛС15";
            this.VLS15.UseVisualStyleBackColor = true;
            // 
            // VLSclb15Gr1
            // 
            this.VLSclb15Gr1.CheckOnClick = true;
            this.VLSclb15Gr1.FormattingEnabled = true;
            this.VLSclb15Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb15Gr1.Name = "VLSclb15Gr1";
            this.VLSclb15Gr1.ScrollAlwaysVisible = true;
            this.VLSclb15Gr1.Size = new System.Drawing.Size(184, 289);
            this.VLSclb15Gr1.TabIndex = 6;
            // 
            // VLS16
            // 
            this.VLS16.Controls.Add(this.VLSclb16Gr1);
            this.VLS16.Location = new System.Drawing.Point(4, 49);
            this.VLS16.Name = "VLS16";
            this.VLS16.Size = new System.Drawing.Size(395, 366);
            this.VLS16.TabIndex = 15;
            this.VLS16.Text = "ВЛС16";
            this.VLS16.UseVisualStyleBackColor = true;
            // 
            // VLSclb16Gr1
            // 
            this.VLSclb16Gr1.CheckOnClick = true;
            this.VLSclb16Gr1.FormattingEnabled = true;
            this.VLSclb16Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb16Gr1.Name = "VLSclb16Gr1";
            this.VLSclb16Gr1.ScrollAlwaysVisible = true;
            this.VLSclb16Gr1.Size = new System.Drawing.Size(184, 289);
            this.VLSclb16Gr1.TabIndex = 6;
            // 
            // groupBox26
            // 
            this.groupBox26.Controls.Add(this.tabControl1);
            this.groupBox26.Location = new System.Drawing.Point(207, 3);
            this.groupBox26.Name = "groupBox26";
            this.groupBox26.Size = new System.Drawing.Size(193, 437);
            this.groupBox26.TabIndex = 2;
            this.groupBox26.TabStop = false;
            this.groupBox26.Text = "Логические сигналы ИЛИ";
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabControl1.Controls.Add(this.tabPage31);
            this.tabControl1.Controls.Add(this.tabPage32);
            this.tabControl1.Controls.Add(this.tabPage33);
            this.tabControl1.Controls.Add(this.tabPage34);
            this.tabControl1.Controls.Add(this.tabPage35);
            this.tabControl1.Controls.Add(this.tabPage36);
            this.tabControl1.Controls.Add(this.tabPage37);
            this.tabControl1.Controls.Add(this.tabPage38);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(3, 16);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(187, 418);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPage31
            // 
            this.tabPage31.Controls.Add(this._lsOrDgv1Gr1);
            this.tabPage31.Location = new System.Drawing.Point(4, 49);
            this.tabPage31.Name = "tabPage31";
            this.tabPage31.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage31.Size = new System.Drawing.Size(179, 365);
            this.tabPage31.TabIndex = 0;
            this.tabPage31.Text = "ЛС9";
            this.tabPage31.UseVisualStyleBackColor = true;
            // 
            // _lsOrDgv1Gr1
            // 
            this._lsOrDgv1Gr1.AllowUserToAddRows = false;
            this._lsOrDgv1Gr1.AllowUserToDeleteRows = false;
            this._lsOrDgv1Gr1.AllowUserToResizeColumns = false;
            this._lsOrDgv1Gr1.AllowUserToResizeRows = false;
            this._lsOrDgv1Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsOrDgv1Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsOrDgv1Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsOrDgv1Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewComboBoxColumn15});
            this._lsOrDgv1Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsOrDgv1Gr1.MultiSelect = false;
            this._lsOrDgv1Gr1.Name = "_lsOrDgv1Gr1";
            this._lsOrDgv1Gr1.RowHeadersVisible = false;
            this._lsOrDgv1Gr1.RowHeadersWidth = 51;
            this._lsOrDgv1Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsOrDgv1Gr1.RowsDefaultCellStyle = dataGridViewCellStyle12;
            this._lsOrDgv1Gr1.RowTemplate.Height = 20;
            this._lsOrDgv1Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsOrDgv1Gr1.ShowCellErrors = false;
            this._lsOrDgv1Gr1.ShowCellToolTips = false;
            this._lsOrDgv1Gr1.ShowEditingIcon = false;
            this._lsOrDgv1Gr1.ShowRowErrors = false;
            this._lsOrDgv1Gr1.Size = new System.Drawing.Size(179, 365);
            this._lsOrDgv1Gr1.TabIndex = 2;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.HeaderText = "№";
            this.dataGridViewTextBoxColumn15.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            this.dataGridViewTextBoxColumn15.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn15.Width = 24;
            // 
            // dataGridViewComboBoxColumn15
            // 
            this.dataGridViewComboBoxColumn15.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn15.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn15.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn15.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn15.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn15.Name = "dataGridViewComboBoxColumn15";
            // 
            // tabPage32
            // 
            this.tabPage32.Controls.Add(this._lsOrDgv2Gr1);
            this.tabPage32.Location = new System.Drawing.Point(4, 49);
            this.tabPage32.Name = "tabPage32";
            this.tabPage32.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage32.Size = new System.Drawing.Size(179, 367);
            this.tabPage32.TabIndex = 1;
            this.tabPage32.Text = "ЛС10";
            this.tabPage32.UseVisualStyleBackColor = true;
            // 
            // _lsOrDgv2Gr1
            // 
            this._lsOrDgv2Gr1.AllowUserToAddRows = false;
            this._lsOrDgv2Gr1.AllowUserToDeleteRows = false;
            this._lsOrDgv2Gr1.AllowUserToResizeColumns = false;
            this._lsOrDgv2Gr1.AllowUserToResizeRows = false;
            this._lsOrDgv2Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsOrDgv2Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsOrDgv2Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsOrDgv2Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewComboBoxColumn16});
            this._lsOrDgv2Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsOrDgv2Gr1.MultiSelect = false;
            this._lsOrDgv2Gr1.Name = "_lsOrDgv2Gr1";
            this._lsOrDgv2Gr1.RowHeadersVisible = false;
            this._lsOrDgv2Gr1.RowHeadersWidth = 51;
            this._lsOrDgv2Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsOrDgv2Gr1.RowsDefaultCellStyle = dataGridViewCellStyle13;
            this._lsOrDgv2Gr1.RowTemplate.Height = 20;
            this._lsOrDgv2Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsOrDgv2Gr1.ShowCellErrors = false;
            this._lsOrDgv2Gr1.ShowCellToolTips = false;
            this._lsOrDgv2Gr1.ShowEditingIcon = false;
            this._lsOrDgv2Gr1.ShowRowErrors = false;
            this._lsOrDgv2Gr1.Size = new System.Drawing.Size(179, 365);
            this._lsOrDgv2Gr1.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.HeaderText = "№";
            this.dataGridViewTextBoxColumn16.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            this.dataGridViewTextBoxColumn16.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn16.Width = 24;
            // 
            // dataGridViewComboBoxColumn16
            // 
            this.dataGridViewComboBoxColumn16.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn16.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn16.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn16.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn16.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn16.Name = "dataGridViewComboBoxColumn16";
            // 
            // tabPage33
            // 
            this.tabPage33.Controls.Add(this._lsOrDgv3Gr1);
            this.tabPage33.Location = new System.Drawing.Point(4, 49);
            this.tabPage33.Name = "tabPage33";
            this.tabPage33.Size = new System.Drawing.Size(179, 367);
            this.tabPage33.TabIndex = 2;
            this.tabPage33.Text = "ЛС11";
            this.tabPage33.UseVisualStyleBackColor = true;
            // 
            // _lsOrDgv3Gr1
            // 
            this._lsOrDgv3Gr1.AllowUserToAddRows = false;
            this._lsOrDgv3Gr1.AllowUserToDeleteRows = false;
            this._lsOrDgv3Gr1.AllowUserToResizeColumns = false;
            this._lsOrDgv3Gr1.AllowUserToResizeRows = false;
            this._lsOrDgv3Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsOrDgv3Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsOrDgv3Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsOrDgv3Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn18,
            this.dataGridViewComboBoxColumn17});
            this._lsOrDgv3Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsOrDgv3Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsOrDgv3Gr1.MultiSelect = false;
            this._lsOrDgv3Gr1.Name = "_lsOrDgv3Gr1";
            this._lsOrDgv3Gr1.RowHeadersVisible = false;
            this._lsOrDgv3Gr1.RowHeadersWidth = 51;
            this._lsOrDgv3Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsOrDgv3Gr1.RowsDefaultCellStyle = dataGridViewCellStyle14;
            this._lsOrDgv3Gr1.RowTemplate.Height = 20;
            this._lsOrDgv3Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsOrDgv3Gr1.ShowCellErrors = false;
            this._lsOrDgv3Gr1.ShowCellToolTips = false;
            this._lsOrDgv3Gr1.ShowEditingIcon = false;
            this._lsOrDgv3Gr1.ShowRowErrors = false;
            this._lsOrDgv3Gr1.Size = new System.Drawing.Size(179, 367);
            this._lsOrDgv3Gr1.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.HeaderText = "№";
            this.dataGridViewTextBoxColumn18.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            this.dataGridViewTextBoxColumn18.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn18.Width = 24;
            // 
            // dataGridViewComboBoxColumn17
            // 
            this.dataGridViewComboBoxColumn17.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn17.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn17.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn17.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn17.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn17.Name = "dataGridViewComboBoxColumn17";
            // 
            // tabPage34
            // 
            this.tabPage34.Controls.Add(this._lsOrDgv4Gr1);
            this.tabPage34.Location = new System.Drawing.Point(4, 49);
            this.tabPage34.Name = "tabPage34";
            this.tabPage34.Size = new System.Drawing.Size(179, 367);
            this.tabPage34.TabIndex = 3;
            this.tabPage34.Text = "ЛС12";
            this.tabPage34.UseVisualStyleBackColor = true;
            // 
            // _lsOrDgv4Gr1
            // 
            this._lsOrDgv4Gr1.AllowUserToAddRows = false;
            this._lsOrDgv4Gr1.AllowUserToDeleteRows = false;
            this._lsOrDgv4Gr1.AllowUserToResizeColumns = false;
            this._lsOrDgv4Gr1.AllowUserToResizeRows = false;
            this._lsOrDgv4Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsOrDgv4Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsOrDgv4Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsOrDgv4Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn19,
            this.dataGridViewComboBoxColumn18});
            this._lsOrDgv4Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsOrDgv4Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsOrDgv4Gr1.MultiSelect = false;
            this._lsOrDgv4Gr1.Name = "_lsOrDgv4Gr1";
            this._lsOrDgv4Gr1.RowHeadersVisible = false;
            this._lsOrDgv4Gr1.RowHeadersWidth = 51;
            this._lsOrDgv4Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsOrDgv4Gr1.RowsDefaultCellStyle = dataGridViewCellStyle15;
            this._lsOrDgv4Gr1.RowTemplate.Height = 20;
            this._lsOrDgv4Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsOrDgv4Gr1.ShowCellErrors = false;
            this._lsOrDgv4Gr1.ShowCellToolTips = false;
            this._lsOrDgv4Gr1.ShowEditingIcon = false;
            this._lsOrDgv4Gr1.ShowRowErrors = false;
            this._lsOrDgv4Gr1.Size = new System.Drawing.Size(179, 367);
            this._lsOrDgv4Gr1.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.HeaderText = "№";
            this.dataGridViewTextBoxColumn19.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.ReadOnly = true;
            this.dataGridViewTextBoxColumn19.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn19.Width = 24;
            // 
            // dataGridViewComboBoxColumn18
            // 
            this.dataGridViewComboBoxColumn18.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn18.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn18.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn18.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn18.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn18.Name = "dataGridViewComboBoxColumn18";
            // 
            // tabPage35
            // 
            this.tabPage35.Controls.Add(this._lsOrDgv5Gr1);
            this.tabPage35.Location = new System.Drawing.Point(4, 49);
            this.tabPage35.Name = "tabPage35";
            this.tabPage35.Size = new System.Drawing.Size(179, 367);
            this.tabPage35.TabIndex = 4;
            this.tabPage35.Text = "ЛС13";
            this.tabPage35.UseVisualStyleBackColor = true;
            // 
            // _lsOrDgv5Gr1
            // 
            this._lsOrDgv5Gr1.AllowUserToAddRows = false;
            this._lsOrDgv5Gr1.AllowUserToDeleteRows = false;
            this._lsOrDgv5Gr1.AllowUserToResizeColumns = false;
            this._lsOrDgv5Gr1.AllowUserToResizeRows = false;
            this._lsOrDgv5Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsOrDgv5Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsOrDgv5Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsOrDgv5Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn20,
            this.dataGridViewComboBoxColumn19});
            this._lsOrDgv5Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsOrDgv5Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsOrDgv5Gr1.MultiSelect = false;
            this._lsOrDgv5Gr1.Name = "_lsOrDgv5Gr1";
            this._lsOrDgv5Gr1.RowHeadersVisible = false;
            this._lsOrDgv5Gr1.RowHeadersWidth = 51;
            this._lsOrDgv5Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsOrDgv5Gr1.RowsDefaultCellStyle = dataGridViewCellStyle16;
            this._lsOrDgv5Gr1.RowTemplate.Height = 20;
            this._lsOrDgv5Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsOrDgv5Gr1.ShowCellErrors = false;
            this._lsOrDgv5Gr1.ShowCellToolTips = false;
            this._lsOrDgv5Gr1.ShowEditingIcon = false;
            this._lsOrDgv5Gr1.ShowRowErrors = false;
            this._lsOrDgv5Gr1.Size = new System.Drawing.Size(179, 367);
            this._lsOrDgv5Gr1.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn20
            // 
            this.dataGridViewTextBoxColumn20.HeaderText = "№";
            this.dataGridViewTextBoxColumn20.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.ReadOnly = true;
            this.dataGridViewTextBoxColumn20.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn20.Width = 24;
            // 
            // dataGridViewComboBoxColumn19
            // 
            this.dataGridViewComboBoxColumn19.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn19.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn19.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn19.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn19.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn19.Name = "dataGridViewComboBoxColumn19";
            // 
            // tabPage36
            // 
            this.tabPage36.Controls.Add(this._lsOrDgv6Gr1);
            this.tabPage36.Location = new System.Drawing.Point(4, 49);
            this.tabPage36.Name = "tabPage36";
            this.tabPage36.Size = new System.Drawing.Size(179, 367);
            this.tabPage36.TabIndex = 5;
            this.tabPage36.Text = "ЛС14";
            this.tabPage36.UseVisualStyleBackColor = true;
            // 
            // _lsOrDgv6Gr1
            // 
            this._lsOrDgv6Gr1.AllowUserToAddRows = false;
            this._lsOrDgv6Gr1.AllowUserToDeleteRows = false;
            this._lsOrDgv6Gr1.AllowUserToResizeColumns = false;
            this._lsOrDgv6Gr1.AllowUserToResizeRows = false;
            this._lsOrDgv6Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsOrDgv6Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsOrDgv6Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsOrDgv6Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn21,
            this.dataGridViewComboBoxColumn20});
            this._lsOrDgv6Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsOrDgv6Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsOrDgv6Gr1.MultiSelect = false;
            this._lsOrDgv6Gr1.Name = "_lsOrDgv6Gr1";
            this._lsOrDgv6Gr1.RowHeadersVisible = false;
            this._lsOrDgv6Gr1.RowHeadersWidth = 51;
            this._lsOrDgv6Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsOrDgv6Gr1.RowsDefaultCellStyle = dataGridViewCellStyle17;
            this._lsOrDgv6Gr1.RowTemplate.Height = 20;
            this._lsOrDgv6Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsOrDgv6Gr1.ShowCellErrors = false;
            this._lsOrDgv6Gr1.ShowCellToolTips = false;
            this._lsOrDgv6Gr1.ShowEditingIcon = false;
            this._lsOrDgv6Gr1.ShowRowErrors = false;
            this._lsOrDgv6Gr1.Size = new System.Drawing.Size(179, 367);
            this._lsOrDgv6Gr1.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn21
            // 
            this.dataGridViewTextBoxColumn21.HeaderText = "№";
            this.dataGridViewTextBoxColumn21.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.ReadOnly = true;
            this.dataGridViewTextBoxColumn21.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn21.Width = 24;
            // 
            // dataGridViewComboBoxColumn20
            // 
            this.dataGridViewComboBoxColumn20.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn20.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn20.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn20.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn20.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn20.Name = "dataGridViewComboBoxColumn20";
            // 
            // tabPage37
            // 
            this.tabPage37.Controls.Add(this._lsOrDgv7Gr1);
            this.tabPage37.Location = new System.Drawing.Point(4, 49);
            this.tabPage37.Name = "tabPage37";
            this.tabPage37.Size = new System.Drawing.Size(179, 367);
            this.tabPage37.TabIndex = 6;
            this.tabPage37.Text = "ЛС15";
            this.tabPage37.UseVisualStyleBackColor = true;
            // 
            // _lsOrDgv7Gr1
            // 
            this._lsOrDgv7Gr1.AllowUserToAddRows = false;
            this._lsOrDgv7Gr1.AllowUserToDeleteRows = false;
            this._lsOrDgv7Gr1.AllowUserToResizeColumns = false;
            this._lsOrDgv7Gr1.AllowUserToResizeRows = false;
            this._lsOrDgv7Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsOrDgv7Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsOrDgv7Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsOrDgv7Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn22,
            this.dataGridViewComboBoxColumn21});
            this._lsOrDgv7Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsOrDgv7Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsOrDgv7Gr1.MultiSelect = false;
            this._lsOrDgv7Gr1.Name = "_lsOrDgv7Gr1";
            this._lsOrDgv7Gr1.RowHeadersVisible = false;
            this._lsOrDgv7Gr1.RowHeadersWidth = 51;
            this._lsOrDgv7Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsOrDgv7Gr1.RowsDefaultCellStyle = dataGridViewCellStyle18;
            this._lsOrDgv7Gr1.RowTemplate.Height = 20;
            this._lsOrDgv7Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsOrDgv7Gr1.ShowCellErrors = false;
            this._lsOrDgv7Gr1.ShowCellToolTips = false;
            this._lsOrDgv7Gr1.ShowEditingIcon = false;
            this._lsOrDgv7Gr1.ShowRowErrors = false;
            this._lsOrDgv7Gr1.Size = new System.Drawing.Size(179, 367);
            this._lsOrDgv7Gr1.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn22
            // 
            this.dataGridViewTextBoxColumn22.HeaderText = "№";
            this.dataGridViewTextBoxColumn22.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.ReadOnly = true;
            this.dataGridViewTextBoxColumn22.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn22.Width = 24;
            // 
            // dataGridViewComboBoxColumn21
            // 
            this.dataGridViewComboBoxColumn21.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn21.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn21.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn21.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn21.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn21.Name = "dataGridViewComboBoxColumn21";
            // 
            // tabPage38
            // 
            this.tabPage38.Controls.Add(this._lsOrDgv8Gr1);
            this.tabPage38.Location = new System.Drawing.Point(4, 49);
            this.tabPage38.Name = "tabPage38";
            this.tabPage38.Size = new System.Drawing.Size(179, 367);
            this.tabPage38.TabIndex = 7;
            this.tabPage38.Text = "ЛС16";
            this.tabPage38.UseVisualStyleBackColor = true;
            // 
            // _lsOrDgv8Gr1
            // 
            this._lsOrDgv8Gr1.AllowUserToAddRows = false;
            this._lsOrDgv8Gr1.AllowUserToDeleteRows = false;
            this._lsOrDgv8Gr1.AllowUserToResizeColumns = false;
            this._lsOrDgv8Gr1.AllowUserToResizeRows = false;
            this._lsOrDgv8Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsOrDgv8Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsOrDgv8Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsOrDgv8Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn23,
            this.dataGridViewComboBoxColumn22});
            this._lsOrDgv8Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsOrDgv8Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsOrDgv8Gr1.MultiSelect = false;
            this._lsOrDgv8Gr1.Name = "_lsOrDgv8Gr1";
            this._lsOrDgv8Gr1.RowHeadersVisible = false;
            this._lsOrDgv8Gr1.RowHeadersWidth = 51;
            this._lsOrDgv8Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsOrDgv8Gr1.RowsDefaultCellStyle = dataGridViewCellStyle19;
            this._lsOrDgv8Gr1.RowTemplate.Height = 20;
            this._lsOrDgv8Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsOrDgv8Gr1.ShowCellErrors = false;
            this._lsOrDgv8Gr1.ShowCellToolTips = false;
            this._lsOrDgv8Gr1.ShowEditingIcon = false;
            this._lsOrDgv8Gr1.ShowRowErrors = false;
            this._lsOrDgv8Gr1.Size = new System.Drawing.Size(179, 367);
            this._lsOrDgv8Gr1.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn23
            // 
            this.dataGridViewTextBoxColumn23.HeaderText = "№";
            this.dataGridViewTextBoxColumn23.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            this.dataGridViewTextBoxColumn23.ReadOnly = true;
            this.dataGridViewTextBoxColumn23.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn23.Width = 24;
            // 
            // dataGridViewComboBoxColumn22
            // 
            this.dataGridViewComboBoxColumn22.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn22.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn22.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn22.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn22.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn22.Name = "dataGridViewComboBoxColumn22";
            // 
            // groupBox45
            // 
            this.groupBox45.Controls.Add(this.tabControl);
            this.groupBox45.Location = new System.Drawing.Point(8, 3);
            this.groupBox45.Name = "groupBox45";
            this.groupBox45.Size = new System.Drawing.Size(193, 437);
            this.groupBox45.TabIndex = 0;
            this.groupBox45.TabStop = false;
            this.groupBox45.Text = "Логические сигналы И";
            // 
            // tabControl
            // 
            this.tabControl.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabControl.Controls.Add(this.tabPage39);
            this.tabControl.Controls.Add(this.tabPage40);
            this.tabControl.Controls.Add(this.tabPage41);
            this.tabControl.Controls.Add(this.tabPage42);
            this.tabControl.Controls.Add(this.tabPage43);
            this.tabControl.Controls.Add(this.tabPage44);
            this.tabControl.Controls.Add(this.tabPage45);
            this.tabControl.Controls.Add(this.tabPage46);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(3, 16);
            this.tabControl.Multiline = true;
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(187, 418);
            this.tabControl.TabIndex = 2;
            // 
            // tabPage39
            // 
            this.tabPage39.Controls.Add(this._lsAndDgv1Gr1);
            this.tabPage39.Location = new System.Drawing.Point(4, 49);
            this.tabPage39.Name = "tabPage39";
            this.tabPage39.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage39.Size = new System.Drawing.Size(179, 365);
            this.tabPage39.TabIndex = 0;
            this.tabPage39.Text = "ЛС1";
            this.tabPage39.UseVisualStyleBackColor = true;
            // 
            // _lsAndDgv1Gr1
            // 
            this._lsAndDgv1Gr1.AllowUserToAddRows = false;
            this._lsAndDgv1Gr1.AllowUserToDeleteRows = false;
            this._lsAndDgv1Gr1.AllowUserToResizeColumns = false;
            this._lsAndDgv1Gr1.AllowUserToResizeRows = false;
            this._lsAndDgv1Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsAndDgv1Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsAndDgv1Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsAndDgv1Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn24,
            this.dataGridViewComboBoxColumn23});
            this._lsAndDgv1Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsAndDgv1Gr1.MultiSelect = false;
            this._lsAndDgv1Gr1.Name = "_lsAndDgv1Gr1";
            this._lsAndDgv1Gr1.RowHeadersVisible = false;
            this._lsAndDgv1Gr1.RowHeadersWidth = 51;
            this._lsAndDgv1Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsAndDgv1Gr1.RowsDefaultCellStyle = dataGridViewCellStyle20;
            this._lsAndDgv1Gr1.RowTemplate.Height = 20;
            this._lsAndDgv1Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsAndDgv1Gr1.ShowCellErrors = false;
            this._lsAndDgv1Gr1.ShowCellToolTips = false;
            this._lsAndDgv1Gr1.ShowEditingIcon = false;
            this._lsAndDgv1Gr1.ShowRowErrors = false;
            this._lsAndDgv1Gr1.Size = new System.Drawing.Size(179, 365);
            this._lsAndDgv1Gr1.TabIndex = 2;
            // 
            // dataGridViewTextBoxColumn24
            // 
            this.dataGridViewTextBoxColumn24.HeaderText = "№";
            this.dataGridViewTextBoxColumn24.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            this.dataGridViewTextBoxColumn24.ReadOnly = true;
            this.dataGridViewTextBoxColumn24.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn24.Width = 24;
            // 
            // dataGridViewComboBoxColumn23
            // 
            this.dataGridViewComboBoxColumn23.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn23.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn23.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn23.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn23.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn23.Name = "dataGridViewComboBoxColumn23";
            // 
            // tabPage40
            // 
            this.tabPage40.Controls.Add(this._lsAndDgv2Gr1);
            this.tabPage40.Location = new System.Drawing.Point(4, 49);
            this.tabPage40.Name = "tabPage40";
            this.tabPage40.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage40.Size = new System.Drawing.Size(179, 367);
            this.tabPage40.TabIndex = 1;
            this.tabPage40.Text = "ЛС2";
            this.tabPage40.UseVisualStyleBackColor = true;
            // 
            // _lsAndDgv2Gr1
            // 
            this._lsAndDgv2Gr1.AllowUserToAddRows = false;
            this._lsAndDgv2Gr1.AllowUserToDeleteRows = false;
            this._lsAndDgv2Gr1.AllowUserToResizeColumns = false;
            this._lsAndDgv2Gr1.AllowUserToResizeRows = false;
            this._lsAndDgv2Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsAndDgv2Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsAndDgv2Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsAndDgv2Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn25,
            this.dataGridViewComboBoxColumn24});
            this._lsAndDgv2Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsAndDgv2Gr1.MultiSelect = false;
            this._lsAndDgv2Gr1.Name = "_lsAndDgv2Gr1";
            this._lsAndDgv2Gr1.RowHeadersVisible = false;
            this._lsAndDgv2Gr1.RowHeadersWidth = 51;
            this._lsAndDgv2Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsAndDgv2Gr1.RowsDefaultCellStyle = dataGridViewCellStyle21;
            this._lsAndDgv2Gr1.RowTemplate.Height = 20;
            this._lsAndDgv2Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsAndDgv2Gr1.ShowCellErrors = false;
            this._lsAndDgv2Gr1.ShowCellToolTips = false;
            this._lsAndDgv2Gr1.ShowEditingIcon = false;
            this._lsAndDgv2Gr1.ShowRowErrors = false;
            this._lsAndDgv2Gr1.Size = new System.Drawing.Size(179, 365);
            this._lsAndDgv2Gr1.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn25
            // 
            this.dataGridViewTextBoxColumn25.HeaderText = "№";
            this.dataGridViewTextBoxColumn25.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn25.Name = "dataGridViewTextBoxColumn25";
            this.dataGridViewTextBoxColumn25.ReadOnly = true;
            this.dataGridViewTextBoxColumn25.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn25.Width = 24;
            // 
            // dataGridViewComboBoxColumn24
            // 
            this.dataGridViewComboBoxColumn24.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn24.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn24.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn24.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn24.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn24.Name = "dataGridViewComboBoxColumn24";
            // 
            // tabPage41
            // 
            this.tabPage41.Controls.Add(this._lsAndDgv3Gr1);
            this.tabPage41.Location = new System.Drawing.Point(4, 49);
            this.tabPage41.Name = "tabPage41";
            this.tabPage41.Size = new System.Drawing.Size(179, 367);
            this.tabPage41.TabIndex = 2;
            this.tabPage41.Text = "ЛС3";
            this.tabPage41.UseVisualStyleBackColor = true;
            // 
            // _lsAndDgv3Gr1
            // 
            this._lsAndDgv3Gr1.AllowUserToAddRows = false;
            this._lsAndDgv3Gr1.AllowUserToDeleteRows = false;
            this._lsAndDgv3Gr1.AllowUserToResizeColumns = false;
            this._lsAndDgv3Gr1.AllowUserToResizeRows = false;
            this._lsAndDgv3Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsAndDgv3Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsAndDgv3Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsAndDgv3Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn26,
            this.dataGridViewComboBoxColumn25});
            this._lsAndDgv3Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsAndDgv3Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsAndDgv3Gr1.MultiSelect = false;
            this._lsAndDgv3Gr1.Name = "_lsAndDgv3Gr1";
            this._lsAndDgv3Gr1.RowHeadersVisible = false;
            this._lsAndDgv3Gr1.RowHeadersWidth = 51;
            this._lsAndDgv3Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsAndDgv3Gr1.RowsDefaultCellStyle = dataGridViewCellStyle22;
            this._lsAndDgv3Gr1.RowTemplate.Height = 20;
            this._lsAndDgv3Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsAndDgv3Gr1.ShowCellErrors = false;
            this._lsAndDgv3Gr1.ShowCellToolTips = false;
            this._lsAndDgv3Gr1.ShowEditingIcon = false;
            this._lsAndDgv3Gr1.ShowRowErrors = false;
            this._lsAndDgv3Gr1.Size = new System.Drawing.Size(179, 367);
            this._lsAndDgv3Gr1.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn26
            // 
            this.dataGridViewTextBoxColumn26.HeaderText = "№";
            this.dataGridViewTextBoxColumn26.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn26.Name = "dataGridViewTextBoxColumn26";
            this.dataGridViewTextBoxColumn26.ReadOnly = true;
            this.dataGridViewTextBoxColumn26.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn26.Width = 24;
            // 
            // dataGridViewComboBoxColumn25
            // 
            this.dataGridViewComboBoxColumn25.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn25.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn25.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn25.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn25.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn25.Name = "dataGridViewComboBoxColumn25";
            // 
            // tabPage42
            // 
            this.tabPage42.Controls.Add(this._lsAndDgv4Gr1);
            this.tabPage42.Location = new System.Drawing.Point(4, 49);
            this.tabPage42.Name = "tabPage42";
            this.tabPage42.Size = new System.Drawing.Size(179, 367);
            this.tabPage42.TabIndex = 3;
            this.tabPage42.Text = "ЛС4";
            this.tabPage42.UseVisualStyleBackColor = true;
            // 
            // _lsAndDgv4Gr1
            // 
            this._lsAndDgv4Gr1.AllowUserToAddRows = false;
            this._lsAndDgv4Gr1.AllowUserToDeleteRows = false;
            this._lsAndDgv4Gr1.AllowUserToResizeColumns = false;
            this._lsAndDgv4Gr1.AllowUserToResizeRows = false;
            this._lsAndDgv4Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsAndDgv4Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsAndDgv4Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsAndDgv4Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn27,
            this.dataGridViewComboBoxColumn26});
            this._lsAndDgv4Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsAndDgv4Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsAndDgv4Gr1.MultiSelect = false;
            this._lsAndDgv4Gr1.Name = "_lsAndDgv4Gr1";
            this._lsAndDgv4Gr1.RowHeadersVisible = false;
            this._lsAndDgv4Gr1.RowHeadersWidth = 51;
            this._lsAndDgv4Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsAndDgv4Gr1.RowsDefaultCellStyle = dataGridViewCellStyle23;
            this._lsAndDgv4Gr1.RowTemplate.Height = 20;
            this._lsAndDgv4Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsAndDgv4Gr1.ShowCellErrors = false;
            this._lsAndDgv4Gr1.ShowCellToolTips = false;
            this._lsAndDgv4Gr1.ShowEditingIcon = false;
            this._lsAndDgv4Gr1.ShowRowErrors = false;
            this._lsAndDgv4Gr1.Size = new System.Drawing.Size(179, 367);
            this._lsAndDgv4Gr1.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn27
            // 
            this.dataGridViewTextBoxColumn27.HeaderText = "№";
            this.dataGridViewTextBoxColumn27.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn27.Name = "dataGridViewTextBoxColumn27";
            this.dataGridViewTextBoxColumn27.ReadOnly = true;
            this.dataGridViewTextBoxColumn27.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn27.Width = 24;
            // 
            // dataGridViewComboBoxColumn26
            // 
            this.dataGridViewComboBoxColumn26.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn26.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn26.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn26.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn26.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn26.Name = "dataGridViewComboBoxColumn26";
            // 
            // tabPage43
            // 
            this.tabPage43.Controls.Add(this._lsAndDgv5Gr1);
            this.tabPage43.Location = new System.Drawing.Point(4, 49);
            this.tabPage43.Name = "tabPage43";
            this.tabPage43.Size = new System.Drawing.Size(179, 367);
            this.tabPage43.TabIndex = 4;
            this.tabPage43.Text = "ЛС5";
            this.tabPage43.UseVisualStyleBackColor = true;
            // 
            // _lsAndDgv5Gr1
            // 
            this._lsAndDgv5Gr1.AllowUserToAddRows = false;
            this._lsAndDgv5Gr1.AllowUserToDeleteRows = false;
            this._lsAndDgv5Gr1.AllowUserToResizeColumns = false;
            this._lsAndDgv5Gr1.AllowUserToResizeRows = false;
            this._lsAndDgv5Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsAndDgv5Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsAndDgv5Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsAndDgv5Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn28,
            this.dataGridViewComboBoxColumn27});
            this._lsAndDgv5Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsAndDgv5Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsAndDgv5Gr1.MultiSelect = false;
            this._lsAndDgv5Gr1.Name = "_lsAndDgv5Gr1";
            this._lsAndDgv5Gr1.RowHeadersVisible = false;
            this._lsAndDgv5Gr1.RowHeadersWidth = 51;
            this._lsAndDgv5Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsAndDgv5Gr1.RowsDefaultCellStyle = dataGridViewCellStyle24;
            this._lsAndDgv5Gr1.RowTemplate.Height = 20;
            this._lsAndDgv5Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsAndDgv5Gr1.ShowCellErrors = false;
            this._lsAndDgv5Gr1.ShowCellToolTips = false;
            this._lsAndDgv5Gr1.ShowEditingIcon = false;
            this._lsAndDgv5Gr1.ShowRowErrors = false;
            this._lsAndDgv5Gr1.Size = new System.Drawing.Size(179, 367);
            this._lsAndDgv5Gr1.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn28
            // 
            this.dataGridViewTextBoxColumn28.HeaderText = "№";
            this.dataGridViewTextBoxColumn28.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn28.Name = "dataGridViewTextBoxColumn28";
            this.dataGridViewTextBoxColumn28.ReadOnly = true;
            this.dataGridViewTextBoxColumn28.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn28.Width = 24;
            // 
            // dataGridViewComboBoxColumn27
            // 
            this.dataGridViewComboBoxColumn27.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn27.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn27.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn27.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn27.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn27.Name = "dataGridViewComboBoxColumn27";
            // 
            // tabPage44
            // 
            this.tabPage44.Controls.Add(this._lsAndDgv6Gr1);
            this.tabPage44.Location = new System.Drawing.Point(4, 49);
            this.tabPage44.Name = "tabPage44";
            this.tabPage44.Size = new System.Drawing.Size(179, 367);
            this.tabPage44.TabIndex = 5;
            this.tabPage44.Text = "ЛС6";
            this.tabPage44.UseVisualStyleBackColor = true;
            // 
            // _lsAndDgv6Gr1
            // 
            this._lsAndDgv6Gr1.AllowUserToAddRows = false;
            this._lsAndDgv6Gr1.AllowUserToDeleteRows = false;
            this._lsAndDgv6Gr1.AllowUserToResizeColumns = false;
            this._lsAndDgv6Gr1.AllowUserToResizeRows = false;
            this._lsAndDgv6Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsAndDgv6Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsAndDgv6Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsAndDgv6Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn29,
            this.dataGridViewComboBoxColumn28});
            this._lsAndDgv6Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsAndDgv6Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsAndDgv6Gr1.MultiSelect = false;
            this._lsAndDgv6Gr1.Name = "_lsAndDgv6Gr1";
            this._lsAndDgv6Gr1.RowHeadersVisible = false;
            this._lsAndDgv6Gr1.RowHeadersWidth = 51;
            this._lsAndDgv6Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsAndDgv6Gr1.RowsDefaultCellStyle = dataGridViewCellStyle25;
            this._lsAndDgv6Gr1.RowTemplate.Height = 20;
            this._lsAndDgv6Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsAndDgv6Gr1.ShowCellErrors = false;
            this._lsAndDgv6Gr1.ShowCellToolTips = false;
            this._lsAndDgv6Gr1.ShowEditingIcon = false;
            this._lsAndDgv6Gr1.ShowRowErrors = false;
            this._lsAndDgv6Gr1.Size = new System.Drawing.Size(179, 367);
            this._lsAndDgv6Gr1.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn29
            // 
            this.dataGridViewTextBoxColumn29.HeaderText = "№";
            this.dataGridViewTextBoxColumn29.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn29.Name = "dataGridViewTextBoxColumn29";
            this.dataGridViewTextBoxColumn29.ReadOnly = true;
            this.dataGridViewTextBoxColumn29.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn29.Width = 24;
            // 
            // dataGridViewComboBoxColumn28
            // 
            this.dataGridViewComboBoxColumn28.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn28.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn28.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn28.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn28.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn28.Name = "dataGridViewComboBoxColumn28";
            // 
            // tabPage45
            // 
            this.tabPage45.Controls.Add(this._lsAndDgv7Gr1);
            this.tabPage45.Location = new System.Drawing.Point(4, 49);
            this.tabPage45.Name = "tabPage45";
            this.tabPage45.Size = new System.Drawing.Size(179, 367);
            this.tabPage45.TabIndex = 6;
            this.tabPage45.Text = "ЛС7";
            this.tabPage45.UseVisualStyleBackColor = true;
            // 
            // _lsAndDgv7Gr1
            // 
            this._lsAndDgv7Gr1.AllowUserToAddRows = false;
            this._lsAndDgv7Gr1.AllowUserToDeleteRows = false;
            this._lsAndDgv7Gr1.AllowUserToResizeColumns = false;
            this._lsAndDgv7Gr1.AllowUserToResizeRows = false;
            this._lsAndDgv7Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsAndDgv7Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsAndDgv7Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsAndDgv7Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn30,
            this.dataGridViewComboBoxColumn29});
            this._lsAndDgv7Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsAndDgv7Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsAndDgv7Gr1.MultiSelect = false;
            this._lsAndDgv7Gr1.Name = "_lsAndDgv7Gr1";
            this._lsAndDgv7Gr1.RowHeadersVisible = false;
            this._lsAndDgv7Gr1.RowHeadersWidth = 51;
            this._lsAndDgv7Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsAndDgv7Gr1.RowsDefaultCellStyle = dataGridViewCellStyle26;
            this._lsAndDgv7Gr1.RowTemplate.Height = 20;
            this._lsAndDgv7Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsAndDgv7Gr1.ShowCellErrors = false;
            this._lsAndDgv7Gr1.ShowCellToolTips = false;
            this._lsAndDgv7Gr1.ShowEditingIcon = false;
            this._lsAndDgv7Gr1.ShowRowErrors = false;
            this._lsAndDgv7Gr1.Size = new System.Drawing.Size(179, 367);
            this._lsAndDgv7Gr1.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn30
            // 
            this.dataGridViewTextBoxColumn30.HeaderText = "№";
            this.dataGridViewTextBoxColumn30.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn30.Name = "dataGridViewTextBoxColumn30";
            this.dataGridViewTextBoxColumn30.ReadOnly = true;
            this.dataGridViewTextBoxColumn30.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn30.Width = 24;
            // 
            // dataGridViewComboBoxColumn29
            // 
            this.dataGridViewComboBoxColumn29.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn29.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn29.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn29.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn29.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn29.Name = "dataGridViewComboBoxColumn29";
            // 
            // tabPage46
            // 
            this.tabPage46.Controls.Add(this._lsAndDgv8Gr1);
            this.tabPage46.Location = new System.Drawing.Point(4, 49);
            this.tabPage46.Name = "tabPage46";
            this.tabPage46.Size = new System.Drawing.Size(179, 367);
            this.tabPage46.TabIndex = 7;
            this.tabPage46.Text = "ЛС8";
            this.tabPage46.UseVisualStyleBackColor = true;
            // 
            // _lsAndDgv8Gr1
            // 
            this._lsAndDgv8Gr1.AllowUserToAddRows = false;
            this._lsAndDgv8Gr1.AllowUserToDeleteRows = false;
            this._lsAndDgv8Gr1.AllowUserToResizeColumns = false;
            this._lsAndDgv8Gr1.AllowUserToResizeRows = false;
            this._lsAndDgv8Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsAndDgv8Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsAndDgv8Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsAndDgv8Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn31,
            this.dataGridViewComboBoxColumn30});
            this._lsAndDgv8Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsAndDgv8Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsAndDgv8Gr1.MultiSelect = false;
            this._lsAndDgv8Gr1.Name = "_lsAndDgv8Gr1";
            this._lsAndDgv8Gr1.RowHeadersVisible = false;
            this._lsAndDgv8Gr1.RowHeadersWidth = 51;
            this._lsAndDgv8Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsAndDgv8Gr1.RowsDefaultCellStyle = dataGridViewCellStyle27;
            this._lsAndDgv8Gr1.RowTemplate.Height = 20;
            this._lsAndDgv8Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsAndDgv8Gr1.ShowCellErrors = false;
            this._lsAndDgv8Gr1.ShowCellToolTips = false;
            this._lsAndDgv8Gr1.ShowEditingIcon = false;
            this._lsAndDgv8Gr1.ShowRowErrors = false;
            this._lsAndDgv8Gr1.Size = new System.Drawing.Size(179, 367);
            this._lsAndDgv8Gr1.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn31
            // 
            this.dataGridViewTextBoxColumn31.HeaderText = "№";
            this.dataGridViewTextBoxColumn31.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn31.Name = "dataGridViewTextBoxColumn31";
            this.dataGridViewTextBoxColumn31.ReadOnly = true;
            this.dataGridViewTextBoxColumn31.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn31.Width = 24;
            // 
            // dataGridViewComboBoxColumn30
            // 
            this.dataGridViewComboBoxColumn30.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn30.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn30.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn30.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn30.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn30.Name = "dataGridViewComboBoxColumn30";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this._setpointsComboBox);
            this.groupBox7.Location = new System.Drawing.Point(3, 3);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(103, 54);
            this.groupBox7.TabIndex = 2;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Группы уставок";
            // 
            // _setpointsComboBox
            // 
            this._setpointsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._setpointsComboBox.FormattingEnabled = true;
            this._setpointsComboBox.Location = new System.Drawing.Point(6, 19);
            this._setpointsComboBox.Name = "_setpointsComboBox";
            this._setpointsComboBox.Size = new System.Drawing.Size(88, 21);
            this._setpointsComboBox.TabIndex = 0;
            // 
            // _automatPage
            // 
            this._automatPage.Controls.Add(this.groupBox32);
            this._automatPage.Controls.Add(this.groupBox33);
            this._automatPage.Location = new System.Drawing.Point(4, 22);
            this._automatPage.Name = "_automatPage";
            this._automatPage.Size = new System.Drawing.Size(976, 553);
            this._automatPage.TabIndex = 9;
            this._automatPage.Text = "Выключатель";
            this._automatPage.UseVisualStyleBackColor = true;
            // 
            // groupBox32
            // 
            this.groupBox32.Controls.Add(this._switchImp);
            this.groupBox32.Controls.Add(this._controlSolenoidCombo);
            this.groupBox32.Controls.Add(this._switchYesNo);
            this.groupBox32.Controls.Add(this.label96);
            this.groupBox32.Controls.Add(this._switchBlock);
            this.groupBox32.Controls.Add(this.label99);
            this.groupBox32.Controls.Add(this._switchError);
            this.groupBox32.Controls.Add(this._comandOtkl);
            this.groupBox32.Controls.Add(this._switchOn);
            this.groupBox32.Controls.Add(this.label51);
            this.groupBox32.Controls.Add(this._switchOff);
            this.groupBox32.Controls.Add(this.label98);
            this.groupBox32.Controls.Add(this.label93);
            this.groupBox32.Controls.Add(this.label97);
            this.groupBox32.Controls.Add(this.label90);
            this.groupBox32.Controls.Add(this._switchKontCep);
            this.groupBox32.Controls.Add(this.label89);
            this.groupBox32.Controls.Add(this._switchTUskor);
            this.groupBox32.Controls.Add(this.label88);
            this.groupBox32.Location = new System.Drawing.Point(8, 3);
            this.groupBox32.Name = "groupBox32";
            this.groupBox32.Size = new System.Drawing.Size(253, 211);
            this.groupBox32.TabIndex = 14;
            this.groupBox32.TabStop = false;
            this.groupBox32.Text = "Выключатель";
            // 
            // _switchImp
            // 
            this._switchImp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._switchImp.Location = new System.Drawing.Point(142, 96);
            this._switchImp.Name = "_switchImp";
            this._switchImp.Size = new System.Drawing.Size(105, 20);
            this._switchImp.TabIndex = 24;
            this._switchImp.Tag = "3276700";
            this._switchImp.Text = "0";
            this._switchImp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _controlSolenoidCombo
            // 
            this._controlSolenoidCombo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._controlSolenoidCombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._controlSolenoidCombo.FormattingEnabled = true;
            this._controlSolenoidCombo.Location = new System.Drawing.Point(142, 186);
            this._controlSolenoidCombo.Name = "_controlSolenoidCombo";
            this._controlSolenoidCombo.Size = new System.Drawing.Size(105, 21);
            this._controlSolenoidCombo.TabIndex = 26;
            // 
            // _switchYesNo
            // 
            this._switchYesNo.AutoSize = true;
            this._switchYesNo.Location = new System.Drawing.Point(80, 1);
            this._switchYesNo.Name = "_switchYesNo";
            this._switchYesNo.Size = new System.Drawing.Size(15, 14);
            this._switchYesNo.TabIndex = 29;
            this._switchYesNo.UseVisualStyleBackColor = true;
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(6, 118);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(115, 13);
            this.label96.TabIndex = 16;
            this.label96.Text = "Команда отключения";
            // 
            // _switchBlock
            // 
            this._switchBlock.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._switchBlock.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._switchBlock.BackColor = System.Drawing.SystemColors.Window;
            this._switchBlock.FormattingEnabled = true;
            this._switchBlock.Location = new System.Drawing.Point(142, 75);
            this._switchBlock.Name = "_switchBlock";
            this._switchBlock.Size = new System.Drawing.Size(105, 21);
            this._switchBlock.TabIndex = 21;
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(6, 99);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(139, 13);
            this.label99.TabIndex = 15;
            this.label99.Text = "Импульс сигнала упр., мс";
            // 
            // _switchError
            // 
            this._switchError.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._switchError.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._switchError.FormattingEnabled = true;
            this._switchError.Location = new System.Drawing.Point(142, 55);
            this._switchError.Name = "_switchError";
            this._switchError.Size = new System.Drawing.Size(105, 21);
            this._switchError.TabIndex = 20;
            // 
            // _comandOtkl
            // 
            this._comandOtkl.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._comandOtkl.FormattingEnabled = true;
            this._comandOtkl.Location = new System.Drawing.Point(142, 115);
            this._comandOtkl.Name = "_comandOtkl";
            this._comandOtkl.Size = new System.Drawing.Size(105, 21);
            this._comandOtkl.TabIndex = 27;
            // 
            // _switchOn
            // 
            this._switchOn.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._switchOn.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._switchOn.FormattingEnabled = true;
            this._switchOn.Location = new System.Drawing.Point(142, 35);
            this._switchOn.Name = "_switchOn";
            this._switchOn.Size = new System.Drawing.Size(105, 21);
            this._switchOn.TabIndex = 19;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(6, 182);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(124, 26);
            this.label51.TabIndex = 17;
            this.label51.Text = "Контроль второго\r\nсоленоида отключения";
            // 
            // _switchOff
            // 
            this._switchOff.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._switchOff.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._switchOff.FormattingEnabled = true;
            this._switchOff.Location = new System.Drawing.Point(142, 15);
            this._switchOff.Name = "_switchOff";
            this._switchOff.Size = new System.Drawing.Size(105, 21);
            this._switchOff.TabIndex = 18;
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(7, 137);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(59, 13);
            this.label98.TabIndex = 16;
            this.label98.Text = "tускор, мс";
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Location = new System.Drawing.Point(6, 78);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(94, 13);
            this.label93.TabIndex = 12;
            this.label93.Text = "Вход блокировки";
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(6, 154);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(88, 26);
            this.label97.TabIndex = 17;
            this.label97.Text = "Контроль цепей\r\nуправления";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Location = new System.Drawing.Point(6, 58);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(111, 13);
            this.label90.TabIndex = 11;
            this.label90.Text = "Вход неисправности";
            // 
            // _switchKontCep
            // 
            this._switchKontCep.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switchKontCep.FormattingEnabled = true;
            this._switchKontCep.Location = new System.Drawing.Point(142, 154);
            this._switchKontCep.Name = "_switchKontCep";
            this._switchKontCep.Size = new System.Drawing.Size(105, 21);
            this._switchKontCep.TabIndex = 26;
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(6, 38);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(124, 13);
            this.label89.TabIndex = 10;
            this.label89.Text = "Состояние \"Включено\"";
            // 
            // _switchTUskor
            // 
            this._switchTUskor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._switchTUskor.Location = new System.Drawing.Point(142, 135);
            this._switchTUskor.Name = "_switchTUskor";
            this._switchTUskor.Size = new System.Drawing.Size(105, 20);
            this._switchTUskor.TabIndex = 25;
            this._switchTUskor.Tag = "3276700";
            this._switchTUskor.Text = "20";
            this._switchTUskor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(6, 18);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(130, 13);
            this.label88.TabIndex = 9;
            this.label88.Text = "Состояние \"Отключено\"";
            // 
            // groupBox33
            // 
            this.groupBox33.Controls.Add(this._blockSDTU);
            this.groupBox33.Controls.Add(this._blockSDTUlabel);
            this.groupBox33.Controls.Add(this._switchSDTU);
            this.groupBox33.Controls.Add(this._switchVnesh);
            this.groupBox33.Controls.Add(this._switchKey);
            this.groupBox33.Controls.Add(this._switchButtons);
            this.groupBox33.Controls.Add(this._switchVneshOff);
            this.groupBox33.Controls.Add(this._switchVneshOn);
            this.groupBox33.Controls.Add(this._switchKeyOff);
            this.groupBox33.Controls.Add(this._switchKeyOn);
            this.groupBox33.Controls.Add(this.label101);
            this.groupBox33.Controls.Add(this.label102);
            this.groupBox33.Controls.Add(this.label103);
            this.groupBox33.Controls.Add(this.label104);
            this.groupBox33.Controls.Add(this.label105);
            this.groupBox33.Controls.Add(this.label106);
            this.groupBox33.Controls.Add(this.label107);
            this.groupBox33.Controls.Add(this.label108);
            this.groupBox33.Location = new System.Drawing.Point(267, 3);
            this.groupBox33.Name = "groupBox33";
            this.groupBox33.Size = new System.Drawing.Size(253, 211);
            this.groupBox33.TabIndex = 15;
            this.groupBox33.TabStop = false;
            this.groupBox33.Text = "Управление";
            // 
            // _blockSDTU
            // 
            this._blockSDTU.FormattingEnabled = true;
            this._blockSDTU.Location = new System.Drawing.Point(142, 177);
            this._blockSDTU.Name = "_blockSDTU";
            this._blockSDTU.Size = new System.Drawing.Size(105, 21);
            this._blockSDTU.TabIndex = 36;
            // 
            // _blockSDTUlabel
            // 
            this._blockSDTUlabel.AutoSize = true;
            this._blockSDTUlabel.Location = new System.Drawing.Point(6, 180);
            this._blockSDTUlabel.Name = "_blockSDTUlabel";
            this._blockSDTUlabel.Size = new System.Drawing.Size(102, 13);
            this._blockSDTUlabel.TabIndex = 35;
            this._blockSDTUlabel.Text = "Блокировка СДТУ";
            // 
            // _switchSDTU
            // 
            this._switchSDTU.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switchSDTU.FormattingEnabled = true;
            this._switchSDTU.Location = new System.Drawing.Point(142, 154);
            this._switchSDTU.Name = "_switchSDTU";
            this._switchSDTU.Size = new System.Drawing.Size(105, 21);
            this._switchSDTU.TabIndex = 34;
            // 
            // _switchVnesh
            // 
            this._switchVnesh.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switchVnesh.FormattingEnabled = true;
            this._switchVnesh.Location = new System.Drawing.Point(142, 134);
            this._switchVnesh.Name = "_switchVnesh";
            this._switchVnesh.Size = new System.Drawing.Size(105, 21);
            this._switchVnesh.TabIndex = 33;
            // 
            // _switchKey
            // 
            this._switchKey.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switchKey.FormattingEnabled = true;
            this._switchKey.Location = new System.Drawing.Point(142, 115);
            this._switchKey.Name = "_switchKey";
            this._switchKey.Size = new System.Drawing.Size(105, 21);
            this._switchKey.TabIndex = 32;
            // 
            // _switchButtons
            // 
            this._switchButtons.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switchButtons.FormattingEnabled = true;
            this._switchButtons.Location = new System.Drawing.Point(142, 95);
            this._switchButtons.Name = "_switchButtons";
            this._switchButtons.Size = new System.Drawing.Size(105, 21);
            this._switchButtons.TabIndex = 31;
            // 
            // _switchVneshOff
            // 
            this._switchVneshOff.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._switchVneshOff.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._switchVneshOff.FormattingEnabled = true;
            this._switchVneshOff.Location = new System.Drawing.Point(142, 75);
            this._switchVneshOff.Name = "_switchVneshOff";
            this._switchVneshOff.Size = new System.Drawing.Size(105, 21);
            this._switchVneshOff.TabIndex = 30;
            // 
            // _switchVneshOn
            // 
            this._switchVneshOn.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._switchVneshOn.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._switchVneshOn.FormattingEnabled = true;
            this._switchVneshOn.Location = new System.Drawing.Point(142, 55);
            this._switchVneshOn.Name = "_switchVneshOn";
            this._switchVneshOn.Size = new System.Drawing.Size(105, 21);
            this._switchVneshOn.TabIndex = 29;
            // 
            // _switchKeyOff
            // 
            this._switchKeyOff.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._switchKeyOff.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._switchKeyOff.FormattingEnabled = true;
            this._switchKeyOff.Location = new System.Drawing.Point(142, 35);
            this._switchKeyOff.Name = "_switchKeyOff";
            this._switchKeyOff.Size = new System.Drawing.Size(105, 21);
            this._switchKeyOff.TabIndex = 28;
            // 
            // _switchKeyOn
            // 
            this._switchKeyOn.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._switchKeyOn.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._switchKeyOn.FormattingEnabled = true;
            this._switchKeyOn.Location = new System.Drawing.Point(142, 15);
            this._switchKeyOn.Name = "_switchKeyOn";
            this._switchKeyOn.Size = new System.Drawing.Size(105, 21);
            this._switchKeyOn.TabIndex = 27;
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(6, 157);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(54, 13);
            this.label101.TabIndex = 25;
            this.label101.Text = "От СДТУ";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Location = new System.Drawing.Point(6, 137);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(52, 13);
            this.label102.TabIndex = 24;
            this.label102.Text = "Внешнее";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(6, 118);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(54, 13);
            this.label103.TabIndex = 23;
            this.label103.Text = "От ключа";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Location = new System.Drawing.Point(6, 98);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(96, 13);
            this.label104.TabIndex = 22;
            this.label104.Text = "От кнопок пульта";
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Location = new System.Drawing.Point(6, 78);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(108, 13);
            this.label105.TabIndex = 21;
            this.label105.Text = "Внешнее отключить";
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Location = new System.Drawing.Point(6, 58);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(103, 13);
            this.label106.TabIndex = 20;
            this.label106.Text = "Внешнее включить";
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Location = new System.Drawing.Point(6, 38);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(89, 13);
            this.label107.TabIndex = 19;
            this.label107.Text = "Ключ отключить";
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Location = new System.Drawing.Point(6, 18);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(84, 13);
            this.label108.TabIndex = 18;
            this.label108.Text = "Ключ включить";
            // 
            // _inputSignalsPage
            // 
            this._inputSignalsPage.Controls.Add(this.groupBox18);
            this._inputSignalsPage.Controls.Add(this.groupBox15);
            this._inputSignalsPage.Location = new System.Drawing.Point(4, 22);
            this._inputSignalsPage.Name = "_inputSignalsPage";
            this._inputSignalsPage.Size = new System.Drawing.Size(976, 553);
            this._inputSignalsPage.TabIndex = 7;
            this._inputSignalsPage.Text = "Входные сигналы";
            this._inputSignalsPage.UseVisualStyleBackColor = true;
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this._indComboBox);
            this.groupBox18.Location = new System.Drawing.Point(8, 93);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(254, 52);
            this.groupBox18.TabIndex = 4;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "Сброс блинкеров";
            // 
            // _indComboBox
            // 
            this._indComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._indComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._indComboBox.FormattingEnabled = true;
            this._indComboBox.Location = new System.Drawing.Point(76, 19);
            this._indComboBox.Name = "_indComboBox";
            this._indComboBox.Size = new System.Drawing.Size(167, 21);
            this._indComboBox.TabIndex = 0;
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.label76);
            this.groupBox15.Controls.Add(this._grUst2ComboBox);
            this.groupBox15.Controls.Add(this.label1);
            this.groupBox15.Controls.Add(this._grUst1ComboBox);
            this.groupBox15.Location = new System.Drawing.Point(8, 3);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(254, 84);
            this.groupBox15.TabIndex = 3;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Группы уставок";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(6, 48);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(51, 13);
            this.label76.TabIndex = 3;
            this.label76.Text = "Группа 2";
            // 
            // _grUst2ComboBox
            // 
            this._grUst2ComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._grUst2ComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._grUst2ComboBox.FormattingEnabled = true;
            this._grUst2ComboBox.Location = new System.Drawing.Point(76, 45);
            this._grUst2ComboBox.Name = "_grUst2ComboBox";
            this._grUst2ComboBox.Size = new System.Drawing.Size(167, 21);
            this._grUst2ComboBox.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Группа 1";
            // 
            // _grUst1ComboBox
            // 
            this._grUst1ComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._grUst1ComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._grUst1ComboBox.FormattingEnabled = true;
            this._grUst1ComboBox.Location = new System.Drawing.Point(76, 18);
            this._grUst1ComboBox.Name = "_grUst1ComboBox";
            this._grUst1ComboBox.Size = new System.Drawing.Size(167, 21);
            this._grUst1ComboBox.TabIndex = 0;
            // 
            // _outputSignalsPage
            // 
            this._outputSignalsPage.Controls.Add(this.groupBox13);
            this._outputSignalsPage.Controls.Add(this.groupBox31);
            this._outputSignalsPage.Controls.Add(this.groupBox175);
            this._outputSignalsPage.Controls.Add(this.groupBox176);
            this._outputSignalsPage.Location = new System.Drawing.Point(4, 22);
            this._outputSignalsPage.Name = "_outputSignalsPage";
            this._outputSignalsPage.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this._outputSignalsPage.Size = new System.Drawing.Size(976, 553);
            this._outputSignalsPage.TabIndex = 10;
            this._outputSignalsPage.Text = "Выходные сигналы";
            this._outputSignalsPage.UseVisualStyleBackColor = true;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this._resetAlarmCheckBox);
            this.groupBox13.Controls.Add(this._resetSystemCheckBox);
            this.groupBox13.Controls.Add(this.label110);
            this.groupBox13.Controls.Add(this.label52);
            this.groupBox13.Location = new System.Drawing.Point(407, 342);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(215, 71);
            this.groupBox13.TabIndex = 22;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Сброс индикаторов";
            // 
            // _resetAlarmCheckBox
            // 
            this._resetAlarmCheckBox.AutoSize = true;
            this._resetAlarmCheckBox.Location = new System.Drawing.Point(185, 46);
            this._resetAlarmCheckBox.Name = "_resetAlarmCheckBox";
            this._resetAlarmCheckBox.Size = new System.Drawing.Size(15, 14);
            this._resetAlarmCheckBox.TabIndex = 21;
            this._resetAlarmCheckBox.UseVisualStyleBackColor = true;
            // 
            // _resetSystemCheckBox
            // 
            this._resetSystemCheckBox.AutoSize = true;
            this._resetSystemCheckBox.Location = new System.Drawing.Point(185, 21);
            this._resetSystemCheckBox.Name = "_resetSystemCheckBox";
            this._resetSystemCheckBox.Size = new System.Drawing.Size(15, 14);
            this._resetSystemCheckBox.TabIndex = 19;
            this._resetSystemCheckBox.UseVisualStyleBackColor = true;
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Location = new System.Drawing.Point(6, 45);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(152, 13);
            this.label110.TabIndex = 20;
            this.label110.Text = "2. По входу в журнал аварий";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(6, 22);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(161, 13);
            this.label52.TabIndex = 19;
            this.label52.Text = "1. По входу в журнал системы";
            // 
            // groupBox31
            // 
            this.groupBox31.Controls.Add(this._fault4CheckBox);
            this.groupBox31.Controls.Add(this.label2);
            this.groupBox31.Controls.Add(this._fault3CheckBox);
            this.groupBox31.Controls.Add(this.label3);
            this.groupBox31.Controls.Add(this._fault2CheckBox);
            this.groupBox31.Controls.Add(this._fault1CheckBox);
            this.groupBox31.Controls.Add(this.label44);
            this.groupBox31.Controls.Add(this.label45);
            this.groupBox31.Controls.Add(this._impTB);
            this.groupBox31.Controls.Add(this.label46);
            this.groupBox31.Location = new System.Drawing.Point(628, 342);
            this.groupBox31.Name = "groupBox31";
            this.groupBox31.Size = new System.Drawing.Size(323, 135);
            this.groupBox31.TabIndex = 21;
            this.groupBox31.TabStop = false;
            this.groupBox31.Text = "Реле неисправность";
            // 
            // _fault4CheckBox
            // 
            this._fault4CheckBox.AutoSize = true;
            this._fault4CheckBox.Location = new System.Drawing.Point(181, 86);
            this._fault4CheckBox.Name = "_fault4CheckBox";
            this._fault4CheckBox.Size = new System.Drawing.Size(15, 14);
            this._fault4CheckBox.TabIndex = 24;
            this._fault4CheckBox.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(136, 13);
            this.label2.TabIndex = 23;
            this.label2.Text = "4. Неисправность логики";
            // 
            // _fault3CheckBox
            // 
            this._fault3CheckBox.AutoSize = true;
            this._fault3CheckBox.Location = new System.Drawing.Point(181, 65);
            this._fault3CheckBox.Name = "_fault3CheckBox";
            this._fault3CheckBox.Size = new System.Drawing.Size(15, 14);
            this._fault3CheckBox.TabIndex = 22;
            this._fault3CheckBox.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(169, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "3. Неисправность выключателя";
            // 
            // _fault2CheckBox
            // 
            this._fault2CheckBox.AutoSize = true;
            this._fault2CheckBox.Location = new System.Drawing.Point(181, 45);
            this._fault2CheckBox.Name = "_fault2CheckBox";
            this._fault2CheckBox.Size = new System.Drawing.Size(15, 14);
            this._fault2CheckBox.TabIndex = 16;
            this._fault2CheckBox.UseVisualStyleBackColor = true;
            // 
            // _fault1CheckBox
            // 
            this._fault1CheckBox.AutoSize = true;
            this._fault1CheckBox.Location = new System.Drawing.Point(181, 21);
            this._fault1CheckBox.Name = "_fault1CheckBox";
            this._fault1CheckBox.Size = new System.Drawing.Size(15, 14);
            this._fault1CheckBox.TabIndex = 15;
            this._fault1CheckBox.UseVisualStyleBackColor = true;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(6, 46);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(170, 13);
            this.label44.TabIndex = 12;
            this.label44.Text = "2. Программная неисправность";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(6, 22);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(159, 13);
            this.label45.TabIndex = 11;
            this.label45.Text = "1. Аппаратная неисправность";
            // 
            // _impTB
            // 
            this._impTB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._impTB.Location = new System.Drawing.Point(92, 106);
            this._impTB.Name = "_impTB";
            this._impTB.Size = new System.Drawing.Size(104, 20);
            this._impTB.TabIndex = 7;
            this._impTB.Text = "0";
            this._impTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(6, 108);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(64, 13);
            this.label46.TabIndex = 3;
            this.label46.Text = "Твозвр, мс";
            // 
            // groupBox175
            // 
            this.groupBox175.Controls.Add(this._outputIndicatorsGrid);
            this.groupBox175.Location = new System.Drawing.Point(407, 6);
            this.groupBox175.Name = "groupBox175";
            this.groupBox175.Size = new System.Drawing.Size(544, 330);
            this.groupBox175.TabIndex = 20;
            this.groupBox175.TabStop = false;
            this.groupBox175.Text = "Индикаторы";
            // 
            // _outputIndicatorsGrid
            // 
            this._outputIndicatorsGrid.AllowUserToAddRows = false;
            this._outputIndicatorsGrid.AllowUserToDeleteRows = false;
            this._outputIndicatorsGrid.AllowUserToResizeColumns = false;
            this._outputIndicatorsGrid.AllowUserToResizeRows = false;
            this._outputIndicatorsGrid.BackgroundColor = System.Drawing.Color.White;
            this._outputIndicatorsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._outputIndicatorsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._outIndNumberCol,
            this._outIndTypeCol,
            this._outIndSignalCol,
            this._out1IndSignalCol,
            this._outIndSignal2Col});
            this._outputIndicatorsGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._outputIndicatorsGrid.Location = new System.Drawing.Point(3, 16);
            this._outputIndicatorsGrid.Name = "_outputIndicatorsGrid";
            this._outputIndicatorsGrid.RowHeadersVisible = false;
            this._outputIndicatorsGrid.RowHeadersWidth = 51;
            this._outputIndicatorsGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._outputIndicatorsGrid.RowsDefaultCellStyle = dataGridViewCellStyle28;
            this._outputIndicatorsGrid.RowTemplate.Height = 24;
            this._outputIndicatorsGrid.ShowCellErrors = false;
            this._outputIndicatorsGrid.ShowRowErrors = false;
            this._outputIndicatorsGrid.Size = new System.Drawing.Size(538, 311);
            this._outputIndicatorsGrid.TabIndex = 0;
            // 
            // _outIndNumberCol
            // 
            this._outIndNumberCol.HeaderText = "№";
            this._outIndNumberCol.MinimumWidth = 6;
            this._outIndNumberCol.Name = "_outIndNumberCol";
            this._outIndNumberCol.ReadOnly = true;
            this._outIndNumberCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._outIndNumberCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._outIndNumberCol.Width = 25;
            // 
            // _outIndTypeCol
            // 
            this._outIndTypeCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._outIndTypeCol.HeaderText = "Тип";
            this._outIndTypeCol.MinimumWidth = 6;
            this._outIndTypeCol.Name = "_outIndTypeCol";
            this._outIndTypeCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._outIndTypeCol.Width = 125;
            // 
            // _outIndSignalCol
            // 
            this._outIndSignalCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._outIndSignalCol.HeaderText = "Сигнал \"зеленый\"";
            this._outIndSignalCol.MinimumWidth = 6;
            this._outIndSignalCol.Name = "_outIndSignalCol";
            this._outIndSignalCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._outIndSignalCol.Width = 130;
            // 
            // _out1IndSignalCol
            // 
            this._out1IndSignalCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._out1IndSignalCol.HeaderText = "Сигнал \"красный\"";
            this._out1IndSignalCol.MinimumWidth = 6;
            this._out1IndSignalCol.Name = "_out1IndSignalCol";
            this._out1IndSignalCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._out1IndSignalCol.Width = 130;
            // 
            // _outIndSignal2Col
            // 
            this._outIndSignal2Col.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._outIndSignal2Col.HeaderText = "Режим работы";
            this._outIndSignal2Col.MinimumWidth = 6;
            this._outIndSignal2Col.Name = "_outIndSignal2Col";
            this._outIndSignal2Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._outIndSignal2Col.Width = 150;
            // 
            // groupBox176
            // 
            this.groupBox176.Controls.Add(this._outputReleGrid);
            this.groupBox176.Location = new System.Drawing.Point(8, 6);
            this.groupBox176.Name = "groupBox176";
            this.groupBox176.Size = new System.Drawing.Size(393, 547);
            this.groupBox176.TabIndex = 19;
            this.groupBox176.TabStop = false;
            this.groupBox176.Text = "Выходные реле";
            // 
            // _outputReleGrid
            // 
            this._outputReleGrid.AllowUserToAddRows = false;
            this._outputReleGrid.AllowUserToDeleteRows = false;
            this._outputReleGrid.AllowUserToResizeColumns = false;
            this._outputReleGrid.AllowUserToResizeRows = false;
            this._outputReleGrid.BackgroundColor = System.Drawing.Color.White;
            this._outputReleGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._outputReleGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._releNumberCol,
            this._releTypeCol,
            this._releSignalCol,
            this._releWaitCol});
            this._outputReleGrid.Location = new System.Drawing.Point(9, 13);
            this._outputReleGrid.Name = "_outputReleGrid";
            this._outputReleGrid.RowHeadersVisible = false;
            this._outputReleGrid.RowHeadersWidth = 51;
            this._outputReleGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._outputReleGrid.RowsDefaultCellStyle = dataGridViewCellStyle29;
            this._outputReleGrid.RowTemplate.Height = 24;
            this._outputReleGrid.ShowCellErrors = false;
            this._outputReleGrid.ShowRowErrors = false;
            this._outputReleGrid.Size = new System.Drawing.Size(376, 526);
            this._outputReleGrid.TabIndex = 0;
            // 
            // _releNumberCol
            // 
            this._releNumberCol.HeaderText = "№";
            this._releNumberCol.MinimumWidth = 6;
            this._releNumberCol.Name = "_releNumberCol";
            this._releNumberCol.ReadOnly = true;
            this._releNumberCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._releNumberCol.Width = 25;
            // 
            // _releTypeCol
            // 
            this._releTypeCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._releTypeCol.HeaderText = "Тип";
            this._releTypeCol.MinimumWidth = 6;
            this._releTypeCol.Name = "_releTypeCol";
            this._releTypeCol.Width = 120;
            // 
            // _releSignalCol
            // 
            this._releSignalCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._releSignalCol.HeaderText = "Сигнал";
            this._releSignalCol.MinimumWidth = 6;
            this._releSignalCol.Name = "_releSignalCol";
            this._releSignalCol.Width = 120;
            // 
            // _releWaitCol
            // 
            this._releWaitCol.HeaderText = "Твозвр, мс";
            this._releWaitCol.MinimumWidth = 6;
            this._releWaitCol.Name = "_releWaitCol";
            this._releWaitCol.Width = 90;
            // 
            // _logicElement
            // 
            this._logicElement.Controls.Add(this.groupBox55);
            this._logicElement.Controls.Add(this.groupBox53);
            this._logicElement.Location = new System.Drawing.Point(4, 22);
            this._logicElement.Name = "_logicElement";
            this._logicElement.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this._logicElement.Size = new System.Drawing.Size(976, 553);
            this._logicElement.TabIndex = 12;
            this._logicElement.Text = "Логические элементы";
            this._logicElement.UseVisualStyleBackColor = true;
            // 
            // groupBox55
            // 
            this.groupBox55.Controls.Add(this._virtualReleDataGrid);
            this.groupBox55.Location = new System.Drawing.Point(3, 3);
            this.groupBox55.Name = "groupBox55";
            this.groupBox55.Size = new System.Drawing.Size(393, 541);
            this.groupBox55.TabIndex = 24;
            this.groupBox55.TabStop = false;
            this.groupBox55.Text = "Виртуальные  реле";
            // 
            // _virtualReleDataGrid
            // 
            this._virtualReleDataGrid.AllowUserToAddRows = false;
            this._virtualReleDataGrid.AllowUserToDeleteRows = false;
            this._virtualReleDataGrid.AllowUserToResizeColumns = false;
            this._virtualReleDataGrid.AllowUserToResizeRows = false;
            this._virtualReleDataGrid.BackgroundColor = System.Drawing.Color.White;
            this._virtualReleDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._virtualReleDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn52,
            this.dataGridViewComboBoxColumn68,
            this.dataGridViewComboBoxColumn69,
            this.dataGridViewTextBoxColumn53});
            this._virtualReleDataGrid.Location = new System.Drawing.Point(9, 13);
            this._virtualReleDataGrid.Name = "_virtualReleDataGrid";
            this._virtualReleDataGrid.RowHeadersVisible = false;
            this._virtualReleDataGrid.RowHeadersWidth = 51;
            this._virtualReleDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._virtualReleDataGrid.RowsDefaultCellStyle = dataGridViewCellStyle30;
            this._virtualReleDataGrid.RowTemplate.Height = 24;
            this._virtualReleDataGrid.ShowCellErrors = false;
            this._virtualReleDataGrid.ShowRowErrors = false;
            this._virtualReleDataGrid.Size = new System.Drawing.Size(376, 522);
            this._virtualReleDataGrid.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn52
            // 
            this.dataGridViewTextBoxColumn52.HeaderText = "№";
            this.dataGridViewTextBoxColumn52.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn52.Name = "dataGridViewTextBoxColumn52";
            this.dataGridViewTextBoxColumn52.ReadOnly = true;
            this.dataGridViewTextBoxColumn52.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn52.Width = 25;
            // 
            // dataGridViewComboBoxColumn68
            // 
            this.dataGridViewComboBoxColumn68.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn68.HeaderText = "Тип";
            this.dataGridViewComboBoxColumn68.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn68.Name = "dataGridViewComboBoxColumn68";
            this.dataGridViewComboBoxColumn68.Width = 120;
            // 
            // dataGridViewComboBoxColumn69
            // 
            this.dataGridViewComboBoxColumn69.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn69.HeaderText = "Сигнал";
            this.dataGridViewComboBoxColumn69.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn69.Name = "dataGridViewComboBoxColumn69";
            this.dataGridViewComboBoxColumn69.Width = 140;
            // 
            // dataGridViewTextBoxColumn53
            // 
            this.dataGridViewTextBoxColumn53.HeaderText = "Tвозвр., мс";
            this.dataGridViewTextBoxColumn53.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn53.Name = "dataGridViewTextBoxColumn53";
            this.dataGridViewTextBoxColumn53.Width = 70;
            // 
            // groupBox53
            // 
            this.groupBox53.Controls.Add(this._rsTriggersDataGrid);
            this.groupBox53.Location = new System.Drawing.Point(402, 3);
            this.groupBox53.Name = "groupBox53";
            this.groupBox53.Size = new System.Drawing.Size(292, 427);
            this.groupBox53.TabIndex = 23;
            this.groupBox53.TabStop = false;
            this.groupBox53.Text = "Энергонезависимые RS-триггеры";
            // 
            // _rsTriggersDataGrid
            // 
            this._rsTriggersDataGrid.AllowUserToAddRows = false;
            this._rsTriggersDataGrid.AllowUserToDeleteRows = false;
            this._rsTriggersDataGrid.AllowUserToResizeColumns = false;
            this._rsTriggersDataGrid.AllowUserToResizeRows = false;
            this._rsTriggersDataGrid.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle31.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle31.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle31.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle31.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle31.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._rsTriggersDataGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle31;
            this._rsTriggersDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._rsTriggersDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn51,
            this.dataGridViewComboBoxColumn65,
            this.dataGridViewComboBoxColumn66,
            this.dataGridViewComboBoxColumn67});
            this._rsTriggersDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._rsTriggersDataGrid.Location = new System.Drawing.Point(3, 16);
            this._rsTriggersDataGrid.Name = "_rsTriggersDataGrid";
            this._rsTriggersDataGrid.RowHeadersVisible = false;
            this._rsTriggersDataGrid.RowHeadersWidth = 51;
            this._rsTriggersDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._rsTriggersDataGrid.RowsDefaultCellStyle = dataGridViewCellStyle32;
            this._rsTriggersDataGrid.RowTemplate.Height = 24;
            this._rsTriggersDataGrid.ShowCellErrors = false;
            this._rsTriggersDataGrid.ShowRowErrors = false;
            this._rsTriggersDataGrid.Size = new System.Drawing.Size(286, 408);
            this._rsTriggersDataGrid.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn51
            // 
            this.dataGridViewTextBoxColumn51.HeaderText = "№";
            this.dataGridViewTextBoxColumn51.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn51.Name = "dataGridViewTextBoxColumn51";
            this.dataGridViewTextBoxColumn51.ReadOnly = true;
            this.dataGridViewTextBoxColumn51.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn51.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn51.Width = 25;
            // 
            // dataGridViewComboBoxColumn65
            // 
            this.dataGridViewComboBoxColumn65.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn65.HeaderText = "Тип";
            this.dataGridViewComboBoxColumn65.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn65.Name = "dataGridViewComboBoxColumn65";
            this.dataGridViewComboBoxColumn65.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn65.Width = 35;
            // 
            // dataGridViewComboBoxColumn66
            // 
            this.dataGridViewComboBoxColumn66.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn66.HeaderText = "Вход R";
            this.dataGridViewComboBoxColumn66.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn66.Name = "dataGridViewComboBoxColumn66";
            this.dataGridViewComboBoxColumn66.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn66.Width = 110;
            // 
            // dataGridViewComboBoxColumn67
            // 
            this.dataGridViewComboBoxColumn67.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn67.HeaderText = "Вход S";
            this.dataGridViewComboBoxColumn67.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn67.Name = "dataGridViewComboBoxColumn67";
            this.dataGridViewComboBoxColumn67.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn67.Width = 110;
            // 
            // _systemPage
            // 
            this._systemPage.Controls.Add(this.groupBox11);
            this._systemPage.Controls.Add(this.groupBox3);
            this._systemPage.Location = new System.Drawing.Point(4, 22);
            this._systemPage.Name = "_systemPage";
            this._systemPage.Size = new System.Drawing.Size(976, 553);
            this._systemPage.TabIndex = 8;
            this._systemPage.Text = "Осциллограф";
            this._systemPage.UseVisualStyleBackColor = true;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this._oscChannelsGrid);
            this.groupBox11.Location = new System.Drawing.Point(291, 3);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(364, 547);
            this.groupBox11.TabIndex = 4;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Программируемые дискретные каналы";
            // 
            // _oscChannelsGrid
            // 
            this._oscChannelsGrid.AllowUserToAddRows = false;
            this._oscChannelsGrid.AllowUserToDeleteRows = false;
            this._oscChannelsGrid.AllowUserToResizeColumns = false;
            this._oscChannelsGrid.AllowUserToResizeRows = false;
            this._oscChannelsGrid.BackgroundColor = System.Drawing.Color.White;
            this._oscChannelsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._oscChannelsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewComboBoxColumn6,
            this.dataGridViewComboBoxColumn7});
            this._oscChannelsGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._oscChannelsGrid.Location = new System.Drawing.Point(3, 16);
            this._oscChannelsGrid.Name = "_oscChannelsGrid";
            this._oscChannelsGrid.RowHeadersVisible = false;
            this._oscChannelsGrid.RowHeadersWidth = 51;
            this._oscChannelsGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._oscChannelsGrid.RowTemplate.Height = 24;
            this._oscChannelsGrid.ShowCellErrors = false;
            this._oscChannelsGrid.ShowRowErrors = false;
            this._oscChannelsGrid.Size = new System.Drawing.Size(358, 528);
            this._oscChannelsGrid.TabIndex = 28;
            // 
            // dataGridViewTextBoxColumn4
            // 
            dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle33.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle33.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle33.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle33.SelectionForeColor = System.Drawing.Color.White;
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle33;
            this.dataGridViewTextBoxColumn4.Frozen = true;
            this.dataGridViewTextBoxColumn4.HeaderText = "Канал";
            this.dataGridViewTextBoxColumn4.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn4.Width = 70;
            // 
            // dataGridViewComboBoxColumn6
            // 
            dataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dataGridViewComboBoxColumn6.DefaultCellStyle = dataGridViewCellStyle34;
            this.dataGridViewComboBoxColumn6.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn6.HeaderText = "База";
            this.dataGridViewComboBoxColumn6.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn6.Name = "dataGridViewComboBoxColumn6";
            this.dataGridViewComboBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn6.Width = 50;
            // 
            // dataGridViewComboBoxColumn7
            // 
            this.dataGridViewComboBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dataGridViewComboBoxColumn7.DefaultCellStyle = dataGridViewCellStyle35;
            this.dataGridViewComboBoxColumn7.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn7.HeaderText = "Сигнал";
            this.dataGridViewComboBoxColumn7.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn7.Name = "dataGridViewComboBoxColumn7";
            this.dataGridViewComboBoxColumn7.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.oscStartCb);
            this.groupBox3.Controls.Add(this.label65);
            this.groupBox3.Controls.Add(this._oscSizeTextBox);
            this.groupBox3.Controls.Add(this._oscWriteLength);
            this.groupBox3.Controls.Add(this._oscFix);
            this.groupBox3.Controls.Add(this._oscLength);
            this.groupBox3.Controls.Add(this.label41);
            this.groupBox3.Controls.Add(this.label42);
            this.groupBox3.Controls.Add(this.label43);
            this.groupBox3.Location = new System.Drawing.Point(8, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(277, 124);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Осциллограф";
            // 
            // oscStartCb
            // 
            this.oscStartCb.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.oscStartCb.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.oscStartCb.FormattingEnabled = true;
            this.oscStartCb.Location = new System.Drawing.Point(134, 91);
            this.oscStartCb.Name = "oscStartCb";
            this.oscStartCb.Size = new System.Drawing.Size(121, 21);
            this.oscStartCb.TabIndex = 30;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(18, 94);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(87, 13);
            this.label65.TabIndex = 29;
            this.label65.Text = "Вход пуска осц.";
            // 
            // _oscSizeTextBox
            // 
            this._oscSizeTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._oscSizeTextBox.Location = new System.Drawing.Point(184, 23);
            this._oscSizeTextBox.Name = "_oscSizeTextBox";
            this._oscSizeTextBox.ReadOnly = true;
            this._oscSizeTextBox.Size = new System.Drawing.Size(71, 20);
            this._oscSizeTextBox.TabIndex = 28;
            this._oscSizeTextBox.Tag = "3000000";
            this._oscSizeTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _oscWriteLength
            // 
            this._oscWriteLength.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._oscWriteLength.Location = new System.Drawing.Point(134, 44);
            this._oscWriteLength.Name = "_oscWriteLength";
            this._oscWriteLength.Size = new System.Drawing.Size(121, 20);
            this._oscWriteLength.TabIndex = 23;
            this._oscWriteLength.Tag = "3000000";
            this._oscWriteLength.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _oscFix
            // 
            this._oscFix.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._oscFix.FormattingEnabled = true;
            this._oscFix.Location = new System.Drawing.Point(134, 64);
            this._oscFix.Name = "_oscFix";
            this._oscFix.Size = new System.Drawing.Size(121, 21);
            this._oscFix.TabIndex = 13;
            // 
            // _oscLength
            // 
            this._oscLength.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._oscLength.FormattingEnabled = true;
            this._oscLength.Location = new System.Drawing.Point(134, 23);
            this._oscLength.Name = "_oscLength";
            this._oscLength.Size = new System.Drawing.Size(44, 21);
            this._oscLength.TabIndex = 12;
            this._oscLength.SelectedIndexChanged += new System.EventHandler(this._oscLength_SelectedIndexChanged);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(18, 67);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(66, 13);
            this.label41.TabIndex = 2;
            this.label41.Text = "Фиксац. по";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(18, 46);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(113, 13);
            this.label42.TabIndex = 1;
            this.label42.Text = "Длит. предзаписи, %";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(18, 26);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(66, 13);
            this.label43.TabIndex = 0;
            this.label43.Text = "Размер, мс";
            // 
            // _bgsGoose
            // 
            this._bgsGoose.Controls.Add(this.groupBox14);
            this._bgsGoose.Controls.Add(this.label36);
            this._bgsGoose.Controls.Add(this.currentBGS);
            this._bgsGoose.Location = new System.Drawing.Point(4, 22);
            this._bgsGoose.Name = "_bgsGoose";
            this._bgsGoose.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this._bgsGoose.Size = new System.Drawing.Size(976, 553);
            this._bgsGoose.TabIndex = 13;
            this._bgsGoose.Text = "БГС";
            this._bgsGoose.UseVisualStyleBackColor = true;
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.groupBox19);
            this.groupBox14.Controls.Add(this.label345);
            this.groupBox14.Controls.Add(this.operationBGS);
            this.groupBox14.Location = new System.Drawing.Point(13, 34);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(950, 511);
            this.groupBox14.TabIndex = 12;
            this.groupBox14.TabStop = false;
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this.goin64);
            this.groupBox19.Controls.Add(this.goin48);
            this.groupBox19.Controls.Add(this.goin32);
            this.groupBox19.Controls.Add(this.label281);
            this.groupBox19.Controls.Add(this.label282);
            this.groupBox19.Controls.Add(this.label283);
            this.groupBox19.Controls.Add(this.goin63);
            this.groupBox19.Controls.Add(this.goin47);
            this.groupBox19.Controls.Add(this.goin31);
            this.groupBox19.Controls.Add(this.label284);
            this.groupBox19.Controls.Add(this.label285);
            this.groupBox19.Controls.Add(this.label286);
            this.groupBox19.Controls.Add(this.goin62);
            this.groupBox19.Controls.Add(this.goin46);
            this.groupBox19.Controls.Add(this.goin30);
            this.groupBox19.Controls.Add(this.label287);
            this.groupBox19.Controls.Add(this.label288);
            this.groupBox19.Controls.Add(this.label289);
            this.groupBox19.Controls.Add(this.goin61);
            this.groupBox19.Controls.Add(this.goin45);
            this.groupBox19.Controls.Add(this.goin29);
            this.groupBox19.Controls.Add(this.label290);
            this.groupBox19.Controls.Add(this.label291);
            this.groupBox19.Controls.Add(this.label292);
            this.groupBox19.Controls.Add(this.goin60);
            this.groupBox19.Controls.Add(this.goin44);
            this.groupBox19.Controls.Add(this.goin28);
            this.groupBox19.Controls.Add(this.label293);
            this.groupBox19.Controls.Add(this.label294);
            this.groupBox19.Controls.Add(this.label295);
            this.groupBox19.Controls.Add(this.goin59);
            this.groupBox19.Controls.Add(this.goin43);
            this.groupBox19.Controls.Add(this.goin27);
            this.groupBox19.Controls.Add(this.label296);
            this.groupBox19.Controls.Add(this.label297);
            this.groupBox19.Controls.Add(this.label298);
            this.groupBox19.Controls.Add(this.goin58);
            this.groupBox19.Controls.Add(this.goin42);
            this.groupBox19.Controls.Add(this.goin26);
            this.groupBox19.Controls.Add(this.label299);
            this.groupBox19.Controls.Add(this.label300);
            this.groupBox19.Controls.Add(this.label301);
            this.groupBox19.Controls.Add(this.goin57);
            this.groupBox19.Controls.Add(this.goin41);
            this.groupBox19.Controls.Add(this.goin25);
            this.groupBox19.Controls.Add(this.label302);
            this.groupBox19.Controls.Add(this.label303);
            this.groupBox19.Controls.Add(this.label304);
            this.groupBox19.Controls.Add(this.goin56);
            this.groupBox19.Controls.Add(this.goin40);
            this.groupBox19.Controls.Add(this.goin24);
            this.groupBox19.Controls.Add(this.label305);
            this.groupBox19.Controls.Add(this.label306);
            this.groupBox19.Controls.Add(this.label307);
            this.groupBox19.Controls.Add(this.goin55);
            this.groupBox19.Controls.Add(this.goin39);
            this.groupBox19.Controls.Add(this.goin23);
            this.groupBox19.Controls.Add(this.label308);
            this.groupBox19.Controls.Add(this.label309);
            this.groupBox19.Controls.Add(this.label310);
            this.groupBox19.Controls.Add(this.goin54);
            this.groupBox19.Controls.Add(this.goin38);
            this.groupBox19.Controls.Add(this.goin22);
            this.groupBox19.Controls.Add(this.label311);
            this.groupBox19.Controls.Add(this.label312);
            this.groupBox19.Controls.Add(this.label313);
            this.groupBox19.Controls.Add(this.goin53);
            this.groupBox19.Controls.Add(this.goin37);
            this.groupBox19.Controls.Add(this.goin21);
            this.groupBox19.Controls.Add(this.label314);
            this.groupBox19.Controls.Add(this.label315);
            this.groupBox19.Controls.Add(this.label316);
            this.groupBox19.Controls.Add(this.goin52);
            this.groupBox19.Controls.Add(this.goin36);
            this.groupBox19.Controls.Add(this.goin20);
            this.groupBox19.Controls.Add(this.label317);
            this.groupBox19.Controls.Add(this.label318);
            this.groupBox19.Controls.Add(this.label319);
            this.groupBox19.Controls.Add(this.goin51);
            this.groupBox19.Controls.Add(this.goin35);
            this.groupBox19.Controls.Add(this.goin19);
            this.groupBox19.Controls.Add(this.label320);
            this.groupBox19.Controls.Add(this.label321);
            this.groupBox19.Controls.Add(this.label322);
            this.groupBox19.Controls.Add(this.goin50);
            this.groupBox19.Controls.Add(this.goin34);
            this.groupBox19.Controls.Add(this.goin18);
            this.groupBox19.Controls.Add(this.label323);
            this.groupBox19.Controls.Add(this.label324);
            this.groupBox19.Controls.Add(this.label325);
            this.groupBox19.Controls.Add(this.goin49);
            this.groupBox19.Controls.Add(this.label326);
            this.groupBox19.Controls.Add(this.goin33);
            this.groupBox19.Controls.Add(this.label327);
            this.groupBox19.Controls.Add(this.goin17);
            this.groupBox19.Controls.Add(this.label328);
            this.groupBox19.Controls.Add(this.goin16);
            this.groupBox19.Controls.Add(this.label329);
            this.groupBox19.Controls.Add(this.goin15);
            this.groupBox19.Controls.Add(this.label330);
            this.groupBox19.Controls.Add(this.goin14);
            this.groupBox19.Controls.Add(this.label331);
            this.groupBox19.Controls.Add(this.goin13);
            this.groupBox19.Controls.Add(this.label332);
            this.groupBox19.Controls.Add(this.goin12);
            this.groupBox19.Controls.Add(this.label333);
            this.groupBox19.Controls.Add(this.goin11);
            this.groupBox19.Controls.Add(this.label334);
            this.groupBox19.Controls.Add(this.goin10);
            this.groupBox19.Controls.Add(this.label335);
            this.groupBox19.Controls.Add(this.goin9);
            this.groupBox19.Controls.Add(this.label336);
            this.groupBox19.Controls.Add(this.goin8);
            this.groupBox19.Controls.Add(this.label337);
            this.groupBox19.Controls.Add(this.goin7);
            this.groupBox19.Controls.Add(this.label338);
            this.groupBox19.Controls.Add(this.goin6);
            this.groupBox19.Controls.Add(this.label339);
            this.groupBox19.Controls.Add(this.goin5);
            this.groupBox19.Controls.Add(this.label340);
            this.groupBox19.Controls.Add(this.goin4);
            this.groupBox19.Controls.Add(this.label341);
            this.groupBox19.Controls.Add(this.goin3);
            this.groupBox19.Controls.Add(this.label342);
            this.groupBox19.Controls.Add(this.goin2);
            this.groupBox19.Controls.Add(this.label343);
            this.groupBox19.Controls.Add(this.goin1);
            this.groupBox19.Controls.Add(this.label344);
            this.groupBox19.Location = new System.Drawing.Point(6, 50);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(645, 455);
            this.groupBox19.TabIndex = 8;
            this.groupBox19.TabStop = false;
            // 
            // goin64
            // 
            this.goin64.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin64.FormattingEnabled = true;
            this.goin64.Location = new System.Drawing.Point(553, 424);
            this.goin64.Name = "goin64";
            this.goin64.Size = new System.Drawing.Size(75, 21);
            this.goin64.TabIndex = 33;
            // 
            // goin48
            // 
            this.goin48.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin48.FormattingEnabled = true;
            this.goin48.Location = new System.Drawing.Point(384, 424);
            this.goin48.Name = "goin48";
            this.goin48.Size = new System.Drawing.Size(75, 21);
            this.goin48.TabIndex = 33;
            // 
            // goin32
            // 
            this.goin32.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin32.FormattingEnabled = true;
            this.goin32.Location = new System.Drawing.Point(217, 424);
            this.goin32.Name = "goin32";
            this.goin32.Size = new System.Drawing.Size(75, 21);
            this.goin32.TabIndex = 33;
            // 
            // label281
            // 
            this.label281.AutoSize = true;
            this.label281.Location = new System.Drawing.Point(505, 427);
            this.label281.Name = "label281";
            this.label281.Size = new System.Drawing.Size(42, 13);
            this.label281.TabIndex = 14;
            this.label281.Text = "GoIn64";
            // 
            // label282
            // 
            this.label282.AutoSize = true;
            this.label282.Location = new System.Drawing.Point(336, 427);
            this.label282.Name = "label282";
            this.label282.Size = new System.Drawing.Size(42, 13);
            this.label282.TabIndex = 14;
            this.label282.Text = "GoIn48";
            // 
            // label283
            // 
            this.label283.AutoSize = true;
            this.label283.Location = new System.Drawing.Point(169, 427);
            this.label283.Name = "label283";
            this.label283.Size = new System.Drawing.Size(42, 13);
            this.label283.TabIndex = 14;
            this.label283.Text = "GoIn32";
            // 
            // goin63
            // 
            this.goin63.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin63.FormattingEnabled = true;
            this.goin63.Location = new System.Drawing.Point(553, 397);
            this.goin63.Name = "goin63";
            this.goin63.Size = new System.Drawing.Size(75, 21);
            this.goin63.TabIndex = 29;
            // 
            // goin47
            // 
            this.goin47.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin47.FormattingEnabled = true;
            this.goin47.Location = new System.Drawing.Point(384, 397);
            this.goin47.Name = "goin47";
            this.goin47.Size = new System.Drawing.Size(75, 21);
            this.goin47.TabIndex = 29;
            // 
            // goin31
            // 
            this.goin31.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin31.FormattingEnabled = true;
            this.goin31.Location = new System.Drawing.Point(217, 397);
            this.goin31.Name = "goin31";
            this.goin31.Size = new System.Drawing.Size(75, 21);
            this.goin31.TabIndex = 29;
            // 
            // label284
            // 
            this.label284.AutoSize = true;
            this.label284.Location = new System.Drawing.Point(505, 400);
            this.label284.Name = "label284";
            this.label284.Size = new System.Drawing.Size(42, 13);
            this.label284.TabIndex = 15;
            this.label284.Text = "GoIn63";
            // 
            // label285
            // 
            this.label285.AutoSize = true;
            this.label285.Location = new System.Drawing.Point(336, 400);
            this.label285.Name = "label285";
            this.label285.Size = new System.Drawing.Size(42, 13);
            this.label285.TabIndex = 15;
            this.label285.Text = "GoIn47";
            // 
            // label286
            // 
            this.label286.AutoSize = true;
            this.label286.Location = new System.Drawing.Point(169, 400);
            this.label286.Name = "label286";
            this.label286.Size = new System.Drawing.Size(42, 13);
            this.label286.TabIndex = 15;
            this.label286.Text = "GoIn31";
            // 
            // goin62
            // 
            this.goin62.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin62.FormattingEnabled = true;
            this.goin62.Location = new System.Drawing.Point(553, 370);
            this.goin62.Name = "goin62";
            this.goin62.Size = new System.Drawing.Size(75, 21);
            this.goin62.TabIndex = 25;
            // 
            // goin46
            // 
            this.goin46.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin46.FormattingEnabled = true;
            this.goin46.Location = new System.Drawing.Point(384, 370);
            this.goin46.Name = "goin46";
            this.goin46.Size = new System.Drawing.Size(75, 21);
            this.goin46.TabIndex = 25;
            // 
            // goin30
            // 
            this.goin30.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin30.FormattingEnabled = true;
            this.goin30.Location = new System.Drawing.Point(217, 370);
            this.goin30.Name = "goin30";
            this.goin30.Size = new System.Drawing.Size(75, 21);
            this.goin30.TabIndex = 25;
            // 
            // label287
            // 
            this.label287.AutoSize = true;
            this.label287.Location = new System.Drawing.Point(505, 373);
            this.label287.Name = "label287";
            this.label287.Size = new System.Drawing.Size(42, 13);
            this.label287.TabIndex = 17;
            this.label287.Text = "GoIn62";
            // 
            // label288
            // 
            this.label288.AutoSize = true;
            this.label288.Location = new System.Drawing.Point(336, 373);
            this.label288.Name = "label288";
            this.label288.Size = new System.Drawing.Size(42, 13);
            this.label288.TabIndex = 17;
            this.label288.Text = "GoIn46";
            // 
            // label289
            // 
            this.label289.AutoSize = true;
            this.label289.Location = new System.Drawing.Point(169, 373);
            this.label289.Name = "label289";
            this.label289.Size = new System.Drawing.Size(42, 13);
            this.label289.TabIndex = 17;
            this.label289.Text = "GoIn30";
            // 
            // goin61
            // 
            this.goin61.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin61.FormattingEnabled = true;
            this.goin61.Location = new System.Drawing.Point(553, 343);
            this.goin61.Name = "goin61";
            this.goin61.Size = new System.Drawing.Size(75, 21);
            this.goin61.TabIndex = 21;
            // 
            // goin45
            // 
            this.goin45.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin45.FormattingEnabled = true;
            this.goin45.Location = new System.Drawing.Point(384, 343);
            this.goin45.Name = "goin45";
            this.goin45.Size = new System.Drawing.Size(75, 21);
            this.goin45.TabIndex = 21;
            // 
            // goin29
            // 
            this.goin29.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin29.FormattingEnabled = true;
            this.goin29.Location = new System.Drawing.Point(217, 343);
            this.goin29.Name = "goin29";
            this.goin29.Size = new System.Drawing.Size(75, 21);
            this.goin29.TabIndex = 21;
            // 
            // label290
            // 
            this.label290.AutoSize = true;
            this.label290.Location = new System.Drawing.Point(505, 346);
            this.label290.Name = "label290";
            this.label290.Size = new System.Drawing.Size(42, 13);
            this.label290.TabIndex = 16;
            this.label290.Text = "GoIn61";
            // 
            // label291
            // 
            this.label291.AutoSize = true;
            this.label291.Location = new System.Drawing.Point(336, 346);
            this.label291.Name = "label291";
            this.label291.Size = new System.Drawing.Size(42, 13);
            this.label291.TabIndex = 16;
            this.label291.Text = "GoIn45";
            // 
            // label292
            // 
            this.label292.AutoSize = true;
            this.label292.Location = new System.Drawing.Point(169, 346);
            this.label292.Name = "label292";
            this.label292.Size = new System.Drawing.Size(42, 13);
            this.label292.TabIndex = 16;
            this.label292.Text = "GoIn29";
            // 
            // goin60
            // 
            this.goin60.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin60.FormattingEnabled = true;
            this.goin60.Location = new System.Drawing.Point(553, 316);
            this.goin60.Name = "goin60";
            this.goin60.Size = new System.Drawing.Size(75, 21);
            this.goin60.TabIndex = 27;
            // 
            // goin44
            // 
            this.goin44.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin44.FormattingEnabled = true;
            this.goin44.Location = new System.Drawing.Point(384, 316);
            this.goin44.Name = "goin44";
            this.goin44.Size = new System.Drawing.Size(75, 21);
            this.goin44.TabIndex = 27;
            // 
            // goin28
            // 
            this.goin28.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin28.FormattingEnabled = true;
            this.goin28.Location = new System.Drawing.Point(217, 316);
            this.goin28.Name = "goin28";
            this.goin28.Size = new System.Drawing.Size(75, 21);
            this.goin28.TabIndex = 27;
            // 
            // label293
            // 
            this.label293.AutoSize = true;
            this.label293.Location = new System.Drawing.Point(505, 319);
            this.label293.Name = "label293";
            this.label293.Size = new System.Drawing.Size(42, 13);
            this.label293.TabIndex = 13;
            this.label293.Text = "GoIn60";
            // 
            // label294
            // 
            this.label294.AutoSize = true;
            this.label294.Location = new System.Drawing.Point(336, 319);
            this.label294.Name = "label294";
            this.label294.Size = new System.Drawing.Size(42, 13);
            this.label294.TabIndex = 13;
            this.label294.Text = "GoIn44";
            // 
            // label295
            // 
            this.label295.AutoSize = true;
            this.label295.Location = new System.Drawing.Point(169, 319);
            this.label295.Name = "label295";
            this.label295.Size = new System.Drawing.Size(42, 13);
            this.label295.TabIndex = 13;
            this.label295.Text = "GoIn28";
            // 
            // goin59
            // 
            this.goin59.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin59.FormattingEnabled = true;
            this.goin59.Location = new System.Drawing.Point(553, 289);
            this.goin59.Name = "goin59";
            this.goin59.Size = new System.Drawing.Size(75, 21);
            this.goin59.TabIndex = 23;
            // 
            // goin43
            // 
            this.goin43.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin43.FormattingEnabled = true;
            this.goin43.Location = new System.Drawing.Point(384, 289);
            this.goin43.Name = "goin43";
            this.goin43.Size = new System.Drawing.Size(75, 21);
            this.goin43.TabIndex = 23;
            // 
            // goin27
            // 
            this.goin27.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin27.FormattingEnabled = true;
            this.goin27.Location = new System.Drawing.Point(217, 289);
            this.goin27.Name = "goin27";
            this.goin27.Size = new System.Drawing.Size(75, 21);
            this.goin27.TabIndex = 23;
            // 
            // label296
            // 
            this.label296.AutoSize = true;
            this.label296.Location = new System.Drawing.Point(505, 292);
            this.label296.Name = "label296";
            this.label296.Size = new System.Drawing.Size(42, 13);
            this.label296.TabIndex = 11;
            this.label296.Text = "GoIn59";
            // 
            // label297
            // 
            this.label297.AutoSize = true;
            this.label297.Location = new System.Drawing.Point(336, 292);
            this.label297.Name = "label297";
            this.label297.Size = new System.Drawing.Size(42, 13);
            this.label297.TabIndex = 11;
            this.label297.Text = "GoIn43";
            // 
            // label298
            // 
            this.label298.AutoSize = true;
            this.label298.Location = new System.Drawing.Point(169, 292);
            this.label298.Name = "label298";
            this.label298.Size = new System.Drawing.Size(42, 13);
            this.label298.TabIndex = 11;
            this.label298.Text = "GoIn27";
            // 
            // goin58
            // 
            this.goin58.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin58.FormattingEnabled = true;
            this.goin58.Location = new System.Drawing.Point(553, 262);
            this.goin58.Name = "goin58";
            this.goin58.Size = new System.Drawing.Size(75, 21);
            this.goin58.TabIndex = 31;
            // 
            // goin42
            // 
            this.goin42.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin42.FormattingEnabled = true;
            this.goin42.Location = new System.Drawing.Point(384, 262);
            this.goin42.Name = "goin42";
            this.goin42.Size = new System.Drawing.Size(75, 21);
            this.goin42.TabIndex = 31;
            // 
            // goin26
            // 
            this.goin26.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin26.FormattingEnabled = true;
            this.goin26.Location = new System.Drawing.Point(217, 262);
            this.goin26.Name = "goin26";
            this.goin26.Size = new System.Drawing.Size(75, 21);
            this.goin26.TabIndex = 31;
            // 
            // label299
            // 
            this.label299.AutoSize = true;
            this.label299.Location = new System.Drawing.Point(505, 265);
            this.label299.Name = "label299";
            this.label299.Size = new System.Drawing.Size(42, 13);
            this.label299.TabIndex = 10;
            this.label299.Text = "GoIn58";
            // 
            // label300
            // 
            this.label300.AutoSize = true;
            this.label300.Location = new System.Drawing.Point(336, 265);
            this.label300.Name = "label300";
            this.label300.Size = new System.Drawing.Size(42, 13);
            this.label300.TabIndex = 10;
            this.label300.Text = "GoIn42";
            // 
            // label301
            // 
            this.label301.AutoSize = true;
            this.label301.Location = new System.Drawing.Point(169, 265);
            this.label301.Name = "label301";
            this.label301.Size = new System.Drawing.Size(42, 13);
            this.label301.TabIndex = 10;
            this.label301.Text = "GoIn26";
            // 
            // goin57
            // 
            this.goin57.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin57.FormattingEnabled = true;
            this.goin57.Location = new System.Drawing.Point(553, 235);
            this.goin57.Name = "goin57";
            this.goin57.Size = new System.Drawing.Size(75, 21);
            this.goin57.TabIndex = 19;
            // 
            // goin41
            // 
            this.goin41.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin41.FormattingEnabled = true;
            this.goin41.Location = new System.Drawing.Point(384, 235);
            this.goin41.Name = "goin41";
            this.goin41.Size = new System.Drawing.Size(75, 21);
            this.goin41.TabIndex = 19;
            // 
            // goin25
            // 
            this.goin25.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin25.FormattingEnabled = true;
            this.goin25.Location = new System.Drawing.Point(217, 235);
            this.goin25.Name = "goin25";
            this.goin25.Size = new System.Drawing.Size(75, 21);
            this.goin25.TabIndex = 19;
            // 
            // label302
            // 
            this.label302.AutoSize = true;
            this.label302.Location = new System.Drawing.Point(505, 238);
            this.label302.Name = "label302";
            this.label302.Size = new System.Drawing.Size(42, 13);
            this.label302.TabIndex = 2;
            this.label302.Text = "GoIn57";
            // 
            // label303
            // 
            this.label303.AutoSize = true;
            this.label303.Location = new System.Drawing.Point(336, 238);
            this.label303.Name = "label303";
            this.label303.Size = new System.Drawing.Size(42, 13);
            this.label303.TabIndex = 2;
            this.label303.Text = "GoIn41";
            // 
            // label304
            // 
            this.label304.AutoSize = true;
            this.label304.Location = new System.Drawing.Point(169, 238);
            this.label304.Name = "label304";
            this.label304.Size = new System.Drawing.Size(42, 13);
            this.label304.TabIndex = 2;
            this.label304.Text = "GoIn25";
            // 
            // goin56
            // 
            this.goin56.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin56.FormattingEnabled = true;
            this.goin56.Location = new System.Drawing.Point(553, 208);
            this.goin56.Name = "goin56";
            this.goin56.Size = new System.Drawing.Size(75, 21);
            this.goin56.TabIndex = 18;
            // 
            // goin40
            // 
            this.goin40.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin40.FormattingEnabled = true;
            this.goin40.Location = new System.Drawing.Point(384, 208);
            this.goin40.Name = "goin40";
            this.goin40.Size = new System.Drawing.Size(75, 21);
            this.goin40.TabIndex = 18;
            // 
            // goin24
            // 
            this.goin24.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin24.FormattingEnabled = true;
            this.goin24.Location = new System.Drawing.Point(217, 208);
            this.goin24.Name = "goin24";
            this.goin24.Size = new System.Drawing.Size(75, 21);
            this.goin24.TabIndex = 18;
            // 
            // label305
            // 
            this.label305.AutoSize = true;
            this.label305.Location = new System.Drawing.Point(505, 211);
            this.label305.Name = "label305";
            this.label305.Size = new System.Drawing.Size(42, 13);
            this.label305.TabIndex = 9;
            this.label305.Text = "GoIn56";
            // 
            // label306
            // 
            this.label306.AutoSize = true;
            this.label306.Location = new System.Drawing.Point(336, 211);
            this.label306.Name = "label306";
            this.label306.Size = new System.Drawing.Size(42, 13);
            this.label306.TabIndex = 9;
            this.label306.Text = "GoIn40";
            // 
            // label307
            // 
            this.label307.AutoSize = true;
            this.label307.Location = new System.Drawing.Point(169, 211);
            this.label307.Name = "label307";
            this.label307.Size = new System.Drawing.Size(42, 13);
            this.label307.TabIndex = 9;
            this.label307.Text = "GoIn24";
            // 
            // goin55
            // 
            this.goin55.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin55.FormattingEnabled = true;
            this.goin55.Location = new System.Drawing.Point(553, 181);
            this.goin55.Name = "goin55";
            this.goin55.Size = new System.Drawing.Size(75, 21);
            this.goin55.TabIndex = 20;
            // 
            // goin39
            // 
            this.goin39.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin39.FormattingEnabled = true;
            this.goin39.Location = new System.Drawing.Point(384, 181);
            this.goin39.Name = "goin39";
            this.goin39.Size = new System.Drawing.Size(75, 21);
            this.goin39.TabIndex = 20;
            // 
            // goin23
            // 
            this.goin23.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin23.FormattingEnabled = true;
            this.goin23.Location = new System.Drawing.Point(217, 181);
            this.goin23.Name = "goin23";
            this.goin23.Size = new System.Drawing.Size(75, 21);
            this.goin23.TabIndex = 20;
            // 
            // label308
            // 
            this.label308.AutoSize = true;
            this.label308.Location = new System.Drawing.Point(505, 184);
            this.label308.Name = "label308";
            this.label308.Size = new System.Drawing.Size(42, 13);
            this.label308.TabIndex = 8;
            this.label308.Text = "GoIn55";
            // 
            // label309
            // 
            this.label309.AutoSize = true;
            this.label309.Location = new System.Drawing.Point(336, 184);
            this.label309.Name = "label309";
            this.label309.Size = new System.Drawing.Size(42, 13);
            this.label309.TabIndex = 8;
            this.label309.Text = "GoIn39";
            // 
            // label310
            // 
            this.label310.AutoSize = true;
            this.label310.Location = new System.Drawing.Point(169, 184);
            this.label310.Name = "label310";
            this.label310.Size = new System.Drawing.Size(42, 13);
            this.label310.TabIndex = 8;
            this.label310.Text = "GoIn23";
            // 
            // goin54
            // 
            this.goin54.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin54.FormattingEnabled = true;
            this.goin54.Location = new System.Drawing.Point(553, 154);
            this.goin54.Name = "goin54";
            this.goin54.Size = new System.Drawing.Size(75, 21);
            this.goin54.TabIndex = 22;
            // 
            // goin38
            // 
            this.goin38.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin38.FormattingEnabled = true;
            this.goin38.Location = new System.Drawing.Point(384, 154);
            this.goin38.Name = "goin38";
            this.goin38.Size = new System.Drawing.Size(75, 21);
            this.goin38.TabIndex = 22;
            // 
            // goin22
            // 
            this.goin22.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin22.FormattingEnabled = true;
            this.goin22.Location = new System.Drawing.Point(217, 154);
            this.goin22.Name = "goin22";
            this.goin22.Size = new System.Drawing.Size(75, 21);
            this.goin22.TabIndex = 22;
            // 
            // label311
            // 
            this.label311.AutoSize = true;
            this.label311.Location = new System.Drawing.Point(505, 157);
            this.label311.Name = "label311";
            this.label311.Size = new System.Drawing.Size(42, 13);
            this.label311.TabIndex = 7;
            this.label311.Text = "GoIn54";
            // 
            // label312
            // 
            this.label312.AutoSize = true;
            this.label312.Location = new System.Drawing.Point(336, 157);
            this.label312.Name = "label312";
            this.label312.Size = new System.Drawing.Size(42, 13);
            this.label312.TabIndex = 7;
            this.label312.Text = "GoIn38";
            // 
            // label313
            // 
            this.label313.AutoSize = true;
            this.label313.Location = new System.Drawing.Point(169, 157);
            this.label313.Name = "label313";
            this.label313.Size = new System.Drawing.Size(42, 13);
            this.label313.TabIndex = 7;
            this.label313.Text = "GoIn22";
            // 
            // goin53
            // 
            this.goin53.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin53.FormattingEnabled = true;
            this.goin53.Location = new System.Drawing.Point(553, 127);
            this.goin53.Name = "goin53";
            this.goin53.Size = new System.Drawing.Size(75, 21);
            this.goin53.TabIndex = 24;
            // 
            // goin37
            // 
            this.goin37.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin37.FormattingEnabled = true;
            this.goin37.Location = new System.Drawing.Point(384, 127);
            this.goin37.Name = "goin37";
            this.goin37.Size = new System.Drawing.Size(75, 21);
            this.goin37.TabIndex = 24;
            // 
            // goin21
            // 
            this.goin21.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin21.FormattingEnabled = true;
            this.goin21.Location = new System.Drawing.Point(217, 127);
            this.goin21.Name = "goin21";
            this.goin21.Size = new System.Drawing.Size(75, 21);
            this.goin21.TabIndex = 24;
            // 
            // label314
            // 
            this.label314.AutoSize = true;
            this.label314.Location = new System.Drawing.Point(505, 130);
            this.label314.Name = "label314";
            this.label314.Size = new System.Drawing.Size(42, 13);
            this.label314.TabIndex = 6;
            this.label314.Text = "GoIn53";
            // 
            // label315
            // 
            this.label315.AutoSize = true;
            this.label315.Location = new System.Drawing.Point(336, 130);
            this.label315.Name = "label315";
            this.label315.Size = new System.Drawing.Size(42, 13);
            this.label315.TabIndex = 6;
            this.label315.Text = "GoIn37";
            // 
            // label316
            // 
            this.label316.AutoSize = true;
            this.label316.Location = new System.Drawing.Point(169, 130);
            this.label316.Name = "label316";
            this.label316.Size = new System.Drawing.Size(42, 13);
            this.label316.TabIndex = 6;
            this.label316.Text = "GoIn21";
            // 
            // goin52
            // 
            this.goin52.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin52.FormattingEnabled = true;
            this.goin52.Location = new System.Drawing.Point(553, 100);
            this.goin52.Name = "goin52";
            this.goin52.Size = new System.Drawing.Size(75, 21);
            this.goin52.TabIndex = 26;
            // 
            // goin36
            // 
            this.goin36.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin36.FormattingEnabled = true;
            this.goin36.Location = new System.Drawing.Point(384, 100);
            this.goin36.Name = "goin36";
            this.goin36.Size = new System.Drawing.Size(75, 21);
            this.goin36.TabIndex = 26;
            // 
            // goin20
            // 
            this.goin20.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin20.FormattingEnabled = true;
            this.goin20.Location = new System.Drawing.Point(217, 100);
            this.goin20.Name = "goin20";
            this.goin20.Size = new System.Drawing.Size(75, 21);
            this.goin20.TabIndex = 26;
            // 
            // label317
            // 
            this.label317.AutoSize = true;
            this.label317.Location = new System.Drawing.Point(505, 103);
            this.label317.Name = "label317";
            this.label317.Size = new System.Drawing.Size(42, 13);
            this.label317.TabIndex = 5;
            this.label317.Text = "GoIn52";
            // 
            // label318
            // 
            this.label318.AutoSize = true;
            this.label318.Location = new System.Drawing.Point(336, 103);
            this.label318.Name = "label318";
            this.label318.Size = new System.Drawing.Size(42, 13);
            this.label318.TabIndex = 5;
            this.label318.Text = "GoIn36";
            // 
            // label319
            // 
            this.label319.AutoSize = true;
            this.label319.Location = new System.Drawing.Point(169, 103);
            this.label319.Name = "label319";
            this.label319.Size = new System.Drawing.Size(42, 13);
            this.label319.TabIndex = 5;
            this.label319.Text = "GoIn20";
            // 
            // goin51
            // 
            this.goin51.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin51.FormattingEnabled = true;
            this.goin51.Location = new System.Drawing.Point(553, 73);
            this.goin51.Name = "goin51";
            this.goin51.Size = new System.Drawing.Size(75, 21);
            this.goin51.TabIndex = 28;
            // 
            // goin35
            // 
            this.goin35.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin35.FormattingEnabled = true;
            this.goin35.Location = new System.Drawing.Point(384, 73);
            this.goin35.Name = "goin35";
            this.goin35.Size = new System.Drawing.Size(75, 21);
            this.goin35.TabIndex = 28;
            // 
            // goin19
            // 
            this.goin19.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin19.FormattingEnabled = true;
            this.goin19.Location = new System.Drawing.Point(217, 73);
            this.goin19.Name = "goin19";
            this.goin19.Size = new System.Drawing.Size(75, 21);
            this.goin19.TabIndex = 28;
            // 
            // label320
            // 
            this.label320.AutoSize = true;
            this.label320.Location = new System.Drawing.Point(505, 76);
            this.label320.Name = "label320";
            this.label320.Size = new System.Drawing.Size(42, 13);
            this.label320.TabIndex = 4;
            this.label320.Text = "GoIn51";
            // 
            // label321
            // 
            this.label321.AutoSize = true;
            this.label321.Location = new System.Drawing.Point(336, 76);
            this.label321.Name = "label321";
            this.label321.Size = new System.Drawing.Size(42, 13);
            this.label321.TabIndex = 4;
            this.label321.Text = "GoIn35";
            // 
            // label322
            // 
            this.label322.AutoSize = true;
            this.label322.Location = new System.Drawing.Point(169, 76);
            this.label322.Name = "label322";
            this.label322.Size = new System.Drawing.Size(42, 13);
            this.label322.TabIndex = 4;
            this.label322.Text = "GoIn19";
            // 
            // goin50
            // 
            this.goin50.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin50.FormattingEnabled = true;
            this.goin50.Location = new System.Drawing.Point(553, 46);
            this.goin50.Name = "goin50";
            this.goin50.Size = new System.Drawing.Size(75, 21);
            this.goin50.TabIndex = 30;
            // 
            // goin34
            // 
            this.goin34.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin34.FormattingEnabled = true;
            this.goin34.Location = new System.Drawing.Point(384, 46);
            this.goin34.Name = "goin34";
            this.goin34.Size = new System.Drawing.Size(75, 21);
            this.goin34.TabIndex = 30;
            // 
            // goin18
            // 
            this.goin18.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin18.FormattingEnabled = true;
            this.goin18.Location = new System.Drawing.Point(217, 46);
            this.goin18.Name = "goin18";
            this.goin18.Size = new System.Drawing.Size(75, 21);
            this.goin18.TabIndex = 30;
            // 
            // label323
            // 
            this.label323.AutoSize = true;
            this.label323.Location = new System.Drawing.Point(505, 49);
            this.label323.Name = "label323";
            this.label323.Size = new System.Drawing.Size(42, 13);
            this.label323.TabIndex = 3;
            this.label323.Text = "GoIn50";
            // 
            // label324
            // 
            this.label324.AutoSize = true;
            this.label324.Location = new System.Drawing.Point(336, 49);
            this.label324.Name = "label324";
            this.label324.Size = new System.Drawing.Size(42, 13);
            this.label324.TabIndex = 3;
            this.label324.Text = "GoIn34";
            // 
            // label325
            // 
            this.label325.AutoSize = true;
            this.label325.Location = new System.Drawing.Point(169, 49);
            this.label325.Name = "label325";
            this.label325.Size = new System.Drawing.Size(42, 13);
            this.label325.TabIndex = 3;
            this.label325.Text = "GoIn18";
            // 
            // goin49
            // 
            this.goin49.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin49.FormattingEnabled = true;
            this.goin49.Location = new System.Drawing.Point(553, 19);
            this.goin49.Name = "goin49";
            this.goin49.Size = new System.Drawing.Size(75, 21);
            this.goin49.TabIndex = 32;
            // 
            // label326
            // 
            this.label326.AutoSize = true;
            this.label326.Location = new System.Drawing.Point(505, 22);
            this.label326.Name = "label326";
            this.label326.Size = new System.Drawing.Size(42, 13);
            this.label326.TabIndex = 12;
            this.label326.Text = "GoIn49";
            // 
            // goin33
            // 
            this.goin33.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin33.FormattingEnabled = true;
            this.goin33.Location = new System.Drawing.Point(384, 19);
            this.goin33.Name = "goin33";
            this.goin33.Size = new System.Drawing.Size(75, 21);
            this.goin33.TabIndex = 32;
            // 
            // label327
            // 
            this.label327.AutoSize = true;
            this.label327.Location = new System.Drawing.Point(336, 22);
            this.label327.Name = "label327";
            this.label327.Size = new System.Drawing.Size(42, 13);
            this.label327.TabIndex = 12;
            this.label327.Text = "GoIn33";
            // 
            // goin17
            // 
            this.goin17.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin17.FormattingEnabled = true;
            this.goin17.Location = new System.Drawing.Point(217, 19);
            this.goin17.Name = "goin17";
            this.goin17.Size = new System.Drawing.Size(75, 21);
            this.goin17.TabIndex = 32;
            // 
            // label328
            // 
            this.label328.AutoSize = true;
            this.label328.Location = new System.Drawing.Point(169, 22);
            this.label328.Name = "label328";
            this.label328.Size = new System.Drawing.Size(42, 13);
            this.label328.TabIndex = 12;
            this.label328.Text = "GoIn17";
            // 
            // goin16
            // 
            this.goin16.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin16.FormattingEnabled = true;
            this.goin16.Location = new System.Drawing.Point(54, 424);
            this.goin16.Name = "goin16";
            this.goin16.Size = new System.Drawing.Size(75, 21);
            this.goin16.TabIndex = 1;
            // 
            // label329
            // 
            this.label329.AutoSize = true;
            this.label329.Location = new System.Drawing.Point(6, 427);
            this.label329.Name = "label329";
            this.label329.Size = new System.Drawing.Size(42, 13);
            this.label329.TabIndex = 0;
            this.label329.Text = "GoIn16";
            // 
            // goin15
            // 
            this.goin15.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin15.FormattingEnabled = true;
            this.goin15.Location = new System.Drawing.Point(54, 397);
            this.goin15.Name = "goin15";
            this.goin15.Size = new System.Drawing.Size(75, 21);
            this.goin15.TabIndex = 1;
            // 
            // label330
            // 
            this.label330.AutoSize = true;
            this.label330.Location = new System.Drawing.Point(6, 400);
            this.label330.Name = "label330";
            this.label330.Size = new System.Drawing.Size(42, 13);
            this.label330.TabIndex = 0;
            this.label330.Text = "GoIn15";
            // 
            // goin14
            // 
            this.goin14.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin14.FormattingEnabled = true;
            this.goin14.Location = new System.Drawing.Point(54, 370);
            this.goin14.Name = "goin14";
            this.goin14.Size = new System.Drawing.Size(75, 21);
            this.goin14.TabIndex = 1;
            // 
            // label331
            // 
            this.label331.AutoSize = true;
            this.label331.Location = new System.Drawing.Point(6, 373);
            this.label331.Name = "label331";
            this.label331.Size = new System.Drawing.Size(42, 13);
            this.label331.TabIndex = 0;
            this.label331.Text = "GoIn14";
            // 
            // goin13
            // 
            this.goin13.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin13.FormattingEnabled = true;
            this.goin13.Location = new System.Drawing.Point(54, 343);
            this.goin13.Name = "goin13";
            this.goin13.Size = new System.Drawing.Size(75, 21);
            this.goin13.TabIndex = 1;
            // 
            // label332
            // 
            this.label332.AutoSize = true;
            this.label332.Location = new System.Drawing.Point(6, 346);
            this.label332.Name = "label332";
            this.label332.Size = new System.Drawing.Size(42, 13);
            this.label332.TabIndex = 0;
            this.label332.Text = "GoIn13";
            // 
            // goin12
            // 
            this.goin12.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin12.FormattingEnabled = true;
            this.goin12.Location = new System.Drawing.Point(54, 316);
            this.goin12.Name = "goin12";
            this.goin12.Size = new System.Drawing.Size(75, 21);
            this.goin12.TabIndex = 1;
            // 
            // label333
            // 
            this.label333.AutoSize = true;
            this.label333.Location = new System.Drawing.Point(6, 319);
            this.label333.Name = "label333";
            this.label333.Size = new System.Drawing.Size(42, 13);
            this.label333.TabIndex = 0;
            this.label333.Text = "GoIn12";
            // 
            // goin11
            // 
            this.goin11.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin11.FormattingEnabled = true;
            this.goin11.Location = new System.Drawing.Point(54, 289);
            this.goin11.Name = "goin11";
            this.goin11.Size = new System.Drawing.Size(75, 21);
            this.goin11.TabIndex = 1;
            // 
            // label334
            // 
            this.label334.AutoSize = true;
            this.label334.Location = new System.Drawing.Point(6, 292);
            this.label334.Name = "label334";
            this.label334.Size = new System.Drawing.Size(42, 13);
            this.label334.TabIndex = 0;
            this.label334.Text = "GoIn11";
            // 
            // goin10
            // 
            this.goin10.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin10.FormattingEnabled = true;
            this.goin10.Location = new System.Drawing.Point(54, 262);
            this.goin10.Name = "goin10";
            this.goin10.Size = new System.Drawing.Size(75, 21);
            this.goin10.TabIndex = 1;
            // 
            // label335
            // 
            this.label335.AutoSize = true;
            this.label335.Location = new System.Drawing.Point(6, 265);
            this.label335.Name = "label335";
            this.label335.Size = new System.Drawing.Size(42, 13);
            this.label335.TabIndex = 0;
            this.label335.Text = "GoIn10";
            // 
            // goin9
            // 
            this.goin9.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin9.FormattingEnabled = true;
            this.goin9.Location = new System.Drawing.Point(54, 235);
            this.goin9.Name = "goin9";
            this.goin9.Size = new System.Drawing.Size(75, 21);
            this.goin9.TabIndex = 1;
            // 
            // label336
            // 
            this.label336.AutoSize = true;
            this.label336.Location = new System.Drawing.Point(6, 238);
            this.label336.Name = "label336";
            this.label336.Size = new System.Drawing.Size(36, 13);
            this.label336.TabIndex = 0;
            this.label336.Text = "GoIn9";
            // 
            // goin8
            // 
            this.goin8.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin8.FormattingEnabled = true;
            this.goin8.Location = new System.Drawing.Point(54, 208);
            this.goin8.Name = "goin8";
            this.goin8.Size = new System.Drawing.Size(75, 21);
            this.goin8.TabIndex = 1;
            // 
            // label337
            // 
            this.label337.AutoSize = true;
            this.label337.Location = new System.Drawing.Point(6, 211);
            this.label337.Name = "label337";
            this.label337.Size = new System.Drawing.Size(36, 13);
            this.label337.TabIndex = 0;
            this.label337.Text = "GoIn8";
            // 
            // goin7
            // 
            this.goin7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin7.FormattingEnabled = true;
            this.goin7.Location = new System.Drawing.Point(54, 181);
            this.goin7.Name = "goin7";
            this.goin7.Size = new System.Drawing.Size(75, 21);
            this.goin7.TabIndex = 1;
            // 
            // label338
            // 
            this.label338.AutoSize = true;
            this.label338.Location = new System.Drawing.Point(6, 184);
            this.label338.Name = "label338";
            this.label338.Size = new System.Drawing.Size(36, 13);
            this.label338.TabIndex = 0;
            this.label338.Text = "GoIn7";
            // 
            // goin6
            // 
            this.goin6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin6.FormattingEnabled = true;
            this.goin6.Location = new System.Drawing.Point(54, 154);
            this.goin6.Name = "goin6";
            this.goin6.Size = new System.Drawing.Size(75, 21);
            this.goin6.TabIndex = 1;
            // 
            // label339
            // 
            this.label339.AutoSize = true;
            this.label339.Location = new System.Drawing.Point(6, 157);
            this.label339.Name = "label339";
            this.label339.Size = new System.Drawing.Size(36, 13);
            this.label339.TabIndex = 0;
            this.label339.Text = "GoIn6";
            // 
            // goin5
            // 
            this.goin5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin5.FormattingEnabled = true;
            this.goin5.Location = new System.Drawing.Point(54, 127);
            this.goin5.Name = "goin5";
            this.goin5.Size = new System.Drawing.Size(75, 21);
            this.goin5.TabIndex = 1;
            // 
            // label340
            // 
            this.label340.AutoSize = true;
            this.label340.Location = new System.Drawing.Point(6, 130);
            this.label340.Name = "label340";
            this.label340.Size = new System.Drawing.Size(36, 13);
            this.label340.TabIndex = 0;
            this.label340.Text = "GoIn5";
            // 
            // goin4
            // 
            this.goin4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin4.FormattingEnabled = true;
            this.goin4.Location = new System.Drawing.Point(54, 100);
            this.goin4.Name = "goin4";
            this.goin4.Size = new System.Drawing.Size(75, 21);
            this.goin4.TabIndex = 1;
            // 
            // label341
            // 
            this.label341.AutoSize = true;
            this.label341.Location = new System.Drawing.Point(6, 103);
            this.label341.Name = "label341";
            this.label341.Size = new System.Drawing.Size(36, 13);
            this.label341.TabIndex = 0;
            this.label341.Text = "GoIn4";
            // 
            // goin3
            // 
            this.goin3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin3.FormattingEnabled = true;
            this.goin3.Location = new System.Drawing.Point(54, 73);
            this.goin3.Name = "goin3";
            this.goin3.Size = new System.Drawing.Size(75, 21);
            this.goin3.TabIndex = 1;
            // 
            // label342
            // 
            this.label342.AutoSize = true;
            this.label342.Location = new System.Drawing.Point(6, 76);
            this.label342.Name = "label342";
            this.label342.Size = new System.Drawing.Size(36, 13);
            this.label342.TabIndex = 0;
            this.label342.Text = "GoIn3";
            // 
            // goin2
            // 
            this.goin2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin2.FormattingEnabled = true;
            this.goin2.Location = new System.Drawing.Point(54, 46);
            this.goin2.Name = "goin2";
            this.goin2.Size = new System.Drawing.Size(75, 21);
            this.goin2.TabIndex = 1;
            // 
            // label343
            // 
            this.label343.AutoSize = true;
            this.label343.Location = new System.Drawing.Point(6, 49);
            this.label343.Name = "label343";
            this.label343.Size = new System.Drawing.Size(36, 13);
            this.label343.TabIndex = 0;
            this.label343.Text = "GoIn2";
            // 
            // goin1
            // 
            this.goin1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin1.FormattingEnabled = true;
            this.goin1.Location = new System.Drawing.Point(54, 19);
            this.goin1.Name = "goin1";
            this.goin1.Size = new System.Drawing.Size(75, 21);
            this.goin1.TabIndex = 1;
            // 
            // label344
            // 
            this.label344.AutoSize = true;
            this.label344.Location = new System.Drawing.Point(6, 22);
            this.label344.Name = "label344";
            this.label344.Size = new System.Drawing.Size(36, 13);
            this.label344.TabIndex = 0;
            this.label344.Text = "GoIn1";
            // 
            // label345
            // 
            this.label345.AutoSize = true;
            this.label345.Location = new System.Drawing.Point(6, 22);
            this.label345.Name = "label345";
            this.label345.Size = new System.Drawing.Size(57, 13);
            this.label345.TabIndex = 6;
            this.label345.Text = "Операция";
            // 
            // operationBGS
            // 
            this.operationBGS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.operationBGS.FormattingEnabled = true;
            this.operationBGS.Location = new System.Drawing.Point(69, 19);
            this.operationBGS.Name = "operationBGS";
            this.operationBGS.Size = new System.Drawing.Size(112, 21);
            this.operationBGS.TabIndex = 7;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(13, 10);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(89, 13);
            this.label36.TabIndex = 10;
            this.label36.Text = "Выбранный БГС";
            // 
            // currentBGS
            // 
            this.currentBGS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.currentBGS.FormattingEnabled = true;
            this.currentBGS.Location = new System.Drawing.Point(108, 7);
            this.currentBGS.Name = "currentBGS";
            this.currentBGS.Size = new System.Drawing.Size(102, 21);
            this.currentBGS.TabIndex = 11;
            // 
            // _ethernetPage
            // 
            this._ethernetPage.Controls.Add(this.groupBox51);
            this._ethernetPage.Location = new System.Drawing.Point(4, 22);
            this._ethernetPage.Name = "_ethernetPage";
            this._ethernetPage.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this._ethernetPage.Size = new System.Drawing.Size(976, 553);
            this._ethernetPage.TabIndex = 11;
            this._ethernetPage.Text = "Конфигурация Ethernet";
            this._ethernetPage.UseVisualStyleBackColor = true;
            // 
            // groupBox51
            // 
            this.groupBox51.Controls.Add(this._ipLo1);
            this.groupBox51.Controls.Add(this._ipLo2);
            this.groupBox51.Controls.Add(this._ipHi1);
            this.groupBox51.Controls.Add(this._ipHi2);
            this.groupBox51.Controls.Add(this.label214);
            this.groupBox51.Controls.Add(this.label213);
            this.groupBox51.Controls.Add(this.label212);
            this.groupBox51.Controls.Add(this.label179);
            this.groupBox51.Location = new System.Drawing.Point(3, 3);
            this.groupBox51.Name = "groupBox51";
            this.groupBox51.Size = new System.Drawing.Size(261, 53);
            this.groupBox51.TabIndex = 1;
            this.groupBox51.TabStop = false;
            this.groupBox51.Text = "Конфигурация Ethernet";
            // 
            // _ipLo1
            // 
            this._ipLo1.Location = new System.Drawing.Point(215, 19);
            this._ipLo1.Name = "_ipLo1";
            this._ipLo1.Size = new System.Drawing.Size(38, 20);
            this._ipLo1.TabIndex = 3;
            // 
            // _ipLo2
            // 
            this._ipLo2.Location = new System.Drawing.Point(163, 19);
            this._ipLo2.Name = "_ipLo2";
            this._ipLo2.Size = new System.Drawing.Size(38, 20);
            this._ipLo2.TabIndex = 2;
            // 
            // _ipHi1
            // 
            this._ipHi1.Location = new System.Drawing.Point(112, 19);
            this._ipHi1.Name = "_ipHi1";
            this._ipHi1.Size = new System.Drawing.Size(38, 20);
            this._ipHi1.TabIndex = 1;
            // 
            // _ipHi2
            // 
            this._ipHi2.Location = new System.Drawing.Point(60, 19);
            this._ipHi2.Name = "_ipHi2";
            this._ipHi2.Size = new System.Drawing.Size(38, 20);
            this._ipHi2.TabIndex = 0;
            // 
            // label214
            // 
            this.label214.AutoSize = true;
            this.label214.Location = new System.Drawing.Point(152, 26);
            this.label214.Name = "label214";
            this.label214.Size = new System.Drawing.Size(10, 13);
            this.label214.TabIndex = 9;
            this.label214.Text = ".";
            // 
            // label213
            // 
            this.label213.AutoSize = true;
            this.label213.Location = new System.Drawing.Point(203, 26);
            this.label213.Name = "label213";
            this.label213.Size = new System.Drawing.Size(10, 13);
            this.label213.TabIndex = 9;
            this.label213.Text = ".";
            // 
            // label212
            // 
            this.label212.AutoSize = true;
            this.label212.Location = new System.Drawing.Point(100, 26);
            this.label212.Name = "label212";
            this.label212.Size = new System.Drawing.Size(10, 13);
            this.label212.TabIndex = 9;
            this.label212.Text = ".";
            // 
            // label179
            // 
            this.label179.AutoSize = true;
            this.label179.Location = new System.Drawing.Point(6, 22);
            this.label179.Name = "label179";
            this.label179.Size = new System.Drawing.Size(50, 13);
            this.label179.TabIndex = 9;
            this.label179.Text = "IP-адрес";
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Location = new System.Drawing.Point(6, 116);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(169, 13);
            this.label109.TabIndex = 19;
            this.label109.Text = "5. Неисправность выключателя";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this._saveToXmlButton);
            this.panel1.Controls.Add(this._resetSetpointsButton);
            this.panel1.Controls.Add(this._writeConfigBut);
            this.panel1.Controls.Add(this.statusStrip1);
            this.panel1.Controls.Add(this._readConfigBut);
            this.panel1.Controls.Add(this._saveConfigBut);
            this.panel1.Controls.Add(this._loadConfigBut);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 582);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(984, 52);
            this.panel1.TabIndex = 33;
            // 
            // _saveToXmlButton
            // 
            this._saveToXmlButton.AllowDrop = true;
            this._saveToXmlButton.AutoSize = true;
            this._saveToXmlButton.Location = new System.Drawing.Point(868, 5);
            this._saveToXmlButton.Name = "_saveToXmlButton";
            this._saveToXmlButton.Size = new System.Drawing.Size(112, 23);
            this._saveToXmlButton.TabIndex = 0;
            this._saveToXmlButton.Text = "Сохранить в HTML";
            this._saveToXmlButton.UseVisualStyleBackColor = true;
            this._saveToXmlButton.Click += new System.EventHandler(this._saveToXmlButton_Click);
            // 
            // _resetSetpointsButton
            // 
            this._resetSetpointsButton.AutoSize = true;
            this._resetSetpointsButton.Location = new System.Drawing.Point(294, 5);
            this._resetSetpointsButton.Name = "_resetSetpointsButton";
            this._resetSetpointsButton.Size = new System.Drawing.Size(137, 23);
            this._resetSetpointsButton.TabIndex = 3;
            this._resetSetpointsButton.Text = "Обнулить уставки";
            this._resetSetpointsButton.UseVisualStyleBackColor = true;
            this._resetSetpointsButton.Click += new System.EventHandler(this._resetSetpointsButton_Click);
            // 
            // _writeConfigBut
            // 
            this._writeConfigBut.AutoSize = true;
            this._writeConfigBut.Location = new System.Drawing.Point(154, 5);
            this._writeConfigBut.Name = "_writeConfigBut";
            this._writeConfigBut.Size = new System.Drawing.Size(134, 23);
            this._writeConfigBut.TabIndex = 4;
            this._writeConfigBut.Text = "Записать в устройство";
            this.toolTip1.SetToolTip(this._writeConfigBut, "Записать конфигурацию в устройство (CTRL+W)");
            this._writeConfigBut.UseVisualStyleBackColor = true;
            this._writeConfigBut.Click += new System.EventHandler(this._writeConfigBut_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this._progressBar,
            this._statusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 30);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.statusStrip1.Size = new System.Drawing.Size(984, 22);
            this.statusStrip1.TabIndex = 26;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // _progressBar
            // 
            this._progressBar.Maximum = 90;
            this._progressBar.Name = "_progressBar";
            this._progressBar.Size = new System.Drawing.Size(100, 16);
            this._progressBar.Step = 1;
            // 
            // _statusLabel
            // 
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // _readConfigBut
            // 
            this._readConfigBut.AutoSize = true;
            this._readConfigBut.Location = new System.Drawing.Point(3, 5);
            this._readConfigBut.Name = "_readConfigBut";
            this._readConfigBut.Size = new System.Drawing.Size(145, 23);
            this._readConfigBut.TabIndex = 5;
            this._readConfigBut.Text = "Прочитать из устройства";
            this.toolTip1.SetToolTip(this._readConfigBut, "Прочитать конфигурацию из устройства (CTRL+R)");
            this._readConfigBut.UseVisualStyleBackColor = true;
            this._readConfigBut.Click += new System.EventHandler(this._readConfigBut_Click);
            // 
            // _saveConfigBut
            // 
            this._saveConfigBut.AllowDrop = true;
            this._saveConfigBut.AutoSize = true;
            this._saveConfigBut.Location = new System.Drawing.Point(754, 5);
            this._saveConfigBut.Name = "_saveConfigBut";
            this._saveConfigBut.Size = new System.Drawing.Size(108, 23);
            this._saveConfigBut.TabIndex = 1;
            this._saveConfigBut.Text = "Сохранить в файл";
            this.toolTip1.SetToolTip(this._saveConfigBut, "Сохранить конфигурацию в файл (CTRL+S)");
            this._saveConfigBut.UseVisualStyleBackColor = true;
            this._saveConfigBut.Click += new System.EventHandler(this._saveConfigBut_Click);
            // 
            // _loadConfigBut
            // 
            this._loadConfigBut.AutoSize = true;
            this._loadConfigBut.Location = new System.Drawing.Point(629, 5);
            this._loadConfigBut.Name = "_loadConfigBut";
            this._loadConfigBut.Size = new System.Drawing.Size(119, 23);
            this._loadConfigBut.TabIndex = 2;
            this._loadConfigBut.Text = "Загрузить из файла";
            this.toolTip1.SetToolTip(this._loadConfigBut, "Загрузить конфигурацию из файла (CTRL+O)");
            this._loadConfigBut.UseVisualStyleBackColor = true;
            this._loadConfigBut.Click += new System.EventHandler(this._loadConfigBut_Click);
            // 
            // _saveConfigurationDlg
            // 
            this._saveConfigurationDlg.DefaultExt = "xml";
            this._saveConfigurationDlg.Filter = "Уставки МР761 ОБР (*.xml) | *.xml";
            this._saveConfigurationDlg.Title = "Сохранить  уставки для МР761 ОБР";
            // 
            // _openConfigurationDlg
            // 
            this._openConfigurationDlg.DefaultExt = "xml";
            this._openConfigurationDlg.Filter = "Уставки МР761 ОБР (*.xml) | *.xml";
            this._openConfigurationDlg.RestoreDirectory = true;
            this._openConfigurationDlg.Title = "Открыть уставки для МР761 ОБР";
            // 
            // _saveXmlDialog
            // 
            this._saveXmlDialog.Filter = "(*.html) | *.html";
            // 
            // _externalDifSbrosColumn
            // 
            dataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle36.NullValue = false;
            this._externalDifSbrosColumn.DefaultCellStyle = dataGridViewCellStyle36;
            this._externalDifSbrosColumn.HeaderText = "Сброс";
            this._externalDifSbrosColumn.MinimumWidth = 6;
            this._externalDifSbrosColumn.Name = "_externalDifSbrosColumn";
            this._externalDifSbrosColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifSbrosColumn.Width = 50;
            // 
            // _externalDifAPVRetColumn
            // 
            dataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle37.NullValue = false;
            this._externalDifAPVRetColumn.DefaultCellStyle = dataGridViewCellStyle37;
            this._externalDifAPVRetColumn.HeaderText = "АПВ возвр.";
            this._externalDifAPVRetColumn.MinimumWidth = 6;
            this._externalDifAPVRetColumn.Name = "_externalDifAPVRetColumn";
            this._externalDifAPVRetColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifAPVRetColumn.Width = 75;
            // 
            // _externalDifAPVColumn
            // 
            dataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle38.NullValue = false;
            this._externalDifAPVColumn.DefaultCellStyle = dataGridViewCellStyle38;
            this._externalDifAPVColumn.HeaderText = "АПВ";
            this._externalDifAPVColumn.MinimumWidth = 6;
            this._externalDifAPVColumn.Name = "_externalDifAPVColumn";
            this._externalDifAPVColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifAPVColumn.Width = 50;
            // 
            // _externalDifUROVColumn
            // 
            dataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle39.NullValue = false;
            this._externalDifUROVColumn.DefaultCellStyle = dataGridViewCellStyle39;
            this._externalDifUROVColumn.HeaderText = "УРОВ";
            this._externalDifUROVColumn.MinimumWidth = 6;
            this._externalDifUROVColumn.Name = "_externalDifUROVColumn";
            this._externalDifUROVColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifUROVColumn.Width = 50;
            // 
            // _externalDifOscColumn
            // 
            dataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifOscColumn.DefaultCellStyle = dataGridViewCellStyle40;
            this._externalDifOscColumn.HeaderText = "Осциллограф";
            this._externalDifOscColumn.MinimumWidth = 6;
            this._externalDifOscColumn.Name = "_externalDifOscColumn";
            this._externalDifOscColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifOscColumn.Width = 110;
            // 
            // _externalDifBlockingColumn
            // 
            dataGridViewCellStyle41.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifBlockingColumn.DefaultCellStyle = dataGridViewCellStyle41;
            this._externalDifBlockingColumn.HeaderText = "Сигнал блокировки";
            this._externalDifBlockingColumn.MinimumWidth = 6;
            this._externalDifBlockingColumn.Name = "_externalDifBlockingColumn";
            this._externalDifBlockingColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifBlockingColumn.Width = 120;
            // 
            // _externalDifVozvrYNColumn
            // 
            dataGridViewCellStyle42.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle42.NullValue = false;
            this._externalDifVozvrYNColumn.DefaultCellStyle = dataGridViewCellStyle42;
            this._externalDifVozvrYNColumn.HeaderText = "Возврат";
            this._externalDifVozvrYNColumn.MinimumWidth = 6;
            this._externalDifVozvrYNColumn.Name = "_externalDifVozvrYNColumn";
            this._externalDifVozvrYNColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifVozvrYNColumn.Width = 65;
            // 
            // _externalDifVozvrColumn
            // 
            dataGridViewCellStyle43.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifVozvrColumn.DefaultCellStyle = dataGridViewCellStyle43;
            this._externalDifVozvrColumn.HeaderText = "Сигнал возврат";
            this._externalDifVozvrColumn.MinimumWidth = 6;
            this._externalDifVozvrColumn.Name = "_externalDifVozvrColumn";
            this._externalDifVozvrColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifVozvrColumn.Width = 125;
            // 
            // _externalDifTvzColumn
            // 
            dataGridViewCellStyle44.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifTvzColumn.DefaultCellStyle = dataGridViewCellStyle44;
            this._externalDifTvzColumn.HeaderText = "tвз [мс]";
            this._externalDifTvzColumn.MinimumWidth = 6;
            this._externalDifTvzColumn.Name = "_externalDifTvzColumn";
            this._externalDifTvzColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifTvzColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._externalDifTvzColumn.Width = 70;
            // 
            // _externalDifTsrColumn
            // 
            dataGridViewCellStyle45.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifTsrColumn.DefaultCellStyle = dataGridViewCellStyle45;
            this._externalDifTsrColumn.HeaderText = "tср [мс]";
            this._externalDifTsrColumn.MinimumWidth = 6;
            this._externalDifTsrColumn.Name = "_externalDifTsrColumn";
            this._externalDifTsrColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifTsrColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._externalDifTsrColumn.Width = 70;
            // 
            // _externalDifSrabColumn
            // 
            dataGridViewCellStyle46.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifSrabColumn.DefaultCellStyle = dataGridViewCellStyle46;
            this._externalDifSrabColumn.HeaderText = "Сигнал срабатывания";
            this._externalDifSrabColumn.MinimumWidth = 6;
            this._externalDifSrabColumn.Name = "_externalDifSrabColumn";
            this._externalDifSrabColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifSrabColumn.Width = 130;
            // 
            // _externalDifModesColumn
            // 
            dataGridViewCellStyle47.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifModesColumn.DefaultCellStyle = dataGridViewCellStyle47;
            this._externalDifModesColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._externalDifModesColumn.HeaderText = "Состояние";
            this._externalDifModesColumn.MinimumWidth = 6;
            this._externalDifModesColumn.Name = "_externalDifModesColumn";
            this._externalDifModesColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifModesColumn.Width = 110;
            // 
            // _externalDifStageColumn
            // 
            dataGridViewCellStyle48.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle48.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle48.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle48.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle48.SelectionForeColor = System.Drawing.Color.White;
            this._externalDifStageColumn.DefaultCellStyle = dataGridViewCellStyle48;
            this._externalDifStageColumn.Frozen = true;
            this._externalDifStageColumn.HeaderText = "Ступень";
            this._externalDifStageColumn.MinimumWidth = 6;
            this._externalDifStageColumn.Name = "_externalDifStageColumn";
            this._externalDifStageColumn.ReadOnly = true;
            this._externalDifStageColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifStageColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._externalDifStageColumn.Width = 125;
            // 
            // ConfigurationFormNew
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 634);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this._configurationTabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(900, 669);
            this.Name = "ConfigurationFormNew";
            this.Text = "ConfigurationForm";
            this.Activated += new System.EventHandler(this.ConfigurationFormNew_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ConfigurationForm_FormClosing);
            this.Load += new System.EventHandler(this.ConfigurationForm_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ConfigurationForm_KeyUp);
            this._configurationTabControl.ResumeLayout(false);
            this.contextMenu.ResumeLayout(false);
            this._setpointPage.ResumeLayout(false);
            this.groupBox24.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this._setpointsTab.ResumeLayout(false);
            this._defExtGr1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._externalDefensesGr1)).EndInit();
            this._logicSignalGr1.ResumeLayout(false);
            this.groupBox49.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.VLS1.ResumeLayout(false);
            this.VLS2.ResumeLayout(false);
            this.VLS3.ResumeLayout(false);
            this.VLS4.ResumeLayout(false);
            this.VLS5.ResumeLayout(false);
            this.VLS6.ResumeLayout(false);
            this.VLS7.ResumeLayout(false);
            this.VLS8.ResumeLayout(false);
            this.VLS9.ResumeLayout(false);
            this.VLS10.ResumeLayout(false);
            this.VLS11.ResumeLayout(false);
            this.VLS12.ResumeLayout(false);
            this.VLS13.ResumeLayout(false);
            this.VLS14.ResumeLayout(false);
            this.VLS15.ResumeLayout(false);
            this.VLS16.ResumeLayout(false);
            this.groupBox26.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage31.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv1Gr1)).EndInit();
            this.tabPage32.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv2Gr1)).EndInit();
            this.tabPage33.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv3Gr1)).EndInit();
            this.tabPage34.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv4Gr1)).EndInit();
            this.tabPage35.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv5Gr1)).EndInit();
            this.tabPage36.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv6Gr1)).EndInit();
            this.tabPage37.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv7Gr1)).EndInit();
            this.tabPage38.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv8Gr1)).EndInit();
            this.groupBox45.ResumeLayout(false);
            this.tabControl.ResumeLayout(false);
            this.tabPage39.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv1Gr1)).EndInit();
            this.tabPage40.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv2Gr1)).EndInit();
            this.tabPage41.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv3Gr1)).EndInit();
            this.tabPage42.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv4Gr1)).EndInit();
            this.tabPage43.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv5Gr1)).EndInit();
            this.tabPage44.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv6Gr1)).EndInit();
            this.tabPage45.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv7Gr1)).EndInit();
            this.tabPage46.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv8Gr1)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this._automatPage.ResumeLayout(false);
            this.groupBox32.ResumeLayout(false);
            this.groupBox32.PerformLayout();
            this.groupBox33.ResumeLayout(false);
            this.groupBox33.PerformLayout();
            this._inputSignalsPage.ResumeLayout(false);
            this.groupBox18.ResumeLayout(false);
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this._outputSignalsPage.ResumeLayout(false);
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox31.ResumeLayout(false);
            this.groupBox31.PerformLayout();
            this.groupBox175.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._outputIndicatorsGrid)).EndInit();
            this.groupBox176.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._outputReleGrid)).EndInit();
            this._logicElement.ResumeLayout(false);
            this.groupBox55.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._virtualReleDataGrid)).EndInit();
            this.groupBox53.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._rsTriggersDataGrid)).EndInit();
            this._systemPage.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._oscChannelsGrid)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this._bgsGoose.ResumeLayout(false);
            this._bgsGoose.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            this._ethernetPage.ResumeLayout(false);
            this.groupBox51.ResumeLayout(false);
            this.groupBox51.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl _configurationTabControl;
        private System.Windows.Forms.TabPage _inputSignalsPage;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.ComboBox _indComboBox;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.ComboBox _grUst1ComboBox;
        private System.Windows.Forms.TabPage _setpointPage;
        private System.Windows.Forms.TabPage _systemPage;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.MaskedTextBox _oscWriteLength;
        private System.Windows.Forms.ComboBox _oscFix;
        private System.Windows.Forms.ComboBox _oscLength;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TabPage _automatPage;
        private System.Windows.Forms.GroupBox groupBox32;
        private System.Windows.Forms.ComboBox _switchKontCep;
        private System.Windows.Forms.MaskedTextBox _switchTUskor;
        private System.Windows.Forms.MaskedTextBox _switchImp;
        private System.Windows.Forms.ComboBox _switchBlock;
        private System.Windows.Forms.ComboBox _switchError;
        private System.Windows.Forms.ComboBox _switchOn;
        private System.Windows.Forms.ComboBox _switchOff;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.GroupBox groupBox33;
        private System.Windows.Forms.ComboBox _switchSDTU;
        private System.Windows.Forms.ComboBox _switchVnesh;
        private System.Windows.Forms.ComboBox _switchKey;
        private System.Windows.Forms.ComboBox _switchButtons;
        private System.Windows.Forms.ComboBox _switchVneshOff;
        private System.Windows.Forms.ComboBox _switchVneshOn;
        private System.Windows.Forms.ComboBox _switchKeyOff;
        private System.Windows.Forms.ComboBox _switchKeyOn;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button _writeConfigBut;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripProgressBar _progressBar;
        private System.Windows.Forms.Button _readConfigBut;
        private System.Windows.Forms.Button _saveConfigBut;
        private System.Windows.Forms.Button _loadConfigBut;
        private System.Windows.Forms.SaveFileDialog _saveConfigurationDlg;
        private System.Windows.Forms.OpenFileDialog _openConfigurationDlg;
        private System.Windows.Forms.ToolStripStatusLabel _statusLabel;
        private System.Windows.Forms.MaskedTextBox _oscSizeTextBox;
        private System.Windows.Forms.ToolTip _toolTip;
        private System.Windows.Forms.Button _resetSetpointsButton;
        private System.Windows.Forms.Button _saveToXmlButton;
        private System.Windows.Forms.SaveFileDialog _saveXmlDialog;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.ComboBox _grUst2ComboBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.ComboBox _setpointsComboBox;
        private System.Windows.Forms.TabPage _outputSignalsPage;
        private System.Windows.Forms.GroupBox groupBox31;
        private System.Windows.Forms.CheckBox _fault2CheckBox;
        private System.Windows.Forms.CheckBox _fault1CheckBox;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.MaskedTextBox _impTB;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.GroupBox groupBox175;
        private System.Windows.Forms.DataGridView _outputIndicatorsGrid;
        private System.Windows.Forms.GroupBox groupBox176;
        private System.Windows.Forms.DataGridView _outputReleGrid;
        private System.Windows.Forms.GroupBox groupBox24;
        private System.Windows.Forms.Button _applyCopySetpoinsButton;
        private System.Windows.Forms.ComboBox _copySetpoinsGroupComboBox;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ComboBox _controlSolenoidCombo;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.DataGridViewTextBoxColumn _outIndNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _outIndTypeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _outIndSignalCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _out1IndSignalCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _outIndSignal2Col;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.ComboBox oscStartCb;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.ComboBox _comandOtkl;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.CheckBox _resetAlarmCheckBox;
        private System.Windows.Forms.CheckBox _resetSystemCheckBox;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.TabControl _setpointsTab;
        private System.Windows.Forms.TabPage _logicSignalGr1;
        private System.Windows.Forms.GroupBox groupBox49;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage VLS1;
        private System.Windows.Forms.CheckedListBox VLSclb1Gr1;
        private System.Windows.Forms.TabPage VLS2;
        private System.Windows.Forms.CheckedListBox VLSclb2Gr1;
        private System.Windows.Forms.TabPage VLS3;
        private System.Windows.Forms.CheckedListBox VLSclb3Gr1;
        private System.Windows.Forms.TabPage VLS4;
        private System.Windows.Forms.CheckedListBox VLSclb4Gr1;
        private System.Windows.Forms.TabPage VLS5;
        private System.Windows.Forms.CheckedListBox VLSclb5Gr1;
        private System.Windows.Forms.TabPage VLS6;
        private System.Windows.Forms.CheckedListBox VLSclb6Gr1;
        private System.Windows.Forms.TabPage VLS7;
        private System.Windows.Forms.CheckedListBox VLSclb7Gr1;
        private System.Windows.Forms.TabPage VLS8;
        private System.Windows.Forms.CheckedListBox VLSclb8Gr1;
        private System.Windows.Forms.TabPage VLS9;
        private System.Windows.Forms.CheckedListBox VLSclb9Gr1;
        private System.Windows.Forms.TabPage VLS10;
        private System.Windows.Forms.CheckedListBox VLSclb10Gr1;
        private System.Windows.Forms.TabPage VLS11;
        private System.Windows.Forms.CheckedListBox VLSclb11Gr1;
        private System.Windows.Forms.TabPage VLS12;
        private System.Windows.Forms.CheckedListBox VLSclb12Gr1;
        private System.Windows.Forms.TabPage VLS13;
        private System.Windows.Forms.CheckedListBox VLSclb13Gr1;
        private System.Windows.Forms.TabPage VLS14;
        private System.Windows.Forms.CheckedListBox VLSclb14Gr1;
        private System.Windows.Forms.TabPage VLS15;
        private System.Windows.Forms.CheckedListBox VLSclb15Gr1;
        private System.Windows.Forms.TabPage VLS16;
        private System.Windows.Forms.CheckedListBox VLSclb16Gr1;
        private System.Windows.Forms.GroupBox groupBox26;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage31;
        private System.Windows.Forms.DataGridView _lsOrDgv1Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn15;
        private System.Windows.Forms.TabPage tabPage32;
        private System.Windows.Forms.DataGridView _lsOrDgv2Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn16;
        private System.Windows.Forms.TabPage tabPage33;
        private System.Windows.Forms.TabPage tabPage34;
        private System.Windows.Forms.DataGridView _lsOrDgv4Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn18;
        private System.Windows.Forms.TabPage tabPage35;
        private System.Windows.Forms.DataGridView _lsOrDgv5Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn19;
        private System.Windows.Forms.TabPage tabPage36;
        private System.Windows.Forms.DataGridView _lsOrDgv6Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn20;
        private System.Windows.Forms.TabPage tabPage37;
        private System.Windows.Forms.DataGridView _lsOrDgv7Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn21;
        private System.Windows.Forms.TabPage tabPage38;
        private System.Windows.Forms.DataGridView _lsOrDgv8Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn22;
        private System.Windows.Forms.GroupBox groupBox45;
        private System.Windows.Forms.TabPage _defExtGr1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _externalDifSbrosColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _externalDifAPVRetColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _externalDifAPVColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _externalDifUROVColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _externalDifOscColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _externalDifBlockingColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _externalDifVozvrYNColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _externalDifVozvrColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _externalDifTvzColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _externalDifTsrColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _externalDifSrabColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _externalDifModesColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _externalDifStageColumn;
        private System.Windows.Forms.DataGridView _externalDefensesGr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn4;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn5;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn4;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn5;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPage39;
        private System.Windows.Forms.TabPage tabPage40;
        private System.Windows.Forms.DataGridView _lsAndDgv2Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn24;
        private System.Windows.Forms.TabPage tabPage41;
        private System.Windows.Forms.TabPage tabPage42;
        private System.Windows.Forms.DataGridView _lsAndDgv4Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn27;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn26;
        private System.Windows.Forms.TabPage tabPage43;
        private System.Windows.Forms.DataGridView _lsAndDgv5Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn28;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn27;
        private System.Windows.Forms.TabPage tabPage44;
        private System.Windows.Forms.DataGridView _lsAndDgv6Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn29;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn28;
        private System.Windows.Forms.TabPage tabPage45;
        private System.Windows.Forms.DataGridView _lsAndDgv7Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn30;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn29;
        private System.Windows.Forms.TabPage tabPage46;
        private System.Windows.Forms.DataGridView _lsAndDgv8Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn31;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn30;
        private System.Windows.Forms.DataGridView _lsOrDgv3Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn17;
        private System.Windows.Forms.DataGridView _lsAndDgv1Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn23;
        private System.Windows.Forms.DataGridView _lsAndDgv3Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn26;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn25;
        private System.Windows.Forms.DataGridView _oscChannelsGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn6;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn7;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem readFromDeviceItem;
        private System.Windows.Forms.ToolStripMenuItem writeToDeviceItem;
        private System.Windows.Forms.ToolStripMenuItem readFromFileItem;
        private System.Windows.Forms.ToolStripMenuItem writeToFileItem;
        private System.Windows.Forms.ToolStripMenuItem writeToHtmlItem;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.TabPage _ethernetPage;
        private System.Windows.Forms.GroupBox groupBox51;
        private System.Windows.Forms.MaskedTextBox _ipLo1;
        private System.Windows.Forms.MaskedTextBox _ipLo2;
        private System.Windows.Forms.MaskedTextBox _ipHi1;
        private System.Windows.Forms.MaskedTextBox _ipHi2;
        private System.Windows.Forms.Label label214;
        private System.Windows.Forms.Label label213;
        private System.Windows.Forms.Label label212;
        private System.Windows.Forms.Label label179;
        private System.Windows.Forms.CheckBox _switchYesNo;
        private System.Windows.Forms.ComboBox _blockSDTU;
        private System.Windows.Forms.Label _blockSDTUlabel;
        private System.Windows.Forms.CheckBox _fault4CheckBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox _fault3CheckBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabPage _logicElement;
        private System.Windows.Forms.GroupBox groupBox55;
        private System.Windows.Forms.DataGridView _virtualReleDataGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn52;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn68;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn69;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn53;
        private System.Windows.Forms.GroupBox groupBox53;
        private System.Windows.Forms.DataGridView _rsTriggersDataGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn51;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn65;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn66;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn67;
        private System.Windows.Forms.TabPage _bgsGoose;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.ComboBox goin64;
        private System.Windows.Forms.ComboBox goin48;
        private System.Windows.Forms.ComboBox goin32;
        private System.Windows.Forms.Label label281;
        private System.Windows.Forms.Label label282;
        private System.Windows.Forms.Label label283;
        private System.Windows.Forms.ComboBox goin63;
        private System.Windows.Forms.ComboBox goin47;
        private System.Windows.Forms.ComboBox goin31;
        private System.Windows.Forms.Label label284;
        private System.Windows.Forms.Label label285;
        private System.Windows.Forms.Label label286;
        private System.Windows.Forms.ComboBox goin62;
        private System.Windows.Forms.ComboBox goin46;
        private System.Windows.Forms.ComboBox goin30;
        private System.Windows.Forms.Label label287;
        private System.Windows.Forms.Label label288;
        private System.Windows.Forms.Label label289;
        private System.Windows.Forms.ComboBox goin61;
        private System.Windows.Forms.ComboBox goin45;
        private System.Windows.Forms.ComboBox goin29;
        private System.Windows.Forms.Label label290;
        private System.Windows.Forms.Label label291;
        private System.Windows.Forms.Label label292;
        private System.Windows.Forms.ComboBox goin60;
        private System.Windows.Forms.ComboBox goin44;
        private System.Windows.Forms.ComboBox goin28;
        private System.Windows.Forms.Label label293;
        private System.Windows.Forms.Label label294;
        private System.Windows.Forms.Label label295;
        private System.Windows.Forms.ComboBox goin59;
        private System.Windows.Forms.ComboBox goin43;
        private System.Windows.Forms.ComboBox goin27;
        private System.Windows.Forms.Label label296;
        private System.Windows.Forms.Label label297;
        private System.Windows.Forms.Label label298;
        private System.Windows.Forms.ComboBox goin58;
        private System.Windows.Forms.ComboBox goin42;
        private System.Windows.Forms.ComboBox goin26;
        private System.Windows.Forms.Label label299;
        private System.Windows.Forms.Label label300;
        private System.Windows.Forms.Label label301;
        private System.Windows.Forms.ComboBox goin57;
        private System.Windows.Forms.ComboBox goin41;
        private System.Windows.Forms.ComboBox goin25;
        private System.Windows.Forms.Label label302;
        private System.Windows.Forms.Label label303;
        private System.Windows.Forms.Label label304;
        private System.Windows.Forms.ComboBox goin56;
        private System.Windows.Forms.ComboBox goin40;
        private System.Windows.Forms.ComboBox goin24;
        private System.Windows.Forms.Label label305;
        private System.Windows.Forms.Label label306;
        private System.Windows.Forms.Label label307;
        private System.Windows.Forms.ComboBox goin55;
        private System.Windows.Forms.ComboBox goin39;
        private System.Windows.Forms.ComboBox goin23;
        private System.Windows.Forms.Label label308;
        private System.Windows.Forms.Label label309;
        private System.Windows.Forms.Label label310;
        private System.Windows.Forms.ComboBox goin54;
        private System.Windows.Forms.ComboBox goin38;
        private System.Windows.Forms.ComboBox goin22;
        private System.Windows.Forms.Label label311;
        private System.Windows.Forms.Label label312;
        private System.Windows.Forms.Label label313;
        private System.Windows.Forms.ComboBox goin53;
        private System.Windows.Forms.ComboBox goin37;
        private System.Windows.Forms.ComboBox goin21;
        private System.Windows.Forms.Label label314;
        private System.Windows.Forms.Label label315;
        private System.Windows.Forms.Label label316;
        private System.Windows.Forms.ComboBox goin52;
        private System.Windows.Forms.ComboBox goin36;
        private System.Windows.Forms.ComboBox goin20;
        private System.Windows.Forms.Label label317;
        private System.Windows.Forms.Label label318;
        private System.Windows.Forms.Label label319;
        private System.Windows.Forms.ComboBox goin51;
        private System.Windows.Forms.ComboBox goin35;
        private System.Windows.Forms.ComboBox goin19;
        private System.Windows.Forms.Label label320;
        private System.Windows.Forms.Label label321;
        private System.Windows.Forms.Label label322;
        private System.Windows.Forms.ComboBox goin50;
        private System.Windows.Forms.ComboBox goin34;
        private System.Windows.Forms.ComboBox goin18;
        private System.Windows.Forms.Label label323;
        private System.Windows.Forms.Label label324;
        private System.Windows.Forms.Label label325;
        private System.Windows.Forms.ComboBox goin49;
        private System.Windows.Forms.Label label326;
        private System.Windows.Forms.ComboBox goin33;
        private System.Windows.Forms.Label label327;
        private System.Windows.Forms.ComboBox goin17;
        private System.Windows.Forms.Label label328;
        private System.Windows.Forms.ComboBox goin16;
        private System.Windows.Forms.Label label329;
        private System.Windows.Forms.ComboBox goin15;
        private System.Windows.Forms.Label label330;
        private System.Windows.Forms.ComboBox goin14;
        private System.Windows.Forms.Label label331;
        private System.Windows.Forms.ComboBox goin13;
        private System.Windows.Forms.Label label332;
        private System.Windows.Forms.ComboBox goin12;
        private System.Windows.Forms.Label label333;
        private System.Windows.Forms.ComboBox goin11;
        private System.Windows.Forms.Label label334;
        private System.Windows.Forms.ComboBox goin10;
        private System.Windows.Forms.Label label335;
        private System.Windows.Forms.ComboBox goin9;
        private System.Windows.Forms.Label label336;
        private System.Windows.Forms.ComboBox goin8;
        private System.Windows.Forms.Label label337;
        private System.Windows.Forms.ComboBox goin7;
        private System.Windows.Forms.Label label338;
        private System.Windows.Forms.ComboBox goin6;
        private System.Windows.Forms.Label label339;
        private System.Windows.Forms.ComboBox goin5;
        private System.Windows.Forms.Label label340;
        private System.Windows.Forms.ComboBox goin4;
        private System.Windows.Forms.Label label341;
        private System.Windows.Forms.ComboBox goin3;
        private System.Windows.Forms.Label label342;
        private System.Windows.Forms.ComboBox goin2;
        private System.Windows.Forms.Label label343;
        private System.Windows.Forms.ComboBox goin1;
        private System.Windows.Forms.Label label344;
        private System.Windows.Forms.Label label345;
        private System.Windows.Forms.ComboBox operationBGS;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.ComboBox currentBGS;
        private System.Windows.Forms.ToolStripMenuItem resetSetpointItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn _releNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _releTypeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _releSignalCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _releWaitCol;
    }
}