﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.MR761Obr.OBRNEW.Configuration.Structures
{
    public class ConfigAddStructNew : StructBase
    {
        [Layout(0)] private ushort _res;
        [Layout(1)] private ushort _general;
        
        /// <summary>
        /// Сброс индикаторов по входу в журнал системы
        /// </summary>
        [BindingProperty(0)]
        [XmlAttribute(AttributeName = "Сброс_по_входу_в_журнал_системы")]
        public bool ResetSystem
        {
            get { return Common.GetBit(this._general, 0); }
            set { this._general = Common.SetBit(this._general, 0, value); }
        }

        /// <summary>
        /// Сброс индикаторов по входу в журнал аварий
        /// </summary>
        [BindingProperty(1)]
        [XmlAttribute(AttributeName = "Сброс_по_входу_в_журнал_аварий")]
        public bool ResetAlarm
        {
            get { return Common.GetBit(this._general, 1); }
            set { this._general = Common.SetBit(this._general, 1, value); }
        }
    }
}
