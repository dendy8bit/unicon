﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR761Obr.OBRNEW.Configuration.Structures.External
{
    public class AllDefenseExternalStruct : StructBase, IDgvRowsContainer<DefenseExternalStruct>
    {
        [Layout(0, Count = 16)]
        private DefenseExternalStruct[] _external; 

        [XmlArray(ElementName = "Все_внешние")]
        public DefenseExternalStruct[] Rows
        {
            get { return this._external; }
            set { this._external = value; }
        }
    }
}
