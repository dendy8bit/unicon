﻿using System.Collections.Generic;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR761Obr.OBRNEW.Configuration.Structures.Goose
{
    public class Goose : StructBase
    {
        [BindingProperty(0)]
        [XmlElement(ElementName = "Конфигурация_гус")]
        public string GooseConfig { get; set; }
        [BindingProperty(1)]
        [XmlElement(ElementName = "Сигнал_гус")]
        public string[] GooseInputs { get; set; }

        public override T Clone<T>()
        {
            Goose g = new Goose
            {
                GooseConfig = this.GooseConfig,
                GooseInputs = new List<string>(this.GooseInputs).ToArray()
            };
            return (T)(StructBase)g;
        }
    }
}
