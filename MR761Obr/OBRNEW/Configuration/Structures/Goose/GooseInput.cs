﻿using System.Collections.Generic;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;
using BEMN.MR761Obr.OBRNEW.Configuration;

namespace BEMN.MR761Obr.OBRNEW.Configuration.Structures.Goose
{
    public class GooseInput : StructBase
    {
        public const int GOOSEINPUT_COUNT = 64;
        private const int INPUT_WORDS_COUNT = 8;
        [Layout(0, Count = INPUT_WORDS_COUNT)] ushort[] _goose;

        public string[] GoIn
        {
            get
            {
                bool[] array = Common.GetBitsArray(this._goose, 0, GOOSEINPUT_COUNT*2-1);
                List<string> goin = new List<string>();
                for (int i = 0; i < array.Length; i+=2)
                {
                    int val = (array[i] ? 1 : 0) + (array[i + 1] ? 2 : 0);
                    goin.Add(Validator.Get((ushort) val, StringsConfigNew.GooseSignal));
                }
                return goin.ToArray();
            }
            set
            {
                List<bool> bits = new List<bool>();
                foreach (string s in value)
                {
                    ushort val = Validator.Set(s, StringsConfigNew.GooseSignal);
                    bool bit1 = (val & 0x01) != 0;
                    bits.Add(bit1);
                    bool bit2 = (val & 0x02) != 0;
                    bits.Add(bit2);
                }
                this._goose = Common.BitsToUshorts(bits);
            }
        }
    }
}
