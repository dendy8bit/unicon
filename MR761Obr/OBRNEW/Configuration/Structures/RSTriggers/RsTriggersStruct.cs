﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR761Obr.OBRNEW.Configuration.Structures.RSTriggers
{
    public class RsTriggersStruct : StructBase
    {
        [Layout(0)] private ushort _set;
        [Layout(1)] private ushort _reset;
        [Layout(2)] private ushort _config;
        [Layout(3)] private ushort _rez;

        [BindingProperty(0)]
        [XmlAttribute(AttributeName = "Тип")]
        public string TypeRs
        {
            get { return Validator.Get(this._config, StringsConfigNew.RSPriority); }
            set { this._config = Validator.Set(value, StringsConfigNew.RSPriority); }
        }

        [BindingProperty(1)]
        [XmlAttribute(AttributeName = "Вход_R")]
        public string InputR
        {
            get { return Validator.Get(this._reset, StringsConfigNew.RelaySignals); }
            set { this._reset = Validator.Set(value, StringsConfigNew.RelaySignals); }
        }

        [BindingProperty(2)]
        [XmlAttribute(AttributeName = "Вход_S")]
        public string InputS
        {
            get { return Validator.Get(this._set, StringsConfigNew.RelaySignals); }
            set { this._set = Validator.Set(value, StringsConfigNew.RelaySignals); }
        }

    }
}
