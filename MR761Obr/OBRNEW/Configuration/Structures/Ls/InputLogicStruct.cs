﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR761Obr.OBRNEW.Configuration.Structures.Ls
{
    /// <summary>
    /// Конфигурация входного логического сигнала
    /// </summary>
    [XmlRoot(ElementName = "Конфигурация_одного_ЛС")]
    public class InputLogicStruct : StructBase, IXmlSerializable
    {
        private const int COUNT = 14;

        public static int DiscretsCount
        {
            get
            {
                switch (StringsConfigNew.DeviceType)
                {
                    case Mr761ObrDevice.T0N0D74R35:
                        return 72;
                    case Mr761ObrDevice.T0N0D106R67:
                        return 104;
                    case Mr761ObrDevice.T0N0D114R59:
                        return 112;
                    default:
                        return 72;
                }
            }
        }

        #region [Private fields]

        [Layout(0, Count = COUNT)] private ushort[] _a;

        #endregion [Private fields]

        [XmlIgnore]
        private ushort[] Mass
        {
            get { return this._a; }
            set { this._a = value; }
        }

        [BindingProperty(0)]
        [XmlIgnore]
        public string this[int ls]
        {
            get
            {
                ushort sourse = this.Mass[ls / 8];
                int pos = (ls * 2) % 16;
                return Validator.Get(sourse, StringsConfigNew.LsState, pos, pos + 1);
            }

            set
            {
                ushort sourse = this.Mass[ls / 8];
                int pos = (ls * 2) % 16;
                sourse = Validator.Set(value, StringsConfigNew.LsState, sourse, pos, pos + 1);
                ushort[] mass = this.Mass;
                mass[ls / 8] = sourse;
                this.Mass = mass;
            }
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {

        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {

            for (int i = 0; i < DiscretsCount; i++)
            {
                writer.WriteElementString("Дискрет", this[i]);
            }
        }
    }
}
