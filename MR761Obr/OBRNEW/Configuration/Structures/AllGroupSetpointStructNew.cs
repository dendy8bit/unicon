﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;

namespace BEMN.MR761Obr.OBRNEW.Configuration.Structures
{
    public class AllGroupSetpointStructNew : StructBase, ISetpointContainer<GroupSetpointStructNew>
    {
        private const int GROUP_COUNT = 2;

        [Layout(0, Count = GROUP_COUNT)] private GroupSetpointStructNew[] _groupSetpoints;

        public GroupSetpointStructNew[] Setpoints
        {
            get
            {
                var res = new GroupSetpointStructNew[GROUP_COUNT];
                for (int i = 0; i < GROUP_COUNT; i++)
                {
                    res[i] = this._groupSetpoints[i].Clone<GroupSetpointStructNew>();
                }
                return res;
            }
            set
            {
                for (int i = 0; i < GROUP_COUNT; i++)
                {
                    this._groupSetpoints[i] = value[i];
                }
            }
        }

        [XmlElement(ElementName = "Группа1")]
        public GroupSetpointStructNew Group1
        {
            get
            {
                return this._groupSetpoints[0].Clone<GroupSetpointStructNew>();
            }
            set { }
        }

        [XmlElement(ElementName = "Группа2")]
        public GroupSetpointStructNew Group2
        {
            get
            {
                return this._groupSetpoints[1].Clone<GroupSetpointStructNew>();
            }
            set { }
        }

    }
}
