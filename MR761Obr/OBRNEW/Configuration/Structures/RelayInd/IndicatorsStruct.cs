﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR761Obr.OBRNEW.Configuration.Structures.RelayInd
{
    /// <summary>
    /// параметры индикаторов
    /// </summary>
    [XmlType(TypeName = "Один_индикатор")]
    public class IndicatorsStruct : StructBase
    {
        #region [Private fields]

        [Layout(0)] private ushort _signal;
        [Layout(1)] private ushort _signal1;
        [Layout(2)] private ushort _type;
        [Layout(3)] private ushort _res;

        #endregion [Private fields]


        #region [Properties]

        /// <summary>
        /// Тип
        /// </summary>
        [BindingProperty(0)]
        [XmlAttribute(AttributeName = "Тип")]
        public string Type
        {
            get { return Validator.Get(this._type, StringsConfigNew.ReleyType, 0); }
            set { this._type = Validator.Set(value, StringsConfigNew.ReleyType, this._type, 0); }
        }

        /// <summary>
        /// Сигнал
        /// </summary>
        [BindingProperty(1)]
        [XmlAttribute(AttributeName = "Сигнал_зеленый")]
        public string SignalGreen
        {
            get { return Validator.Get(this._signal, StringsConfigNew.RelaySignals); }
            set { this._signal = Validator.Set(value, StringsConfigNew.RelaySignals); }
        }

        [BindingProperty(2)]
        [XmlAttribute(AttributeName = "Сигнал_красный")]
        public string SignalRed
        {
            get { return Validator.Get(this._signal1, StringsConfigNew.RelaySignals); }
            set { this._signal1 = Validator.Set(value, StringsConfigNew.RelaySignals); }
        }
        
        /// <summary>
        /// Режим работы
        /// </summary>
        [BindingProperty(3)]
        [XmlAttribute(AttributeName = "Режим_работы")]
        public string Mode
        {
            get { return Validator.Get(this._type, StringsConfigNew.Mode, 8, 9); }
            set { this._type = Validator.Set(value, StringsConfigNew.Mode, this._type, 8, 9); }
        }
        
        #endregion [Properties]
    }
}
