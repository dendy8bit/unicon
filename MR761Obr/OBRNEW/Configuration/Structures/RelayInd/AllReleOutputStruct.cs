using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR761Obr.OBRNEW.Configuration.Structures.RelayInd
{
    /// <summary>
    /// ��� ����
    /// </summary>
    public class AllReleOutputStruct : StructBase, IDgvRowsContainer<ReleOutputStruct>
    {
        public const int RELAY_COUNT_CONST = 80;

        /// <summary>
        /// ����
        /// </summary>
        [Layout(0, Count = RELAY_COUNT_CONST)] private ReleOutputStruct[] _relays;

        /// <summary>
        /// ����
        /// </summary>
        [XmlArray(ElementName = "���_����")]

        public ReleOutputStruct[] Rows
        {
            get { return this._relays; }
            set { this._relays = value; }
        }

        public static int RelaysCount
        {
            get
            {
                switch (StringsConfigNew.DeviceType)
                {
                    case Mr761ObrDevice.T0N0D74R35:
                        return 34;
                    case Mr761ObrDevice.T0N0D106R67:
                        return 66;
                    case Mr761ObrDevice.T0N0D114R59:
                        return 58;
                    default:
                        return 34;
                }

            }
        }
    }
}