﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR761Obr.OBRNEW.Configuration.Structures.InputSignals
{
    /// <summary>
    /// Конфигурациия входных сигналов
    /// </summary>
    [XmlRoot(ElementName = "Конфигурациия_входных_сигналов")]
    public class InputSignalStruct :StructBase
    {
        #region [Private fields]
        [Layout(0)] private ushort _groopUst1;  //вход аварийная группа уставок 1
        [Layout(1)] private ushort _groopUst2;  //вход аварийная группа уставок 2
        [Layout(2)] private ushort _groopUst3;  //вход аварийная группа уставок 3
        [Layout(3)] private ushort _groopUst4;  //вход аварийная группа уставок 4
        [Layout(4)] private ushort _groopUst5;  //вход аварийная группа уставок 5
        [Layout(5)] private ushort _groopUst6;  //вход аварийная группа уставок 6
        [Layout(6)] private ushort _clrInd;     //вход сброс индикации 
        [Layout(7)] private ushort _res;
        #endregion [Private fields]


        #region [Properties]
        /// <summary>
        /// вход аварийная группа уставок 1
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "вход_аварийная_группа_уставок_1")]
        public string Crash1Xml
        {
            get { return Validator.Get(this._groopUst1,StringsConfigNew.SwitchSignals) ; }
            set { this._groopUst1 = Validator.Set(value,StringsConfigNew.SwitchSignals) ; }
        }
        /// <summary>
        /// вход аварийная группа уставок
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "вход_аварийная_группа_уставок_2")]
        public string Crash2Xml
        {
            get { return Validator.Get(this._groopUst2, StringsConfigNew.SwitchSignals); }
            set { this._groopUst2 = Validator.Set(value, StringsConfigNew.SwitchSignals); }
        }
        
        /// <summary>
        /// вход сброс индикации
        /// </summary>
        [BindingProperty(6)]
        [XmlElement(ElementName = "вход_сброс_индикации")]
        public string ResetIndicationXml
        {
            get { return Validator.Get(this._clrInd,StringsConfigNew.SwitchSignals); }
            set { this._clrInd = Validator.Set(value, StringsConfigNew.SwitchSignals); }
        } 
        #endregion [Properties]   
    }
}
