﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR761Obr.OBRNEW.Configuration.Structures.Oscope
{
    [XmlType(TypeName = "Один_канал")]
    public class ChannelStructNew : StructBase
    {
        [Layout(0)] private ushort _channel;

        [BindingProperty(0)]
        [XmlAttribute(AttributeName = "Канал")]
        public string ChannelStr
        {
            get { return Validator.Get(this._channel, StringsConfigNew.RelaySignals); }
            set { this._channel = Validator.Set(value, StringsConfigNew.RelaySignals); }
        }
    }
}
