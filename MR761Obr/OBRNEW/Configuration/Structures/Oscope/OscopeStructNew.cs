﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR761Obr.OBRNEW.Configuration.Structures.Oscope
{
    /// <summary>
    /// Конфигурация осцилографа
    /// </summary>
    public class OscopeStructNew : StructBase
    {

        #region [Private fields]

        [Layout(0)] private OscopeConfigStructNew _oscopeConfig;
        [Layout(1)] private ChannelStructNew _test; //вход запуска осциллографа
        [Layout(2)] private OscopeAllChannelsStructNew _oscopeAllChannels;
        #endregion [Private fields]


        #region [Properties]
        /// <summary>
        /// Конфигурация_осц
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "Конфигурация_осц")]
        public OscopeConfigStructNew OscopeConfig
        {
            get { return this._oscopeConfig; }
            set { this._oscopeConfig = value; }
        }

        /// <summary>
        /// Конфигурация каналов
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "Конфигурация_каналов")]
        public OscopeAllChannelsStructNew OscopeAllChannels
        {
            get { return this._oscopeAllChannels; }
            set { this._oscopeAllChannels = value; }
        }

        /// <summary>
        /// вход запуска осциллографа
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "вход_запуска_осциллографа")]
        public ChannelStructNew StartOscChannel
        {
            get { return this._test; }
            set { this._test = value; }
        }
        
        #endregion [Properties]
    }
}
