﻿using System.Collections.Generic;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR761Obr.OBRNEW.Configuration.Structures.Oscope
{
    public class ChannelWithBaseNew : StructBase
    {
        [BindingProperty(0)]
        [XmlAttribute(AttributeName = "База")]
        public string BaseStr
        {
            get { return Validator.Get(this.Base, StringsConfigNew.OscBases); }
            set { this.Base = (byte)Validator.Set(value, StringsConfigNew.OscBases); }
        }

        [BindingProperty(1)]
        [XmlAttribute(AttributeName = "Канал")]
        public string ChannelStr
        {
            get
            {
                Dictionary<ushort, string> list = StringsConfigNew.OscChannelSignals[this.Base];
                return Validator.Get(this.Channel, list);
            }
            set
            {
                Dictionary<ushort, string> list = StringsConfigNew.OscChannelSignals[this.Base];
                this.Channel = Validator.Set(value, list);
            }
        }

        public byte Base { get; set; }

        public ushort Channel { get; set; }

        public static int ChannelCount
        {
            get
            {
                switch (StringsConfigNew.DeviceType)
                {
                    case Mr761ObrDevice.T0N0D74R35:
                        return 56;
                    case Mr761ObrDevice.T0N0D106R67:
                        return 24;
                    case Mr761ObrDevice.T0N0D114R59:
                        return 16;
                    default:
                        return 56;
                }

            }
        }
    }
}
