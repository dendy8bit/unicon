﻿using System;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MR761Obr.OBRNEW.Configuration.Structures.Goose;
using BEMN.MR761Obr.OBRNEW.Configuration.Structures.ConfigSystem;
using BEMN.MR761Obr.OBRNEW.Configuration.Structures.InputSignals;
using BEMN.MR761Obr.OBRNEW.Configuration.Structures.Oscope;
using BEMN.MR761Obr.OBRNEW.Configuration.Structures.RelayInd;
using BEMN.MR761Obr.OBRNEW.Configuration.Structures.Switch;

namespace BEMN.MR761Obr.OBRNEW.Configuration.Structures
{
    /// <summary>
    /// Вся конфигурация
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "МР761ОБР")]
    public class ConfigurationStructNew : StructBase
    {

        [XmlElement(ElementName = "Версия")]
        public string DeviceVersion { get; set; }

        [XmlElement(ElementName = "Номер_устройства")]
        public string DeviceNumber { get; set; }

        [XmlElement(ElementName = "Тип_устройства")]
        public string DeviceType => "МР761ОБР";

        [XmlElement(ElementName = "Размер_осциллограмы")]
        public string SizeOsc { get; set; }

        [XmlElement(ElementName = "Группа")]
        public int Group { get; set; }

        #region [Private fields]
        // конфигурация всех групп уставок
        [Layout(0)] private AllGroupSetpointStructNew _allGroupSetpoints;
        // конфигурациия выключателя
        [Layout(1)] private SwitchStruct _sw;
        // конфигурациия входных сигналов
        [Layout(2)] private InputSignalStruct _impsg;
        // конфигурациия осцилографа
        [Layout(3)] private OscopeStructNew _osc;
        // Параметры автоматики
        [Layout(4)] private AutomaticsParametersStruct _automatics;
        // конфигурациия RS485
        [Layout(5, Count = 4, Ignore = true)] private ushort[] _configRS484;
        // конфигурациия Ethernet
        [Layout(6)] private ConfigIPAddress _ipAddress;
        // конфигурация сети
        [Layout(7, Count = 8, Ignore = true)] private ushort[] _portAndMacAddress;
        [Layout(8, Count = 126, Ignore = true)] private ushort[] _res;
        // дополнительная конфигурация
        [Layout(9)] private ConfigAddStructNew _configAdd;
        [Layout(10)] private GooseConfig _gooseConfig;
        #endregion [Private fields]


        #region [Properties]

        /// <summary>
        /// Группы уставок
        /// </summary>
        [XmlElement(ElementName = "Конфигурация_всех_групп_уставок")]
        [BindingProperty(0)]
        public AllGroupSetpointStructNew AllGroupSetpointsNew
        {
            get { return this._allGroupSetpoints; }
            set { this._allGroupSetpoints = value; }
        }
        /// <summary>
        /// конфигурациия выключателя
        /// </summary>
        [XmlElement(ElementName = "Конфигурация_выключателя")]
        [BindingProperty(1)]
        public SwitchStruct Sw
        {
            get { return this._sw; }
            set { this._sw = value; }
        }
        /// <summary>
        /// конфигурациия входных сигналов
        /// </summary>
        [XmlElement(ElementName = "Конфигурация_входных_сигналов")]
        [BindingProperty(2)]
        public InputSignalStruct Impsg
        {
            get { return this._impsg; }
            set { this._impsg = value; }
        }
        /// <summary>
        /// конфигурациия осцилографа
        /// </summary>
        [XmlElement(ElementName = "Конфигурация_осцилографа")]
        [BindingProperty(3)]
        public OscopeStructNew Osc
        {
            get { return this._osc; }
            set { this._osc = value; }
        }
        /// <summary>
        /// Конфигурация реле и индикаторов
        /// </summary>
        [XmlElement(ElementName = "Конфигурация_реле,индикаторов,неисправностей")]
        [BindingProperty(4)]
        public AutomaticsParametersStruct Automatics
        {
            get { return this._automatics; }
            set { this._automatics = value; }
        }
        [XmlElement(ElementName = "IP")]
        [BindingProperty(5)]
        public ConfigIPAddress IP
        {
            get { return this._ipAddress; }
            set { this._ipAddress = value; }
        }
        /// <summary>
        /// Доп. конфигурация
        /// </summary>
        [XmlElement(ElementName = "Дополнительная_конфигурация")]
        [BindingProperty(6)]
        public ConfigAddStructNew AddConfig
        {
            get { return this._configAdd; }
            set { this._configAdd = value; }
        }

        /// <summary>
        /// Конфигурация гусов
        /// </summary>
        [XmlElement(ElementName = "Гусы")]
        [BindingProperty(7)]
        public GooseConfig GooseConfig
        {
            get { return this._gooseConfig; }
            set { this._gooseConfig = value; }
        }

        #endregion [Properties]
    }
}
