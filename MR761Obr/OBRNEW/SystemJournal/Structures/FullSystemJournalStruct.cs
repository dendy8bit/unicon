﻿using System.Linq;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR761Obr.OBRNEW.SystemJournal.Structures
{
    public class FullSystemJournalStruct1024 : StructBase
    {
        [Layout(0, Count = 113)] private SystemJournalStructNew[] _records;

        public SystemJournalStructNew[] Records
        {
            get { return this._records.Where(r => !r.IsEmpty).ToArray(); }
        }
    }

    public class FullSystemJournalStruct64 : StructBase
    {
        [Layout(0, Count = 7)] private SystemJournalStructNew[] _records;
        public SystemJournalStructNew[] Records
        {
            get { return this._records.Where(r => !r.IsEmpty).ToArray(); }
        }
    }
}
