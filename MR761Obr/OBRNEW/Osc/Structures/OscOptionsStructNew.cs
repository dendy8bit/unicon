﻿using System;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MR761Obr.OBRNEW.Configuration;

namespace BEMN.MR761Obr.OBRNEW.Osc.Structures
{
    /// <summary>
    /// Параматры осцилографа
    /// </summary>
    public class OscOptionsStructNew : StructBase
    {
        #region [Private fields]
        /// <summary>
        /// Общий размер осциллографа
        /// </summary>
        [Layout(0)] private int _loadedFullOscSize;
        /// <summary>
        /// Полный размер осцилографа в страницах
        /// </summary>
        [Layout(1)] private ushort _fullOscSizeInPages;
        /// <summary>
        /// Размер одного отсчёта в словах
        /// </summary>
        [Layout(2)] private ushort _sizeCounting;
        /// <summary>
        /// Длинна одной осциллограммы в отсчётах
        /// </summary>
        [Layout(3)] private ushort _oscLenght;
        /// <summary>
        /// Размер страницы в словах(const)
        /// </summary>
        [Layout(4)] private ushort _pageSize;
        [Layout(5)] private ushort _res1;
        [Layout(6)] private ushort _res2;
        [Layout(7)] private ushort _discretsCount;
        [Layout(8)] private ushort _channelsCount;

        #endregion [Private fields]


        #region [Properties]

        /// <summary>
        /// Полный размер осцилографа в страницах
        /// </summary>
        public ushort FullOscSizeInPages
        {
            get { return this._fullOscSizeInPages; }
        }

        /// <summary>
        /// Размер страницы в словах(const)
        /// </summary>
        public ushort PageSize
        {
            get { return 1024; }
        }

        /// <summary>
        /// Общий размер осциллографа
        /// </summary>
        public int LoadedFullOscSizeInWords
        {
            get { return this._loadedFullOscSize; }
            set { this._loadedFullOscSize = value; }
        }

        public ushort SizeCounting => this._sizeCounting;

        public int DiscretsCount => this._discretsCount;

        public int ChannelsCount
        {
            get
            {
                switch (StringsConfigNew.DeviceType)
                {
                    case Mr761ObrDevice.T0N0D74R35:
                        return 56;
                    case Mr761ObrDevice.T0N0D106R67:
                        return 24;
                    case Mr761ObrDevice.T0N0D114R59:
                        return 16;
                    default:
                        return 56;
                }

            }
        }
        #endregion

    }
}
