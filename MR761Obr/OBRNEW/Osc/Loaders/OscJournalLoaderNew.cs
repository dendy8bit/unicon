﻿using System;
using System.Collections.Generic;
using System.Threading;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.MR761Obr.OBRNEW.Osc.Structures;
using BEMN.MR761Obr.Osc.Structures;

namespace BEMN.MR761Obr.OBRNEW.Osc.Loaders
{
    /// <summary>
    /// Загружает журнал осцилограммы
    /// </summary>
    public class OscJournalLoaderNew
    {
        #region [Private fields]
        /// <summary>
        /// Записи журнала
        /// </summary>
        private readonly MemoryEntity<OscJournalStructNew> _oscJournal;
        /// <summary>
        /// Сброс журнала на нулевую запись
        /// </summary>
        private readonly MemoryEntity<OneWordStruct> _refreshOscJournal;
        /// <summary>
        /// Список структур "Запись журнала осциллографа"
        /// </summary>
        private readonly List<OscJournalStructNew> _oscRecords;
        /// <summary>
        /// Текущий номер записи журнала осциллографа
        /// </summary>
        private int _recordNumber;

        #endregion [Private fields]


        #region [Events]
        /// <summary>
        /// Успешно прочитана одна запись журнала осциллографа
        /// </summary>
        public event Action ReadRecordOk;
        /// <summary>
        /// Успешно прочитаны все записи
        /// </summary>
        public event Action AllJournalReadOk;
        /// <summary>
        /// Возникла ошибка при чтении журнала осциллографа
        /// </summary>
        public event Action ReadJournalFail;
        #endregion [Events]


        #region [Ctor's]
        /// <summary>
        /// Создаёт загрузчик Журнала осциллографа
        /// </summary>
        /// <param name="oscJournal">Объект записи журнала</param>
        /// <param name="refreshOscJournal">Объект сброса журнала</param>
        public OscJournalLoaderNew(MemoryEntity<OscJournalStructNew> oscJournal, MemoryEntity<OneWordStruct> refreshOscJournal)
        {
            this._oscRecords = new List<OscJournalStructNew>();
            //Записи журнала
            this._oscJournal = oscJournal;
            this._oscJournal.AllReadOk += HandlerHelper.CreateReadArrayHandler(this.ReadRecord);
            this._oscJournal.AllReadFail += HandlerHelper.CreateReadArrayHandler(this.FailReadOscJournal);
            //запись индекса ЖО
            this._refreshOscJournal = refreshOscJournal;
            this._refreshOscJournal.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this._oscJournal.LoadStruct);
            this._refreshOscJournal.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this.FailReadOscJournal);
        } 
        #endregion [Ctor's]


        #region [Properties]
        /// <summary>
        /// Количество записей в журнале осциллографа
        /// </summary>
        public int RecordNumber
        {
            get { return this._oscRecords.Count; }
        }
        /// <summary>
        /// Список структур "Запись журнала осциллографа"
        /// </summary>
        public List<OscJournalStructNew> OscRecords
        {
            get { return this._oscRecords; }
        }

        public object[] GetRecord
        {
            get { return this._oscJournal.Value.GetRecord; }
        }
        #endregion [Properties]


        #region [Private MemoryEntity Events Handlers]
        /// <summary>
        /// Невозможно прочитать журнал
        /// </summary>
        private void FailReadOscJournal()
        {
            if (this.ReadJournalFail != null)
                this.ReadJournalFail.Invoke();
        }
        /// <summary>
        /// Прочитана одна запись журнала
        /// </summary>
        private void ReadRecord()
        {
            if (!this._oscJournal.Value.IsEmpty)
            {
                OscJournalStructNew.RecordIndex = this.RecordNumber;
                this.OscRecords.Add(this._oscJournal.Value.Clone<OscJournalStructNew>());
                this._recordNumber++;
                this.SaveIdex();
                if (this.ReadRecordOk == null) return;
                this.ReadRecordOk.Invoke();
            }
            else
            {
                if (this.AllJournalReadOk == null) return;
                this.AllJournalReadOk.Invoke();
            }
        }

        #endregion [Private MemoryEntity Events Handlers]


        #region [Public members]
        /// <summary>
        /// Запуск чтения журнала осциллографа
        /// </summary>
        public void StartReadJournal()
        {
            this._recordNumber = 0;
            this.SaveIdex();
        }

        public void ClearEvents()
        {
            this._oscJournal.AllReadOk -= HandlerHelper.CreateReadArrayHandler(this.ReadRecord);
            this._oscJournal.AllReadFail -= HandlerHelper.CreateReadArrayHandler(this.FailReadOscJournal);
            
            this._refreshOscJournal.AllWriteOk -= HandlerHelper.CreateReadArrayHandler(this._oscJournal.LoadStruct);
            this._refreshOscJournal.AllWriteFail -= HandlerHelper.CreateReadArrayHandler(this.FailReadOscJournal);
        }
        #endregion [Public members]

        internal void Reset()
        {
            this._oscRecords.Clear();
        }

        private void SaveIdex()
        {
            OneWordStruct ind = this._refreshOscJournal.Value;
            ind.Word = (ushort)this._recordNumber;
            this._refreshOscJournal.Value = ind;
            this._refreshOscJournal.SaveStruct6();
        }
    }
}
