using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using BEMN.MBServer;
using BEMN.MR761Obr.OBRNEW.Configuration;
using BEMN.MR761Obr.OBRNEW.Configuration.Structures.Oscope;
using BEMN.MR761Obr.OBRNEW.Osc.Structures;
using BEMN.MR761Obr.Osc.Structures;

namespace BEMN.MR761Obr.OBRNEW.Osc.HelpClasses
{
    /// <summary>
    /// ��������� �������� ������������
    /// </summary>
    public class CountingListNew
    {
        #region [Constants]
        /// <summary>
        /// ������ ������ �������(� ������)
        /// </summary>
        private int _countingSize;
        /// <summary>
        /// ���-�� �������
        /// </summary>
        private int _discretsCount;
        /// <summary>
        /// ���-�� �������
        /// </summary>
        private int _channelsCount;

        ///// <summary>
        ///// ������ ������ �������(� ������)
        ///// </summary>
        //public const int COUNTING_SIZE = 16;
        ///// <summary>
        ///// ���-�� �������
        ///// </summary>
        //public const int DISCRETS_COUNT = 112;
        ///// <summary>
        ///// ���-�� �������
        ///// </summary>
        //public const int CHANNELS_COUNT = 56;

        #endregion [Constants]


        #region [Private fields]
        private readonly ushort[][] _countingArray;
        private ChannelWithBaseNew[] _channelsWithBase;
        private string _hdrString;
        private int _count;
        private int _alarm;
        private OscJournalStructNew _oscJournalStruct;


        #endregion [Private fields]

        /// <summary>
        /// ������
        /// </summary>
        public ushort[][] _channels;

        /// <summary>
        /// ��������
        /// </summary>
        public ushort[][] _discrets;

        /// <summary>
        /// ����� ���������� ��������
        /// </summary>
        public int Count
        {
            get { return this._count; }
        }
        /// <summary>
        /// ������("���� �����������") � ��������
        /// </summary>
        public int Alarm
        {
            get { return this._alarm; }
        }
        public OscJournalStructNew OscJournalStruct
        {
            get { return this._oscJournalStruct; }
        }

        public string HdrString
        {
            get { return this._hdrString; }
        }

        private Mr761ObrDevice _device;

        #region [Ctor's]
        public CountingListNew(Mr761ObrDevice device, ushort[] pageValue, OscJournalStructNew oscJournalStruct)
        {
            this._device = device;

            StringsConfigNew.DeviceType = !_device.IsConnect ? this._device.Info.Plant : this._device.Info.DeviceConfiguration;

            this._channelsWithBase = this._device.AllChannelsNew.Value.Rows;
            this._oscJournalStruct = oscJournalStruct;
            this._alarm = this._oscJournalStruct.Len - this._oscJournalStruct.After;

            this.GetCountingSettings(this._device.OscOptionsNew.Value);

            this._countingArray = new ushort[this._countingSize][];
            //����� ���������� ��������
            this._count = pageValue.Length / this._countingSize;
            //������������� �������
            for (int i = 0; i < this._countingSize; i++)
            {
                this._countingArray[i] = new ushort[this._count];
            }
            int m = 0;
            int n = 0;
            foreach (ushort value in pageValue)
            {
                this._countingArray[n][m] = value;
                n++;
                if (n != this._countingSize) continue;
                m++;
                n = 0;
            }

            //��������
            this._discrets = this.SetDiscrets(this._discretsCount, out int increment, out bool b);//dicrets.ToArray();

            //������
            this._channels = this.SetChannels(this._channelsCount, increment, b);//channels.ToArray();

            this._hdrString = $"��761Obr v{this._device.DeviceVersion} {this._device.DevicePlant} {oscJournalStruct.GetDate} {oscJournalStruct.GetTime} ������� - {oscJournalStruct.Stage}";
        }

        private ushort[][] SetDiscrets(int discrCount, out int increment, out bool b)
        {
            List<ushort[]> discrets = new List<ushort[]>();
            increment = 0;
            b = false;
            int discrWords = discrCount / 16;
            if (discrWords % 16 != 0)
            {
                b = true;
                discrWords++;
            }
            for (int i = 0; i < discrWords; i++)
            {
                if (b && i == discrWords - 1)
                {
                    ushort[][] d = this.DiscretArrayInit(this._countingArray[increment]);
                    discrets.AddRange(d.Take(8));
                }
                else
                {
                    discrets.AddRange(this.DiscretArrayInit(this._countingArray[increment++]));
                }
            }
            return discrets.ToArray();
        }

        private ushort[][] SetChannels(int channelsCount, int increment, bool b)
        {
            List<ushort[]> channels = new List<ushort[]>();
            int channelsWords = channelsCount / 16;
            bool bb;
            if (b)
            {
                bb = (channelsCount - 8) % 16 != 0;
                channelsWords++;
            }
            else
            {
                bb = channelsCount % 16 != 0;
            }
            if (bb)
            {
                channelsWords++;
            }
            for (int i = 0; i < channelsWords; i++)
            {
                if (b && i == 0)
                {
                    ushort[][] d = this.DiscretArrayInit(this._countingArray[increment++]);
                    channels.AddRange(d.Skip(8));
                }
                else if (bb && i < channelsWords - 1)
                {
                    ushort[][] d = this.DiscretArrayInit(this._countingArray[increment]);
                    channels.AddRange(d.Take(8));
                }
                else
                {
                    channels.AddRange(this.DiscretArrayInit(this._countingArray[increment++]));
                }
            }

            return channels.ToArray();
        }

        private void GetCountingSettings(OscOptionsStructNew settings)
        {
            this._countingSize = settings.SizeCounting;
            this._discretsCount = settings.DiscretsCount;
            this._channelsCount = settings.ChannelsCount;
        }
        
        private CountingListNew(int count)
        {
            this._discrets = new ushort[this._discretsCount][];
            this._channels = new ushort[this._channelsCount][];

            this._count = count;
        }
        #endregion [Ctor's]

        /// <summary>
        /// ���������� ������ ����� � ��������������� ������ ���(�������� 0/1) 
        /// </summary>
        /// <param name="sourseArray">������ �������� ���</param>
        /// <returns></returns>
        private ushort[][] DiscretArrayInit(ushort[] sourseArray)
        {
            ushort[][] result = new ushort[16][];
            for (int i = 0; i < 16; i++)
            {
                result[i] = new ushort[sourseArray.Length];
            }

            for (int i = 0; i < sourseArray.Length; i++)
            {
                for (int j = 0; j < 16; j++)
                {
                    result[j][i] = (ushort)(Common.GetBit(sourseArray[i], j) ? 1 : 0);
                }
            }

            return result;
        }
        
        public void Save(string filePath)
        {
            string hdrPath = Path.ChangeExtension(filePath, "hdr");
            using (StreamWriter hdrFile = new StreamWriter(hdrPath))
            {
                hdrFile.WriteLine(this._hdrString);
                for (int i = 0; i < this._channelsWithBase.Length; i++)
                {
                    hdrFile.WriteLine("K{0} = {1}, {2}", i + 1, this._channelsWithBase[i].Channel, this._channelsWithBase[i].Base);
                }
                hdrFile.WriteLine(1251);
            }

            string cgfPath = Path.ChangeExtension(filePath, "cfg");
            using (StreamWriter cgfFile = new StreamWriter(cgfPath, false, Encoding.GetEncoding(1251)))
            {
                cgfFile.WriteLine("MP761OBR,1,1991");
                cgfFile.WriteLine($"{ this._discretsCount + this._channelsCount},0�,{this._discretsCount + this._channelsCount}D");
                int index = 1;
                
                for (int i = 0; i < this._discrets.Length; i++)
                {
                    cgfFile.WriteLine("{0},D{1},0", index, i + 1);
                    index++;
                }
                for (int i = 0; i < this._channels.Length; i++)
                {
                    try
                    {
                        cgfFile.WriteLine("{0},K{1} ({2}),0", index, i + 1, this._channelsWithBase[i].ChannelStr);
                        index++;
                    }
                    catch (Exception)
                    {
                        cgfFile.WriteLine("{0},K{1} ({2}),0", index, i + 1, "Error");
                        index++;
                    }
                }

                cgfFile.WriteLine("50");
                cgfFile.WriteLine("1");
                cgfFile.WriteLine("1000,{0}", this._oscJournalStruct.Len);

                cgfFile.WriteLine(this._oscJournalStruct.GetFormattedDateTimeAlarm(this._alarm));
                cgfFile.WriteLine(this._oscJournalStruct.GetFormattedDateTime);
                cgfFile.WriteLine("ASCII");
            }

            string datPath = Path.ChangeExtension(filePath, "dat");
            using (StreamWriter datFile = new StreamWriter(datPath))
            {
                for (int i = 0; i < this._count; i++)
                {
                    datFile.Write("{0:D6},{1:D6}", i, i * 1000);
                    foreach (ushort[] discret in this._discrets)
                    {
                        datFile.Write(",{0}", discret[i]);
                    }
                    foreach (ushort[] chanel in this._channels)
                    {
                        datFile.Write(",{0}", chanel[i]);
                    }
                    datFile.WriteLine();
                }
            }
        }

        public  bool IsLoad { get; private set; }
        public string FilePath { get; private set; }
        public CountingListNew Load(string filePath, Mr761ObrDevice device)
        {
            this._device = device;

            string hdrPath = Path.ChangeExtension(filePath, "hdr");
            string[] hdrStrings = File.ReadAllLines(hdrPath);
            string hdrString = hdrStrings[0];
            string cgfPath = Path.ChangeExtension(filePath, "cfg");
            string[] cfgStrings = File.ReadAllLines(cgfPath);

            ChannelWithBaseNew[] channelWithBase = new ChannelWithBaseNew[this._channelsCount];
            for (int i = 0; i < this._channelsCount; i++)
            {
                string[] channelAndBase =
                    hdrStrings[1 + i].Split(new[] { '=', ' ' }, StringSplitOptions.RemoveEmptyEntries)[1]
                        .Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                channelWithBase[i] = new ChannelWithBaseNew
                {
                    Channel = Convert.ToUInt16(channelAndBase[0]),
                    Base = channelAndBase.Length > 1 ? Convert.ToByte(channelAndBase[1]) : (byte)0
                };
            }
           
            if (this._discretsCount == 0 && this._channelsCount == 0)
            {
                switch (_device.Info.DeviceConfiguration)
                {
                    case "T0N0D74R35":
                        this._channelsCount = 72;
                        this._discretsCount = 56;
                        break;
                    case "T0N0D106R67":
                        this._channelsCount = 104;
                        this._discretsCount = 24;
                        break;
                    case "T0N0D114R59":
                        this._channelsCount = 112;
                        this._discretsCount = 16;
                        break;
                }
            }

            int indexCounts = 2 + this._discretsCount + this._channelsCount + 2;
            int counts = int.Parse(cfgStrings[indexCounts].Replace("1000,", string.Empty));
            int alarm = 0;
            try
            {
                string startTime = cfgStrings[indexCounts + 1].Split(',')[1];
                string runTime = cfgStrings[indexCounts + 2].Split(',')[1];

                TimeSpan a = DateTime.Parse(runTime) - DateTime.Parse(startTime);
                alarm = (int)a.TotalMilliseconds;
            }
            catch (Exception)
            {
                // ignored
            }
            CountingListNew result = new CountingListNew(counts) { _alarm = alarm };
            string datPath = Path.ChangeExtension(filePath, "dat");
            string[] datStrings = File.ReadAllLines(datPath);
        
            ushort[][] discrets = new ushort[this._discretsCount][];
            ushort[][] channels = new ushort[this._channelsCount][];
            

            for (int i = 0; i < discrets.Length; i++)
            {
                discrets[i] = new ushort[counts];
            }

            for (int i = 0; i < channels.Length; i++)
            {
                channels[i] = new ushort[counts];
            }

            for (int i = 0; i < datStrings.Length; i++)
            {
                string[] means = datStrings[i].Split(',');
                

                for (int j = 0; j < this._discretsCount; j++)
                {
                    discrets[j][i] = Convert.ToUInt16(means[j + 2]);
                }

                for (int j = 0; j < this._channelsCount; j++)
                {
                    channels[j][i] = Convert.ToUInt16(means[j + 2 + this._discretsCount]);
                }
            }
            result._channels = channels;
            result._discrets = discrets;
            result._channelsWithBase = channelWithBase;
            result._hdrString = hdrString;
            result.IsLoad = true;
            result.FilePath = filePath;
            return result;
        }
    }
}
