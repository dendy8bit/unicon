﻿using System.Collections.Generic;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.MR761OBR.OBRNEW.Measuring.Structures
{
    public class DiscretDataBaseStructNew : StructBase
    {
        #region [Constants]
        private const int BASE_SIZE = 32;
        private const int ALARM_SIZE = 16;
        private const int PARAM_SIZE = 16;
        #endregion [Constants]
        
        #region [Private fields]
        [Layout(0, Count = BASE_SIZE)] private ushort[] _base;      //бд общаяя
        [Layout(1, Count = ALARM_SIZE)] private ushort[] _alarm;    //БД неисправностей
        [Layout(2, Count = PARAM_SIZE)] private ushort[] _param;    //БД параметров
        #endregion [Private fields]

        #region [Properties]
        /// <summary>
        /// Дискретные входы
        /// </summary>
        public bool[] DiscretInputs
        {
            get
            {
                List<bool> retDiscr = new List<bool>();
                retDiscr.AddRange(Common.GetBitsArray(this._base, 0, 111));
                retDiscr.AddRange(Common.GetBitsArray(this._base, 262, 263));
                return retDiscr.ToArray();
            }
        }

        public bool[] State => Common.GetBitsArray(this._base, 419, 420);

        /// <summary>
        /// Входные ЛС
        /// </summary>
        public bool[] InputsLogicSignals => Common.GetBitsArray(this._base, 160, 175);

        public bool[] Bgs => Common.GetBitsArray(this._base, 176, 191);

        /// <summary>
        /// Выходные ЛС
        /// </summary>
        public bool[] OutputLogicSignals => Common.GetBitsArray(this._base, 192, 207);

        /// <summary>
        /// Свободная логика
        /// </summary>
        public bool[] FreeLogic => Common.GetBitsArray(this._base, 208, 255);

        /// <summary>
        /// Состояния и АПВ
        /// </summary>
        public bool[] StateAndApv => Common.GetBitsArray(this._base, 254, 261);
        
        /// <summary>
        /// Повр. фаз и качание
        /// </summary>
        public bool[] PhaseAndSw => Common.GetBitsArray(this._base, 315, 320);

        /// <summary>
        /// Внешние защиты
        /// </summary>
        public bool[] ExternalDefenses => Common.GetBitsArray(this._base, 272, 287);

        /// <summary>
        /// Реле
        /// </summary>
        public bool[] Relays => Common.GetBitsArray(this._base, 288, 382);

        /// <summary>
        /// RS-Тригеры
        /// </summary>
        public bool[] RSTriggers => Common.GetBitsArray(this._base, 144, 159);

        /// <summary>
        /// Индикаторы
        /// </summary>
        public List<bool[]> Indicators
        {
            get
            {
                bool[] allIndArray = Common.GetBitsArray(this._base, 384, 407); // все подряд биты
                List<bool[]> retList = new List<bool[]>();
                for (int i = 0; i < allIndArray.Length; i = i + 2)    // выделяем попарно состояния одного диода
                {
                    retList.Add(new[] { allIndArray[i], allIndArray[i + 1] });// (bool зеленый, bool красный)
                }
                return retList;
            }
        }
        /// <summary>
        /// Контроль
        /// </summary>
        public bool[] ControlSignals => Common.GetBitsArray(this._base, 410, 418);

        /// <summary>
        /// Неисправности общие
        /// </summary>
        public bool[] Faults1 => Common.GetBitsArray(this._alarm, 0, 17);

        /// <summary>
        /// Неисправности выключателя
        /// </summary>
        public bool[] Faults2
        {
            get { return Common.GetBitsArray(this._alarm, 18, 25); }
        }

        /// <summary>
        /// Направления токов
        /// </summary>
        public virtual string[] CurrentsSymbols
        {
            get
            {
                bool[] booleanArray = Common.GetBitsArray(this._param, 0, 65);
                string[] result = new string[33];
                for (int i = 0; i < 66; i += 2)
                {
                    result[i / 2] = this.GetCurrentSymbol(i, booleanArray[i], booleanArray[i + 1]);
                }
                return result;
            }
        }

        private string GetCurrentSymbol(int i, bool symbol, bool error)
        {
            string res = "";
            //if (i >= 33) res = "нет";
            return error ? res : (symbol ? "-" : "+");
        }
        List<bool> _logicList = new List<bool>();

        /// <summary>
        /// Неисправность логики
        /// </summary>
        public bool[] FaultLogicErr
        {
            get
            {
                this._logicList.Clear();
                this._logicList.AddRange(Common.GetBitsArray(this._alarm, 26, 31));
                this._logicList.RemoveAt(3);
                return this._logicList.ToArray();
            }
        }
        #endregion [Properties]
    }
}
