﻿using System;
using System.Drawing;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms;
using BEMN.Interfaces;
using BEMN.MR761Obr.Measuring.Structures;
using BEMN.MR761Obr.OBRNEW.Configuration;
using BEMN.MR761OBR.OBRNEW.Measuring.Structures;

namespace BEMN.MR761Obr.OBRNEW.Measuring
{
    public partial class Mr761ObrMeasuringFormNew : Form, IFormView
    {
        #region Const

        private const string RESET_SJ = "Сбросить новую запись в журнале системы";
        private const string RESET_AJ = "Сбросить новую запись в журнале аварий";
        private const string RESET_OSC = "Сбросить новую запись журнала осциллографа";
        private const string RESET_FAULT_SJ = "Сбросить наличие неисправности по ЖС";
        private const string RESET_INDICATION = "Сбросить блинкеры";
        private const string CURRENT_GROUP = "Группа №{0}";
        private const string SWITCH_GROUP_USTAVKI = "Переключить на группу уставок №{0}?";
        private const string START_OSC = "Запустить осциллограф";
        private const string SWITCH_OFF = "Отключить выключатель";
        private const string SWITCH_ON = "Включить выключатель";
        #endregion

        #region [Private fields]

        private Mr761ObrDevice _device;
        private readonly MemoryEntity<DiscretDataBaseStructNew> _discretDataBase;
        private readonly MemoryEntity<OneWordStruct> _groupUstavki;
        private readonly MemoryEntity<DateTimeStruct> _dateTime;
        private ushort? _numGroup;
        private int ind;

        /// <summary>
        /// Дискретные входы
        /// </summary>
        private LedControl[] _discretInputs;

        /// <summary>
        /// Входные ЛС
        /// </summary>
        private LedControl[] _inputsLogicSignals;

        /// <summary>
        /// Выходные ЛС
        /// </summary>
        private LedControl[] _outputLogicSignals;

        /// <summary>
        /// Внешние защиты
        /// </summary>
        private LedControl[] _externalDefenses;

        /// <summary>
        /// Свободная логика
        /// </summary>
        private LedControl[] _freeLogic;

        /// <summary>
        /// Состояния
        /// </summary>
        private LedControl[] _stateAndApv;

        /// <summary>
        /// Реле
        /// </summary>
        private LedControl[] _relays;

        /// <summary>
        /// Индикаторы
        /// </summary>
        private Diod[] _indicators;

        /// <summary>
        /// Индикаторы
        /// </summary>
        private LedControl[] _controlSignals;

        /// <summary>
        /// Неисправности
        /// </summary>
        private LedControl[] _faultsMain;

        /// <summary>
        /// Неисправности выключателя
        /// </summary>
        private LedControl[] _faultsSwitch;

        /// <summary>
        /// Энергонезависимые RS-Тригеры
        /// </summary>
        private LedControl[] _rsTriggers;

        /// <summary>
        /// Ошибки СПЛ
        /// </summary>
        private LedControl[] _splErr;

        private LedControl[] _state;

        #endregion [Private fields]

        #region Constructor

        public Mr761ObrMeasuringFormNew()
        {
            this.InitializeComponent();
        }

        public Mr761ObrMeasuringFormNew(Mr761ObrDevice device)
        {
            this.InitializeComponent();
            this._device = device;
            this._device.ConnectionModeChanged += this.StartStopLoad;

            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._device.Info.DeviceConfiguration = this._device.DevicePlant;
            }

            this._dateTime = device.DateTime;
            this._dateTime.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.DateTimeLoad);
            
            this._discretDataBase = device.DiscretNew;
            this._discretDataBase.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.DiscretBdReadOk);
            this._discretDataBase.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.DiscretBdReadFail);
            
            this._groupUstavki = device.GroupUstavkiNew;
            this._groupUstavki.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.GroupUstavkiLoaded);
            this._groupUstavki.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
                MessageBox.Show("Группа уставок успешно изменена", "Запись группы уставок", MessageBoxButtons.OK,
                    MessageBoxIcon.Information));
            this._groupUstavki.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                MessageBox.Show("Невозможно изменить группу уставок", "Запись группы уставок", MessageBoxButtons.OK,
                    MessageBoxIcon.Error));

            this.Init();
        }

        private void Init()
        {
            this._commandComboBox.SelectedIndex = 0;
            this._freeLogic = new[]
            {
                this._ssl1, this._ssl2, this._ssl3, this._ssl4, this._ssl5, this._ssl6, this._ssl7, this._ssl8,
                this._ssl9, this._ssl10, this._ssl11, this._ssl12, this._ssl13, this._ssl14, this._ssl15, this._ssl16,
                this._ssl17, this._ssl18, this._ssl19, this._ssl20, this._ssl21, this._ssl22, this._ssl23, this._ssl24,
                this._ssl25, this._ssl26, this._ssl27, this._ssl28, this._ssl29, this._ssl30, this._ssl31, this._ssl32,
                this._ssl33, this._ssl34, this._ssl35, this._ssl36, this._ssl37, this._ssl38, this._ssl39, this._ssl40,
                this._ssl41, this._ssl42, this._ssl43, this._ssl44, this._ssl45, this._ssl46, this._ssl47, this._ssl48
            };


            this._stateAndApv = new[]
            {
                this._fault, this._acceleration, this._signaling, this._faultOff
            };

            this._indicators = new[]
            {
                this.diod1, this.diod2, this.diod3, this.diod4, this.diod5, this.diod6, this.diod7, this.diod8,
                this.diod9, this.diod10, this.diod11, this.diod12
            };

            this._rsTriggers = new[]
            {
                this._rst1, this._rst2, this._rst3, this._rst4, this._rst5, this._rst6, this._rst7, this._rst8,
                this._rst9, this._rst10, this._rst11, this._rst12, this._rst13, this._rst14, this._rst15, this._rst16
            };

            this._relays = new[]
            {
                this._module1, this._module2, this._module3, this._module4, this._module5, this._module6,
                this._module7, this._module8, this._module9, this._module10, this._module11, this._module12,
                this._module13, this._module14, this._module15, this._module16, this._module17, this._module18,
                this._module19, this._module20, this._module21, this._module22, this._module23, this._module24,
                this._module25, this._module26, this._module27, this._module28, this._module29, this._module30,
                this._module31, this._module32, this._module33, this._module34, this._module35, this._module36,
                this._module37, this._module38, this._module39, this._module40, this._module41, this._module42,
                this._module43, this._module44, this._module45, this._module46, this._module47, this._module48,
                this._module49, this._module50, this._module51, this._module52, this._module53, this._module54,
                this._module55, this._module56, this._module57, this._module58, this._module59, this._module60,
                this._module61, this._module62, this._module63, this._module64, this._module65, this._module66,
                this._module67, this._module68, this._module69, this._module70, this._module71, this._module72,
                this._module73, this._module74, this._module75, this._module76, this._module77, this._module78,
                this._module79, this._module80
            };

            this._state = new[]
            {
                this._mainGroup, this._reservedGroup
            };

            this._externalDefenses = new[]
            {
                this._vz1, this._vz2, this._vz3, this._vz4, this._vz5, this._vz6, this._vz7, this._vz8,
                this._vz9, this._vz10, this._vz11, this._vz12, this._vz13, this._vz14, this._vz15, this._vz16
            };

            this._outputLogicSignals = new[]
            {
                this._vls1, this._vls2, this._vls3, this._vls4, this._vls5, this._vls6, this._vls7, this._vls8,
                this._vls9, this._vls10, this._vls11, this._vls12, this._vls13, this._vls14, this._vls15, this._vls16
            };
            this._inputsLogicSignals = new[]
            {
                this._ls1, this._ls2, this._ls3, this._ls4, this._ls5, this._ls6, this._ls7, this._ls8,
                this._ls9, this._ls10, this._ls11, this._ls12, this._ls13, this._ls14, this._ls15, this._ls16
            };

            this._controlSignals = new[]
            {
                this._newRecordSystemJournal, this._newRecordAlarmJournal, this._newRecordOscJournal,
                this._availabilityFaultSystemJournal, new LedControl(), this._switchOff, this._switchOn
            };
            this._faultsMain = new[]
            {
                this._faultHardware, this._faultSoftware, new LedControl(), new LedControl(), this._faultSwitchOff, this._faultLogic,
                this._faultModule1, this._faultModule2,this._faultModule3, this._faultModule4, this._faultModule5, this._faultModule6,
                this._faultSetpoints, this._faultGroupsOfSetpoints, this._faultPass, this._faultSystemJournal, this._faultAlarmJournal, this._faultOsc
            };
            this._discretInputs = new[]
            {
                this._d1, this._d2, this._d3, this._d4, this._d5, this._d6, this._d7, this._d8, this._d9, this._d10,
                this._d11, this._d12, this._d13, this._d14, this._d15, this._d16, this._d17, this._d18, this._d19,
                this._d20, this._d21, this._d22, this._d23, this._d24, this._d25, this._d26, this._d27, this._d28,
                this._d29, this._d30, this._d31, this._d32, this._d33, this._d34, this._d35, this._d36, this._d37,
                this._d38, this._d39, this._d40, this._d41, this._d42, this._d43, this._d44, this._d45, this._d46,
                this._d47, this._d48, this._d49, this._d50, this._d51, this._d52, this._d53, this._d54, this._d55,
                this._d56, this._d57, this._d58, this._d59, this._d60, this._d61, this._d62, this._d63, this._d64,
                this._d65, this._d66, this._d67, this._d68, this._d69, this._d70, this._d71, this._d72, this._d73,
                this._d74, this._d75, this._d76, this._d77, this._d78, this._d79, this._d80, this._d81, this._d82,
                this._d83, this._d84, this._d85, this._d86, this._d87, this._d88, this._d89, this._d90, this._d91,
                this._d92, this._d93, this._d94, this._d95, this._d96, this._d97, this._d98, this._d99, this._d100,
                this._d101, this._d102, this._d103, this._d104, this._d105, this._d106, this._d107, this._d108, this._d109,
                this._d110, this._d111, this._d112,this._k1, this._k2
            };

            this._splErr = new[]
            {
                this._fSpl5, this._fSpl1, this._fSpl2, this._fSpl3
            };
            this._groupCombo.SelectedIndex = 0;

            this._faultsSwitch = new[]
            {
                this._faultOut, this._faultBlockCon, this._faultManage, new LedControl(), this._faultSwithON,
                this._faultDisable1, this._faultDisable2
            };

            this.HideDiskret();

        }

        #endregion Constructor

        #region MemoryEntity Members

        private void HideDiskret()
        {
            switch (_device.Info.DeviceConfiguration)
            {
                case "T0N0D106R67":
                    this._releGB.Size = new Size(180, 355);

                    this._module35.Location = new Point(155, 22);
                    this._r35Label.Location = new Point(174, 22);

                    this._module36.Location = new Point(155, 41);
                    this._r36Label.Location = new Point(174, 41);

                    this._module37.Location = new Point(155, 60);
                    this._r37Label.Location = new Point(174, 60);

                    this._module38.Location = new Point(155, 79);
                    this._r38Label.Location = new Point(174, 79);

                    this._module39.Location = new Point(155, 98);
                    this._r39Label.Location = new Point(174, 98);

                    this._module40.Location = new Point(155, 117);
                    this._r40Label.Location = new Point(174, 117);

                    this._module41.Location = new Point(155, 136);
                    this._r41Label.Location = new Point(174, 136);

                    this._module42.Location = new Point(155, 155);
                    this._r42Label.Location = new Point(174, 155);

                    this._module43.Location = new Point(155, 174);
                    this._r43Label.Location = new Point(174, 174);

                    this._module44.Location = new Point(155, 193);
                    this._r44Label.Location = new Point(174, 193);

                    this._module45.Location = new Point(155, 212);
                    this._r45Label.Location = new Point(174, 212);

                    this._module46.Location = new Point(155, 231);
                    this._r46Label.Location = new Point(174, 231);

                    this._module47.Location = new Point(155, 250);
                    this._r47Label.Location = new Point(174, 250);

                    this._module48.Location = new Point(155, 269);
                    this._r48Label.Location = new Point(174, 269);

                    this._module49.Location = new Point(155, 288);
                    this._r49Label.Location = new Point(174, 288);

                    this._module50.Location = new Point(155, 307);
                    this._r50Label.Location = new Point(174, 307);

                    this._module51.Location = new Point(155, 326);
                    this._r51Label.Location = new Point(174, 326);

                    this._module52.Location = new Point(199, 22);
                    this._r52Label.Location = new Point(218, 22);

                    this._module53.Location = new Point(199, 41);
                    this._r53Label.Location = new Point(218, 41);

                    this._module54.Location = new Point(199, 60);
                    this._r54Label.Location = new Point(218, 60);

                    this._module55.Location = new Point(199, 79);
                    this._r55Label.Location = new Point(218, 79);

                    this._module56.Location = new Point(199, 98);
                    this._r56Label.Location = new Point(218, 98);

                    this._module57.Location = new Point(199, 117);
                    this._r57Label.Location = new Point(218, 117);

                    this._module58.Location = new Point(199, 136);
                    this._r58Label.Location = new Point(218, 136);

                    this._module59.Location = new Point(199, 155);
                    this._r59Label.Location = new Point(218, 155);

                    this._module60.Location = new Point(199, 174);
                    this._r60Label.Location = new Point(218, 174);

                    this._module61.Location = new Point(199, 193);
                    this._r61Label.Location = new Point(218, 193);

                    this._module62.Location = new Point(199, 212);
                    this._r62Label.Location = new Point(218, 212);

                    this._module63.Location = new Point(199, 231);
                    this._r63Label.Location = new Point(218, 231);

                    this._module64.Location = new Point(199, 250);
                    this._r64Label.Location = new Point(218, 250);

                    this._module65.Location = new Point(199, 269);
                    this._r65Label.Location = new Point(218, 269);

                    this._module66.Location = new Point(199, 288);
                    this._r66Label.Location = new Point(218, 288);

                    this._virtualReleGB.Location = new Point(248, 3);
                    this._virtualReleGB.Size = new Size(115, 355);
                    
                    this._module67.Location = new Point(258, 22);
                    this._r67Label.Location = new Point(277, 22);

                    this._module68.Location = new Point(258, 41);
                    this._r68Label.Location = new Point(277, 41);

                    this._module69.Location = new Point(258, 60);
                    this._r69Label.Location = new Point(277, 60);

                    this._module70.Location = new Point(258, 79);
                    this._r70Label.Location = new Point(277, 79);

                    this._module71.Location = new Point(258, 98);
                    this._r71Label.Location = new Point(277, 98);

                    this._module72.Location = new Point(258, 117);
                    this._r72Label.Location = new Point(277, 117);

                    this._module73.Location = new Point(258, 136);
                    this._r73Label.Location = new Point(277, 136);

                    this._module74.Location = new Point(258, 155);
                    this._r74Label.Location = new Point(277, 155);

                    this._module75.Location = new Point(258, 174);
                    this._r75Label.Location = new Point(277, 174);

                    this._module76.Location = new Point(258, 193);
                    this._r76Label.Location = new Point(277, 193);

                    this._module77.Location = new Point(258, 212);
                    this._r77Label.Location = new Point(277, 212);

                    this._module78.Location = new Point(258, 231);
                    this._r78Label.Location = new Point(277, 231);

                    this._module79.Location = new Point(258, 250);
                    this._r79Label.Location = new Point(277, 250);

                    this._module80.Location = new Point(258, 269);
                    this._r80Label.Location = new Point(277, 269);

                    this._discretsGB.Location = new Point(369, 3);
                    this._discretsGB.Size = new Size(375, 355);

                    this._d105.Visible = false;
                    this._d105Label.Visible = false;

                    this._d106.Visible = false;
                    this._d106Label.Visible = false;

                    this._d107.Visible = false;
                    this._d107Label.Visible = false;

                    this._d108.Visible = false;
                    this._d108Label.Visible = false;

                    this._d109.Visible = false;
                    this._d109Label.Visible = false;

                    this._d110.Visible = false;
                    this._d110Label.Visible = false;

                    this._d111.Visible = false;
                    this._d111Label.Visible = false;

                    this._d112.Visible = false;
                    this._d112Label.Visible = false;

                    this._k1.Location = new Point(316, 171);
                    this._k1Label.Location = new Point(335, 171);

                    this._k2.Location = new Point(316, 190);
                    this._k2Label.Location = new Point(335, 190);

                    this._rsTriggersGB.Location = new Point(750, 3);
                    break;
                case "T0N0D74R35":
                    this._releGB.Size = new Size(95, 355);
                    this._virtualReleGB.Size = new Size(139, 355);
                    this._virtualReleGB.Location = new Point(163, 3);

                    this._module35.Location = new Point(171, 22);
                    this._r35Label.Location = new Point(190, 22);

                    this._module36.Location = new Point(171, 41);
                    this._r36Label.Location = new Point(190, 41);

                    this._module37.Location = new Point(171, 60);
                    this._r37Label.Location = new Point(190, 60);

                    this._module38.Location = new Point(171, 79);
                    this._r38Label.Location = new Point(190, 79);

                    this._module39.Location = new Point(171, 98);
                    this._r39Label.Location = new Point(190, 98);

                    this._module40.Location = new Point(171, 117);
                    this._r40Label.Location = new Point(190, 117);

                    this._module41.Location = new Point(171, 136);
                    this._r41Label.Location = new Point(190, 136);

                    this._module42.Location = new Point(171, 155);
                    this._r42Label.Location = new Point(190, 155);

                    this._module43.Location = new Point(171, 174);
                    this._r43Label.Location = new Point(190, 174);

                    this._module44.Location = new Point(171, 193);
                    this._r44Label.Location = new Point(190, 193);

                    this._module45.Location = new Point(171, 212);
                    this._r45Label.Location = new Point(190, 212);

                    this._module46.Location = new Point(171, 231);
                    this._r46Label.Location = new Point(190, 231);

                    this._module47.Location = new Point(171, 250);
                    this._r47Label.Location = new Point(190, 250);

                    this._module48.Location = new Point(171, 269);
                    this._r48Label.Location = new Point(190, 269);

                    this._module49.Location = new Point(171, 288);
                    this._r49Label.Location = new Point(190, 288);

                    this._module50.Location = new Point(171, 307);
                    this._r50Label.Location = new Point(190, 307);

                    this._module51.Location = new Point(171, 326);
                    this._r51Label.Location = new Point(190, 326);

                    this._module52.Location = new Point(212, 22);
                    this._r52Label.Location = new Point(231, 22);

                    this._module53.Location = new Point(212, 41);
                    this._r53Label.Location = new Point(231, 41);

                    this._module54.Location = new Point(212, 60);
                    this._r54Label.Location = new Point(231, 60);

                    this._module55.Location = new Point(212, 79);
                    this._r55Label.Location = new Point(231, 79);

                    this._module56.Location = new Point(212, 98);
                    this._r56Label.Location = new Point(231, 98);

                    this._module57.Location = new Point(212, 117);
                    this._r57Label.Location = new Point(231, 117);

                    this._module58.Location = new Point(212, 136);
                    this._r58Label.Location = new Point(231, 136);

                    this._module59.Location = new Point(212, 155);
                    this._r59Label.Location = new Point(231, 155);

                    this._module60.Location = new Point(212, 174);
                    this._r60Label.Location = new Point(231, 174);

                    this._module61.Location = new Point(212, 193);
                    this._r61Label.Location = new Point(231, 193);

                    this._module62.Location = new Point(212, 212);
                    this._r62Label.Location = new Point(231, 212);

                    this._module63.Location = new Point(212, 231);
                    this._r63Label.Location = new Point(231, 231);

                    this._module64.Location = new Point(212, 250);
                    this._r64Label.Location = new Point(231, 250);

                    this._module65.Location = new Point(212, 269);
                    this._r65Label.Location = new Point(231, 269);

                    this._module66.Location = new Point(212, 288);
                    this._r66Label.Location = new Point(231, 288);

                    this._module67.Location = new Point(212, 307);
                    this._r67Label.Location = new Point(231, 307);

                    this._module68.Location = new Point(212, 326);
                    this._r68Label.Location = new Point(231, 326);

                    this._module69.Location = new Point(257, 22);
                    this._r69Label.Location = new Point(276, 22);

                    this._module70.Location = new Point(257, 41);
                    this._r70Label.Location = new Point(276, 41);

                    this._module71.Location = new Point(257, 60);
                    this._r71Label.Location = new Point(276, 60);

                    this._module72.Location = new Point(257, 79);
                    this._r72Label.Location = new Point(276, 79);

                    this._module73.Location = new Point(257, 98);
                    this._r73Label.Location = new Point(276, 98);

                    this._module74.Location = new Point(257, 117);
                    this._r74Label.Location = new Point(276, 117);

                    this._module75.Location = new Point(257, 136);
                    this._r75Label.Location = new Point(276, 136);

                    this._module76.Location = new Point(257, 155);
                    this._r76Label.Location = new Point(276, 155);

                    this._module77.Location = new Point(257, 174);
                    this._r77Label.Location = new Point(276, 174);

                    this._module78.Location = new Point(257, 193);
                    this._r78Label.Location = new Point(276, 193);

                    this._module79.Location = new Point(257, 212);
                    this._r79Label.Location = new Point(276, 212);

                    this._module80.Location = new Point(257, 231);
                    this._r80Label.Location = new Point(276, 231);

                    this._discretsGB.Size = new Size(262, 355);
                    this._discretsGB.Location = new Point(308, 3);

                    this._d73.Visible = false;
                    this._d73Label.Visible = false;

                    this._d74.Visible = false;
                    this._d74Label.Visible = false;

                    this._d75.Visible = false;
                    this._d75Label.Visible = false;

                    this._d76.Visible = false;
                    this._d76Label.Visible = false;

                    this._d77.Visible = false;
                    this._d77Label.Visible = false;

                    this._d78.Visible = false;
                    this._d78Label.Visible = false;

                    this._d79.Visible = false;
                    this._d79Label.Visible = false;

                    this._d80.Visible = false;
                    this._d80Label.Visible = false;

                    this._k1.Location = new Point(212, 171);
                    this._k1Label.Location = new Point(231, 171);

                    this._k2.Location = new Point(212, 190);
                    this._k2Label.Location = new Point(231, 190);

                    this._rsTriggersGB.Location = new Point(576, 3);

                    break;
            }
        }

        private void GroupUstavkiLoaded()
        {
            if (this._numGroup == this._groupUstavki.Value.Word) return;
            this._numGroup = this._groupUstavki.Value.Word;
            this._currenGroupLabel.Text = string.Format(CURRENT_GROUP, this._numGroup + 1);
        }
        
        /// <summary>
        /// Ошибка чтения дискретной базы данных
        /// </summary>
        private void DiscretBdReadFail()
        {
            LedManager.TurnOffLeds(this._discretInputs);
            LedManager.TurnOffLeds(this._inputsLogicSignals);
            LedManager.TurnOffLeds(this._outputLogicSignals);
            LedManager.TurnOffLeds(this._externalDefenses);
            LedManager.TurnOffLeds(this._freeLogic);
            LedManager.TurnOffLeds(this._stateAndApv);
            LedManager.TurnOffLeds(this._relays);
            LedManager.TurnOffLeds(this._controlSignals);
            LedManager.TurnOffLeds(this._rsTriggers);
            LedManager.TurnOffLeds(this._faultsMain);
            LedManager.TurnOffLeds(this._faultsSwitch);
            LedManager.TurnOffLeds(this._splErr);
            LedManager.TurnOffLeds(this._state);
            this._logicState.State = LedState.Off;
            foreach (var indicator in this._indicators)
            {
                indicator.TurnOff();
            }
        }

        /// <summary>
        /// Прочитана дискретная база данных
        /// </summary>
        private void DiscretBdReadOk()
        {

            //Дискретные входы
            LedManager.SetLeds(this._discretInputs, this._discretDataBase.Value.DiscretInputs);
            //Входные ЛС
            LedManager.SetLeds(this._inputsLogicSignals, this._discretDataBase.Value.InputsLogicSignals);
            //Выходные ВЛС
            LedManager.SetLeds(this._outputLogicSignals, this._discretDataBase.Value.OutputLogicSignals);
            //Внешние защиты
            LedManager.SetLeds(this._externalDefenses, this._discretDataBase.Value.ExternalDefenses);
            //Свободная логика
            LedManager.SetLeds(this._freeLogic, this._discretDataBase.Value.FreeLogic);
            //Состояния и АПВ
            LedManager.SetLeds(this._stateAndApv, this._discretDataBase.Value.StateAndApv);
            //Реле
            LedManager.SetLeds(this._relays, this._discretDataBase.Value.Relays);
            //RS-Тригеры 
            LedManager.SetLeds(this._rsTriggers, this._discretDataBase.Value.RSTriggers);
            //Индикаторы
            for (int i = 0; i < this._indicators.Length; i++)
            {
                this._indicators[i].SetState(this._discretDataBase.Value.Indicators[i]);
            }
            LedManager.SetLeds(this._state, this._discretDataBase.Value.State);
            //Контроль
            LedManager.SetLeds(this._controlSignals, this._discretDataBase.Value.ControlSignals);
            //Неисправности основные
            LedManager.SetLeds(this._faultsMain, this._discretDataBase.Value.Faults1);
            //Неисправности выключателя
            LedManager.SetLeds(this._faultsSwitch, this._discretDataBase.Value.Faults2);
            //Ошибки СПЛ
            LedManager.SetLeds(this._splErr, this._discretDataBase.Value.FaultLogicErr);

            bool res = this._discretDataBase.Value.ControlSignals[7] &&
                       !(this._discretDataBase.Value.FaultLogicErr[0] || this._discretDataBase.Value.FaultLogicErr[1] ||
                         this._discretDataBase.Value.FaultLogicErr[2] || this._discretDataBase.Value.FaultLogicErr[3]);
            this._logicState.State = res ? LedState.NoSignaled : LedState.Signaled;
        }

        /// <summary>
        /// Прочитанно время
        /// </summary>
        private void DateTimeLoad()
        {
            this._dateTimeControl.DateTime = this._dateTime.Value;
        }

        #endregion MemoryEntity Members

        #region [Help members]

        private void dateTimeControl_TimeChanged()
        {
            this._dateTime.Value = this._dateTimeControl.DateTime;
            this._dateTime.SaveStruct();
        }

        private void MeasuringForm_Load(object sender, EventArgs e)
        {
            this.StartStopLoad();
        }

        private void MeasuringForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._groupUstavki.RemoveStructQueries();
            this._discretDataBase.RemoveStructQueries();
            this._dateTime.RemoveStructQueries();
            this._device.ConnectionModeChanged -= this.StartStopLoad;
        }

        private void StartStopLoad()
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._groupUstavki.LoadStructCycle();
                this._discretDataBase.LoadStructCycle();
                this._dateTime.LoadStructCycle();
            }
            else
            {
                this._groupUstavki.RemoveStructQueries();
                this._discretDataBase.RemoveStructQueries();
                this._dateTime.RemoveStructQueries();
                this.DiscretBdReadFail();
            }
        }

        private void _resetSystemJournalButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._discretDataBase.SetBitByAdress(0x0D01, RESET_SJ);
        }

        private void _resetAlarmJournalButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._discretDataBase.SetBitByAdress(0x0D02, RESET_AJ);
        }

        private void _resetOscJournalButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._discretDataBase.SetBitByAdress(0x0D03, RESET_OSC);
        }

        private void _resetAvailabilityFaultSystemJournalButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._discretDataBase.SetBitByAdress(0x0D04, RESET_FAULT_SJ);
        }

        private void _resetAnButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._discretDataBase.SetBitByAdress(0x0D05, RESET_INDICATION);
        }
        
        private void _breakerOffBut_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._discretDataBase.SetBitByAdress(0x0D08, SWITCH_OFF);
        }
        
        private void _breakerOnBut_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._discretDataBase.SetBitByAdress(0x0D09, SWITCH_ON);
        }
        
        private void StopLogicBtnClick(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show(
                "Остановить свободно программируемую логику в устройстве? ВНИМАНИЕ! Это может привести к выводу из работы важных функций устройства",
                "Останов СПЛ",
                MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;
            this._device.SetBit(this._device.DeviceNumber, 0x0D0C, true, "Останов СПЛ", this._device);
        }

        private void StartLogicBtnClick(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show("Запустить свободно программируемую логику в устройстве?", "Запуск СПЛ",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;
            this._device.SetBit(this._device.DeviceNumber, 0x0D0D, true, "Запуск СПЛ", this._device);
        }
        
        private void _startOscBtnClick(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._discretDataBase.SetBitByAdress(0x0D11, START_OSC);
        }

        private void _switchGroupButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (this._groupCombo.SelectedIndex == this._groupUstavki.Value.Word) return;
            int ind = 0;
            if (this._groupCombo.SelectedIndex != -1) ind = this._groupCombo.SelectedIndex;
            bool res = MessageBox.Show(string.Format(SWITCH_GROUP_USTAVKI, ind + 1),
                "Группы уставок", MessageBoxButtons.YesNo) != DialogResult.Yes;
            if (res) return;
            this._groupUstavki.Value.Word = (ushort)ind;
            this._groupUstavki.SaveStruct();
            this._numGroup = this._groupUstavki.Value.Word;
            this._currenGroupLabel.Text = string.Format(CURRENT_GROUP, this._numGroup + 1);
        }

        private void _mainGroupButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            ind = 0;
            if (ind == this._groupUstavki.Value.Word) return;
            bool res = MessageBox.Show(string.Format(SWITCH_GROUP_USTAVKI, ind + 1),
                           "Группы уставок", MessageBoxButtons.YesNo) != DialogResult.Yes;
            if (res) return;
            this._groupUstavki.Value.Word = (ushort)ind;
            this._groupUstavki.SaveStruct();
        }

        private void _reserveGroupButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            ind = 1;
            if (ind == this._groupUstavki.Value.Word) return;
            bool res = MessageBox.Show(string.Format(SWITCH_GROUP_USTAVKI, ind + 1),
                           "Группы уставок", MessageBoxButtons.YesNo) != DialogResult.Yes;
            if (res) return;
            this._groupUstavki.Value.Word = (ushort)ind;
            this._groupUstavki.SaveStruct();
        }

        private void CommandRunClick(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            int i = _commandComboBox.SelectedIndex;
            ConfirmCommand((ushort)(0x0D20 + i), "Выполнить команду " + (i + 1));
            //this._discretDataBase.SetBitByAdress((ushort) (0x0D12 + i), "Выполнить команду " + (i+1));
        }

        private void ConfirmCommand(ushort address, string command)
        {
            DialogResult res = MessageBox.Show(command + "?", "Подтверждение комманды", MessageBoxButtons.YesNo);
            if (res == DialogResult.No) return;
            this._discretDataBase.SetBitByAdress(address, command);
        }

        #endregion [Help members]

        #region [IFormView Members]

        public Type FormDevice
        {
            get { return typeof (Mr761ObrDevice); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof (Mr761ObrMeasuringFormNew); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Properties.Resources.measuring.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Измерения"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] {}; }
        }

        public bool Deletable
        {
            get { return false; }
        }


        #endregion [INodeView Members]
    }
}
