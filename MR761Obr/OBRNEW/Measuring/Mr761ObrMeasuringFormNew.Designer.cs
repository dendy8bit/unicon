﻿namespace BEMN.MR761Obr.OBRNEW.Measuring
{
    partial class Mr761ObrMeasuringFormNew
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            BEMN.Devices.Structures.DateTimeStruct dateTimeStruct1 = new BEMN.Devices.Structures.DateTimeStruct();
            this._controlSignalsTabPage = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this._reserveGroupButton = new System.Windows.Forms.Button();
            this._mainGroupButton = new System.Windows.Forms.Button();
            this.label490 = new System.Windows.Forms.Label();
            this.label491 = new System.Windows.Forms.Label();
            this._reservedGroup = new BEMN.Forms.LedControl();
            this._mainGroup = new BEMN.Forms.LedControl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._commandBtn = new System.Windows.Forms.Button();
            this._commandComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox28 = new System.Windows.Forms.GroupBox();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this._currenGroupLabel = new System.Windows.Forms.Label();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this._groupCombo = new System.Windows.Forms.ComboBox();
            this._switchGroupButton = new System.Windows.Forms.Button();
            this.groupBox27 = new System.Windows.Forms.GroupBox();
            this.groupBox40 = new System.Windows.Forms.GroupBox();
            this._logicState = new BEMN.Forms.LedControl();
            this.stopLogic = new System.Windows.Forms.Button();
            this.startLogic = new System.Windows.Forms.Button();
            this.label314 = new System.Windows.Forms.Label();
            this._breakerOffBut = new System.Windows.Forms.Button();
            this.label100 = new System.Windows.Forms.Label();
            this._breakerOnBut = new System.Windows.Forms.Button();
            this.label101 = new System.Windows.Forms.Label();
            this._switchOn = new BEMN.Forms.LedControl();
            this._switchOff = new BEMN.Forms.LedControl();
            this._startOscBtn = new System.Windows.Forms.Button();
            this._availabilityFaultSystemJournal = new BEMN.Forms.LedControl();
            this._newRecordOscJournal = new BEMN.Forms.LedControl();
            this._newRecordAlarmJournal = new BEMN.Forms.LedControl();
            this._newRecordSystemJournal = new BEMN.Forms.LedControl();
            this._resetAnButton = new System.Windows.Forms.Button();
            this._resetAvailabilityFaultSystemJournalButton = new System.Windows.Forms.Button();
            this._resetOscJournalButton = new System.Windows.Forms.Button();
            this._resetAlarmJournalButton = new System.Windows.Forms.Button();
            this._resetSystemJournalButton = new System.Windows.Forms.Button();
            this.label227 = new System.Windows.Forms.Label();
            this.label225 = new System.Windows.Forms.Label();
            this.label226 = new System.Windows.Forms.Label();
            this.label231 = new System.Windows.Forms.Label();
            this._discretTabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox49 = new System.Windows.Forms.GroupBox();
            this.label347 = new System.Windows.Forms.Label();
            this._faultDisable2 = new BEMN.Forms.LedControl();
            this.label346 = new System.Windows.Forms.Label();
            this.label345 = new System.Windows.Forms.Label();
            this.label311 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this._faultDisable1 = new BEMN.Forms.LedControl();
            this._faultSwithON = new BEMN.Forms.LedControl();
            this._faultManage = new BEMN.Forms.LedControl();
            this._faultBlockCon = new BEMN.Forms.LedControl();
            this._faultOut = new BEMN.Forms.LedControl();
            this.groupBox38 = new System.Windows.Forms.GroupBox();
            this.groupBox39 = new System.Windows.Forms.GroupBox();
            this._fSpl1 = new BEMN.Forms.LedControl();
            this.label315 = new System.Windows.Forms.Label();
            this.label316 = new System.Windows.Forms.Label();
            this._fSpl2 = new BEMN.Forms.LedControl();
            this.label317 = new System.Windows.Forms.Label();
            this._fSpl3 = new BEMN.Forms.LedControl();
            this._fSpl5 = new BEMN.Forms.LedControl();
            this.label320 = new System.Windows.Forms.Label();
            this.label319 = new System.Windows.Forms.Label();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.label344 = new System.Windows.Forms.Label();
            this._faultSwitchOff = new BEMN.Forms.LedControl();
            this._faultAlarmJournal = new BEMN.Forms.LedControl();
            this.label192 = new System.Windows.Forms.Label();
            this._faultOsc = new BEMN.Forms.LedControl();
            this.label193 = new System.Windows.Forms.Label();
            this._faultModule5 = new BEMN.Forms.LedControl();
            this._faultSystemJournal = new BEMN.Forms.LedControl();
            this.label200 = new System.Windows.Forms.Label();
            this.label194 = new System.Windows.Forms.Label();
            this._faultGroupsOfSetpoints = new BEMN.Forms.LedControl();
            this.label201 = new System.Windows.Forms.Label();
            this._faultModule4 = new BEMN.Forms.LedControl();
            this._faultLogic = new BEMN.Forms.LedControl();
            this.label79 = new System.Windows.Forms.Label();
            this._faultSetpoints = new BEMN.Forms.LedControl();
            this.label202 = new System.Windows.Forms.Label();
            this.label195 = new System.Windows.Forms.Label();
            this._faultPass = new BEMN.Forms.LedControl();
            this.label203 = new System.Windows.Forms.Label();
            this._faultModule3 = new BEMN.Forms.LedControl();
            this.label196 = new System.Windows.Forms.Label();
            this._faultSoftware = new BEMN.Forms.LedControl();
            this.label205 = new System.Windows.Forms.Label();
            this._faultModule2 = new BEMN.Forms.LedControl();
            this._faultHardware = new BEMN.Forms.LedControl();
            this.label206 = new System.Windows.Forms.Label();
            this.label197 = new System.Windows.Forms.Label();
            this._faultModule1 = new BEMN.Forms.LedControl();
            this.label198 = new System.Windows.Forms.Label();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this._vz16 = new BEMN.Forms.LedControl();
            this.label111 = new System.Windows.Forms.Label();
            this._vz15 = new BEMN.Forms.LedControl();
            this.label112 = new System.Windows.Forms.Label();
            this._vz14 = new BEMN.Forms.LedControl();
            this.label113 = new System.Windows.Forms.Label();
            this._vz13 = new BEMN.Forms.LedControl();
            this.label114 = new System.Windows.Forms.Label();
            this._vz12 = new BEMN.Forms.LedControl();
            this.label115 = new System.Windows.Forms.Label();
            this._vz11 = new BEMN.Forms.LedControl();
            this.label116 = new System.Windows.Forms.Label();
            this._vz10 = new BEMN.Forms.LedControl();
            this.label117 = new System.Windows.Forms.Label();
            this._vz9 = new BEMN.Forms.LedControl();
            this.label118 = new System.Windows.Forms.Label();
            this._vz8 = new BEMN.Forms.LedControl();
            this.label119 = new System.Windows.Forms.Label();
            this._vz7 = new BEMN.Forms.LedControl();
            this.label120 = new System.Windows.Forms.Label();
            this._vz6 = new BEMN.Forms.LedControl();
            this.label121 = new System.Windows.Forms.Label();
            this._vz5 = new BEMN.Forms.LedControl();
            this.label122 = new System.Windows.Forms.Label();
            this._vz4 = new BEMN.Forms.LedControl();
            this.label123 = new System.Windows.Forms.Label();
            this._vz3 = new BEMN.Forms.LedControl();
            this.label124 = new System.Windows.Forms.Label();
            this._vz2 = new BEMN.Forms.LedControl();
            this.label125 = new System.Windows.Forms.Label();
            this._vz1 = new BEMN.Forms.LedControl();
            this.label126 = new System.Windows.Forms.Label();
            this._dateTimeControl = new BEMN.Forms.DateTimeControl();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this._fault = new BEMN.Forms.LedControl();
            this.label210 = new System.Windows.Forms.Label();
            this._faultOff = new BEMN.Forms.LedControl();
            this.label21 = new System.Windows.Forms.Label();
            this._acceleration = new BEMN.Forms.LedControl();
            this._auto4Led = new System.Windows.Forms.Label();
            this._signaling = new BEMN.Forms.LedControl();
            this._auto2Led = new System.Windows.Forms.Label();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this._ssl48 = new BEMN.Forms.LedControl();
            this._ssl40 = new BEMN.Forms.LedControl();
            this.label17 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this._ssl47 = new BEMN.Forms.LedControl();
            this._ssl39 = new BEMN.Forms.LedControl();
            this.label16 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this._ssl46 = new BEMN.Forms.LedControl();
            this._ssl38 = new BEMN.Forms.LedControl();
            this.label15 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this._ssl45 = new BEMN.Forms.LedControl();
            this._ssl37 = new BEMN.Forms.LedControl();
            this.label14 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this._ssl44 = new BEMN.Forms.LedControl();
            this._ssl36 = new BEMN.Forms.LedControl();
            this.label13 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this._ssl43 = new BEMN.Forms.LedControl();
            this._ssl35 = new BEMN.Forms.LedControl();
            this.label11 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this._ssl42 = new BEMN.Forms.LedControl();
            this._ssl34 = new BEMN.Forms.LedControl();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this._ssl41 = new BEMN.Forms.LedControl();
            this.label9 = new System.Windows.Forms.Label();
            this._ssl33 = new BEMN.Forms.LedControl();
            this.label8 = new System.Windows.Forms.Label();
            this._ssl32 = new BEMN.Forms.LedControl();
            this.label237 = new System.Windows.Forms.Label();
            this._ssl31 = new BEMN.Forms.LedControl();
            this.label238 = new System.Windows.Forms.Label();
            this._ssl30 = new BEMN.Forms.LedControl();
            this.label239 = new System.Windows.Forms.Label();
            this._ssl29 = new BEMN.Forms.LedControl();
            this.label240 = new System.Windows.Forms.Label();
            this._ssl28 = new BEMN.Forms.LedControl();
            this.label241 = new System.Windows.Forms.Label();
            this._ssl27 = new BEMN.Forms.LedControl();
            this.label242 = new System.Windows.Forms.Label();
            this._ssl26 = new BEMN.Forms.LedControl();
            this.label243 = new System.Windows.Forms.Label();
            this._ssl25 = new BEMN.Forms.LedControl();
            this.label244 = new System.Windows.Forms.Label();
            this._ssl24 = new BEMN.Forms.LedControl();
            this.label245 = new System.Windows.Forms.Label();
            this._ssl23 = new BEMN.Forms.LedControl();
            this.label246 = new System.Windows.Forms.Label();
            this._ssl22 = new BEMN.Forms.LedControl();
            this.label247 = new System.Windows.Forms.Label();
            this._ssl21 = new BEMN.Forms.LedControl();
            this.label248 = new System.Windows.Forms.Label();
            this._ssl20 = new BEMN.Forms.LedControl();
            this.label249 = new System.Windows.Forms.Label();
            this._ssl19 = new BEMN.Forms.LedControl();
            this.label250 = new System.Windows.Forms.Label();
            this._ssl18 = new BEMN.Forms.LedControl();
            this.label251 = new System.Windows.Forms.Label();
            this._ssl17 = new BEMN.Forms.LedControl();
            this.label252 = new System.Windows.Forms.Label();
            this._ssl16 = new BEMN.Forms.LedControl();
            this.label253 = new System.Windows.Forms.Label();
            this._ssl15 = new BEMN.Forms.LedControl();
            this.label254 = new System.Windows.Forms.Label();
            this._ssl14 = new BEMN.Forms.LedControl();
            this.label255 = new System.Windows.Forms.Label();
            this._ssl13 = new BEMN.Forms.LedControl();
            this.label256 = new System.Windows.Forms.Label();
            this._ssl12 = new BEMN.Forms.LedControl();
            this.label257 = new System.Windows.Forms.Label();
            this._ssl11 = new BEMN.Forms.LedControl();
            this.label258 = new System.Windows.Forms.Label();
            this._ssl10 = new BEMN.Forms.LedControl();
            this.label259 = new System.Windows.Forms.Label();
            this._ssl9 = new BEMN.Forms.LedControl();
            this.label260 = new System.Windows.Forms.Label();
            this._ssl8 = new BEMN.Forms.LedControl();
            this.label261 = new System.Windows.Forms.Label();
            this._ssl7 = new BEMN.Forms.LedControl();
            this.label262 = new System.Windows.Forms.Label();
            this._ssl6 = new BEMN.Forms.LedControl();
            this.label263 = new System.Windows.Forms.Label();
            this._ssl5 = new BEMN.Forms.LedControl();
            this.label264 = new System.Windows.Forms.Label();
            this._ssl4 = new BEMN.Forms.LedControl();
            this.label265 = new System.Windows.Forms.Label();
            this._ssl3 = new BEMN.Forms.LedControl();
            this.label266 = new System.Windows.Forms.Label();
            this._ssl2 = new BEMN.Forms.LedControl();
            this.label267 = new System.Windows.Forms.Label();
            this._ssl1 = new BEMN.Forms.LedControl();
            this.label268 = new System.Windows.Forms.Label();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this._vls16 = new BEMN.Forms.LedControl();
            this.label63 = new System.Windows.Forms.Label();
            this._vls15 = new BEMN.Forms.LedControl();
            this.label64 = new System.Windows.Forms.Label();
            this._vls14 = new BEMN.Forms.LedControl();
            this.label65 = new System.Windows.Forms.Label();
            this._vls13 = new BEMN.Forms.LedControl();
            this.label66 = new System.Windows.Forms.Label();
            this._vls12 = new BEMN.Forms.LedControl();
            this.label67 = new System.Windows.Forms.Label();
            this._vls11 = new BEMN.Forms.LedControl();
            this.label68 = new System.Windows.Forms.Label();
            this._vls10 = new BEMN.Forms.LedControl();
            this.label69 = new System.Windows.Forms.Label();
            this._vls9 = new BEMN.Forms.LedControl();
            this.label70 = new System.Windows.Forms.Label();
            this._vls8 = new BEMN.Forms.LedControl();
            this.label71 = new System.Windows.Forms.Label();
            this._vls7 = new BEMN.Forms.LedControl();
            this.label72 = new System.Windows.Forms.Label();
            this._vls6 = new BEMN.Forms.LedControl();
            this.label73 = new System.Windows.Forms.Label();
            this._vls5 = new BEMN.Forms.LedControl();
            this.label74 = new System.Windows.Forms.Label();
            this._vls4 = new BEMN.Forms.LedControl();
            this.label75 = new System.Windows.Forms.Label();
            this._vls3 = new BEMN.Forms.LedControl();
            this.label76 = new System.Windows.Forms.Label();
            this._vls2 = new BEMN.Forms.LedControl();
            this.label77 = new System.Windows.Forms.Label();
            this._vls1 = new BEMN.Forms.LedControl();
            this.label78 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this._ls16 = new BEMN.Forms.LedControl();
            this.label47 = new System.Windows.Forms.Label();
            this._ls15 = new BEMN.Forms.LedControl();
            this.label48 = new System.Windows.Forms.Label();
            this._ls14 = new BEMN.Forms.LedControl();
            this.label49 = new System.Windows.Forms.Label();
            this._ls13 = new BEMN.Forms.LedControl();
            this.label50 = new System.Windows.Forms.Label();
            this._ls12 = new BEMN.Forms.LedControl();
            this.label51 = new System.Windows.Forms.Label();
            this._ls11 = new BEMN.Forms.LedControl();
            this.label52 = new System.Windows.Forms.Label();
            this._ls10 = new BEMN.Forms.LedControl();
            this.label53 = new System.Windows.Forms.Label();
            this._ls9 = new BEMN.Forms.LedControl();
            this.label54 = new System.Windows.Forms.Label();
            this._ls8 = new BEMN.Forms.LedControl();
            this.label55 = new System.Windows.Forms.Label();
            this._ls7 = new BEMN.Forms.LedControl();
            this.label56 = new System.Windows.Forms.Label();
            this._ls6 = new BEMN.Forms.LedControl();
            this.label57 = new System.Windows.Forms.Label();
            this._ls5 = new BEMN.Forms.LedControl();
            this.label58 = new System.Windows.Forms.Label();
            this._ls4 = new BEMN.Forms.LedControl();
            this.label59 = new System.Windows.Forms.Label();
            this._ls3 = new BEMN.Forms.LedControl();
            this.label60 = new System.Windows.Forms.Label();
            this._ls2 = new BEMN.Forms.LedControl();
            this.label61 = new System.Windows.Forms.Label();
            this._ls1 = new BEMN.Forms.LedControl();
            this.label62 = new System.Windows.Forms.Label();
            this._dataBaseTabControl = new System.Windows.Forms.TabControl();
            this._diskretAndReleTabPage = new System.Windows.Forms.TabPage();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.diod12 = new BEMN.Forms.Diod();
            this.diod11 = new BEMN.Forms.Diod();
            this.diod10 = new BEMN.Forms.Diod();
            this.diod9 = new BEMN.Forms.Diod();
            this.diod8 = new BEMN.Forms.Diod();
            this.diod7 = new BEMN.Forms.Diod();
            this.diod6 = new BEMN.Forms.Diod();
            this.diod5 = new BEMN.Forms.Diod();
            this.diod4 = new BEMN.Forms.Diod();
            this.diod3 = new BEMN.Forms.Diod();
            this.diod2 = new BEMN.Forms.Diod();
            this.diod1 = new BEMN.Forms.Diod();
            this.label190 = new System.Windows.Forms.Label();
            this.label189 = new System.Windows.Forms.Label();
            this.label179 = new System.Windows.Forms.Label();
            this.label180 = new System.Windows.Forms.Label();
            this.label181 = new System.Windows.Forms.Label();
            this.label182 = new System.Windows.Forms.Label();
            this.label183 = new System.Windows.Forms.Label();
            this.label184 = new System.Windows.Forms.Label();
            this.label185 = new System.Windows.Forms.Label();
            this.label186 = new System.Windows.Forms.Label();
            this.label187 = new System.Windows.Forms.Label();
            this.label188 = new System.Windows.Forms.Label();
            this._module35 = new BEMN.Forms.LedControl();
            this._module42 = new BEMN.Forms.LedControl();
            this._rsTriggersGB = new System.Windows.Forms.GroupBox();
            this._rst8 = new BEMN.Forms.LedControl();
            this.label12 = new System.Windows.Forms.Label();
            this._rst7 = new BEMN.Forms.LedControl();
            this.label96 = new System.Windows.Forms.Label();
            this._rst1 = new BEMN.Forms.LedControl();
            this._rst6 = new BEMN.Forms.LedControl();
            this.label99 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this._rst5 = new BEMN.Forms.LedControl();
            this._rst2 = new BEMN.Forms.LedControl();
            this.label150 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this._rst4 = new BEMN.Forms.LedControl();
            this._rst16 = new BEMN.Forms.LedControl();
            this._rst3 = new BEMN.Forms.LedControl();
            this.label207 = new System.Windows.Forms.Label();
            this.label208 = new System.Windows.Forms.Label();
            this._rst15 = new BEMN.Forms.LedControl();
            this._rst9 = new BEMN.Forms.LedControl();
            this.label209 = new System.Windows.Forms.Label();
            this.label211 = new System.Windows.Forms.Label();
            this._rst14 = new BEMN.Forms.LedControl();
            this.label214 = new System.Windows.Forms.Label();
            this.label215 = new System.Windows.Forms.Label();
            this._rst10 = new BEMN.Forms.LedControl();
            this._rst13 = new BEMN.Forms.LedControl();
            this.label216 = new System.Windows.Forms.Label();
            this.label217 = new System.Windows.Forms.Label();
            this._rst11 = new BEMN.Forms.LedControl();
            this._rst12 = new BEMN.Forms.LedControl();
            this.label219 = new System.Windows.Forms.Label();
            this._module47 = new BEMN.Forms.LedControl();
            this._r56Label = new System.Windows.Forms.Label();
            this._module36 = new BEMN.Forms.LedControl();
            this._r58Label = new System.Windows.Forms.Label();
            this._module43 = new BEMN.Forms.LedControl();
            this._r70Label = new System.Windows.Forms.Label();
            this._module48 = new BEMN.Forms.LedControl();
            this._r18Label = new System.Windows.Forms.Label();
            this._module41 = new BEMN.Forms.LedControl();
            this._r59Label = new System.Windows.Forms.Label();
            this._module44 = new BEMN.Forms.LedControl();
            this._r71Label = new System.Windows.Forms.Label();
            this._module37 = new BEMN.Forms.LedControl();
            this.label105 = new System.Windows.Forms.Label();
            this._module49 = new BEMN.Forms.LedControl();
            this._r64Label = new System.Windows.Forms.Label();
            this._module45 = new BEMN.Forms.LedControl();
            this.label106 = new System.Windows.Forms.Label();
            this._r40Label = new System.Windows.Forms.Label();
            this._r76Label = new System.Windows.Forms.Label();
            this._module38 = new BEMN.Forms.LedControl();
            this._r69Label = new System.Windows.Forms.Label();
            this._module50 = new BEMN.Forms.LedControl();
            this._r55Label = new System.Windows.Forms.Label();
            this._module46 = new BEMN.Forms.LedControl();
            this._r51Label = new System.Windows.Forms.Label();
            this._module39 = new BEMN.Forms.LedControl();
            this.label107 = new System.Windows.Forms.Label();
            this._module40 = new BEMN.Forms.LedControl();
            this._r60Label = new System.Windows.Forms.Label();
            this._r43Label = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this._r47Label = new System.Windows.Forms.Label();
            this._r72Label = new System.Windows.Forms.Label();
            this._r35Label = new System.Windows.Forms.Label();
            this._r57Label = new System.Windows.Forms.Label();
            this._r36Label = new System.Windows.Forms.Label();
            this._r65Label = new System.Windows.Forms.Label();
            this._r41Label = new System.Windows.Forms.Label();
            this._r80Label = new System.Windows.Forms.Label();
            this._r48Label = new System.Windows.Forms.Label();
            this._r77Label = new System.Windows.Forms.Label();
            this._r42Label = new System.Windows.Forms.Label();
            this._r68Label = new System.Windows.Forms.Label();
            this._r44Label = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this._r37Label = new System.Windows.Forms.Label();
            this._r75Label = new System.Windows.Forms.Label();
            this._r39Label = new System.Windows.Forms.Label();
            this._r54Label = new System.Windows.Forms.Label();
            this._r49Label = new System.Windows.Forms.Label();
            this._r61Label = new System.Windows.Forms.Label();
            this._r45Label = new System.Windows.Forms.Label();
            this._r63Label = new System.Windows.Forms.Label();
            this._r38Label = new System.Windows.Forms.Label();
            this._r73Label = new System.Windows.Forms.Label();
            this._r50Label = new System.Windows.Forms.Label();
            this.label110 = new System.Windows.Forms.Label();
            this._r46Label = new System.Windows.Forms.Label();
            this._r66Label = new System.Windows.Forms.Label();
            this._r78Label = new System.Windows.Forms.Label();
            this.label135 = new System.Windows.Forms.Label();
            this.label136 = new System.Windows.Forms.Label();
            this._r53Label = new System.Windows.Forms.Label();
            this.label137 = new System.Windows.Forms.Label();
            this._r62Label = new System.Windows.Forms.Label();
            this._r74Label = new System.Windows.Forms.Label();
            this._r67Label = new System.Windows.Forms.Label();
            this._r34Label = new System.Windows.Forms.Label();
            this._r79Label = new System.Windows.Forms.Label();
            this._r52Label = new System.Windows.Forms.Label();
            this.label143 = new System.Windows.Forms.Label();
            this.label144 = new System.Windows.Forms.Label();
            this.label145 = new System.Windows.Forms.Label();
            this.label146 = new System.Windows.Forms.Label();
            this.label147 = new System.Windows.Forms.Label();
            this.label148 = new System.Windows.Forms.Label();
            this.label149 = new System.Windows.Forms.Label();
            this.label151 = new System.Windows.Forms.Label();
            this._r30Label = new System.Windows.Forms.Label();
            this._r29Label = new System.Windows.Forms.Label();
            this._r28Label = new System.Windows.Forms.Label();
            this._r27Label = new System.Windows.Forms.Label();
            this._r33Label = new System.Windows.Forms.Label();
            this._r26Label = new System.Windows.Forms.Label();
            this._r25Label = new System.Windows.Forms.Label();
            this._r24Label = new System.Windows.Forms.Label();
            this._r23Label = new System.Windows.Forms.Label();
            this._r31Label = new System.Windows.Forms.Label();
            this._r22Label = new System.Windows.Forms.Label();
            this._r21Label = new System.Windows.Forms.Label();
            this._r20Label = new System.Windows.Forms.Label();
            this._r19Label = new System.Windows.Forms.Label();
            this._r32Label = new System.Windows.Forms.Label();
            this._module56 = new BEMN.Forms.LedControl();
            this._module57 = new BEMN.Forms.LedControl();
            this._module18 = new BEMN.Forms.LedControl();
            this._module10 = new BEMN.Forms.LedControl();
            this._module69 = new BEMN.Forms.LedControl();
            this._module58 = new BEMN.Forms.LedControl();
            this._module70 = new BEMN.Forms.LedControl();
            this._module80 = new BEMN.Forms.LedControl();
            this._module17 = new BEMN.Forms.LedControl();
            this._module51 = new BEMN.Forms.LedControl();
            this._module68 = new BEMN.Forms.LedControl();
            this._module55 = new BEMN.Forms.LedControl();
            this._module59 = new BEMN.Forms.LedControl();
            this._module9 = new BEMN.Forms.LedControl();
            this._module71 = new BEMN.Forms.LedControl();
            this._module75 = new BEMN.Forms.LedControl();
            this._module16 = new BEMN.Forms.LedControl();
            this._module64 = new BEMN.Forms.LedControl();
            this._module76 = new BEMN.Forms.LedControl();
            this._module63 = new BEMN.Forms.LedControl();
            this._module15 = new BEMN.Forms.LedControl();
            this._module5 = new BEMN.Forms.LedControl();
            this._module54 = new BEMN.Forms.LedControl();
            this._module34 = new BEMN.Forms.LedControl();
            this._module60 = new BEMN.Forms.LedControl();
            this._module72 = new BEMN.Forms.LedControl();
            this._module65 = new BEMN.Forms.LedControl();
            this._module8 = new BEMN.Forms.LedControl();
            this._module77 = new BEMN.Forms.LedControl();
            this._module14 = new BEMN.Forms.LedControl();
            this._module53 = new BEMN.Forms.LedControl();
            this._module79 = new BEMN.Forms.LedControl();
            this._module61 = new BEMN.Forms.LedControl();
            this._module73 = new BEMN.Forms.LedControl();
            this._module67 = new BEMN.Forms.LedControl();
            this._module66 = new BEMN.Forms.LedControl();
            this._module13 = new BEMN.Forms.LedControl();
            this._module78 = new BEMN.Forms.LedControl();
            this._module1 = new BEMN.Forms.LedControl();
            this._module74 = new BEMN.Forms.LedControl();
            this._module62 = new BEMN.Forms.LedControl();
            this._module52 = new BEMN.Forms.LedControl();
            this._module7 = new BEMN.Forms.LedControl();
            this._module12 = new BEMN.Forms.LedControl();
            this._module11 = new BEMN.Forms.LedControl();
            this._module2 = new BEMN.Forms.LedControl();
            this._module6 = new BEMN.Forms.LedControl();
            this._module3 = new BEMN.Forms.LedControl();
            this._module33 = new BEMN.Forms.LedControl();
            this._module4 = new BEMN.Forms.LedControl();
            this._module19 = new BEMN.Forms.LedControl();
            this._module29 = new BEMN.Forms.LedControl();
            this._module28 = new BEMN.Forms.LedControl();
            this._module27 = new BEMN.Forms.LedControl();
            this._module30 = new BEMN.Forms.LedControl();
            this._module26 = new BEMN.Forms.LedControl();
            this._module25 = new BEMN.Forms.LedControl();
            this._module24 = new BEMN.Forms.LedControl();
            this._module23 = new BEMN.Forms.LedControl();
            this._module22 = new BEMN.Forms.LedControl();
            this._module21 = new BEMN.Forms.LedControl();
            this._module20 = new BEMN.Forms.LedControl();
            this._module32 = new BEMN.Forms.LedControl();
            this._module31 = new BEMN.Forms.LedControl();
            this._virtualReleGB = new System.Windows.Forms.GroupBox();
            this._releGB = new System.Windows.Forms.GroupBox();
            this._discretsGB = new System.Windows.Forms.GroupBox();
            this._d112 = new BEMN.Forms.LedControl();
            this._d112Label = new System.Windows.Forms.Label();
            this._d111 = new BEMN.Forms.LedControl();
            this._d111Label = new System.Windows.Forms.Label();
            this._d110 = new BEMN.Forms.LedControl();
            this._d110Label = new System.Windows.Forms.Label();
            this._d109 = new BEMN.Forms.LedControl();
            this._d109Label = new System.Windows.Forms.Label();
            this._d108 = new BEMN.Forms.LedControl();
            this._d108Label = new System.Windows.Forms.Label();
            this._d107 = new BEMN.Forms.LedControl();
            this._d107Label = new System.Windows.Forms.Label();
            this._d106 = new BEMN.Forms.LedControl();
            this._d106Label = new System.Windows.Forms.Label();
            this._d105 = new BEMN.Forms.LedControl();
            this._d105Label = new System.Windows.Forms.Label();
            this._d104 = new BEMN.Forms.LedControl();
            this._d104Label = new System.Windows.Forms.Label();
            this._d103 = new BEMN.Forms.LedControl();
            this._d103Label = new System.Windows.Forms.Label();
            this._d102 = new BEMN.Forms.LedControl();
            this._d102Label = new System.Windows.Forms.Label();
            this._d101 = new BEMN.Forms.LedControl();
            this._d101Label = new System.Windows.Forms.Label();
            this._d100 = new BEMN.Forms.LedControl();
            this._d100Label = new System.Windows.Forms.Label();
            this._d99 = new BEMN.Forms.LedControl();
            this._d99Label = new System.Windows.Forms.Label();
            this._d98 = new BEMN.Forms.LedControl();
            this._d98Label = new System.Windows.Forms.Label();
            this._d97 = new BEMN.Forms.LedControl();
            this._d97Label = new System.Windows.Forms.Label();
            this._d96 = new BEMN.Forms.LedControl();
            this._d96Label = new System.Windows.Forms.Label();
            this._d95 = new BEMN.Forms.LedControl();
            this._d95Label = new System.Windows.Forms.Label();
            this._d94 = new BEMN.Forms.LedControl();
            this._d94Label = new System.Windows.Forms.Label();
            this._d93 = new BEMN.Forms.LedControl();
            this._d93Label = new System.Windows.Forms.Label();
            this._d92 = new BEMN.Forms.LedControl();
            this._d92Label = new System.Windows.Forms.Label();
            this._d91 = new BEMN.Forms.LedControl();
            this._d91Label = new System.Windows.Forms.Label();
            this._d90 = new BEMN.Forms.LedControl();
            this._d90Label = new System.Windows.Forms.Label();
            this._d89 = new BEMN.Forms.LedControl();
            this._d89Label = new System.Windows.Forms.Label();
            this._d88 = new BEMN.Forms.LedControl();
            this._d88Label = new System.Windows.Forms.Label();
            this._d87 = new BEMN.Forms.LedControl();
            this._d87Label = new System.Windows.Forms.Label();
            this._d86 = new BEMN.Forms.LedControl();
            this._d86Label = new System.Windows.Forms.Label();
            this._d85 = new BEMN.Forms.LedControl();
            this._d85Label = new System.Windows.Forms.Label();
            this._d84 = new BEMN.Forms.LedControl();
            this._d84Label = new System.Windows.Forms.Label();
            this._d83 = new BEMN.Forms.LedControl();
            this._d83Label = new System.Windows.Forms.Label();
            this._d82 = new BEMN.Forms.LedControl();
            this._d82Label = new System.Windows.Forms.Label();
            this._d81 = new BEMN.Forms.LedControl();
            this._d81Label = new System.Windows.Forms.Label();
            this._d80 = new BEMN.Forms.LedControl();
            this._d80Label = new System.Windows.Forms.Label();
            this._d79 = new BEMN.Forms.LedControl();
            this._d79Label = new System.Windows.Forms.Label();
            this._d78 = new BEMN.Forms.LedControl();
            this._d78Label = new System.Windows.Forms.Label();
            this._d77 = new BEMN.Forms.LedControl();
            this._d77Label = new System.Windows.Forms.Label();
            this._d76 = new BEMN.Forms.LedControl();
            this._d76Label = new System.Windows.Forms.Label();
            this._d75 = new BEMN.Forms.LedControl();
            this._d75Label = new System.Windows.Forms.Label();
            this._d74 = new BEMN.Forms.LedControl();
            this._d74Label = new System.Windows.Forms.Label();
            this._d73 = new BEMN.Forms.LedControl();
            this._d73Label = new System.Windows.Forms.Label();
            this._k2 = new BEMN.Forms.LedControl();
            this._k2Label = new System.Windows.Forms.Label();
            this._k1 = new BEMN.Forms.LedControl();
            this._k1Label = new System.Windows.Forms.Label();
            this._d72 = new BEMN.Forms.LedControl();
            this._d72Label = new System.Windows.Forms.Label();
            this._d71 = new BEMN.Forms.LedControl();
            this._d71Label = new System.Windows.Forms.Label();
            this._d70 = new BEMN.Forms.LedControl();
            this._d70Label = new System.Windows.Forms.Label();
            this._d69 = new BEMN.Forms.LedControl();
            this._d69Label = new System.Windows.Forms.Label();
            this._d68 = new BEMN.Forms.LedControl();
            this._d68Label = new System.Windows.Forms.Label();
            this._d67 = new BEMN.Forms.LedControl();
            this._d67Label = new System.Windows.Forms.Label();
            this._d66 = new BEMN.Forms.LedControl();
            this._d66Label = new System.Windows.Forms.Label();
            this._d65 = new BEMN.Forms.LedControl();
            this._d65Label = new System.Windows.Forms.Label();
            this._d64 = new BEMN.Forms.LedControl();
            this._d64Label = new System.Windows.Forms.Label();
            this._d63 = new BEMN.Forms.LedControl();
            this._d63Label = new System.Windows.Forms.Label();
            this._d62 = new BEMN.Forms.LedControl();
            this._d62Label = new System.Windows.Forms.Label();
            this._d61 = new BEMN.Forms.LedControl();
            this._d61Label = new System.Windows.Forms.Label();
            this._d60 = new BEMN.Forms.LedControl();
            this._d60Label = new System.Windows.Forms.Label();
            this._d59 = new BEMN.Forms.LedControl();
            this._d59Label = new System.Windows.Forms.Label();
            this._d58 = new BEMN.Forms.LedControl();
            this._d58Label = new System.Windows.Forms.Label();
            this._d57 = new BEMN.Forms.LedControl();
            this._d57Label = new System.Windows.Forms.Label();
            this._d56 = new BEMN.Forms.LedControl();
            this._d56Label = new System.Windows.Forms.Label();
            this._d55 = new BEMN.Forms.LedControl();
            this._d55Label = new System.Windows.Forms.Label();
            this._d54 = new BEMN.Forms.LedControl();
            this._d54Label = new System.Windows.Forms.Label();
            this._d53 = new BEMN.Forms.LedControl();
            this._d53Label = new System.Windows.Forms.Label();
            this._d52 = new BEMN.Forms.LedControl();
            this._d52Label = new System.Windows.Forms.Label();
            this._d51 = new BEMN.Forms.LedControl();
            this._d51Label = new System.Windows.Forms.Label();
            this._d50 = new BEMN.Forms.LedControl();
            this._d50Label = new System.Windows.Forms.Label();
            this._d49 = new BEMN.Forms.LedControl();
            this._d49Label = new System.Windows.Forms.Label();
            this._d48 = new BEMN.Forms.LedControl();
            this._d48Label = new System.Windows.Forms.Label();
            this._d47 = new BEMN.Forms.LedControl();
            this._d47Label = new System.Windows.Forms.Label();
            this._d46 = new BEMN.Forms.LedControl();
            this._d46Label = new System.Windows.Forms.Label();
            this._d45 = new BEMN.Forms.LedControl();
            this._d45Label = new System.Windows.Forms.Label();
            this._d44 = new BEMN.Forms.LedControl();
            this._d44Label = new System.Windows.Forms.Label();
            this._d43 = new BEMN.Forms.LedControl();
            this._d43Label = new System.Windows.Forms.Label();
            this._d42 = new BEMN.Forms.LedControl();
            this._d42Label = new System.Windows.Forms.Label();
            this._d41 = new BEMN.Forms.LedControl();
            this._d41Label = new System.Windows.Forms.Label();
            this._d40 = new BEMN.Forms.LedControl();
            this._d24 = new BEMN.Forms.LedControl();
            this._d40Label = new System.Windows.Forms.Label();
            this._d8 = new BEMN.Forms.LedControl();
            this._d39 = new BEMN.Forms.LedControl();
            this._d24Label = new System.Windows.Forms.Label();
            this._d39Label = new System.Windows.Forms.Label();
            this._d38 = new BEMN.Forms.LedControl();
            this._d23 = new BEMN.Forms.LedControl();
            this._d38Label = new System.Windows.Forms.Label();
            this._d8Label = new System.Windows.Forms.Label();
            this._d37 = new BEMN.Forms.LedControl();
            this._d23Label = new System.Windows.Forms.Label();
            this._d37Label = new System.Windows.Forms.Label();
            this._d22 = new BEMN.Forms.LedControl();
            this._d36 = new BEMN.Forms.LedControl();
            this._d7 = new BEMN.Forms.LedControl();
            this._d36Label = new System.Windows.Forms.Label();
            this._d22Label = new System.Windows.Forms.Label();
            this._d35 = new BEMN.Forms.LedControl();
            this._d7Label = new System.Windows.Forms.Label();
            this._d35Label = new System.Windows.Forms.Label();
            this._d21 = new BEMN.Forms.LedControl();
            this._d34 = new BEMN.Forms.LedControl();
            this._d1 = new BEMN.Forms.LedControl();
            this._d34Label = new System.Windows.Forms.Label();
            this._d21Label = new System.Windows.Forms.Label();
            this._d33 = new BEMN.Forms.LedControl();
            this._d33Label = new System.Windows.Forms.Label();
            this._d6 = new BEMN.Forms.LedControl();
            this._d20 = new BEMN.Forms.LedControl();
            this._d32 = new BEMN.Forms.LedControl();
            this._d1Label = new System.Windows.Forms.Label();
            this._d32Label = new System.Windows.Forms.Label();
            this._d20Label = new System.Windows.Forms.Label();
            this._d31 = new BEMN.Forms.LedControl();
            this._d6Label = new System.Windows.Forms.Label();
            this._d31Label = new System.Windows.Forms.Label();
            this._d19 = new BEMN.Forms.LedControl();
            this._d30 = new BEMN.Forms.LedControl();
            this._d2Label = new System.Windows.Forms.Label();
            this._d30Label = new System.Windows.Forms.Label();
            this._d19Label = new System.Windows.Forms.Label();
            this._d29 = new BEMN.Forms.LedControl();
            this._d5 = new BEMN.Forms.LedControl();
            this._d29Label = new System.Windows.Forms.Label();
            this._d18 = new BEMN.Forms.LedControl();
            this._d28 = new BEMN.Forms.LedControl();
            this._d2 = new BEMN.Forms.LedControl();
            this._d28Label = new System.Windows.Forms.Label();
            this._d18Label = new System.Windows.Forms.Label();
            this._d27 = new BEMN.Forms.LedControl();
            this._d5Label = new System.Windows.Forms.Label();
            this._d27Label = new System.Windows.Forms.Label();
            this._d17 = new BEMN.Forms.LedControl();
            this._d26 = new BEMN.Forms.LedControl();
            this._d17Label = new System.Windows.Forms.Label();
            this._d26Label = new System.Windows.Forms.Label();
            this._d3Label = new System.Windows.Forms.Label();
            this._d25 = new BEMN.Forms.LedControl();
            this._d25Label = new System.Windows.Forms.Label();
            this._d4 = new BEMN.Forms.LedControl();
            this._d16 = new BEMN.Forms.LedControl();
            this._d3 = new BEMN.Forms.LedControl();
            this._d16Label = new System.Windows.Forms.Label();
            this._d4Label = new System.Windows.Forms.Label();
            this._d15 = new BEMN.Forms.LedControl();
            this._d9 = new BEMN.Forms.LedControl();
            this._d15Label = new System.Windows.Forms.Label();
            this._d9Label = new System.Windows.Forms.Label();
            this._d14 = new BEMN.Forms.LedControl();
            this._d10Label = new System.Windows.Forms.Label();
            this._d14Label = new System.Windows.Forms.Label();
            this._d10 = new BEMN.Forms.LedControl();
            this._d13 = new BEMN.Forms.LedControl();
            this._d11Label = new System.Windows.Forms.Label();
            this._d13Label = new System.Windows.Forms.Label();
            this._d11 = new BEMN.Forms.LedControl();
            this._d12 = new BEMN.Forms.LedControl();
            this._d12Label = new System.Windows.Forms.Label();
            this._faultModule6 = new BEMN.Forms.LedControl();
            this.label18 = new System.Windows.Forms.Label();
            this._controlSignalsTabPage.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox28.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.groupBox27.SuspendLayout();
            this.groupBox40.SuspendLayout();
            this._discretTabPage1.SuspendLayout();
            this.groupBox49.SuspendLayout();
            this.groupBox38.SuspendLayout();
            this.groupBox39.SuspendLayout();
            this.groupBox19.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox21.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this._dataBaseTabControl.SuspendLayout();
            this._diskretAndReleTabPage.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this._rsTriggersGB.SuspendLayout();
            this._discretsGB.SuspendLayout();
            this.SuspendLayout();
            // 
            // _controlSignalsTabPage
            // 
            this._controlSignalsTabPage.Controls.Add(this.groupBox3);
            this._controlSignalsTabPage.Controls.Add(this.groupBox1);
            this._controlSignalsTabPage.Controls.Add(this.groupBox28);
            this._controlSignalsTabPage.Controls.Add(this.groupBox27);
            this._controlSignalsTabPage.Location = new System.Drawing.Point(4, 22);
            this._controlSignalsTabPage.Name = "_controlSignalsTabPage";
            this._controlSignalsTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._controlSignalsTabPage.Size = new System.Drawing.Size(933, 599);
            this._controlSignalsTabPage.TabIndex = 2;
            this._controlSignalsTabPage.Text = "Управляющие сигналы";
            this._controlSignalsTabPage.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this._reserveGroupButton);
            this.groupBox3.Controls.Add(this._mainGroupButton);
            this.groupBox3.Controls.Add(this.label490);
            this.groupBox3.Controls.Add(this.label491);
            this.groupBox3.Controls.Add(this._reservedGroup);
            this.groupBox3.Controls.Add(this._mainGroup);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox3.Location = new System.Drawing.Point(329, 11);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(193, 83);
            this.groupBox3.TabIndex = 27;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Группа уставок";
            // 
            // _reserveGroupButton
            // 
            this._reserveGroupButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._reserveGroupButton.Location = new System.Drawing.Point(101, 56);
            this._reserveGroupButton.Name = "_reserveGroupButton";
            this._reserveGroupButton.Size = new System.Drawing.Size(86, 23);
            this._reserveGroupButton.TabIndex = 30;
            this._reserveGroupButton.Text = "Переключить";
            this._reserveGroupButton.UseVisualStyleBackColor = true;
            this._reserveGroupButton.Click += new System.EventHandler(this._reserveGroupButton_Click);
            // 
            // _mainGroupButton
            // 
            this._mainGroupButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._mainGroupButton.Location = new System.Drawing.Point(6, 56);
            this._mainGroupButton.Name = "_mainGroupButton";
            this._mainGroupButton.Size = new System.Drawing.Size(86, 23);
            this._mainGroupButton.TabIndex = 29;
            this._mainGroupButton.Text = "Переключить";
            this._mainGroupButton.UseVisualStyleBackColor = true;
            this._mainGroupButton.Click += new System.EventHandler(this._mainGroupButton_Click);
            // 
            // label490
            // 
            this.label490.AutoSize = true;
            this.label490.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label490.Location = new System.Drawing.Point(118, 39);
            this.label490.Name = "label490";
            this.label490.Size = new System.Drawing.Size(51, 13);
            this.label490.TabIndex = 28;
            this.label490.Text = "Группа 2";
            // 
            // label491
            // 
            this.label491.AutoSize = true;
            this.label491.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label491.Location = new System.Drawing.Point(22, 39);
            this.label491.Name = "label491";
            this.label491.Size = new System.Drawing.Size(51, 13);
            this.label491.TabIndex = 27;
            this.label491.Text = "Группа 1";
            // 
            // _reservedGroup
            // 
            this._reservedGroup.Location = new System.Drawing.Point(136, 19);
            this._reservedGroup.Name = "_reservedGroup";
            this._reservedGroup.Size = new System.Drawing.Size(13, 13);
            this._reservedGroup.State = BEMN.Forms.LedState.Off;
            this._reservedGroup.TabIndex = 24;
            // 
            // _mainGroup
            // 
            this._mainGroup.Location = new System.Drawing.Point(41, 19);
            this._mainGroup.Name = "_mainGroup";
            this._mainGroup.Size = new System.Drawing.Size(14, 13);
            this._mainGroup.State = BEMN.Forms.LedState.Off;
            this._mainGroup.TabIndex = 23;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._commandBtn);
            this.groupBox1.Controls.Add(this._commandComboBox);
            this.groupBox1.Location = new System.Drawing.Point(329, 100);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(193, 53);
            this.groupBox1.TabIndex = 25;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Команды";
            // 
            // _commandBtn
            // 
            this._commandBtn.Location = new System.Drawing.Point(112, 16);
            this._commandBtn.Name = "_commandBtn";
            this._commandBtn.Size = new System.Drawing.Size(75, 23);
            this._commandBtn.TabIndex = 1;
            this._commandBtn.Text = "Выполнить";
            this._commandBtn.UseVisualStyleBackColor = true;
            this._commandBtn.Click += new System.EventHandler(this.CommandRunClick);
            // 
            // _commandComboBox
            // 
            this._commandComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._commandComboBox.FormattingEnabled = true;
            this._commandComboBox.Items.AddRange(new object[] {
            "Команда 1",
            "Команда 2",
            "Команда 3",
            "Команда 4",
            "Команда 5",
            "Команда 6",
            "Команда 7",
            "Команда 8",
            "Команда 9",
            "Команда 10",
            "Команда 11",
            "Команда 12",
            "Команда 13",
            "Команда 14",
            "Команда 15",
            "Команда 16",
            "Команда 17",
            "Команда 18",
            "Команда 19",
            "Команда 20",
            "Команда 21",
            "Команда 22",
            "Команда 23",
            "Команда 24",
            "Команда 25",
            "Команда 26",
            "Команда 27",
            "Команда 28",
            "Команда 29",
            "Команда 30",
            "Команда 31",
            "Команда 32"});
            this._commandComboBox.Location = new System.Drawing.Point(7, 18);
            this._commandComboBox.Name = "_commandComboBox";
            this._commandComboBox.Size = new System.Drawing.Size(99, 21);
            this._commandComboBox.TabIndex = 0;
            // 
            // groupBox28
            // 
            this.groupBox28.Controls.Add(this.groupBox16);
            this.groupBox28.Controls.Add(this.groupBox15);
            this.groupBox28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox28.Location = new System.Drawing.Point(562, 11);
            this.groupBox28.Name = "groupBox28";
            this.groupBox28.Size = new System.Drawing.Size(223, 101);
            this.groupBox28.TabIndex = 24;
            this.groupBox28.TabStop = false;
            this.groupBox28.Text = "Группа уставок";
            this.groupBox28.Visible = false;
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this._currenGroupLabel);
            this.groupBox16.Location = new System.Drawing.Point(114, 14);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(103, 81);
            this.groupBox16.TabIndex = 32;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Текущая группа";
            // 
            // _currenGroupLabel
            // 
            this._currenGroupLabel.AutoSize = true;
            this._currenGroupLabel.Location = new System.Drawing.Point(17, 33);
            this._currenGroupLabel.Name = "_currenGroupLabel";
            this._currenGroupLabel.Size = new System.Drawing.Size(62, 13);
            this._currenGroupLabel.TabIndex = 0;
            this._currenGroupLabel.Text = "Группа №1";
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this._groupCombo);
            this.groupBox15.Controls.Add(this._switchGroupButton);
            this.groupBox15.Location = new System.Drawing.Point(6, 14);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(102, 81);
            this.groupBox15.TabIndex = 32;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Выбрать группу";
            // 
            // _groupCombo
            // 
            this._groupCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._groupCombo.FormattingEnabled = true;
            this._groupCombo.Items.AddRange(new object[] {
            "Группа 1",
            "Группа 2",
            "Группа 3",
            "Группа 4",
            "Группа 5",
            "Группа 6"});
            this._groupCombo.Location = new System.Drawing.Point(6, 25);
            this._groupCombo.Name = "_groupCombo";
            this._groupCombo.Size = new System.Drawing.Size(86, 21);
            this._groupCombo.TabIndex = 31;
            // 
            // _switchGroupButton
            // 
            this._switchGroupButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._switchGroupButton.Location = new System.Drawing.Point(6, 52);
            this._switchGroupButton.Name = "_switchGroupButton";
            this._switchGroupButton.Size = new System.Drawing.Size(86, 23);
            this._switchGroupButton.TabIndex = 30;
            this._switchGroupButton.Text = "Переключить";
            this._switchGroupButton.UseVisualStyleBackColor = true;
            this._switchGroupButton.Click += new System.EventHandler(this._switchGroupButton_Click);
            // 
            // groupBox27
            // 
            this.groupBox27.Controls.Add(this.groupBox40);
            this.groupBox27.Controls.Add(this._breakerOffBut);
            this.groupBox27.Controls.Add(this.label100);
            this.groupBox27.Controls.Add(this._breakerOnBut);
            this.groupBox27.Controls.Add(this.label101);
            this.groupBox27.Controls.Add(this._switchOn);
            this.groupBox27.Controls.Add(this._switchOff);
            this.groupBox27.Controls.Add(this._startOscBtn);
            this.groupBox27.Controls.Add(this._availabilityFaultSystemJournal);
            this.groupBox27.Controls.Add(this._newRecordOscJournal);
            this.groupBox27.Controls.Add(this._newRecordAlarmJournal);
            this.groupBox27.Controls.Add(this._newRecordSystemJournal);
            this.groupBox27.Controls.Add(this._resetAnButton);
            this.groupBox27.Controls.Add(this._resetAvailabilityFaultSystemJournalButton);
            this.groupBox27.Controls.Add(this._resetOscJournalButton);
            this.groupBox27.Controls.Add(this._resetAlarmJournalButton);
            this.groupBox27.Controls.Add(this._resetSystemJournalButton);
            this.groupBox27.Controls.Add(this.label227);
            this.groupBox27.Controls.Add(this.label225);
            this.groupBox27.Controls.Add(this.label226);
            this.groupBox27.Controls.Add(this.label231);
            this.groupBox27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox27.Location = new System.Drawing.Point(6, 6);
            this.groupBox27.Name = "groupBox27";
            this.groupBox27.Size = new System.Drawing.Size(316, 305);
            this.groupBox27.TabIndex = 23;
            this.groupBox27.TabStop = false;
            this.groupBox27.Text = "Управляющие сигналы";
            // 
            // groupBox40
            // 
            this.groupBox40.Controls.Add(this._logicState);
            this.groupBox40.Controls.Add(this.stopLogic);
            this.groupBox40.Controls.Add(this.startLogic);
            this.groupBox40.Controls.Add(this.label314);
            this.groupBox40.Location = new System.Drawing.Point(6, 176);
            this.groupBox40.Name = "groupBox40";
            this.groupBox40.Size = new System.Drawing.Size(304, 60);
            this.groupBox40.TabIndex = 54;
            this.groupBox40.TabStop = false;
            this.groupBox40.Text = "Свободно программируемая логика";
            // 
            // _logicState
            // 
            this._logicState.Location = new System.Drawing.Point(6, 25);
            this._logicState.Name = "_logicState";
            this._logicState.Size = new System.Drawing.Size(13, 13);
            this._logicState.State = BEMN.Forms.LedState.Off;
            this._logicState.TabIndex = 17;
            // 
            // stopLogic
            // 
            this.stopLogic.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.stopLogic.Location = new System.Drawing.Point(223, 32);
            this.stopLogic.Name = "stopLogic";
            this.stopLogic.Size = new System.Drawing.Size(75, 23);
            this.stopLogic.TabIndex = 15;
            this.stopLogic.Text = "Остановить";
            this.stopLogic.UseVisualStyleBackColor = true;
            this.stopLogic.Click += new System.EventHandler(this.StopLogicBtnClick);
            // 
            // startLogic
            // 
            this.startLogic.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.startLogic.Location = new System.Drawing.Point(223, 10);
            this.startLogic.Name = "startLogic";
            this.startLogic.Size = new System.Drawing.Size(75, 23);
            this.startLogic.TabIndex = 16;
            this.startLogic.Text = "Запустить";
            this.startLogic.UseVisualStyleBackColor = true;
            this.startLogic.Click += new System.EventHandler(this.StartLogicBtnClick);
            // 
            // label314
            // 
            this.label314.AutoSize = true;
            this.label314.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label314.Location = new System.Drawing.Point(30, 25);
            this.label314.Name = "label314";
            this.label314.Size = new System.Drawing.Size(137, 13);
            this.label314.TabIndex = 14;
            this.label314.Text = "Состояние задачи логики";
            // 
            // _breakerOffBut
            // 
            this._breakerOffBut.Location = new System.Drawing.Point(235, 37);
            this._breakerOffBut.Name = "_breakerOffBut";
            this._breakerOffBut.Size = new System.Drawing.Size(75, 23);
            this._breakerOffBut.TabIndex = 53;
            this._breakerOffBut.Text = "Отключить";
            this._breakerOffBut.UseVisualStyleBackColor = true;
            this._breakerOffBut.Click += new System.EventHandler(this._breakerOffBut_Click);
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(30, 42);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(122, 13);
            this.label100.TabIndex = 52;
            this.label100.Text = "Выключатель включен";
            // 
            // _breakerOnBut
            // 
            this._breakerOnBut.Location = new System.Drawing.Point(235, 14);
            this._breakerOnBut.Name = "_breakerOnBut";
            this._breakerOnBut.Size = new System.Drawing.Size(75, 23);
            this._breakerOnBut.TabIndex = 51;
            this._breakerOnBut.Text = "Включить";
            this._breakerOnBut.UseVisualStyleBackColor = true;
            this._breakerOnBut.Click += new System.EventHandler(this._breakerOnBut_Click);
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(30, 19);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(127, 13);
            this.label101.TabIndex = 50;
            this.label101.Text = "Выключатель отключен";
            // 
            // _switchOn
            // 
            this._switchOn.Location = new System.Drawing.Point(6, 42);
            this._switchOn.Name = "_switchOn";
            this._switchOn.Size = new System.Drawing.Size(13, 13);
            this._switchOn.State = BEMN.Forms.LedState.Off;
            this._switchOn.TabIndex = 49;
            // 
            // _switchOff
            // 
            this._switchOff.Location = new System.Drawing.Point(6, 19);
            this._switchOff.Name = "_switchOff";
            this._switchOff.Size = new System.Drawing.Size(13, 13);
            this._switchOff.State = BEMN.Forms.LedState.Off;
            this._switchOff.TabIndex = 48;
            // 
            // _startOscBtn
            // 
            this._startOscBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._startOscBtn.Location = new System.Drawing.Point(6, 271);
            this._startOscBtn.Name = "_startOscBtn";
            this._startOscBtn.Size = new System.Drawing.Size(304, 23);
            this._startOscBtn.TabIndex = 14;
            this._startOscBtn.Text = "Пуск осциллографа";
            this._startOscBtn.UseVisualStyleBackColor = true;
            this._startOscBtn.Click += new System.EventHandler(this._startOscBtnClick);
            // 
            // _availabilityFaultSystemJournal
            // 
            this._availabilityFaultSystemJournal.Location = new System.Drawing.Point(6, 134);
            this._availabilityFaultSystemJournal.Name = "_availabilityFaultSystemJournal";
            this._availabilityFaultSystemJournal.Size = new System.Drawing.Size(13, 13);
            this._availabilityFaultSystemJournal.State = BEMN.Forms.LedState.Off;
            this._availabilityFaultSystemJournal.TabIndex = 13;
            // 
            // _newRecordOscJournal
            // 
            this._newRecordOscJournal.Location = new System.Drawing.Point(6, 111);
            this._newRecordOscJournal.Name = "_newRecordOscJournal";
            this._newRecordOscJournal.Size = new System.Drawing.Size(13, 13);
            this._newRecordOscJournal.State = BEMN.Forms.LedState.Off;
            this._newRecordOscJournal.TabIndex = 12;
            // 
            // _newRecordAlarmJournal
            // 
            this._newRecordAlarmJournal.Location = new System.Drawing.Point(6, 88);
            this._newRecordAlarmJournal.Name = "_newRecordAlarmJournal";
            this._newRecordAlarmJournal.Size = new System.Drawing.Size(13, 13);
            this._newRecordAlarmJournal.State = BEMN.Forms.LedState.Off;
            this._newRecordAlarmJournal.TabIndex = 11;
            // 
            // _newRecordSystemJournal
            // 
            this._newRecordSystemJournal.Location = new System.Drawing.Point(6, 65);
            this._newRecordSystemJournal.Name = "_newRecordSystemJournal";
            this._newRecordSystemJournal.Size = new System.Drawing.Size(13, 13);
            this._newRecordSystemJournal.State = BEMN.Forms.LedState.Off;
            this._newRecordSystemJournal.TabIndex = 10;
            // 
            // _resetAnButton
            // 
            this._resetAnButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._resetAnButton.Location = new System.Drawing.Point(6, 242);
            this._resetAnButton.Name = "_resetAnButton";
            this._resetAnButton.Size = new System.Drawing.Size(304, 23);
            this._resetAnButton.TabIndex = 9;
            this._resetAnButton.Text = "Сброс блинкеров";
            this._resetAnButton.UseVisualStyleBackColor = true;
            this._resetAnButton.Click += new System.EventHandler(this._resetAnButton_Click);
            // 
            // _resetAvailabilityFaultSystemJournalButton
            // 
            this._resetAvailabilityFaultSystemJournalButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._resetAvailabilityFaultSystemJournalButton.Location = new System.Drawing.Point(235, 129);
            this._resetAvailabilityFaultSystemJournalButton.Name = "_resetAvailabilityFaultSystemJournalButton";
            this._resetAvailabilityFaultSystemJournalButton.Size = new System.Drawing.Size(75, 23);
            this._resetAvailabilityFaultSystemJournalButton.TabIndex = 8;
            this._resetAvailabilityFaultSystemJournalButton.Text = "Сбросить";
            this._resetAvailabilityFaultSystemJournalButton.UseVisualStyleBackColor = true;
            this._resetAvailabilityFaultSystemJournalButton.Click += new System.EventHandler(this._resetAvailabilityFaultSystemJournalButton_Click);
            // 
            // _resetOscJournalButton
            // 
            this._resetOscJournalButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._resetOscJournalButton.Location = new System.Drawing.Point(235, 106);
            this._resetOscJournalButton.Name = "_resetOscJournalButton";
            this._resetOscJournalButton.Size = new System.Drawing.Size(75, 23);
            this._resetOscJournalButton.TabIndex = 7;
            this._resetOscJournalButton.Text = "Сбросить";
            this._resetOscJournalButton.UseVisualStyleBackColor = true;
            this._resetOscJournalButton.Click += new System.EventHandler(this._resetOscJournalButton_Click);
            // 
            // _resetAlarmJournalButton
            // 
            this._resetAlarmJournalButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._resetAlarmJournalButton.Location = new System.Drawing.Point(235, 83);
            this._resetAlarmJournalButton.Name = "_resetAlarmJournalButton";
            this._resetAlarmJournalButton.Size = new System.Drawing.Size(75, 23);
            this._resetAlarmJournalButton.TabIndex = 6;
            this._resetAlarmJournalButton.Text = "Сбросить";
            this._resetAlarmJournalButton.UseVisualStyleBackColor = true;
            this._resetAlarmJournalButton.Click += new System.EventHandler(this._resetAlarmJournalButton_Click);
            // 
            // _resetSystemJournalButton
            // 
            this._resetSystemJournalButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._resetSystemJournalButton.Location = new System.Drawing.Point(235, 60);
            this._resetSystemJournalButton.Name = "_resetSystemJournalButton";
            this._resetSystemJournalButton.Size = new System.Drawing.Size(75, 23);
            this._resetSystemJournalButton.TabIndex = 5;
            this._resetSystemJournalButton.Text = "Сбросить";
            this._resetSystemJournalButton.UseVisualStyleBackColor = true;
            this._resetSystemJournalButton.Click += new System.EventHandler(this._resetSystemJournalButton_Click);
            // 
            // label227
            // 
            this.label227.AutoSize = true;
            this.label227.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label227.Location = new System.Drawing.Point(30, 134);
            this.label227.Name = "label227";
            this.label227.Size = new System.Drawing.Size(166, 13);
            this.label227.TabIndex = 3;
            this.label227.Text = "Наличие неисправности по ЖС";
            // 
            // label225
            // 
            this.label225.AutoSize = true;
            this.label225.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label225.Location = new System.Drawing.Point(30, 111);
            this.label225.Name = "label225";
            this.label225.Size = new System.Drawing.Size(200, 13);
            this.label225.TabIndex = 2;
            this.label225.Text = "Новая запись журнала осциллографа";
            // 
            // label226
            // 
            this.label226.AutoSize = true;
            this.label226.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label226.Location = new System.Drawing.Point(30, 88);
            this.label226.Name = "label226";
            this.label226.Size = new System.Drawing.Size(172, 13);
            this.label226.TabIndex = 1;
            this.label226.Text = "Новая запись в журнале аварий";
            // 
            // label231
            // 
            this.label231.AutoSize = true;
            this.label231.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label231.Location = new System.Drawing.Point(30, 65);
            this.label231.Name = "label231";
            this.label231.Size = new System.Drawing.Size(181, 13);
            this.label231.TabIndex = 0;
            this.label231.Text = "Новая запись в журнале системы";
            // 
            // _discretTabPage1
            // 
            this._discretTabPage1.Controls.Add(this.groupBox49);
            this._discretTabPage1.Controls.Add(this.groupBox38);
            this._discretTabPage1.Controls.Add(this.groupBox19);
            this._discretTabPage1.Controls.Add(this.groupBox12);
            this._discretTabPage1.Controls.Add(this._dateTimeControl);
            this._discretTabPage1.Controls.Add(this.groupBox14);
            this._discretTabPage1.Controls.Add(this.groupBox21);
            this._discretTabPage1.Controls.Add(this.groupBox10);
            this._discretTabPage1.Controls.Add(this.groupBox9);
            this._discretTabPage1.Location = new System.Drawing.Point(4, 22);
            this._discretTabPage1.Name = "_discretTabPage1";
            this._discretTabPage1.Padding = new System.Windows.Forms.Padding(3);
            this._discretTabPage1.Size = new System.Drawing.Size(933, 599);
            this._discretTabPage1.TabIndex = 1;
            this._discretTabPage1.Text = "Основные сигналы";
            this._discretTabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox49
            // 
            this.groupBox49.Controls.Add(this.label347);
            this.groupBox49.Controls.Add(this._faultDisable2);
            this.groupBox49.Controls.Add(this.label346);
            this.groupBox49.Controls.Add(this.label345);
            this.groupBox49.Controls.Add(this.label311);
            this.groupBox49.Controls.Add(this.label98);
            this.groupBox49.Controls.Add(this.label22);
            this.groupBox49.Controls.Add(this._faultDisable1);
            this.groupBox49.Controls.Add(this._faultSwithON);
            this.groupBox49.Controls.Add(this._faultManage);
            this.groupBox49.Controls.Add(this._faultBlockCon);
            this.groupBox49.Controls.Add(this._faultOut);
            this.groupBox49.Location = new System.Drawing.Point(575, 208);
            this.groupBox49.Name = "groupBox49";
            this.groupBox49.Size = new System.Drawing.Size(179, 146);
            this.groupBox49.TabIndex = 48;
            this.groupBox49.TabStop = false;
            this.groupBox49.Text = "Неисправности выключателя";
            // 
            // label347
            // 
            this.label347.AutoSize = true;
            this.label347.Location = new System.Drawing.Point(25, 118);
            this.label347.Name = "label347";
            this.label347.Size = new System.Drawing.Size(105, 13);
            this.label347.TabIndex = 13;
            this.label347.Text = "Цепи отключения 2";
            // 
            // _faultDisable2
            // 
            this._faultDisable2.Location = new System.Drawing.Point(6, 117);
            this._faultDisable2.Name = "_faultDisable2";
            this._faultDisable2.Size = new System.Drawing.Size(13, 13);
            this._faultDisable2.State = BEMN.Forms.LedState.Off;
            this._faultDisable2.TabIndex = 12;
            // 
            // label346
            // 
            this.label346.AutoSize = true;
            this.label346.Location = new System.Drawing.Point(25, 98);
            this.label346.Name = "label346";
            this.label346.Size = new System.Drawing.Size(105, 13);
            this.label346.TabIndex = 11;
            this.label346.Text = "Цепи отключения 1";
            // 
            // label345
            // 
            this.label345.AutoSize = true;
            this.label345.Location = new System.Drawing.Point(25, 79);
            this.label345.Name = "label345";
            this.label345.Size = new System.Drawing.Size(91, 13);
            this.label345.TabIndex = 10;
            this.label345.Text = "Цепи включения";
            // 
            // label311
            // 
            this.label311.AutoSize = true;
            this.label311.Location = new System.Drawing.Point(25, 60);
            this.label311.Name = "label311";
            this.label311.Size = new System.Drawing.Size(69, 13);
            this.label311.TabIndex = 9;
            this.label311.Text = "Управления";
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(25, 41);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(105, 13);
            this.label98.TabIndex = 7;
            this.label98.Text = "По блок-контактам";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(25, 22);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(55, 13);
            this.label22.TabIndex = 6;
            this.label22.Text = "Внешняя ";
            // 
            // _faultDisable1
            // 
            this._faultDisable1.Location = new System.Drawing.Point(6, 98);
            this._faultDisable1.Name = "_faultDisable1";
            this._faultDisable1.Size = new System.Drawing.Size(13, 13);
            this._faultDisable1.State = BEMN.Forms.LedState.Off;
            this._faultDisable1.TabIndex = 5;
            // 
            // _faultSwithON
            // 
            this._faultSwithON.Location = new System.Drawing.Point(6, 79);
            this._faultSwithON.Name = "_faultSwithON";
            this._faultSwithON.Size = new System.Drawing.Size(13, 13);
            this._faultSwithON.State = BEMN.Forms.LedState.Off;
            this._faultSwithON.TabIndex = 4;
            // 
            // _faultManage
            // 
            this._faultManage.Location = new System.Drawing.Point(6, 60);
            this._faultManage.Name = "_faultManage";
            this._faultManage.Size = new System.Drawing.Size(13, 13);
            this._faultManage.State = BEMN.Forms.LedState.Off;
            this._faultManage.TabIndex = 3;
            // 
            // _faultBlockCon
            // 
            this._faultBlockCon.Location = new System.Drawing.Point(6, 41);
            this._faultBlockCon.Name = "_faultBlockCon";
            this._faultBlockCon.Size = new System.Drawing.Size(13, 13);
            this._faultBlockCon.State = BEMN.Forms.LedState.Off;
            this._faultBlockCon.TabIndex = 1;
            // 
            // _faultOut
            // 
            this._faultOut.Location = new System.Drawing.Point(6, 22);
            this._faultOut.Name = "_faultOut";
            this._faultOut.Size = new System.Drawing.Size(13, 13);
            this._faultOut.State = BEMN.Forms.LedState.Off;
            this._faultOut.TabIndex = 0;
            // 
            // groupBox38
            // 
            this.groupBox38.Controls.Add(this.groupBox39);
            this.groupBox38.Controls.Add(this._fSpl5);
            this.groupBox38.Controls.Add(this.label320);
            this.groupBox38.Controls.Add(this.label319);
            this.groupBox38.Location = new System.Drawing.Point(445, 208);
            this.groupBox38.Name = "groupBox38";
            this.groupBox38.Size = new System.Drawing.Size(122, 146);
            this.groupBox38.TabIndex = 47;
            this.groupBox38.TabStop = false;
            this.groupBox38.Text = "Ошибки СПЛ";
            // 
            // groupBox39
            // 
            this.groupBox39.Controls.Add(this._fSpl1);
            this.groupBox39.Controls.Add(this.label315);
            this.groupBox39.Controls.Add(this.label316);
            this.groupBox39.Controls.Add(this._fSpl2);
            this.groupBox39.Controls.Add(this.label317);
            this.groupBox39.Controls.Add(this._fSpl3);
            this.groupBox39.Location = new System.Drawing.Point(6, 44);
            this.groupBox39.Name = "groupBox39";
            this.groupBox39.Size = new System.Drawing.Size(110, 83);
            this.groupBox39.TabIndex = 32;
            this.groupBox39.TabStop = false;
            this.groupBox39.Text = "Ошибки CRC";
            // 
            // _fSpl1
            // 
            this._fSpl1.Location = new System.Drawing.Point(6, 19);
            this._fSpl1.Name = "_fSpl1";
            this._fSpl1.Size = new System.Drawing.Size(13, 13);
            this._fSpl1.State = BEMN.Forms.LedState.Off;
            this._fSpl1.TabIndex = 25;
            // 
            // label315
            // 
            this.label315.AutoSize = true;
            this.label315.Location = new System.Drawing.Point(25, 19);
            this.label315.Name = "label315";
            this.label315.Size = new System.Drawing.Size(54, 13);
            this.label315.TabIndex = 24;
            this.label315.Text = "Констант";
            // 
            // label316
            // 
            this.label316.AutoSize = true;
            this.label316.Location = new System.Drawing.Point(25, 38);
            this.label316.Name = "label316";
            this.label316.Size = new System.Drawing.Size(70, 13);
            this.label316.TabIndex = 26;
            this.label316.Text = "Разрешения";
            // 
            // _fSpl2
            // 
            this._fSpl2.Location = new System.Drawing.Point(6, 38);
            this._fSpl2.Name = "_fSpl2";
            this._fSpl2.Size = new System.Drawing.Size(13, 13);
            this._fSpl2.State = BEMN.Forms.LedState.Off;
            this._fSpl2.TabIndex = 27;
            // 
            // label317
            // 
            this.label317.AutoSize = true;
            this.label317.Location = new System.Drawing.Point(25, 57);
            this.label317.Name = "label317";
            this.label317.Size = new System.Drawing.Size(68, 13);
            this.label317.TabIndex = 28;
            this.label317.Text = "Программы";
            // 
            // _fSpl3
            // 
            this._fSpl3.Location = new System.Drawing.Point(6, 57);
            this._fSpl3.Name = "_fSpl3";
            this._fSpl3.Size = new System.Drawing.Size(13, 13);
            this._fSpl3.State = BEMN.Forms.LedState.Off;
            this._fSpl3.TabIndex = 29;
            // 
            // _fSpl5
            // 
            this._fSpl5.Location = new System.Drawing.Point(4, 19);
            this._fSpl5.Name = "_fSpl5";
            this._fSpl5.Size = new System.Drawing.Size(13, 13);
            this._fSpl5.State = BEMN.Forms.LedState.Off;
            this._fSpl5.TabIndex = 31;
            // 
            // label320
            // 
            this.label320.AutoSize = true;
            this.label320.Location = new System.Drawing.Point(17, 28);
            this.label320.Name = "label320";
            this.label320.Size = new System.Drawing.Size(66, 13);
            this.label320.TabIndex = 30;
            this.label320.Text = "программы";
            // 
            // label319
            // 
            this.label319.AutoSize = true;
            this.label319.Location = new System.Drawing.Point(16, 14);
            this.label319.Name = "label319";
            this.label319.Size = new System.Drawing.Size(105, 13);
            this.label319.TabIndex = 30;
            this.label319.Text = "В ходе выполнения";
            // 
            // groupBox19
            // 
            this.groupBox19.BackColor = System.Drawing.Color.Transparent;
            this.groupBox19.Controls.Add(this._faultModule6);
            this.groupBox19.Controls.Add(this.label18);
            this.groupBox19.Controls.Add(this.label344);
            this.groupBox19.Controls.Add(this._faultSwitchOff);
            this.groupBox19.Controls.Add(this._faultAlarmJournal);
            this.groupBox19.Controls.Add(this.label192);
            this.groupBox19.Controls.Add(this._faultOsc);
            this.groupBox19.Controls.Add(this.label193);
            this.groupBox19.Controls.Add(this._faultModule5);
            this.groupBox19.Controls.Add(this._faultSystemJournal);
            this.groupBox19.Controls.Add(this.label200);
            this.groupBox19.Controls.Add(this.label194);
            this.groupBox19.Controls.Add(this._faultGroupsOfSetpoints);
            this.groupBox19.Controls.Add(this.label201);
            this.groupBox19.Controls.Add(this._faultModule4);
            this.groupBox19.Controls.Add(this._faultLogic);
            this.groupBox19.Controls.Add(this.label79);
            this.groupBox19.Controls.Add(this._faultSetpoints);
            this.groupBox19.Controls.Add(this.label202);
            this.groupBox19.Controls.Add(this.label195);
            this.groupBox19.Controls.Add(this._faultPass);
            this.groupBox19.Controls.Add(this.label203);
            this.groupBox19.Controls.Add(this._faultModule3);
            this.groupBox19.Controls.Add(this.label196);
            this.groupBox19.Controls.Add(this._faultSoftware);
            this.groupBox19.Controls.Add(this.label205);
            this.groupBox19.Controls.Add(this._faultModule2);
            this.groupBox19.Controls.Add(this._faultHardware);
            this.groupBox19.Controls.Add(this.label206);
            this.groupBox19.Controls.Add(this.label197);
            this.groupBox19.Controls.Add(this._faultModule1);
            this.groupBox19.Controls.Add(this.label198);
            this.groupBox19.Location = new System.Drawing.Point(213, 208);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(226, 175);
            this.groupBox19.TabIndex = 46;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "Неисправности";
            // 
            // label344
            // 
            this.label344.AutoSize = true;
            this.label344.Location = new System.Drawing.Point(25, 57);
            this.label344.Name = "label344";
            this.label344.Size = new System.Drawing.Size(76, 13);
            this.label344.TabIndex = 35;
            this.label344.Text = "Выключателя";
            // 
            // _faultSwitchOff
            // 
            this._faultSwitchOff.Location = new System.Drawing.Point(6, 57);
            this._faultSwitchOff.Name = "_faultSwitchOff";
            this._faultSwitchOff.Size = new System.Drawing.Size(13, 13);
            this._faultSwitchOff.State = BEMN.Forms.LedState.Off;
            this._faultSwitchOff.TabIndex = 34;
            // 
            // _faultAlarmJournal
            // 
            this._faultAlarmJournal.Location = new System.Drawing.Point(117, 38);
            this._faultAlarmJournal.Name = "_faultAlarmJournal";
            this._faultAlarmJournal.Size = new System.Drawing.Size(13, 13);
            this._faultAlarmJournal.State = BEMN.Forms.LedState.Off;
            this._faultAlarmJournal.TabIndex = 29;
            // 
            // label192
            // 
            this.label192.AutoSize = true;
            this.label192.Location = new System.Drawing.Point(136, 38);
            this.label192.Name = "label192";
            this.label192.Size = new System.Drawing.Size(25, 13);
            this.label192.TabIndex = 28;
            this.label192.Text = "ЖА";
            // 
            // _faultOsc
            // 
            this._faultOsc.Location = new System.Drawing.Point(6, 152);
            this._faultOsc.Name = "_faultOsc";
            this._faultOsc.Size = new System.Drawing.Size(13, 13);
            this._faultOsc.State = BEMN.Forms.LedState.Off;
            this._faultOsc.TabIndex = 27;
            // 
            // label193
            // 
            this.label193.AutoSize = true;
            this.label193.Location = new System.Drawing.Point(25, 152);
            this.label193.Name = "label193";
            this.label193.Size = new System.Drawing.Size(84, 13);
            this.label193.TabIndex = 26;
            this.label193.Text = "Осциллограмы";
            // 
            // _faultModule5
            // 
            this._faultModule5.Location = new System.Drawing.Point(117, 133);
            this._faultModule5.Name = "_faultModule5";
            this._faultModule5.Size = new System.Drawing.Size(13, 13);
            this._faultModule5.State = BEMN.Forms.LedState.Off;
            this._faultModule5.TabIndex = 25;
            // 
            // _faultSystemJournal
            // 
            this._faultSystemJournal.Location = new System.Drawing.Point(117, 19);
            this._faultSystemJournal.Name = "_faultSystemJournal";
            this._faultSystemJournal.Size = new System.Drawing.Size(13, 13);
            this._faultSystemJournal.State = BEMN.Forms.LedState.Off;
            this._faultSystemJournal.TabIndex = 13;
            // 
            // label200
            // 
            this.label200.AutoSize = true;
            this.label200.Location = new System.Drawing.Point(136, 19);
            this.label200.Name = "label200";
            this.label200.Size = new System.Drawing.Size(25, 13);
            this.label200.TabIndex = 12;
            this.label200.Text = "ЖС";
            // 
            // label194
            // 
            this.label194.AutoSize = true;
            this.label194.Location = new System.Drawing.Point(136, 133);
            this.label194.Name = "label194";
            this.label194.Size = new System.Drawing.Size(54, 13);
            this.label194.TabIndex = 24;
            this.label194.Text = "Модуля 5";
            // 
            // _faultGroupsOfSetpoints
            // 
            this._faultGroupsOfSetpoints.Location = new System.Drawing.Point(6, 114);
            this._faultGroupsOfSetpoints.Name = "_faultGroupsOfSetpoints";
            this._faultGroupsOfSetpoints.Size = new System.Drawing.Size(13, 13);
            this._faultGroupsOfSetpoints.State = BEMN.Forms.LedState.Off;
            this._faultGroupsOfSetpoints.TabIndex = 11;
            // 
            // label201
            // 
            this.label201.AutoSize = true;
            this.label201.Location = new System.Drawing.Point(25, 114);
            this.label201.Name = "label201";
            this.label201.Size = new System.Drawing.Size(79, 13);
            this.label201.TabIndex = 10;
            this.label201.Text = "Групп уставок";
            // 
            // _faultModule4
            // 
            this._faultModule4.Location = new System.Drawing.Point(117, 114);
            this._faultModule4.Name = "_faultModule4";
            this._faultModule4.Size = new System.Drawing.Size(13, 13);
            this._faultModule4.State = BEMN.Forms.LedState.Off;
            this._faultModule4.TabIndex = 23;
            // 
            // _faultLogic
            // 
            this._faultLogic.Location = new System.Drawing.Point(6, 76);
            this._faultLogic.Name = "_faultLogic";
            this._faultLogic.Size = new System.Drawing.Size(13, 13);
            this._faultLogic.State = BEMN.Forms.LedState.Off;
            this._faultLogic.TabIndex = 9;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(25, 76);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(44, 13);
            this.label79.TabIndex = 8;
            this.label79.Text = "Логики";
            // 
            // _faultSetpoints
            // 
            this._faultSetpoints.Location = new System.Drawing.Point(6, 95);
            this._faultSetpoints.Name = "_faultSetpoints";
            this._faultSetpoints.Size = new System.Drawing.Size(13, 13);
            this._faultSetpoints.State = BEMN.Forms.LedState.Off;
            this._faultSetpoints.TabIndex = 9;
            // 
            // label202
            // 
            this.label202.AutoSize = true;
            this.label202.Location = new System.Drawing.Point(25, 95);
            this.label202.Name = "label202";
            this.label202.Size = new System.Drawing.Size(50, 13);
            this.label202.TabIndex = 8;
            this.label202.Text = "Уставок";
            // 
            // label195
            // 
            this.label195.AutoSize = true;
            this.label195.Location = new System.Drawing.Point(136, 114);
            this.label195.Name = "label195";
            this.label195.Size = new System.Drawing.Size(54, 13);
            this.label195.TabIndex = 22;
            this.label195.Text = "Модуля 4";
            // 
            // _faultPass
            // 
            this._faultPass.Location = new System.Drawing.Point(6, 133);
            this._faultPass.Name = "_faultPass";
            this._faultPass.Size = new System.Drawing.Size(13, 13);
            this._faultPass.State = BEMN.Forms.LedState.Off;
            this._faultPass.TabIndex = 7;
            // 
            // label203
            // 
            this.label203.AutoSize = true;
            this.label203.Location = new System.Drawing.Point(25, 133);
            this.label203.Name = "label203";
            this.label203.Size = new System.Drawing.Size(45, 13);
            this.label203.TabIndex = 6;
            this.label203.Text = "Пароля";
            // 
            // _faultModule3
            // 
            this._faultModule3.Location = new System.Drawing.Point(117, 95);
            this._faultModule3.Name = "_faultModule3";
            this._faultModule3.Size = new System.Drawing.Size(13, 13);
            this._faultModule3.State = BEMN.Forms.LedState.Off;
            this._faultModule3.TabIndex = 21;
            // 
            // label196
            // 
            this.label196.AutoSize = true;
            this.label196.Location = new System.Drawing.Point(136, 95);
            this.label196.Name = "label196";
            this.label196.Size = new System.Drawing.Size(54, 13);
            this.label196.TabIndex = 20;
            this.label196.Text = "Модуля 3";
            // 
            // _faultSoftware
            // 
            this._faultSoftware.Location = new System.Drawing.Point(6, 38);
            this._faultSoftware.Name = "_faultSoftware";
            this._faultSoftware.Size = new System.Drawing.Size(13, 13);
            this._faultSoftware.State = BEMN.Forms.LedState.Off;
            this._faultSoftware.TabIndex = 3;
            // 
            // label205
            // 
            this.label205.AutoSize = true;
            this.label205.Location = new System.Drawing.Point(25, 38);
            this.label205.Name = "label205";
            this.label205.Size = new System.Drawing.Size(78, 13);
            this.label205.TabIndex = 2;
            this.label205.Text = "Программная";
            // 
            // _faultModule2
            // 
            this._faultModule2.Location = new System.Drawing.Point(117, 76);
            this._faultModule2.Name = "_faultModule2";
            this._faultModule2.Size = new System.Drawing.Size(13, 13);
            this._faultModule2.State = BEMN.Forms.LedState.Off;
            this._faultModule2.TabIndex = 19;
            // 
            // _faultHardware
            // 
            this._faultHardware.Location = new System.Drawing.Point(6, 19);
            this._faultHardware.Name = "_faultHardware";
            this._faultHardware.Size = new System.Drawing.Size(13, 13);
            this._faultHardware.State = BEMN.Forms.LedState.Off;
            this._faultHardware.TabIndex = 1;
            // 
            // label206
            // 
            this.label206.AutoSize = true;
            this.label206.Location = new System.Drawing.Point(25, 19);
            this.label206.Name = "label206";
            this.label206.Size = new System.Drawing.Size(67, 13);
            this.label206.TabIndex = 0;
            this.label206.Text = "Аппаратная";
            // 
            // label197
            // 
            this.label197.AutoSize = true;
            this.label197.Location = new System.Drawing.Point(136, 76);
            this.label197.Name = "label197";
            this.label197.Size = new System.Drawing.Size(54, 13);
            this.label197.TabIndex = 18;
            this.label197.Text = "Модуля 2";
            // 
            // _faultModule1
            // 
            this._faultModule1.Location = new System.Drawing.Point(117, 57);
            this._faultModule1.Name = "_faultModule1";
            this._faultModule1.Size = new System.Drawing.Size(13, 13);
            this._faultModule1.State = BEMN.Forms.LedState.Off;
            this._faultModule1.TabIndex = 17;
            // 
            // label198
            // 
            this.label198.AutoSize = true;
            this.label198.Location = new System.Drawing.Point(136, 57);
            this.label198.Name = "label198";
            this.label198.Size = new System.Drawing.Size(54, 13);
            this.label198.TabIndex = 16;
            this.label198.Text = "Модуля 1";
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this._vz16);
            this.groupBox12.Controls.Add(this.label111);
            this.groupBox12.Controls.Add(this._vz15);
            this.groupBox12.Controls.Add(this.label112);
            this.groupBox12.Controls.Add(this._vz14);
            this.groupBox12.Controls.Add(this.label113);
            this.groupBox12.Controls.Add(this._vz13);
            this.groupBox12.Controls.Add(this.label114);
            this.groupBox12.Controls.Add(this._vz12);
            this.groupBox12.Controls.Add(this.label115);
            this.groupBox12.Controls.Add(this._vz11);
            this.groupBox12.Controls.Add(this.label116);
            this.groupBox12.Controls.Add(this._vz10);
            this.groupBox12.Controls.Add(this.label117);
            this.groupBox12.Controls.Add(this._vz9);
            this.groupBox12.Controls.Add(this.label118);
            this.groupBox12.Controls.Add(this._vz8);
            this.groupBox12.Controls.Add(this.label119);
            this.groupBox12.Controls.Add(this._vz7);
            this.groupBox12.Controls.Add(this.label120);
            this.groupBox12.Controls.Add(this._vz6);
            this.groupBox12.Controls.Add(this.label121);
            this.groupBox12.Controls.Add(this._vz5);
            this.groupBox12.Controls.Add(this.label122);
            this.groupBox12.Controls.Add(this._vz4);
            this.groupBox12.Controls.Add(this.label123);
            this.groupBox12.Controls.Add(this._vz3);
            this.groupBox12.Controls.Add(this.label124);
            this.groupBox12.Controls.Add(this._vz2);
            this.groupBox12.Controls.Add(this.label125);
            this.groupBox12.Controls.Add(this._vz1);
            this.groupBox12.Controls.Add(this.label126);
            this.groupBox12.Location = new System.Drawing.Point(720, 3);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(172, 175);
            this.groupBox12.TabIndex = 45;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Внешние защиты";
            // 
            // _vz16
            // 
            this._vz16.Location = new System.Drawing.Point(84, 152);
            this._vz16.Name = "_vz16";
            this._vz16.Size = new System.Drawing.Size(13, 13);
            this._vz16.State = BEMN.Forms.LedState.Off;
            this._vz16.TabIndex = 31;
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Location = new System.Drawing.Point(103, 152);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(56, 13);
            this.label111.TabIndex = 30;
            this.label111.Text = "ВНЕШ. 16";
            // 
            // _vz15
            // 
            this._vz15.Location = new System.Drawing.Point(84, 133);
            this._vz15.Name = "_vz15";
            this._vz15.Size = new System.Drawing.Size(13, 13);
            this._vz15.State = BEMN.Forms.LedState.Off;
            this._vz15.TabIndex = 29;
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Location = new System.Drawing.Point(103, 133);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(56, 13);
            this.label112.TabIndex = 28;
            this.label112.Text = "ВНЕШ. 15";
            // 
            // _vz14
            // 
            this._vz14.Location = new System.Drawing.Point(84, 114);
            this._vz14.Name = "_vz14";
            this._vz14.Size = new System.Drawing.Size(13, 13);
            this._vz14.State = BEMN.Forms.LedState.Off;
            this._vz14.TabIndex = 27;
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Location = new System.Drawing.Point(103, 114);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(56, 13);
            this.label113.TabIndex = 26;
            this.label113.Text = "ВНЕШ. 14";
            // 
            // _vz13
            // 
            this._vz13.Location = new System.Drawing.Point(84, 95);
            this._vz13.Name = "_vz13";
            this._vz13.Size = new System.Drawing.Size(13, 13);
            this._vz13.State = BEMN.Forms.LedState.Off;
            this._vz13.TabIndex = 25;
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Location = new System.Drawing.Point(103, 95);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(56, 13);
            this.label114.TabIndex = 24;
            this.label114.Text = "ВНЕШ. 13";
            // 
            // _vz12
            // 
            this._vz12.Location = new System.Drawing.Point(84, 76);
            this._vz12.Name = "_vz12";
            this._vz12.Size = new System.Drawing.Size(13, 13);
            this._vz12.State = BEMN.Forms.LedState.Off;
            this._vz12.TabIndex = 23;
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Location = new System.Drawing.Point(103, 76);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(56, 13);
            this.label115.TabIndex = 22;
            this.label115.Text = "ВНЕШ. 12";
            // 
            // _vz11
            // 
            this._vz11.Location = new System.Drawing.Point(84, 57);
            this._vz11.Name = "_vz11";
            this._vz11.Size = new System.Drawing.Size(13, 13);
            this._vz11.State = BEMN.Forms.LedState.Off;
            this._vz11.TabIndex = 21;
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Location = new System.Drawing.Point(103, 57);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(56, 13);
            this.label116.TabIndex = 20;
            this.label116.Text = "ВНЕШ. 11";
            // 
            // _vz10
            // 
            this._vz10.Location = new System.Drawing.Point(84, 38);
            this._vz10.Name = "_vz10";
            this._vz10.Size = new System.Drawing.Size(13, 13);
            this._vz10.State = BEMN.Forms.LedState.Off;
            this._vz10.TabIndex = 19;
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Location = new System.Drawing.Point(103, 38);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(56, 13);
            this.label117.TabIndex = 18;
            this.label117.Text = "ВНЕШ. 10";
            // 
            // _vz9
            // 
            this._vz9.Location = new System.Drawing.Point(84, 19);
            this._vz9.Name = "_vz9";
            this._vz9.Size = new System.Drawing.Size(13, 13);
            this._vz9.State = BEMN.Forms.LedState.Off;
            this._vz9.TabIndex = 17;
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Location = new System.Drawing.Point(103, 19);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(50, 13);
            this.label118.TabIndex = 16;
            this.label118.Text = "ВНЕШ. 9";
            // 
            // _vz8
            // 
            this._vz8.Location = new System.Drawing.Point(6, 152);
            this._vz8.Name = "_vz8";
            this._vz8.Size = new System.Drawing.Size(13, 13);
            this._vz8.State = BEMN.Forms.LedState.Off;
            this._vz8.TabIndex = 15;
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Location = new System.Drawing.Point(25, 152);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(50, 13);
            this.label119.TabIndex = 14;
            this.label119.Text = "ВНЕШ. 8";
            // 
            // _vz7
            // 
            this._vz7.Location = new System.Drawing.Point(6, 133);
            this._vz7.Name = "_vz7";
            this._vz7.Size = new System.Drawing.Size(13, 13);
            this._vz7.State = BEMN.Forms.LedState.Off;
            this._vz7.TabIndex = 13;
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Location = new System.Drawing.Point(25, 133);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(50, 13);
            this.label120.TabIndex = 12;
            this.label120.Text = "ВНЕШ. 7";
            // 
            // _vz6
            // 
            this._vz6.Location = new System.Drawing.Point(6, 114);
            this._vz6.Name = "_vz6";
            this._vz6.Size = new System.Drawing.Size(13, 13);
            this._vz6.State = BEMN.Forms.LedState.Off;
            this._vz6.TabIndex = 11;
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Location = new System.Drawing.Point(25, 114);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(50, 13);
            this.label121.TabIndex = 10;
            this.label121.Text = "ВНЕШ. 6";
            // 
            // _vz5
            // 
            this._vz5.Location = new System.Drawing.Point(6, 95);
            this._vz5.Name = "_vz5";
            this._vz5.Size = new System.Drawing.Size(13, 13);
            this._vz5.State = BEMN.Forms.LedState.Off;
            this._vz5.TabIndex = 9;
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Location = new System.Drawing.Point(25, 95);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(50, 13);
            this.label122.TabIndex = 8;
            this.label122.Text = "ВНЕШ. 5";
            // 
            // _vz4
            // 
            this._vz4.Location = new System.Drawing.Point(6, 76);
            this._vz4.Name = "_vz4";
            this._vz4.Size = new System.Drawing.Size(13, 13);
            this._vz4.State = BEMN.Forms.LedState.Off;
            this._vz4.TabIndex = 7;
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Location = new System.Drawing.Point(25, 76);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(50, 13);
            this.label123.TabIndex = 6;
            this.label123.Text = "ВНЕШ. 4";
            // 
            // _vz3
            // 
            this._vz3.Location = new System.Drawing.Point(6, 57);
            this._vz3.Name = "_vz3";
            this._vz3.Size = new System.Drawing.Size(13, 13);
            this._vz3.State = BEMN.Forms.LedState.Off;
            this._vz3.TabIndex = 5;
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Location = new System.Drawing.Point(25, 57);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(50, 13);
            this.label124.TabIndex = 4;
            this.label124.Text = "ВНЕШ. 3";
            // 
            // _vz2
            // 
            this._vz2.Location = new System.Drawing.Point(6, 38);
            this._vz2.Name = "_vz2";
            this._vz2.Size = new System.Drawing.Size(13, 13);
            this._vz2.State = BEMN.Forms.LedState.Off;
            this._vz2.TabIndex = 3;
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Location = new System.Drawing.Point(25, 38);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(50, 13);
            this.label125.TabIndex = 2;
            this.label125.Text = "ВНЕШ. 2";
            // 
            // _vz1
            // 
            this._vz1.Location = new System.Drawing.Point(6, 19);
            this._vz1.Name = "_vz1";
            this._vz1.Size = new System.Drawing.Size(13, 13);
            this._vz1.State = BEMN.Forms.LedState.Off;
            this._vz1.TabIndex = 1;
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Location = new System.Drawing.Point(25, 19);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(50, 13);
            this.label126.TabIndex = 0;
            this.label126.Text = "ВНЕШ. 1";
            // 
            // _dateTimeControl
            // 
            this._dateTimeControl.DateTime = dateTimeStruct1;
            this._dateTimeControl.Location = new System.Drawing.Point(7, 207);
            this._dateTimeControl.Name = "_dateTimeControl";
            this._dateTimeControl.Size = new System.Drawing.Size(201, 166);
            this._dateTimeControl.TabIndex = 44;
            this._dateTimeControl.TimeChanged += new System.Action(this.dateTimeControl_TimeChanged);
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this._fault);
            this.groupBox14.Controls.Add(this.label210);
            this.groupBox14.Controls.Add(this._faultOff);
            this.groupBox14.Controls.Add(this.label21);
            this.groupBox14.Controls.Add(this._acceleration);
            this.groupBox14.Controls.Add(this._auto4Led);
            this.groupBox14.Controls.Add(this._signaling);
            this.groupBox14.Controls.Add(this._auto2Led);
            this.groupBox14.Location = new System.Drawing.Point(760, 208);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(135, 103);
            this.groupBox14.TabIndex = 24;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Общие сигналы";
            // 
            // _fault
            // 
            this._fault.Location = new System.Drawing.Point(6, 19);
            this._fault.Name = "_fault";
            this._fault.Size = new System.Drawing.Size(13, 13);
            this._fault.State = BEMN.Forms.LedState.Off;
            this._fault.TabIndex = 37;
            // 
            // label210
            // 
            this.label210.AutoSize = true;
            this.label210.Location = new System.Drawing.Point(25, 19);
            this.label210.Name = "label210";
            this.label210.Size = new System.Drawing.Size(86, 13);
            this.label210.TabIndex = 36;
            this.label210.Text = "Неисправность";
            // 
            // _faultOff
            // 
            this._faultOff.Location = new System.Drawing.Point(6, 76);
            this._faultOff.Name = "_faultOff";
            this._faultOff.Size = new System.Drawing.Size(13, 13);
            this._faultOff.State = BEMN.Forms.LedState.Off;
            this._faultOff.TabIndex = 35;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(25, 76);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(98, 13);
            this.label21.TabIndex = 34;
            this.label21.Text = "Авар. отключение";
            // 
            // _acceleration
            // 
            this._acceleration.Location = new System.Drawing.Point(6, 57);
            this._acceleration.Name = "_acceleration";
            this._acceleration.Size = new System.Drawing.Size(13, 13);
            this._acceleration.State = BEMN.Forms.LedState.Off;
            this._acceleration.TabIndex = 31;
            // 
            // _auto4Led
            // 
            this._auto4Led.AutoSize = true;
            this._auto4Led.Location = new System.Drawing.Point(25, 57);
            this._auto4Led.Name = "_auto4Led";
            this._auto4Led.Size = new System.Drawing.Size(81, 13);
            this._auto4Led.TabIndex = 30;
            this._auto4Led.Text = "Ускор. по вкл.";
            // 
            // _signaling
            // 
            this._signaling.Location = new System.Drawing.Point(6, 38);
            this._signaling.Name = "_signaling";
            this._signaling.Size = new System.Drawing.Size(13, 13);
            this._signaling.State = BEMN.Forms.LedState.Off;
            this._signaling.TabIndex = 27;
            // 
            // _auto2Led
            // 
            this._auto2Led.AutoSize = true;
            this._auto2Led.Location = new System.Drawing.Point(25, 38);
            this._auto2Led.Name = "_auto2Led";
            this._auto2Led.Size = new System.Drawing.Size(79, 13);
            this._auto2Led.TabIndex = 26;
            this._auto2Led.Text = "Сигнализация";
            // 
            // groupBox21
            // 
            this.groupBox21.Controls.Add(this._ssl48);
            this.groupBox21.Controls.Add(this._ssl40);
            this.groupBox21.Controls.Add(this.label17);
            this.groupBox21.Controls.Add(this.label1);
            this.groupBox21.Controls.Add(this._ssl47);
            this.groupBox21.Controls.Add(this._ssl39);
            this.groupBox21.Controls.Add(this.label16);
            this.groupBox21.Controls.Add(this.label2);
            this.groupBox21.Controls.Add(this._ssl46);
            this.groupBox21.Controls.Add(this._ssl38);
            this.groupBox21.Controls.Add(this.label15);
            this.groupBox21.Controls.Add(this.label3);
            this.groupBox21.Controls.Add(this._ssl45);
            this.groupBox21.Controls.Add(this._ssl37);
            this.groupBox21.Controls.Add(this.label14);
            this.groupBox21.Controls.Add(this.label4);
            this.groupBox21.Controls.Add(this._ssl44);
            this.groupBox21.Controls.Add(this._ssl36);
            this.groupBox21.Controls.Add(this.label13);
            this.groupBox21.Controls.Add(this.label5);
            this.groupBox21.Controls.Add(this._ssl43);
            this.groupBox21.Controls.Add(this._ssl35);
            this.groupBox21.Controls.Add(this.label11);
            this.groupBox21.Controls.Add(this.label6);
            this.groupBox21.Controls.Add(this._ssl42);
            this.groupBox21.Controls.Add(this._ssl34);
            this.groupBox21.Controls.Add(this.label10);
            this.groupBox21.Controls.Add(this.label7);
            this.groupBox21.Controls.Add(this._ssl41);
            this.groupBox21.Controls.Add(this.label9);
            this.groupBox21.Controls.Add(this._ssl33);
            this.groupBox21.Controls.Add(this.label8);
            this.groupBox21.Controls.Add(this._ssl32);
            this.groupBox21.Controls.Add(this.label237);
            this.groupBox21.Controls.Add(this._ssl31);
            this.groupBox21.Controls.Add(this.label238);
            this.groupBox21.Controls.Add(this._ssl30);
            this.groupBox21.Controls.Add(this.label239);
            this.groupBox21.Controls.Add(this._ssl29);
            this.groupBox21.Controls.Add(this.label240);
            this.groupBox21.Controls.Add(this._ssl28);
            this.groupBox21.Controls.Add(this.label241);
            this.groupBox21.Controls.Add(this._ssl27);
            this.groupBox21.Controls.Add(this.label242);
            this.groupBox21.Controls.Add(this._ssl26);
            this.groupBox21.Controls.Add(this.label243);
            this.groupBox21.Controls.Add(this._ssl25);
            this.groupBox21.Controls.Add(this.label244);
            this.groupBox21.Controls.Add(this._ssl24);
            this.groupBox21.Controls.Add(this.label245);
            this.groupBox21.Controls.Add(this._ssl23);
            this.groupBox21.Controls.Add(this.label246);
            this.groupBox21.Controls.Add(this._ssl22);
            this.groupBox21.Controls.Add(this.label247);
            this.groupBox21.Controls.Add(this._ssl21);
            this.groupBox21.Controls.Add(this.label248);
            this.groupBox21.Controls.Add(this._ssl20);
            this.groupBox21.Controls.Add(this.label249);
            this.groupBox21.Controls.Add(this._ssl19);
            this.groupBox21.Controls.Add(this.label250);
            this.groupBox21.Controls.Add(this._ssl18);
            this.groupBox21.Controls.Add(this.label251);
            this.groupBox21.Controls.Add(this._ssl17);
            this.groupBox21.Controls.Add(this.label252);
            this.groupBox21.Controls.Add(this._ssl16);
            this.groupBox21.Controls.Add(this.label253);
            this.groupBox21.Controls.Add(this._ssl15);
            this.groupBox21.Controls.Add(this.label254);
            this.groupBox21.Controls.Add(this._ssl14);
            this.groupBox21.Controls.Add(this.label255);
            this.groupBox21.Controls.Add(this._ssl13);
            this.groupBox21.Controls.Add(this.label256);
            this.groupBox21.Controls.Add(this._ssl12);
            this.groupBox21.Controls.Add(this.label257);
            this.groupBox21.Controls.Add(this._ssl11);
            this.groupBox21.Controls.Add(this.label258);
            this.groupBox21.Controls.Add(this._ssl10);
            this.groupBox21.Controls.Add(this.label259);
            this.groupBox21.Controls.Add(this._ssl9);
            this.groupBox21.Controls.Add(this.label260);
            this.groupBox21.Controls.Add(this._ssl8);
            this.groupBox21.Controls.Add(this.label261);
            this.groupBox21.Controls.Add(this._ssl7);
            this.groupBox21.Controls.Add(this.label262);
            this.groupBox21.Controls.Add(this._ssl6);
            this.groupBox21.Controls.Add(this.label263);
            this.groupBox21.Controls.Add(this._ssl5);
            this.groupBox21.Controls.Add(this.label264);
            this.groupBox21.Controls.Add(this._ssl4);
            this.groupBox21.Controls.Add(this.label265);
            this.groupBox21.Controls.Add(this._ssl3);
            this.groupBox21.Controls.Add(this.label266);
            this.groupBox21.Controls.Add(this._ssl2);
            this.groupBox21.Controls.Add(this.label267);
            this.groupBox21.Controls.Add(this._ssl1);
            this.groupBox21.Controls.Add(this.label268);
            this.groupBox21.Location = new System.Drawing.Point(7, 4);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(396, 175);
            this.groupBox21.TabIndex = 14;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "Сигналы СП-логики";
            // 
            // _ssl48
            // 
            this._ssl48.Location = new System.Drawing.Point(323, 152);
            this._ssl48.Name = "_ssl48";
            this._ssl48.Size = new System.Drawing.Size(13, 13);
            this._ssl48.State = BEMN.Forms.LedState.Off;
            this._ssl48.TabIndex = 79;
            // 
            // _ssl40
            // 
            this._ssl40.Location = new System.Drawing.Point(260, 152);
            this._ssl40.Name = "_ssl40";
            this._ssl40.Size = new System.Drawing.Size(13, 13);
            this._ssl40.State = BEMN.Forms.LedState.Off;
            this._ssl40.TabIndex = 79;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(342, 152);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(41, 13);
            this.label17.TabIndex = 78;
            this.label17.Text = "ССЛ48";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(279, 152);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 78;
            this.label1.Text = "ССЛ40";
            // 
            // _ssl47
            // 
            this._ssl47.Location = new System.Drawing.Point(323, 133);
            this._ssl47.Name = "_ssl47";
            this._ssl47.Size = new System.Drawing.Size(13, 13);
            this._ssl47.State = BEMN.Forms.LedState.Off;
            this._ssl47.TabIndex = 77;
            // 
            // _ssl39
            // 
            this._ssl39.Location = new System.Drawing.Point(260, 133);
            this._ssl39.Name = "_ssl39";
            this._ssl39.Size = new System.Drawing.Size(13, 13);
            this._ssl39.State = BEMN.Forms.LedState.Off;
            this._ssl39.TabIndex = 77;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(342, 133);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(41, 13);
            this.label16.TabIndex = 76;
            this.label16.Text = "ССЛ47";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(279, 133);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 76;
            this.label2.Text = "ССЛ39";
            // 
            // _ssl46
            // 
            this._ssl46.Location = new System.Drawing.Point(323, 114);
            this._ssl46.Name = "_ssl46";
            this._ssl46.Size = new System.Drawing.Size(13, 13);
            this._ssl46.State = BEMN.Forms.LedState.Off;
            this._ssl46.TabIndex = 75;
            // 
            // _ssl38
            // 
            this._ssl38.Location = new System.Drawing.Point(260, 114);
            this._ssl38.Name = "_ssl38";
            this._ssl38.Size = new System.Drawing.Size(13, 13);
            this._ssl38.State = BEMN.Forms.LedState.Off;
            this._ssl38.TabIndex = 75;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(342, 114);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 13);
            this.label15.TabIndex = 74;
            this.label15.Text = "ССЛ46";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(279, 114);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 74;
            this.label3.Text = "ССЛ38";
            // 
            // _ssl45
            // 
            this._ssl45.Location = new System.Drawing.Point(323, 95);
            this._ssl45.Name = "_ssl45";
            this._ssl45.Size = new System.Drawing.Size(13, 13);
            this._ssl45.State = BEMN.Forms.LedState.Off;
            this._ssl45.TabIndex = 73;
            // 
            // _ssl37
            // 
            this._ssl37.Location = new System.Drawing.Point(260, 95);
            this._ssl37.Name = "_ssl37";
            this._ssl37.Size = new System.Drawing.Size(13, 13);
            this._ssl37.State = BEMN.Forms.LedState.Off;
            this._ssl37.TabIndex = 73;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(342, 95);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(41, 13);
            this.label14.TabIndex = 72;
            this.label14.Text = "ССЛ45";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(279, 95);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 13);
            this.label4.TabIndex = 72;
            this.label4.Text = "ССЛ37";
            // 
            // _ssl44
            // 
            this._ssl44.Location = new System.Drawing.Point(323, 76);
            this._ssl44.Name = "_ssl44";
            this._ssl44.Size = new System.Drawing.Size(13, 13);
            this._ssl44.State = BEMN.Forms.LedState.Off;
            this._ssl44.TabIndex = 71;
            // 
            // _ssl36
            // 
            this._ssl36.Location = new System.Drawing.Point(260, 76);
            this._ssl36.Name = "_ssl36";
            this._ssl36.Size = new System.Drawing.Size(13, 13);
            this._ssl36.State = BEMN.Forms.LedState.Off;
            this._ssl36.TabIndex = 71;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(342, 76);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(41, 13);
            this.label13.TabIndex = 70;
            this.label13.Text = "ССЛ44";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(279, 76);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 70;
            this.label5.Text = "ССЛ36";
            // 
            // _ssl43
            // 
            this._ssl43.Location = new System.Drawing.Point(323, 57);
            this._ssl43.Name = "_ssl43";
            this._ssl43.Size = new System.Drawing.Size(13, 13);
            this._ssl43.State = BEMN.Forms.LedState.Off;
            this._ssl43.TabIndex = 69;
            // 
            // _ssl35
            // 
            this._ssl35.Location = new System.Drawing.Point(260, 57);
            this._ssl35.Name = "_ssl35";
            this._ssl35.Size = new System.Drawing.Size(13, 13);
            this._ssl35.State = BEMN.Forms.LedState.Off;
            this._ssl35.TabIndex = 69;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(342, 57);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 13);
            this.label11.TabIndex = 68;
            this.label11.Text = "ССЛ43";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(279, 57);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 68;
            this.label6.Text = "ССЛ35";
            // 
            // _ssl42
            // 
            this._ssl42.Location = new System.Drawing.Point(323, 38);
            this._ssl42.Name = "_ssl42";
            this._ssl42.Size = new System.Drawing.Size(13, 13);
            this._ssl42.State = BEMN.Forms.LedState.Off;
            this._ssl42.TabIndex = 67;
            // 
            // _ssl34
            // 
            this._ssl34.Location = new System.Drawing.Point(260, 38);
            this._ssl34.Name = "_ssl34";
            this._ssl34.Size = new System.Drawing.Size(13, 13);
            this._ssl34.State = BEMN.Forms.LedState.Off;
            this._ssl34.TabIndex = 67;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(342, 38);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 13);
            this.label10.TabIndex = 66;
            this.label10.Text = "ССЛ42";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(279, 38);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 13);
            this.label7.TabIndex = 66;
            this.label7.Text = "ССЛ34";
            // 
            // _ssl41
            // 
            this._ssl41.Location = new System.Drawing.Point(323, 19);
            this._ssl41.Name = "_ssl41";
            this._ssl41.Size = new System.Drawing.Size(13, 13);
            this._ssl41.State = BEMN.Forms.LedState.Off;
            this._ssl41.TabIndex = 65;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(342, 19);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 13);
            this.label9.TabIndex = 64;
            this.label9.Text = "ССЛ41";
            // 
            // _ssl33
            // 
            this._ssl33.Location = new System.Drawing.Point(260, 19);
            this._ssl33.Name = "_ssl33";
            this._ssl33.Size = new System.Drawing.Size(13, 13);
            this._ssl33.State = BEMN.Forms.LedState.Off;
            this._ssl33.TabIndex = 65;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(279, 19);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 13);
            this.label8.TabIndex = 64;
            this.label8.Text = "ССЛ33";
            // 
            // _ssl32
            // 
            this._ssl32.Location = new System.Drawing.Point(194, 152);
            this._ssl32.Name = "_ssl32";
            this._ssl32.Size = new System.Drawing.Size(13, 13);
            this._ssl32.State = BEMN.Forms.LedState.Off;
            this._ssl32.TabIndex = 63;
            // 
            // label237
            // 
            this.label237.AutoSize = true;
            this.label237.Location = new System.Drawing.Point(213, 152);
            this.label237.Name = "label237";
            this.label237.Size = new System.Drawing.Size(41, 13);
            this.label237.TabIndex = 62;
            this.label237.Text = "ССЛ32";
            // 
            // _ssl31
            // 
            this._ssl31.Location = new System.Drawing.Point(194, 133);
            this._ssl31.Name = "_ssl31";
            this._ssl31.Size = new System.Drawing.Size(13, 13);
            this._ssl31.State = BEMN.Forms.LedState.Off;
            this._ssl31.TabIndex = 61;
            // 
            // label238
            // 
            this.label238.AutoSize = true;
            this.label238.Location = new System.Drawing.Point(213, 133);
            this.label238.Name = "label238";
            this.label238.Size = new System.Drawing.Size(41, 13);
            this.label238.TabIndex = 60;
            this.label238.Text = "ССЛ31";
            // 
            // _ssl30
            // 
            this._ssl30.Location = new System.Drawing.Point(194, 114);
            this._ssl30.Name = "_ssl30";
            this._ssl30.Size = new System.Drawing.Size(13, 13);
            this._ssl30.State = BEMN.Forms.LedState.Off;
            this._ssl30.TabIndex = 59;
            // 
            // label239
            // 
            this.label239.AutoSize = true;
            this.label239.Location = new System.Drawing.Point(213, 114);
            this.label239.Name = "label239";
            this.label239.Size = new System.Drawing.Size(41, 13);
            this.label239.TabIndex = 58;
            this.label239.Text = "ССЛ30";
            // 
            // _ssl29
            // 
            this._ssl29.Location = new System.Drawing.Point(194, 95);
            this._ssl29.Name = "_ssl29";
            this._ssl29.Size = new System.Drawing.Size(13, 13);
            this._ssl29.State = BEMN.Forms.LedState.Off;
            this._ssl29.TabIndex = 57;
            // 
            // label240
            // 
            this.label240.AutoSize = true;
            this.label240.Location = new System.Drawing.Point(213, 95);
            this.label240.Name = "label240";
            this.label240.Size = new System.Drawing.Size(41, 13);
            this.label240.TabIndex = 56;
            this.label240.Text = "ССЛ29";
            // 
            // _ssl28
            // 
            this._ssl28.Location = new System.Drawing.Point(194, 76);
            this._ssl28.Name = "_ssl28";
            this._ssl28.Size = new System.Drawing.Size(13, 13);
            this._ssl28.State = BEMN.Forms.LedState.Off;
            this._ssl28.TabIndex = 55;
            // 
            // label241
            // 
            this.label241.AutoSize = true;
            this.label241.Location = new System.Drawing.Point(213, 76);
            this.label241.Name = "label241";
            this.label241.Size = new System.Drawing.Size(41, 13);
            this.label241.TabIndex = 54;
            this.label241.Text = "ССЛ28";
            // 
            // _ssl27
            // 
            this._ssl27.Location = new System.Drawing.Point(194, 57);
            this._ssl27.Name = "_ssl27";
            this._ssl27.Size = new System.Drawing.Size(13, 13);
            this._ssl27.State = BEMN.Forms.LedState.Off;
            this._ssl27.TabIndex = 53;
            // 
            // label242
            // 
            this.label242.AutoSize = true;
            this.label242.Location = new System.Drawing.Point(213, 57);
            this.label242.Name = "label242";
            this.label242.Size = new System.Drawing.Size(41, 13);
            this.label242.TabIndex = 52;
            this.label242.Text = "ССЛ27";
            // 
            // _ssl26
            // 
            this._ssl26.Location = new System.Drawing.Point(194, 38);
            this._ssl26.Name = "_ssl26";
            this._ssl26.Size = new System.Drawing.Size(13, 13);
            this._ssl26.State = BEMN.Forms.LedState.Off;
            this._ssl26.TabIndex = 51;
            // 
            // label243
            // 
            this.label243.AutoSize = true;
            this.label243.Location = new System.Drawing.Point(213, 38);
            this.label243.Name = "label243";
            this.label243.Size = new System.Drawing.Size(41, 13);
            this.label243.TabIndex = 50;
            this.label243.Text = "ССЛ26";
            // 
            // _ssl25
            // 
            this._ssl25.Location = new System.Drawing.Point(194, 19);
            this._ssl25.Name = "_ssl25";
            this._ssl25.Size = new System.Drawing.Size(13, 13);
            this._ssl25.State = BEMN.Forms.LedState.Off;
            this._ssl25.TabIndex = 49;
            // 
            // label244
            // 
            this.label244.AutoSize = true;
            this.label244.Location = new System.Drawing.Point(213, 19);
            this.label244.Name = "label244";
            this.label244.Size = new System.Drawing.Size(41, 13);
            this.label244.TabIndex = 48;
            this.label244.Text = "ССЛ25";
            // 
            // _ssl24
            // 
            this._ssl24.Location = new System.Drawing.Point(128, 152);
            this._ssl24.Name = "_ssl24";
            this._ssl24.Size = new System.Drawing.Size(13, 13);
            this._ssl24.State = BEMN.Forms.LedState.Off;
            this._ssl24.TabIndex = 47;
            // 
            // label245
            // 
            this.label245.AutoSize = true;
            this.label245.Location = new System.Drawing.Point(147, 152);
            this.label245.Name = "label245";
            this.label245.Size = new System.Drawing.Size(41, 13);
            this.label245.TabIndex = 46;
            this.label245.Text = "ССЛ24";
            // 
            // _ssl23
            // 
            this._ssl23.Location = new System.Drawing.Point(128, 133);
            this._ssl23.Name = "_ssl23";
            this._ssl23.Size = new System.Drawing.Size(13, 13);
            this._ssl23.State = BEMN.Forms.LedState.Off;
            this._ssl23.TabIndex = 45;
            // 
            // label246
            // 
            this.label246.AutoSize = true;
            this.label246.Location = new System.Drawing.Point(147, 133);
            this.label246.Name = "label246";
            this.label246.Size = new System.Drawing.Size(41, 13);
            this.label246.TabIndex = 44;
            this.label246.Text = "ССЛ23";
            // 
            // _ssl22
            // 
            this._ssl22.Location = new System.Drawing.Point(128, 114);
            this._ssl22.Name = "_ssl22";
            this._ssl22.Size = new System.Drawing.Size(13, 13);
            this._ssl22.State = BEMN.Forms.LedState.Off;
            this._ssl22.TabIndex = 43;
            // 
            // label247
            // 
            this.label247.AutoSize = true;
            this.label247.Location = new System.Drawing.Point(147, 114);
            this.label247.Name = "label247";
            this.label247.Size = new System.Drawing.Size(41, 13);
            this.label247.TabIndex = 42;
            this.label247.Text = "ССЛ22";
            // 
            // _ssl21
            // 
            this._ssl21.Location = new System.Drawing.Point(128, 95);
            this._ssl21.Name = "_ssl21";
            this._ssl21.Size = new System.Drawing.Size(13, 13);
            this._ssl21.State = BEMN.Forms.LedState.Off;
            this._ssl21.TabIndex = 41;
            // 
            // label248
            // 
            this.label248.AutoSize = true;
            this.label248.Location = new System.Drawing.Point(147, 95);
            this.label248.Name = "label248";
            this.label248.Size = new System.Drawing.Size(41, 13);
            this.label248.TabIndex = 40;
            this.label248.Text = "ССЛ21";
            // 
            // _ssl20
            // 
            this._ssl20.Location = new System.Drawing.Point(128, 76);
            this._ssl20.Name = "_ssl20";
            this._ssl20.Size = new System.Drawing.Size(13, 13);
            this._ssl20.State = BEMN.Forms.LedState.Off;
            this._ssl20.TabIndex = 39;
            // 
            // label249
            // 
            this.label249.AutoSize = true;
            this.label249.Location = new System.Drawing.Point(148, 76);
            this.label249.Name = "label249";
            this.label249.Size = new System.Drawing.Size(41, 13);
            this.label249.TabIndex = 38;
            this.label249.Text = "ССЛ20";
            // 
            // _ssl19
            // 
            this._ssl19.Location = new System.Drawing.Point(128, 57);
            this._ssl19.Name = "_ssl19";
            this._ssl19.Size = new System.Drawing.Size(13, 13);
            this._ssl19.State = BEMN.Forms.LedState.Off;
            this._ssl19.TabIndex = 37;
            // 
            // label250
            // 
            this.label250.AutoSize = true;
            this.label250.Location = new System.Drawing.Point(147, 57);
            this.label250.Name = "label250";
            this.label250.Size = new System.Drawing.Size(41, 13);
            this.label250.TabIndex = 36;
            this.label250.Text = "ССЛ19";
            // 
            // _ssl18
            // 
            this._ssl18.Location = new System.Drawing.Point(128, 38);
            this._ssl18.Name = "_ssl18";
            this._ssl18.Size = new System.Drawing.Size(13, 13);
            this._ssl18.State = BEMN.Forms.LedState.Off;
            this._ssl18.TabIndex = 35;
            // 
            // label251
            // 
            this.label251.AutoSize = true;
            this.label251.Location = new System.Drawing.Point(147, 38);
            this.label251.Name = "label251";
            this.label251.Size = new System.Drawing.Size(41, 13);
            this.label251.TabIndex = 34;
            this.label251.Text = "ССЛ18";
            // 
            // _ssl17
            // 
            this._ssl17.Location = new System.Drawing.Point(128, 19);
            this._ssl17.Name = "_ssl17";
            this._ssl17.Size = new System.Drawing.Size(13, 13);
            this._ssl17.State = BEMN.Forms.LedState.Off;
            this._ssl17.TabIndex = 33;
            // 
            // label252
            // 
            this.label252.AutoSize = true;
            this.label252.Location = new System.Drawing.Point(147, 19);
            this.label252.Name = "label252";
            this.label252.Size = new System.Drawing.Size(41, 13);
            this.label252.TabIndex = 32;
            this.label252.Text = "ССЛ17";
            // 
            // _ssl16
            // 
            this._ssl16.Location = new System.Drawing.Point(64, 152);
            this._ssl16.Name = "_ssl16";
            this._ssl16.Size = new System.Drawing.Size(13, 13);
            this._ssl16.State = BEMN.Forms.LedState.Off;
            this._ssl16.TabIndex = 31;
            // 
            // label253
            // 
            this.label253.AutoSize = true;
            this.label253.Location = new System.Drawing.Point(83, 152);
            this.label253.Name = "label253";
            this.label253.Size = new System.Drawing.Size(41, 13);
            this.label253.TabIndex = 30;
            this.label253.Text = "ССЛ16";
            // 
            // _ssl15
            // 
            this._ssl15.Location = new System.Drawing.Point(64, 133);
            this._ssl15.Name = "_ssl15";
            this._ssl15.Size = new System.Drawing.Size(13, 13);
            this._ssl15.State = BEMN.Forms.LedState.Off;
            this._ssl15.TabIndex = 29;
            // 
            // label254
            // 
            this.label254.AutoSize = true;
            this.label254.Location = new System.Drawing.Point(83, 133);
            this.label254.Name = "label254";
            this.label254.Size = new System.Drawing.Size(41, 13);
            this.label254.TabIndex = 28;
            this.label254.Text = "ССЛ15";
            // 
            // _ssl14
            // 
            this._ssl14.Location = new System.Drawing.Point(64, 114);
            this._ssl14.Name = "_ssl14";
            this._ssl14.Size = new System.Drawing.Size(13, 13);
            this._ssl14.State = BEMN.Forms.LedState.Off;
            this._ssl14.TabIndex = 27;
            // 
            // label255
            // 
            this.label255.AutoSize = true;
            this.label255.Location = new System.Drawing.Point(83, 114);
            this.label255.Name = "label255";
            this.label255.Size = new System.Drawing.Size(41, 13);
            this.label255.TabIndex = 26;
            this.label255.Text = "ССЛ14";
            // 
            // _ssl13
            // 
            this._ssl13.Location = new System.Drawing.Point(64, 95);
            this._ssl13.Name = "_ssl13";
            this._ssl13.Size = new System.Drawing.Size(13, 13);
            this._ssl13.State = BEMN.Forms.LedState.Off;
            this._ssl13.TabIndex = 25;
            // 
            // label256
            // 
            this.label256.AutoSize = true;
            this.label256.Location = new System.Drawing.Point(83, 95);
            this.label256.Name = "label256";
            this.label256.Size = new System.Drawing.Size(41, 13);
            this.label256.TabIndex = 24;
            this.label256.Text = "ССЛ13";
            // 
            // _ssl12
            // 
            this._ssl12.Location = new System.Drawing.Point(64, 76);
            this._ssl12.Name = "_ssl12";
            this._ssl12.Size = new System.Drawing.Size(13, 13);
            this._ssl12.State = BEMN.Forms.LedState.Off;
            this._ssl12.TabIndex = 23;
            // 
            // label257
            // 
            this.label257.AutoSize = true;
            this.label257.Location = new System.Drawing.Point(83, 76);
            this.label257.Name = "label257";
            this.label257.Size = new System.Drawing.Size(41, 13);
            this.label257.TabIndex = 22;
            this.label257.Text = "ССЛ12";
            // 
            // _ssl11
            // 
            this._ssl11.Location = new System.Drawing.Point(64, 57);
            this._ssl11.Name = "_ssl11";
            this._ssl11.Size = new System.Drawing.Size(13, 13);
            this._ssl11.State = BEMN.Forms.LedState.Off;
            this._ssl11.TabIndex = 21;
            // 
            // label258
            // 
            this.label258.AutoSize = true;
            this.label258.Location = new System.Drawing.Point(83, 57);
            this.label258.Name = "label258";
            this.label258.Size = new System.Drawing.Size(41, 13);
            this.label258.TabIndex = 20;
            this.label258.Text = "ССЛ11";
            // 
            // _ssl10
            // 
            this._ssl10.Location = new System.Drawing.Point(64, 38);
            this._ssl10.Name = "_ssl10";
            this._ssl10.Size = new System.Drawing.Size(13, 13);
            this._ssl10.State = BEMN.Forms.LedState.Off;
            this._ssl10.TabIndex = 19;
            // 
            // label259
            // 
            this.label259.AutoSize = true;
            this.label259.Location = new System.Drawing.Point(83, 38);
            this.label259.Name = "label259";
            this.label259.Size = new System.Drawing.Size(41, 13);
            this.label259.TabIndex = 18;
            this.label259.Text = "ССЛ10";
            // 
            // _ssl9
            // 
            this._ssl9.Location = new System.Drawing.Point(64, 19);
            this._ssl9.Name = "_ssl9";
            this._ssl9.Size = new System.Drawing.Size(13, 13);
            this._ssl9.State = BEMN.Forms.LedState.Off;
            this._ssl9.TabIndex = 17;
            // 
            // label260
            // 
            this.label260.AutoSize = true;
            this.label260.Location = new System.Drawing.Point(83, 19);
            this.label260.Name = "label260";
            this.label260.Size = new System.Drawing.Size(35, 13);
            this.label260.TabIndex = 16;
            this.label260.Text = "ССЛ9";
            // 
            // _ssl8
            // 
            this._ssl8.Location = new System.Drawing.Point(6, 152);
            this._ssl8.Name = "_ssl8";
            this._ssl8.Size = new System.Drawing.Size(13, 13);
            this._ssl8.State = BEMN.Forms.LedState.Off;
            this._ssl8.TabIndex = 15;
            // 
            // label261
            // 
            this.label261.AutoSize = true;
            this.label261.Location = new System.Drawing.Point(25, 152);
            this.label261.Name = "label261";
            this.label261.Size = new System.Drawing.Size(35, 13);
            this.label261.TabIndex = 14;
            this.label261.Text = "ССЛ8";
            // 
            // _ssl7
            // 
            this._ssl7.Location = new System.Drawing.Point(6, 133);
            this._ssl7.Name = "_ssl7";
            this._ssl7.Size = new System.Drawing.Size(13, 13);
            this._ssl7.State = BEMN.Forms.LedState.Off;
            this._ssl7.TabIndex = 13;
            // 
            // label262
            // 
            this.label262.AutoSize = true;
            this.label262.Location = new System.Drawing.Point(25, 133);
            this.label262.Name = "label262";
            this.label262.Size = new System.Drawing.Size(35, 13);
            this.label262.TabIndex = 12;
            this.label262.Text = "ССЛ7";
            // 
            // _ssl6
            // 
            this._ssl6.Location = new System.Drawing.Point(6, 114);
            this._ssl6.Name = "_ssl6";
            this._ssl6.Size = new System.Drawing.Size(13, 13);
            this._ssl6.State = BEMN.Forms.LedState.Off;
            this._ssl6.TabIndex = 11;
            // 
            // label263
            // 
            this.label263.AutoSize = true;
            this.label263.Location = new System.Drawing.Point(25, 114);
            this.label263.Name = "label263";
            this.label263.Size = new System.Drawing.Size(35, 13);
            this.label263.TabIndex = 10;
            this.label263.Text = "ССЛ6";
            // 
            // _ssl5
            // 
            this._ssl5.Location = new System.Drawing.Point(6, 95);
            this._ssl5.Name = "_ssl5";
            this._ssl5.Size = new System.Drawing.Size(13, 13);
            this._ssl5.State = BEMN.Forms.LedState.Off;
            this._ssl5.TabIndex = 9;
            // 
            // label264
            // 
            this.label264.AutoSize = true;
            this.label264.Location = new System.Drawing.Point(25, 95);
            this.label264.Name = "label264";
            this.label264.Size = new System.Drawing.Size(35, 13);
            this.label264.TabIndex = 8;
            this.label264.Text = "ССЛ5";
            // 
            // _ssl4
            // 
            this._ssl4.Location = new System.Drawing.Point(6, 76);
            this._ssl4.Name = "_ssl4";
            this._ssl4.Size = new System.Drawing.Size(13, 13);
            this._ssl4.State = BEMN.Forms.LedState.Off;
            this._ssl4.TabIndex = 7;
            // 
            // label265
            // 
            this.label265.AutoSize = true;
            this.label265.Location = new System.Drawing.Point(26, 76);
            this.label265.Name = "label265";
            this.label265.Size = new System.Drawing.Size(35, 13);
            this.label265.TabIndex = 6;
            this.label265.Text = "ССЛ4";
            // 
            // _ssl3
            // 
            this._ssl3.Location = new System.Drawing.Point(6, 57);
            this._ssl3.Name = "_ssl3";
            this._ssl3.Size = new System.Drawing.Size(13, 13);
            this._ssl3.State = BEMN.Forms.LedState.Off;
            this._ssl3.TabIndex = 5;
            // 
            // label266
            // 
            this.label266.AutoSize = true;
            this.label266.Location = new System.Drawing.Point(25, 57);
            this.label266.Name = "label266";
            this.label266.Size = new System.Drawing.Size(35, 13);
            this.label266.TabIndex = 4;
            this.label266.Text = "ССЛ3";
            // 
            // _ssl2
            // 
            this._ssl2.Location = new System.Drawing.Point(6, 38);
            this._ssl2.Name = "_ssl2";
            this._ssl2.Size = new System.Drawing.Size(13, 13);
            this._ssl2.State = BEMN.Forms.LedState.Off;
            this._ssl2.TabIndex = 3;
            // 
            // label267
            // 
            this.label267.AutoSize = true;
            this.label267.Location = new System.Drawing.Point(25, 38);
            this.label267.Name = "label267";
            this.label267.Size = new System.Drawing.Size(35, 13);
            this.label267.TabIndex = 2;
            this.label267.Text = "ССЛ2";
            // 
            // _ssl1
            // 
            this._ssl1.Location = new System.Drawing.Point(6, 19);
            this._ssl1.Name = "_ssl1";
            this._ssl1.Size = new System.Drawing.Size(13, 13);
            this._ssl1.State = BEMN.Forms.LedState.Off;
            this._ssl1.TabIndex = 1;
            // 
            // label268
            // 
            this.label268.AutoSize = true;
            this.label268.Location = new System.Drawing.Point(25, 19);
            this.label268.Name = "label268";
            this.label268.Size = new System.Drawing.Size(35, 13);
            this.label268.TabIndex = 0;
            this.label268.Text = "ССЛ1";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this._vls16);
            this.groupBox10.Controls.Add(this.label63);
            this.groupBox10.Controls.Add(this._vls15);
            this.groupBox10.Controls.Add(this.label64);
            this.groupBox10.Controls.Add(this._vls14);
            this.groupBox10.Controls.Add(this.label65);
            this.groupBox10.Controls.Add(this._vls13);
            this.groupBox10.Controls.Add(this.label66);
            this.groupBox10.Controls.Add(this._vls12);
            this.groupBox10.Controls.Add(this.label67);
            this.groupBox10.Controls.Add(this._vls11);
            this.groupBox10.Controls.Add(this.label68);
            this.groupBox10.Controls.Add(this._vls10);
            this.groupBox10.Controls.Add(this.label69);
            this.groupBox10.Controls.Add(this._vls9);
            this.groupBox10.Controls.Add(this.label70);
            this.groupBox10.Controls.Add(this._vls8);
            this.groupBox10.Controls.Add(this.label71);
            this.groupBox10.Controls.Add(this._vls7);
            this.groupBox10.Controls.Add(this.label72);
            this.groupBox10.Controls.Add(this._vls6);
            this.groupBox10.Controls.Add(this.label73);
            this.groupBox10.Controls.Add(this._vls5);
            this.groupBox10.Controls.Add(this.label74);
            this.groupBox10.Controls.Add(this._vls4);
            this.groupBox10.Controls.Add(this.label75);
            this.groupBox10.Controls.Add(this._vls3);
            this.groupBox10.Controls.Add(this.label76);
            this.groupBox10.Controls.Add(this._vls2);
            this.groupBox10.Controls.Add(this.label77);
            this.groupBox10.Controls.Add(this._vls1);
            this.groupBox10.Controls.Add(this.label78);
            this.groupBox10.Location = new System.Drawing.Point(573, 3);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(141, 191);
            this.groupBox10.TabIndex = 3;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Выходные логические сигналы ВЛС";
            // 
            // _vls16
            // 
            this._vls16.Location = new System.Drawing.Point(71, 166);
            this._vls16.Name = "_vls16";
            this._vls16.Size = new System.Drawing.Size(13, 13);
            this._vls16.State = BEMN.Forms.LedState.Off;
            this._vls16.TabIndex = 31;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(90, 166);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(41, 13);
            this.label63.TabIndex = 30;
            this.label63.Text = "ВЛС16";
            // 
            // _vls15
            // 
            this._vls15.Location = new System.Drawing.Point(71, 147);
            this._vls15.Name = "_vls15";
            this._vls15.Size = new System.Drawing.Size(13, 13);
            this._vls15.State = BEMN.Forms.LedState.Off;
            this._vls15.TabIndex = 29;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(90, 147);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(41, 13);
            this.label64.TabIndex = 28;
            this.label64.Text = "ВЛС15";
            // 
            // _vls14
            // 
            this._vls14.Location = new System.Drawing.Point(71, 128);
            this._vls14.Name = "_vls14";
            this._vls14.Size = new System.Drawing.Size(13, 13);
            this._vls14.State = BEMN.Forms.LedState.Off;
            this._vls14.TabIndex = 27;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(90, 128);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(41, 13);
            this.label65.TabIndex = 26;
            this.label65.Text = "ВЛС14";
            // 
            // _vls13
            // 
            this._vls13.Location = new System.Drawing.Point(71, 109);
            this._vls13.Name = "_vls13";
            this._vls13.Size = new System.Drawing.Size(13, 13);
            this._vls13.State = BEMN.Forms.LedState.Off;
            this._vls13.TabIndex = 25;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(90, 109);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(41, 13);
            this.label66.TabIndex = 24;
            this.label66.Text = "ВЛС13";
            // 
            // _vls12
            // 
            this._vls12.Location = new System.Drawing.Point(71, 90);
            this._vls12.Name = "_vls12";
            this._vls12.Size = new System.Drawing.Size(13, 13);
            this._vls12.State = BEMN.Forms.LedState.Off;
            this._vls12.TabIndex = 23;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(90, 90);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(41, 13);
            this.label67.TabIndex = 22;
            this.label67.Text = "ВЛС12";
            // 
            // _vls11
            // 
            this._vls11.Location = new System.Drawing.Point(71, 71);
            this._vls11.Name = "_vls11";
            this._vls11.Size = new System.Drawing.Size(13, 13);
            this._vls11.State = BEMN.Forms.LedState.Off;
            this._vls11.TabIndex = 21;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(90, 71);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(41, 13);
            this.label68.TabIndex = 20;
            this.label68.Text = "ВЛС11";
            // 
            // _vls10
            // 
            this._vls10.Location = new System.Drawing.Point(71, 52);
            this._vls10.Name = "_vls10";
            this._vls10.Size = new System.Drawing.Size(13, 13);
            this._vls10.State = BEMN.Forms.LedState.Off;
            this._vls10.TabIndex = 19;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(90, 52);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(41, 13);
            this.label69.TabIndex = 18;
            this.label69.Text = "ВЛС10";
            // 
            // _vls9
            // 
            this._vls9.Location = new System.Drawing.Point(71, 33);
            this._vls9.Name = "_vls9";
            this._vls9.Size = new System.Drawing.Size(13, 13);
            this._vls9.State = BEMN.Forms.LedState.Off;
            this._vls9.TabIndex = 17;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(90, 33);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(35, 13);
            this.label70.TabIndex = 16;
            this.label70.Text = "ВЛС9";
            // 
            // _vls8
            // 
            this._vls8.Location = new System.Drawing.Point(6, 166);
            this._vls8.Name = "_vls8";
            this._vls8.Size = new System.Drawing.Size(13, 13);
            this._vls8.State = BEMN.Forms.LedState.Off;
            this._vls8.TabIndex = 15;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(25, 166);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(35, 13);
            this.label71.TabIndex = 14;
            this.label71.Text = "ВЛС8";
            // 
            // _vls7
            // 
            this._vls7.Location = new System.Drawing.Point(6, 147);
            this._vls7.Name = "_vls7";
            this._vls7.Size = new System.Drawing.Size(13, 13);
            this._vls7.State = BEMN.Forms.LedState.Off;
            this._vls7.TabIndex = 13;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(25, 147);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(35, 13);
            this.label72.TabIndex = 12;
            this.label72.Text = "ВЛС7";
            // 
            // _vls6
            // 
            this._vls6.Location = new System.Drawing.Point(6, 128);
            this._vls6.Name = "_vls6";
            this._vls6.Size = new System.Drawing.Size(13, 13);
            this._vls6.State = BEMN.Forms.LedState.Off;
            this._vls6.TabIndex = 11;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(25, 128);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(35, 13);
            this.label73.TabIndex = 10;
            this.label73.Text = "ВЛС6";
            // 
            // _vls5
            // 
            this._vls5.Location = new System.Drawing.Point(6, 109);
            this._vls5.Name = "_vls5";
            this._vls5.Size = new System.Drawing.Size(13, 13);
            this._vls5.State = BEMN.Forms.LedState.Off;
            this._vls5.TabIndex = 9;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(25, 109);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(35, 13);
            this.label74.TabIndex = 8;
            this.label74.Text = "ВЛС5";
            // 
            // _vls4
            // 
            this._vls4.Location = new System.Drawing.Point(6, 90);
            this._vls4.Name = "_vls4";
            this._vls4.Size = new System.Drawing.Size(13, 13);
            this._vls4.State = BEMN.Forms.LedState.Off;
            this._vls4.TabIndex = 7;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(25, 90);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(35, 13);
            this.label75.TabIndex = 6;
            this.label75.Text = "ВЛС4";
            // 
            // _vls3
            // 
            this._vls3.Location = new System.Drawing.Point(6, 71);
            this._vls3.Name = "_vls3";
            this._vls3.Size = new System.Drawing.Size(13, 13);
            this._vls3.State = BEMN.Forms.LedState.Off;
            this._vls3.TabIndex = 5;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(25, 71);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(35, 13);
            this.label76.TabIndex = 4;
            this.label76.Text = "ВЛС3";
            // 
            // _vls2
            // 
            this._vls2.Location = new System.Drawing.Point(6, 52);
            this._vls2.Name = "_vls2";
            this._vls2.Size = new System.Drawing.Size(13, 13);
            this._vls2.State = BEMN.Forms.LedState.Off;
            this._vls2.TabIndex = 3;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(25, 52);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(35, 13);
            this.label77.TabIndex = 2;
            this.label77.Text = "ВЛС2";
            // 
            // _vls1
            // 
            this._vls1.Location = new System.Drawing.Point(6, 33);
            this._vls1.Name = "_vls1";
            this._vls1.Size = new System.Drawing.Size(13, 13);
            this._vls1.State = BEMN.Forms.LedState.Off;
            this._vls1.TabIndex = 1;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(25, 33);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(35, 13);
            this.label78.TabIndex = 0;
            this.label78.Text = "ВЛС1";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this._ls16);
            this.groupBox9.Controls.Add(this.label47);
            this.groupBox9.Controls.Add(this._ls15);
            this.groupBox9.Controls.Add(this.label48);
            this.groupBox9.Controls.Add(this._ls14);
            this.groupBox9.Controls.Add(this.label49);
            this.groupBox9.Controls.Add(this._ls13);
            this.groupBox9.Controls.Add(this.label50);
            this.groupBox9.Controls.Add(this._ls12);
            this.groupBox9.Controls.Add(this.label51);
            this.groupBox9.Controls.Add(this._ls11);
            this.groupBox9.Controls.Add(this.label52);
            this.groupBox9.Controls.Add(this._ls10);
            this.groupBox9.Controls.Add(this.label53);
            this.groupBox9.Controls.Add(this._ls9);
            this.groupBox9.Controls.Add(this.label54);
            this.groupBox9.Controls.Add(this._ls8);
            this.groupBox9.Controls.Add(this.label55);
            this.groupBox9.Controls.Add(this._ls7);
            this.groupBox9.Controls.Add(this.label56);
            this.groupBox9.Controls.Add(this._ls6);
            this.groupBox9.Controls.Add(this.label57);
            this.groupBox9.Controls.Add(this._ls5);
            this.groupBox9.Controls.Add(this.label58);
            this.groupBox9.Controls.Add(this._ls4);
            this.groupBox9.Controls.Add(this.label59);
            this.groupBox9.Controls.Add(this._ls3);
            this.groupBox9.Controls.Add(this.label60);
            this.groupBox9.Controls.Add(this._ls2);
            this.groupBox9.Controls.Add(this.label61);
            this.groupBox9.Controls.Add(this._ls1);
            this.groupBox9.Controls.Add(this.label62);
            this.groupBox9.Location = new System.Drawing.Point(426, 3);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(141, 191);
            this.groupBox9.TabIndex = 2;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Входные логические сигналы ЛС";
            // 
            // _ls16
            // 
            this._ls16.Location = new System.Drawing.Point(71, 166);
            this._ls16.Name = "_ls16";
            this._ls16.Size = new System.Drawing.Size(13, 13);
            this._ls16.State = BEMN.Forms.LedState.Off;
            this._ls16.TabIndex = 31;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(90, 166);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(34, 13);
            this.label47.TabIndex = 30;
            this.label47.Text = "ЛС16";
            // 
            // _ls15
            // 
            this._ls15.Location = new System.Drawing.Point(71, 147);
            this._ls15.Name = "_ls15";
            this._ls15.Size = new System.Drawing.Size(13, 13);
            this._ls15.State = BEMN.Forms.LedState.Off;
            this._ls15.TabIndex = 29;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(90, 147);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(34, 13);
            this.label48.TabIndex = 28;
            this.label48.Text = "ЛС15";
            // 
            // _ls14
            // 
            this._ls14.Location = new System.Drawing.Point(71, 128);
            this._ls14.Name = "_ls14";
            this._ls14.Size = new System.Drawing.Size(13, 13);
            this._ls14.State = BEMN.Forms.LedState.Off;
            this._ls14.TabIndex = 27;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(90, 128);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(34, 13);
            this.label49.TabIndex = 26;
            this.label49.Text = "ЛС14";
            // 
            // _ls13
            // 
            this._ls13.Location = new System.Drawing.Point(71, 109);
            this._ls13.Name = "_ls13";
            this._ls13.Size = new System.Drawing.Size(13, 13);
            this._ls13.State = BEMN.Forms.LedState.Off;
            this._ls13.TabIndex = 25;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(90, 109);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(34, 13);
            this.label50.TabIndex = 24;
            this.label50.Text = "ЛС13";
            // 
            // _ls12
            // 
            this._ls12.Location = new System.Drawing.Point(71, 90);
            this._ls12.Name = "_ls12";
            this._ls12.Size = new System.Drawing.Size(13, 13);
            this._ls12.State = BEMN.Forms.LedState.Off;
            this._ls12.TabIndex = 23;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(90, 90);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(34, 13);
            this.label51.TabIndex = 22;
            this.label51.Text = "ЛС12";
            // 
            // _ls11
            // 
            this._ls11.Location = new System.Drawing.Point(71, 71);
            this._ls11.Name = "_ls11";
            this._ls11.Size = new System.Drawing.Size(13, 13);
            this._ls11.State = BEMN.Forms.LedState.Off;
            this._ls11.TabIndex = 21;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(90, 71);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(34, 13);
            this.label52.TabIndex = 20;
            this.label52.Text = "ЛС11";
            // 
            // _ls10
            // 
            this._ls10.Location = new System.Drawing.Point(71, 52);
            this._ls10.Name = "_ls10";
            this._ls10.Size = new System.Drawing.Size(13, 13);
            this._ls10.State = BEMN.Forms.LedState.Off;
            this._ls10.TabIndex = 19;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(90, 52);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(34, 13);
            this.label53.TabIndex = 18;
            this.label53.Text = "ЛС10";
            // 
            // _ls9
            // 
            this._ls9.Location = new System.Drawing.Point(71, 33);
            this._ls9.Name = "_ls9";
            this._ls9.Size = new System.Drawing.Size(13, 13);
            this._ls9.State = BEMN.Forms.LedState.Off;
            this._ls9.TabIndex = 17;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(90, 33);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(28, 13);
            this.label54.TabIndex = 16;
            this.label54.Text = "ЛС9";
            // 
            // _ls8
            // 
            this._ls8.Location = new System.Drawing.Point(6, 166);
            this._ls8.Name = "_ls8";
            this._ls8.Size = new System.Drawing.Size(13, 13);
            this._ls8.State = BEMN.Forms.LedState.Off;
            this._ls8.TabIndex = 15;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(25, 166);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(28, 13);
            this.label55.TabIndex = 14;
            this.label55.Text = "ЛС8";
            // 
            // _ls7
            // 
            this._ls7.Location = new System.Drawing.Point(6, 147);
            this._ls7.Name = "_ls7";
            this._ls7.Size = new System.Drawing.Size(13, 13);
            this._ls7.State = BEMN.Forms.LedState.Off;
            this._ls7.TabIndex = 13;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(25, 147);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(28, 13);
            this.label56.TabIndex = 12;
            this.label56.Text = "ЛС7";
            // 
            // _ls6
            // 
            this._ls6.Location = new System.Drawing.Point(6, 128);
            this._ls6.Name = "_ls6";
            this._ls6.Size = new System.Drawing.Size(13, 13);
            this._ls6.State = BEMN.Forms.LedState.Off;
            this._ls6.TabIndex = 11;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(25, 128);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(28, 13);
            this.label57.TabIndex = 10;
            this.label57.Text = "ЛС6";
            // 
            // _ls5
            // 
            this._ls5.Location = new System.Drawing.Point(6, 109);
            this._ls5.Name = "_ls5";
            this._ls5.Size = new System.Drawing.Size(13, 13);
            this._ls5.State = BEMN.Forms.LedState.Off;
            this._ls5.TabIndex = 9;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(25, 109);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(28, 13);
            this.label58.TabIndex = 8;
            this.label58.Text = "ЛС5";
            // 
            // _ls4
            // 
            this._ls4.Location = new System.Drawing.Point(6, 90);
            this._ls4.Name = "_ls4";
            this._ls4.Size = new System.Drawing.Size(13, 13);
            this._ls4.State = BEMN.Forms.LedState.Off;
            this._ls4.TabIndex = 7;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(25, 90);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(28, 13);
            this.label59.TabIndex = 6;
            this.label59.Text = "ЛС4";
            // 
            // _ls3
            // 
            this._ls3.Location = new System.Drawing.Point(6, 71);
            this._ls3.Name = "_ls3";
            this._ls3.Size = new System.Drawing.Size(13, 13);
            this._ls3.State = BEMN.Forms.LedState.Off;
            this._ls3.TabIndex = 5;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(25, 71);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(28, 13);
            this.label60.TabIndex = 4;
            this.label60.Text = "ЛС3";
            // 
            // _ls2
            // 
            this._ls2.Location = new System.Drawing.Point(6, 52);
            this._ls2.Name = "_ls2";
            this._ls2.Size = new System.Drawing.Size(13, 13);
            this._ls2.State = BEMN.Forms.LedState.Off;
            this._ls2.TabIndex = 3;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(25, 52);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(28, 13);
            this.label61.TabIndex = 2;
            this.label61.Text = "ЛС2";
            // 
            // _ls1
            // 
            this._ls1.Location = new System.Drawing.Point(6, 33);
            this._ls1.Name = "_ls1";
            this._ls1.Size = new System.Drawing.Size(13, 13);
            this._ls1.State = BEMN.Forms.LedState.Off;
            this._ls1.TabIndex = 1;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(25, 33);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(28, 13);
            this.label62.TabIndex = 0;
            this.label62.Text = "ЛС1";
            // 
            // _dataBaseTabControl
            // 
            this._dataBaseTabControl.Controls.Add(this._discretTabPage1);
            this._dataBaseTabControl.Controls.Add(this._diskretAndReleTabPage);
            this._dataBaseTabControl.Controls.Add(this._controlSignalsTabPage);
            this._dataBaseTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._dataBaseTabControl.Location = new System.Drawing.Point(0, 0);
            this._dataBaseTabControl.Name = "_dataBaseTabControl";
            this._dataBaseTabControl.SelectedIndex = 0;
            this._dataBaseTabControl.Size = new System.Drawing.Size(941, 625);
            this._dataBaseTabControl.TabIndex = 1;
            // 
            // _diskretAndReleTabPage
            // 
            this._diskretAndReleTabPage.Controls.Add(this.groupBox18);
            this._diskretAndReleTabPage.Controls.Add(this._module35);
            this._diskretAndReleTabPage.Controls.Add(this._module42);
            this._diskretAndReleTabPage.Controls.Add(this._rsTriggersGB);
            this._diskretAndReleTabPage.Controls.Add(this._module47);
            this._diskretAndReleTabPage.Controls.Add(this._r56Label);
            this._diskretAndReleTabPage.Controls.Add(this._module36);
            this._diskretAndReleTabPage.Controls.Add(this._r58Label);
            this._diskretAndReleTabPage.Controls.Add(this._module43);
            this._diskretAndReleTabPage.Controls.Add(this._r70Label);
            this._diskretAndReleTabPage.Controls.Add(this._module48);
            this._diskretAndReleTabPage.Controls.Add(this._r18Label);
            this._diskretAndReleTabPage.Controls.Add(this._module41);
            this._diskretAndReleTabPage.Controls.Add(this._r59Label);
            this._diskretAndReleTabPage.Controls.Add(this._module44);
            this._diskretAndReleTabPage.Controls.Add(this._r71Label);
            this._diskretAndReleTabPage.Controls.Add(this._module37);
            this._diskretAndReleTabPage.Controls.Add(this.label105);
            this._diskretAndReleTabPage.Controls.Add(this._module49);
            this._diskretAndReleTabPage.Controls.Add(this._r64Label);
            this._diskretAndReleTabPage.Controls.Add(this._module45);
            this._diskretAndReleTabPage.Controls.Add(this.label106);
            this._diskretAndReleTabPage.Controls.Add(this._r40Label);
            this._diskretAndReleTabPage.Controls.Add(this._r76Label);
            this._diskretAndReleTabPage.Controls.Add(this._module38);
            this._diskretAndReleTabPage.Controls.Add(this._r69Label);
            this._diskretAndReleTabPage.Controls.Add(this._module50);
            this._diskretAndReleTabPage.Controls.Add(this._r55Label);
            this._diskretAndReleTabPage.Controls.Add(this._module46);
            this._diskretAndReleTabPage.Controls.Add(this._r51Label);
            this._diskretAndReleTabPage.Controls.Add(this._module39);
            this._diskretAndReleTabPage.Controls.Add(this.label107);
            this._diskretAndReleTabPage.Controls.Add(this._module40);
            this._diskretAndReleTabPage.Controls.Add(this._r60Label);
            this._diskretAndReleTabPage.Controls.Add(this._r43Label);
            this._diskretAndReleTabPage.Controls.Add(this.label108);
            this._diskretAndReleTabPage.Controls.Add(this._r47Label);
            this._diskretAndReleTabPage.Controls.Add(this._r72Label);
            this._diskretAndReleTabPage.Controls.Add(this._r35Label);
            this._diskretAndReleTabPage.Controls.Add(this._r57Label);
            this._diskretAndReleTabPage.Controls.Add(this._r36Label);
            this._diskretAndReleTabPage.Controls.Add(this._r65Label);
            this._diskretAndReleTabPage.Controls.Add(this._r41Label);
            this._diskretAndReleTabPage.Controls.Add(this._r80Label);
            this._diskretAndReleTabPage.Controls.Add(this._r48Label);
            this._diskretAndReleTabPage.Controls.Add(this._r77Label);
            this._diskretAndReleTabPage.Controls.Add(this._r42Label);
            this._diskretAndReleTabPage.Controls.Add(this._r68Label);
            this._diskretAndReleTabPage.Controls.Add(this._r44Label);
            this._diskretAndReleTabPage.Controls.Add(this.label109);
            this._diskretAndReleTabPage.Controls.Add(this._r37Label);
            this._diskretAndReleTabPage.Controls.Add(this._r75Label);
            this._diskretAndReleTabPage.Controls.Add(this._r39Label);
            this._diskretAndReleTabPage.Controls.Add(this._r54Label);
            this._diskretAndReleTabPage.Controls.Add(this._r49Label);
            this._diskretAndReleTabPage.Controls.Add(this._r61Label);
            this._diskretAndReleTabPage.Controls.Add(this._r45Label);
            this._diskretAndReleTabPage.Controls.Add(this._r63Label);
            this._diskretAndReleTabPage.Controls.Add(this._r38Label);
            this._diskretAndReleTabPage.Controls.Add(this._r73Label);
            this._diskretAndReleTabPage.Controls.Add(this._r50Label);
            this._diskretAndReleTabPage.Controls.Add(this.label110);
            this._diskretAndReleTabPage.Controls.Add(this._r46Label);
            this._diskretAndReleTabPage.Controls.Add(this._r66Label);
            this._diskretAndReleTabPage.Controls.Add(this._r78Label);
            this._diskretAndReleTabPage.Controls.Add(this.label135);
            this._diskretAndReleTabPage.Controls.Add(this.label136);
            this._diskretAndReleTabPage.Controls.Add(this._r53Label);
            this._diskretAndReleTabPage.Controls.Add(this.label137);
            this._diskretAndReleTabPage.Controls.Add(this._r62Label);
            this._diskretAndReleTabPage.Controls.Add(this._r74Label);
            this._diskretAndReleTabPage.Controls.Add(this._r67Label);
            this._diskretAndReleTabPage.Controls.Add(this._r34Label);
            this._diskretAndReleTabPage.Controls.Add(this._r79Label);
            this._diskretAndReleTabPage.Controls.Add(this._r52Label);
            this._diskretAndReleTabPage.Controls.Add(this.label143);
            this._diskretAndReleTabPage.Controls.Add(this.label144);
            this._diskretAndReleTabPage.Controls.Add(this.label145);
            this._diskretAndReleTabPage.Controls.Add(this.label146);
            this._diskretAndReleTabPage.Controls.Add(this.label147);
            this._diskretAndReleTabPage.Controls.Add(this.label148);
            this._diskretAndReleTabPage.Controls.Add(this.label149);
            this._diskretAndReleTabPage.Controls.Add(this.label151);
            this._diskretAndReleTabPage.Controls.Add(this._r30Label);
            this._diskretAndReleTabPage.Controls.Add(this._r29Label);
            this._diskretAndReleTabPage.Controls.Add(this._r28Label);
            this._diskretAndReleTabPage.Controls.Add(this._r27Label);
            this._diskretAndReleTabPage.Controls.Add(this._r33Label);
            this._diskretAndReleTabPage.Controls.Add(this._r26Label);
            this._diskretAndReleTabPage.Controls.Add(this._r25Label);
            this._diskretAndReleTabPage.Controls.Add(this._r24Label);
            this._diskretAndReleTabPage.Controls.Add(this._r23Label);
            this._diskretAndReleTabPage.Controls.Add(this._r31Label);
            this._diskretAndReleTabPage.Controls.Add(this._r22Label);
            this._diskretAndReleTabPage.Controls.Add(this._r21Label);
            this._diskretAndReleTabPage.Controls.Add(this._r20Label);
            this._diskretAndReleTabPage.Controls.Add(this._r19Label);
            this._diskretAndReleTabPage.Controls.Add(this._r32Label);
            this._diskretAndReleTabPage.Controls.Add(this._module56);
            this._diskretAndReleTabPage.Controls.Add(this._module57);
            this._diskretAndReleTabPage.Controls.Add(this._module18);
            this._diskretAndReleTabPage.Controls.Add(this._module10);
            this._diskretAndReleTabPage.Controls.Add(this._module69);
            this._diskretAndReleTabPage.Controls.Add(this._module58);
            this._diskretAndReleTabPage.Controls.Add(this._module70);
            this._diskretAndReleTabPage.Controls.Add(this._module80);
            this._diskretAndReleTabPage.Controls.Add(this._module17);
            this._diskretAndReleTabPage.Controls.Add(this._module51);
            this._diskretAndReleTabPage.Controls.Add(this._module68);
            this._diskretAndReleTabPage.Controls.Add(this._module55);
            this._diskretAndReleTabPage.Controls.Add(this._module59);
            this._diskretAndReleTabPage.Controls.Add(this._module9);
            this._diskretAndReleTabPage.Controls.Add(this._module71);
            this._diskretAndReleTabPage.Controls.Add(this._module75);
            this._diskretAndReleTabPage.Controls.Add(this._module16);
            this._diskretAndReleTabPage.Controls.Add(this._module64);
            this._diskretAndReleTabPage.Controls.Add(this._module76);
            this._diskretAndReleTabPage.Controls.Add(this._module63);
            this._diskretAndReleTabPage.Controls.Add(this._module15);
            this._diskretAndReleTabPage.Controls.Add(this._module5);
            this._diskretAndReleTabPage.Controls.Add(this._module54);
            this._diskretAndReleTabPage.Controls.Add(this._module34);
            this._diskretAndReleTabPage.Controls.Add(this._module60);
            this._diskretAndReleTabPage.Controls.Add(this._module72);
            this._diskretAndReleTabPage.Controls.Add(this._module65);
            this._diskretAndReleTabPage.Controls.Add(this._module8);
            this._diskretAndReleTabPage.Controls.Add(this._module77);
            this._diskretAndReleTabPage.Controls.Add(this._module14);
            this._diskretAndReleTabPage.Controls.Add(this._module53);
            this._diskretAndReleTabPage.Controls.Add(this._module79);
            this._diskretAndReleTabPage.Controls.Add(this._module61);
            this._diskretAndReleTabPage.Controls.Add(this._module73);
            this._diskretAndReleTabPage.Controls.Add(this._module67);
            this._diskretAndReleTabPage.Controls.Add(this._module66);
            this._diskretAndReleTabPage.Controls.Add(this._module13);
            this._diskretAndReleTabPage.Controls.Add(this._module78);
            this._diskretAndReleTabPage.Controls.Add(this._module1);
            this._diskretAndReleTabPage.Controls.Add(this._module74);
            this._diskretAndReleTabPage.Controls.Add(this._module62);
            this._diskretAndReleTabPage.Controls.Add(this._module52);
            this._diskretAndReleTabPage.Controls.Add(this._module7);
            this._diskretAndReleTabPage.Controls.Add(this._module12);
            this._diskretAndReleTabPage.Controls.Add(this._module11);
            this._diskretAndReleTabPage.Controls.Add(this._module2);
            this._diskretAndReleTabPage.Controls.Add(this._module6);
            this._diskretAndReleTabPage.Controls.Add(this._module3);
            this._diskretAndReleTabPage.Controls.Add(this._module33);
            this._diskretAndReleTabPage.Controls.Add(this._module4);
            this._diskretAndReleTabPage.Controls.Add(this._module19);
            this._diskretAndReleTabPage.Controls.Add(this._module29);
            this._diskretAndReleTabPage.Controls.Add(this._module28);
            this._diskretAndReleTabPage.Controls.Add(this._module27);
            this._diskretAndReleTabPage.Controls.Add(this._module30);
            this._diskretAndReleTabPage.Controls.Add(this._module26);
            this._diskretAndReleTabPage.Controls.Add(this._module25);
            this._diskretAndReleTabPage.Controls.Add(this._module24);
            this._diskretAndReleTabPage.Controls.Add(this._module23);
            this._diskretAndReleTabPage.Controls.Add(this._module22);
            this._diskretAndReleTabPage.Controls.Add(this._module21);
            this._diskretAndReleTabPage.Controls.Add(this._module20);
            this._diskretAndReleTabPage.Controls.Add(this._module32);
            this._diskretAndReleTabPage.Controls.Add(this._module31);
            this._diskretAndReleTabPage.Controls.Add(this._virtualReleGB);
            this._diskretAndReleTabPage.Controls.Add(this._releGB);
            this._diskretAndReleTabPage.Controls.Add(this._discretsGB);
            this._diskretAndReleTabPage.Location = new System.Drawing.Point(4, 22);
            this._diskretAndReleTabPage.Name = "_diskretAndReleTabPage";
            this._diskretAndReleTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._diskretAndReleTabPage.Size = new System.Drawing.Size(933, 599);
            this._diskretAndReleTabPage.TabIndex = 3;
            this._diskretAndReleTabPage.Text = "Реле и Дискреты ";
            this._diskretAndReleTabPage.UseVisualStyleBackColor = true;
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this.diod12);
            this.groupBox18.Controls.Add(this.diod11);
            this.groupBox18.Controls.Add(this.diod10);
            this.groupBox18.Controls.Add(this.diod9);
            this.groupBox18.Controls.Add(this.diod8);
            this.groupBox18.Controls.Add(this.diod7);
            this.groupBox18.Controls.Add(this.diod6);
            this.groupBox18.Controls.Add(this.diod5);
            this.groupBox18.Controls.Add(this.diod4);
            this.groupBox18.Controls.Add(this.diod3);
            this.groupBox18.Controls.Add(this.diod2);
            this.groupBox18.Controls.Add(this.diod1);
            this.groupBox18.Controls.Add(this.label190);
            this.groupBox18.Controls.Add(this.label189);
            this.groupBox18.Controls.Add(this.label179);
            this.groupBox18.Controls.Add(this.label180);
            this.groupBox18.Controls.Add(this.label181);
            this.groupBox18.Controls.Add(this.label182);
            this.groupBox18.Controls.Add(this.label183);
            this.groupBox18.Controls.Add(this.label184);
            this.groupBox18.Controls.Add(this.label185);
            this.groupBox18.Controls.Add(this.label186);
            this.groupBox18.Controls.Add(this.label187);
            this.groupBox18.Controls.Add(this.label188);
            this.groupBox18.Location = new System.Drawing.Point(8, 3);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(48, 259);
            this.groupBox18.TabIndex = 220;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "Инд.";
            // 
            // diod12
            // 
            this.diod12.BackColor = System.Drawing.Color.Transparent;
            this.diod12.Location = new System.Drawing.Point(6, 235);
            this.diod12.Name = "diod12";
            this.diod12.Size = new System.Drawing.Size(14, 14);
            this.diod12.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod12.TabIndex = 30;
            // 
            // diod11
            // 
            this.diod11.BackColor = System.Drawing.Color.Transparent;
            this.diod11.Location = new System.Drawing.Point(6, 215);
            this.diod11.Name = "diod11";
            this.diod11.Size = new System.Drawing.Size(14, 14);
            this.diod11.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod11.TabIndex = 31;
            // 
            // diod10
            // 
            this.diod10.BackColor = System.Drawing.Color.Transparent;
            this.diod10.Location = new System.Drawing.Point(6, 195);
            this.diod10.Name = "diod10";
            this.diod10.Size = new System.Drawing.Size(14, 14);
            this.diod10.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod10.TabIndex = 32;
            // 
            // diod9
            // 
            this.diod9.BackColor = System.Drawing.Color.Transparent;
            this.diod9.Location = new System.Drawing.Point(6, 175);
            this.diod9.Name = "diod9";
            this.diod9.Size = new System.Drawing.Size(14, 14);
            this.diod9.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod9.TabIndex = 33;
            // 
            // diod8
            // 
            this.diod8.BackColor = System.Drawing.Color.Transparent;
            this.diod8.Location = new System.Drawing.Point(6, 155);
            this.diod8.Name = "diod8";
            this.diod8.Size = new System.Drawing.Size(14, 14);
            this.diod8.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod8.TabIndex = 34;
            // 
            // diod7
            // 
            this.diod7.BackColor = System.Drawing.Color.Transparent;
            this.diod7.Location = new System.Drawing.Point(6, 135);
            this.diod7.Name = "diod7";
            this.diod7.Size = new System.Drawing.Size(14, 14);
            this.diod7.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod7.TabIndex = 35;
            // 
            // diod6
            // 
            this.diod6.BackColor = System.Drawing.Color.Transparent;
            this.diod6.Location = new System.Drawing.Point(6, 115);
            this.diod6.Name = "diod6";
            this.diod6.Size = new System.Drawing.Size(14, 14);
            this.diod6.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod6.TabIndex = 36;
            // 
            // diod5
            // 
            this.diod5.BackColor = System.Drawing.Color.Transparent;
            this.diod5.Location = new System.Drawing.Point(6, 95);
            this.diod5.Name = "diod5";
            this.diod5.Size = new System.Drawing.Size(14, 14);
            this.diod5.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod5.TabIndex = 37;
            // 
            // diod4
            // 
            this.diod4.BackColor = System.Drawing.Color.Transparent;
            this.diod4.Location = new System.Drawing.Point(6, 75);
            this.diod4.Name = "diod4";
            this.diod4.Size = new System.Drawing.Size(14, 14);
            this.diod4.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod4.TabIndex = 38;
            // 
            // diod3
            // 
            this.diod3.BackColor = System.Drawing.Color.Transparent;
            this.diod3.Location = new System.Drawing.Point(6, 55);
            this.diod3.Name = "diod3";
            this.diod3.Size = new System.Drawing.Size(14, 14);
            this.diod3.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod3.TabIndex = 39;
            // 
            // diod2
            // 
            this.diod2.BackColor = System.Drawing.Color.Transparent;
            this.diod2.Location = new System.Drawing.Point(6, 35);
            this.diod2.Name = "diod2";
            this.diod2.Size = new System.Drawing.Size(14, 14);
            this.diod2.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod2.TabIndex = 40;
            // 
            // diod1
            // 
            this.diod1.BackColor = System.Drawing.Color.Transparent;
            this.diod1.Location = new System.Drawing.Point(6, 15);
            this.diod1.Name = "diod1";
            this.diod1.Size = new System.Drawing.Size(14, 14);
            this.diod1.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod1.TabIndex = 41;
            // 
            // label190
            // 
            this.label190.AutoSize = true;
            this.label190.Location = new System.Drawing.Point(25, 216);
            this.label190.Name = "label190";
            this.label190.Size = new System.Drawing.Size(19, 13);
            this.label190.TabIndex = 28;
            this.label190.Text = "11";
            // 
            // label189
            // 
            this.label189.AutoSize = true;
            this.label189.Location = new System.Drawing.Point(25, 116);
            this.label189.Name = "label189";
            this.label189.Size = new System.Drawing.Size(13, 13);
            this.label189.TabIndex = 26;
            this.label189.Text = "6";
            // 
            // label179
            // 
            this.label179.AutoSize = true;
            this.label179.Location = new System.Drawing.Point(25, 196);
            this.label179.Name = "label179";
            this.label179.Size = new System.Drawing.Size(19, 13);
            this.label179.TabIndex = 24;
            this.label179.Text = "10";
            // 
            // label180
            // 
            this.label180.AutoSize = true;
            this.label180.Location = new System.Drawing.Point(25, 176);
            this.label180.Name = "label180";
            this.label180.Size = new System.Drawing.Size(13, 13);
            this.label180.TabIndex = 22;
            this.label180.Text = "9";
            // 
            // label181
            // 
            this.label181.AutoSize = true;
            this.label181.Location = new System.Drawing.Point(25, 156);
            this.label181.Name = "label181";
            this.label181.Size = new System.Drawing.Size(13, 13);
            this.label181.TabIndex = 20;
            this.label181.Text = "8";
            // 
            // label182
            // 
            this.label182.AutoSize = true;
            this.label182.Location = new System.Drawing.Point(25, 136);
            this.label182.Name = "label182";
            this.label182.Size = new System.Drawing.Size(13, 13);
            this.label182.TabIndex = 18;
            this.label182.Text = "7";
            // 
            // label183
            // 
            this.label183.AutoSize = true;
            this.label183.Location = new System.Drawing.Point(25, 236);
            this.label183.Name = "label183";
            this.label183.Size = new System.Drawing.Size(19, 13);
            this.label183.TabIndex = 16;
            this.label183.Text = "12";
            // 
            // label184
            // 
            this.label184.AutoSize = true;
            this.label184.Location = new System.Drawing.Point(25, 96);
            this.label184.Name = "label184";
            this.label184.Size = new System.Drawing.Size(13, 13);
            this.label184.TabIndex = 8;
            this.label184.Text = "5";
            // 
            // label185
            // 
            this.label185.AutoSize = true;
            this.label185.Location = new System.Drawing.Point(25, 76);
            this.label185.Name = "label185";
            this.label185.Size = new System.Drawing.Size(13, 13);
            this.label185.TabIndex = 6;
            this.label185.Text = "4";
            // 
            // label186
            // 
            this.label186.AutoSize = true;
            this.label186.Location = new System.Drawing.Point(25, 56);
            this.label186.Name = "label186";
            this.label186.Size = new System.Drawing.Size(13, 13);
            this.label186.TabIndex = 4;
            this.label186.Text = "3";
            // 
            // label187
            // 
            this.label187.AutoSize = true;
            this.label187.Location = new System.Drawing.Point(25, 36);
            this.label187.Name = "label187";
            this.label187.Size = new System.Drawing.Size(13, 13);
            this.label187.TabIndex = 2;
            this.label187.Text = "2";
            // 
            // label188
            // 
            this.label188.AutoSize = true;
            this.label188.Location = new System.Drawing.Point(25, 16);
            this.label188.Name = "label188";
            this.label188.Size = new System.Drawing.Size(13, 13);
            this.label188.TabIndex = 0;
            this.label188.Text = "1";
            // 
            // _module35
            // 
            this._module35.Location = new System.Drawing.Point(155, 22);
            this._module35.Name = "_module35";
            this._module35.Size = new System.Drawing.Size(13, 13);
            this._module35.State = BEMN.Forms.LedState.Off;
            this._module35.TabIndex = 130;
            // 
            // _module42
            // 
            this._module42.Location = new System.Drawing.Point(155, 155);
            this._module42.Name = "_module42";
            this._module42.Size = new System.Drawing.Size(13, 13);
            this._module42.State = BEMN.Forms.LedState.Off;
            this._module42.TabIndex = 134;
            // 
            // _rsTriggersGB
            // 
            this._rsTriggersGB.Controls.Add(this._rst8);
            this._rsTriggersGB.Controls.Add(this.label12);
            this._rsTriggersGB.Controls.Add(this._rst7);
            this._rsTriggersGB.Controls.Add(this.label96);
            this._rsTriggersGB.Controls.Add(this._rst1);
            this._rsTriggersGB.Controls.Add(this._rst6);
            this._rsTriggersGB.Controls.Add(this.label99);
            this._rsTriggersGB.Controls.Add(this.label102);
            this._rsTriggersGB.Controls.Add(this.label103);
            this._rsTriggersGB.Controls.Add(this._rst5);
            this._rsTriggersGB.Controls.Add(this._rst2);
            this._rsTriggersGB.Controls.Add(this.label150);
            this._rsTriggersGB.Controls.Add(this.label104);
            this._rsTriggersGB.Controls.Add(this._rst4);
            this._rsTriggersGB.Controls.Add(this._rst16);
            this._rsTriggersGB.Controls.Add(this._rst3);
            this._rsTriggersGB.Controls.Add(this.label207);
            this._rsTriggersGB.Controls.Add(this.label208);
            this._rsTriggersGB.Controls.Add(this._rst15);
            this._rsTriggersGB.Controls.Add(this._rst9);
            this._rsTriggersGB.Controls.Add(this.label209);
            this._rsTriggersGB.Controls.Add(this.label211);
            this._rsTriggersGB.Controls.Add(this._rst14);
            this._rsTriggersGB.Controls.Add(this.label214);
            this._rsTriggersGB.Controls.Add(this.label215);
            this._rsTriggersGB.Controls.Add(this._rst10);
            this._rsTriggersGB.Controls.Add(this._rst13);
            this._rsTriggersGB.Controls.Add(this.label216);
            this._rsTriggersGB.Controls.Add(this.label217);
            this._rsTriggersGB.Controls.Add(this._rst11);
            this._rsTriggersGB.Controls.Add(this._rst12);
            this._rsTriggersGB.Controls.Add(this.label219);
            this._rsTriggersGB.Location = new System.Drawing.Point(789, 3);
            this._rsTriggersGB.Name = "_rsTriggersGB";
            this._rsTriggersGB.Size = new System.Drawing.Size(136, 199);
            this._rsTriggersGB.TabIndex = 219;
            this._rsTriggersGB.TabStop = false;
            this._rsTriggersGB.Text = "Энергонезависимые RS-триггеры";
            // 
            // _rst8
            // 
            this._rst8.Location = new System.Drawing.Point(6, 171);
            this._rst8.Name = "_rst8";
            this._rst8.Size = new System.Drawing.Size(13, 13);
            this._rst8.State = BEMN.Forms.LedState.Off;
            this._rst8.TabIndex = 15;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(21, 171);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(35, 13);
            this.label12.TabIndex = 14;
            this.label12.Text = "RST8";
            // 
            // _rst7
            // 
            this._rst7.Location = new System.Drawing.Point(6, 152);
            this._rst7.Name = "_rst7";
            this._rst7.Size = new System.Drawing.Size(13, 13);
            this._rst7.State = BEMN.Forms.LedState.Off;
            this._rst7.TabIndex = 13;
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(21, 152);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(35, 13);
            this.label96.TabIndex = 12;
            this.label96.Text = "RST7";
            // 
            // _rst1
            // 
            this._rst1.Location = new System.Drawing.Point(6, 38);
            this._rst1.Name = "_rst1";
            this._rst1.Size = new System.Drawing.Size(13, 13);
            this._rst1.State = BEMN.Forms.LedState.Off;
            this._rst1.TabIndex = 1;
            // 
            // _rst6
            // 
            this._rst6.Location = new System.Drawing.Point(6, 133);
            this._rst6.Name = "_rst6";
            this._rst6.Size = new System.Drawing.Size(13, 13);
            this._rst6.State = BEMN.Forms.LedState.Off;
            this._rst6.TabIndex = 11;
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(21, 38);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(35, 13);
            this.label99.TabIndex = 0;
            this.label99.Text = "RST1";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Location = new System.Drawing.Point(21, 133);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(35, 13);
            this.label102.TabIndex = 10;
            this.label102.Text = "RST6";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(21, 57);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(35, 13);
            this.label103.TabIndex = 2;
            this.label103.Text = "RST2";
            // 
            // _rst5
            // 
            this._rst5.Location = new System.Drawing.Point(6, 114);
            this._rst5.Name = "_rst5";
            this._rst5.Size = new System.Drawing.Size(13, 13);
            this._rst5.State = BEMN.Forms.LedState.Off;
            this._rst5.TabIndex = 9;
            // 
            // _rst2
            // 
            this._rst2.Location = new System.Drawing.Point(6, 57);
            this._rst2.Name = "_rst2";
            this._rst2.Size = new System.Drawing.Size(13, 13);
            this._rst2.State = BEMN.Forms.LedState.Off;
            this._rst2.TabIndex = 3;
            // 
            // label150
            // 
            this.label150.AutoSize = true;
            this.label150.Location = new System.Drawing.Point(21, 115);
            this.label150.Name = "label150";
            this.label150.Size = new System.Drawing.Size(35, 13);
            this.label150.TabIndex = 8;
            this.label150.Text = "RST5";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Location = new System.Drawing.Point(21, 76);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(35, 13);
            this.label104.TabIndex = 4;
            this.label104.Text = "RST3";
            // 
            // _rst4
            // 
            this._rst4.Location = new System.Drawing.Point(6, 95);
            this._rst4.Name = "_rst4";
            this._rst4.Size = new System.Drawing.Size(13, 13);
            this._rst4.State = BEMN.Forms.LedState.Off;
            this._rst4.TabIndex = 7;
            // 
            // _rst16
            // 
            this._rst16.Location = new System.Drawing.Point(62, 171);
            this._rst16.Name = "_rst16";
            this._rst16.Size = new System.Drawing.Size(13, 13);
            this._rst16.State = BEMN.Forms.LedState.Off;
            this._rst16.TabIndex = 15;
            // 
            // _rst3
            // 
            this._rst3.Location = new System.Drawing.Point(6, 76);
            this._rst3.Name = "_rst3";
            this._rst3.Size = new System.Drawing.Size(13, 13);
            this._rst3.State = BEMN.Forms.LedState.Off;
            this._rst3.TabIndex = 5;
            // 
            // label207
            // 
            this.label207.AutoSize = true;
            this.label207.Location = new System.Drawing.Point(78, 171);
            this.label207.Name = "label207";
            this.label207.Size = new System.Drawing.Size(41, 13);
            this.label207.TabIndex = 14;
            this.label207.Text = "RST16";
            // 
            // label208
            // 
            this.label208.AutoSize = true;
            this.label208.Location = new System.Drawing.Point(21, 94);
            this.label208.Name = "label208";
            this.label208.Size = new System.Drawing.Size(35, 13);
            this.label208.TabIndex = 6;
            this.label208.Text = "RST4";
            // 
            // _rst15
            // 
            this._rst15.Location = new System.Drawing.Point(62, 152);
            this._rst15.Name = "_rst15";
            this._rst15.Size = new System.Drawing.Size(13, 13);
            this._rst15.State = BEMN.Forms.LedState.Off;
            this._rst15.TabIndex = 13;
            // 
            // _rst9
            // 
            this._rst9.Location = new System.Drawing.Point(62, 38);
            this._rst9.Name = "_rst9";
            this._rst9.Size = new System.Drawing.Size(13, 13);
            this._rst9.State = BEMN.Forms.LedState.Off;
            this._rst9.TabIndex = 1;
            // 
            // label209
            // 
            this.label209.AutoSize = true;
            this.label209.Location = new System.Drawing.Point(78, 152);
            this.label209.Name = "label209";
            this.label209.Size = new System.Drawing.Size(41, 13);
            this.label209.TabIndex = 12;
            this.label209.Text = "RST15";
            // 
            // label211
            // 
            this.label211.AutoSize = true;
            this.label211.Location = new System.Drawing.Point(78, 38);
            this.label211.Name = "label211";
            this.label211.Size = new System.Drawing.Size(35, 13);
            this.label211.TabIndex = 0;
            this.label211.Text = "RST9";
            // 
            // _rst14
            // 
            this._rst14.Location = new System.Drawing.Point(62, 133);
            this._rst14.Name = "_rst14";
            this._rst14.Size = new System.Drawing.Size(13, 13);
            this._rst14.State = BEMN.Forms.LedState.Off;
            this._rst14.TabIndex = 11;
            // 
            // label214
            // 
            this.label214.AutoSize = true;
            this.label214.Location = new System.Drawing.Point(78, 57);
            this.label214.Name = "label214";
            this.label214.Size = new System.Drawing.Size(41, 13);
            this.label214.TabIndex = 2;
            this.label214.Text = "RST10";
            // 
            // label215
            // 
            this.label215.AutoSize = true;
            this.label215.Location = new System.Drawing.Point(78, 133);
            this.label215.Name = "label215";
            this.label215.Size = new System.Drawing.Size(41, 13);
            this.label215.TabIndex = 10;
            this.label215.Text = "RST14";
            // 
            // _rst10
            // 
            this._rst10.Location = new System.Drawing.Point(62, 57);
            this._rst10.Name = "_rst10";
            this._rst10.Size = new System.Drawing.Size(13, 13);
            this._rst10.State = BEMN.Forms.LedState.Off;
            this._rst10.TabIndex = 3;
            // 
            // _rst13
            // 
            this._rst13.Location = new System.Drawing.Point(62, 114);
            this._rst13.Name = "_rst13";
            this._rst13.Size = new System.Drawing.Size(13, 13);
            this._rst13.State = BEMN.Forms.LedState.Off;
            this._rst13.TabIndex = 9;
            // 
            // label216
            // 
            this.label216.AutoSize = true;
            this.label216.Location = new System.Drawing.Point(78, 76);
            this.label216.Name = "label216";
            this.label216.Size = new System.Drawing.Size(41, 13);
            this.label216.TabIndex = 4;
            this.label216.Text = "RST11";
            // 
            // label217
            // 
            this.label217.AutoSize = true;
            this.label217.Location = new System.Drawing.Point(78, 114);
            this.label217.Name = "label217";
            this.label217.Size = new System.Drawing.Size(41, 13);
            this.label217.TabIndex = 8;
            this.label217.Text = "RST13";
            // 
            // _rst11
            // 
            this._rst11.Location = new System.Drawing.Point(62, 76);
            this._rst11.Name = "_rst11";
            this._rst11.Size = new System.Drawing.Size(13, 13);
            this._rst11.State = BEMN.Forms.LedState.Off;
            this._rst11.TabIndex = 5;
            // 
            // _rst12
            // 
            this._rst12.Location = new System.Drawing.Point(62, 95);
            this._rst12.Name = "_rst12";
            this._rst12.Size = new System.Drawing.Size(13, 13);
            this._rst12.State = BEMN.Forms.LedState.Off;
            this._rst12.TabIndex = 7;
            // 
            // label219
            // 
            this.label219.AutoSize = true;
            this.label219.Location = new System.Drawing.Point(78, 95);
            this.label219.Name = "label219";
            this.label219.Size = new System.Drawing.Size(41, 13);
            this.label219.TabIndex = 6;
            this.label219.Text = "RST12";
            // 
            // _module47
            // 
            this._module47.Location = new System.Drawing.Point(155, 250);
            this._module47.Name = "_module47";
            this._module47.Size = new System.Drawing.Size(13, 13);
            this._module47.State = BEMN.Forms.LedState.Off;
            this._module47.TabIndex = 136;
            // 
            // _r56Label
            // 
            this._r56Label.AutoSize = true;
            this._r56Label.Location = new System.Drawing.Point(215, 98);
            this._r56Label.Name = "_r56Label";
            this._r56Label.Size = new System.Drawing.Size(19, 13);
            this._r56Label.TabIndex = 215;
            this._r56Label.Text = "56";
            // 
            // _module36
            // 
            this._module36.Location = new System.Drawing.Point(155, 41);
            this._module36.Name = "_module36";
            this._module36.Size = new System.Drawing.Size(13, 13);
            this._module36.State = BEMN.Forms.LedState.Off;
            this._module36.TabIndex = 147;
            // 
            // _r58Label
            // 
            this._r58Label.AutoSize = true;
            this._r58Label.Location = new System.Drawing.Point(215, 136);
            this._r58Label.Name = "_r58Label";
            this._r58Label.Size = new System.Drawing.Size(19, 13);
            this._r58Label.TabIndex = 108;
            this._r58Label.Text = "58";
            // 
            // _module43
            // 
            this._module43.Location = new System.Drawing.Point(155, 174);
            this._module43.Name = "_module43";
            this._module43.Size = new System.Drawing.Size(13, 13);
            this._module43.State = BEMN.Forms.LedState.Off;
            this._module43.TabIndex = 149;
            // 
            // _r70Label
            // 
            this._r70Label.AutoSize = true;
            this._r70Label.Location = new System.Drawing.Point(274, 232);
            this._r70Label.Name = "_r70Label";
            this._r70Label.Size = new System.Drawing.Size(19, 13);
            this._r70Label.TabIndex = 107;
            this._r70Label.Text = "70";
            // 
            // _module48
            // 
            this._module48.Location = new System.Drawing.Point(155, 269);
            this._module48.Name = "_module48";
            this._module48.Size = new System.Drawing.Size(13, 13);
            this._module48.State = BEMN.Forms.LedState.Off;
            this._module48.TabIndex = 150;
            // 
            // _r18Label
            // 
            this._r18Label.AutoSize = true;
            this._r18Label.Location = new System.Drawing.Point(130, 22);
            this._r18Label.Name = "_r18Label";
            this._r18Label.Size = new System.Drawing.Size(19, 13);
            this._r18Label.TabIndex = 95;
            this._r18Label.Text = "18";
            // 
            // _module41
            // 
            this._module41.Location = new System.Drawing.Point(155, 136);
            this._module41.Name = "_module41";
            this._module41.Size = new System.Drawing.Size(13, 13);
            this._module41.State = BEMN.Forms.LedState.Off;
            this._module41.TabIndex = 112;
            // 
            // _r59Label
            // 
            this._r59Label.AutoSize = true;
            this._r59Label.Location = new System.Drawing.Point(274, 22);
            this._r59Label.Name = "_r59Label";
            this._r59Label.Size = new System.Drawing.Size(19, 13);
            this._r59Label.TabIndex = 120;
            this._r59Label.Text = "59";
            // 
            // _module44
            // 
            this._module44.Location = new System.Drawing.Point(155, 193);
            this._module44.Name = "_module44";
            this._module44.Size = new System.Drawing.Size(13, 13);
            this._module44.State = BEMN.Forms.LedState.Off;
            this._module44.TabIndex = 169;
            // 
            // _r71Label
            // 
            this._r71Label.AutoSize = true;
            this._r71Label.Location = new System.Drawing.Point(274, 250);
            this._r71Label.Name = "_r71Label";
            this._r71Label.Size = new System.Drawing.Size(19, 13);
            this._r71Label.TabIndex = 121;
            this._r71Label.Text = "71";
            // 
            // _module37
            // 
            this._module37.Location = new System.Drawing.Point(155, 60);
            this._module37.Name = "_module37";
            this._module37.Size = new System.Drawing.Size(13, 13);
            this._module37.State = BEMN.Forms.LedState.Off;
            this._module37.TabIndex = 167;
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Location = new System.Drawing.Point(88, 326);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(19, 13);
            this.label105.TabIndex = 91;
            this.label105.Text = "17";
            // 
            // _module49
            // 
            this._module49.Location = new System.Drawing.Point(155, 288);
            this._module49.Name = "_module49";
            this._module49.Size = new System.Drawing.Size(13, 13);
            this._module49.State = BEMN.Forms.LedState.Off;
            this._module49.TabIndex = 168;
            // 
            // _r64Label
            // 
            this._r64Label.AutoSize = true;
            this._r64Label.Location = new System.Drawing.Point(274, 117);
            this._r64Label.Name = "_r64Label";
            this._r64Label.Size = new System.Drawing.Size(19, 13);
            this._r64Label.TabIndex = 122;
            this._r64Label.Text = "64";
            // 
            // _module45
            // 
            this._module45.Location = new System.Drawing.Point(155, 212);
            this._module45.Name = "_module45";
            this._module45.Size = new System.Drawing.Size(13, 13);
            this._module45.State = BEMN.Forms.LedState.Off;
            this._module45.TabIndex = 188;
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Location = new System.Drawing.Point(88, 193);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(19, 13);
            this.label106.TabIndex = 145;
            this.label106.Text = "10";
            // 
            // _r40Label
            // 
            this._r40Label.AutoSize = true;
            this._r40Label.Location = new System.Drawing.Point(174, 116);
            this._r40Label.Name = "_r40Label";
            this._r40Label.Size = new System.Drawing.Size(19, 13);
            this._r40Label.TabIndex = 194;
            this._r40Label.Text = "40";
            // 
            // _r76Label
            // 
            this._r76Label.AutoSize = true;
            this._r76Label.Location = new System.Drawing.Point(318, 22);
            this._r76Label.Name = "_r76Label";
            this._r76Label.Size = new System.Drawing.Size(19, 13);
            this._r76Label.TabIndex = 123;
            this._r76Label.Text = "76";
            // 
            // _module38
            // 
            this._module38.Location = new System.Drawing.Point(155, 79);
            this._module38.Name = "_module38";
            this._module38.Size = new System.Drawing.Size(13, 13);
            this._module38.State = BEMN.Forms.LedState.Off;
            this._module38.TabIndex = 187;
            // 
            // _r69Label
            // 
            this._r69Label.AutoSize = true;
            this._r69Label.Location = new System.Drawing.Point(274, 212);
            this._r69Label.Name = "_r69Label";
            this._r69Label.Size = new System.Drawing.Size(19, 13);
            this._r69Label.TabIndex = 193;
            this._r69Label.Text = "69";
            // 
            // _module50
            // 
            this._module50.Location = new System.Drawing.Point(155, 307);
            this._module50.Name = "_module50";
            this._module50.Size = new System.Drawing.Size(13, 13);
            this._module50.State = BEMN.Forms.LedState.Off;
            this._module50.TabIndex = 184;
            // 
            // _r55Label
            // 
            this._r55Label.AutoSize = true;
            this._r55Label.Location = new System.Drawing.Point(215, 79);
            this._r55Label.Name = "_r55Label";
            this._r55Label.Size = new System.Drawing.Size(19, 13);
            this._r55Label.TabIndex = 179;
            this._r55Label.Text = "55";
            // 
            // _module46
            // 
            this._module46.Location = new System.Drawing.Point(155, 231);
            this._module46.Name = "_module46";
            this._module46.Size = new System.Drawing.Size(13, 13);
            this._module46.State = BEMN.Forms.LedState.Off;
            this._module46.TabIndex = 214;
            // 
            // _r51Label
            // 
            this._r51Label.AutoSize = true;
            this._r51Label.Location = new System.Drawing.Point(174, 326);
            this._r51Label.Name = "_r51Label";
            this._r51Label.Size = new System.Drawing.Size(19, 13);
            this._r51Label.TabIndex = 195;
            this._r51Label.Text = "51";
            // 
            // _module39
            // 
            this._module39.Location = new System.Drawing.Point(155, 98);
            this._module39.Name = "_module39";
            this._module39.Size = new System.Drawing.Size(13, 13);
            this._module39.State = BEMN.Forms.LedState.Off;
            this._module39.TabIndex = 210;
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Location = new System.Drawing.Point(88, 174);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(13, 13);
            this.label107.TabIndex = 124;
            this.label107.Text = "9";
            // 
            // _module40
            // 
            this._module40.Location = new System.Drawing.Point(155, 116);
            this._module40.Name = "_module40";
            this._module40.Size = new System.Drawing.Size(13, 13);
            this._module40.State = BEMN.Forms.LedState.Off;
            this._module40.TabIndex = 213;
            // 
            // _r60Label
            // 
            this._r60Label.AutoSize = true;
            this._r60Label.Location = new System.Drawing.Point(274, 41);
            this._r60Label.Name = "_r60Label";
            this._r60Label.Size = new System.Drawing.Size(19, 13);
            this._r60Label.TabIndex = 142;
            this._r60Label.Text = "60";
            // 
            // _r43Label
            // 
            this._r43Label.AutoSize = true;
            this._r43Label.Location = new System.Drawing.Point(174, 174);
            this._r43Label.Name = "_r43Label";
            this._r43Label.Size = new System.Drawing.Size(19, 13);
            this._r43Label.TabIndex = 137;
            this._r43Label.Text = "43";
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Location = new System.Drawing.Point(88, 307);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(19, 13);
            this.label108.TabIndex = 87;
            this.label108.Text = "16";
            // 
            // _r47Label
            // 
            this._r47Label.AutoSize = true;
            this._r47Label.Location = new System.Drawing.Point(174, 250);
            this._r47Label.Name = "_r47Label";
            this._r47Label.Size = new System.Drawing.Size(19, 13);
            this._r47Label.TabIndex = 118;
            this._r47Label.Text = "47";
            // 
            // _r72Label
            // 
            this._r72Label.AutoSize = true;
            this._r72Label.Location = new System.Drawing.Point(274, 269);
            this._r72Label.Name = "_r72Label";
            this._r72Label.Size = new System.Drawing.Size(19, 13);
            this._r72Label.TabIndex = 143;
            this._r72Label.Text = "72";
            // 
            // _r35Label
            // 
            this._r35Label.AutoSize = true;
            this._r35Label.Location = new System.Drawing.Point(174, 22);
            this._r35Label.Name = "_r35Label";
            this._r35Label.Size = new System.Drawing.Size(19, 13);
            this._r35Label.TabIndex = 125;
            this._r35Label.Text = "35";
            // 
            // _r57Label
            // 
            this._r57Label.AutoSize = true;
            this._r57Label.Location = new System.Drawing.Point(215, 116);
            this._r57Label.Name = "_r57Label";
            this._r57Label.Size = new System.Drawing.Size(19, 13);
            this._r57Label.TabIndex = 196;
            this._r57Label.Text = "57";
            // 
            // _r36Label
            // 
            this._r36Label.AutoSize = true;
            this._r36Label.Location = new System.Drawing.Point(174, 41);
            this._r36Label.Name = "_r36Label";
            this._r36Label.Size = new System.Drawing.Size(19, 13);
            this._r36Label.TabIndex = 144;
            this._r36Label.Text = "36";
            // 
            // _r65Label
            // 
            this._r65Label.AutoSize = true;
            this._r65Label.Location = new System.Drawing.Point(274, 136);
            this._r65Label.Name = "_r65Label";
            this._r65Label.Size = new System.Drawing.Size(19, 13);
            this._r65Label.TabIndex = 140;
            this._r65Label.Text = "65";
            // 
            // _r41Label
            // 
            this._r41Label.AutoSize = true;
            this._r41Label.Location = new System.Drawing.Point(174, 136);
            this._r41Label.Name = "_r41Label";
            this._r41Label.Size = new System.Drawing.Size(19, 13);
            this._r41Label.TabIndex = 109;
            this._r41Label.Text = "41";
            // 
            // _r80Label
            // 
            this._r80Label.AutoSize = true;
            this._r80Label.Location = new System.Drawing.Point(318, 98);
            this._r80Label.Name = "_r80Label";
            this._r80Label.Size = new System.Drawing.Size(19, 13);
            this._r80Label.TabIndex = 199;
            this._r80Label.Text = "80";
            // 
            // _r48Label
            // 
            this._r48Label.AutoSize = true;
            this._r48Label.Location = new System.Drawing.Point(174, 269);
            this._r48Label.Name = "_r48Label";
            this._r48Label.Size = new System.Drawing.Size(19, 13);
            this._r48Label.TabIndex = 146;
            this._r48Label.Text = "48";
            // 
            // _r77Label
            // 
            this._r77Label.AutoSize = true;
            this._r77Label.Location = new System.Drawing.Point(318, 41);
            this._r77Label.Name = "_r77Label";
            this._r77Label.Size = new System.Drawing.Size(19, 13);
            this._r77Label.TabIndex = 139;
            this._r77Label.Text = "77";
            // 
            // _r42Label
            // 
            this._r42Label.AutoSize = true;
            this._r42Label.Location = new System.Drawing.Point(174, 155);
            this._r42Label.Name = "_r42Label";
            this._r42Label.Size = new System.Drawing.Size(19, 13);
            this._r42Label.TabIndex = 117;
            this._r42Label.Text = "42";
            // 
            // _r68Label
            // 
            this._r68Label.AutoSize = true;
            this._r68Label.Location = new System.Drawing.Point(274, 193);
            this._r68Label.Name = "_r68Label";
            this._r68Label.Size = new System.Drawing.Size(19, 13);
            this._r68Label.TabIndex = 197;
            this._r68Label.Text = "68";
            // 
            // _r44Label
            // 
            this._r44Label.AutoSize = true;
            this._r44Label.Location = new System.Drawing.Point(174, 193);
            this._r44Label.Name = "_r44Label";
            this._r44Label.Size = new System.Drawing.Size(19, 13);
            this._r44Label.TabIndex = 163;
            this._r44Label.Text = "44";
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Location = new System.Drawing.Point(88, 288);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(19, 13);
            this.label109.TabIndex = 82;
            this.label109.Text = "15";
            // 
            // _r37Label
            // 
            this._r37Label.AutoSize = true;
            this._r37Label.Location = new System.Drawing.Point(174, 60);
            this._r37Label.Name = "_r37Label";
            this._r37Label.Size = new System.Drawing.Size(19, 13);
            this._r37Label.TabIndex = 157;
            this._r37Label.Text = "37";
            // 
            // _r75Label
            // 
            this._r75Label.AutoSize = true;
            this._r75Label.Location = new System.Drawing.Point(274, 326);
            this._r75Label.Name = "_r75Label";
            this._r75Label.Size = new System.Drawing.Size(19, 13);
            this._r75Label.TabIndex = 203;
            this._r75Label.Text = "75";
            // 
            // _r39Label
            // 
            this._r39Label.AutoSize = true;
            this._r39Label.Location = new System.Drawing.Point(174, 98);
            this._r39Label.Name = "_r39Label";
            this._r39Label.Size = new System.Drawing.Size(19, 13);
            this._r39Label.TabIndex = 198;
            this._r39Label.Text = "39";
            // 
            // _r54Label
            // 
            this._r54Label.AutoSize = true;
            this._r54Label.Location = new System.Drawing.Point(215, 60);
            this._r54Label.Name = "_r54Label";
            this._r54Label.Size = new System.Drawing.Size(19, 13);
            this._r54Label.TabIndex = 158;
            this._r54Label.Text = "54";
            // 
            // _r49Label
            // 
            this._r49Label.AutoSize = true;
            this._r49Label.Location = new System.Drawing.Point(174, 288);
            this._r49Label.Name = "_r49Label";
            this._r49Label.Size = new System.Drawing.Size(19, 13);
            this._r49Label.TabIndex = 164;
            this._r49Label.Text = "49";
            // 
            // _r61Label
            // 
            this._r61Label.AutoSize = true;
            this._r61Label.Location = new System.Drawing.Point(274, 60);
            this._r61Label.Name = "_r61Label";
            this._r61Label.Size = new System.Drawing.Size(19, 13);
            this._r61Label.TabIndex = 159;
            this._r61Label.Text = "61";
            // 
            // _r45Label
            // 
            this._r45Label.AutoSize = true;
            this._r45Label.Location = new System.Drawing.Point(174, 212);
            this._r45Label.Name = "_r45Label";
            this._r45Label.Size = new System.Drawing.Size(19, 13);
            this._r45Label.TabIndex = 176;
            this._r45Label.Text = "45";
            // 
            // _r63Label
            // 
            this._r63Label.AutoSize = true;
            this._r63Label.Location = new System.Drawing.Point(274, 98);
            this._r63Label.Name = "_r63Label";
            this._r63Label.Size = new System.Drawing.Size(19, 13);
            this._r63Label.TabIndex = 201;
            this._r63Label.Text = "63";
            // 
            // _r38Label
            // 
            this._r38Label.AutoSize = true;
            this._r38Label.Location = new System.Drawing.Point(174, 79);
            this._r38Label.Name = "_r38Label";
            this._r38Label.Size = new System.Drawing.Size(19, 13);
            this._r38Label.TabIndex = 183;
            this._r38Label.Text = "38";
            // 
            // _r73Label
            // 
            this._r73Label.AutoSize = true;
            this._r73Label.Location = new System.Drawing.Point(274, 288);
            this._r73Label.Name = "_r73Label";
            this._r73Label.Size = new System.Drawing.Size(19, 13);
            this._r73Label.TabIndex = 160;
            this._r73Label.Text = "73";
            // 
            // _r50Label
            // 
            this._r50Label.AutoSize = true;
            this._r50Label.Location = new System.Drawing.Point(174, 307);
            this._r50Label.Name = "_r50Label";
            this._r50Label.Size = new System.Drawing.Size(19, 13);
            this._r50Label.TabIndex = 177;
            this._r50Label.Text = "50";
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Location = new System.Drawing.Point(88, 22);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(13, 13);
            this.label110.TabIndex = 58;
            this.label110.Text = "1";
            // 
            // _r46Label
            // 
            this._r46Label.AutoSize = true;
            this._r46Label.Location = new System.Drawing.Point(174, 231);
            this._r46Label.Name = "_r46Label";
            this._r46Label.Size = new System.Drawing.Size(19, 13);
            this._r46Label.TabIndex = 202;
            this._r46Label.Text = "46";
            // 
            // _r66Label
            // 
            this._r66Label.AutoSize = true;
            this._r66Label.Location = new System.Drawing.Point(274, 155);
            this._r66Label.Name = "_r66Label";
            this._r66Label.Size = new System.Drawing.Size(19, 13);
            this._r66Label.TabIndex = 161;
            this._r66Label.Text = "66";
            // 
            // _r78Label
            // 
            this._r78Label.AutoSize = true;
            this._r78Label.Location = new System.Drawing.Point(318, 60);
            this._r78Label.Name = "_r78Label";
            this._r78Label.Size = new System.Drawing.Size(19, 13);
            this._r78Label.TabIndex = 162;
            this._r78Label.Text = "78";
            // 
            // label135
            // 
            this.label135.AutoSize = true;
            this.label135.Location = new System.Drawing.Point(88, 269);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(19, 13);
            this.label135.TabIndex = 75;
            this.label135.Text = "14";
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Location = new System.Drawing.Point(88, 155);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(13, 13);
            this.label136.TabIndex = 111;
            this.label136.Text = "8";
            // 
            // _r53Label
            // 
            this._r53Label.AutoSize = true;
            this._r53Label.Location = new System.Drawing.Point(215, 41);
            this._r53Label.Name = "_r53Label";
            this._r53Label.Size = new System.Drawing.Size(19, 13);
            this._r53Label.TabIndex = 138;
            this._r53Label.Text = "53";
            // 
            // label137
            // 
            this.label137.AutoSize = true;
            this.label137.Location = new System.Drawing.Point(88, 250);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(19, 13);
            this.label137.TabIndex = 71;
            this.label137.Text = "13";
            // 
            // _r62Label
            // 
            this._r62Label.AutoSize = true;
            this._r62Label.Location = new System.Drawing.Point(274, 79);
            this._r62Label.Name = "_r62Label";
            this._r62Label.Size = new System.Drawing.Size(19, 13);
            this._r62Label.TabIndex = 181;
            this._r62Label.Text = "62";
            // 
            // _r74Label
            // 
            this._r74Label.AutoSize = true;
            this._r74Label.Location = new System.Drawing.Point(274, 306);
            this._r74Label.Name = "_r74Label";
            this._r74Label.Size = new System.Drawing.Size(19, 13);
            this._r74Label.TabIndex = 175;
            this._r74Label.Text = "74";
            // 
            // _r67Label
            // 
            this._r67Label.AutoSize = true;
            this._r67Label.Location = new System.Drawing.Point(274, 174);
            this._r67Label.Name = "_r67Label";
            this._r67Label.Size = new System.Drawing.Size(19, 13);
            this._r67Label.TabIndex = 182;
            this._r67Label.Text = "67";
            // 
            // _r34Label
            // 
            this._r34Label.AutoSize = true;
            this._r34Label.Location = new System.Drawing.Point(130, 326);
            this._r34Label.Name = "_r34Label";
            this._r34Label.Size = new System.Drawing.Size(19, 13);
            this._r34Label.TabIndex = 200;
            this._r34Label.Text = "34";
            // 
            // _r79Label
            // 
            this._r79Label.AutoSize = true;
            this._r79Label.Location = new System.Drawing.Point(318, 79);
            this._r79Label.Name = "_r79Label";
            this._r79Label.Size = new System.Drawing.Size(19, 13);
            this._r79Label.TabIndex = 180;
            this._r79Label.Text = "79";
            // 
            // _r52Label
            // 
            this._r52Label.AutoSize = true;
            this._r52Label.Location = new System.Drawing.Point(215, 22);
            this._r52Label.Name = "_r52Label";
            this._r52Label.Size = new System.Drawing.Size(19, 13);
            this._r52Label.TabIndex = 126;
            this._r52Label.Text = "52";
            // 
            // label143
            // 
            this.label143.AutoSize = true;
            this.label143.Location = new System.Drawing.Point(88, 41);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(13, 13);
            this.label143.TabIndex = 64;
            this.label143.Text = "2";
            // 
            // label144
            // 
            this.label144.AutoSize = true;
            this.label144.Location = new System.Drawing.Point(88, 231);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(19, 13);
            this.label144.TabIndex = 63;
            this.label144.Text = "12";
            // 
            // label145
            // 
            this.label145.AutoSize = true;
            this.label145.Location = new System.Drawing.Point(88, 136);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(13, 13);
            this.label145.TabIndex = 103;
            this.label145.Text = "7";
            // 
            // label146
            // 
            this.label146.AutoSize = true;
            this.label146.Location = new System.Drawing.Point(88, 212);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(19, 13);
            this.label146.TabIndex = 57;
            this.label146.Text = "11";
            // 
            // label147
            // 
            this.label147.AutoSize = true;
            this.label147.Location = new System.Drawing.Point(88, 117);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(13, 13);
            this.label147.TabIndex = 99;
            this.label147.Text = "6";
            // 
            // label148
            // 
            this.label148.AutoSize = true;
            this.label148.Location = new System.Drawing.Point(88, 60);
            this.label148.Name = "label148";
            this.label148.Size = new System.Drawing.Size(13, 13);
            this.label148.TabIndex = 69;
            this.label148.Text = "3";
            // 
            // label149
            // 
            this.label149.AutoSize = true;
            this.label149.Location = new System.Drawing.Point(88, 79);
            this.label149.Name = "label149";
            this.label149.Size = new System.Drawing.Size(13, 13);
            this.label149.TabIndex = 76;
            this.label149.Text = "4";
            // 
            // label151
            // 
            this.label151.AutoSize = true;
            this.label151.Location = new System.Drawing.Point(88, 98);
            this.label151.Name = "label151";
            this.label151.Size = new System.Drawing.Size(13, 13);
            this.label151.TabIndex = 81;
            this.label151.Text = "5";
            // 
            // _r30Label
            // 
            this._r30Label.AutoSize = true;
            this._r30Label.Location = new System.Drawing.Point(130, 250);
            this._r30Label.Name = "_r30Label";
            this._r30Label.Size = new System.Drawing.Size(19, 13);
            this._r30Label.TabIndex = 119;
            this._r30Label.Text = "30";
            // 
            // _r29Label
            // 
            this._r29Label.AutoSize = true;
            this._r29Label.Location = new System.Drawing.Point(130, 231);
            this._r29Label.Name = "_r29Label";
            this._r29Label.Size = new System.Drawing.Size(19, 13);
            this._r29Label.TabIndex = 110;
            this._r29Label.Text = "29";
            // 
            // _r28Label
            // 
            this._r28Label.AutoSize = true;
            this._r28Label.Location = new System.Drawing.Point(130, 212);
            this._r28Label.Name = "_r28Label";
            this._r28Label.Size = new System.Drawing.Size(19, 13);
            this._r28Label.TabIndex = 104;
            this._r28Label.Text = "28";
            // 
            // _r27Label
            // 
            this._r27Label.AutoSize = true;
            this._r27Label.Location = new System.Drawing.Point(130, 193);
            this._r27Label.Name = "_r27Label";
            this._r27Label.Size = new System.Drawing.Size(19, 13);
            this._r27Label.TabIndex = 100;
            this._r27Label.Text = "27";
            // 
            // _r33Label
            // 
            this._r33Label.AutoSize = true;
            this._r33Label.Location = new System.Drawing.Point(130, 307);
            this._r33Label.Name = "_r33Label";
            this._r33Label.Size = new System.Drawing.Size(19, 13);
            this._r33Label.TabIndex = 178;
            this._r33Label.Text = "33";
            // 
            // _r26Label
            // 
            this._r26Label.AutoSize = true;
            this._r26Label.Location = new System.Drawing.Point(130, 174);
            this._r26Label.Name = "_r26Label";
            this._r26Label.Size = new System.Drawing.Size(19, 13);
            this._r26Label.TabIndex = 96;
            this._r26Label.Text = "26";
            // 
            // _r25Label
            // 
            this._r25Label.AutoSize = true;
            this._r25Label.Location = new System.Drawing.Point(130, 155);
            this._r25Label.Name = "_r25Label";
            this._r25Label.Size = new System.Drawing.Size(19, 13);
            this._r25Label.TabIndex = 92;
            this._r25Label.Text = "25";
            // 
            // _r24Label
            // 
            this._r24Label.AutoSize = true;
            this._r24Label.Location = new System.Drawing.Point(130, 136);
            this._r24Label.Name = "_r24Label";
            this._r24Label.Size = new System.Drawing.Size(19, 13);
            this._r24Label.TabIndex = 88;
            this._r24Label.Text = "24";
            // 
            // _r23Label
            // 
            this._r23Label.AutoSize = true;
            this._r23Label.Location = new System.Drawing.Point(130, 117);
            this._r23Label.Name = "_r23Label";
            this._r23Label.Size = new System.Drawing.Size(19, 13);
            this._r23Label.TabIndex = 83;
            this._r23Label.Text = "23";
            // 
            // _r31Label
            // 
            this._r31Label.AutoSize = true;
            this._r31Label.Location = new System.Drawing.Point(130, 269);
            this._r31Label.Name = "_r31Label";
            this._r31Label.Size = new System.Drawing.Size(19, 13);
            this._r31Label.TabIndex = 141;
            this._r31Label.Text = "31";
            // 
            // _r22Label
            // 
            this._r22Label.AutoSize = true;
            this._r22Label.Location = new System.Drawing.Point(130, 99);
            this._r22Label.Name = "_r22Label";
            this._r22Label.Size = new System.Drawing.Size(19, 13);
            this._r22Label.TabIndex = 77;
            this._r22Label.Text = "22";
            // 
            // _r21Label
            // 
            this._r21Label.AutoSize = true;
            this._r21Label.Location = new System.Drawing.Point(130, 79);
            this._r21Label.Name = "_r21Label";
            this._r21Label.Size = new System.Drawing.Size(19, 13);
            this._r21Label.TabIndex = 70;
            this._r21Label.Text = "21";
            // 
            // _r20Label
            // 
            this._r20Label.AutoSize = true;
            this._r20Label.Location = new System.Drawing.Point(130, 60);
            this._r20Label.Name = "_r20Label";
            this._r20Label.Size = new System.Drawing.Size(19, 13);
            this._r20Label.TabIndex = 65;
            this._r20Label.Text = "20";
            // 
            // _r19Label
            // 
            this._r19Label.AutoSize = true;
            this._r19Label.Location = new System.Drawing.Point(130, 41);
            this._r19Label.Name = "_r19Label";
            this._r19Label.Size = new System.Drawing.Size(19, 13);
            this._r19Label.TabIndex = 59;
            this._r19Label.Text = "19";
            // 
            // _r32Label
            // 
            this._r32Label.AutoSize = true;
            this._r32Label.Location = new System.Drawing.Point(130, 288);
            this._r32Label.Name = "_r32Label";
            this._r32Label.Size = new System.Drawing.Size(19, 13);
            this._r32Label.TabIndex = 165;
            this._r32Label.Text = "32";
            // 
            // _module56
            // 
            this._module56.Location = new System.Drawing.Point(196, 98);
            this._module56.Name = "_module56";
            this._module56.Size = new System.Drawing.Size(13, 13);
            this._module56.State = BEMN.Forms.LedState.Off;
            this._module56.TabIndex = 216;
            // 
            // _module57
            // 
            this._module57.Location = new System.Drawing.Point(196, 116);
            this._module57.Name = "_module57";
            this._module57.Size = new System.Drawing.Size(13, 13);
            this._module57.State = BEMN.Forms.LedState.Off;
            this._module57.TabIndex = 212;
            // 
            // _module18
            // 
            this._module18.Location = new System.Drawing.Point(111, 22);
            this._module18.Name = "_module18";
            this._module18.Size = new System.Drawing.Size(13, 13);
            this._module18.State = BEMN.Forms.LedState.Off;
            this._module18.TabIndex = 97;
            // 
            // _module10
            // 
            this._module10.Location = new System.Drawing.Point(69, 193);
            this._module10.Name = "_module10";
            this._module10.Size = new System.Drawing.Size(13, 13);
            this._module10.State = BEMN.Forms.LedState.Off;
            this._module10.TabIndex = 155;
            // 
            // _module69
            // 
            this._module69.Location = new System.Drawing.Point(255, 212);
            this._module69.Name = "_module69";
            this._module69.Size = new System.Drawing.Size(13, 13);
            this._module69.State = BEMN.Forms.LedState.Off;
            this._module69.TabIndex = 211;
            // 
            // _module58
            // 
            this._module58.Location = new System.Drawing.Point(196, 136);
            this._module58.Name = "_module58";
            this._module58.Size = new System.Drawing.Size(13, 13);
            this._module58.State = BEMN.Forms.LedState.Off;
            this._module58.TabIndex = 116;
            // 
            // _module70
            // 
            this._module70.Location = new System.Drawing.Point(255, 231);
            this._module70.Name = "_module70";
            this._module70.Size = new System.Drawing.Size(13, 13);
            this._module70.State = BEMN.Forms.LedState.Off;
            this._module70.TabIndex = 114;
            // 
            // _module80
            // 
            this._module80.Location = new System.Drawing.Point(299, 98);
            this._module80.Name = "_module80";
            this._module80.Size = new System.Drawing.Size(13, 13);
            this._module80.State = BEMN.Forms.LedState.Off;
            this._module80.TabIndex = 209;
            // 
            // _module17
            // 
            this._module17.Location = new System.Drawing.Point(69, 326);
            this._module17.Name = "_module17";
            this._module17.Size = new System.Drawing.Size(13, 13);
            this._module17.State = BEMN.Forms.LedState.Off;
            this._module17.TabIndex = 93;
            // 
            // _module51
            // 
            this._module51.Location = new System.Drawing.Point(155, 326);
            this._module51.Name = "_module51";
            this._module51.Size = new System.Drawing.Size(13, 13);
            this._module51.State = BEMN.Forms.LedState.Off;
            this._module51.TabIndex = 205;
            // 
            // _module68
            // 
            this._module68.Location = new System.Drawing.Point(255, 193);
            this._module68.Name = "_module68";
            this._module68.Size = new System.Drawing.Size(13, 13);
            this._module68.State = BEMN.Forms.LedState.Off;
            this._module68.TabIndex = 204;
            // 
            // _module55
            // 
            this._module55.Location = new System.Drawing.Point(196, 79);
            this._module55.Name = "_module55";
            this._module55.Size = new System.Drawing.Size(13, 13);
            this._module55.State = BEMN.Forms.LedState.Off;
            this._module55.TabIndex = 189;
            // 
            // _module59
            // 
            this._module59.Location = new System.Drawing.Point(255, 22);
            this._module59.Name = "_module59";
            this._module59.Size = new System.Drawing.Size(13, 13);
            this._module59.State = BEMN.Forms.LedState.Off;
            this._module59.TabIndex = 129;
            // 
            // _module9
            // 
            this._module9.Location = new System.Drawing.Point(69, 174);
            this._module9.Name = "_module9";
            this._module9.Size = new System.Drawing.Size(13, 13);
            this._module9.State = BEMN.Forms.LedState.Off;
            this._module9.TabIndex = 131;
            // 
            // _module71
            // 
            this._module71.Location = new System.Drawing.Point(255, 250);
            this._module71.Name = "_module71";
            this._module71.Size = new System.Drawing.Size(13, 13);
            this._module71.State = BEMN.Forms.LedState.Off;
            this._module71.TabIndex = 128;
            // 
            // _module75
            // 
            this._module75.Location = new System.Drawing.Point(255, 326);
            this._module75.Name = "_module75";
            this._module75.Size = new System.Drawing.Size(13, 13);
            this._module75.State = BEMN.Forms.LedState.Off;
            this._module75.TabIndex = 207;
            // 
            // _module16
            // 
            this._module16.Location = new System.Drawing.Point(69, 307);
            this._module16.Name = "_module16";
            this._module16.Size = new System.Drawing.Size(13, 13);
            this._module16.State = BEMN.Forms.LedState.Off;
            this._module16.TabIndex = 89;
            // 
            // _module64
            // 
            this._module64.Location = new System.Drawing.Point(255, 117);
            this._module64.Name = "_module64";
            this._module64.Size = new System.Drawing.Size(13, 13);
            this._module64.State = BEMN.Forms.LedState.Off;
            this._module64.TabIndex = 133;
            // 
            // _module76
            // 
            this._module76.Location = new System.Drawing.Point(299, 22);
            this._module76.Name = "_module76";
            this._module76.Size = new System.Drawing.Size(13, 13);
            this._module76.State = BEMN.Forms.LedState.Off;
            this._module76.TabIndex = 132;
            // 
            // _module63
            // 
            this._module63.Location = new System.Drawing.Point(255, 98);
            this._module63.Name = "_module63";
            this._module63.Size = new System.Drawing.Size(13, 13);
            this._module63.State = BEMN.Forms.LedState.Off;
            this._module63.TabIndex = 206;
            // 
            // _module15
            // 
            this._module15.Location = new System.Drawing.Point(69, 288);
            this._module15.Name = "_module15";
            this._module15.Size = new System.Drawing.Size(13, 13);
            this._module15.State = BEMN.Forms.LedState.Off;
            this._module15.TabIndex = 85;
            // 
            // _module5
            // 
            this._module5.Location = new System.Drawing.Point(69, 98);
            this._module5.Name = "_module5";
            this._module5.Size = new System.Drawing.Size(13, 13);
            this._module5.State = BEMN.Forms.LedState.Off;
            this._module5.TabIndex = 84;
            // 
            // _module54
            // 
            this._module54.Location = new System.Drawing.Point(196, 60);
            this._module54.Name = "_module54";
            this._module54.Size = new System.Drawing.Size(13, 13);
            this._module54.State = BEMN.Forms.LedState.Off;
            this._module54.TabIndex = 174;
            // 
            // _module34
            // 
            this._module34.Location = new System.Drawing.Point(111, 326);
            this._module34.Name = "_module34";
            this._module34.Size = new System.Drawing.Size(13, 13);
            this._module34.State = BEMN.Forms.LedState.Off;
            this._module34.TabIndex = 208;
            // 
            // _module60
            // 
            this._module60.Location = new System.Drawing.Point(255, 41);
            this._module60.Name = "_module60";
            this._module60.Size = new System.Drawing.Size(13, 13);
            this._module60.State = BEMN.Forms.LedState.Off;
            this._module60.TabIndex = 153;
            // 
            // _module72
            // 
            this._module72.Location = new System.Drawing.Point(255, 269);
            this._module72.Name = "_module72";
            this._module72.Size = new System.Drawing.Size(13, 13);
            this._module72.State = BEMN.Forms.LedState.Off;
            this._module72.TabIndex = 152;
            // 
            // _module65
            // 
            this._module65.Location = new System.Drawing.Point(255, 136);
            this._module65.Name = "_module65";
            this._module65.Size = new System.Drawing.Size(13, 13);
            this._module65.State = BEMN.Forms.LedState.Off;
            this._module65.TabIndex = 148;
            // 
            // _module8
            // 
            this._module8.Location = new System.Drawing.Point(69, 155);
            this._module8.Name = "_module8";
            this._module8.Size = new System.Drawing.Size(13, 13);
            this._module8.State = BEMN.Forms.LedState.Off;
            this._module8.TabIndex = 115;
            // 
            // _module77
            // 
            this._module77.Location = new System.Drawing.Point(299, 41);
            this._module77.Name = "_module77";
            this._module77.Size = new System.Drawing.Size(13, 13);
            this._module77.State = BEMN.Forms.LedState.Off;
            this._module77.TabIndex = 154;
            // 
            // _module14
            // 
            this._module14.Location = new System.Drawing.Point(69, 269);
            this._module14.Name = "_module14";
            this._module14.Size = new System.Drawing.Size(13, 13);
            this._module14.State = BEMN.Forms.LedState.Off;
            this._module14.TabIndex = 79;
            // 
            // _module53
            // 
            this._module53.Location = new System.Drawing.Point(196, 41);
            this._module53.Name = "_module53";
            this._module53.Size = new System.Drawing.Size(13, 13);
            this._module53.State = BEMN.Forms.LedState.Off;
            this._module53.TabIndex = 151;
            // 
            // _module79
            // 
            this._module79.Location = new System.Drawing.Point(299, 79);
            this._module79.Name = "_module79";
            this._module79.Size = new System.Drawing.Size(13, 13);
            this._module79.State = BEMN.Forms.LedState.Off;
            this._module79.TabIndex = 186;
            // 
            // _module61
            // 
            this._module61.Location = new System.Drawing.Point(255, 60);
            this._module61.Name = "_module61";
            this._module61.Size = new System.Drawing.Size(13, 13);
            this._module61.State = BEMN.Forms.LedState.Off;
            this._module61.TabIndex = 172;
            // 
            // _module73
            // 
            this._module73.Location = new System.Drawing.Point(255, 288);
            this._module73.Name = "_module73";
            this._module73.Size = new System.Drawing.Size(13, 13);
            this._module73.State = BEMN.Forms.LedState.Off;
            this._module73.TabIndex = 166;
            // 
            // _module67
            // 
            this._module67.Location = new System.Drawing.Point(255, 174);
            this._module67.Name = "_module67";
            this._module67.Size = new System.Drawing.Size(13, 13);
            this._module67.State = BEMN.Forms.LedState.Off;
            this._module67.TabIndex = 191;
            // 
            // _module66
            // 
            this._module66.Location = new System.Drawing.Point(255, 155);
            this._module66.Name = "_module66";
            this._module66.Size = new System.Drawing.Size(13, 13);
            this._module66.State = BEMN.Forms.LedState.Off;
            this._module66.TabIndex = 173;
            // 
            // _module13
            // 
            this._module13.Location = new System.Drawing.Point(69, 250);
            this._module13.Name = "_module13";
            this._module13.Size = new System.Drawing.Size(13, 13);
            this._module13.State = BEMN.Forms.LedState.Off;
            this._module13.TabIndex = 73;
            // 
            // _module78
            // 
            this._module78.Location = new System.Drawing.Point(299, 60);
            this._module78.Name = "_module78";
            this._module78.Size = new System.Drawing.Size(13, 13);
            this._module78.State = BEMN.Forms.LedState.Off;
            this._module78.TabIndex = 171;
            // 
            // _module1
            // 
            this._module1.Location = new System.Drawing.Point(69, 22);
            this._module1.Name = "_module1";
            this._module1.Size = new System.Drawing.Size(13, 13);
            this._module1.State = BEMN.Forms.LedState.Off;
            this._module1.TabIndex = 61;
            // 
            // _module74
            // 
            this._module74.Location = new System.Drawing.Point(255, 306);
            this._module74.Name = "_module74";
            this._module74.Size = new System.Drawing.Size(13, 13);
            this._module74.State = BEMN.Forms.LedState.Off;
            this._module74.TabIndex = 192;
            // 
            // _module62
            // 
            this._module62.Location = new System.Drawing.Point(255, 79);
            this._module62.Name = "_module62";
            this._module62.Size = new System.Drawing.Size(13, 13);
            this._module62.State = BEMN.Forms.LedState.Off;
            this._module62.TabIndex = 185;
            // 
            // _module52
            // 
            this._module52.Location = new System.Drawing.Point(196, 22);
            this._module52.Name = "_module52";
            this._module52.Size = new System.Drawing.Size(13, 13);
            this._module52.State = BEMN.Forms.LedState.Off;
            this._module52.TabIndex = 127;
            // 
            // _module7
            // 
            this._module7.Location = new System.Drawing.Point(69, 136);
            this._module7.Name = "_module7";
            this._module7.Size = new System.Drawing.Size(13, 13);
            this._module7.State = BEMN.Forms.LedState.Off;
            this._module7.TabIndex = 106;
            // 
            // _module12
            // 
            this._module12.Location = new System.Drawing.Point(69, 231);
            this._module12.Name = "_module12";
            this._module12.Size = new System.Drawing.Size(13, 13);
            this._module12.State = BEMN.Forms.LedState.Off;
            this._module12.TabIndex = 66;
            // 
            // _module11
            // 
            this._module11.Location = new System.Drawing.Point(69, 212);
            this._module11.Name = "_module11";
            this._module11.Size = new System.Drawing.Size(13, 13);
            this._module11.State = BEMN.Forms.LedState.Off;
            this._module11.TabIndex = 60;
            // 
            // _module2
            // 
            this._module2.Location = new System.Drawing.Point(69, 41);
            this._module2.Name = "_module2";
            this._module2.Size = new System.Drawing.Size(13, 13);
            this._module2.State = BEMN.Forms.LedState.Off;
            this._module2.TabIndex = 68;
            // 
            // _module6
            // 
            this._module6.Location = new System.Drawing.Point(69, 117);
            this._module6.Name = "_module6";
            this._module6.Size = new System.Drawing.Size(13, 13);
            this._module6.State = BEMN.Forms.LedState.Off;
            this._module6.TabIndex = 102;
            // 
            // _module3
            // 
            this._module3.Location = new System.Drawing.Point(69, 60);
            this._module3.Name = "_module3";
            this._module3.Size = new System.Drawing.Size(13, 13);
            this._module3.State = BEMN.Forms.LedState.Off;
            this._module3.TabIndex = 72;
            // 
            // _module33
            // 
            this._module33.Location = new System.Drawing.Point(111, 307);
            this._module33.Name = "_module33";
            this._module33.Size = new System.Drawing.Size(13, 13);
            this._module33.State = BEMN.Forms.LedState.Off;
            this._module33.TabIndex = 190;
            // 
            // _module4
            // 
            this._module4.Location = new System.Drawing.Point(69, 79);
            this._module4.Name = "_module4";
            this._module4.Size = new System.Drawing.Size(13, 13);
            this._module4.State = BEMN.Forms.LedState.Off;
            this._module4.TabIndex = 80;
            // 
            // _module19
            // 
            this._module19.Location = new System.Drawing.Point(111, 41);
            this._module19.Name = "_module19";
            this._module19.Size = new System.Drawing.Size(13, 13);
            this._module19.State = BEMN.Forms.LedState.Off;
            this._module19.TabIndex = 62;
            // 
            // _module29
            // 
            this._module29.Location = new System.Drawing.Point(111, 231);
            this._module29.Name = "_module29";
            this._module29.Size = new System.Drawing.Size(13, 13);
            this._module29.State = BEMN.Forms.LedState.Off;
            this._module29.TabIndex = 113;
            // 
            // _module28
            // 
            this._module28.Location = new System.Drawing.Point(111, 212);
            this._module28.Name = "_module28";
            this._module28.Size = new System.Drawing.Size(13, 13);
            this._module28.State = BEMN.Forms.LedState.Off;
            this._module28.TabIndex = 105;
            // 
            // _module27
            // 
            this._module27.Location = new System.Drawing.Point(111, 193);
            this._module27.Name = "_module27";
            this._module27.Size = new System.Drawing.Size(13, 13);
            this._module27.State = BEMN.Forms.LedState.Off;
            this._module27.TabIndex = 101;
            // 
            // _module30
            // 
            this._module30.Location = new System.Drawing.Point(111, 250);
            this._module30.Name = "_module30";
            this._module30.Size = new System.Drawing.Size(13, 13);
            this._module30.State = BEMN.Forms.LedState.Off;
            this._module30.TabIndex = 135;
            // 
            // _module26
            // 
            this._module26.Location = new System.Drawing.Point(111, 174);
            this._module26.Name = "_module26";
            this._module26.Size = new System.Drawing.Size(13, 13);
            this._module26.State = BEMN.Forms.LedState.Off;
            this._module26.TabIndex = 98;
            // 
            // _module25
            // 
            this._module25.Location = new System.Drawing.Point(111, 155);
            this._module25.Name = "_module25";
            this._module25.Size = new System.Drawing.Size(13, 13);
            this._module25.State = BEMN.Forms.LedState.Off;
            this._module25.TabIndex = 94;
            // 
            // _module24
            // 
            this._module24.Location = new System.Drawing.Point(111, 136);
            this._module24.Name = "_module24";
            this._module24.Size = new System.Drawing.Size(13, 13);
            this._module24.State = BEMN.Forms.LedState.Off;
            this._module24.TabIndex = 90;
            // 
            // _module23
            // 
            this._module23.Location = new System.Drawing.Point(111, 117);
            this._module23.Name = "_module23";
            this._module23.Size = new System.Drawing.Size(13, 13);
            this._module23.State = BEMN.Forms.LedState.Off;
            this._module23.TabIndex = 86;
            // 
            // _module22
            // 
            this._module22.Location = new System.Drawing.Point(111, 98);
            this._module22.Name = "_module22";
            this._module22.Size = new System.Drawing.Size(13, 13);
            this._module22.State = BEMN.Forms.LedState.Off;
            this._module22.TabIndex = 78;
            // 
            // _module21
            // 
            this._module21.Location = new System.Drawing.Point(111, 79);
            this._module21.Name = "_module21";
            this._module21.Size = new System.Drawing.Size(13, 13);
            this._module21.State = BEMN.Forms.LedState.Off;
            this._module21.TabIndex = 74;
            // 
            // _module20
            // 
            this._module20.Location = new System.Drawing.Point(111, 60);
            this._module20.Name = "_module20";
            this._module20.Size = new System.Drawing.Size(13, 13);
            this._module20.State = BEMN.Forms.LedState.Off;
            this._module20.TabIndex = 67;
            // 
            // _module32
            // 
            this._module32.Location = new System.Drawing.Point(111, 288);
            this._module32.Name = "_module32";
            this._module32.Size = new System.Drawing.Size(13, 13);
            this._module32.State = BEMN.Forms.LedState.Off;
            this._module32.TabIndex = 170;
            // 
            // _module31
            // 
            this._module31.Location = new System.Drawing.Point(111, 269);
            this._module31.Name = "_module31";
            this._module31.Size = new System.Drawing.Size(13, 13);
            this._module31.State = BEMN.Forms.LedState.Off;
            this._module31.TabIndex = 156;
            // 
            // _virtualReleGB
            // 
            this._virtualReleGB.Location = new System.Drawing.Point(248, 3);
            this._virtualReleGB.Name = "_virtualReleGB";
            this._virtualReleGB.Size = new System.Drawing.Size(115, 355);
            this._virtualReleGB.TabIndex = 218;
            this._virtualReleGB.TabStop = false;
            this._virtualReleGB.Text = "Виртуальные реле";
            // 
            // _releGB
            // 
            this._releGB.Location = new System.Drawing.Point(62, 3);
            this._releGB.Name = "_releGB";
            this._releGB.Size = new System.Drawing.Size(180, 355);
            this._releGB.TabIndex = 217;
            this._releGB.TabStop = false;
            this._releGB.Text = "Реле";
            // 
            // _discretsGB
            // 
            this._discretsGB.Controls.Add(this._d112);
            this._discretsGB.Controls.Add(this._d112Label);
            this._discretsGB.Controls.Add(this._d111);
            this._discretsGB.Controls.Add(this._d111Label);
            this._discretsGB.Controls.Add(this._d110);
            this._discretsGB.Controls.Add(this._d110Label);
            this._discretsGB.Controls.Add(this._d109);
            this._discretsGB.Controls.Add(this._d109Label);
            this._discretsGB.Controls.Add(this._d108);
            this._discretsGB.Controls.Add(this._d108Label);
            this._discretsGB.Controls.Add(this._d107);
            this._discretsGB.Controls.Add(this._d107Label);
            this._discretsGB.Controls.Add(this._d106);
            this._discretsGB.Controls.Add(this._d106Label);
            this._discretsGB.Controls.Add(this._d105);
            this._discretsGB.Controls.Add(this._d105Label);
            this._discretsGB.Controls.Add(this._d104);
            this._discretsGB.Controls.Add(this._d104Label);
            this._discretsGB.Controls.Add(this._d103);
            this._discretsGB.Controls.Add(this._d103Label);
            this._discretsGB.Controls.Add(this._d102);
            this._discretsGB.Controls.Add(this._d102Label);
            this._discretsGB.Controls.Add(this._d101);
            this._discretsGB.Controls.Add(this._d101Label);
            this._discretsGB.Controls.Add(this._d100);
            this._discretsGB.Controls.Add(this._d100Label);
            this._discretsGB.Controls.Add(this._d99);
            this._discretsGB.Controls.Add(this._d99Label);
            this._discretsGB.Controls.Add(this._d98);
            this._discretsGB.Controls.Add(this._d98Label);
            this._discretsGB.Controls.Add(this._d97);
            this._discretsGB.Controls.Add(this._d97Label);
            this._discretsGB.Controls.Add(this._d96);
            this._discretsGB.Controls.Add(this._d96Label);
            this._discretsGB.Controls.Add(this._d95);
            this._discretsGB.Controls.Add(this._d95Label);
            this._discretsGB.Controls.Add(this._d94);
            this._discretsGB.Controls.Add(this._d94Label);
            this._discretsGB.Controls.Add(this._d93);
            this._discretsGB.Controls.Add(this._d93Label);
            this._discretsGB.Controls.Add(this._d92);
            this._discretsGB.Controls.Add(this._d92Label);
            this._discretsGB.Controls.Add(this._d91);
            this._discretsGB.Controls.Add(this._d91Label);
            this._discretsGB.Controls.Add(this._d90);
            this._discretsGB.Controls.Add(this._d90Label);
            this._discretsGB.Controls.Add(this._d89);
            this._discretsGB.Controls.Add(this._d89Label);
            this._discretsGB.Controls.Add(this._d88);
            this._discretsGB.Controls.Add(this._d88Label);
            this._discretsGB.Controls.Add(this._d87);
            this._discretsGB.Controls.Add(this._d87Label);
            this._discretsGB.Controls.Add(this._d86);
            this._discretsGB.Controls.Add(this._d86Label);
            this._discretsGB.Controls.Add(this._d85);
            this._discretsGB.Controls.Add(this._d85Label);
            this._discretsGB.Controls.Add(this._d84);
            this._discretsGB.Controls.Add(this._d84Label);
            this._discretsGB.Controls.Add(this._d83);
            this._discretsGB.Controls.Add(this._d83Label);
            this._discretsGB.Controls.Add(this._d82);
            this._discretsGB.Controls.Add(this._d82Label);
            this._discretsGB.Controls.Add(this._d81);
            this._discretsGB.Controls.Add(this._d81Label);
            this._discretsGB.Controls.Add(this._d80);
            this._discretsGB.Controls.Add(this._d80Label);
            this._discretsGB.Controls.Add(this._d79);
            this._discretsGB.Controls.Add(this._d79Label);
            this._discretsGB.Controls.Add(this._d78);
            this._discretsGB.Controls.Add(this._d78Label);
            this._discretsGB.Controls.Add(this._d77);
            this._discretsGB.Controls.Add(this._d77Label);
            this._discretsGB.Controls.Add(this._d76);
            this._discretsGB.Controls.Add(this._d76Label);
            this._discretsGB.Controls.Add(this._d75);
            this._discretsGB.Controls.Add(this._d75Label);
            this._discretsGB.Controls.Add(this._d74);
            this._discretsGB.Controls.Add(this._d74Label);
            this._discretsGB.Controls.Add(this._d73);
            this._discretsGB.Controls.Add(this._d73Label);
            this._discretsGB.Controls.Add(this._k2);
            this._discretsGB.Controls.Add(this._k2Label);
            this._discretsGB.Controls.Add(this._k1);
            this._discretsGB.Controls.Add(this._k1Label);
            this._discretsGB.Controls.Add(this._d72);
            this._discretsGB.Controls.Add(this._d72Label);
            this._discretsGB.Controls.Add(this._d71);
            this._discretsGB.Controls.Add(this._d71Label);
            this._discretsGB.Controls.Add(this._d70);
            this._discretsGB.Controls.Add(this._d70Label);
            this._discretsGB.Controls.Add(this._d69);
            this._discretsGB.Controls.Add(this._d69Label);
            this._discretsGB.Controls.Add(this._d68);
            this._discretsGB.Controls.Add(this._d68Label);
            this._discretsGB.Controls.Add(this._d67);
            this._discretsGB.Controls.Add(this._d67Label);
            this._discretsGB.Controls.Add(this._d66);
            this._discretsGB.Controls.Add(this._d66Label);
            this._discretsGB.Controls.Add(this._d65);
            this._discretsGB.Controls.Add(this._d65Label);
            this._discretsGB.Controls.Add(this._d64);
            this._discretsGB.Controls.Add(this._d64Label);
            this._discretsGB.Controls.Add(this._d63);
            this._discretsGB.Controls.Add(this._d63Label);
            this._discretsGB.Controls.Add(this._d62);
            this._discretsGB.Controls.Add(this._d62Label);
            this._discretsGB.Controls.Add(this._d61);
            this._discretsGB.Controls.Add(this._d61Label);
            this._discretsGB.Controls.Add(this._d60);
            this._discretsGB.Controls.Add(this._d60Label);
            this._discretsGB.Controls.Add(this._d59);
            this._discretsGB.Controls.Add(this._d59Label);
            this._discretsGB.Controls.Add(this._d58);
            this._discretsGB.Controls.Add(this._d58Label);
            this._discretsGB.Controls.Add(this._d57);
            this._discretsGB.Controls.Add(this._d57Label);
            this._discretsGB.Controls.Add(this._d56);
            this._discretsGB.Controls.Add(this._d56Label);
            this._discretsGB.Controls.Add(this._d55);
            this._discretsGB.Controls.Add(this._d55Label);
            this._discretsGB.Controls.Add(this._d54);
            this._discretsGB.Controls.Add(this._d54Label);
            this._discretsGB.Controls.Add(this._d53);
            this._discretsGB.Controls.Add(this._d53Label);
            this._discretsGB.Controls.Add(this._d52);
            this._discretsGB.Controls.Add(this._d52Label);
            this._discretsGB.Controls.Add(this._d51);
            this._discretsGB.Controls.Add(this._d51Label);
            this._discretsGB.Controls.Add(this._d50);
            this._discretsGB.Controls.Add(this._d50Label);
            this._discretsGB.Controls.Add(this._d49);
            this._discretsGB.Controls.Add(this._d49Label);
            this._discretsGB.Controls.Add(this._d48);
            this._discretsGB.Controls.Add(this._d48Label);
            this._discretsGB.Controls.Add(this._d47);
            this._discretsGB.Controls.Add(this._d47Label);
            this._discretsGB.Controls.Add(this._d46);
            this._discretsGB.Controls.Add(this._d46Label);
            this._discretsGB.Controls.Add(this._d45);
            this._discretsGB.Controls.Add(this._d45Label);
            this._discretsGB.Controls.Add(this._d44);
            this._discretsGB.Controls.Add(this._d44Label);
            this._discretsGB.Controls.Add(this._d43);
            this._discretsGB.Controls.Add(this._d43Label);
            this._discretsGB.Controls.Add(this._d42);
            this._discretsGB.Controls.Add(this._d42Label);
            this._discretsGB.Controls.Add(this._d41);
            this._discretsGB.Controls.Add(this._d41Label);
            this._discretsGB.Controls.Add(this._d40);
            this._discretsGB.Controls.Add(this._d24);
            this._discretsGB.Controls.Add(this._d40Label);
            this._discretsGB.Controls.Add(this._d8);
            this._discretsGB.Controls.Add(this._d39);
            this._discretsGB.Controls.Add(this._d24Label);
            this._discretsGB.Controls.Add(this._d39Label);
            this._discretsGB.Controls.Add(this._d38);
            this._discretsGB.Controls.Add(this._d23);
            this._discretsGB.Controls.Add(this._d38Label);
            this._discretsGB.Controls.Add(this._d8Label);
            this._discretsGB.Controls.Add(this._d37);
            this._discretsGB.Controls.Add(this._d23Label);
            this._discretsGB.Controls.Add(this._d37Label);
            this._discretsGB.Controls.Add(this._d22);
            this._discretsGB.Controls.Add(this._d36);
            this._discretsGB.Controls.Add(this._d7);
            this._discretsGB.Controls.Add(this._d36Label);
            this._discretsGB.Controls.Add(this._d22Label);
            this._discretsGB.Controls.Add(this._d35);
            this._discretsGB.Controls.Add(this._d7Label);
            this._discretsGB.Controls.Add(this._d35Label);
            this._discretsGB.Controls.Add(this._d21);
            this._discretsGB.Controls.Add(this._d34);
            this._discretsGB.Controls.Add(this._d1);
            this._discretsGB.Controls.Add(this._d34Label);
            this._discretsGB.Controls.Add(this._d21Label);
            this._discretsGB.Controls.Add(this._d33);
            this._discretsGB.Controls.Add(this._d33Label);
            this._discretsGB.Controls.Add(this._d6);
            this._discretsGB.Controls.Add(this._d20);
            this._discretsGB.Controls.Add(this._d32);
            this._discretsGB.Controls.Add(this._d1Label);
            this._discretsGB.Controls.Add(this._d32Label);
            this._discretsGB.Controls.Add(this._d20Label);
            this._discretsGB.Controls.Add(this._d31);
            this._discretsGB.Controls.Add(this._d6Label);
            this._discretsGB.Controls.Add(this._d31Label);
            this._discretsGB.Controls.Add(this._d19);
            this._discretsGB.Controls.Add(this._d30);
            this._discretsGB.Controls.Add(this._d2Label);
            this._discretsGB.Controls.Add(this._d30Label);
            this._discretsGB.Controls.Add(this._d19Label);
            this._discretsGB.Controls.Add(this._d29);
            this._discretsGB.Controls.Add(this._d5);
            this._discretsGB.Controls.Add(this._d29Label);
            this._discretsGB.Controls.Add(this._d18);
            this._discretsGB.Controls.Add(this._d28);
            this._discretsGB.Controls.Add(this._d2);
            this._discretsGB.Controls.Add(this._d28Label);
            this._discretsGB.Controls.Add(this._d18Label);
            this._discretsGB.Controls.Add(this._d27);
            this._discretsGB.Controls.Add(this._d5Label);
            this._discretsGB.Controls.Add(this._d27Label);
            this._discretsGB.Controls.Add(this._d17);
            this._discretsGB.Controls.Add(this._d26);
            this._discretsGB.Controls.Add(this._d17Label);
            this._discretsGB.Controls.Add(this._d26Label);
            this._discretsGB.Controls.Add(this._d3Label);
            this._discretsGB.Controls.Add(this._d25);
            this._discretsGB.Controls.Add(this._d25Label);
            this._discretsGB.Controls.Add(this._d4);
            this._discretsGB.Controls.Add(this._d16);
            this._discretsGB.Controls.Add(this._d3);
            this._discretsGB.Controls.Add(this._d16Label);
            this._discretsGB.Controls.Add(this._d4Label);
            this._discretsGB.Controls.Add(this._d15);
            this._discretsGB.Controls.Add(this._d9);
            this._discretsGB.Controls.Add(this._d15Label);
            this._discretsGB.Controls.Add(this._d9Label);
            this._discretsGB.Controls.Add(this._d14);
            this._discretsGB.Controls.Add(this._d10Label);
            this._discretsGB.Controls.Add(this._d14Label);
            this._discretsGB.Controls.Add(this._d10);
            this._discretsGB.Controls.Add(this._d13);
            this._discretsGB.Controls.Add(this._d11Label);
            this._discretsGB.Controls.Add(this._d13Label);
            this._discretsGB.Controls.Add(this._d11);
            this._discretsGB.Controls.Add(this._d12);
            this._discretsGB.Controls.Add(this._d12Label);
            this._discretsGB.Location = new System.Drawing.Point(369, 3);
            this._discretsGB.Name = "_discretsGB";
            this._discretsGB.Size = new System.Drawing.Size(414, 355);
            this._discretsGB.TabIndex = 11;
            this._discretsGB.TabStop = false;
            this._discretsGB.Text = "Дискретные входы";
            // 
            // _d112
            // 
            this._d112.Location = new System.Drawing.Point(316, 304);
            this._d112.Name = "_d112";
            this._d112.Size = new System.Drawing.Size(13, 13);
            this._d112.State = BEMN.Forms.LedState.Off;
            this._d112.TabIndex = 179;
            // 
            // _d112Label
            // 
            this._d112Label.AutoSize = true;
            this._d112Label.Location = new System.Drawing.Point(335, 304);
            this._d112Label.Name = "_d112Label";
            this._d112Label.Size = new System.Drawing.Size(34, 13);
            this._d112Label.TabIndex = 178;
            this._d112Label.Text = "Д112";
            // 
            // _d111
            // 
            this._d111.Location = new System.Drawing.Point(316, 285);
            this._d111.Name = "_d111";
            this._d111.Size = new System.Drawing.Size(13, 13);
            this._d111.State = BEMN.Forms.LedState.Off;
            this._d111.TabIndex = 177;
            // 
            // _d111Label
            // 
            this._d111Label.AutoSize = true;
            this._d111Label.Location = new System.Drawing.Point(335, 285);
            this._d111Label.Name = "_d111Label";
            this._d111Label.Size = new System.Drawing.Size(34, 13);
            this._d111Label.TabIndex = 176;
            this._d111Label.Text = "Д111";
            // 
            // _d110
            // 
            this._d110.Location = new System.Drawing.Point(316, 266);
            this._d110.Name = "_d110";
            this._d110.Size = new System.Drawing.Size(13, 13);
            this._d110.State = BEMN.Forms.LedState.Off;
            this._d110.TabIndex = 175;
            // 
            // _d110Label
            // 
            this._d110Label.AutoSize = true;
            this._d110Label.Location = new System.Drawing.Point(335, 266);
            this._d110Label.Name = "_d110Label";
            this._d110Label.Size = new System.Drawing.Size(34, 13);
            this._d110Label.TabIndex = 174;
            this._d110Label.Text = "Д110";
            // 
            // _d109
            // 
            this._d109.Location = new System.Drawing.Point(316, 247);
            this._d109.Name = "_d109";
            this._d109.Size = new System.Drawing.Size(13, 13);
            this._d109.State = BEMN.Forms.LedState.Off;
            this._d109.TabIndex = 173;
            // 
            // _d109Label
            // 
            this._d109Label.AutoSize = true;
            this._d109Label.Location = new System.Drawing.Point(335, 247);
            this._d109Label.Name = "_d109Label";
            this._d109Label.Size = new System.Drawing.Size(34, 13);
            this._d109Label.TabIndex = 172;
            this._d109Label.Text = "Д109";
            // 
            // _d108
            // 
            this._d108.Location = new System.Drawing.Point(316, 228);
            this._d108.Name = "_d108";
            this._d108.Size = new System.Drawing.Size(13, 13);
            this._d108.State = BEMN.Forms.LedState.Off;
            this._d108.TabIndex = 171;
            // 
            // _d108Label
            // 
            this._d108Label.AutoSize = true;
            this._d108Label.Location = new System.Drawing.Point(335, 228);
            this._d108Label.Name = "_d108Label";
            this._d108Label.Size = new System.Drawing.Size(34, 13);
            this._d108Label.TabIndex = 170;
            this._d108Label.Text = "Д108";
            // 
            // _d107
            // 
            this._d107.Location = new System.Drawing.Point(316, 209);
            this._d107.Name = "_d107";
            this._d107.Size = new System.Drawing.Size(13, 13);
            this._d107.State = BEMN.Forms.LedState.Off;
            this._d107.TabIndex = 169;
            // 
            // _d107Label
            // 
            this._d107Label.AutoSize = true;
            this._d107Label.Location = new System.Drawing.Point(335, 209);
            this._d107Label.Name = "_d107Label";
            this._d107Label.Size = new System.Drawing.Size(34, 13);
            this._d107Label.TabIndex = 168;
            this._d107Label.Text = "Д107";
            // 
            // _d106
            // 
            this._d106.Location = new System.Drawing.Point(316, 190);
            this._d106.Name = "_d106";
            this._d106.Size = new System.Drawing.Size(13, 13);
            this._d106.State = BEMN.Forms.LedState.Off;
            this._d106.TabIndex = 167;
            // 
            // _d106Label
            // 
            this._d106Label.AutoSize = true;
            this._d106Label.Location = new System.Drawing.Point(335, 190);
            this._d106Label.Name = "_d106Label";
            this._d106Label.Size = new System.Drawing.Size(34, 13);
            this._d106Label.TabIndex = 166;
            this._d106Label.Text = "Д106";
            // 
            // _d105
            // 
            this._d105.Location = new System.Drawing.Point(316, 171);
            this._d105.Name = "_d105";
            this._d105.Size = new System.Drawing.Size(13, 13);
            this._d105.State = BEMN.Forms.LedState.Off;
            this._d105.TabIndex = 165;
            // 
            // _d105Label
            // 
            this._d105Label.AutoSize = true;
            this._d105Label.Location = new System.Drawing.Point(335, 171);
            this._d105Label.Name = "_d105Label";
            this._d105Label.Size = new System.Drawing.Size(34, 13);
            this._d105Label.TabIndex = 164;
            this._d105Label.Text = "Д105";
            // 
            // _d104
            // 
            this._d104.Location = new System.Drawing.Point(316, 152);
            this._d104.Name = "_d104";
            this._d104.Size = new System.Drawing.Size(13, 13);
            this._d104.State = BEMN.Forms.LedState.Off;
            this._d104.TabIndex = 163;
            // 
            // _d104Label
            // 
            this._d104Label.AutoSize = true;
            this._d104Label.Location = new System.Drawing.Point(335, 151);
            this._d104Label.Name = "_d104Label";
            this._d104Label.Size = new System.Drawing.Size(34, 13);
            this._d104Label.TabIndex = 162;
            this._d104Label.Text = "Д104";
            // 
            // _d103
            // 
            this._d103.Location = new System.Drawing.Point(316, 133);
            this._d103.Name = "_d103";
            this._d103.Size = new System.Drawing.Size(13, 13);
            this._d103.State = BEMN.Forms.LedState.Off;
            this._d103.TabIndex = 161;
            // 
            // _d103Label
            // 
            this._d103Label.AutoSize = true;
            this._d103Label.Location = new System.Drawing.Point(335, 132);
            this._d103Label.Name = "_d103Label";
            this._d103Label.Size = new System.Drawing.Size(34, 13);
            this._d103Label.TabIndex = 160;
            this._d103Label.Text = "Д103";
            // 
            // _d102
            // 
            this._d102.Location = new System.Drawing.Point(316, 114);
            this._d102.Name = "_d102";
            this._d102.Size = new System.Drawing.Size(13, 13);
            this._d102.State = BEMN.Forms.LedState.Off;
            this._d102.TabIndex = 159;
            // 
            // _d102Label
            // 
            this._d102Label.AutoSize = true;
            this._d102Label.Location = new System.Drawing.Point(335, 113);
            this._d102Label.Name = "_d102Label";
            this._d102Label.Size = new System.Drawing.Size(34, 13);
            this._d102Label.TabIndex = 158;
            this._d102Label.Text = "Д102";
            // 
            // _d101
            // 
            this._d101.Location = new System.Drawing.Point(316, 95);
            this._d101.Name = "_d101";
            this._d101.Size = new System.Drawing.Size(13, 13);
            this._d101.State = BEMN.Forms.LedState.Off;
            this._d101.TabIndex = 157;
            // 
            // _d101Label
            // 
            this._d101Label.AutoSize = true;
            this._d101Label.Location = new System.Drawing.Point(335, 94);
            this._d101Label.Name = "_d101Label";
            this._d101Label.Size = new System.Drawing.Size(34, 13);
            this._d101Label.TabIndex = 156;
            this._d101Label.Text = "Д101";
            // 
            // _d100
            // 
            this._d100.Location = new System.Drawing.Point(316, 76);
            this._d100.Name = "_d100";
            this._d100.Size = new System.Drawing.Size(13, 13);
            this._d100.State = BEMN.Forms.LedState.Off;
            this._d100.TabIndex = 155;
            // 
            // _d100Label
            // 
            this._d100Label.AutoSize = true;
            this._d100Label.Location = new System.Drawing.Point(335, 75);
            this._d100Label.Name = "_d100Label";
            this._d100Label.Size = new System.Drawing.Size(34, 13);
            this._d100Label.TabIndex = 154;
            this._d100Label.Text = "Д100";
            // 
            // _d99
            // 
            this._d99.Location = new System.Drawing.Point(316, 57);
            this._d99.Name = "_d99";
            this._d99.Size = new System.Drawing.Size(13, 13);
            this._d99.State = BEMN.Forms.LedState.Off;
            this._d99.TabIndex = 153;
            // 
            // _d99Label
            // 
            this._d99Label.AutoSize = true;
            this._d99Label.Location = new System.Drawing.Point(335, 56);
            this._d99Label.Name = "_d99Label";
            this._d99Label.Size = new System.Drawing.Size(28, 13);
            this._d99Label.TabIndex = 152;
            this._d99Label.Text = "Д99";
            // 
            // _d98
            // 
            this._d98.Location = new System.Drawing.Point(316, 38);
            this._d98.Name = "_d98";
            this._d98.Size = new System.Drawing.Size(13, 13);
            this._d98.State = BEMN.Forms.LedState.Off;
            this._d98.TabIndex = 151;
            // 
            // _d98Label
            // 
            this._d98Label.AutoSize = true;
            this._d98Label.Location = new System.Drawing.Point(335, 37);
            this._d98Label.Name = "_d98Label";
            this._d98Label.Size = new System.Drawing.Size(28, 13);
            this._d98Label.TabIndex = 150;
            this._d98Label.Text = "Д98";
            // 
            // _d97
            // 
            this._d97.Location = new System.Drawing.Point(316, 19);
            this._d97.Name = "_d97";
            this._d97.Size = new System.Drawing.Size(13, 13);
            this._d97.State = BEMN.Forms.LedState.Off;
            this._d97.TabIndex = 149;
            // 
            // _d97Label
            // 
            this._d97Label.AutoSize = true;
            this._d97Label.Location = new System.Drawing.Point(335, 18);
            this._d97Label.Name = "_d97Label";
            this._d97Label.Size = new System.Drawing.Size(28, 13);
            this._d97Label.TabIndex = 148;
            this._d97Label.Text = "Д97";
            // 
            // _d96
            // 
            this._d96.Location = new System.Drawing.Point(263, 304);
            this._d96.Name = "_d96";
            this._d96.Size = new System.Drawing.Size(13, 13);
            this._d96.State = BEMN.Forms.LedState.Off;
            this._d96.TabIndex = 147;
            // 
            // _d96Label
            // 
            this._d96Label.AutoSize = true;
            this._d96Label.Location = new System.Drawing.Point(282, 304);
            this._d96Label.Name = "_d96Label";
            this._d96Label.Size = new System.Drawing.Size(28, 13);
            this._d96Label.TabIndex = 146;
            this._d96Label.Text = "Д96";
            // 
            // _d95
            // 
            this._d95.Location = new System.Drawing.Point(263, 285);
            this._d95.Name = "_d95";
            this._d95.Size = new System.Drawing.Size(13, 13);
            this._d95.State = BEMN.Forms.LedState.Off;
            this._d95.TabIndex = 145;
            // 
            // _d95Label
            // 
            this._d95Label.AutoSize = true;
            this._d95Label.Location = new System.Drawing.Point(282, 285);
            this._d95Label.Name = "_d95Label";
            this._d95Label.Size = new System.Drawing.Size(28, 13);
            this._d95Label.TabIndex = 144;
            this._d95Label.Text = "Д95";
            // 
            // _d94
            // 
            this._d94.Location = new System.Drawing.Point(263, 266);
            this._d94.Name = "_d94";
            this._d94.Size = new System.Drawing.Size(13, 13);
            this._d94.State = BEMN.Forms.LedState.Off;
            this._d94.TabIndex = 143;
            // 
            // _d94Label
            // 
            this._d94Label.AutoSize = true;
            this._d94Label.Location = new System.Drawing.Point(282, 266);
            this._d94Label.Name = "_d94Label";
            this._d94Label.Size = new System.Drawing.Size(28, 13);
            this._d94Label.TabIndex = 142;
            this._d94Label.Text = "Д94";
            // 
            // _d93
            // 
            this._d93.Location = new System.Drawing.Point(263, 247);
            this._d93.Name = "_d93";
            this._d93.Size = new System.Drawing.Size(13, 13);
            this._d93.State = BEMN.Forms.LedState.Off;
            this._d93.TabIndex = 141;
            // 
            // _d93Label
            // 
            this._d93Label.AutoSize = true;
            this._d93Label.Location = new System.Drawing.Point(282, 247);
            this._d93Label.Name = "_d93Label";
            this._d93Label.Size = new System.Drawing.Size(28, 13);
            this._d93Label.TabIndex = 140;
            this._d93Label.Text = "Д93";
            // 
            // _d92
            // 
            this._d92.Location = new System.Drawing.Point(263, 228);
            this._d92.Name = "_d92";
            this._d92.Size = new System.Drawing.Size(13, 13);
            this._d92.State = BEMN.Forms.LedState.Off;
            this._d92.TabIndex = 139;
            // 
            // _d92Label
            // 
            this._d92Label.AutoSize = true;
            this._d92Label.Location = new System.Drawing.Point(282, 228);
            this._d92Label.Name = "_d92Label";
            this._d92Label.Size = new System.Drawing.Size(28, 13);
            this._d92Label.TabIndex = 138;
            this._d92Label.Text = "Д92";
            // 
            // _d91
            // 
            this._d91.Location = new System.Drawing.Point(263, 209);
            this._d91.Name = "_d91";
            this._d91.Size = new System.Drawing.Size(13, 13);
            this._d91.State = BEMN.Forms.LedState.Off;
            this._d91.TabIndex = 137;
            // 
            // _d91Label
            // 
            this._d91Label.AutoSize = true;
            this._d91Label.Location = new System.Drawing.Point(282, 209);
            this._d91Label.Name = "_d91Label";
            this._d91Label.Size = new System.Drawing.Size(28, 13);
            this._d91Label.TabIndex = 136;
            this._d91Label.Text = "Д91";
            // 
            // _d90
            // 
            this._d90.Location = new System.Drawing.Point(263, 190);
            this._d90.Name = "_d90";
            this._d90.Size = new System.Drawing.Size(13, 13);
            this._d90.State = BEMN.Forms.LedState.Off;
            this._d90.TabIndex = 135;
            // 
            // _d90Label
            // 
            this._d90Label.AutoSize = true;
            this._d90Label.Location = new System.Drawing.Point(282, 190);
            this._d90Label.Name = "_d90Label";
            this._d90Label.Size = new System.Drawing.Size(28, 13);
            this._d90Label.TabIndex = 134;
            this._d90Label.Text = "Д90";
            // 
            // _d89
            // 
            this._d89.Location = new System.Drawing.Point(263, 171);
            this._d89.Name = "_d89";
            this._d89.Size = new System.Drawing.Size(13, 13);
            this._d89.State = BEMN.Forms.LedState.Off;
            this._d89.TabIndex = 133;
            // 
            // _d89Label
            // 
            this._d89Label.AutoSize = true;
            this._d89Label.Location = new System.Drawing.Point(282, 171);
            this._d89Label.Name = "_d89Label";
            this._d89Label.Size = new System.Drawing.Size(28, 13);
            this._d89Label.TabIndex = 132;
            this._d89Label.Text = "Д89";
            // 
            // _d88
            // 
            this._d88.Location = new System.Drawing.Point(263, 152);
            this._d88.Name = "_d88";
            this._d88.Size = new System.Drawing.Size(13, 13);
            this._d88.State = BEMN.Forms.LedState.Off;
            this._d88.TabIndex = 131;
            // 
            // _d88Label
            // 
            this._d88Label.AutoSize = true;
            this._d88Label.Location = new System.Drawing.Point(282, 152);
            this._d88Label.Name = "_d88Label";
            this._d88Label.Size = new System.Drawing.Size(28, 13);
            this._d88Label.TabIndex = 130;
            this._d88Label.Text = "Д88";
            // 
            // _d87
            // 
            this._d87.Location = new System.Drawing.Point(263, 133);
            this._d87.Name = "_d87";
            this._d87.Size = new System.Drawing.Size(13, 13);
            this._d87.State = BEMN.Forms.LedState.Off;
            this._d87.TabIndex = 129;
            // 
            // _d87Label
            // 
            this._d87Label.AutoSize = true;
            this._d87Label.Location = new System.Drawing.Point(282, 133);
            this._d87Label.Name = "_d87Label";
            this._d87Label.Size = new System.Drawing.Size(28, 13);
            this._d87Label.TabIndex = 128;
            this._d87Label.Text = "Д87";
            // 
            // _d86
            // 
            this._d86.Location = new System.Drawing.Point(263, 114);
            this._d86.Name = "_d86";
            this._d86.Size = new System.Drawing.Size(13, 13);
            this._d86.State = BEMN.Forms.LedState.Off;
            this._d86.TabIndex = 127;
            // 
            // _d86Label
            // 
            this._d86Label.AutoSize = true;
            this._d86Label.Location = new System.Drawing.Point(282, 114);
            this._d86Label.Name = "_d86Label";
            this._d86Label.Size = new System.Drawing.Size(28, 13);
            this._d86Label.TabIndex = 126;
            this._d86Label.Text = "Д86";
            // 
            // _d85
            // 
            this._d85.Location = new System.Drawing.Point(263, 95);
            this._d85.Name = "_d85";
            this._d85.Size = new System.Drawing.Size(13, 13);
            this._d85.State = BEMN.Forms.LedState.Off;
            this._d85.TabIndex = 125;
            // 
            // _d85Label
            // 
            this._d85Label.AutoSize = true;
            this._d85Label.Location = new System.Drawing.Point(282, 95);
            this._d85Label.Name = "_d85Label";
            this._d85Label.Size = new System.Drawing.Size(28, 13);
            this._d85Label.TabIndex = 124;
            this._d85Label.Text = "Д85";
            // 
            // _d84
            // 
            this._d84.Location = new System.Drawing.Point(263, 76);
            this._d84.Name = "_d84";
            this._d84.Size = new System.Drawing.Size(13, 13);
            this._d84.State = BEMN.Forms.LedState.Off;
            this._d84.TabIndex = 123;
            // 
            // _d84Label
            // 
            this._d84Label.AutoSize = true;
            this._d84Label.Location = new System.Drawing.Point(282, 76);
            this._d84Label.Name = "_d84Label";
            this._d84Label.Size = new System.Drawing.Size(28, 13);
            this._d84Label.TabIndex = 122;
            this._d84Label.Text = "Д84";
            // 
            // _d83
            // 
            this._d83.Location = new System.Drawing.Point(263, 57);
            this._d83.Name = "_d83";
            this._d83.Size = new System.Drawing.Size(13, 13);
            this._d83.State = BEMN.Forms.LedState.Off;
            this._d83.TabIndex = 121;
            // 
            // _d83Label
            // 
            this._d83Label.AutoSize = true;
            this._d83Label.Location = new System.Drawing.Point(282, 57);
            this._d83Label.Name = "_d83Label";
            this._d83Label.Size = new System.Drawing.Size(28, 13);
            this._d83Label.TabIndex = 120;
            this._d83Label.Text = "Д83";
            // 
            // _d82
            // 
            this._d82.Location = new System.Drawing.Point(263, 38);
            this._d82.Name = "_d82";
            this._d82.Size = new System.Drawing.Size(13, 13);
            this._d82.State = BEMN.Forms.LedState.Off;
            this._d82.TabIndex = 119;
            // 
            // _d82Label
            // 
            this._d82Label.AutoSize = true;
            this._d82Label.Location = new System.Drawing.Point(282, 38);
            this._d82Label.Name = "_d82Label";
            this._d82Label.Size = new System.Drawing.Size(28, 13);
            this._d82Label.TabIndex = 118;
            this._d82Label.Text = "Д82";
            // 
            // _d81
            // 
            this._d81.Location = new System.Drawing.Point(263, 19);
            this._d81.Name = "_d81";
            this._d81.Size = new System.Drawing.Size(13, 13);
            this._d81.State = BEMN.Forms.LedState.Off;
            this._d81.TabIndex = 117;
            // 
            // _d81Label
            // 
            this._d81Label.AutoSize = true;
            this._d81Label.Location = new System.Drawing.Point(282, 19);
            this._d81Label.Name = "_d81Label";
            this._d81Label.Size = new System.Drawing.Size(28, 13);
            this._d81Label.TabIndex = 116;
            this._d81Label.Text = "Д81";
            // 
            // _d80
            // 
            this._d80.Location = new System.Drawing.Point(212, 304);
            this._d80.Name = "_d80";
            this._d80.Size = new System.Drawing.Size(13, 13);
            this._d80.State = BEMN.Forms.LedState.Off;
            this._d80.TabIndex = 115;
            // 
            // _d80Label
            // 
            this._d80Label.AutoSize = true;
            this._d80Label.Location = new System.Drawing.Point(231, 304);
            this._d80Label.Name = "_d80Label";
            this._d80Label.Size = new System.Drawing.Size(28, 13);
            this._d80Label.TabIndex = 114;
            this._d80Label.Text = "Д80";
            // 
            // _d79
            // 
            this._d79.Location = new System.Drawing.Point(212, 285);
            this._d79.Name = "_d79";
            this._d79.Size = new System.Drawing.Size(13, 13);
            this._d79.State = BEMN.Forms.LedState.Off;
            this._d79.TabIndex = 113;
            // 
            // _d79Label
            // 
            this._d79Label.AutoSize = true;
            this._d79Label.Location = new System.Drawing.Point(231, 285);
            this._d79Label.Name = "_d79Label";
            this._d79Label.Size = new System.Drawing.Size(28, 13);
            this._d79Label.TabIndex = 112;
            this._d79Label.Text = "Д79";
            // 
            // _d78
            // 
            this._d78.Location = new System.Drawing.Point(212, 266);
            this._d78.Name = "_d78";
            this._d78.Size = new System.Drawing.Size(13, 13);
            this._d78.State = BEMN.Forms.LedState.Off;
            this._d78.TabIndex = 111;
            // 
            // _d78Label
            // 
            this._d78Label.AutoSize = true;
            this._d78Label.Location = new System.Drawing.Point(231, 266);
            this._d78Label.Name = "_d78Label";
            this._d78Label.Size = new System.Drawing.Size(28, 13);
            this._d78Label.TabIndex = 110;
            this._d78Label.Text = "Д78";
            // 
            // _d77
            // 
            this._d77.Location = new System.Drawing.Point(212, 247);
            this._d77.Name = "_d77";
            this._d77.Size = new System.Drawing.Size(13, 13);
            this._d77.State = BEMN.Forms.LedState.Off;
            this._d77.TabIndex = 109;
            // 
            // _d77Label
            // 
            this._d77Label.AutoSize = true;
            this._d77Label.Location = new System.Drawing.Point(231, 247);
            this._d77Label.Name = "_d77Label";
            this._d77Label.Size = new System.Drawing.Size(28, 13);
            this._d77Label.TabIndex = 108;
            this._d77Label.Text = "Д77";
            // 
            // _d76
            // 
            this._d76.Location = new System.Drawing.Point(212, 228);
            this._d76.Name = "_d76";
            this._d76.Size = new System.Drawing.Size(13, 13);
            this._d76.State = BEMN.Forms.LedState.Off;
            this._d76.TabIndex = 107;
            // 
            // _d76Label
            // 
            this._d76Label.AutoSize = true;
            this._d76Label.Location = new System.Drawing.Point(231, 228);
            this._d76Label.Name = "_d76Label";
            this._d76Label.Size = new System.Drawing.Size(28, 13);
            this._d76Label.TabIndex = 106;
            this._d76Label.Text = "Д76";
            // 
            // _d75
            // 
            this._d75.Location = new System.Drawing.Point(212, 209);
            this._d75.Name = "_d75";
            this._d75.Size = new System.Drawing.Size(13, 13);
            this._d75.State = BEMN.Forms.LedState.Off;
            this._d75.TabIndex = 105;
            // 
            // _d75Label
            // 
            this._d75Label.AutoSize = true;
            this._d75Label.Location = new System.Drawing.Point(231, 209);
            this._d75Label.Name = "_d75Label";
            this._d75Label.Size = new System.Drawing.Size(28, 13);
            this._d75Label.TabIndex = 104;
            this._d75Label.Text = "Д75";
            // 
            // _d74
            // 
            this._d74.Location = new System.Drawing.Point(212, 190);
            this._d74.Name = "_d74";
            this._d74.Size = new System.Drawing.Size(13, 13);
            this._d74.State = BEMN.Forms.LedState.Off;
            this._d74.TabIndex = 103;
            // 
            // _d74Label
            // 
            this._d74Label.AutoSize = true;
            this._d74Label.Location = new System.Drawing.Point(231, 190);
            this._d74Label.Name = "_d74Label";
            this._d74Label.Size = new System.Drawing.Size(28, 13);
            this._d74Label.TabIndex = 102;
            this._d74Label.Text = "Д74";
            // 
            // _d73
            // 
            this._d73.Location = new System.Drawing.Point(212, 171);
            this._d73.Name = "_d73";
            this._d73.Size = new System.Drawing.Size(13, 13);
            this._d73.State = BEMN.Forms.LedState.Off;
            this._d73.TabIndex = 101;
            // 
            // _d73Label
            // 
            this._d73Label.AutoSize = true;
            this._d73Label.Location = new System.Drawing.Point(231, 171);
            this._d73Label.Name = "_d73Label";
            this._d73Label.Size = new System.Drawing.Size(28, 13);
            this._d73Label.TabIndex = 100;
            this._d73Label.Text = "Д73";
            // 
            // _k2
            // 
            this._k2.Location = new System.Drawing.Point(369, 38);
            this._k2.Name = "_k2";
            this._k2.Size = new System.Drawing.Size(13, 13);
            this._k2.State = BEMN.Forms.LedState.Off;
            this._k2.TabIndex = 99;
            // 
            // _k2Label
            // 
            this._k2Label.AutoSize = true;
            this._k2Label.Location = new System.Drawing.Point(388, 38);
            this._k2Label.Name = "_k2Label";
            this._k2Label.Size = new System.Drawing.Size(20, 13);
            this._k2Label.TabIndex = 98;
            this._k2Label.Text = "K2";
            // 
            // _k1
            // 
            this._k1.Location = new System.Drawing.Point(369, 19);
            this._k1.Name = "_k1";
            this._k1.Size = new System.Drawing.Size(13, 13);
            this._k1.State = BEMN.Forms.LedState.Off;
            this._k1.TabIndex = 97;
            // 
            // _k1Label
            // 
            this._k1Label.AutoSize = true;
            this._k1Label.Location = new System.Drawing.Point(388, 19);
            this._k1Label.Name = "_k1Label";
            this._k1Label.Size = new System.Drawing.Size(20, 13);
            this._k1Label.TabIndex = 96;
            this._k1Label.Text = "K1";
            // 
            // _d72
            // 
            this._d72.Location = new System.Drawing.Point(212, 152);
            this._d72.Name = "_d72";
            this._d72.Size = new System.Drawing.Size(13, 13);
            this._d72.State = BEMN.Forms.LedState.Off;
            this._d72.TabIndex = 95;
            // 
            // _d72Label
            // 
            this._d72Label.AutoSize = true;
            this._d72Label.Location = new System.Drawing.Point(231, 151);
            this._d72Label.Name = "_d72Label";
            this._d72Label.Size = new System.Drawing.Size(28, 13);
            this._d72Label.TabIndex = 94;
            this._d72Label.Text = "Д72";
            // 
            // _d71
            // 
            this._d71.Location = new System.Drawing.Point(212, 133);
            this._d71.Name = "_d71";
            this._d71.Size = new System.Drawing.Size(13, 13);
            this._d71.State = BEMN.Forms.LedState.Off;
            this._d71.TabIndex = 93;
            // 
            // _d71Label
            // 
            this._d71Label.AutoSize = true;
            this._d71Label.Location = new System.Drawing.Point(231, 132);
            this._d71Label.Name = "_d71Label";
            this._d71Label.Size = new System.Drawing.Size(28, 13);
            this._d71Label.TabIndex = 92;
            this._d71Label.Text = "Д71";
            // 
            // _d70
            // 
            this._d70.Location = new System.Drawing.Point(212, 114);
            this._d70.Name = "_d70";
            this._d70.Size = new System.Drawing.Size(13, 13);
            this._d70.State = BEMN.Forms.LedState.Off;
            this._d70.TabIndex = 91;
            // 
            // _d70Label
            // 
            this._d70Label.AutoSize = true;
            this._d70Label.Location = new System.Drawing.Point(231, 113);
            this._d70Label.Name = "_d70Label";
            this._d70Label.Size = new System.Drawing.Size(28, 13);
            this._d70Label.TabIndex = 90;
            this._d70Label.Text = "Д70";
            // 
            // _d69
            // 
            this._d69.Location = new System.Drawing.Point(212, 95);
            this._d69.Name = "_d69";
            this._d69.Size = new System.Drawing.Size(13, 13);
            this._d69.State = BEMN.Forms.LedState.Off;
            this._d69.TabIndex = 89;
            // 
            // _d69Label
            // 
            this._d69Label.AutoSize = true;
            this._d69Label.Location = new System.Drawing.Point(231, 94);
            this._d69Label.Name = "_d69Label";
            this._d69Label.Size = new System.Drawing.Size(28, 13);
            this._d69Label.TabIndex = 88;
            this._d69Label.Text = "Д69";
            // 
            // _d68
            // 
            this._d68.Location = new System.Drawing.Point(212, 76);
            this._d68.Name = "_d68";
            this._d68.Size = new System.Drawing.Size(13, 13);
            this._d68.State = BEMN.Forms.LedState.Off;
            this._d68.TabIndex = 87;
            // 
            // _d68Label
            // 
            this._d68Label.AutoSize = true;
            this._d68Label.Location = new System.Drawing.Point(231, 75);
            this._d68Label.Name = "_d68Label";
            this._d68Label.Size = new System.Drawing.Size(28, 13);
            this._d68Label.TabIndex = 86;
            this._d68Label.Text = "Д68";
            // 
            // _d67
            // 
            this._d67.Location = new System.Drawing.Point(212, 57);
            this._d67.Name = "_d67";
            this._d67.Size = new System.Drawing.Size(13, 13);
            this._d67.State = BEMN.Forms.LedState.Off;
            this._d67.TabIndex = 85;
            // 
            // _d67Label
            // 
            this._d67Label.AutoSize = true;
            this._d67Label.Location = new System.Drawing.Point(231, 56);
            this._d67Label.Name = "_d67Label";
            this._d67Label.Size = new System.Drawing.Size(28, 13);
            this._d67Label.TabIndex = 84;
            this._d67Label.Text = "Д67";
            // 
            // _d66
            // 
            this._d66.Location = new System.Drawing.Point(212, 38);
            this._d66.Name = "_d66";
            this._d66.Size = new System.Drawing.Size(13, 13);
            this._d66.State = BEMN.Forms.LedState.Off;
            this._d66.TabIndex = 83;
            // 
            // _d66Label
            // 
            this._d66Label.AutoSize = true;
            this._d66Label.Location = new System.Drawing.Point(231, 37);
            this._d66Label.Name = "_d66Label";
            this._d66Label.Size = new System.Drawing.Size(28, 13);
            this._d66Label.TabIndex = 82;
            this._d66Label.Text = "Д66";
            // 
            // _d65
            // 
            this._d65.Location = new System.Drawing.Point(212, 19);
            this._d65.Name = "_d65";
            this._d65.Size = new System.Drawing.Size(13, 13);
            this._d65.State = BEMN.Forms.LedState.Off;
            this._d65.TabIndex = 81;
            // 
            // _d65Label
            // 
            this._d65Label.AutoSize = true;
            this._d65Label.Location = new System.Drawing.Point(231, 18);
            this._d65Label.Name = "_d65Label";
            this._d65Label.Size = new System.Drawing.Size(28, 13);
            this._d65Label.TabIndex = 80;
            this._d65Label.Text = "Д65";
            // 
            // _d64
            // 
            this._d64.Location = new System.Drawing.Point(159, 304);
            this._d64.Name = "_d64";
            this._d64.Size = new System.Drawing.Size(13, 13);
            this._d64.State = BEMN.Forms.LedState.Off;
            this._d64.TabIndex = 79;
            // 
            // _d64Label
            // 
            this._d64Label.AutoSize = true;
            this._d64Label.Location = new System.Drawing.Point(178, 304);
            this._d64Label.Name = "_d64Label";
            this._d64Label.Size = new System.Drawing.Size(28, 13);
            this._d64Label.TabIndex = 78;
            this._d64Label.Text = "Д64";
            // 
            // _d63
            // 
            this._d63.Location = new System.Drawing.Point(159, 285);
            this._d63.Name = "_d63";
            this._d63.Size = new System.Drawing.Size(13, 13);
            this._d63.State = BEMN.Forms.LedState.Off;
            this._d63.TabIndex = 77;
            // 
            // _d63Label
            // 
            this._d63Label.AutoSize = true;
            this._d63Label.Location = new System.Drawing.Point(178, 285);
            this._d63Label.Name = "_d63Label";
            this._d63Label.Size = new System.Drawing.Size(28, 13);
            this._d63Label.TabIndex = 76;
            this._d63Label.Text = "Д63";
            // 
            // _d62
            // 
            this._d62.Location = new System.Drawing.Point(159, 266);
            this._d62.Name = "_d62";
            this._d62.Size = new System.Drawing.Size(13, 13);
            this._d62.State = BEMN.Forms.LedState.Off;
            this._d62.TabIndex = 75;
            // 
            // _d62Label
            // 
            this._d62Label.AutoSize = true;
            this._d62Label.Location = new System.Drawing.Point(178, 266);
            this._d62Label.Name = "_d62Label";
            this._d62Label.Size = new System.Drawing.Size(28, 13);
            this._d62Label.TabIndex = 74;
            this._d62Label.Text = "Д62";
            // 
            // _d61
            // 
            this._d61.Location = new System.Drawing.Point(159, 247);
            this._d61.Name = "_d61";
            this._d61.Size = new System.Drawing.Size(13, 13);
            this._d61.State = BEMN.Forms.LedState.Off;
            this._d61.TabIndex = 73;
            // 
            // _d61Label
            // 
            this._d61Label.AutoSize = true;
            this._d61Label.Location = new System.Drawing.Point(178, 247);
            this._d61Label.Name = "_d61Label";
            this._d61Label.Size = new System.Drawing.Size(28, 13);
            this._d61Label.TabIndex = 72;
            this._d61Label.Text = "Д61";
            // 
            // _d60
            // 
            this._d60.Location = new System.Drawing.Point(159, 228);
            this._d60.Name = "_d60";
            this._d60.Size = new System.Drawing.Size(13, 13);
            this._d60.State = BEMN.Forms.LedState.Off;
            this._d60.TabIndex = 71;
            // 
            // _d60Label
            // 
            this._d60Label.AutoSize = true;
            this._d60Label.Location = new System.Drawing.Point(178, 228);
            this._d60Label.Name = "_d60Label";
            this._d60Label.Size = new System.Drawing.Size(28, 13);
            this._d60Label.TabIndex = 70;
            this._d60Label.Text = "Д60";
            // 
            // _d59
            // 
            this._d59.Location = new System.Drawing.Point(159, 209);
            this._d59.Name = "_d59";
            this._d59.Size = new System.Drawing.Size(13, 13);
            this._d59.State = BEMN.Forms.LedState.Off;
            this._d59.TabIndex = 69;
            // 
            // _d59Label
            // 
            this._d59Label.AutoSize = true;
            this._d59Label.Location = new System.Drawing.Point(178, 209);
            this._d59Label.Name = "_d59Label";
            this._d59Label.Size = new System.Drawing.Size(28, 13);
            this._d59Label.TabIndex = 68;
            this._d59Label.Text = "Д59";
            // 
            // _d58
            // 
            this._d58.Location = new System.Drawing.Point(159, 190);
            this._d58.Name = "_d58";
            this._d58.Size = new System.Drawing.Size(13, 13);
            this._d58.State = BEMN.Forms.LedState.Off;
            this._d58.TabIndex = 67;
            // 
            // _d58Label
            // 
            this._d58Label.AutoSize = true;
            this._d58Label.Location = new System.Drawing.Point(178, 190);
            this._d58Label.Name = "_d58Label";
            this._d58Label.Size = new System.Drawing.Size(28, 13);
            this._d58Label.TabIndex = 66;
            this._d58Label.Text = "Д58";
            // 
            // _d57
            // 
            this._d57.Location = new System.Drawing.Point(159, 171);
            this._d57.Name = "_d57";
            this._d57.Size = new System.Drawing.Size(13, 13);
            this._d57.State = BEMN.Forms.LedState.Off;
            this._d57.TabIndex = 65;
            // 
            // _d57Label
            // 
            this._d57Label.AutoSize = true;
            this._d57Label.Location = new System.Drawing.Point(178, 171);
            this._d57Label.Name = "_d57Label";
            this._d57Label.Size = new System.Drawing.Size(28, 13);
            this._d57Label.TabIndex = 64;
            this._d57Label.Text = "Д57";
            // 
            // _d56
            // 
            this._d56.Location = new System.Drawing.Point(159, 152);
            this._d56.Name = "_d56";
            this._d56.Size = new System.Drawing.Size(13, 13);
            this._d56.State = BEMN.Forms.LedState.Off;
            this._d56.TabIndex = 63;
            // 
            // _d56Label
            // 
            this._d56Label.AutoSize = true;
            this._d56Label.Location = new System.Drawing.Point(178, 152);
            this._d56Label.Name = "_d56Label";
            this._d56Label.Size = new System.Drawing.Size(28, 13);
            this._d56Label.TabIndex = 62;
            this._d56Label.Text = "Д56";
            // 
            // _d55
            // 
            this._d55.Location = new System.Drawing.Point(159, 133);
            this._d55.Name = "_d55";
            this._d55.Size = new System.Drawing.Size(13, 13);
            this._d55.State = BEMN.Forms.LedState.Off;
            this._d55.TabIndex = 61;
            // 
            // _d55Label
            // 
            this._d55Label.AutoSize = true;
            this._d55Label.Location = new System.Drawing.Point(178, 133);
            this._d55Label.Name = "_d55Label";
            this._d55Label.Size = new System.Drawing.Size(28, 13);
            this._d55Label.TabIndex = 60;
            this._d55Label.Text = "Д55";
            // 
            // _d54
            // 
            this._d54.Location = new System.Drawing.Point(159, 114);
            this._d54.Name = "_d54";
            this._d54.Size = new System.Drawing.Size(13, 13);
            this._d54.State = BEMN.Forms.LedState.Off;
            this._d54.TabIndex = 59;
            // 
            // _d54Label
            // 
            this._d54Label.AutoSize = true;
            this._d54Label.Location = new System.Drawing.Point(178, 114);
            this._d54Label.Name = "_d54Label";
            this._d54Label.Size = new System.Drawing.Size(28, 13);
            this._d54Label.TabIndex = 58;
            this._d54Label.Text = "Д54";
            // 
            // _d53
            // 
            this._d53.Location = new System.Drawing.Point(159, 95);
            this._d53.Name = "_d53";
            this._d53.Size = new System.Drawing.Size(13, 13);
            this._d53.State = BEMN.Forms.LedState.Off;
            this._d53.TabIndex = 57;
            // 
            // _d53Label
            // 
            this._d53Label.AutoSize = true;
            this._d53Label.Location = new System.Drawing.Point(178, 95);
            this._d53Label.Name = "_d53Label";
            this._d53Label.Size = new System.Drawing.Size(28, 13);
            this._d53Label.TabIndex = 56;
            this._d53Label.Text = "Д53";
            // 
            // _d52
            // 
            this._d52.Location = new System.Drawing.Point(159, 76);
            this._d52.Name = "_d52";
            this._d52.Size = new System.Drawing.Size(13, 13);
            this._d52.State = BEMN.Forms.LedState.Off;
            this._d52.TabIndex = 55;
            // 
            // _d52Label
            // 
            this._d52Label.AutoSize = true;
            this._d52Label.Location = new System.Drawing.Point(178, 76);
            this._d52Label.Name = "_d52Label";
            this._d52Label.Size = new System.Drawing.Size(28, 13);
            this._d52Label.TabIndex = 54;
            this._d52Label.Text = "Д52";
            // 
            // _d51
            // 
            this._d51.Location = new System.Drawing.Point(159, 57);
            this._d51.Name = "_d51";
            this._d51.Size = new System.Drawing.Size(13, 13);
            this._d51.State = BEMN.Forms.LedState.Off;
            this._d51.TabIndex = 53;
            // 
            // _d51Label
            // 
            this._d51Label.AutoSize = true;
            this._d51Label.Location = new System.Drawing.Point(178, 57);
            this._d51Label.Name = "_d51Label";
            this._d51Label.Size = new System.Drawing.Size(28, 13);
            this._d51Label.TabIndex = 52;
            this._d51Label.Text = "Д51";
            // 
            // _d50
            // 
            this._d50.Location = new System.Drawing.Point(159, 38);
            this._d50.Name = "_d50";
            this._d50.Size = new System.Drawing.Size(13, 13);
            this._d50.State = BEMN.Forms.LedState.Off;
            this._d50.TabIndex = 51;
            // 
            // _d50Label
            // 
            this._d50Label.AutoSize = true;
            this._d50Label.Location = new System.Drawing.Point(178, 38);
            this._d50Label.Name = "_d50Label";
            this._d50Label.Size = new System.Drawing.Size(28, 13);
            this._d50Label.TabIndex = 50;
            this._d50Label.Text = "Д50";
            // 
            // _d49
            // 
            this._d49.Location = new System.Drawing.Point(159, 19);
            this._d49.Name = "_d49";
            this._d49.Size = new System.Drawing.Size(13, 13);
            this._d49.State = BEMN.Forms.LedState.Off;
            this._d49.TabIndex = 49;
            // 
            // _d49Label
            // 
            this._d49Label.AutoSize = true;
            this._d49Label.Location = new System.Drawing.Point(178, 19);
            this._d49Label.Name = "_d49Label";
            this._d49Label.Size = new System.Drawing.Size(28, 13);
            this._d49Label.TabIndex = 48;
            this._d49Label.Text = "Д49";
            // 
            // _d48
            // 
            this._d48.Location = new System.Drawing.Point(106, 304);
            this._d48.Name = "_d48";
            this._d48.Size = new System.Drawing.Size(13, 13);
            this._d48.State = BEMN.Forms.LedState.Off;
            this._d48.TabIndex = 47;
            // 
            // _d48Label
            // 
            this._d48Label.AutoSize = true;
            this._d48Label.Location = new System.Drawing.Point(125, 304);
            this._d48Label.Name = "_d48Label";
            this._d48Label.Size = new System.Drawing.Size(28, 13);
            this._d48Label.TabIndex = 46;
            this._d48Label.Text = "Д48";
            // 
            // _d47
            // 
            this._d47.Location = new System.Drawing.Point(106, 285);
            this._d47.Name = "_d47";
            this._d47.Size = new System.Drawing.Size(13, 13);
            this._d47.State = BEMN.Forms.LedState.Off;
            this._d47.TabIndex = 45;
            // 
            // _d47Label
            // 
            this._d47Label.AutoSize = true;
            this._d47Label.Location = new System.Drawing.Point(125, 285);
            this._d47Label.Name = "_d47Label";
            this._d47Label.Size = new System.Drawing.Size(28, 13);
            this._d47Label.TabIndex = 44;
            this._d47Label.Text = "Д47";
            // 
            // _d46
            // 
            this._d46.Location = new System.Drawing.Point(106, 266);
            this._d46.Name = "_d46";
            this._d46.Size = new System.Drawing.Size(13, 13);
            this._d46.State = BEMN.Forms.LedState.Off;
            this._d46.TabIndex = 43;
            // 
            // _d46Label
            // 
            this._d46Label.AutoSize = true;
            this._d46Label.Location = new System.Drawing.Point(125, 266);
            this._d46Label.Name = "_d46Label";
            this._d46Label.Size = new System.Drawing.Size(28, 13);
            this._d46Label.TabIndex = 42;
            this._d46Label.Text = "Д46";
            // 
            // _d45
            // 
            this._d45.Location = new System.Drawing.Point(106, 247);
            this._d45.Name = "_d45";
            this._d45.Size = new System.Drawing.Size(13, 13);
            this._d45.State = BEMN.Forms.LedState.Off;
            this._d45.TabIndex = 41;
            // 
            // _d45Label
            // 
            this._d45Label.AutoSize = true;
            this._d45Label.Location = new System.Drawing.Point(125, 247);
            this._d45Label.Name = "_d45Label";
            this._d45Label.Size = new System.Drawing.Size(28, 13);
            this._d45Label.TabIndex = 40;
            this._d45Label.Text = "Д45";
            // 
            // _d44
            // 
            this._d44.Location = new System.Drawing.Point(106, 228);
            this._d44.Name = "_d44";
            this._d44.Size = new System.Drawing.Size(13, 13);
            this._d44.State = BEMN.Forms.LedState.Off;
            this._d44.TabIndex = 39;
            // 
            // _d44Label
            // 
            this._d44Label.AutoSize = true;
            this._d44Label.Location = new System.Drawing.Point(125, 228);
            this._d44Label.Name = "_d44Label";
            this._d44Label.Size = new System.Drawing.Size(28, 13);
            this._d44Label.TabIndex = 38;
            this._d44Label.Text = "Д44";
            // 
            // _d43
            // 
            this._d43.Location = new System.Drawing.Point(106, 209);
            this._d43.Name = "_d43";
            this._d43.Size = new System.Drawing.Size(13, 13);
            this._d43.State = BEMN.Forms.LedState.Off;
            this._d43.TabIndex = 37;
            // 
            // _d43Label
            // 
            this._d43Label.AutoSize = true;
            this._d43Label.Location = new System.Drawing.Point(125, 209);
            this._d43Label.Name = "_d43Label";
            this._d43Label.Size = new System.Drawing.Size(28, 13);
            this._d43Label.TabIndex = 36;
            this._d43Label.Text = "Д43";
            // 
            // _d42
            // 
            this._d42.Location = new System.Drawing.Point(106, 190);
            this._d42.Name = "_d42";
            this._d42.Size = new System.Drawing.Size(13, 13);
            this._d42.State = BEMN.Forms.LedState.Off;
            this._d42.TabIndex = 35;
            // 
            // _d42Label
            // 
            this._d42Label.AutoSize = true;
            this._d42Label.Location = new System.Drawing.Point(125, 190);
            this._d42Label.Name = "_d42Label";
            this._d42Label.Size = new System.Drawing.Size(28, 13);
            this._d42Label.TabIndex = 34;
            this._d42Label.Text = "Д42";
            // 
            // _d41
            // 
            this._d41.Location = new System.Drawing.Point(106, 171);
            this._d41.Name = "_d41";
            this._d41.Size = new System.Drawing.Size(13, 13);
            this._d41.State = BEMN.Forms.LedState.Off;
            this._d41.TabIndex = 33;
            // 
            // _d41Label
            // 
            this._d41Label.AutoSize = true;
            this._d41Label.Location = new System.Drawing.Point(125, 171);
            this._d41Label.Name = "_d41Label";
            this._d41Label.Size = new System.Drawing.Size(28, 13);
            this._d41Label.TabIndex = 32;
            this._d41Label.Text = "Д41";
            // 
            // _d40
            // 
            this._d40.Location = new System.Drawing.Point(106, 152);
            this._d40.Name = "_d40";
            this._d40.Size = new System.Drawing.Size(13, 13);
            this._d40.State = BEMN.Forms.LedState.Off;
            this._d40.TabIndex = 31;
            // 
            // _d24
            // 
            this._d24.Location = new System.Drawing.Point(53, 152);
            this._d24.Name = "_d24";
            this._d24.Size = new System.Drawing.Size(13, 13);
            this._d24.State = BEMN.Forms.LedState.Off;
            this._d24.TabIndex = 31;
            // 
            // _d40Label
            // 
            this._d40Label.AutoSize = true;
            this._d40Label.Location = new System.Drawing.Point(125, 152);
            this._d40Label.Name = "_d40Label";
            this._d40Label.Size = new System.Drawing.Size(28, 13);
            this._d40Label.TabIndex = 30;
            this._d40Label.Text = "Д40";
            // 
            // _d8
            // 
            this._d8.Location = new System.Drawing.Point(6, 152);
            this._d8.Name = "_d8";
            this._d8.Size = new System.Drawing.Size(13, 13);
            this._d8.State = BEMN.Forms.LedState.Off;
            this._d8.TabIndex = 15;
            // 
            // _d39
            // 
            this._d39.Location = new System.Drawing.Point(106, 133);
            this._d39.Name = "_d39";
            this._d39.Size = new System.Drawing.Size(13, 13);
            this._d39.State = BEMN.Forms.LedState.Off;
            this._d39.TabIndex = 29;
            // 
            // _d24Label
            // 
            this._d24Label.AutoSize = true;
            this._d24Label.Location = new System.Drawing.Point(72, 152);
            this._d24Label.Name = "_d24Label";
            this._d24Label.Size = new System.Drawing.Size(28, 13);
            this._d24Label.TabIndex = 30;
            this._d24Label.Text = "Д24";
            // 
            // _d39Label
            // 
            this._d39Label.AutoSize = true;
            this._d39Label.Location = new System.Drawing.Point(125, 133);
            this._d39Label.Name = "_d39Label";
            this._d39Label.Size = new System.Drawing.Size(28, 13);
            this._d39Label.TabIndex = 28;
            this._d39Label.Text = "Д39";
            // 
            // _d38
            // 
            this._d38.Location = new System.Drawing.Point(106, 114);
            this._d38.Name = "_d38";
            this._d38.Size = new System.Drawing.Size(13, 13);
            this._d38.State = BEMN.Forms.LedState.Off;
            this._d38.TabIndex = 27;
            // 
            // _d23
            // 
            this._d23.Location = new System.Drawing.Point(53, 133);
            this._d23.Name = "_d23";
            this._d23.Size = new System.Drawing.Size(13, 13);
            this._d23.State = BEMN.Forms.LedState.Off;
            this._d23.TabIndex = 29;
            // 
            // _d38Label
            // 
            this._d38Label.AutoSize = true;
            this._d38Label.Location = new System.Drawing.Point(125, 114);
            this._d38Label.Name = "_d38Label";
            this._d38Label.Size = new System.Drawing.Size(28, 13);
            this._d38Label.TabIndex = 26;
            this._d38Label.Text = "Д38";
            // 
            // _d8Label
            // 
            this._d8Label.AutoSize = true;
            this._d8Label.Location = new System.Drawing.Point(25, 152);
            this._d8Label.Name = "_d8Label";
            this._d8Label.Size = new System.Drawing.Size(22, 13);
            this._d8Label.TabIndex = 14;
            this._d8Label.Text = "Д8";
            // 
            // _d37
            // 
            this._d37.Location = new System.Drawing.Point(106, 95);
            this._d37.Name = "_d37";
            this._d37.Size = new System.Drawing.Size(13, 13);
            this._d37.State = BEMN.Forms.LedState.Off;
            this._d37.TabIndex = 25;
            // 
            // _d23Label
            // 
            this._d23Label.AutoSize = true;
            this._d23Label.Location = new System.Drawing.Point(72, 133);
            this._d23Label.Name = "_d23Label";
            this._d23Label.Size = new System.Drawing.Size(28, 13);
            this._d23Label.TabIndex = 28;
            this._d23Label.Text = "Д23";
            // 
            // _d37Label
            // 
            this._d37Label.AutoSize = true;
            this._d37Label.Location = new System.Drawing.Point(125, 95);
            this._d37Label.Name = "_d37Label";
            this._d37Label.Size = new System.Drawing.Size(28, 13);
            this._d37Label.TabIndex = 24;
            this._d37Label.Text = "Д37";
            // 
            // _d22
            // 
            this._d22.Location = new System.Drawing.Point(53, 114);
            this._d22.Name = "_d22";
            this._d22.Size = new System.Drawing.Size(13, 13);
            this._d22.State = BEMN.Forms.LedState.Off;
            this._d22.TabIndex = 27;
            // 
            // _d36
            // 
            this._d36.Location = new System.Drawing.Point(106, 76);
            this._d36.Name = "_d36";
            this._d36.Size = new System.Drawing.Size(13, 13);
            this._d36.State = BEMN.Forms.LedState.Off;
            this._d36.TabIndex = 23;
            // 
            // _d7
            // 
            this._d7.Location = new System.Drawing.Point(6, 133);
            this._d7.Name = "_d7";
            this._d7.Size = new System.Drawing.Size(13, 13);
            this._d7.State = BEMN.Forms.LedState.Off;
            this._d7.TabIndex = 13;
            // 
            // _d36Label
            // 
            this._d36Label.AutoSize = true;
            this._d36Label.Location = new System.Drawing.Point(125, 76);
            this._d36Label.Name = "_d36Label";
            this._d36Label.Size = new System.Drawing.Size(28, 13);
            this._d36Label.TabIndex = 22;
            this._d36Label.Text = "Д36";
            // 
            // _d22Label
            // 
            this._d22Label.AutoSize = true;
            this._d22Label.Location = new System.Drawing.Point(72, 114);
            this._d22Label.Name = "_d22Label";
            this._d22Label.Size = new System.Drawing.Size(28, 13);
            this._d22Label.TabIndex = 26;
            this._d22Label.Text = "Д22";
            // 
            // _d35
            // 
            this._d35.Location = new System.Drawing.Point(106, 57);
            this._d35.Name = "_d35";
            this._d35.Size = new System.Drawing.Size(13, 13);
            this._d35.State = BEMN.Forms.LedState.Off;
            this._d35.TabIndex = 21;
            // 
            // _d7Label
            // 
            this._d7Label.AutoSize = true;
            this._d7Label.Location = new System.Drawing.Point(25, 133);
            this._d7Label.Name = "_d7Label";
            this._d7Label.Size = new System.Drawing.Size(22, 13);
            this._d7Label.TabIndex = 12;
            this._d7Label.Text = "Д7";
            // 
            // _d35Label
            // 
            this._d35Label.AutoSize = true;
            this._d35Label.Location = new System.Drawing.Point(125, 57);
            this._d35Label.Name = "_d35Label";
            this._d35Label.Size = new System.Drawing.Size(28, 13);
            this._d35Label.TabIndex = 20;
            this._d35Label.Text = "Д35";
            // 
            // _d21
            // 
            this._d21.Location = new System.Drawing.Point(53, 95);
            this._d21.Name = "_d21";
            this._d21.Size = new System.Drawing.Size(13, 13);
            this._d21.State = BEMN.Forms.LedState.Off;
            this._d21.TabIndex = 25;
            // 
            // _d34
            // 
            this._d34.Location = new System.Drawing.Point(106, 38);
            this._d34.Name = "_d34";
            this._d34.Size = new System.Drawing.Size(13, 13);
            this._d34.State = BEMN.Forms.LedState.Off;
            this._d34.TabIndex = 19;
            // 
            // _d1
            // 
            this._d1.Location = new System.Drawing.Point(6, 19);
            this._d1.Name = "_d1";
            this._d1.Size = new System.Drawing.Size(13, 13);
            this._d1.State = BEMN.Forms.LedState.Off;
            this._d1.TabIndex = 1;
            // 
            // _d34Label
            // 
            this._d34Label.AutoSize = true;
            this._d34Label.Location = new System.Drawing.Point(125, 38);
            this._d34Label.Name = "_d34Label";
            this._d34Label.Size = new System.Drawing.Size(28, 13);
            this._d34Label.TabIndex = 18;
            this._d34Label.Text = "Д34";
            // 
            // _d21Label
            // 
            this._d21Label.AutoSize = true;
            this._d21Label.Location = new System.Drawing.Point(72, 95);
            this._d21Label.Name = "_d21Label";
            this._d21Label.Size = new System.Drawing.Size(28, 13);
            this._d21Label.TabIndex = 24;
            this._d21Label.Text = "Д21";
            // 
            // _d33
            // 
            this._d33.Location = new System.Drawing.Point(106, 19);
            this._d33.Name = "_d33";
            this._d33.Size = new System.Drawing.Size(13, 13);
            this._d33.State = BEMN.Forms.LedState.Off;
            this._d33.TabIndex = 17;
            // 
            // _d33Label
            // 
            this._d33Label.AutoSize = true;
            this._d33Label.Location = new System.Drawing.Point(125, 19);
            this._d33Label.Name = "_d33Label";
            this._d33Label.Size = new System.Drawing.Size(28, 13);
            this._d33Label.TabIndex = 16;
            this._d33Label.Text = "Д33";
            // 
            // _d6
            // 
            this._d6.Location = new System.Drawing.Point(6, 114);
            this._d6.Name = "_d6";
            this._d6.Size = new System.Drawing.Size(13, 13);
            this._d6.State = BEMN.Forms.LedState.Off;
            this._d6.TabIndex = 11;
            // 
            // _d20
            // 
            this._d20.Location = new System.Drawing.Point(53, 76);
            this._d20.Name = "_d20";
            this._d20.Size = new System.Drawing.Size(13, 13);
            this._d20.State = BEMN.Forms.LedState.Off;
            this._d20.TabIndex = 23;
            // 
            // _d32
            // 
            this._d32.Location = new System.Drawing.Point(53, 304);
            this._d32.Name = "_d32";
            this._d32.Size = new System.Drawing.Size(13, 13);
            this._d32.State = BEMN.Forms.LedState.Off;
            this._d32.TabIndex = 15;
            // 
            // _d1Label
            // 
            this._d1Label.AutoSize = true;
            this._d1Label.Location = new System.Drawing.Point(25, 19);
            this._d1Label.Name = "_d1Label";
            this._d1Label.Size = new System.Drawing.Size(22, 13);
            this._d1Label.TabIndex = 0;
            this._d1Label.Text = "Д1";
            // 
            // _d32Label
            // 
            this._d32Label.AutoSize = true;
            this._d32Label.Location = new System.Drawing.Point(72, 304);
            this._d32Label.Name = "_d32Label";
            this._d32Label.Size = new System.Drawing.Size(28, 13);
            this._d32Label.TabIndex = 14;
            this._d32Label.Text = "Д32";
            // 
            // _d20Label
            // 
            this._d20Label.AutoSize = true;
            this._d20Label.Location = new System.Drawing.Point(72, 76);
            this._d20Label.Name = "_d20Label";
            this._d20Label.Size = new System.Drawing.Size(28, 13);
            this._d20Label.TabIndex = 22;
            this._d20Label.Text = "Д20";
            // 
            // _d31
            // 
            this._d31.Location = new System.Drawing.Point(53, 285);
            this._d31.Name = "_d31";
            this._d31.Size = new System.Drawing.Size(13, 13);
            this._d31.State = BEMN.Forms.LedState.Off;
            this._d31.TabIndex = 13;
            // 
            // _d6Label
            // 
            this._d6Label.AutoSize = true;
            this._d6Label.Location = new System.Drawing.Point(25, 114);
            this._d6Label.Name = "_d6Label";
            this._d6Label.Size = new System.Drawing.Size(22, 13);
            this._d6Label.TabIndex = 10;
            this._d6Label.Text = "Д6";
            // 
            // _d31Label
            // 
            this._d31Label.AutoSize = true;
            this._d31Label.Location = new System.Drawing.Point(72, 285);
            this._d31Label.Name = "_d31Label";
            this._d31Label.Size = new System.Drawing.Size(28, 13);
            this._d31Label.TabIndex = 12;
            this._d31Label.Text = "Д31";
            // 
            // _d19
            // 
            this._d19.Location = new System.Drawing.Point(53, 57);
            this._d19.Name = "_d19";
            this._d19.Size = new System.Drawing.Size(13, 13);
            this._d19.State = BEMN.Forms.LedState.Off;
            this._d19.TabIndex = 21;
            // 
            // _d30
            // 
            this._d30.Location = new System.Drawing.Point(53, 266);
            this._d30.Name = "_d30";
            this._d30.Size = new System.Drawing.Size(13, 13);
            this._d30.State = BEMN.Forms.LedState.Off;
            this._d30.TabIndex = 11;
            // 
            // _d2Label
            // 
            this._d2Label.AutoSize = true;
            this._d2Label.Location = new System.Drawing.Point(25, 38);
            this._d2Label.Name = "_d2Label";
            this._d2Label.Size = new System.Drawing.Size(22, 13);
            this._d2Label.TabIndex = 2;
            this._d2Label.Text = "Д2";
            // 
            // _d30Label
            // 
            this._d30Label.AutoSize = true;
            this._d30Label.Location = new System.Drawing.Point(72, 266);
            this._d30Label.Name = "_d30Label";
            this._d30Label.Size = new System.Drawing.Size(28, 13);
            this._d30Label.TabIndex = 10;
            this._d30Label.Text = "Д30";
            // 
            // _d19Label
            // 
            this._d19Label.AutoSize = true;
            this._d19Label.Location = new System.Drawing.Point(72, 57);
            this._d19Label.Name = "_d19Label";
            this._d19Label.Size = new System.Drawing.Size(28, 13);
            this._d19Label.TabIndex = 20;
            this._d19Label.Text = "Д19";
            // 
            // _d29
            // 
            this._d29.Location = new System.Drawing.Point(53, 247);
            this._d29.Name = "_d29";
            this._d29.Size = new System.Drawing.Size(13, 13);
            this._d29.State = BEMN.Forms.LedState.Off;
            this._d29.TabIndex = 9;
            // 
            // _d5
            // 
            this._d5.Location = new System.Drawing.Point(6, 95);
            this._d5.Name = "_d5";
            this._d5.Size = new System.Drawing.Size(13, 13);
            this._d5.State = BEMN.Forms.LedState.Off;
            this._d5.TabIndex = 9;
            // 
            // _d29Label
            // 
            this._d29Label.AutoSize = true;
            this._d29Label.Location = new System.Drawing.Point(72, 247);
            this._d29Label.Name = "_d29Label";
            this._d29Label.Size = new System.Drawing.Size(28, 13);
            this._d29Label.TabIndex = 8;
            this._d29Label.Text = "Д29";
            // 
            // _d18
            // 
            this._d18.Location = new System.Drawing.Point(53, 38);
            this._d18.Name = "_d18";
            this._d18.Size = new System.Drawing.Size(13, 13);
            this._d18.State = BEMN.Forms.LedState.Off;
            this._d18.TabIndex = 19;
            // 
            // _d28
            // 
            this._d28.Location = new System.Drawing.Point(53, 228);
            this._d28.Name = "_d28";
            this._d28.Size = new System.Drawing.Size(13, 13);
            this._d28.State = BEMN.Forms.LedState.Off;
            this._d28.TabIndex = 7;
            // 
            // _d2
            // 
            this._d2.Location = new System.Drawing.Point(6, 38);
            this._d2.Name = "_d2";
            this._d2.Size = new System.Drawing.Size(13, 13);
            this._d2.State = BEMN.Forms.LedState.Off;
            this._d2.TabIndex = 3;
            // 
            // _d28Label
            // 
            this._d28Label.AutoSize = true;
            this._d28Label.Location = new System.Drawing.Point(72, 228);
            this._d28Label.Name = "_d28Label";
            this._d28Label.Size = new System.Drawing.Size(28, 13);
            this._d28Label.TabIndex = 6;
            this._d28Label.Text = "Д28";
            // 
            // _d18Label
            // 
            this._d18Label.AutoSize = true;
            this._d18Label.Location = new System.Drawing.Point(72, 38);
            this._d18Label.Name = "_d18Label";
            this._d18Label.Size = new System.Drawing.Size(28, 13);
            this._d18Label.TabIndex = 18;
            this._d18Label.Text = "Д18";
            // 
            // _d27
            // 
            this._d27.Location = new System.Drawing.Point(53, 209);
            this._d27.Name = "_d27";
            this._d27.Size = new System.Drawing.Size(13, 13);
            this._d27.State = BEMN.Forms.LedState.Off;
            this._d27.TabIndex = 5;
            // 
            // _d5Label
            // 
            this._d5Label.AutoSize = true;
            this._d5Label.Location = new System.Drawing.Point(25, 95);
            this._d5Label.Name = "_d5Label";
            this._d5Label.Size = new System.Drawing.Size(22, 13);
            this._d5Label.TabIndex = 8;
            this._d5Label.Text = "Д5";
            // 
            // _d27Label
            // 
            this._d27Label.AutoSize = true;
            this._d27Label.Location = new System.Drawing.Point(72, 209);
            this._d27Label.Name = "_d27Label";
            this._d27Label.Size = new System.Drawing.Size(28, 13);
            this._d27Label.TabIndex = 4;
            this._d27Label.Text = "Д27";
            // 
            // _d17
            // 
            this._d17.Location = new System.Drawing.Point(53, 19);
            this._d17.Name = "_d17";
            this._d17.Size = new System.Drawing.Size(13, 13);
            this._d17.State = BEMN.Forms.LedState.Off;
            this._d17.TabIndex = 17;
            // 
            // _d26
            // 
            this._d26.Location = new System.Drawing.Point(53, 190);
            this._d26.Name = "_d26";
            this._d26.Size = new System.Drawing.Size(13, 13);
            this._d26.State = BEMN.Forms.LedState.Off;
            this._d26.TabIndex = 3;
            // 
            // _d17Label
            // 
            this._d17Label.AutoSize = true;
            this._d17Label.Location = new System.Drawing.Point(72, 19);
            this._d17Label.Name = "_d17Label";
            this._d17Label.Size = new System.Drawing.Size(28, 13);
            this._d17Label.TabIndex = 16;
            this._d17Label.Text = "Д17";
            // 
            // _d26Label
            // 
            this._d26Label.AutoSize = true;
            this._d26Label.Location = new System.Drawing.Point(72, 190);
            this._d26Label.Name = "_d26Label";
            this._d26Label.Size = new System.Drawing.Size(28, 13);
            this._d26Label.TabIndex = 2;
            this._d26Label.Text = "Д26";
            // 
            // _d3Label
            // 
            this._d3Label.AutoSize = true;
            this._d3Label.Location = new System.Drawing.Point(25, 57);
            this._d3Label.Name = "_d3Label";
            this._d3Label.Size = new System.Drawing.Size(22, 13);
            this._d3Label.TabIndex = 4;
            this._d3Label.Text = "Д3";
            // 
            // _d25
            // 
            this._d25.Location = new System.Drawing.Point(53, 171);
            this._d25.Name = "_d25";
            this._d25.Size = new System.Drawing.Size(13, 13);
            this._d25.State = BEMN.Forms.LedState.Off;
            this._d25.TabIndex = 1;
            // 
            // _d25Label
            // 
            this._d25Label.AutoSize = true;
            this._d25Label.Location = new System.Drawing.Point(72, 171);
            this._d25Label.Name = "_d25Label";
            this._d25Label.Size = new System.Drawing.Size(28, 13);
            this._d25Label.TabIndex = 0;
            this._d25Label.Text = "Д25";
            // 
            // _d4
            // 
            this._d4.Location = new System.Drawing.Point(6, 76);
            this._d4.Name = "_d4";
            this._d4.Size = new System.Drawing.Size(13, 13);
            this._d4.State = BEMN.Forms.LedState.Off;
            this._d4.TabIndex = 7;
            // 
            // _d16
            // 
            this._d16.Location = new System.Drawing.Point(6, 304);
            this._d16.Name = "_d16";
            this._d16.Size = new System.Drawing.Size(13, 13);
            this._d16.State = BEMN.Forms.LedState.Off;
            this._d16.TabIndex = 15;
            // 
            // _d3
            // 
            this._d3.Location = new System.Drawing.Point(6, 57);
            this._d3.Name = "_d3";
            this._d3.Size = new System.Drawing.Size(13, 13);
            this._d3.State = BEMN.Forms.LedState.Off;
            this._d3.TabIndex = 5;
            // 
            // _d16Label
            // 
            this._d16Label.AutoSize = true;
            this._d16Label.Location = new System.Drawing.Point(25, 304);
            this._d16Label.Name = "_d16Label";
            this._d16Label.Size = new System.Drawing.Size(28, 13);
            this._d16Label.TabIndex = 14;
            this._d16Label.Text = "Д16";
            // 
            // _d4Label
            // 
            this._d4Label.AutoSize = true;
            this._d4Label.Location = new System.Drawing.Point(25, 76);
            this._d4Label.Name = "_d4Label";
            this._d4Label.Size = new System.Drawing.Size(22, 13);
            this._d4Label.TabIndex = 6;
            this._d4Label.Text = "Д4";
            // 
            // _d15
            // 
            this._d15.Location = new System.Drawing.Point(6, 285);
            this._d15.Name = "_d15";
            this._d15.Size = new System.Drawing.Size(13, 13);
            this._d15.State = BEMN.Forms.LedState.Off;
            this._d15.TabIndex = 13;
            // 
            // _d9
            // 
            this._d9.Location = new System.Drawing.Point(6, 171);
            this._d9.Name = "_d9";
            this._d9.Size = new System.Drawing.Size(13, 13);
            this._d9.State = BEMN.Forms.LedState.Off;
            this._d9.TabIndex = 1;
            // 
            // _d15Label
            // 
            this._d15Label.AutoSize = true;
            this._d15Label.Location = new System.Drawing.Point(25, 285);
            this._d15Label.Name = "_d15Label";
            this._d15Label.Size = new System.Drawing.Size(28, 13);
            this._d15Label.TabIndex = 12;
            this._d15Label.Text = "Д15";
            // 
            // _d9Label
            // 
            this._d9Label.AutoSize = true;
            this._d9Label.Location = new System.Drawing.Point(25, 171);
            this._d9Label.Name = "_d9Label";
            this._d9Label.Size = new System.Drawing.Size(22, 13);
            this._d9Label.TabIndex = 0;
            this._d9Label.Text = "Д9";
            // 
            // _d14
            // 
            this._d14.Location = new System.Drawing.Point(6, 266);
            this._d14.Name = "_d14";
            this._d14.Size = new System.Drawing.Size(13, 13);
            this._d14.State = BEMN.Forms.LedState.Off;
            this._d14.TabIndex = 11;
            // 
            // _d10Label
            // 
            this._d10Label.AutoSize = true;
            this._d10Label.Location = new System.Drawing.Point(25, 190);
            this._d10Label.Name = "_d10Label";
            this._d10Label.Size = new System.Drawing.Size(28, 13);
            this._d10Label.TabIndex = 2;
            this._d10Label.Text = "Д10";
            // 
            // _d14Label
            // 
            this._d14Label.AutoSize = true;
            this._d14Label.Location = new System.Drawing.Point(25, 266);
            this._d14Label.Name = "_d14Label";
            this._d14Label.Size = new System.Drawing.Size(28, 13);
            this._d14Label.TabIndex = 10;
            this._d14Label.Text = "Д14";
            // 
            // _d10
            // 
            this._d10.Location = new System.Drawing.Point(6, 190);
            this._d10.Name = "_d10";
            this._d10.Size = new System.Drawing.Size(13, 13);
            this._d10.State = BEMN.Forms.LedState.Off;
            this._d10.TabIndex = 3;
            // 
            // _d13
            // 
            this._d13.Location = new System.Drawing.Point(6, 247);
            this._d13.Name = "_d13";
            this._d13.Size = new System.Drawing.Size(13, 13);
            this._d13.State = BEMN.Forms.LedState.Off;
            this._d13.TabIndex = 9;
            // 
            // _d11Label
            // 
            this._d11Label.AutoSize = true;
            this._d11Label.Location = new System.Drawing.Point(25, 209);
            this._d11Label.Name = "_d11Label";
            this._d11Label.Size = new System.Drawing.Size(28, 13);
            this._d11Label.TabIndex = 4;
            this._d11Label.Text = "Д11";
            // 
            // _d13Label
            // 
            this._d13Label.AutoSize = true;
            this._d13Label.Location = new System.Drawing.Point(25, 247);
            this._d13Label.Name = "_d13Label";
            this._d13Label.Size = new System.Drawing.Size(28, 13);
            this._d13Label.TabIndex = 8;
            this._d13Label.Text = "Д13";
            // 
            // _d11
            // 
            this._d11.Location = new System.Drawing.Point(6, 209);
            this._d11.Name = "_d11";
            this._d11.Size = new System.Drawing.Size(13, 13);
            this._d11.State = BEMN.Forms.LedState.Off;
            this._d11.TabIndex = 5;
            // 
            // _d12
            // 
            this._d12.Location = new System.Drawing.Point(6, 228);
            this._d12.Name = "_d12";
            this._d12.Size = new System.Drawing.Size(13, 13);
            this._d12.State = BEMN.Forms.LedState.Off;
            this._d12.TabIndex = 7;
            // 
            // _d12Label
            // 
            this._d12Label.AutoSize = true;
            this._d12Label.Location = new System.Drawing.Point(25, 228);
            this._d12Label.Name = "_d12Label";
            this._d12Label.Size = new System.Drawing.Size(28, 13);
            this._d12Label.TabIndex = 6;
            this._d12Label.Text = "Д12";
            // 
            // _faultModule6
            // 
            this._faultModule6.Location = new System.Drawing.Point(117, 152);
            this._faultModule6.Name = "_faultModule6";
            this._faultModule6.Size = new System.Drawing.Size(13, 13);
            this._faultModule6.State = BEMN.Forms.LedState.Off;
            this._faultModule6.TabIndex = 37;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(136, 152);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(54, 13);
            this.label18.TabIndex = 36;
            this.label18.Text = "Модуля 6";
            // 
            // Mr761ObrMeasuringFormNew
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(941, 625);
            this.Controls.Add(this._dataBaseTabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Mr761ObrMeasuringFormNew";
            this.Text = "Mr761OBRMeasuringForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MeasuringForm_FormClosing);
            this.Load += new System.EventHandler(this.MeasuringForm_Load);
            this._controlSignalsTabPage.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox28.ResumeLayout(false);
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.groupBox15.ResumeLayout(false);
            this.groupBox27.ResumeLayout(false);
            this.groupBox27.PerformLayout();
            this.groupBox40.ResumeLayout(false);
            this.groupBox40.PerformLayout();
            this._discretTabPage1.ResumeLayout(false);
            this.groupBox49.ResumeLayout(false);
            this.groupBox49.PerformLayout();
            this.groupBox38.ResumeLayout(false);
            this.groupBox38.PerformLayout();
            this.groupBox39.ResumeLayout(false);
            this.groupBox39.PerformLayout();
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.groupBox21.ResumeLayout(false);
            this.groupBox21.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this._dataBaseTabControl.ResumeLayout(false);
            this._diskretAndReleTabPage.ResumeLayout(false);
            this._diskretAndReleTabPage.PerformLayout();
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this._rsTriggersGB.ResumeLayout(false);
            this._rsTriggersGB.PerformLayout();
            this._discretsGB.ResumeLayout(false);
            this._discretsGB.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage _controlSignalsTabPage;
        private System.Windows.Forms.GroupBox groupBox28;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.Label _currenGroupLabel;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.ComboBox _groupCombo;
        private System.Windows.Forms.Button _switchGroupButton;
        private System.Windows.Forms.GroupBox groupBox27;
        private System.Windows.Forms.GroupBox groupBox40;
        private Forms.LedControl _logicState;
        private System.Windows.Forms.Button stopLogic;
        private System.Windows.Forms.Button startLogic;
        private System.Windows.Forms.Label label314;
        private System.Windows.Forms.Button _breakerOffBut;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Button _breakerOnBut;
        private System.Windows.Forms.Label label101;
        private Forms.LedControl _switchOn;
        private Forms.LedControl _switchOff;
        private System.Windows.Forms.Button _startOscBtn;
        private Forms.LedControl _availabilityFaultSystemJournal;
        private Forms.LedControl _newRecordOscJournal;
        private Forms.LedControl _newRecordAlarmJournal;
        private Forms.LedControl _newRecordSystemJournal;
        private System.Windows.Forms.Button _resetAnButton;
        private System.Windows.Forms.Button _resetAvailabilityFaultSystemJournalButton;
        private System.Windows.Forms.Button _resetOscJournalButton;
        private System.Windows.Forms.Button _resetAlarmJournalButton;
        private System.Windows.Forms.Button _resetSystemJournalButton;
        private System.Windows.Forms.Label label227;
        private System.Windows.Forms.Label label225;
        private System.Windows.Forms.Label label226;
        private System.Windows.Forms.Label label231;
        private System.Windows.Forms.TabPage _discretTabPage1;
        private System.Windows.Forms.GroupBox groupBox14;
        private Forms.LedControl _fault;
        private System.Windows.Forms.Label label210;
        private Forms.LedControl _faultOff;
        private System.Windows.Forms.Label label21;
        private Forms.LedControl _acceleration;
        private System.Windows.Forms.Label _auto4Led;
        private Forms.LedControl _signaling;
        private System.Windows.Forms.Label _auto2Led;
        private System.Windows.Forms.GroupBox groupBox21;
        private Forms.LedControl _ssl32;
        private System.Windows.Forms.Label label237;
        private Forms.LedControl _ssl31;
        private System.Windows.Forms.Label label238;
        private Forms.LedControl _ssl30;
        private System.Windows.Forms.Label label239;
        private Forms.LedControl _ssl29;
        private System.Windows.Forms.Label label240;
        private Forms.LedControl _ssl28;
        private System.Windows.Forms.Label label241;
        private Forms.LedControl _ssl27;
        private System.Windows.Forms.Label label242;
        private Forms.LedControl _ssl26;
        private System.Windows.Forms.Label label243;
        private Forms.LedControl _ssl25;
        private System.Windows.Forms.Label label244;
        private Forms.LedControl _ssl24;
        private System.Windows.Forms.Label label245;
        private Forms.LedControl _ssl23;
        private System.Windows.Forms.Label label246;
        private Forms.LedControl _ssl22;
        private System.Windows.Forms.Label label247;
        private Forms.LedControl _ssl21;
        private System.Windows.Forms.Label label248;
        private Forms.LedControl _ssl20;
        private System.Windows.Forms.Label label249;
        private Forms.LedControl _ssl19;
        private System.Windows.Forms.Label label250;
        private Forms.LedControl _ssl18;
        private System.Windows.Forms.Label label251;
        private Forms.LedControl _ssl17;
        private System.Windows.Forms.Label label252;
        private Forms.LedControl _ssl16;
        private System.Windows.Forms.Label label253;
        private Forms.LedControl _ssl15;
        private System.Windows.Forms.Label label254;
        private Forms.LedControl _ssl14;
        private System.Windows.Forms.Label label255;
        private Forms.LedControl _ssl13;
        private System.Windows.Forms.Label label256;
        private Forms.LedControl _ssl12;
        private System.Windows.Forms.Label label257;
        private Forms.LedControl _ssl11;
        private System.Windows.Forms.Label label258;
        private Forms.LedControl _ssl10;
        private System.Windows.Forms.Label label259;
        private Forms.LedControl _ssl9;
        private System.Windows.Forms.Label label260;
        private Forms.LedControl _ssl8;
        private System.Windows.Forms.Label label261;
        private Forms.LedControl _ssl7;
        private System.Windows.Forms.Label label262;
        private Forms.LedControl _ssl6;
        private System.Windows.Forms.Label label263;
        private Forms.LedControl _ssl5;
        private System.Windows.Forms.Label label264;
        private Forms.LedControl _ssl4;
        private System.Windows.Forms.Label label265;
        private Forms.LedControl _ssl3;
        private System.Windows.Forms.Label label266;
        private Forms.LedControl _ssl2;
        private System.Windows.Forms.Label label267;
        private Forms.LedControl _ssl1;
        private System.Windows.Forms.Label label268;
        private System.Windows.Forms.GroupBox groupBox10;
        private Forms.LedControl _vls16;
        private System.Windows.Forms.Label label63;
        private Forms.LedControl _vls15;
        private System.Windows.Forms.Label label64;
        private Forms.LedControl _vls14;
        private System.Windows.Forms.Label label65;
        private Forms.LedControl _vls13;
        private System.Windows.Forms.Label label66;
        private Forms.LedControl _vls12;
        private System.Windows.Forms.Label label67;
        private Forms.LedControl _vls11;
        private System.Windows.Forms.Label label68;
        private Forms.LedControl _vls10;
        private System.Windows.Forms.Label label69;
        private Forms.LedControl _vls9;
        private System.Windows.Forms.Label label70;
        private Forms.LedControl _vls8;
        private System.Windows.Forms.Label label71;
        private Forms.LedControl _vls7;
        private System.Windows.Forms.Label label72;
        private Forms.LedControl _vls6;
        private System.Windows.Forms.Label label73;
        private Forms.LedControl _vls5;
        private System.Windows.Forms.Label label74;
        private Forms.LedControl _vls4;
        private System.Windows.Forms.Label label75;
        private Forms.LedControl _vls3;
        private System.Windows.Forms.Label label76;
        private Forms.LedControl _vls2;
        private System.Windows.Forms.Label label77;
        private Forms.LedControl _vls1;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.GroupBox groupBox9;
        private Forms.LedControl _ls16;
        private System.Windows.Forms.Label label47;
        private Forms.LedControl _ls15;
        private System.Windows.Forms.Label label48;
        private Forms.LedControl _ls14;
        private System.Windows.Forms.Label label49;
        private Forms.LedControl _ls13;
        private System.Windows.Forms.Label label50;
        private Forms.LedControl _ls12;
        private System.Windows.Forms.Label label51;
        private Forms.LedControl _ls11;
        private System.Windows.Forms.Label label52;
        private Forms.LedControl _ls10;
        private System.Windows.Forms.Label label53;
        private Forms.LedControl _ls9;
        private System.Windows.Forms.Label label54;
        private Forms.LedControl _ls8;
        private System.Windows.Forms.Label label55;
        private Forms.LedControl _ls7;
        private System.Windows.Forms.Label label56;
        private Forms.LedControl _ls6;
        private System.Windows.Forms.Label label57;
        private Forms.LedControl _ls5;
        private System.Windows.Forms.Label label58;
        private Forms.LedControl _ls4;
        private System.Windows.Forms.Label label59;
        private Forms.LedControl _ls3;
        private System.Windows.Forms.Label label60;
        private Forms.LedControl _ls2;
        private System.Windows.Forms.Label label61;
        private Forms.LedControl _ls1;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.TabControl _dataBaseTabControl;
        private Forms.DateTimeControl _dateTimeControl;
        private System.Windows.Forms.GroupBox groupBox49;
        private System.Windows.Forms.Label label347;
        private Forms.LedControl _faultDisable2;
        private System.Windows.Forms.Label label346;
        private System.Windows.Forms.Label label345;
        private System.Windows.Forms.Label label311;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label22;
        private Forms.LedControl _faultDisable1;
        private Forms.LedControl _faultSwithON;
        private Forms.LedControl _faultManage;
        private Forms.LedControl _faultBlockCon;
        private Forms.LedControl _faultOut;
        private System.Windows.Forms.GroupBox groupBox38;
        private System.Windows.Forms.GroupBox groupBox39;
        private Forms.LedControl _fSpl1;
        private System.Windows.Forms.Label label315;
        private System.Windows.Forms.Label label316;
        private Forms.LedControl _fSpl2;
        private System.Windows.Forms.Label label317;
        private Forms.LedControl _fSpl3;
        private Forms.LedControl _fSpl5;
        private System.Windows.Forms.Label label320;
        private System.Windows.Forms.Label label319;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.Label label344;
        private Forms.LedControl _faultSwitchOff;
        private Forms.LedControl _faultAlarmJournal;
        private System.Windows.Forms.Label label192;
        private Forms.LedControl _faultOsc;
        private System.Windows.Forms.Label label193;
        private Forms.LedControl _faultModule5;
        private Forms.LedControl _faultSystemJournal;
        private System.Windows.Forms.Label label200;
        private System.Windows.Forms.Label label194;
        private Forms.LedControl _faultGroupsOfSetpoints;
        private System.Windows.Forms.Label label201;
        private Forms.LedControl _faultModule4;
        private Forms.LedControl _faultLogic;
        private System.Windows.Forms.Label label79;
        private Forms.LedControl _faultSetpoints;
        private System.Windows.Forms.Label label202;
        private System.Windows.Forms.Label label195;
        private Forms.LedControl _faultPass;
        private System.Windows.Forms.Label label203;
        private Forms.LedControl _faultModule3;
        private System.Windows.Forms.Label label196;
        private Forms.LedControl _faultSoftware;
        private System.Windows.Forms.Label label205;
        private Forms.LedControl _faultModule2;
        private Forms.LedControl _faultHardware;
        private System.Windows.Forms.Label label206;
        private System.Windows.Forms.Label label197;
        private Forms.LedControl _faultModule1;
        private System.Windows.Forms.Label label198;
        private System.Windows.Forms.GroupBox groupBox12;
        private Forms.LedControl _vz16;
        private System.Windows.Forms.Label label111;
        private Forms.LedControl _vz15;
        private System.Windows.Forms.Label label112;
        private Forms.LedControl _vz14;
        private System.Windows.Forms.Label label113;
        private Forms.LedControl _vz13;
        private System.Windows.Forms.Label label114;
        private Forms.LedControl _vz12;
        private System.Windows.Forms.Label label115;
        private Forms.LedControl _vz11;
        private System.Windows.Forms.Label label116;
        private Forms.LedControl _vz10;
        private System.Windows.Forms.Label label117;
        private Forms.LedControl _vz9;
        private System.Windows.Forms.Label label118;
        private Forms.LedControl _vz8;
        private System.Windows.Forms.Label label119;
        private Forms.LedControl _vz7;
        private System.Windows.Forms.Label label120;
        private Forms.LedControl _vz6;
        private System.Windows.Forms.Label label121;
        private Forms.LedControl _vz5;
        private System.Windows.Forms.Label label122;
        private Forms.LedControl _vz4;
        private System.Windows.Forms.Label label123;
        private Forms.LedControl _vz3;
        private System.Windows.Forms.Label label124;
        private Forms.LedControl _vz2;
        private System.Windows.Forms.Label label125;
        private Forms.LedControl _vz1;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button _commandBtn;
        private System.Windows.Forms.ComboBox _commandComboBox;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button _reserveGroupButton;
        private System.Windows.Forms.Button _mainGroupButton;
        private System.Windows.Forms.Label label490;
        private System.Windows.Forms.Label label491;
        private Forms.LedControl _reservedGroup;
        private Forms.LedControl _mainGroup;
        private System.Windows.Forms.TabPage _diskretAndReleTabPage;
        private System.Windows.Forms.GroupBox _discretsGB;
        private Forms.LedControl _k2;
        private System.Windows.Forms.Label _k2Label;
        private Forms.LedControl _k1;
        private System.Windows.Forms.Label _k1Label;
        private Forms.LedControl _d72;
        private System.Windows.Forms.Label _d72Label;
        private Forms.LedControl _d71;
        private System.Windows.Forms.Label _d71Label;
        private Forms.LedControl _d70;
        private System.Windows.Forms.Label _d70Label;
        private Forms.LedControl _d69;
        private System.Windows.Forms.Label _d69Label;
        private Forms.LedControl _d68;
        private System.Windows.Forms.Label _d68Label;
        private Forms.LedControl _d67;
        private System.Windows.Forms.Label _d67Label;
        private Forms.LedControl _d66;
        private System.Windows.Forms.Label _d66Label;
        private Forms.LedControl _d65;
        private System.Windows.Forms.Label _d65Label;
        private Forms.LedControl _d64;
        private System.Windows.Forms.Label _d64Label;
        private Forms.LedControl _d63;
        private System.Windows.Forms.Label _d63Label;
        private Forms.LedControl _d62;
        private System.Windows.Forms.Label _d62Label;
        private Forms.LedControl _d61;
        private System.Windows.Forms.Label _d61Label;
        private Forms.LedControl _d60;
        private System.Windows.Forms.Label _d60Label;
        private Forms.LedControl _d59;
        private System.Windows.Forms.Label _d59Label;
        private Forms.LedControl _d58;
        private System.Windows.Forms.Label _d58Label;
        private Forms.LedControl _d57;
        private System.Windows.Forms.Label _d57Label;
        private Forms.LedControl _d56;
        private System.Windows.Forms.Label _d56Label;
        private Forms.LedControl _d55;
        private System.Windows.Forms.Label _d55Label;
        private Forms.LedControl _d54;
        private System.Windows.Forms.Label _d54Label;
        private Forms.LedControl _d53;
        private System.Windows.Forms.Label _d53Label;
        private Forms.LedControl _d52;
        private System.Windows.Forms.Label _d52Label;
        private Forms.LedControl _d51;
        private System.Windows.Forms.Label _d51Label;
        private Forms.LedControl _d50;
        private System.Windows.Forms.Label _d50Label;
        private Forms.LedControl _d49;
        private System.Windows.Forms.Label _d49Label;
        private Forms.LedControl _d48;
        private System.Windows.Forms.Label _d48Label;
        private Forms.LedControl _d47;
        private System.Windows.Forms.Label _d47Label;
        private Forms.LedControl _d46;
        private System.Windows.Forms.Label _d46Label;
        private Forms.LedControl _d45;
        private System.Windows.Forms.Label _d45Label;
        private Forms.LedControl _d44;
        private System.Windows.Forms.Label _d44Label;
        private Forms.LedControl _d43;
        private System.Windows.Forms.Label _d43Label;
        private Forms.LedControl _d42;
        private System.Windows.Forms.Label _d42Label;
        private Forms.LedControl _d41;
        private System.Windows.Forms.Label _d41Label;
        private Forms.LedControl _d40;
        private Forms.LedControl _d24;
        private System.Windows.Forms.Label _d40Label;
        private Forms.LedControl _d8;
        private Forms.LedControl _d39;
        private System.Windows.Forms.Label _d24Label;
        private System.Windows.Forms.Label _d39Label;
        private Forms.LedControl _d38;
        private Forms.LedControl _d23;
        private System.Windows.Forms.Label _d38Label;
        private System.Windows.Forms.Label _d8Label;
        private Forms.LedControl _d37;
        private System.Windows.Forms.Label _d23Label;
        private System.Windows.Forms.Label _d37Label;
        private Forms.LedControl _d22;
        private Forms.LedControl _d36;
        private Forms.LedControl _d7;
        private System.Windows.Forms.Label _d36Label;
        private System.Windows.Forms.Label _d22Label;
        private Forms.LedControl _d35;
        private System.Windows.Forms.Label _d7Label;
        private System.Windows.Forms.Label _d35Label;
        private Forms.LedControl _d21;
        private Forms.LedControl _d34;
        private Forms.LedControl _d1;
        private System.Windows.Forms.Label _d34Label;
        private System.Windows.Forms.Label _d21Label;
        private Forms.LedControl _d33;
        private System.Windows.Forms.Label _d33Label;
        private Forms.LedControl _d6;
        private Forms.LedControl _d20;
        private Forms.LedControl _d32;
        private System.Windows.Forms.Label _d1Label;
        private System.Windows.Forms.Label _d32Label;
        private System.Windows.Forms.Label _d20Label;
        private Forms.LedControl _d31;
        private System.Windows.Forms.Label _d6Label;
        private System.Windows.Forms.Label _d31Label;
        private Forms.LedControl _d19;
        private Forms.LedControl _d30;
        private System.Windows.Forms.Label _d2Label;
        private System.Windows.Forms.Label _d30Label;
        private System.Windows.Forms.Label _d19Label;
        private Forms.LedControl _d29;
        private Forms.LedControl _d5;
        private System.Windows.Forms.Label _d29Label;
        private Forms.LedControl _d18;
        private Forms.LedControl _d28;
        private Forms.LedControl _d2;
        private System.Windows.Forms.Label _d28Label;
        private System.Windows.Forms.Label _d18Label;
        private Forms.LedControl _d27;
        private System.Windows.Forms.Label _d5Label;
        private System.Windows.Forms.Label _d27Label;
        private Forms.LedControl _d17;
        private Forms.LedControl _d26;
        private System.Windows.Forms.Label _d17Label;
        private System.Windows.Forms.Label _d26Label;
        private System.Windows.Forms.Label _d3Label;
        private Forms.LedControl _d25;
        private System.Windows.Forms.Label _d25Label;
        private Forms.LedControl _d4;
        private Forms.LedControl _d16;
        private Forms.LedControl _d3;
        private System.Windows.Forms.Label _d16Label;
        private System.Windows.Forms.Label _d4Label;
        private Forms.LedControl _d15;
        private Forms.LedControl _d9;
        private System.Windows.Forms.Label _d15Label;
        private System.Windows.Forms.Label _d9Label;
        private Forms.LedControl _d14;
        private System.Windows.Forms.Label _d10Label;
        private System.Windows.Forms.Label _d14Label;
        private Forms.LedControl _d10;
        private Forms.LedControl _d13;
        private System.Windows.Forms.Label _d11Label;
        private System.Windows.Forms.Label _d13Label;
        private Forms.LedControl _d11;
        private Forms.LedControl _d12;
        private System.Windows.Forms.Label _d12Label;
        private System.Windows.Forms.GroupBox _rsTriggersGB;
        private Forms.LedControl _rst8;
        private System.Windows.Forms.Label label12;
        private Forms.LedControl _rst7;
        private System.Windows.Forms.Label label96;
        private Forms.LedControl _rst1;
        private Forms.LedControl _rst6;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label label103;
        private Forms.LedControl _rst5;
        private Forms.LedControl _rst2;
        private System.Windows.Forms.Label label150;
        private System.Windows.Forms.Label label104;
        private Forms.LedControl _rst4;
        private Forms.LedControl _rst16;
        private Forms.LedControl _rst3;
        private System.Windows.Forms.Label label207;
        private System.Windows.Forms.Label label208;
        private Forms.LedControl _rst15;
        private Forms.LedControl _rst9;
        private System.Windows.Forms.Label label209;
        private System.Windows.Forms.Label label211;
        private Forms.LedControl _rst14;
        private System.Windows.Forms.Label label214;
        private System.Windows.Forms.Label label215;
        private Forms.LedControl _rst10;
        private Forms.LedControl _rst13;
        private System.Windows.Forms.Label label216;
        private System.Windows.Forms.Label label217;
        private Forms.LedControl _rst11;
        private Forms.LedControl _rst12;
        private System.Windows.Forms.Label label219;
        private System.Windows.Forms.Label _r56Label;
        private System.Windows.Forms.Label _r58Label;
        private System.Windows.Forms.Label _r70Label;
        private System.Windows.Forms.Label _r18Label;
        private System.Windows.Forms.Label _r59Label;
        private System.Windows.Forms.Label _r71Label;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Label _r64Label;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label _r76Label;
        private System.Windows.Forms.Label _r40Label;
        private System.Windows.Forms.Label _r69Label;
        private System.Windows.Forms.Label _r55Label;
        private System.Windows.Forms.Label _r51Label;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label _r60Label;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.Label _r72Label;
        private System.Windows.Forms.Label _r57Label;
        private System.Windows.Forms.Label _r65Label;
        private System.Windows.Forms.Label _r80Label;
        private System.Windows.Forms.Label _r77Label;
        private System.Windows.Forms.Label _r68Label;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.Label _r39Label;
        private System.Windows.Forms.Label _r75Label;
        private System.Windows.Forms.Label _r54Label;
        private System.Windows.Forms.Label _r61Label;
        private System.Windows.Forms.Label _r63Label;
        private System.Windows.Forms.Label _r73Label;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.Label _r66Label;
        private System.Windows.Forms.Label _r78Label;
        private System.Windows.Forms.Label _r46Label;
        private System.Windows.Forms.Label label135;
        private System.Windows.Forms.Label label136;
        private System.Windows.Forms.Label _r53Label;
        private System.Windows.Forms.Label label137;
        private System.Windows.Forms.Label _r62Label;
        private System.Windows.Forms.Label _r74Label;
        private System.Windows.Forms.Label _r67Label;
        private System.Windows.Forms.Label _r34Label;
        private System.Windows.Forms.Label _r79Label;
        private System.Windows.Forms.Label _r52Label;
        private System.Windows.Forms.Label label143;
        private System.Windows.Forms.Label label144;
        private System.Windows.Forms.Label label145;
        private System.Windows.Forms.Label label146;
        private System.Windows.Forms.Label label147;
        private System.Windows.Forms.Label _r50Label;
        private System.Windows.Forms.Label label148;
        private System.Windows.Forms.Label _r38Label;
        private System.Windows.Forms.Label label149;
        private System.Windows.Forms.Label label151;
        private System.Windows.Forms.Label _r45Label;
        private System.Windows.Forms.Label _r30Label;
        private System.Windows.Forms.Label _r29Label;
        private System.Windows.Forms.Label _r28Label;
        private System.Windows.Forms.Label _r27Label;
        private System.Windows.Forms.Label _r33Label;
        private System.Windows.Forms.Label _r26Label;
        private System.Windows.Forms.Label _r25Label;
        private System.Windows.Forms.Label _r24Label;
        private System.Windows.Forms.Label _r23Label;
        private System.Windows.Forms.Label _r31Label;
        private System.Windows.Forms.Label _r49Label;
        private System.Windows.Forms.Label _r22Label;
        private System.Windows.Forms.Label _r21Label;
        private System.Windows.Forms.Label _r37Label;
        private System.Windows.Forms.Label _r20Label;
        private System.Windows.Forms.Label _r44Label;
        private System.Windows.Forms.Label _r19Label;
        private System.Windows.Forms.Label _r32Label;
        private System.Windows.Forms.Label _r42Label;
        private System.Windows.Forms.Label _r48Label;
        private System.Windows.Forms.Label _r41Label;
        private System.Windows.Forms.Label _r36Label;
        private System.Windows.Forms.Label _r35Label;
        private System.Windows.Forms.Label _r47Label;
        private System.Windows.Forms.Label _r43Label;
        private Forms.LedControl _module56;
        private Forms.LedControl _module40;
        private Forms.LedControl _module57;
        private Forms.LedControl _module18;
        private Forms.LedControl _module10;
        private Forms.LedControl _module69;
        private Forms.LedControl _module58;
        private Forms.LedControl _module70;
        private Forms.LedControl _module39;
        private Forms.LedControl _module80;
        private Forms.LedControl _module17;
        private Forms.LedControl _module51;
        private Forms.LedControl _module68;
        private Forms.LedControl _module55;
        private Forms.LedControl _module59;
        private Forms.LedControl _module9;
        private Forms.LedControl _module71;
        private Forms.LedControl _module75;
        private Forms.LedControl _module16;
        private Forms.LedControl _module64;
        private Forms.LedControl _module76;
        private Forms.LedControl _module46;
        private Forms.LedControl _module63;
        private Forms.LedControl _module15;
        private Forms.LedControl _module5;
        private Forms.LedControl _module54;
        private Forms.LedControl _module34;
        private Forms.LedControl _module60;
        private Forms.LedControl _module72;
        private Forms.LedControl _module65;
        private Forms.LedControl _module8;
        private Forms.LedControl _module77;
        private Forms.LedControl _module14;
        private Forms.LedControl _module53;
        private Forms.LedControl _module79;
        private Forms.LedControl _module61;
        private Forms.LedControl _module73;
        private Forms.LedControl _module67;
        private Forms.LedControl _module66;
        private Forms.LedControl _module13;
        private Forms.LedControl _module78;
        private Forms.LedControl _module1;
        private Forms.LedControl _module74;
        private Forms.LedControl _module50;
        private Forms.LedControl _module62;
        private Forms.LedControl _module52;
        private Forms.LedControl _module7;
        private Forms.LedControl _module12;
        private Forms.LedControl _module38;
        private Forms.LedControl _module45;
        private Forms.LedControl _module11;
        private Forms.LedControl _module2;
        private Forms.LedControl _module6;
        private Forms.LedControl _module3;
        private Forms.LedControl _module33;
        private Forms.LedControl _module4;
        private Forms.LedControl _module19;
        private Forms.LedControl _module29;
        private Forms.LedControl _module28;
        private Forms.LedControl _module27;
        private Forms.LedControl _module30;
        private Forms.LedControl _module49;
        private Forms.LedControl _module26;
        private Forms.LedControl _module37;
        private Forms.LedControl _module25;
        private Forms.LedControl _module44;
        private Forms.LedControl _module24;
        private Forms.LedControl _module23;
        private Forms.LedControl _module22;
        private Forms.LedControl _module21;
        private Forms.LedControl _module20;
        private Forms.LedControl _module32;
        private Forms.LedControl _module31;
        private Forms.LedControl _module35;
        private Forms.LedControl _module41;
        private Forms.LedControl _module48;
        private Forms.LedControl _module43;
        private Forms.LedControl _module36;
        private Forms.LedControl _module47;
        private Forms.LedControl _module42;
        private System.Windows.Forms.GroupBox _virtualReleGB;
        private System.Windows.Forms.GroupBox _releGB;
        private System.Windows.Forms.GroupBox groupBox18;
        private Forms.Diod diod12;
        private Forms.Diod diod11;
        private Forms.Diod diod10;
        private Forms.Diod diod9;
        private Forms.Diod diod8;
        private Forms.Diod diod7;
        private Forms.Diod diod6;
        private Forms.Diod diod5;
        private Forms.Diod diod4;
        private Forms.Diod diod3;
        private Forms.Diod diod2;
        private Forms.Diod diod1;
        private System.Windows.Forms.Label label190;
        private System.Windows.Forms.Label label189;
        private System.Windows.Forms.Label label179;
        private System.Windows.Forms.Label label180;
        private System.Windows.Forms.Label label181;
        private System.Windows.Forms.Label label182;
        private System.Windows.Forms.Label label183;
        private System.Windows.Forms.Label label184;
        private System.Windows.Forms.Label label185;
        private System.Windows.Forms.Label label186;
        private System.Windows.Forms.Label label187;
        private System.Windows.Forms.Label label188;
        private Forms.LedControl _d112;
        private System.Windows.Forms.Label _d112Label;
        private Forms.LedControl _d111;
        private System.Windows.Forms.Label _d111Label;
        private Forms.LedControl _d110;
        private System.Windows.Forms.Label _d110Label;
        private Forms.LedControl _d109;
        private System.Windows.Forms.Label _d109Label;
        private Forms.LedControl _d108;
        private System.Windows.Forms.Label _d108Label;
        private Forms.LedControl _d107;
        private System.Windows.Forms.Label _d107Label;
        private Forms.LedControl _d106;
        private System.Windows.Forms.Label _d106Label;
        private Forms.LedControl _d105;
        private System.Windows.Forms.Label _d105Label;
        private Forms.LedControl _d104;
        private System.Windows.Forms.Label _d104Label;
        private Forms.LedControl _d103;
        private System.Windows.Forms.Label _d103Label;
        private Forms.LedControl _d102;
        private System.Windows.Forms.Label _d102Label;
        private Forms.LedControl _d101;
        private System.Windows.Forms.Label _d101Label;
        private Forms.LedControl _d100;
        private System.Windows.Forms.Label _d100Label;
        private Forms.LedControl _d99;
        private System.Windows.Forms.Label _d99Label;
        private Forms.LedControl _d98;
        private System.Windows.Forms.Label _d98Label;
        private Forms.LedControl _d97;
        private System.Windows.Forms.Label _d97Label;
        private Forms.LedControl _d96;
        private System.Windows.Forms.Label _d96Label;
        private Forms.LedControl _d95;
        private System.Windows.Forms.Label _d95Label;
        private Forms.LedControl _d94;
        private System.Windows.Forms.Label _d94Label;
        private Forms.LedControl _d93;
        private System.Windows.Forms.Label _d93Label;
        private Forms.LedControl _d92;
        private System.Windows.Forms.Label _d92Label;
        private Forms.LedControl _d91;
        private System.Windows.Forms.Label _d91Label;
        private Forms.LedControl _d90;
        private System.Windows.Forms.Label _d90Label;
        private Forms.LedControl _d89;
        private System.Windows.Forms.Label _d89Label;
        private Forms.LedControl _d88;
        private System.Windows.Forms.Label _d88Label;
        private Forms.LedControl _d87;
        private System.Windows.Forms.Label _d87Label;
        private Forms.LedControl _d86;
        private System.Windows.Forms.Label _d86Label;
        private Forms.LedControl _d85;
        private System.Windows.Forms.Label _d85Label;
        private Forms.LedControl _d84;
        private System.Windows.Forms.Label _d84Label;
        private Forms.LedControl _d83;
        private System.Windows.Forms.Label _d83Label;
        private Forms.LedControl _d82;
        private System.Windows.Forms.Label _d82Label;
        private Forms.LedControl _d81;
        private System.Windows.Forms.Label _d81Label;
        private Forms.LedControl _d80;
        private System.Windows.Forms.Label _d80Label;
        private Forms.LedControl _d79;
        private System.Windows.Forms.Label _d79Label;
        private Forms.LedControl _d78;
        private System.Windows.Forms.Label _d78Label;
        private Forms.LedControl _d77;
        private System.Windows.Forms.Label _d77Label;
        private Forms.LedControl _d76;
        private System.Windows.Forms.Label _d76Label;
        private Forms.LedControl _d75;
        private System.Windows.Forms.Label _d75Label;
        private Forms.LedControl _d74;
        private System.Windows.Forms.Label _d74Label;
        private Forms.LedControl _d73;
        private System.Windows.Forms.Label _d73Label;
        private Forms.LedControl _ssl48;
        private Forms.LedControl _ssl40;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label1;
        private Forms.LedControl _ssl47;
        private Forms.LedControl _ssl39;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label2;
        private Forms.LedControl _ssl46;
        private Forms.LedControl _ssl38;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label3;
        private Forms.LedControl _ssl45;
        private Forms.LedControl _ssl37;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label4;
        private Forms.LedControl _ssl44;
        private Forms.LedControl _ssl36;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label5;
        private Forms.LedControl _ssl43;
        private Forms.LedControl _ssl35;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label6;
        private Forms.LedControl _ssl42;
        private Forms.LedControl _ssl34;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private Forms.LedControl _ssl41;
        private System.Windows.Forms.Label label9;
        private Forms.LedControl _ssl33;
        private System.Windows.Forms.Label label8;
        private Forms.LedControl _faultModule6;
        private System.Windows.Forms.Label label18;
    }
}