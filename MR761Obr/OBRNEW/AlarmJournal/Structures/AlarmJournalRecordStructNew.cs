﻿using System;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.MR761Obr.OBRNEW.AlarmJournal.Structures
{
    public class AlarmJournalRecordStructNew : StructBase
    {
        #region [Constants]

        private const string DATE_TIME_PATTERN = "{0:d2}.{1:d2}.{2:d2} {3:d2}:{4:d2}:{5:d2},{6:d3}";
        #endregion [Constants]


        #region [Private fields]

        [Layout(0)] private ushort _year;
        [Layout(1)] private ushort _month;
        [Layout(2)] private ushort _date;
        [Layout(3)] private ushort _hour;
        [Layout(4)] private ushort _minute;
        [Layout(5)] private ushort _second;
        [Layout(6)] private ushort _millisecond;
        [Layout(7)] private ushort _message;
        [Layout(8)] private ushort _numOfDefAndNumOfTrigParam;
        [Layout(9)] private ushort _groupOfSetpointsAndTypeDmg;
        [Layout(10)] private ushort _valueOfTriggeredParametr;
        [Layout(11)] private ushort _valueOfTriggeredParametr1;
        [Layout(12)] private ushort _d1;
        [Layout(13)] private ushort _d2;
        [Layout(14)] private ushort _d3;
        [Layout(15)] private ushort _d4;
        [Layout(16)] private ushort _d5;
        [Layout(17)] private ushort _d6;
        [Layout(18)] private ushort _d7;
        [Layout(19)] private ushort _d8;

        #endregion [Private fields]


        #region [Properties]
        public ushort Year
        {
            get { return this._year; }
        }

        public ushort Month
        {
            get { return this._month; }
        }

        public ushort Date
        {
            get { return this._date; }
        }

        public ushort Hour
        {
            get { return this._hour; }
        }

        public ushort Minute
        {
            get { return this._minute; }
        }

        public ushort Second
        {
            get { return this._second; }
        }

        public ushort Millisecond
        {
            get { return this._millisecond; }
        }
        /// <summary>
        /// true если во всех полях 0, условие конца ЖА
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                var sum = this.Year + this.Month + this.Date + this.Hour + this.Minute + this.Second + this.Millisecond
                    + this._message + this._numOfDefAndNumOfTrigParam + this._groupOfSetpointsAndTypeDmg + this._valueOfTriggeredParametr;
                return sum == 0;
            }
        }
        /// <summary>
        /// Дата и время сообщения
        /// </summary>
        public string GetTime
        {
            get
            {
                return string.Format
                    (
                        DATE_TIME_PATTERN,
                        this.Date,
                        this.Month,
                        this.Year,
                        this.Hour,
                        this.Minute,
                        this.Second,
                        this.Millisecond
                    );
            }
        }

        public ushort Message
        {
            get { return this._message; }
        }

        public int TriggeredDefense
        {
            get { return Common.GetBits(this._numOfDefAndNumOfTrigParam, 0, 1, 2, 3, 4, 5, 6, 7); }
        }

        public int GroupOfSetpoints
        {
            get { return Common.GetBits(this._groupOfSetpointsAndTypeDmg, 0, 1, 2, 3, 4, 5, 6, 7); }
        }

        public ushort D1
        {
            get { return this._d1; }
        }

        public ushort D2
        {
            get { return this._d2; }
        }

        public ushort D3
        {
            get { return this._d3; }
        }

        public ushort D4
        {
            get { return this._d4; }
        }

        public ushort D5
        {
            get { return this._d5; }
        }
        public ushort D6
        {
            get { return this._d6; }
        }
        public ushort D7
        {
            get { return this._d7; }
        }
        public ushort D8
        {
            get { return this._d8; }
        }

        public string D1To8
        {
            get { return Common.ByteToMask(Common.LOBYTE(this.D1), true); }
        }

        public string D9To16
        {
            get { return Common.ByteToMask(Common.HIBYTE(this.D1), true); }
        }

        public string D17To24
        {
            get { return Common.ByteToMask(Common.LOBYTE(this.D2), true); }
        }

        public string D25To32
        {
            get { return Common.ByteToMask(Common.HIBYTE(this.D2), true); }
        }

        public string D33To40
        {
            get { return Common.ByteToMask(Common.LOBYTE(this.D3), true); }
        }

        public string D41To48
        {
            get { return Common.ByteToMask(Common.HIBYTE(this.D3), true); }
        }
        public string D49To56
        {
            get { return Common.ByteToMask(Common.LOBYTE(this.D4), true); }
        }

        public string D57To64
        {
            get { return Common.ByteToMask(Common.HIBYTE(this.D4), true); }
        }
        public string D65To72
        {
            get { return Common.ByteToMask(Common.LOBYTE(this.D5), true); }
        }

        public string D73To80
        {
            get { return Common.ByteToMask(Common.HIBYTE(this.D5), true); }
        }

        public string D81To88
        {
            get { return Common.ByteToMask(Common.LOBYTE(this.D6), true); }
        }

        public string D89To96
        {
            get { return Common.ByteToMask(Common.HIBYTE(this.D6), true); }
        }

        public string D97To104
        {
            get { return Common.ByteToMask(Common.LOBYTE(this.D7), true); }
        }

        public string D105To112
        {
            get { return Common.ByteToMask(Common.HIBYTE(this.D7), true); }
        }

        #endregion [Properties]

    }
}
