﻿using System.Collections.Generic;
using System.Linq;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MR761Obr.OBRNEW.AlarmJournal.Structures;

namespace BEMN.MR761OBR.OBRNEW.AlarmJournal.Structures
{
    public class AllAlarmJournalStruct : StructBase
    {
        [Layout(0, Count = 14)] AlarmJournalRecordStructNew[] _alarmJournalRecords;

        public List<AlarmJournalRecordStructNew> AllJournalRecords
        {
            get { return new List<AlarmJournalRecordStructNew>(this._alarmJournalRecords.Where(j=>!j.IsEmpty));}
        }
    }
}
