﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.MemoryEntityClasses.FileOperations;
using BEMN.Devices.Structures;
using BEMN.Forms.Export;
using BEMN.Interfaces;
using BEMN.MR761Obr.OBRNEW.AlarmJournal.Structures;
using BEMN.MR761Obr.Properties;
using BEMN.MR761OBR.OBRNEW.AlarmJournal;
using SchemeEditorSystem.ResourceLibs;

namespace BEMN.MR761Obr.OBRNEW.AlarmJournal
{
    public partial class AlarmJournalFormNew : Form, IFormView
    {
        #region [Constants]
        private const string RECORDS_IN_JOURNAL = "Аварий в журнале - {0}";
        private const string READ_AJ_FAIL = "Невозможно прочитать журнал аварий";
        private const string READ_AJ = "Чтение журнала аварий";
        private const string ALARM_JOURNAL = "Журнал аварий";
        private const string TABLE_NAME = "MR761Obr_журнал_аварий";
        private const string JOURNAL_IS_EMPTY = "Журнал пуст";
        private const string JOURNAL_SAVED = "Журнал сохранён!";
        #endregion [Constants]

        #region [Private fields]
        private readonly MemoryEntity<OneWordStruct> _setPageAlarmJournal;
        private readonly MemoryEntity<AlarmJournalRecordStructNew> _alarmJournal;
        private readonly AlarmJournalLoader _journalLoader;
        private DataTable _table;
        private int _recordNumber;
        private int _failCounter;
        private Mr761ObrDevice _device;
        private FileDriver _fileDriver;

        private const string LIST_FILE_NAME = "jlist.xml";
        private const string READING_LIST_FILE = "Идет чтение файла списка подписей сигналов ЖА СПЛ";
        private const string DEVICE_NAME = "MR761OBR";
        private const string FAIL_READ =
            "Невозможно прочитать список подписей сигналов ЖА СПЛ. Списки будут сформированы по умолчанию.";
        private List<string> _messages;

        #endregion [Private fields]


        #region [Ctor's]
        public AlarmJournalFormNew()
        {
            this.InitializeComponent();
        }

        public AlarmJournalFormNew(Mr761ObrDevice device)
        {
            this.InitializeComponent();
            this._device = device;

            this._setPageAlarmJournal = device.RefreshAlarmJournalNew;
            this._setPageAlarmJournal.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadJournalRecord);
            this._setPageAlarmJournal.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadJournal);

            this._alarmJournal = device.AlarmJournalNew;
            this._alarmJournal.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadRecord);
            this._alarmJournal.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                if (this._failCounter < 80)
                {
                    this._failCounter++;
                }
                else
                {
                    this._alarmJournal.RemoveStructQueries();
                    this._failCounter = 0;
                    this.ButtonsEnabled = true;
                }
            });
            this._fileDriver = new FileDriver(device, this);
        }
        #endregion [Ctor's]


        #region [Help members]

        private bool ButtonsEnabled
        {
            set
            {
                this._readAlarmJournalButton.Enabled = this._saveAlarmJournalButton.Enabled =
                    this._loadAlarmJournalButton.Enabled = this._exportButton.Enabled = value;
            }
        }

        private void ReadJournalRecord()
        {
            this._alarmJournal.LoadStruct();
        }
   
        private void FailReadJournal()
        {
            this._statusLabel.Text = READ_AJ_FAIL;
            this.ButtonsEnabled = true;
        }

        private void ReadAllRecords()
        {
            if (this._journalLoader.JournalRecords.Count == 0)
            {
                this._statusLabel.Text = JOURNAL_IS_EMPTY;
                return;
            }

            this._recordNumber = 1;

            foreach (AlarmJournalRecordStructNew record in this._journalLoader.JournalRecords)
            {
                try
                {
                    this._recordNumber++;
                    this._failCounter = this._recordNumber;

                    this._table.Rows.Add
                    (
                        this._recordNumber,
                        record.GetTime,
                        AjStringsNew.Message[record.Message],
                        AjStringsNew.TriggeredDefense[record.TriggeredDefense],
                        AjStringsNew.AlarmJournalSetpointsGroup[record.GroupOfSetpoints],
                        record.D1To8,
                        record.D9To16,
                        record.D17To24,
                        record.D25To32,
                        record.D33To40,
                        record.D41To48,
                        record.D49To56,
                        record.D57To64,
                        record.D65To72,
                        record.D73To80,
                        record.D81To88,
                        record.D89To96,
                        record.D97To104,
                        record.D105To112
                    );

                    this._alarmJournalGrid.DataSource = this._table;
                    this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._recordNumber);
                    this.statusStrip1.Update();

                }
                catch (Exception e)
                {
                    this._table.Rows.Add
                    (
                        this._recordNumber - 1, "", "Ошибка (" + e.Message + ")", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""
                    );
                }
                this._recordNumber++;
            }
            this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._recordNumber - 1);
            this.statusStrip1.Update();
        }

        private void ReadRecord()
        {
            try
            {
                if (!this._alarmJournal.Value.IsEmpty)
                {
                    this._recordNumber++;
                    this._failCounter = 0;

                    AlarmJournalRecordStructNew record = this._alarmJournal.Value;
                    if (this._device.Info.DeviceConfiguration == "T0N0D74R35")
                    {
                        this._table.Rows.Add
                        (
                            this._recordNumber,
                            record.GetTime,
                            AjStringsNew.Message[record.Message],
                            AjStringsNew.TriggeredDefense[record.TriggeredDefense],
                            AjStringsNew.AlarmJournalSetpointsGroup[record.GroupOfSetpoints],
                            record.D1To8,
                            record.D9To16,
                            record.D17To24,
                            record.D25To32,
                            record.D33To40,
                            record.D41To48,
                            record.D49To56,
                            record.D57To64,
                            record.D65To72
                        );
                     }
                    else if (this._device.Info.DeviceConfiguration == "T0N0D106R67")
                    {
                        this._table.Rows.Add
                        (
                            this._recordNumber,
                            record.GetTime,
                            AjStringsNew.Message[record.Message],
                            AjStringsNew.TriggeredDefense[record.TriggeredDefense],
                            AjStringsNew.AlarmJournalSetpointsGroup[record.GroupOfSetpoints],
                            record.D1To8,
                            record.D9To16,
                            record.D17To24,
                            record.D25To32,
                            record.D33To40,
                            record.D41To48,
                            record.D49To56,
                            record.D57To64,
                            record.D65To72,
                            record.D73To80,
                            record.D81To88,
                            record.D89To96,
                            record.D97To104
                        );
                    }
                    else
                    {
                        this._table.Rows.Add
                        (
                            this._recordNumber,
                            record.GetTime,
                            AjStringsNew.Message[record.Message],
                            AjStringsNew.TriggeredDefense[record.TriggeredDefense],
                            AjStringsNew.AlarmJournalSetpointsGroup[record.GroupOfSetpoints],
                            record.D1To8,
                            record.D9To16,
                            record.D17To24,
                            record.D25To32,
                            record.D33To40,
                            record.D41To48,
                            record.D49To56,
                            record.D57To64,
                            record.D65To72,
                            record.D73To80,
                            record.D81To88,
                            record.D89To96,
                            record.D97To104,
                            record.D105To112
                        );
                    }

                    this._alarmJournalGrid.DataSource = this._table;
                    this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._recordNumber);
                    this.statusStrip1.Update();

                    SavePageNumber();
                }
                else
                {
                    if (this._table.Rows.Count == 0)
                    {
                        this._statusLabel.Text = JOURNAL_IS_EMPTY;
                    }
                    this.ButtonsEnabled = true;
                }
            }
            catch
            {
                this._statusLabel.Text = JOURNAL_IS_EMPTY;
            }
        }

        private DataTable GetJournalDataTable()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._device.Info.DeviceConfiguration = this._device.DevicePlant;
            }

            switch (_device.Info.DeviceConfiguration)
            {
                case "T0N0D74R35":
                    this._alarmJournalGrid.Columns.Remove(this._D9Col);
                    this._alarmJournalGrid.Columns.Remove(this._D10Col);
                    this._alarmJournalGrid.Columns.Remove(this._D11Col);
                    this._alarmJournalGrid.Columns.Remove(this._D12Col);
                    this._alarmJournalGrid.Columns.Remove(this._D13Col);
                    break;
                case "T0N0D106R67":
                    this._alarmJournalGrid.Columns.Remove(this._D13Col);
                    break;
            }

            DataTable table = new DataTable(TABLE_NAME);
            for (int j = 0; j < this._alarmJournalGrid.Columns.Count; j++)
            {
                table.Columns.Add(this._alarmJournalGrid.Columns[j].Name);
            }
            return table;
        }

        private void OnListRead(byte[] readBytes, string mes)
        {
            try
            {
                if (readBytes != null && readBytes.Length != 0 && mes == "Операция успешно выполнена")
                {
                    using (StreamReader streamReader = new StreamReader(new MemoryStream(readBytes),
                        Encoding.GetEncoding("windows-1251")))
                    {
                        using (XmlTextReader reader = new XmlTextReader(streamReader))
                        {
                            XmlRootAttribute root = new XmlRootAttribute(DEVICE_NAME);
                            XmlSerializer serializer = new XmlSerializer(typeof(ListsOfJournals), root);
                            ListsOfJournals lists = (ListsOfJournals)serializer.Deserialize(reader);
                            this._messages = lists.AlarmJournal.MessagesList;
                        }
                    }
                }
                else
                {
                    MessageBox.Show(FAIL_READ, "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    this._messages = PropFormsStrings.GetNewAlarmList();
                }
            }
            catch
            {
                MessageBox.Show(FAIL_READ, "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this._messages = PropFormsStrings.GetNewAlarmList();
            }
            this._recordNumber = 0;
            this.StartRead();
        }

        #endregion [Help members]

        #region [Event Handlers]
        private void AlarmJournalForm_Load(object sender, EventArgs e)
        {
            if (Device.AutoloadConfig && this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._fileDriver.ReadFile(this.OnListRead, LIST_FILE_NAME);
                this.ButtonsEnabled = false;
                this._statusLabel.Text = READING_LIST_FILE;
                this.statusStrip1.Update();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this._recordNumber = 0;
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._fileDriver.ReadFile(this.OnListRead, LIST_FILE_NAME);
                this.ButtonsEnabled = false;
                this._statusLabel.Text = READING_LIST_FILE;
                this.statusStrip1.Update();
            }
        }

        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._table = this.GetJournalDataTable();
            this._alarmJournalGrid.DataSource = this._table;
            this._table.Rows.Clear();
            this._statusLabel.Text = READ_AJ;

            this.SavePageNumber();
        }

        private void SavePageNumber()
        {
            this._setPageAlarmJournal.Value.Word = (ushort)this._recordNumber;
            this._setPageAlarmJournal.SaveStruct();
        }

        private void _loadAlarmJournalButton_Click(object sender, EventArgs e)
        {
            if (this._openAlarmJournalDialog.ShowDialog() == DialogResult.OK)
            {
                this._table.Clear();
                this._table.ReadXml(this._openAlarmJournalDialog.FileName);
                this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._table.Rows.Count);
            }
        }

        private void _saveAlarmJournalButton_Click(object sender, EventArgs e)
        {
            if (this._table.Columns.Count == 0)
            {
                MessageBox.Show(JOURNAL_IS_EMPTY);
                return;
            }
            if (this._saveAlarmJournalDialog.ShowDialog() == DialogResult.OK)
            {
                this._table.WriteXml(this._saveAlarmJournalDialog.FileName);
                _statusLabel.Text = "Файл: " + _saveAlarmJournalDialog.FileName + " успешно сохранен!";
            }
        }

        private void _exportButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (DialogResult.OK == this._saveJournalHtmlDialog.ShowDialog())
                {
                    HtmlExport.Export(this._table, this._saveJournalHtmlDialog.FileName, Resources.MR731AJ);
                    _statusLabel.Text = "Файл: " + _saveAlarmJournalDialog.FileName + " успешно сохранен!";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка сохранения!","Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _statusLabel.Text = "Файл: " + _saveAlarmJournalDialog.FileName + " не сохранен!";
                throw;
            }
            
        }
        
        private void AlarmJournalForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this._alarmJournal != null)
            {
                this._alarmJournal.RemoveStructQueries();
            }
            if (this._journalLoader != null)
            {
                this._journalLoader.ClearEvents();
            }
        }
        #endregion [Event Handlers]

        #region [IFormView Members]
        public Type FormDevice
        {
            get { return typeof(Mr761ObrDevice); }
        }

        public bool Multishow { get; private set; }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public Type ClassType
        {
            get { return typeof(AlarmJournalFormNew); }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.ja; }
        }

        public string NodeName
        {
            get { return ALARM_JOURNAL; }
        }
        #endregion [IFormView Members]

    }
}
