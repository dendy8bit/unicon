﻿namespace BEMN.MR761Obr.OBRNEW.AlarmJournal
{
    partial class AlarmJournalFormNew
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this._readAlarmJournalButton = new System.Windows.Forms.Button();
            this._alarmJournalGrid = new System.Windows.Forms.DataGridView();
            this._saveAlarmJournalButton = new System.Windows.Forms.Button();
            this._loadAlarmJournalButton = new System.Windows.Forms.Button();
            this._openAlarmJournalDialog = new System.Windows.Forms.OpenFileDialog();
            this._saveAlarmJournalDialog = new System.Windows.Forms.SaveFileDialog();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this._statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._exportButton = new System.Windows.Forms.Button();
            this._saveJournalHtmlDialog = new System.Windows.Forms.SaveFileDialog();
            this._indexCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._timeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._msg1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._msgCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._groupCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._D0Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._D1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._D2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._D3Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._D4Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._D5Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._D6Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._D7Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._D8Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._D9Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._D10Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._D11Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._D12Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._D13Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this._alarmJournalGrid)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _readAlarmJournalButton
            // 
            this._readAlarmJournalButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._readAlarmJournalButton.Location = new System.Drawing.Point(12, 539);
            this._readAlarmJournalButton.Name = "_readAlarmJournalButton";
            this._readAlarmJournalButton.Size = new System.Drawing.Size(143, 23);
            this._readAlarmJournalButton.TabIndex = 1;
            this._readAlarmJournalButton.Text = "Прочитать";
            this._readAlarmJournalButton.UseVisualStyleBackColor = true;
            this._readAlarmJournalButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // _alarmJournalGrid
            // 
            this._alarmJournalGrid.AllowUserToAddRows = false;
            this._alarmJournalGrid.AllowUserToDeleteRows = false;
            this._alarmJournalGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._alarmJournalGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._alarmJournalGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._alarmJournalGrid.BackgroundColor = System.Drawing.Color.White;
            this._alarmJournalGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._alarmJournalGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._indexCol,
            this._timeCol,
            this._msg1Col,
            this._msgCol,
            this._groupCol,
            this._D0Col,
            this._D1Col,
            this._D2Col,
            this._D3Col,
            this._D4Col,
            this._D5Col,
            this._D6Col,
            this._D7Col,
            this._D8Col,
            this._D9Col,
            this._D10Col,
            this._D11Col,
            this._D12Col,
            this._D13Col});
            this._alarmJournalGrid.Location = new System.Drawing.Point(0, 0);
            this._alarmJournalGrid.Margin = new System.Windows.Forms.Padding(100, 3, 3, 100);
            this._alarmJournalGrid.Name = "_alarmJournalGrid";
            this._alarmJournalGrid.ReadOnly = true;
            this._alarmJournalGrid.RowHeadersVisible = false;
            this._alarmJournalGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._alarmJournalGrid.Size = new System.Drawing.Size(788, 528);
            this._alarmJournalGrid.TabIndex = 19;
            // 
            // _saveAlarmJournalButton
            // 
            this._saveAlarmJournalButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._saveAlarmJournalButton.Location = new System.Drawing.Point(161, 539);
            this._saveAlarmJournalButton.Name = "_saveAlarmJournalButton";
            this._saveAlarmJournalButton.Size = new System.Drawing.Size(143, 23);
            this._saveAlarmJournalButton.TabIndex = 20;
            this._saveAlarmJournalButton.Text = "Сохранить в файл";
            this._saveAlarmJournalButton.UseVisualStyleBackColor = true;
            this._saveAlarmJournalButton.Click += new System.EventHandler(this._saveAlarmJournalButton_Click);
            // 
            // _loadAlarmJournalButton
            // 
            this._loadAlarmJournalButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._loadAlarmJournalButton.Location = new System.Drawing.Point(453, 539);
            this._loadAlarmJournalButton.Name = "_loadAlarmJournalButton";
            this._loadAlarmJournalButton.Size = new System.Drawing.Size(143, 23);
            this._loadAlarmJournalButton.TabIndex = 21;
            this._loadAlarmJournalButton.Text = "Загрузить из файла";
            this._loadAlarmJournalButton.UseVisualStyleBackColor = true;
            this._loadAlarmJournalButton.Click += new System.EventHandler(this._loadAlarmJournalButton_Click);
            // 
            // _openAlarmJournalDialog
            // 
            this._openAlarmJournalDialog.DefaultExt = "xml";
            this._openAlarmJournalDialog.FileName = "Журнал аварий МР 761 ОБР";
            this._openAlarmJournalDialog.Filter = "(Журнал аварий МР761 ОБР) | *.xml";
            this._openAlarmJournalDialog.RestoreDirectory = true;
            this._openAlarmJournalDialog.Title = "Открыть журнал  аварий для МР 761 ОБР";
            // 
            // _saveAlarmJournalDialog
            // 
            this._saveAlarmJournalDialog.DefaultExt = "xml";
            this._saveAlarmJournalDialog.FileName = "Журнал аварий МР 761 ОБР";
            this._saveAlarmJournalDialog.Filter = "(Журнал аварий МР 761 ОБР) | *.xml";
            this._saveAlarmJournalDialog.Title = "Сохранить  журнал аварий для МР 761 ОБР";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._statusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 571);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(788, 22);
            this.statusStrip1.TabIndex = 22;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // _statusLabel
            // 
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.Size = new System.Drawing.Size(108, 17);
            this._statusLabel.Text = "Аварий в журнале";
            // 
            // _exportButton
            // 
            this._exportButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._exportButton.Location = new System.Drawing.Point(310, 539);
            this._exportButton.Name = "_exportButton";
            this._exportButton.Size = new System.Drawing.Size(137, 23);
            this._exportButton.TabIndex = 23;
            this._exportButton.Text = "Сохранить в HTML";
            this._exportButton.UseVisualStyleBackColor = true;
            this._exportButton.Click += new System.EventHandler(this._exportButton_Click);
            // 
            // _saveJournalHtmlDialog
            // 
            this._saveJournalHtmlDialog.DefaultExt = "xml";
            this._saveJournalHtmlDialog.FileName = "Журнал аварий МР 761 ОБР";
            this._saveJournalHtmlDialog.Filter = "*.html | *.html";
            this._saveJournalHtmlDialog.Title = "Сохранить  журнал аварий для МР761 ОБР";
            // 
            // _indexCol
            // 
            this._indexCol.DataPropertyName = "_indexCol";
            this._indexCol.Frozen = true;
            this._indexCol.HeaderText = "№";
            this._indexCol.Name = "_indexCol";
            this._indexCol.ReadOnly = true;
            this._indexCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._indexCol.Width = 24;
            // 
            // _timeCol
            // 
            this._timeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this._timeCol.DataPropertyName = "_timeCol";
            this._timeCol.HeaderText = "Дата/Время";
            this._timeCol.Name = "_timeCol";
            this._timeCol.ReadOnly = true;
            this._timeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._timeCol.Width = 77;
            // 
            // _msg1Col
            // 
            this._msg1Col.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._msg1Col.DataPropertyName = "_msg1Col";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._msg1Col.DefaultCellStyle = dataGridViewCellStyle1;
            this._msg1Col.HeaderText = "Сообщение";
            this._msg1Col.Name = "_msg1Col";
            this._msg1Col.ReadOnly = true;
            this._msg1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _msgCol
            // 
            this._msgCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._msgCol.DataPropertyName = "_msgCol";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._msgCol.DefaultCellStyle = dataGridViewCellStyle2;
            this._msgCol.HeaderText = "Сработавшая защита";
            this._msgCol.Name = "_msgCol";
            this._msgCol.ReadOnly = true;
            this._msgCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._msgCol.Width = 140;
            // 
            // _groupCol
            // 
            this._groupCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._groupCol.DataPropertyName = "_groupCol";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._groupCol.DefaultCellStyle = dataGridViewCellStyle3;
            this._groupCol.HeaderText = "Группа уставок";
            this._groupCol.Name = "_groupCol";
            this._groupCol.ReadOnly = true;
            this._groupCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _D0Col
            // 
            this._D0Col.DataPropertyName = "_D0Col";
            this._D0Col.HeaderText = "Д [1-8]";
            this._D0Col.Name = "_D0Col";
            this._D0Col.ReadOnly = true;
            this._D0Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._D0Col.Width = 46;
            // 
            // _D1Col
            // 
            this._D1Col.DataPropertyName = "_D1Col";
            this._D1Col.HeaderText = "Д [9-16]";
            this._D1Col.Name = "_D1Col";
            this._D1Col.ReadOnly = true;
            this._D1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._D1Col.Width = 52;
            // 
            // _D2Col
            // 
            this._D2Col.DataPropertyName = "_D2Col";
            this._D2Col.HeaderText = "Д [17-24]";
            this._D2Col.Name = "_D2Col";
            this._D2Col.ReadOnly = true;
            this._D2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._D2Col.Width = 58;
            // 
            // _D3Col
            // 
            this._D3Col.DataPropertyName = "_D3Col";
            this._D3Col.HeaderText = "Д [25-32]";
            this._D3Col.Name = "_D3Col";
            this._D3Col.ReadOnly = true;
            this._D3Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._D3Col.Width = 58;
            // 
            // _D4Col
            // 
            this._D4Col.DataPropertyName = "_D4Col";
            this._D4Col.HeaderText = "Д [33-40]";
            this._D4Col.Name = "_D4Col";
            this._D4Col.ReadOnly = true;
            this._D4Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._D4Col.Width = 58;
            // 
            // _D5Col
            // 
            this._D5Col.DataPropertyName = "_D5Col";
            this._D5Col.HeaderText = "Д [41-48]";
            this._D5Col.Name = "_D5Col";
            this._D5Col.ReadOnly = true;
            this._D5Col.Width = 77;
            // 
            // _D6Col
            // 
            this._D6Col.DataPropertyName = "_D6Col";
            this._D6Col.HeaderText = "Д [49-56]";
            this._D6Col.Name = "_D6Col";
            this._D6Col.ReadOnly = true;
            this._D6Col.Width = 77;
            // 
            // _D7Col
            // 
            this._D7Col.DataPropertyName = "_D7Col";
            this._D7Col.HeaderText = "Д [57-64]";
            this._D7Col.Name = "_D7Col";
            this._D7Col.ReadOnly = true;
            this._D7Col.Width = 77;
            // 
            // _D8Col
            // 
            this._D8Col.DataPropertyName = "_D8Col";
            this._D8Col.HeaderText = "Д [65-72]";
            this._D8Col.Name = "_D8Col";
            this._D8Col.ReadOnly = true;
            this._D8Col.Width = 77;
            // 
            // _D9Col
            // 
            this._D9Col.DataPropertyName = "_D9Col";
            this._D9Col.HeaderText = "Д [73-80]";
            this._D9Col.Name = "_D9Col";
            this._D9Col.ReadOnly = true;
            this._D9Col.Width = 77;
            // 
            // _D10Col
            // 
            this._D10Col.DataPropertyName = "_D10Col";
            this._D10Col.HeaderText = "Д [81-88]";
            this._D10Col.Name = "_D10Col";
            this._D10Col.ReadOnly = true;
            this._D10Col.Width = 77;
            // 
            // _D11Col
            // 
            this._D11Col.DataPropertyName = "_D11Col";
            this._D11Col.HeaderText = "Д [89-96]";
            this._D11Col.Name = "_D11Col";
            this._D11Col.ReadOnly = true;
            this._D11Col.Width = 77;
            // 
            // _D12Col
            // 
            this._D12Col.DataPropertyName = "_D12Col";
            this._D12Col.HeaderText = "Д [97-104]";
            this._D12Col.Name = "_D12Col";
            this._D12Col.ReadOnly = true;
            this._D12Col.Width = 83;
            // 
            // _D13Col
            // 
            this._D13Col.DataPropertyName = "_D13Col";
            this._D13Col.HeaderText = "Д [105-112]";
            this._D13Col.Name = "_D13Col";
            this._D13Col.ReadOnly = true;
            this._D13Col.Width = 89;
            // 
            // AlarmJournalFormNew
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(788, 593);
            this.Controls.Add(this._exportButton);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this._loadAlarmJournalButton);
            this.Controls.Add(this._alarmJournalGrid);
            this.Controls.Add(this._readAlarmJournalButton);
            this.Controls.Add(this._saveAlarmJournalButton);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(600, 400);
            this.Name = "AlarmJournalFormNew";
            this.Text = "Mr901AlarmJournalForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AlarmJournalForm_FormClosing);
            this.Load += new System.EventHandler(this.AlarmJournalForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this._alarmJournalGrid)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button _readAlarmJournalButton;
        private System.Windows.Forms.DataGridView _alarmJournalGrid;
        private System.Windows.Forms.Button _saveAlarmJournalButton;
        private System.Windows.Forms.Button _loadAlarmJournalButton;
        private System.Windows.Forms.OpenFileDialog _openAlarmJournalDialog;
        private System.Windows.Forms.SaveFileDialog _saveAlarmJournalDialog;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel _statusLabel;
        private System.Windows.Forms.Button _exportButton;
        private System.Windows.Forms.SaveFileDialog _saveJournalHtmlDialog;
        private System.Windows.Forms.DataGridViewTextBoxColumn _indexCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _timeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _msg1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _msgCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _groupCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _D0Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _D1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _D2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _D3Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _D4Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _D5Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _D6Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _D7Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _D8Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _D9Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _D10Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _D11Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _D12Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _D13Col;
    }
}