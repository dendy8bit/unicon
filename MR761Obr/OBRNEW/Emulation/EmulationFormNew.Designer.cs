﻿namespace BEMN.MR761OBR.OBRNEW.Emulation
{
    partial class EmulationFormNew
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._startTime = new System.Windows.Forms.CheckBox();
            this._signal = new System.Windows.Forms.ComboBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.label31 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this._status = new System.Windows.Forms.MaskedTextBox();
            this._step = new System.Windows.Forms.NumericUpDown();
            this._getTimeGroupBox = new System.Windows.Forms.GroupBox();
            this._timeSignal = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.groupBoxDiskrets = new System.Windows.Forms.GroupBox();
            this.panelDiskretsTime = new System.Windows.Forms.Panel();
            this._d112list = new System.Windows.Forms.CheckBox();
            this._d111list = new System.Windows.Forms.CheckBox();
            this._k2list = new System.Windows.Forms.CheckBox();
            this._k1list = new System.Windows.Forms.CheckBox();
            this._d110list = new System.Windows.Forms.CheckBox();
            this._d109list = new System.Windows.Forms.CheckBox();
            this._d108list = new System.Windows.Forms.CheckBox();
            this._d107list = new System.Windows.Forms.CheckBox();
            this._d106list = new System.Windows.Forms.CheckBox();
            this._d105list = new System.Windows.Forms.CheckBox();
            this._d104list = new System.Windows.Forms.CheckBox();
            this._d103list = new System.Windows.Forms.CheckBox();
            this._d102list = new System.Windows.Forms.CheckBox();
            this._d101list = new System.Windows.Forms.CheckBox();
            this._d100list = new System.Windows.Forms.CheckBox();
            this._d99list = new System.Windows.Forms.CheckBox();
            this._d98list = new System.Windows.Forms.CheckBox();
            this._d97list = new System.Windows.Forms.CheckBox();
            this._d96list = new System.Windows.Forms.CheckBox();
            this._d95list = new System.Windows.Forms.CheckBox();
            this._d94list = new System.Windows.Forms.CheckBox();
            this._d93list = new System.Windows.Forms.CheckBox();
            this._d92list = new System.Windows.Forms.CheckBox();
            this._d91list = new System.Windows.Forms.CheckBox();
            this._d90list = new System.Windows.Forms.CheckBox();
            this._d89list = new System.Windows.Forms.CheckBox();
            this._d88list = new System.Windows.Forms.CheckBox();
            this._d87list = new System.Windows.Forms.CheckBox();
            this._d86list = new System.Windows.Forms.CheckBox();
            this._d85list = new System.Windows.Forms.CheckBox();
            this._d84list = new System.Windows.Forms.CheckBox();
            this._d83list = new System.Windows.Forms.CheckBox();
            this._d82list = new System.Windows.Forms.CheckBox();
            this._d81list = new System.Windows.Forms.CheckBox();
            this._d80list = new System.Windows.Forms.CheckBox();
            this._d79list = new System.Windows.Forms.CheckBox();
            this._d78list = new System.Windows.Forms.CheckBox();
            this._d77list = new System.Windows.Forms.CheckBox();
            this._d76list = new System.Windows.Forms.CheckBox();
            this._d75list = new System.Windows.Forms.CheckBox();
            this._d74list = new System.Windows.Forms.CheckBox();
            this._d73list = new System.Windows.Forms.CheckBox();
            this._d72list = new System.Windows.Forms.CheckBox();
            this._d71list = new System.Windows.Forms.CheckBox();
            this._d70list = new System.Windows.Forms.CheckBox();
            this._d69list = new System.Windows.Forms.CheckBox();
            this._d68list = new System.Windows.Forms.CheckBox();
            this._d67list = new System.Windows.Forms.CheckBox();
            this._d66list = new System.Windows.Forms.CheckBox();
            this._d65list = new System.Windows.Forms.CheckBox();
            this._d64list = new System.Windows.Forms.CheckBox();
            this._d63list = new System.Windows.Forms.CheckBox();
            this._d62list = new System.Windows.Forms.CheckBox();
            this._d61list = new System.Windows.Forms.CheckBox();
            this._d60list = new System.Windows.Forms.CheckBox();
            this._d59list = new System.Windows.Forms.CheckBox();
            this._d58list = new System.Windows.Forms.CheckBox();
            this._d57list = new System.Windows.Forms.CheckBox();
            this._d56list = new System.Windows.Forms.CheckBox();
            this._d55list = new System.Windows.Forms.CheckBox();
            this._d54list = new System.Windows.Forms.CheckBox();
            this._d53list = new System.Windows.Forms.CheckBox();
            this._d52list = new System.Windows.Forms.CheckBox();
            this._d51list = new System.Windows.Forms.CheckBox();
            this._d50list = new System.Windows.Forms.CheckBox();
            this._d49list = new System.Windows.Forms.CheckBox();
            this._d48list = new System.Windows.Forms.CheckBox();
            this._d47list = new System.Windows.Forms.CheckBox();
            this._d46list = new System.Windows.Forms.CheckBox();
            this._d45list = new System.Windows.Forms.CheckBox();
            this._d44list = new System.Windows.Forms.CheckBox();
            this._d43list = new System.Windows.Forms.CheckBox();
            this._d42list = new System.Windows.Forms.CheckBox();
            this._d41list = new System.Windows.Forms.CheckBox();
            this._d40list = new System.Windows.Forms.CheckBox();
            this._d39list = new System.Windows.Forms.CheckBox();
            this._d38list = new System.Windows.Forms.CheckBox();
            this._d37list = new System.Windows.Forms.CheckBox();
            this._d36list = new System.Windows.Forms.CheckBox();
            this._d35list = new System.Windows.Forms.CheckBox();
            this._d34list = new System.Windows.Forms.CheckBox();
            this._d33list = new System.Windows.Forms.CheckBox();
            this._d32list = new System.Windows.Forms.CheckBox();
            this._d31list = new System.Windows.Forms.CheckBox();
            this._d30list = new System.Windows.Forms.CheckBox();
            this._d29list = new System.Windows.Forms.CheckBox();
            this._d28list = new System.Windows.Forms.CheckBox();
            this._d27list = new System.Windows.Forms.CheckBox();
            this._d26list = new System.Windows.Forms.CheckBox();
            this._d25list = new System.Windows.Forms.CheckBox();
            this._d24list = new System.Windows.Forms.CheckBox();
            this._d23list = new System.Windows.Forms.CheckBox();
            this._d22list = new System.Windows.Forms.CheckBox();
            this._d21list = new System.Windows.Forms.CheckBox();
            this._d20list = new System.Windows.Forms.CheckBox();
            this._d19list = new System.Windows.Forms.CheckBox();
            this._d18list = new System.Windows.Forms.CheckBox();
            this._d17list = new System.Windows.Forms.CheckBox();
            this._d16list = new System.Windows.Forms.CheckBox();
            this._d15list = new System.Windows.Forms.CheckBox();
            this._d14list = new System.Windows.Forms.CheckBox();
            this._d13list = new System.Windows.Forms.CheckBox();
            this._d12list = new System.Windows.Forms.CheckBox();
            this._d11list = new System.Windows.Forms.CheckBox();
            this._d10list = new System.Windows.Forms.CheckBox();
            this._d9list = new System.Windows.Forms.CheckBox();
            this._d8list = new System.Windows.Forms.CheckBox();
            this._d7list = new System.Windows.Forms.CheckBox();
            this._d6list = new System.Windows.Forms.CheckBox();
            this._d5list = new System.Windows.Forms.CheckBox();
            this._d4list = new System.Windows.Forms.CheckBox();
            this._d3list = new System.Windows.Forms.CheckBox();
            this._d2list = new System.Windows.Forms.CheckBox();
            this._d1list = new System.Windows.Forms.CheckBox();
            this._writeEmulButton = new System.Windows.Forms.CheckBox();
            this.label34 = new System.Windows.Forms.Label();
            this.groupBoxInputSignal = new System.Windows.Forms.GroupBox();
            this.groupBoxInputDiskrets = new System.Windows.Forms.GroupBox();
            this.panelDiscrets = new System.Windows.Forms.Panel();
            this._k2 = new System.Windows.Forms.CheckBox();
            this._k1 = new System.Windows.Forms.CheckBox();
            this._d110 = new System.Windows.Forms.CheckBox();
            this._d109 = new System.Windows.Forms.CheckBox();
            this._d108 = new System.Windows.Forms.CheckBox();
            this._d107 = new System.Windows.Forms.CheckBox();
            this._d106 = new System.Windows.Forms.CheckBox();
            this._d105 = new System.Windows.Forms.CheckBox();
            this._d104 = new System.Windows.Forms.CheckBox();
            this._d103 = new System.Windows.Forms.CheckBox();
            this._d102 = new System.Windows.Forms.CheckBox();
            this._d112 = new System.Windows.Forms.CheckBox();
            this._d101 = new System.Windows.Forms.CheckBox();
            this._d111 = new System.Windows.Forms.CheckBox();
            this._d100 = new System.Windows.Forms.CheckBox();
            this._d99 = new System.Windows.Forms.CheckBox();
            this._d98 = new System.Windows.Forms.CheckBox();
            this._d97 = new System.Windows.Forms.CheckBox();
            this._d96 = new System.Windows.Forms.CheckBox();
            this._d95 = new System.Windows.Forms.CheckBox();
            this._d94 = new System.Windows.Forms.CheckBox();
            this._d93 = new System.Windows.Forms.CheckBox();
            this._d92 = new System.Windows.Forms.CheckBox();
            this._d91 = new System.Windows.Forms.CheckBox();
            this._d90 = new System.Windows.Forms.CheckBox();
            this._d89 = new System.Windows.Forms.CheckBox();
            this._d88 = new System.Windows.Forms.CheckBox();
            this._d87 = new System.Windows.Forms.CheckBox();
            this._d86 = new System.Windows.Forms.CheckBox();
            this._d85 = new System.Windows.Forms.CheckBox();
            this._d84 = new System.Windows.Forms.CheckBox();
            this._d83 = new System.Windows.Forms.CheckBox();
            this._d82 = new System.Windows.Forms.CheckBox();
            this._d81 = new System.Windows.Forms.CheckBox();
            this._d80 = new System.Windows.Forms.CheckBox();
            this._d79 = new System.Windows.Forms.CheckBox();
            this._d78 = new System.Windows.Forms.CheckBox();
            this._d77 = new System.Windows.Forms.CheckBox();
            this._d76 = new System.Windows.Forms.CheckBox();
            this._d75 = new System.Windows.Forms.CheckBox();
            this._d74 = new System.Windows.Forms.CheckBox();
            this._d73 = new System.Windows.Forms.CheckBox();
            this._d72 = new System.Windows.Forms.CheckBox();
            this._d71 = new System.Windows.Forms.CheckBox();
            this._d70 = new System.Windows.Forms.CheckBox();
            this._d69 = new System.Windows.Forms.CheckBox();
            this._d68 = new System.Windows.Forms.CheckBox();
            this._d67 = new System.Windows.Forms.CheckBox();
            this._d66 = new System.Windows.Forms.CheckBox();
            this._d65 = new System.Windows.Forms.CheckBox();
            this._d64 = new System.Windows.Forms.CheckBox();
            this._d63 = new System.Windows.Forms.CheckBox();
            this._d62 = new System.Windows.Forms.CheckBox();
            this._d61 = new System.Windows.Forms.CheckBox();
            this._d60 = new System.Windows.Forms.CheckBox();
            this._d59 = new System.Windows.Forms.CheckBox();
            this._d58 = new System.Windows.Forms.CheckBox();
            this._d57 = new System.Windows.Forms.CheckBox();
            this._d56 = new System.Windows.Forms.CheckBox();
            this._d55 = new System.Windows.Forms.CheckBox();
            this._d54 = new System.Windows.Forms.CheckBox();
            this._d53 = new System.Windows.Forms.CheckBox();
            this._d52 = new System.Windows.Forms.CheckBox();
            this._d51 = new System.Windows.Forms.CheckBox();
            this._d50 = new System.Windows.Forms.CheckBox();
            this._d49 = new System.Windows.Forms.CheckBox();
            this._d48 = new System.Windows.Forms.CheckBox();
            this._d47 = new System.Windows.Forms.CheckBox();
            this._d46 = new System.Windows.Forms.CheckBox();
            this._d45 = new System.Windows.Forms.CheckBox();
            this._d44 = new System.Windows.Forms.CheckBox();
            this._d43 = new System.Windows.Forms.CheckBox();
            this._d42 = new System.Windows.Forms.CheckBox();
            this._d41 = new System.Windows.Forms.CheckBox();
            this._d40 = new System.Windows.Forms.CheckBox();
            this._d39 = new System.Windows.Forms.CheckBox();
            this._d38 = new System.Windows.Forms.CheckBox();
            this._d37 = new System.Windows.Forms.CheckBox();
            this._d36 = new System.Windows.Forms.CheckBox();
            this._d35 = new System.Windows.Forms.CheckBox();
            this._d34 = new System.Windows.Forms.CheckBox();
            this._d33 = new System.Windows.Forms.CheckBox();
            this._d32 = new System.Windows.Forms.CheckBox();
            this._d31 = new System.Windows.Forms.CheckBox();
            this._d30 = new System.Windows.Forms.CheckBox();
            this._d29 = new System.Windows.Forms.CheckBox();
            this._d28 = new System.Windows.Forms.CheckBox();
            this._d27 = new System.Windows.Forms.CheckBox();
            this._d26 = new System.Windows.Forms.CheckBox();
            this._d25 = new System.Windows.Forms.CheckBox();
            this._d24 = new System.Windows.Forms.CheckBox();
            this._d23 = new System.Windows.Forms.CheckBox();
            this._d22 = new System.Windows.Forms.CheckBox();
            this._d21 = new System.Windows.Forms.CheckBox();
            this._d20 = new System.Windows.Forms.CheckBox();
            this._d19 = new System.Windows.Forms.CheckBox();
            this._d18 = new System.Windows.Forms.CheckBox();
            this._d17 = new System.Windows.Forms.CheckBox();
            this._d16 = new System.Windows.Forms.CheckBox();
            this._d15 = new System.Windows.Forms.CheckBox();
            this._d14 = new System.Windows.Forms.CheckBox();
            this._d13 = new System.Windows.Forms.CheckBox();
            this._d12 = new System.Windows.Forms.CheckBox();
            this._d11 = new System.Windows.Forms.CheckBox();
            this._d10 = new System.Windows.Forms.CheckBox();
            this._d9 = new System.Windows.Forms.CheckBox();
            this._d8 = new System.Windows.Forms.CheckBox();
            this._d7 = new System.Windows.Forms.CheckBox();
            this._d6 = new System.Windows.Forms.CheckBox();
            this._d5 = new System.Windows.Forms.CheckBox();
            this._d4 = new System.Windows.Forms.CheckBox();
            this._d3 = new System.Windows.Forms.CheckBox();
            this._d2 = new System.Windows.Forms.CheckBox();
            this._d1 = new System.Windows.Forms.CheckBox();
            this._time = new System.Windows.Forms.Label();
            this._labelStatus = new System.Windows.Forms.Label();
            this._kvit = new System.Windows.Forms.Button();
            this.kvitTooltip = new System.Windows.Forms.ToolTip(this.components);
            this._exitEmulation = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this._statusLedControl = new BEMN.Forms.LedControl();
            ((System.ComponentModel.ISupportInitialize)(this._step)).BeginInit();
            this._getTimeGroupBox.SuspendLayout();
            this.groupBoxDiskrets.SuspendLayout();
            this.panelDiskretsTime.SuspendLayout();
            this.groupBoxInputSignal.SuspendLayout();
            this.groupBoxInputDiskrets.SuspendLayout();
            this.panelDiscrets.SuspendLayout();
            this.SuspendLayout();
            // 
            // _startTime
            // 
            this._startTime.AutoSize = true;
            this._startTime.Location = new System.Drawing.Point(16, 46);
            this._startTime.Name = "_startTime";
            this._startTime.Size = new System.Drawing.Size(173, 17);
            this._startTime.TabIndex = 0;
            this._startTime.Text = "Старт по любому изменению";
            this._startTime.UseVisualStyleBackColor = true;
            this._startTime.CheckedChanged += new System.EventHandler(this._startTime_CheckedChanged);
            // 
            // _signal
            // 
            this._signal.FormattingEnabled = true;
            this._signal.Location = new System.Drawing.Point(117, 19);
            this._signal.Name = "_signal";
            this._signal.Size = new System.Drawing.Size(96, 21);
            this._signal.TabIndex = 1;
            this._signal.SelectedIndexChanged += new System.EventHandler(this._signal_SelectedIndexChanged);
            // 
            // label31
            // 
            this.label31.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(22, 526);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(36, 13);
            this.label31.TabIndex = 65;
            this.label31.Text = "Шаг  :";
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button2.Location = new System.Drawing.Point(163, 564);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(140, 33);
            this.button2.TabIndex = 67;
            this.button2.Text = "Прочитать эмуляцию";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.readEmulation_Click);
            // 
            // label32
            // 
            this.label32.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label32.Location = new System.Drawing.Point(6, 259);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(118, 26);
            this.label32.TabIndex = 68;
            this.label32.Text = "Время с момента подачи воздействия";
            // 
            // label33
            // 
            this.label33.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(149, 526);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(135, 13);
            this.label33.TabIndex = 70;
            this.label33.Text = "Время режима эмуляции";
            // 
            // _status
            // 
            this._status.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._status.Location = new System.Drawing.Point(184, 266);
            this._status.Name = "_status";
            this._status.Size = new System.Drawing.Size(29, 20);
            this._status.TabIndex = 73;
            this._status.Visible = false;
            this._status.TextChanged += new System.EventHandler(this._status_TextChanged);
            // 
            // _step
            // 
            this._step.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._step.DecimalPlaces = 2;
            this._step.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this._step.Location = new System.Drawing.Point(65, 526);
            this._step.Name = "_step";
            this._step.Size = new System.Drawing.Size(58, 20);
            this._step.TabIndex = 74;
            this._step.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // _getTimeGroupBox
            // 
            this._getTimeGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this._getTimeGroupBox.Controls.Add(this._timeSignal);
            this._getTimeGroupBox.Controls.Add(this.label35);
            this._getTimeGroupBox.Controls.Add(this.label32);
            this._getTimeGroupBox.Controls.Add(this.groupBoxDiskrets);
            this._getTimeGroupBox.Controls.Add(this._startTime);
            this._getTimeGroupBox.Controls.Add(this._signal);
            this._getTimeGroupBox.Controls.Add(this._status);
            this._getTimeGroupBox.Location = new System.Drawing.Point(6, 219);
            this._getTimeGroupBox.Name = "_getTimeGroupBox";
            this._getTimeGroupBox.Size = new System.Drawing.Size(571, 292);
            this._getTimeGroupBox.TabIndex = 75;
            this._getTimeGroupBox.TabStop = false;
            this._getTimeGroupBox.Text = "Расчёт времени";
            // 
            // _timeSignal
            // 
            this._timeSignal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._timeSignal.AutoSize = true;
            this._timeSignal.Location = new System.Drawing.Point(139, 262);
            this._timeSignal.Name = "_timeSignal";
            this._timeSignal.Size = new System.Drawing.Size(0, 13);
            this._timeSignal.TabIndex = 78;
            // 
            // label35
            // 
            this.label35.Location = new System.Drawing.Point(13, 15);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(104, 29);
            this.label35.TabIndex = 77;
            this.label35.Text = "Сигнал окончания расчёта времени";
            // 
            // groupBoxDiskrets
            // 
            this.groupBoxDiskrets.Controls.Add(this.panelDiskretsTime);
            this.groupBoxDiskrets.Location = new System.Drawing.Point(9, 69);
            this.groupBoxDiskrets.Name = "groupBoxDiskrets";
            this.groupBoxDiskrets.Size = new System.Drawing.Size(553, 190);
            this.groupBoxDiskrets.TabIndex = 76;
            this.groupBoxDiskrets.TabStop = false;
            this.groupBoxDiskrets.Text = "Дискреты";
            // 
            // panelDiskretsTime
            // 
            this.panelDiskretsTime.Controls.Add(this._d112list);
            this.panelDiskretsTime.Controls.Add(this._d111list);
            this.panelDiskretsTime.Controls.Add(this._k2list);
            this.panelDiskretsTime.Controls.Add(this._k1list);
            this.panelDiskretsTime.Controls.Add(this._d110list);
            this.panelDiskretsTime.Controls.Add(this._d109list);
            this.panelDiskretsTime.Controls.Add(this._d108list);
            this.panelDiskretsTime.Controls.Add(this._d107list);
            this.panelDiskretsTime.Controls.Add(this._d106list);
            this.panelDiskretsTime.Controls.Add(this._d105list);
            this.panelDiskretsTime.Controls.Add(this._d104list);
            this.panelDiskretsTime.Controls.Add(this._d103list);
            this.panelDiskretsTime.Controls.Add(this._d102list);
            this.panelDiskretsTime.Controls.Add(this._d101list);
            this.panelDiskretsTime.Controls.Add(this._d100list);
            this.panelDiskretsTime.Controls.Add(this._d99list);
            this.panelDiskretsTime.Controls.Add(this._d98list);
            this.panelDiskretsTime.Controls.Add(this._d97list);
            this.panelDiskretsTime.Controls.Add(this._d96list);
            this.panelDiskretsTime.Controls.Add(this._d95list);
            this.panelDiskretsTime.Controls.Add(this._d94list);
            this.panelDiskretsTime.Controls.Add(this._d93list);
            this.panelDiskretsTime.Controls.Add(this._d92list);
            this.panelDiskretsTime.Controls.Add(this._d91list);
            this.panelDiskretsTime.Controls.Add(this._d90list);
            this.panelDiskretsTime.Controls.Add(this._d89list);
            this.panelDiskretsTime.Controls.Add(this._d88list);
            this.panelDiskretsTime.Controls.Add(this._d87list);
            this.panelDiskretsTime.Controls.Add(this._d86list);
            this.panelDiskretsTime.Controls.Add(this._d85list);
            this.panelDiskretsTime.Controls.Add(this._d84list);
            this.panelDiskretsTime.Controls.Add(this._d83list);
            this.panelDiskretsTime.Controls.Add(this._d82list);
            this.panelDiskretsTime.Controls.Add(this._d81list);
            this.panelDiskretsTime.Controls.Add(this._d80list);
            this.panelDiskretsTime.Controls.Add(this._d79list);
            this.panelDiskretsTime.Controls.Add(this._d78list);
            this.panelDiskretsTime.Controls.Add(this._d77list);
            this.panelDiskretsTime.Controls.Add(this._d76list);
            this.panelDiskretsTime.Controls.Add(this._d75list);
            this.panelDiskretsTime.Controls.Add(this._d74list);
            this.panelDiskretsTime.Controls.Add(this._d73list);
            this.panelDiskretsTime.Controls.Add(this._d72list);
            this.panelDiskretsTime.Controls.Add(this._d71list);
            this.panelDiskretsTime.Controls.Add(this._d70list);
            this.panelDiskretsTime.Controls.Add(this._d69list);
            this.panelDiskretsTime.Controls.Add(this._d68list);
            this.panelDiskretsTime.Controls.Add(this._d67list);
            this.panelDiskretsTime.Controls.Add(this._d66list);
            this.panelDiskretsTime.Controls.Add(this._d65list);
            this.panelDiskretsTime.Controls.Add(this._d64list);
            this.panelDiskretsTime.Controls.Add(this._d63list);
            this.panelDiskretsTime.Controls.Add(this._d62list);
            this.panelDiskretsTime.Controls.Add(this._d61list);
            this.panelDiskretsTime.Controls.Add(this._d60list);
            this.panelDiskretsTime.Controls.Add(this._d59list);
            this.panelDiskretsTime.Controls.Add(this._d58list);
            this.panelDiskretsTime.Controls.Add(this._d57list);
            this.panelDiskretsTime.Controls.Add(this._d56list);
            this.panelDiskretsTime.Controls.Add(this._d55list);
            this.panelDiskretsTime.Controls.Add(this._d54list);
            this.panelDiskretsTime.Controls.Add(this._d53list);
            this.panelDiskretsTime.Controls.Add(this._d52list);
            this.panelDiskretsTime.Controls.Add(this._d51list);
            this.panelDiskretsTime.Controls.Add(this._d50list);
            this.panelDiskretsTime.Controls.Add(this._d49list);
            this.panelDiskretsTime.Controls.Add(this._d48list);
            this.panelDiskretsTime.Controls.Add(this._d47list);
            this.panelDiskretsTime.Controls.Add(this._d46list);
            this.panelDiskretsTime.Controls.Add(this._d45list);
            this.panelDiskretsTime.Controls.Add(this._d44list);
            this.panelDiskretsTime.Controls.Add(this._d43list);
            this.panelDiskretsTime.Controls.Add(this._d42list);
            this.panelDiskretsTime.Controls.Add(this._d41list);
            this.panelDiskretsTime.Controls.Add(this._d40list);
            this.panelDiskretsTime.Controls.Add(this._d39list);
            this.panelDiskretsTime.Controls.Add(this._d38list);
            this.panelDiskretsTime.Controls.Add(this._d37list);
            this.panelDiskretsTime.Controls.Add(this._d36list);
            this.panelDiskretsTime.Controls.Add(this._d35list);
            this.panelDiskretsTime.Controls.Add(this._d34list);
            this.panelDiskretsTime.Controls.Add(this._d33list);
            this.panelDiskretsTime.Controls.Add(this._d32list);
            this.panelDiskretsTime.Controls.Add(this._d31list);
            this.panelDiskretsTime.Controls.Add(this._d30list);
            this.panelDiskretsTime.Controls.Add(this._d29list);
            this.panelDiskretsTime.Controls.Add(this._d28list);
            this.panelDiskretsTime.Controls.Add(this._d27list);
            this.panelDiskretsTime.Controls.Add(this._d26list);
            this.panelDiskretsTime.Controls.Add(this._d25list);
            this.panelDiskretsTime.Controls.Add(this._d24list);
            this.panelDiskretsTime.Controls.Add(this._d23list);
            this.panelDiskretsTime.Controls.Add(this._d22list);
            this.panelDiskretsTime.Controls.Add(this._d21list);
            this.panelDiskretsTime.Controls.Add(this._d20list);
            this.panelDiskretsTime.Controls.Add(this._d19list);
            this.panelDiskretsTime.Controls.Add(this._d18list);
            this.panelDiskretsTime.Controls.Add(this._d17list);
            this.panelDiskretsTime.Controls.Add(this._d16list);
            this.panelDiskretsTime.Controls.Add(this._d15list);
            this.panelDiskretsTime.Controls.Add(this._d14list);
            this.panelDiskretsTime.Controls.Add(this._d13list);
            this.panelDiskretsTime.Controls.Add(this._d12list);
            this.panelDiskretsTime.Controls.Add(this._d11list);
            this.panelDiskretsTime.Controls.Add(this._d10list);
            this.panelDiskretsTime.Controls.Add(this._d9list);
            this.panelDiskretsTime.Controls.Add(this._d8list);
            this.panelDiskretsTime.Controls.Add(this._d7list);
            this.panelDiskretsTime.Controls.Add(this._d6list);
            this.panelDiskretsTime.Controls.Add(this._d5list);
            this.panelDiskretsTime.Controls.Add(this._d4list);
            this.panelDiskretsTime.Controls.Add(this._d3list);
            this.panelDiskretsTime.Controls.Add(this._d2list);
            this.panelDiskretsTime.Controls.Add(this._d1list);
            this.panelDiskretsTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelDiskretsTime.Location = new System.Drawing.Point(3, 16);
            this.panelDiskretsTime.Name = "panelDiskretsTime";
            this.panelDiskretsTime.Size = new System.Drawing.Size(547, 171);
            this.panelDiskretsTime.TabIndex = 82;
            // 
            // _d112list
            // 
            this._d112list.AutoSize = true;
            this._d112list.Location = new System.Drawing.Point(489, 17);
            this._d112list.Name = "_d112list";
            this._d112list.Size = new System.Drawing.Size(53, 17);
            this._d112list.TabIndex = 225;
            this._d112list.Text = "Д112";
            this._d112list.UseVisualStyleBackColor = true;
            // 
            // _d111list
            // 
            this._d111list.AutoSize = true;
            this._d111list.Location = new System.Drawing.Point(489, 2);
            this._d111list.Name = "_d111list";
            this._d111list.Size = new System.Drawing.Size(53, 17);
            this._d111list.TabIndex = 224;
            this._d111list.Text = "Д111";
            this._d111list.UseVisualStyleBackColor = true;
            // 
            // _k2list
            // 
            this._k2list.AutoSize = true;
            this._k2list.Location = new System.Drawing.Point(489, 47);
            this._k2list.Name = "_k2list";
            this._k2list.Size = new System.Drawing.Size(39, 17);
            this._k2list.TabIndex = 223;
            this._k2list.Text = "К2";
            this._k2list.UseVisualStyleBackColor = true;
            // 
            // _k1list
            // 
            this._k1list.AutoSize = true;
            this._k1list.Location = new System.Drawing.Point(489, 32);
            this._k1list.Name = "_k1list";
            this._k1list.Size = new System.Drawing.Size(39, 17);
            this._k1list.TabIndex = 222;
            this._k1list.Text = "К1";
            this._k1list.UseVisualStyleBackColor = true;
            // 
            // _d110list
            // 
            this._d110list.AutoSize = true;
            this._d110list.Location = new System.Drawing.Point(439, 152);
            this._d110list.Name = "_d110list";
            this._d110list.Size = new System.Drawing.Size(53, 17);
            this._d110list.TabIndex = 221;
            this._d110list.Text = "Д110";
            this._d110list.UseVisualStyleBackColor = true;
            // 
            // _d109list
            // 
            this._d109list.AutoSize = true;
            this._d109list.Location = new System.Drawing.Point(439, 137);
            this._d109list.Name = "_d109list";
            this._d109list.Size = new System.Drawing.Size(53, 17);
            this._d109list.TabIndex = 220;
            this._d109list.Text = "Д109";
            this._d109list.UseVisualStyleBackColor = true;
            // 
            // _d108list
            // 
            this._d108list.AutoSize = true;
            this._d108list.Location = new System.Drawing.Point(439, 122);
            this._d108list.Name = "_d108list";
            this._d108list.Size = new System.Drawing.Size(53, 17);
            this._d108list.TabIndex = 219;
            this._d108list.Text = "Д108";
            this._d108list.UseVisualStyleBackColor = true;
            // 
            // _d107list
            // 
            this._d107list.AutoSize = true;
            this._d107list.Location = new System.Drawing.Point(439, 107);
            this._d107list.Name = "_d107list";
            this._d107list.Size = new System.Drawing.Size(53, 17);
            this._d107list.TabIndex = 218;
            this._d107list.Text = "Д107";
            this._d107list.UseVisualStyleBackColor = true;
            // 
            // _d106list
            // 
            this._d106list.AutoSize = true;
            this._d106list.Location = new System.Drawing.Point(439, 92);
            this._d106list.Name = "_d106list";
            this._d106list.Size = new System.Drawing.Size(53, 17);
            this._d106list.TabIndex = 217;
            this._d106list.Text = "Д106";
            this._d106list.UseVisualStyleBackColor = true;
            // 
            // _d105list
            // 
            this._d105list.AutoSize = true;
            this._d105list.Location = new System.Drawing.Point(439, 77);
            this._d105list.Name = "_d105list";
            this._d105list.Size = new System.Drawing.Size(53, 17);
            this._d105list.TabIndex = 216;
            this._d105list.Text = "Д105";
            this._d105list.UseVisualStyleBackColor = true;
            // 
            // _d104list
            // 
            this._d104list.AutoSize = true;
            this._d104list.Location = new System.Drawing.Point(439, 62);
            this._d104list.Name = "_d104list";
            this._d104list.Size = new System.Drawing.Size(53, 17);
            this._d104list.TabIndex = 215;
            this._d104list.Text = "Д104";
            this._d104list.UseVisualStyleBackColor = true;
            // 
            // _d103list
            // 
            this._d103list.AutoSize = true;
            this._d103list.Location = new System.Drawing.Point(439, 47);
            this._d103list.Name = "_d103list";
            this._d103list.Size = new System.Drawing.Size(53, 17);
            this._d103list.TabIndex = 214;
            this._d103list.Text = "Д103";
            this._d103list.UseVisualStyleBackColor = true;
            // 
            // _d102list
            // 
            this._d102list.AutoSize = true;
            this._d102list.Location = new System.Drawing.Point(439, 32);
            this._d102list.Name = "_d102list";
            this._d102list.Size = new System.Drawing.Size(53, 17);
            this._d102list.TabIndex = 213;
            this._d102list.Text = "Д102";
            this._d102list.UseVisualStyleBackColor = true;
            // 
            // _d101list
            // 
            this._d101list.AutoSize = true;
            this._d101list.Location = new System.Drawing.Point(439, 17);
            this._d101list.Name = "_d101list";
            this._d101list.Size = new System.Drawing.Size(53, 17);
            this._d101list.TabIndex = 212;
            this._d101list.Text = "Д101";
            this._d101list.UseVisualStyleBackColor = true;
            // 
            // _d100list
            // 
            this._d100list.AutoSize = true;
            this._d100list.Location = new System.Drawing.Point(439, 2);
            this._d100list.Name = "_d100list";
            this._d100list.Size = new System.Drawing.Size(53, 17);
            this._d100list.TabIndex = 211;
            this._d100list.Text = "Д100";
            this._d100list.UseVisualStyleBackColor = true;
            // 
            // _d99list
            // 
            this._d99list.AutoSize = true;
            this._d99list.Location = new System.Drawing.Point(391, 152);
            this._d99list.Name = "_d99list";
            this._d99list.Size = new System.Drawing.Size(47, 17);
            this._d99list.TabIndex = 210;
            this._d99list.Text = "Д99";
            this._d99list.UseVisualStyleBackColor = true;
            // 
            // _d98list
            // 
            this._d98list.AutoSize = true;
            this._d98list.Location = new System.Drawing.Point(391, 137);
            this._d98list.Name = "_d98list";
            this._d98list.Size = new System.Drawing.Size(47, 17);
            this._d98list.TabIndex = 209;
            this._d98list.Text = "Д98";
            this._d98list.UseVisualStyleBackColor = true;
            // 
            // _d97list
            // 
            this._d97list.AutoSize = true;
            this._d97list.Location = new System.Drawing.Point(391, 122);
            this._d97list.Name = "_d97list";
            this._d97list.Size = new System.Drawing.Size(47, 17);
            this._d97list.TabIndex = 208;
            this._d97list.Text = "Д97";
            this._d97list.UseVisualStyleBackColor = true;
            // 
            // _d96list
            // 
            this._d96list.AutoSize = true;
            this._d96list.Location = new System.Drawing.Point(391, 107);
            this._d96list.Name = "_d96list";
            this._d96list.Size = new System.Drawing.Size(47, 17);
            this._d96list.TabIndex = 207;
            this._d96list.Text = "Д96";
            this._d96list.UseVisualStyleBackColor = true;
            // 
            // _d95list
            // 
            this._d95list.AutoSize = true;
            this._d95list.Location = new System.Drawing.Point(391, 92);
            this._d95list.Name = "_d95list";
            this._d95list.Size = new System.Drawing.Size(47, 17);
            this._d95list.TabIndex = 206;
            this._d95list.Text = "Д95";
            this._d95list.UseVisualStyleBackColor = true;
            // 
            // _d94list
            // 
            this._d94list.AutoSize = true;
            this._d94list.Location = new System.Drawing.Point(391, 77);
            this._d94list.Name = "_d94list";
            this._d94list.Size = new System.Drawing.Size(47, 17);
            this._d94list.TabIndex = 205;
            this._d94list.Text = "Д94";
            this._d94list.UseVisualStyleBackColor = true;
            // 
            // _d93list
            // 
            this._d93list.AutoSize = true;
            this._d93list.Location = new System.Drawing.Point(391, 62);
            this._d93list.Name = "_d93list";
            this._d93list.Size = new System.Drawing.Size(47, 17);
            this._d93list.TabIndex = 204;
            this._d93list.Text = "Д93";
            this._d93list.UseVisualStyleBackColor = true;
            // 
            // _d92list
            // 
            this._d92list.AutoSize = true;
            this._d92list.Location = new System.Drawing.Point(391, 47);
            this._d92list.Name = "_d92list";
            this._d92list.Size = new System.Drawing.Size(47, 17);
            this._d92list.TabIndex = 203;
            this._d92list.Text = "Д92";
            this._d92list.UseVisualStyleBackColor = true;
            // 
            // _d91list
            // 
            this._d91list.AutoSize = true;
            this._d91list.Location = new System.Drawing.Point(391, 32);
            this._d91list.Name = "_d91list";
            this._d91list.Size = new System.Drawing.Size(47, 17);
            this._d91list.TabIndex = 202;
            this._d91list.Text = "Д91";
            this._d91list.UseVisualStyleBackColor = true;
            // 
            // _d90list
            // 
            this._d90list.AutoSize = true;
            this._d90list.Location = new System.Drawing.Point(391, 17);
            this._d90list.Name = "_d90list";
            this._d90list.Size = new System.Drawing.Size(47, 17);
            this._d90list.TabIndex = 201;
            this._d90list.Text = "Д90";
            this._d90list.UseVisualStyleBackColor = true;
            // 
            // _d89list
            // 
            this._d89list.AutoSize = true;
            this._d89list.Location = new System.Drawing.Point(391, 2);
            this._d89list.Name = "_d89list";
            this._d89list.Size = new System.Drawing.Size(47, 17);
            this._d89list.TabIndex = 200;
            this._d89list.Text = "Д89";
            this._d89list.UseVisualStyleBackColor = true;
            // 
            // _d88list
            // 
            this._d88list.AutoSize = true;
            this._d88list.Location = new System.Drawing.Point(342, 152);
            this._d88list.Name = "_d88list";
            this._d88list.Size = new System.Drawing.Size(47, 17);
            this._d88list.TabIndex = 199;
            this._d88list.Text = "Д88";
            this._d88list.UseVisualStyleBackColor = true;
            // 
            // _d87list
            // 
            this._d87list.AutoSize = true;
            this._d87list.Location = new System.Drawing.Point(342, 137);
            this._d87list.Name = "_d87list";
            this._d87list.Size = new System.Drawing.Size(47, 17);
            this._d87list.TabIndex = 198;
            this._d87list.Text = "Д87";
            this._d87list.UseVisualStyleBackColor = true;
            // 
            // _d86list
            // 
            this._d86list.AutoSize = true;
            this._d86list.Location = new System.Drawing.Point(342, 122);
            this._d86list.Name = "_d86list";
            this._d86list.Size = new System.Drawing.Size(47, 17);
            this._d86list.TabIndex = 197;
            this._d86list.Text = "Д86";
            this._d86list.UseVisualStyleBackColor = true;
            // 
            // _d85list
            // 
            this._d85list.AutoSize = true;
            this._d85list.Location = new System.Drawing.Point(342, 107);
            this._d85list.Name = "_d85list";
            this._d85list.Size = new System.Drawing.Size(47, 17);
            this._d85list.TabIndex = 196;
            this._d85list.Text = "Д85";
            this._d85list.UseVisualStyleBackColor = true;
            // 
            // _d84list
            // 
            this._d84list.AutoSize = true;
            this._d84list.Location = new System.Drawing.Point(342, 92);
            this._d84list.Name = "_d84list";
            this._d84list.Size = new System.Drawing.Size(47, 17);
            this._d84list.TabIndex = 195;
            this._d84list.Text = "Д84";
            this._d84list.UseVisualStyleBackColor = true;
            // 
            // _d83list
            // 
            this._d83list.AutoSize = true;
            this._d83list.Location = new System.Drawing.Point(342, 77);
            this._d83list.Name = "_d83list";
            this._d83list.Size = new System.Drawing.Size(47, 17);
            this._d83list.TabIndex = 194;
            this._d83list.Text = "Д83";
            this._d83list.UseVisualStyleBackColor = true;
            // 
            // _d82list
            // 
            this._d82list.AutoSize = true;
            this._d82list.Location = new System.Drawing.Point(342, 62);
            this._d82list.Name = "_d82list";
            this._d82list.Size = new System.Drawing.Size(47, 17);
            this._d82list.TabIndex = 193;
            this._d82list.Text = "Д82";
            this._d82list.UseVisualStyleBackColor = true;
            // 
            // _d81list
            // 
            this._d81list.AutoSize = true;
            this._d81list.Location = new System.Drawing.Point(342, 47);
            this._d81list.Name = "_d81list";
            this._d81list.Size = new System.Drawing.Size(47, 17);
            this._d81list.TabIndex = 192;
            this._d81list.Text = "Д81";
            this._d81list.UseVisualStyleBackColor = true;
            // 
            // _d80list
            // 
            this._d80list.AutoSize = true;
            this._d80list.Location = new System.Drawing.Point(342, 32);
            this._d80list.Name = "_d80list";
            this._d80list.Size = new System.Drawing.Size(47, 17);
            this._d80list.TabIndex = 191;
            this._d80list.Text = "Д80";
            this._d80list.UseVisualStyleBackColor = true;
            // 
            // _d79list
            // 
            this._d79list.AutoSize = true;
            this._d79list.Location = new System.Drawing.Point(342, 17);
            this._d79list.Name = "_d79list";
            this._d79list.Size = new System.Drawing.Size(47, 17);
            this._d79list.TabIndex = 190;
            this._d79list.Text = "Д79";
            this._d79list.UseVisualStyleBackColor = true;
            // 
            // _d78list
            // 
            this._d78list.AutoSize = true;
            this._d78list.Location = new System.Drawing.Point(342, 2);
            this._d78list.Name = "_d78list";
            this._d78list.Size = new System.Drawing.Size(47, 17);
            this._d78list.TabIndex = 189;
            this._d78list.Text = "Д78";
            this._d78list.UseVisualStyleBackColor = true;
            // 
            // _d77list
            // 
            this._d77list.AutoSize = true;
            this._d77list.Location = new System.Drawing.Point(293, 152);
            this._d77list.Name = "_d77list";
            this._d77list.Size = new System.Drawing.Size(47, 17);
            this._d77list.TabIndex = 188;
            this._d77list.Text = "Д77";
            this._d77list.UseVisualStyleBackColor = true;
            // 
            // _d76list
            // 
            this._d76list.AutoSize = true;
            this._d76list.Location = new System.Drawing.Point(293, 137);
            this._d76list.Name = "_d76list";
            this._d76list.Size = new System.Drawing.Size(47, 17);
            this._d76list.TabIndex = 187;
            this._d76list.Text = "Д76";
            this._d76list.UseVisualStyleBackColor = true;
            // 
            // _d75list
            // 
            this._d75list.AutoSize = true;
            this._d75list.Location = new System.Drawing.Point(293, 122);
            this._d75list.Name = "_d75list";
            this._d75list.Size = new System.Drawing.Size(47, 17);
            this._d75list.TabIndex = 186;
            this._d75list.Text = "Д75";
            this._d75list.UseVisualStyleBackColor = true;
            // 
            // _d74list
            // 
            this._d74list.AutoSize = true;
            this._d74list.Location = new System.Drawing.Point(293, 107);
            this._d74list.Name = "_d74list";
            this._d74list.Size = new System.Drawing.Size(47, 17);
            this._d74list.TabIndex = 185;
            this._d74list.Text = "Д74";
            this._d74list.UseVisualStyleBackColor = true;
            // 
            // _d73list
            // 
            this._d73list.AutoSize = true;
            this._d73list.Location = new System.Drawing.Point(293, 92);
            this._d73list.Name = "_d73list";
            this._d73list.Size = new System.Drawing.Size(47, 17);
            this._d73list.TabIndex = 184;
            this._d73list.Text = "Д73";
            this._d73list.UseVisualStyleBackColor = true;
            // 
            // _d72list
            // 
            this._d72list.AutoSize = true;
            this._d72list.Location = new System.Drawing.Point(293, 77);
            this._d72list.Name = "_d72list";
            this._d72list.Size = new System.Drawing.Size(47, 17);
            this._d72list.TabIndex = 183;
            this._d72list.Text = "Д72";
            this._d72list.UseVisualStyleBackColor = true;
            // 
            // _d71list
            // 
            this._d71list.AutoSize = true;
            this._d71list.Location = new System.Drawing.Point(293, 62);
            this._d71list.Name = "_d71list";
            this._d71list.Size = new System.Drawing.Size(47, 17);
            this._d71list.TabIndex = 182;
            this._d71list.Text = "Д71";
            this._d71list.UseVisualStyleBackColor = true;
            // 
            // _d70list
            // 
            this._d70list.AutoSize = true;
            this._d70list.Location = new System.Drawing.Point(293, 47);
            this._d70list.Name = "_d70list";
            this._d70list.Size = new System.Drawing.Size(47, 17);
            this._d70list.TabIndex = 181;
            this._d70list.Text = "Д70";
            this._d70list.UseVisualStyleBackColor = true;
            // 
            // _d69list
            // 
            this._d69list.AutoSize = true;
            this._d69list.Location = new System.Drawing.Point(293, 32);
            this._d69list.Name = "_d69list";
            this._d69list.Size = new System.Drawing.Size(47, 17);
            this._d69list.TabIndex = 180;
            this._d69list.Text = "Д69";
            this._d69list.UseVisualStyleBackColor = true;
            // 
            // _d68list
            // 
            this._d68list.AutoSize = true;
            this._d68list.Location = new System.Drawing.Point(293, 17);
            this._d68list.Name = "_d68list";
            this._d68list.Size = new System.Drawing.Size(47, 17);
            this._d68list.TabIndex = 179;
            this._d68list.Text = "Д68";
            this._d68list.UseVisualStyleBackColor = true;
            // 
            // _d67list
            // 
            this._d67list.AutoSize = true;
            this._d67list.Location = new System.Drawing.Point(293, 2);
            this._d67list.Name = "_d67list";
            this._d67list.Size = new System.Drawing.Size(47, 17);
            this._d67list.TabIndex = 178;
            this._d67list.Text = "Д67";
            this._d67list.UseVisualStyleBackColor = true;
            // 
            // _d66list
            // 
            this._d66list.AutoSize = true;
            this._d66list.Location = new System.Drawing.Point(244, 152);
            this._d66list.Name = "_d66list";
            this._d66list.Size = new System.Drawing.Size(47, 17);
            this._d66list.TabIndex = 177;
            this._d66list.Text = "Д66";
            this._d66list.UseVisualStyleBackColor = true;
            // 
            // _d65list
            // 
            this._d65list.AutoSize = true;
            this._d65list.Location = new System.Drawing.Point(244, 137);
            this._d65list.Name = "_d65list";
            this._d65list.Size = new System.Drawing.Size(47, 17);
            this._d65list.TabIndex = 176;
            this._d65list.Text = "Д65";
            this._d65list.UseVisualStyleBackColor = true;
            // 
            // _d64list
            // 
            this._d64list.AutoSize = true;
            this._d64list.Location = new System.Drawing.Point(244, 122);
            this._d64list.Name = "_d64list";
            this._d64list.Size = new System.Drawing.Size(47, 17);
            this._d64list.TabIndex = 175;
            this._d64list.Text = "Д64";
            this._d64list.UseVisualStyleBackColor = true;
            // 
            // _d63list
            // 
            this._d63list.AutoSize = true;
            this._d63list.Location = new System.Drawing.Point(244, 107);
            this._d63list.Name = "_d63list";
            this._d63list.Size = new System.Drawing.Size(47, 17);
            this._d63list.TabIndex = 174;
            this._d63list.Text = "Д63";
            this._d63list.UseVisualStyleBackColor = true;
            // 
            // _d62list
            // 
            this._d62list.AutoSize = true;
            this._d62list.Location = new System.Drawing.Point(244, 92);
            this._d62list.Name = "_d62list";
            this._d62list.Size = new System.Drawing.Size(47, 17);
            this._d62list.TabIndex = 173;
            this._d62list.Text = "Д62";
            this._d62list.UseVisualStyleBackColor = true;
            // 
            // _d61list
            // 
            this._d61list.AutoSize = true;
            this._d61list.Location = new System.Drawing.Point(244, 77);
            this._d61list.Name = "_d61list";
            this._d61list.Size = new System.Drawing.Size(47, 17);
            this._d61list.TabIndex = 172;
            this._d61list.Text = "Д61";
            this._d61list.UseVisualStyleBackColor = true;
            // 
            // _d60list
            // 
            this._d60list.AutoSize = true;
            this._d60list.Location = new System.Drawing.Point(244, 62);
            this._d60list.Name = "_d60list";
            this._d60list.Size = new System.Drawing.Size(47, 17);
            this._d60list.TabIndex = 171;
            this._d60list.Text = "Д60";
            this._d60list.UseVisualStyleBackColor = true;
            // 
            // _d59list
            // 
            this._d59list.AutoSize = true;
            this._d59list.Location = new System.Drawing.Point(244, 47);
            this._d59list.Name = "_d59list";
            this._d59list.Size = new System.Drawing.Size(47, 17);
            this._d59list.TabIndex = 170;
            this._d59list.Text = "Д59";
            this._d59list.UseVisualStyleBackColor = true;
            // 
            // _d58list
            // 
            this._d58list.AutoSize = true;
            this._d58list.Location = new System.Drawing.Point(244, 32);
            this._d58list.Name = "_d58list";
            this._d58list.Size = new System.Drawing.Size(47, 17);
            this._d58list.TabIndex = 169;
            this._d58list.Text = "Д58";
            this._d58list.UseVisualStyleBackColor = true;
            // 
            // _d57list
            // 
            this._d57list.AutoSize = true;
            this._d57list.Location = new System.Drawing.Point(244, 17);
            this._d57list.Name = "_d57list";
            this._d57list.Size = new System.Drawing.Size(47, 17);
            this._d57list.TabIndex = 168;
            this._d57list.Text = "Д57";
            this._d57list.UseVisualStyleBackColor = true;
            // 
            // _d56list
            // 
            this._d56list.AutoSize = true;
            this._d56list.Location = new System.Drawing.Point(244, 2);
            this._d56list.Name = "_d56list";
            this._d56list.Size = new System.Drawing.Size(47, 17);
            this._d56list.TabIndex = 167;
            this._d56list.Text = "Д56";
            this._d56list.UseVisualStyleBackColor = true;
            // 
            // _d55list
            // 
            this._d55list.AutoSize = true;
            this._d55list.Location = new System.Drawing.Point(196, 152);
            this._d55list.Name = "_d55list";
            this._d55list.Size = new System.Drawing.Size(47, 17);
            this._d55list.TabIndex = 166;
            this._d55list.Text = "Д55";
            this._d55list.UseVisualStyleBackColor = true;
            // 
            // _d54list
            // 
            this._d54list.AutoSize = true;
            this._d54list.Location = new System.Drawing.Point(196, 137);
            this._d54list.Name = "_d54list";
            this._d54list.Size = new System.Drawing.Size(47, 17);
            this._d54list.TabIndex = 165;
            this._d54list.Text = "Д54";
            this._d54list.UseVisualStyleBackColor = true;
            // 
            // _d53list
            // 
            this._d53list.AutoSize = true;
            this._d53list.Location = new System.Drawing.Point(196, 122);
            this._d53list.Name = "_d53list";
            this._d53list.Size = new System.Drawing.Size(47, 17);
            this._d53list.TabIndex = 164;
            this._d53list.Text = "Д53";
            this._d53list.UseVisualStyleBackColor = true;
            // 
            // _d52list
            // 
            this._d52list.AutoSize = true;
            this._d52list.Location = new System.Drawing.Point(196, 107);
            this._d52list.Name = "_d52list";
            this._d52list.Size = new System.Drawing.Size(47, 17);
            this._d52list.TabIndex = 163;
            this._d52list.Text = "Д52";
            this._d52list.UseVisualStyleBackColor = true;
            // 
            // _d51list
            // 
            this._d51list.AutoSize = true;
            this._d51list.Location = new System.Drawing.Point(196, 92);
            this._d51list.Name = "_d51list";
            this._d51list.Size = new System.Drawing.Size(47, 17);
            this._d51list.TabIndex = 162;
            this._d51list.Text = "Д51";
            this._d51list.UseVisualStyleBackColor = true;
            // 
            // _d50list
            // 
            this._d50list.AutoSize = true;
            this._d50list.Location = new System.Drawing.Point(196, 77);
            this._d50list.Name = "_d50list";
            this._d50list.Size = new System.Drawing.Size(47, 17);
            this._d50list.TabIndex = 161;
            this._d50list.Text = "Д50";
            this._d50list.UseVisualStyleBackColor = true;
            // 
            // _d49list
            // 
            this._d49list.AutoSize = true;
            this._d49list.Location = new System.Drawing.Point(196, 62);
            this._d49list.Name = "_d49list";
            this._d49list.Size = new System.Drawing.Size(47, 17);
            this._d49list.TabIndex = 160;
            this._d49list.Text = "Д49";
            this._d49list.UseVisualStyleBackColor = true;
            // 
            // _d48list
            // 
            this._d48list.AutoSize = true;
            this._d48list.Location = new System.Drawing.Point(196, 47);
            this._d48list.Name = "_d48list";
            this._d48list.Size = new System.Drawing.Size(47, 17);
            this._d48list.TabIndex = 159;
            this._d48list.Text = "Д48";
            this._d48list.UseVisualStyleBackColor = true;
            // 
            // _d47list
            // 
            this._d47list.AutoSize = true;
            this._d47list.Location = new System.Drawing.Point(196, 32);
            this._d47list.Name = "_d47list";
            this._d47list.Size = new System.Drawing.Size(47, 17);
            this._d47list.TabIndex = 158;
            this._d47list.Text = "Д47";
            this._d47list.UseVisualStyleBackColor = true;
            // 
            // _d46list
            // 
            this._d46list.AutoSize = true;
            this._d46list.Location = new System.Drawing.Point(196, 17);
            this._d46list.Name = "_d46list";
            this._d46list.Size = new System.Drawing.Size(47, 17);
            this._d46list.TabIndex = 157;
            this._d46list.Text = "Д46";
            this._d46list.UseVisualStyleBackColor = true;
            // 
            // _d45list
            // 
            this._d45list.AutoSize = true;
            this._d45list.Location = new System.Drawing.Point(196, 2);
            this._d45list.Name = "_d45list";
            this._d45list.Size = new System.Drawing.Size(47, 17);
            this._d45list.TabIndex = 156;
            this._d45list.Text = "Д45";
            this._d45list.UseVisualStyleBackColor = true;
            // 
            // _d44list
            // 
            this._d44list.AutoSize = true;
            this._d44list.Location = new System.Drawing.Point(147, 152);
            this._d44list.Name = "_d44list";
            this._d44list.Size = new System.Drawing.Size(47, 17);
            this._d44list.TabIndex = 155;
            this._d44list.Text = "Д44";
            this._d44list.UseVisualStyleBackColor = true;
            // 
            // _d43list
            // 
            this._d43list.AutoSize = true;
            this._d43list.Location = new System.Drawing.Point(147, 137);
            this._d43list.Name = "_d43list";
            this._d43list.Size = new System.Drawing.Size(47, 17);
            this._d43list.TabIndex = 154;
            this._d43list.Text = "Д43";
            this._d43list.UseVisualStyleBackColor = true;
            // 
            // _d42list
            // 
            this._d42list.AutoSize = true;
            this._d42list.Location = new System.Drawing.Point(147, 122);
            this._d42list.Name = "_d42list";
            this._d42list.Size = new System.Drawing.Size(47, 17);
            this._d42list.TabIndex = 153;
            this._d42list.Text = "Д42";
            this._d42list.UseVisualStyleBackColor = true;
            // 
            // _d41list
            // 
            this._d41list.AutoSize = true;
            this._d41list.Location = new System.Drawing.Point(147, 107);
            this._d41list.Name = "_d41list";
            this._d41list.Size = new System.Drawing.Size(47, 17);
            this._d41list.TabIndex = 152;
            this._d41list.Text = "Д41";
            this._d41list.UseVisualStyleBackColor = true;
            // 
            // _d40list
            // 
            this._d40list.AutoSize = true;
            this._d40list.Location = new System.Drawing.Point(147, 92);
            this._d40list.Name = "_d40list";
            this._d40list.Size = new System.Drawing.Size(47, 17);
            this._d40list.TabIndex = 151;
            this._d40list.Text = "Д40";
            this._d40list.UseVisualStyleBackColor = true;
            // 
            // _d39list
            // 
            this._d39list.AutoSize = true;
            this._d39list.Location = new System.Drawing.Point(147, 77);
            this._d39list.Name = "_d39list";
            this._d39list.Size = new System.Drawing.Size(47, 17);
            this._d39list.TabIndex = 150;
            this._d39list.Text = "Д39";
            this._d39list.UseVisualStyleBackColor = true;
            // 
            // _d38list
            // 
            this._d38list.AutoSize = true;
            this._d38list.Location = new System.Drawing.Point(147, 62);
            this._d38list.Name = "_d38list";
            this._d38list.Size = new System.Drawing.Size(47, 17);
            this._d38list.TabIndex = 149;
            this._d38list.Text = "Д38";
            this._d38list.UseVisualStyleBackColor = true;
            // 
            // _d37list
            // 
            this._d37list.AutoSize = true;
            this._d37list.Location = new System.Drawing.Point(147, 47);
            this._d37list.Name = "_d37list";
            this._d37list.Size = new System.Drawing.Size(47, 17);
            this._d37list.TabIndex = 148;
            this._d37list.Text = "Д37";
            this._d37list.UseVisualStyleBackColor = true;
            // 
            // _d36list
            // 
            this._d36list.AutoSize = true;
            this._d36list.Location = new System.Drawing.Point(147, 32);
            this._d36list.Name = "_d36list";
            this._d36list.Size = new System.Drawing.Size(47, 17);
            this._d36list.TabIndex = 147;
            this._d36list.Text = "Д36";
            this._d36list.UseVisualStyleBackColor = true;
            // 
            // _d35list
            // 
            this._d35list.AutoSize = true;
            this._d35list.Location = new System.Drawing.Point(147, 17);
            this._d35list.Name = "_d35list";
            this._d35list.Size = new System.Drawing.Size(47, 17);
            this._d35list.TabIndex = 146;
            this._d35list.Text = "Д35";
            this._d35list.UseVisualStyleBackColor = true;
            // 
            // _d34list
            // 
            this._d34list.AutoSize = true;
            this._d34list.Location = new System.Drawing.Point(147, 2);
            this._d34list.Name = "_d34list";
            this._d34list.Size = new System.Drawing.Size(47, 17);
            this._d34list.TabIndex = 145;
            this._d34list.Text = "Д34";
            this._d34list.UseVisualStyleBackColor = true;
            // 
            // _d33list
            // 
            this._d33list.AutoSize = true;
            this._d33list.Location = new System.Drawing.Point(99, 152);
            this._d33list.Name = "_d33list";
            this._d33list.Size = new System.Drawing.Size(47, 17);
            this._d33list.TabIndex = 144;
            this._d33list.Text = "Д33";
            this._d33list.UseVisualStyleBackColor = true;
            // 
            // _d32list
            // 
            this._d32list.AutoSize = true;
            this._d32list.Location = new System.Drawing.Point(99, 137);
            this._d32list.Name = "_d32list";
            this._d32list.Size = new System.Drawing.Size(47, 17);
            this._d32list.TabIndex = 143;
            this._d32list.Text = "Д32";
            this._d32list.UseVisualStyleBackColor = true;
            // 
            // _d31list
            // 
            this._d31list.AutoSize = true;
            this._d31list.Location = new System.Drawing.Point(99, 122);
            this._d31list.Name = "_d31list";
            this._d31list.Size = new System.Drawing.Size(47, 17);
            this._d31list.TabIndex = 142;
            this._d31list.Text = "Д31";
            this._d31list.UseVisualStyleBackColor = true;
            // 
            // _d30list
            // 
            this._d30list.AutoSize = true;
            this._d30list.Location = new System.Drawing.Point(99, 107);
            this._d30list.Name = "_d30list";
            this._d30list.Size = new System.Drawing.Size(47, 17);
            this._d30list.TabIndex = 141;
            this._d30list.Text = "Д30";
            this._d30list.UseVisualStyleBackColor = true;
            // 
            // _d29list
            // 
            this._d29list.AutoSize = true;
            this._d29list.Location = new System.Drawing.Point(99, 92);
            this._d29list.Name = "_d29list";
            this._d29list.Size = new System.Drawing.Size(47, 17);
            this._d29list.TabIndex = 140;
            this._d29list.Text = "Д29";
            this._d29list.UseVisualStyleBackColor = true;
            // 
            // _d28list
            // 
            this._d28list.AutoSize = true;
            this._d28list.Location = new System.Drawing.Point(99, 77);
            this._d28list.Name = "_d28list";
            this._d28list.Size = new System.Drawing.Size(47, 17);
            this._d28list.TabIndex = 139;
            this._d28list.Text = "Д28";
            this._d28list.UseVisualStyleBackColor = true;
            // 
            // _d27list
            // 
            this._d27list.AutoSize = true;
            this._d27list.Location = new System.Drawing.Point(99, 62);
            this._d27list.Name = "_d27list";
            this._d27list.Size = new System.Drawing.Size(47, 17);
            this._d27list.TabIndex = 138;
            this._d27list.Text = "Д27";
            this._d27list.UseVisualStyleBackColor = true;
            // 
            // _d26list
            // 
            this._d26list.AutoSize = true;
            this._d26list.Location = new System.Drawing.Point(99, 47);
            this._d26list.Name = "_d26list";
            this._d26list.Size = new System.Drawing.Size(47, 17);
            this._d26list.TabIndex = 137;
            this._d26list.Text = "Д26";
            this._d26list.UseVisualStyleBackColor = true;
            // 
            // _d25list
            // 
            this._d25list.AutoSize = true;
            this._d25list.Location = new System.Drawing.Point(99, 32);
            this._d25list.Name = "_d25list";
            this._d25list.Size = new System.Drawing.Size(47, 17);
            this._d25list.TabIndex = 136;
            this._d25list.Text = "Д25";
            this._d25list.UseVisualStyleBackColor = true;
            // 
            // _d24list
            // 
            this._d24list.AutoSize = true;
            this._d24list.Location = new System.Drawing.Point(99, 17);
            this._d24list.Name = "_d24list";
            this._d24list.Size = new System.Drawing.Size(47, 17);
            this._d24list.TabIndex = 135;
            this._d24list.Text = "Д24";
            this._d24list.UseVisualStyleBackColor = true;
            // 
            // _d23list
            // 
            this._d23list.AutoSize = true;
            this._d23list.Location = new System.Drawing.Point(99, 2);
            this._d23list.Name = "_d23list";
            this._d23list.Size = new System.Drawing.Size(47, 17);
            this._d23list.TabIndex = 134;
            this._d23list.Text = "Д23";
            this._d23list.UseVisualStyleBackColor = true;
            // 
            // _d22list
            // 
            this._d22list.AutoSize = true;
            this._d22list.Location = new System.Drawing.Point(50, 152);
            this._d22list.Name = "_d22list";
            this._d22list.Size = new System.Drawing.Size(47, 17);
            this._d22list.TabIndex = 133;
            this._d22list.Text = "Д22";
            this._d22list.UseVisualStyleBackColor = true;
            // 
            // _d21list
            // 
            this._d21list.AutoSize = true;
            this._d21list.Location = new System.Drawing.Point(50, 137);
            this._d21list.Name = "_d21list";
            this._d21list.Size = new System.Drawing.Size(47, 17);
            this._d21list.TabIndex = 132;
            this._d21list.Text = "Д21";
            this._d21list.UseVisualStyleBackColor = true;
            // 
            // _d20list
            // 
            this._d20list.AutoSize = true;
            this._d20list.Location = new System.Drawing.Point(50, 122);
            this._d20list.Name = "_d20list";
            this._d20list.Size = new System.Drawing.Size(47, 17);
            this._d20list.TabIndex = 131;
            this._d20list.Text = "Д20";
            this._d20list.UseVisualStyleBackColor = true;
            // 
            // _d19list
            // 
            this._d19list.AutoSize = true;
            this._d19list.Location = new System.Drawing.Point(50, 107);
            this._d19list.Name = "_d19list";
            this._d19list.Size = new System.Drawing.Size(47, 17);
            this._d19list.TabIndex = 130;
            this._d19list.Text = "Д19";
            this._d19list.UseVisualStyleBackColor = true;
            // 
            // _d18list
            // 
            this._d18list.AutoSize = true;
            this._d18list.Location = new System.Drawing.Point(50, 92);
            this._d18list.Name = "_d18list";
            this._d18list.Size = new System.Drawing.Size(47, 17);
            this._d18list.TabIndex = 129;
            this._d18list.Text = "Д18";
            this._d18list.UseVisualStyleBackColor = true;
            // 
            // _d17list
            // 
            this._d17list.AutoSize = true;
            this._d17list.Location = new System.Drawing.Point(50, 77);
            this._d17list.Name = "_d17list";
            this._d17list.Size = new System.Drawing.Size(47, 17);
            this._d17list.TabIndex = 128;
            this._d17list.Text = "Д17";
            this._d17list.UseVisualStyleBackColor = true;
            // 
            // _d16list
            // 
            this._d16list.AutoSize = true;
            this._d16list.Location = new System.Drawing.Point(50, 62);
            this._d16list.Name = "_d16list";
            this._d16list.Size = new System.Drawing.Size(47, 17);
            this._d16list.TabIndex = 127;
            this._d16list.Text = "Д16";
            this._d16list.UseVisualStyleBackColor = true;
            // 
            // _d15list
            // 
            this._d15list.AutoSize = true;
            this._d15list.Location = new System.Drawing.Point(50, 47);
            this._d15list.Name = "_d15list";
            this._d15list.Size = new System.Drawing.Size(47, 17);
            this._d15list.TabIndex = 126;
            this._d15list.Text = "Д15";
            this._d15list.UseVisualStyleBackColor = true;
            // 
            // _d14list
            // 
            this._d14list.AutoSize = true;
            this._d14list.Location = new System.Drawing.Point(50, 32);
            this._d14list.Name = "_d14list";
            this._d14list.Size = new System.Drawing.Size(47, 17);
            this._d14list.TabIndex = 125;
            this._d14list.Text = "Д14";
            this._d14list.UseVisualStyleBackColor = true;
            // 
            // _d13list
            // 
            this._d13list.AutoSize = true;
            this._d13list.Location = new System.Drawing.Point(50, 17);
            this._d13list.Name = "_d13list";
            this._d13list.Size = new System.Drawing.Size(47, 17);
            this._d13list.TabIndex = 124;
            this._d13list.Text = "Д13";
            this._d13list.UseVisualStyleBackColor = true;
            // 
            // _d12list
            // 
            this._d12list.AutoSize = true;
            this._d12list.Location = new System.Drawing.Point(50, 2);
            this._d12list.Name = "_d12list";
            this._d12list.Size = new System.Drawing.Size(47, 17);
            this._d12list.TabIndex = 123;
            this._d12list.Text = "Д12";
            this._d12list.UseVisualStyleBackColor = true;
            // 
            // _d11list
            // 
            this._d11list.AutoSize = true;
            this._d11list.Location = new System.Drawing.Point(4, 152);
            this._d11list.Name = "_d11list";
            this._d11list.Size = new System.Drawing.Size(47, 17);
            this._d11list.TabIndex = 122;
            this._d11list.Text = "Д11";
            this._d11list.UseVisualStyleBackColor = true;
            // 
            // _d10list
            // 
            this._d10list.AutoSize = true;
            this._d10list.Location = new System.Drawing.Point(4, 137);
            this._d10list.Name = "_d10list";
            this._d10list.Size = new System.Drawing.Size(47, 17);
            this._d10list.TabIndex = 121;
            this._d10list.Text = "Д10";
            this._d10list.UseVisualStyleBackColor = true;
            // 
            // _d9list
            // 
            this._d9list.AutoSize = true;
            this._d9list.Location = new System.Drawing.Point(4, 122);
            this._d9list.Name = "_d9list";
            this._d9list.Size = new System.Drawing.Size(41, 17);
            this._d9list.TabIndex = 120;
            this._d9list.Text = "Д9";
            this._d9list.UseVisualStyleBackColor = true;
            // 
            // _d8list
            // 
            this._d8list.AutoSize = true;
            this._d8list.Location = new System.Drawing.Point(4, 107);
            this._d8list.Name = "_d8list";
            this._d8list.Size = new System.Drawing.Size(41, 17);
            this._d8list.TabIndex = 119;
            this._d8list.Text = "Д8";
            this._d8list.UseVisualStyleBackColor = true;
            // 
            // _d7list
            // 
            this._d7list.AutoSize = true;
            this._d7list.Location = new System.Drawing.Point(4, 92);
            this._d7list.Name = "_d7list";
            this._d7list.Size = new System.Drawing.Size(41, 17);
            this._d7list.TabIndex = 118;
            this._d7list.Text = "Д7";
            this._d7list.UseVisualStyleBackColor = true;
            // 
            // _d6list
            // 
            this._d6list.AutoSize = true;
            this._d6list.Location = new System.Drawing.Point(4, 77);
            this._d6list.Name = "_d6list";
            this._d6list.Size = new System.Drawing.Size(41, 17);
            this._d6list.TabIndex = 117;
            this._d6list.Text = "Д6";
            this._d6list.UseVisualStyleBackColor = true;
            // 
            // _d5list
            // 
            this._d5list.AutoSize = true;
            this._d5list.Location = new System.Drawing.Point(4, 62);
            this._d5list.Name = "_d5list";
            this._d5list.Size = new System.Drawing.Size(41, 17);
            this._d5list.TabIndex = 116;
            this._d5list.Text = "Д5";
            this._d5list.UseVisualStyleBackColor = true;
            // 
            // _d4list
            // 
            this._d4list.AutoSize = true;
            this._d4list.Location = new System.Drawing.Point(4, 47);
            this._d4list.Name = "_d4list";
            this._d4list.Size = new System.Drawing.Size(41, 17);
            this._d4list.TabIndex = 115;
            this._d4list.Text = "Д4";
            this._d4list.UseVisualStyleBackColor = true;
            // 
            // _d3list
            // 
            this._d3list.AutoSize = true;
            this._d3list.Location = new System.Drawing.Point(4, 32);
            this._d3list.Name = "_d3list";
            this._d3list.Size = new System.Drawing.Size(41, 17);
            this._d3list.TabIndex = 114;
            this._d3list.Text = "Д3";
            this._d3list.UseVisualStyleBackColor = true;
            // 
            // _d2list
            // 
            this._d2list.AutoSize = true;
            this._d2list.Location = new System.Drawing.Point(4, 17);
            this._d2list.Name = "_d2list";
            this._d2list.Size = new System.Drawing.Size(41, 17);
            this._d2list.TabIndex = 113;
            this._d2list.Text = "Д2";
            this._d2list.UseVisualStyleBackColor = true;
            // 
            // _d1list
            // 
            this._d1list.AutoSize = true;
            this._d1list.Location = new System.Drawing.Point(4, 2);
            this._d1list.Name = "_d1list";
            this._d1list.Size = new System.Drawing.Size(41, 17);
            this._d1list.TabIndex = 112;
            this._d1list.Text = "Д1";
            this._d1list.UseVisualStyleBackColor = true;
            // 
            // _writeEmulButton
            // 
            this._writeEmulButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._writeEmulButton.Appearance = System.Windows.Forms.Appearance.Button;
            this._writeEmulButton.BackColor = System.Drawing.SystemColors.Control;
            this._writeEmulButton.Location = new System.Drawing.Point(24, 564);
            this._writeEmulButton.Name = "_writeEmulButton";
            this._writeEmulButton.Size = new System.Drawing.Size(121, 33);
            this._writeEmulButton.TabIndex = 77;
            this._writeEmulButton.Text = "Записать эмуляцию ";
            this._writeEmulButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this._writeEmulButton.UseVisualStyleBackColor = false;
            this._writeEmulButton.CheckedChanged += new System.EventHandler(this._writeEmulButton_CheckedChanged);
            // 
            // label34
            // 
            this.label34.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(283, 526);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(71, 13);
            this.label34.TabIndex = 74;
            this.label34.Text = "(мин:сек,мс)";
            // 
            // groupBoxInputSignal
            // 
            this.groupBoxInputSignal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBoxInputSignal.Controls.Add(this.groupBoxInputDiskrets);
            this.groupBoxInputSignal.Location = new System.Drawing.Point(6, 3);
            this.groupBoxInputSignal.Name = "groupBoxInputSignal";
            this.groupBoxInputSignal.Size = new System.Drawing.Size(571, 210);
            this.groupBoxInputSignal.TabIndex = 78;
            this.groupBoxInputSignal.TabStop = false;
            this.groupBoxInputSignal.Text = "Входные сигналы";
            this.groupBoxInputSignal.TextChanged += new System.EventHandler(this._time_TextChanged);
            // 
            // groupBoxInputDiskrets
            // 
            this.groupBoxInputDiskrets.Controls.Add(this.panelDiscrets);
            this.groupBoxInputDiskrets.Location = new System.Drawing.Point(6, 19);
            this.groupBoxInputDiskrets.Name = "groupBoxInputDiskrets";
            this.groupBoxInputDiskrets.Size = new System.Drawing.Size(559, 191);
            this.groupBoxInputDiskrets.TabIndex = 65;
            this.groupBoxInputDiskrets.TabStop = false;
            this.groupBoxInputDiskrets.Text = "Дискреты";
            // 
            // panelDiscrets
            // 
            this.panelDiscrets.Controls.Add(this._k2);
            this.panelDiscrets.Controls.Add(this._k1);
            this.panelDiscrets.Controls.Add(this._d110);
            this.panelDiscrets.Controls.Add(this._d109);
            this.panelDiscrets.Controls.Add(this._d108);
            this.panelDiscrets.Controls.Add(this._d107);
            this.panelDiscrets.Controls.Add(this._d106);
            this.panelDiscrets.Controls.Add(this._d105);
            this.panelDiscrets.Controls.Add(this._d104);
            this.panelDiscrets.Controls.Add(this._d103);
            this.panelDiscrets.Controls.Add(this._d102);
            this.panelDiscrets.Controls.Add(this._d112);
            this.panelDiscrets.Controls.Add(this._d101);
            this.panelDiscrets.Controls.Add(this._d111);
            this.panelDiscrets.Controls.Add(this._d100);
            this.panelDiscrets.Controls.Add(this._d99);
            this.panelDiscrets.Controls.Add(this._d98);
            this.panelDiscrets.Controls.Add(this._d97);
            this.panelDiscrets.Controls.Add(this._d96);
            this.panelDiscrets.Controls.Add(this._d95);
            this.panelDiscrets.Controls.Add(this._d94);
            this.panelDiscrets.Controls.Add(this._d93);
            this.panelDiscrets.Controls.Add(this._d92);
            this.panelDiscrets.Controls.Add(this._d91);
            this.panelDiscrets.Controls.Add(this._d90);
            this.panelDiscrets.Controls.Add(this._d89);
            this.panelDiscrets.Controls.Add(this._d88);
            this.panelDiscrets.Controls.Add(this._d87);
            this.panelDiscrets.Controls.Add(this._d86);
            this.panelDiscrets.Controls.Add(this._d85);
            this.panelDiscrets.Controls.Add(this._d84);
            this.panelDiscrets.Controls.Add(this._d83);
            this.panelDiscrets.Controls.Add(this._d82);
            this.panelDiscrets.Controls.Add(this._d81);
            this.panelDiscrets.Controls.Add(this._d80);
            this.panelDiscrets.Controls.Add(this._d79);
            this.panelDiscrets.Controls.Add(this._d78);
            this.panelDiscrets.Controls.Add(this._d77);
            this.panelDiscrets.Controls.Add(this._d76);
            this.panelDiscrets.Controls.Add(this._d75);
            this.panelDiscrets.Controls.Add(this._d74);
            this.panelDiscrets.Controls.Add(this._d73);
            this.panelDiscrets.Controls.Add(this._d72);
            this.panelDiscrets.Controls.Add(this._d71);
            this.panelDiscrets.Controls.Add(this._d70);
            this.panelDiscrets.Controls.Add(this._d69);
            this.panelDiscrets.Controls.Add(this._d68);
            this.panelDiscrets.Controls.Add(this._d67);
            this.panelDiscrets.Controls.Add(this._d66);
            this.panelDiscrets.Controls.Add(this._d65);
            this.panelDiscrets.Controls.Add(this._d64);
            this.panelDiscrets.Controls.Add(this._d63);
            this.panelDiscrets.Controls.Add(this._d62);
            this.panelDiscrets.Controls.Add(this._d61);
            this.panelDiscrets.Controls.Add(this._d60);
            this.panelDiscrets.Controls.Add(this._d59);
            this.panelDiscrets.Controls.Add(this._d58);
            this.panelDiscrets.Controls.Add(this._d57);
            this.panelDiscrets.Controls.Add(this._d56);
            this.panelDiscrets.Controls.Add(this._d55);
            this.panelDiscrets.Controls.Add(this._d54);
            this.panelDiscrets.Controls.Add(this._d53);
            this.panelDiscrets.Controls.Add(this._d52);
            this.panelDiscrets.Controls.Add(this._d51);
            this.panelDiscrets.Controls.Add(this._d50);
            this.panelDiscrets.Controls.Add(this._d49);
            this.panelDiscrets.Controls.Add(this._d48);
            this.panelDiscrets.Controls.Add(this._d47);
            this.panelDiscrets.Controls.Add(this._d46);
            this.panelDiscrets.Controls.Add(this._d45);
            this.panelDiscrets.Controls.Add(this._d44);
            this.panelDiscrets.Controls.Add(this._d43);
            this.panelDiscrets.Controls.Add(this._d42);
            this.panelDiscrets.Controls.Add(this._d41);
            this.panelDiscrets.Controls.Add(this._d40);
            this.panelDiscrets.Controls.Add(this._d39);
            this.panelDiscrets.Controls.Add(this._d38);
            this.panelDiscrets.Controls.Add(this._d37);
            this.panelDiscrets.Controls.Add(this._d36);
            this.panelDiscrets.Controls.Add(this._d35);
            this.panelDiscrets.Controls.Add(this._d34);
            this.panelDiscrets.Controls.Add(this._d33);
            this.panelDiscrets.Controls.Add(this._d32);
            this.panelDiscrets.Controls.Add(this._d31);
            this.panelDiscrets.Controls.Add(this._d30);
            this.panelDiscrets.Controls.Add(this._d29);
            this.panelDiscrets.Controls.Add(this._d28);
            this.panelDiscrets.Controls.Add(this._d27);
            this.panelDiscrets.Controls.Add(this._d26);
            this.panelDiscrets.Controls.Add(this._d25);
            this.panelDiscrets.Controls.Add(this._d24);
            this.panelDiscrets.Controls.Add(this._d23);
            this.panelDiscrets.Controls.Add(this._d22);
            this.panelDiscrets.Controls.Add(this._d21);
            this.panelDiscrets.Controls.Add(this._d20);
            this.panelDiscrets.Controls.Add(this._d19);
            this.panelDiscrets.Controls.Add(this._d18);
            this.panelDiscrets.Controls.Add(this._d17);
            this.panelDiscrets.Controls.Add(this._d16);
            this.panelDiscrets.Controls.Add(this._d15);
            this.panelDiscrets.Controls.Add(this._d14);
            this.panelDiscrets.Controls.Add(this._d13);
            this.panelDiscrets.Controls.Add(this._d12);
            this.panelDiscrets.Controls.Add(this._d11);
            this.panelDiscrets.Controls.Add(this._d10);
            this.panelDiscrets.Controls.Add(this._d9);
            this.panelDiscrets.Controls.Add(this._d8);
            this.panelDiscrets.Controls.Add(this._d7);
            this.panelDiscrets.Controls.Add(this._d6);
            this.panelDiscrets.Controls.Add(this._d5);
            this.panelDiscrets.Controls.Add(this._d4);
            this.panelDiscrets.Controls.Add(this._d3);
            this.panelDiscrets.Controls.Add(this._d2);
            this.panelDiscrets.Controls.Add(this._d1);
            this.panelDiscrets.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelDiscrets.Location = new System.Drawing.Point(3, 16);
            this.panelDiscrets.Name = "panelDiscrets";
            this.panelDiscrets.Size = new System.Drawing.Size(553, 172);
            this.panelDiscrets.TabIndex = 42;
            // 
            // _k2
            // 
            this._k2.AutoSize = true;
            this._k2.Location = new System.Drawing.Point(492, 49);
            this._k2.Name = "_k2";
            this._k2.Size = new System.Drawing.Size(39, 17);
            this._k2.TabIndex = 111;
            this._k2.Text = "К2";
            this._k2.UseVisualStyleBackColor = true;
            // 
            // _k1
            // 
            this._k1.AutoSize = true;
            this._k1.Location = new System.Drawing.Point(492, 34);
            this._k1.Name = "_k1";
            this._k1.Size = new System.Drawing.Size(39, 17);
            this._k1.TabIndex = 110;
            this._k1.Text = "К1";
            this._k1.UseVisualStyleBackColor = true;
            // 
            // _d110
            // 
            this._d110.AutoSize = true;
            this._d110.Location = new System.Drawing.Point(439, 154);
            this._d110.Name = "_d110";
            this._d110.Size = new System.Drawing.Size(53, 17);
            this._d110.TabIndex = 109;
            this._d110.Text = "Д110";
            this._d110.UseVisualStyleBackColor = true;
            // 
            // _d109
            // 
            this._d109.AutoSize = true;
            this._d109.Location = new System.Drawing.Point(439, 139);
            this._d109.Name = "_d109";
            this._d109.Size = new System.Drawing.Size(53, 17);
            this._d109.TabIndex = 108;
            this._d109.Text = "Д109";
            this._d109.UseVisualStyleBackColor = true;
            // 
            // _d108
            // 
            this._d108.AutoSize = true;
            this._d108.Location = new System.Drawing.Point(439, 124);
            this._d108.Name = "_d108";
            this._d108.Size = new System.Drawing.Size(53, 17);
            this._d108.TabIndex = 107;
            this._d108.Text = "Д108";
            this._d108.UseVisualStyleBackColor = true;
            // 
            // _d107
            // 
            this._d107.AutoSize = true;
            this._d107.Location = new System.Drawing.Point(439, 109);
            this._d107.Name = "_d107";
            this._d107.Size = new System.Drawing.Size(53, 17);
            this._d107.TabIndex = 106;
            this._d107.Text = "Д107";
            this._d107.UseVisualStyleBackColor = true;
            // 
            // _d106
            // 
            this._d106.AutoSize = true;
            this._d106.Location = new System.Drawing.Point(439, 94);
            this._d106.Name = "_d106";
            this._d106.Size = new System.Drawing.Size(53, 17);
            this._d106.TabIndex = 105;
            this._d106.Text = "Д106";
            this._d106.UseVisualStyleBackColor = true;
            // 
            // _d105
            // 
            this._d105.AutoSize = true;
            this._d105.Location = new System.Drawing.Point(439, 79);
            this._d105.Name = "_d105";
            this._d105.Size = new System.Drawing.Size(53, 17);
            this._d105.TabIndex = 104;
            this._d105.Text = "Д105";
            this._d105.UseVisualStyleBackColor = true;
            // 
            // _d104
            // 
            this._d104.AutoSize = true;
            this._d104.Location = new System.Drawing.Point(439, 64);
            this._d104.Name = "_d104";
            this._d104.Size = new System.Drawing.Size(53, 17);
            this._d104.TabIndex = 103;
            this._d104.Text = "Д104";
            this._d104.UseVisualStyleBackColor = true;
            // 
            // _d103
            // 
            this._d103.AutoSize = true;
            this._d103.Location = new System.Drawing.Point(439, 49);
            this._d103.Name = "_d103";
            this._d103.Size = new System.Drawing.Size(53, 17);
            this._d103.TabIndex = 102;
            this._d103.Text = "Д103";
            this._d103.UseVisualStyleBackColor = true;
            // 
            // _d102
            // 
            this._d102.AutoSize = true;
            this._d102.Location = new System.Drawing.Point(439, 34);
            this._d102.Name = "_d102";
            this._d102.Size = new System.Drawing.Size(53, 17);
            this._d102.TabIndex = 101;
            this._d102.Text = "Д102";
            this._d102.UseVisualStyleBackColor = true;
            // 
            // _d112
            // 
            this._d112.AutoSize = true;
            this._d112.Location = new System.Drawing.Point(492, 19);
            this._d112.Name = "_d112";
            this._d112.Size = new System.Drawing.Size(53, 17);
            this._d112.TabIndex = 100;
            this._d112.Text = "Д112";
            this._d112.UseVisualStyleBackColor = true;
            // 
            // _d101
            // 
            this._d101.AutoSize = true;
            this._d101.Location = new System.Drawing.Point(439, 19);
            this._d101.Name = "_d101";
            this._d101.Size = new System.Drawing.Size(53, 17);
            this._d101.TabIndex = 100;
            this._d101.Text = "Д101";
            this._d101.UseVisualStyleBackColor = true;
            // 
            // _d111
            // 
            this._d111.AutoSize = true;
            this._d111.Location = new System.Drawing.Point(492, 4);
            this._d111.Name = "_d111";
            this._d111.Size = new System.Drawing.Size(53, 17);
            this._d111.TabIndex = 99;
            this._d111.Text = "Д111";
            this._d111.UseVisualStyleBackColor = true;
            // 
            // _d100
            // 
            this._d100.AutoSize = true;
            this._d100.Location = new System.Drawing.Point(439, 4);
            this._d100.Name = "_d100";
            this._d100.Size = new System.Drawing.Size(53, 17);
            this._d100.TabIndex = 99;
            this._d100.Text = "Д100";
            this._d100.UseVisualStyleBackColor = true;
            // 
            // _d99
            // 
            this._d99.AutoSize = true;
            this._d99.Location = new System.Drawing.Point(391, 154);
            this._d99.Name = "_d99";
            this._d99.Size = new System.Drawing.Size(47, 17);
            this._d99.TabIndex = 98;
            this._d99.Text = "Д99";
            this._d99.UseVisualStyleBackColor = true;
            // 
            // _d98
            // 
            this._d98.AutoSize = true;
            this._d98.Location = new System.Drawing.Point(391, 139);
            this._d98.Name = "_d98";
            this._d98.Size = new System.Drawing.Size(47, 17);
            this._d98.TabIndex = 97;
            this._d98.Text = "Д98";
            this._d98.UseVisualStyleBackColor = true;
            // 
            // _d97
            // 
            this._d97.AutoSize = true;
            this._d97.Location = new System.Drawing.Point(391, 124);
            this._d97.Name = "_d97";
            this._d97.Size = new System.Drawing.Size(47, 17);
            this._d97.TabIndex = 96;
            this._d97.Text = "Д97";
            this._d97.UseVisualStyleBackColor = true;
            // 
            // _d96
            // 
            this._d96.AutoSize = true;
            this._d96.Location = new System.Drawing.Point(391, 109);
            this._d96.Name = "_d96";
            this._d96.Size = new System.Drawing.Size(47, 17);
            this._d96.TabIndex = 95;
            this._d96.Text = "Д96";
            this._d96.UseVisualStyleBackColor = true;
            // 
            // _d95
            // 
            this._d95.AutoSize = true;
            this._d95.Location = new System.Drawing.Point(391, 94);
            this._d95.Name = "_d95";
            this._d95.Size = new System.Drawing.Size(47, 17);
            this._d95.TabIndex = 94;
            this._d95.Text = "Д95";
            this._d95.UseVisualStyleBackColor = true;
            // 
            // _d94
            // 
            this._d94.AutoSize = true;
            this._d94.Location = new System.Drawing.Point(391, 79);
            this._d94.Name = "_d94";
            this._d94.Size = new System.Drawing.Size(47, 17);
            this._d94.TabIndex = 93;
            this._d94.Text = "Д94";
            this._d94.UseVisualStyleBackColor = true;
            // 
            // _d93
            // 
            this._d93.AutoSize = true;
            this._d93.Location = new System.Drawing.Point(391, 64);
            this._d93.Name = "_d93";
            this._d93.Size = new System.Drawing.Size(47, 17);
            this._d93.TabIndex = 92;
            this._d93.Text = "Д93";
            this._d93.UseVisualStyleBackColor = true;
            // 
            // _d92
            // 
            this._d92.AutoSize = true;
            this._d92.Location = new System.Drawing.Point(391, 49);
            this._d92.Name = "_d92";
            this._d92.Size = new System.Drawing.Size(47, 17);
            this._d92.TabIndex = 91;
            this._d92.Text = "Д92";
            this._d92.UseVisualStyleBackColor = true;
            // 
            // _d91
            // 
            this._d91.AutoSize = true;
            this._d91.Location = new System.Drawing.Point(391, 34);
            this._d91.Name = "_d91";
            this._d91.Size = new System.Drawing.Size(47, 17);
            this._d91.TabIndex = 90;
            this._d91.Text = "Д91";
            this._d91.UseVisualStyleBackColor = true;
            // 
            // _d90
            // 
            this._d90.AutoSize = true;
            this._d90.Location = new System.Drawing.Point(391, 19);
            this._d90.Name = "_d90";
            this._d90.Size = new System.Drawing.Size(47, 17);
            this._d90.TabIndex = 89;
            this._d90.Text = "Д90";
            this._d90.UseVisualStyleBackColor = true;
            // 
            // _d89
            // 
            this._d89.AutoSize = true;
            this._d89.Location = new System.Drawing.Point(391, 4);
            this._d89.Name = "_d89";
            this._d89.Size = new System.Drawing.Size(47, 17);
            this._d89.TabIndex = 88;
            this._d89.Text = "Д89";
            this._d89.UseVisualStyleBackColor = true;
            // 
            // _d88
            // 
            this._d88.AutoSize = true;
            this._d88.Location = new System.Drawing.Point(342, 154);
            this._d88.Name = "_d88";
            this._d88.Size = new System.Drawing.Size(47, 17);
            this._d88.TabIndex = 87;
            this._d88.Text = "Д88";
            this._d88.UseVisualStyleBackColor = true;
            // 
            // _d87
            // 
            this._d87.AutoSize = true;
            this._d87.Location = new System.Drawing.Point(342, 139);
            this._d87.Name = "_d87";
            this._d87.Size = new System.Drawing.Size(47, 17);
            this._d87.TabIndex = 86;
            this._d87.Text = "Д87";
            this._d87.UseVisualStyleBackColor = true;
            // 
            // _d86
            // 
            this._d86.AutoSize = true;
            this._d86.Location = new System.Drawing.Point(342, 124);
            this._d86.Name = "_d86";
            this._d86.Size = new System.Drawing.Size(47, 17);
            this._d86.TabIndex = 85;
            this._d86.Text = "Д86";
            this._d86.UseVisualStyleBackColor = true;
            // 
            // _d85
            // 
            this._d85.AutoSize = true;
            this._d85.Location = new System.Drawing.Point(342, 109);
            this._d85.Name = "_d85";
            this._d85.Size = new System.Drawing.Size(47, 17);
            this._d85.TabIndex = 84;
            this._d85.Text = "Д85";
            this._d85.UseVisualStyleBackColor = true;
            // 
            // _d84
            // 
            this._d84.AutoSize = true;
            this._d84.Location = new System.Drawing.Point(342, 94);
            this._d84.Name = "_d84";
            this._d84.Size = new System.Drawing.Size(47, 17);
            this._d84.TabIndex = 83;
            this._d84.Text = "Д84";
            this._d84.UseVisualStyleBackColor = true;
            // 
            // _d83
            // 
            this._d83.AutoSize = true;
            this._d83.Location = new System.Drawing.Point(342, 79);
            this._d83.Name = "_d83";
            this._d83.Size = new System.Drawing.Size(47, 17);
            this._d83.TabIndex = 82;
            this._d83.Text = "Д83";
            this._d83.UseVisualStyleBackColor = true;
            // 
            // _d82
            // 
            this._d82.AutoSize = true;
            this._d82.Location = new System.Drawing.Point(342, 64);
            this._d82.Name = "_d82";
            this._d82.Size = new System.Drawing.Size(47, 17);
            this._d82.TabIndex = 81;
            this._d82.Text = "Д82";
            this._d82.UseVisualStyleBackColor = true;
            // 
            // _d81
            // 
            this._d81.AutoSize = true;
            this._d81.Location = new System.Drawing.Point(342, 49);
            this._d81.Name = "_d81";
            this._d81.Size = new System.Drawing.Size(47, 17);
            this._d81.TabIndex = 80;
            this._d81.Text = "Д81";
            this._d81.UseVisualStyleBackColor = true;
            // 
            // _d80
            // 
            this._d80.AutoSize = true;
            this._d80.Location = new System.Drawing.Point(342, 34);
            this._d80.Name = "_d80";
            this._d80.Size = new System.Drawing.Size(47, 17);
            this._d80.TabIndex = 79;
            this._d80.Text = "Д80";
            this._d80.UseVisualStyleBackColor = true;
            // 
            // _d79
            // 
            this._d79.AutoSize = true;
            this._d79.Location = new System.Drawing.Point(342, 19);
            this._d79.Name = "_d79";
            this._d79.Size = new System.Drawing.Size(47, 17);
            this._d79.TabIndex = 78;
            this._d79.Text = "Д79";
            this._d79.UseVisualStyleBackColor = true;
            // 
            // _d78
            // 
            this._d78.AutoSize = true;
            this._d78.Location = new System.Drawing.Point(342, 4);
            this._d78.Name = "_d78";
            this._d78.Size = new System.Drawing.Size(47, 17);
            this._d78.TabIndex = 77;
            this._d78.Text = "Д78";
            this._d78.UseVisualStyleBackColor = true;
            // 
            // _d77
            // 
            this._d77.AutoSize = true;
            this._d77.Location = new System.Drawing.Point(293, 154);
            this._d77.Name = "_d77";
            this._d77.Size = new System.Drawing.Size(47, 17);
            this._d77.TabIndex = 76;
            this._d77.Text = "Д77";
            this._d77.UseVisualStyleBackColor = true;
            // 
            // _d76
            // 
            this._d76.AutoSize = true;
            this._d76.Location = new System.Drawing.Point(293, 139);
            this._d76.Name = "_d76";
            this._d76.Size = new System.Drawing.Size(47, 17);
            this._d76.TabIndex = 75;
            this._d76.Text = "Д76";
            this._d76.UseVisualStyleBackColor = true;
            // 
            // _d75
            // 
            this._d75.AutoSize = true;
            this._d75.Location = new System.Drawing.Point(293, 124);
            this._d75.Name = "_d75";
            this._d75.Size = new System.Drawing.Size(47, 17);
            this._d75.TabIndex = 74;
            this._d75.Text = "Д75";
            this._d75.UseVisualStyleBackColor = true;
            // 
            // _d74
            // 
            this._d74.AutoSize = true;
            this._d74.Location = new System.Drawing.Point(293, 109);
            this._d74.Name = "_d74";
            this._d74.Size = new System.Drawing.Size(47, 17);
            this._d74.TabIndex = 73;
            this._d74.Text = "Д74";
            this._d74.UseVisualStyleBackColor = true;
            // 
            // _d73
            // 
            this._d73.AutoSize = true;
            this._d73.Location = new System.Drawing.Point(293, 94);
            this._d73.Name = "_d73";
            this._d73.Size = new System.Drawing.Size(47, 17);
            this._d73.TabIndex = 72;
            this._d73.Text = "Д73";
            this._d73.UseVisualStyleBackColor = true;
            // 
            // _d72
            // 
            this._d72.AutoSize = true;
            this._d72.Location = new System.Drawing.Point(293, 79);
            this._d72.Name = "_d72";
            this._d72.Size = new System.Drawing.Size(47, 17);
            this._d72.TabIndex = 71;
            this._d72.Text = "Д72";
            this._d72.UseVisualStyleBackColor = true;
            // 
            // _d71
            // 
            this._d71.AutoSize = true;
            this._d71.Location = new System.Drawing.Point(293, 64);
            this._d71.Name = "_d71";
            this._d71.Size = new System.Drawing.Size(47, 17);
            this._d71.TabIndex = 70;
            this._d71.Text = "Д71";
            this._d71.UseVisualStyleBackColor = true;
            // 
            // _d70
            // 
            this._d70.AutoSize = true;
            this._d70.Location = new System.Drawing.Point(293, 49);
            this._d70.Name = "_d70";
            this._d70.Size = new System.Drawing.Size(47, 17);
            this._d70.TabIndex = 69;
            this._d70.Text = "Д70";
            this._d70.UseVisualStyleBackColor = true;
            // 
            // _d69
            // 
            this._d69.AutoSize = true;
            this._d69.Location = new System.Drawing.Point(293, 34);
            this._d69.Name = "_d69";
            this._d69.Size = new System.Drawing.Size(47, 17);
            this._d69.TabIndex = 68;
            this._d69.Text = "Д69";
            this._d69.UseVisualStyleBackColor = true;
            // 
            // _d68
            // 
            this._d68.AutoSize = true;
            this._d68.Location = new System.Drawing.Point(293, 19);
            this._d68.Name = "_d68";
            this._d68.Size = new System.Drawing.Size(47, 17);
            this._d68.TabIndex = 67;
            this._d68.Text = "Д68";
            this._d68.UseVisualStyleBackColor = true;
            // 
            // _d67
            // 
            this._d67.AutoSize = true;
            this._d67.Location = new System.Drawing.Point(293, 4);
            this._d67.Name = "_d67";
            this._d67.Size = new System.Drawing.Size(47, 17);
            this._d67.TabIndex = 66;
            this._d67.Text = "Д67";
            this._d67.UseVisualStyleBackColor = true;
            // 
            // _d66
            // 
            this._d66.AutoSize = true;
            this._d66.Location = new System.Drawing.Point(244, 154);
            this._d66.Name = "_d66";
            this._d66.Size = new System.Drawing.Size(47, 17);
            this._d66.TabIndex = 65;
            this._d66.Text = "Д66";
            this._d66.UseVisualStyleBackColor = true;
            // 
            // _d65
            // 
            this._d65.AutoSize = true;
            this._d65.Location = new System.Drawing.Point(244, 139);
            this._d65.Name = "_d65";
            this._d65.Size = new System.Drawing.Size(47, 17);
            this._d65.TabIndex = 64;
            this._d65.Text = "Д65";
            this._d65.UseVisualStyleBackColor = true;
            // 
            // _d64
            // 
            this._d64.AutoSize = true;
            this._d64.Location = new System.Drawing.Point(244, 124);
            this._d64.Name = "_d64";
            this._d64.Size = new System.Drawing.Size(47, 17);
            this._d64.TabIndex = 63;
            this._d64.Text = "Д64";
            this._d64.UseVisualStyleBackColor = true;
            // 
            // _d63
            // 
            this._d63.AutoSize = true;
            this._d63.Location = new System.Drawing.Point(244, 109);
            this._d63.Name = "_d63";
            this._d63.Size = new System.Drawing.Size(47, 17);
            this._d63.TabIndex = 62;
            this._d63.Text = "Д63";
            this._d63.UseVisualStyleBackColor = true;
            // 
            // _d62
            // 
            this._d62.AutoSize = true;
            this._d62.Location = new System.Drawing.Point(244, 94);
            this._d62.Name = "_d62";
            this._d62.Size = new System.Drawing.Size(47, 17);
            this._d62.TabIndex = 61;
            this._d62.Text = "Д62";
            this._d62.UseVisualStyleBackColor = true;
            // 
            // _d61
            // 
            this._d61.AutoSize = true;
            this._d61.Location = new System.Drawing.Point(244, 79);
            this._d61.Name = "_d61";
            this._d61.Size = new System.Drawing.Size(47, 17);
            this._d61.TabIndex = 60;
            this._d61.Text = "Д61";
            this._d61.UseVisualStyleBackColor = true;
            // 
            // _d60
            // 
            this._d60.AutoSize = true;
            this._d60.Location = new System.Drawing.Point(244, 64);
            this._d60.Name = "_d60";
            this._d60.Size = new System.Drawing.Size(47, 17);
            this._d60.TabIndex = 59;
            this._d60.Text = "Д60";
            this._d60.UseVisualStyleBackColor = true;
            // 
            // _d59
            // 
            this._d59.AutoSize = true;
            this._d59.Location = new System.Drawing.Point(244, 49);
            this._d59.Name = "_d59";
            this._d59.Size = new System.Drawing.Size(47, 17);
            this._d59.TabIndex = 58;
            this._d59.Text = "Д59";
            this._d59.UseVisualStyleBackColor = true;
            // 
            // _d58
            // 
            this._d58.AutoSize = true;
            this._d58.Location = new System.Drawing.Point(244, 34);
            this._d58.Name = "_d58";
            this._d58.Size = new System.Drawing.Size(47, 17);
            this._d58.TabIndex = 57;
            this._d58.Text = "Д58";
            this._d58.UseVisualStyleBackColor = true;
            // 
            // _d57
            // 
            this._d57.AutoSize = true;
            this._d57.Location = new System.Drawing.Point(244, 19);
            this._d57.Name = "_d57";
            this._d57.Size = new System.Drawing.Size(47, 17);
            this._d57.TabIndex = 56;
            this._d57.Text = "Д57";
            this._d57.UseVisualStyleBackColor = true;
            // 
            // _d56
            // 
            this._d56.AutoSize = true;
            this._d56.Location = new System.Drawing.Point(244, 4);
            this._d56.Name = "_d56";
            this._d56.Size = new System.Drawing.Size(47, 17);
            this._d56.TabIndex = 55;
            this._d56.Text = "Д56";
            this._d56.UseVisualStyleBackColor = true;
            // 
            // _d55
            // 
            this._d55.AutoSize = true;
            this._d55.Location = new System.Drawing.Point(196, 154);
            this._d55.Name = "_d55";
            this._d55.Size = new System.Drawing.Size(47, 17);
            this._d55.TabIndex = 54;
            this._d55.Text = "Д55";
            this._d55.UseVisualStyleBackColor = true;
            // 
            // _d54
            // 
            this._d54.AutoSize = true;
            this._d54.Location = new System.Drawing.Point(196, 139);
            this._d54.Name = "_d54";
            this._d54.Size = new System.Drawing.Size(47, 17);
            this._d54.TabIndex = 53;
            this._d54.Text = "Д54";
            this._d54.UseVisualStyleBackColor = true;
            // 
            // _d53
            // 
            this._d53.AutoSize = true;
            this._d53.Location = new System.Drawing.Point(196, 124);
            this._d53.Name = "_d53";
            this._d53.Size = new System.Drawing.Size(47, 17);
            this._d53.TabIndex = 52;
            this._d53.Text = "Д53";
            this._d53.UseVisualStyleBackColor = true;
            // 
            // _d52
            // 
            this._d52.AutoSize = true;
            this._d52.Location = new System.Drawing.Point(196, 109);
            this._d52.Name = "_d52";
            this._d52.Size = new System.Drawing.Size(47, 17);
            this._d52.TabIndex = 51;
            this._d52.Text = "Д52";
            this._d52.UseVisualStyleBackColor = true;
            // 
            // _d51
            // 
            this._d51.AutoSize = true;
            this._d51.Location = new System.Drawing.Point(196, 94);
            this._d51.Name = "_d51";
            this._d51.Size = new System.Drawing.Size(47, 17);
            this._d51.TabIndex = 50;
            this._d51.Text = "Д51";
            this._d51.UseVisualStyleBackColor = true;
            // 
            // _d50
            // 
            this._d50.AutoSize = true;
            this._d50.Location = new System.Drawing.Point(196, 79);
            this._d50.Name = "_d50";
            this._d50.Size = new System.Drawing.Size(47, 17);
            this._d50.TabIndex = 49;
            this._d50.Text = "Д50";
            this._d50.UseVisualStyleBackColor = true;
            // 
            // _d49
            // 
            this._d49.AutoSize = true;
            this._d49.Location = new System.Drawing.Point(196, 64);
            this._d49.Name = "_d49";
            this._d49.Size = new System.Drawing.Size(47, 17);
            this._d49.TabIndex = 48;
            this._d49.Text = "Д49";
            this._d49.UseVisualStyleBackColor = true;
            // 
            // _d48
            // 
            this._d48.AutoSize = true;
            this._d48.Location = new System.Drawing.Point(196, 49);
            this._d48.Name = "_d48";
            this._d48.Size = new System.Drawing.Size(47, 17);
            this._d48.TabIndex = 47;
            this._d48.Text = "Д48";
            this._d48.UseVisualStyleBackColor = true;
            // 
            // _d47
            // 
            this._d47.AutoSize = true;
            this._d47.Location = new System.Drawing.Point(196, 34);
            this._d47.Name = "_d47";
            this._d47.Size = new System.Drawing.Size(47, 17);
            this._d47.TabIndex = 46;
            this._d47.Text = "Д47";
            this._d47.UseVisualStyleBackColor = true;
            // 
            // _d46
            // 
            this._d46.AutoSize = true;
            this._d46.Location = new System.Drawing.Point(196, 19);
            this._d46.Name = "_d46";
            this._d46.Size = new System.Drawing.Size(47, 17);
            this._d46.TabIndex = 45;
            this._d46.Text = "Д46";
            this._d46.UseVisualStyleBackColor = true;
            // 
            // _d45
            // 
            this._d45.AutoSize = true;
            this._d45.Location = new System.Drawing.Point(196, 4);
            this._d45.Name = "_d45";
            this._d45.Size = new System.Drawing.Size(47, 17);
            this._d45.TabIndex = 44;
            this._d45.Text = "Д45";
            this._d45.UseVisualStyleBackColor = true;
            // 
            // _d44
            // 
            this._d44.AutoSize = true;
            this._d44.Location = new System.Drawing.Point(147, 154);
            this._d44.Name = "_d44";
            this._d44.Size = new System.Drawing.Size(47, 17);
            this._d44.TabIndex = 43;
            this._d44.Text = "Д44";
            this._d44.UseVisualStyleBackColor = true;
            // 
            // _d43
            // 
            this._d43.AutoSize = true;
            this._d43.Location = new System.Drawing.Point(147, 139);
            this._d43.Name = "_d43";
            this._d43.Size = new System.Drawing.Size(47, 17);
            this._d43.TabIndex = 42;
            this._d43.Text = "Д43";
            this._d43.UseVisualStyleBackColor = true;
            // 
            // _d42
            // 
            this._d42.AutoSize = true;
            this._d42.Location = new System.Drawing.Point(147, 124);
            this._d42.Name = "_d42";
            this._d42.Size = new System.Drawing.Size(47, 17);
            this._d42.TabIndex = 41;
            this._d42.Text = "Д42";
            this._d42.UseVisualStyleBackColor = true;
            // 
            // _d41
            // 
            this._d41.AutoSize = true;
            this._d41.Location = new System.Drawing.Point(147, 109);
            this._d41.Name = "_d41";
            this._d41.Size = new System.Drawing.Size(47, 17);
            this._d41.TabIndex = 40;
            this._d41.Text = "Д41";
            this._d41.UseVisualStyleBackColor = true;
            // 
            // _d40
            // 
            this._d40.AutoSize = true;
            this._d40.Location = new System.Drawing.Point(147, 94);
            this._d40.Name = "_d40";
            this._d40.Size = new System.Drawing.Size(47, 17);
            this._d40.TabIndex = 39;
            this._d40.Text = "Д40";
            this._d40.UseVisualStyleBackColor = true;
            // 
            // _d39
            // 
            this._d39.AutoSize = true;
            this._d39.Location = new System.Drawing.Point(147, 79);
            this._d39.Name = "_d39";
            this._d39.Size = new System.Drawing.Size(47, 17);
            this._d39.TabIndex = 38;
            this._d39.Text = "Д39";
            this._d39.UseVisualStyleBackColor = true;
            // 
            // _d38
            // 
            this._d38.AutoSize = true;
            this._d38.Location = new System.Drawing.Point(147, 64);
            this._d38.Name = "_d38";
            this._d38.Size = new System.Drawing.Size(47, 17);
            this._d38.TabIndex = 37;
            this._d38.Text = "Д38";
            this._d38.UseVisualStyleBackColor = true;
            // 
            // _d37
            // 
            this._d37.AutoSize = true;
            this._d37.Location = new System.Drawing.Point(147, 49);
            this._d37.Name = "_d37";
            this._d37.Size = new System.Drawing.Size(47, 17);
            this._d37.TabIndex = 36;
            this._d37.Text = "Д37";
            this._d37.UseVisualStyleBackColor = true;
            // 
            // _d36
            // 
            this._d36.AutoSize = true;
            this._d36.Location = new System.Drawing.Point(147, 34);
            this._d36.Name = "_d36";
            this._d36.Size = new System.Drawing.Size(47, 17);
            this._d36.TabIndex = 35;
            this._d36.Text = "Д36";
            this._d36.UseVisualStyleBackColor = true;
            // 
            // _d35
            // 
            this._d35.AutoSize = true;
            this._d35.Location = new System.Drawing.Point(147, 19);
            this._d35.Name = "_d35";
            this._d35.Size = new System.Drawing.Size(47, 17);
            this._d35.TabIndex = 34;
            this._d35.Text = "Д35";
            this._d35.UseVisualStyleBackColor = true;
            // 
            // _d34
            // 
            this._d34.AutoSize = true;
            this._d34.Location = new System.Drawing.Point(147, 4);
            this._d34.Name = "_d34";
            this._d34.Size = new System.Drawing.Size(47, 17);
            this._d34.TabIndex = 33;
            this._d34.Text = "Д34";
            this._d34.UseVisualStyleBackColor = true;
            // 
            // _d33
            // 
            this._d33.AutoSize = true;
            this._d33.Location = new System.Drawing.Point(99, 154);
            this._d33.Name = "_d33";
            this._d33.Size = new System.Drawing.Size(47, 17);
            this._d33.TabIndex = 32;
            this._d33.Text = "Д33";
            this._d33.UseVisualStyleBackColor = true;
            // 
            // _d32
            // 
            this._d32.AutoSize = true;
            this._d32.Location = new System.Drawing.Point(99, 139);
            this._d32.Name = "_d32";
            this._d32.Size = new System.Drawing.Size(47, 17);
            this._d32.TabIndex = 31;
            this._d32.Text = "Д32";
            this._d32.UseVisualStyleBackColor = true;
            // 
            // _d31
            // 
            this._d31.AutoSize = true;
            this._d31.Location = new System.Drawing.Point(99, 124);
            this._d31.Name = "_d31";
            this._d31.Size = new System.Drawing.Size(47, 17);
            this._d31.TabIndex = 30;
            this._d31.Text = "Д31";
            this._d31.UseVisualStyleBackColor = true;
            // 
            // _d30
            // 
            this._d30.AutoSize = true;
            this._d30.Location = new System.Drawing.Point(99, 109);
            this._d30.Name = "_d30";
            this._d30.Size = new System.Drawing.Size(47, 17);
            this._d30.TabIndex = 29;
            this._d30.Text = "Д30";
            this._d30.UseVisualStyleBackColor = true;
            // 
            // _d29
            // 
            this._d29.AutoSize = true;
            this._d29.Location = new System.Drawing.Point(99, 94);
            this._d29.Name = "_d29";
            this._d29.Size = new System.Drawing.Size(47, 17);
            this._d29.TabIndex = 28;
            this._d29.Text = "Д29";
            this._d29.UseVisualStyleBackColor = true;
            // 
            // _d28
            // 
            this._d28.AutoSize = true;
            this._d28.Location = new System.Drawing.Point(99, 79);
            this._d28.Name = "_d28";
            this._d28.Size = new System.Drawing.Size(47, 17);
            this._d28.TabIndex = 27;
            this._d28.Text = "Д28";
            this._d28.UseVisualStyleBackColor = true;
            // 
            // _d27
            // 
            this._d27.AutoSize = true;
            this._d27.Location = new System.Drawing.Point(99, 64);
            this._d27.Name = "_d27";
            this._d27.Size = new System.Drawing.Size(47, 17);
            this._d27.TabIndex = 26;
            this._d27.Text = "Д27";
            this._d27.UseVisualStyleBackColor = true;
            // 
            // _d26
            // 
            this._d26.AutoSize = true;
            this._d26.Location = new System.Drawing.Point(99, 49);
            this._d26.Name = "_d26";
            this._d26.Size = new System.Drawing.Size(47, 17);
            this._d26.TabIndex = 25;
            this._d26.Text = "Д26";
            this._d26.UseVisualStyleBackColor = true;
            // 
            // _d25
            // 
            this._d25.AutoSize = true;
            this._d25.Location = new System.Drawing.Point(99, 34);
            this._d25.Name = "_d25";
            this._d25.Size = new System.Drawing.Size(47, 17);
            this._d25.TabIndex = 24;
            this._d25.Text = "Д25";
            this._d25.UseVisualStyleBackColor = true;
            // 
            // _d24
            // 
            this._d24.AutoSize = true;
            this._d24.Location = new System.Drawing.Point(99, 19);
            this._d24.Name = "_d24";
            this._d24.Size = new System.Drawing.Size(47, 17);
            this._d24.TabIndex = 23;
            this._d24.Text = "Д24";
            this._d24.UseVisualStyleBackColor = true;
            // 
            // _d23
            // 
            this._d23.AutoSize = true;
            this._d23.Location = new System.Drawing.Point(99, 4);
            this._d23.Name = "_d23";
            this._d23.Size = new System.Drawing.Size(47, 17);
            this._d23.TabIndex = 22;
            this._d23.Text = "Д23";
            this._d23.UseVisualStyleBackColor = true;
            // 
            // _d22
            // 
            this._d22.AutoSize = true;
            this._d22.Location = new System.Drawing.Point(50, 154);
            this._d22.Name = "_d22";
            this._d22.Size = new System.Drawing.Size(47, 17);
            this._d22.TabIndex = 21;
            this._d22.Text = "Д22";
            this._d22.UseVisualStyleBackColor = true;
            // 
            // _d21
            // 
            this._d21.AutoSize = true;
            this._d21.Location = new System.Drawing.Point(50, 139);
            this._d21.Name = "_d21";
            this._d21.Size = new System.Drawing.Size(47, 17);
            this._d21.TabIndex = 20;
            this._d21.Text = "Д21";
            this._d21.UseVisualStyleBackColor = true;
            // 
            // _d20
            // 
            this._d20.AutoSize = true;
            this._d20.Location = new System.Drawing.Point(50, 124);
            this._d20.Name = "_d20";
            this._d20.Size = new System.Drawing.Size(47, 17);
            this._d20.TabIndex = 19;
            this._d20.Text = "Д20";
            this._d20.UseVisualStyleBackColor = true;
            // 
            // _d19
            // 
            this._d19.AutoSize = true;
            this._d19.Location = new System.Drawing.Point(50, 109);
            this._d19.Name = "_d19";
            this._d19.Size = new System.Drawing.Size(47, 17);
            this._d19.TabIndex = 18;
            this._d19.Text = "Д19";
            this._d19.UseVisualStyleBackColor = true;
            // 
            // _d18
            // 
            this._d18.AutoSize = true;
            this._d18.Location = new System.Drawing.Point(50, 94);
            this._d18.Name = "_d18";
            this._d18.Size = new System.Drawing.Size(47, 17);
            this._d18.TabIndex = 17;
            this._d18.Text = "Д18";
            this._d18.UseVisualStyleBackColor = true;
            // 
            // _d17
            // 
            this._d17.AutoSize = true;
            this._d17.Location = new System.Drawing.Point(50, 79);
            this._d17.Name = "_d17";
            this._d17.Size = new System.Drawing.Size(47, 17);
            this._d17.TabIndex = 16;
            this._d17.Text = "Д17";
            this._d17.UseVisualStyleBackColor = true;
            // 
            // _d16
            // 
            this._d16.AutoSize = true;
            this._d16.Location = new System.Drawing.Point(50, 64);
            this._d16.Name = "_d16";
            this._d16.Size = new System.Drawing.Size(47, 17);
            this._d16.TabIndex = 15;
            this._d16.Text = "Д16";
            this._d16.UseVisualStyleBackColor = true;
            // 
            // _d15
            // 
            this._d15.AutoSize = true;
            this._d15.Location = new System.Drawing.Point(50, 49);
            this._d15.Name = "_d15";
            this._d15.Size = new System.Drawing.Size(47, 17);
            this._d15.TabIndex = 14;
            this._d15.Text = "Д15";
            this._d15.UseVisualStyleBackColor = true;
            // 
            // _d14
            // 
            this._d14.AutoSize = true;
            this._d14.Location = new System.Drawing.Point(50, 34);
            this._d14.Name = "_d14";
            this._d14.Size = new System.Drawing.Size(47, 17);
            this._d14.TabIndex = 13;
            this._d14.Text = "Д14";
            this._d14.UseVisualStyleBackColor = true;
            // 
            // _d13
            // 
            this._d13.AutoSize = true;
            this._d13.Location = new System.Drawing.Point(50, 19);
            this._d13.Name = "_d13";
            this._d13.Size = new System.Drawing.Size(47, 17);
            this._d13.TabIndex = 12;
            this._d13.Text = "Д13";
            this._d13.UseVisualStyleBackColor = true;
            // 
            // _d12
            // 
            this._d12.AutoSize = true;
            this._d12.Location = new System.Drawing.Point(50, 4);
            this._d12.Name = "_d12";
            this._d12.Size = new System.Drawing.Size(47, 17);
            this._d12.TabIndex = 11;
            this._d12.Text = "Д12";
            this._d12.UseVisualStyleBackColor = true;
            // 
            // _d11
            // 
            this._d11.AutoSize = true;
            this._d11.Location = new System.Drawing.Point(4, 154);
            this._d11.Name = "_d11";
            this._d11.Size = new System.Drawing.Size(47, 17);
            this._d11.TabIndex = 10;
            this._d11.Text = "Д11";
            this._d11.UseVisualStyleBackColor = true;
            // 
            // _d10
            // 
            this._d10.AutoSize = true;
            this._d10.Location = new System.Drawing.Point(4, 139);
            this._d10.Name = "_d10";
            this._d10.Size = new System.Drawing.Size(47, 17);
            this._d10.TabIndex = 9;
            this._d10.Text = "Д10";
            this._d10.UseVisualStyleBackColor = true;
            // 
            // _d9
            // 
            this._d9.AutoSize = true;
            this._d9.Location = new System.Drawing.Point(4, 124);
            this._d9.Name = "_d9";
            this._d9.Size = new System.Drawing.Size(41, 17);
            this._d9.TabIndex = 8;
            this._d9.Text = "Д9";
            this._d9.UseVisualStyleBackColor = true;
            // 
            // _d8
            // 
            this._d8.AutoSize = true;
            this._d8.Location = new System.Drawing.Point(4, 109);
            this._d8.Name = "_d8";
            this._d8.Size = new System.Drawing.Size(41, 17);
            this._d8.TabIndex = 7;
            this._d8.Text = "Д8";
            this._d8.UseVisualStyleBackColor = true;
            // 
            // _d7
            // 
            this._d7.AutoSize = true;
            this._d7.Location = new System.Drawing.Point(4, 94);
            this._d7.Name = "_d7";
            this._d7.Size = new System.Drawing.Size(41, 17);
            this._d7.TabIndex = 6;
            this._d7.Text = "Д7";
            this._d7.UseVisualStyleBackColor = true;
            // 
            // _d6
            // 
            this._d6.AutoSize = true;
            this._d6.Location = new System.Drawing.Point(4, 79);
            this._d6.Name = "_d6";
            this._d6.Size = new System.Drawing.Size(41, 17);
            this._d6.TabIndex = 5;
            this._d6.Text = "Д6";
            this._d6.UseVisualStyleBackColor = true;
            // 
            // _d5
            // 
            this._d5.AutoSize = true;
            this._d5.Location = new System.Drawing.Point(4, 64);
            this._d5.Name = "_d5";
            this._d5.Size = new System.Drawing.Size(41, 17);
            this._d5.TabIndex = 4;
            this._d5.Text = "Д5";
            this._d5.UseVisualStyleBackColor = true;
            // 
            // _d4
            // 
            this._d4.AutoSize = true;
            this._d4.Location = new System.Drawing.Point(4, 49);
            this._d4.Name = "_d4";
            this._d4.Size = new System.Drawing.Size(41, 17);
            this._d4.TabIndex = 3;
            this._d4.Text = "Д4";
            this._d4.UseVisualStyleBackColor = true;
            // 
            // _d3
            // 
            this._d3.AutoSize = true;
            this._d3.Location = new System.Drawing.Point(4, 34);
            this._d3.Name = "_d3";
            this._d3.Size = new System.Drawing.Size(41, 17);
            this._d3.TabIndex = 2;
            this._d3.Text = "Д3";
            this._d3.UseVisualStyleBackColor = true;
            // 
            // _d2
            // 
            this._d2.AutoSize = true;
            this._d2.Location = new System.Drawing.Point(4, 19);
            this._d2.Name = "_d2";
            this._d2.Size = new System.Drawing.Size(41, 17);
            this._d2.TabIndex = 1;
            this._d2.Text = "Д2";
            this._d2.UseVisualStyleBackColor = true;
            // 
            // _d1
            // 
            this._d1.AutoSize = true;
            this._d1.Location = new System.Drawing.Point(4, 4);
            this._d1.Name = "_d1";
            this._d1.Size = new System.Drawing.Size(41, 17);
            this._d1.TabIndex = 0;
            this._d1.Text = "Д1";
            this._d1.UseVisualStyleBackColor = true;
            // 
            // _time
            // 
            this._time.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._time.AutoSize = true;
            this._time.Location = new System.Drawing.Point(360, 530);
            this._time.Name = "_time";
            this._time.Size = new System.Drawing.Size(0, 13);
            this._time.TabIndex = 79;
            this._time.TextChanged += new System.EventHandler(this._time_TextChanged);
            // 
            // _labelStatus
            // 
            this._labelStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._labelStatus.AutoSize = true;
            this._labelStatus.Location = new System.Drawing.Point(44, 604);
            this._labelStatus.Name = "_labelStatus";
            this._labelStatus.Size = new System.Drawing.Size(136, 13);
            this._labelStatus.TabIndex = 81;
            this._labelStatus.Text = "Нет связи с устройством";
            // 
            // _kvit
            // 
            this._kvit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._kvit.Location = new System.Drawing.Point(305, 564);
            this._kvit.Name = "_kvit";
            this._kvit.Size = new System.Drawing.Size(140, 33);
            this._kvit.TabIndex = 82;
            this._kvit.Text = "Квитировать";
            this._kvit.UseVisualStyleBackColor = true;
            this._kvit.Click += new System.EventHandler(this._kvit_Click);
            // 
            // _exitEmulation
            // 
            this._exitEmulation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._exitEmulation.AutoSize = true;
            this._exitEmulation.Location = new System.Drawing.Point(152, 541);
            this._exitEmulation.Name = "_exitEmulation";
            this._exitEmulation.Size = new System.Drawing.Size(201, 17);
            this._exitEmulation.TabIndex = 83;
            this._exitEmulation.Text = "Не выходить из режима эмуляции";
            this._exitEmulation.UseVisualStyleBackColor = true;
            this._exitEmulation.CheckedChanged += new System.EventHandler(this._exitEmulation_CheckedChanged);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button1.Location = new System.Drawing.Point(451, 564);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 33);
            this.button1.TabIndex = 84;
            this.button1.Text = "To XML";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // _statusLedControl
            // 
            this._statusLedControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._statusLedControl.Location = new System.Drawing.Point(25, 604);
            this._statusLedControl.Name = "_statusLedControl";
            this._statusLedControl.Size = new System.Drawing.Size(13, 13);
            this._statusLedControl.State = BEMN.Forms.LedState.Off;
            this._statusLedControl.TabIndex = 80;
            // 
            // EmulationFormNew
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(589, 634);
            this.Controls.Add(this.button1);
            this.Controls.Add(this._getTimeGroupBox);
            this.Controls.Add(this._exitEmulation);
            this.Controls.Add(this._kvit);
            this.Controls.Add(this._labelStatus);
            this.Controls.Add(this._statusLedControl);
            this.Controls.Add(this._time);
            this.Controls.Add(this.groupBoxInputSignal);
            this.Controls.Add(this.label34);
            this.Controls.Add(this._writeEmulButton);
            this.Controls.Add(this.label33);
            this.Controls.Add(this._step);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label31);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "EmulationFormNew";
            this.Text = "EmulationForm";
            this.Activated += new System.EventHandler(this.EmulationForm_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.EmulationForm_FormClosed);
            this.Load += new System.EventHandler(this.EmulationForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this._step)).EndInit();
            this._getTimeGroupBox.ResumeLayout(false);
            this._getTimeGroupBox.PerformLayout();
            this.groupBoxDiskrets.ResumeLayout(false);
            this.panelDiskretsTime.ResumeLayout(false);
            this.panelDiskretsTime.PerformLayout();
            this.groupBoxInputSignal.ResumeLayout(false);
            this.groupBoxInputDiskrets.ResumeLayout(false);
            this.panelDiscrets.ResumeLayout(false);
            this.panelDiscrets.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox _startTime;
        private System.Windows.Forms.ComboBox _signal;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.MaskedTextBox _status;
        private System.Windows.Forms.NumericUpDown _step;
        private System.Windows.Forms.GroupBox _getTimeGroupBox;
        private System.Windows.Forms.GroupBox groupBoxDiskrets;
        private System.Windows.Forms.CheckBox _writeEmulButton;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.GroupBox groupBoxInputSignal;
        private System.Windows.Forms.GroupBox groupBoxInputDiskrets;
        private System.Windows.Forms.Label _timeSignal;
        private System.Windows.Forms.Label _time;
        private Forms.LedControl _statusLedControl;
        private System.Windows.Forms.Label _labelStatus;
        private System.Windows.Forms.Button _kvit;
        private System.Windows.Forms.ToolTip kvitTooltip;
        private System.Windows.Forms.CheckBox _exitEmulation;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panelDiscrets;
        private System.Windows.Forms.Panel panelDiskretsTime;
        private System.Windows.Forms.CheckBox _d4;
        private System.Windows.Forms.CheckBox _d3;
        private System.Windows.Forms.CheckBox _d2;
        private System.Windows.Forms.CheckBox _d1;
        private System.Windows.Forms.CheckBox _k2;
        private System.Windows.Forms.CheckBox _k1;
        private System.Windows.Forms.CheckBox _d110;
        private System.Windows.Forms.CheckBox _d109;
        private System.Windows.Forms.CheckBox _d108;
        private System.Windows.Forms.CheckBox _d107;
        private System.Windows.Forms.CheckBox _d106;
        private System.Windows.Forms.CheckBox _d105;
        private System.Windows.Forms.CheckBox _d104;
        private System.Windows.Forms.CheckBox _d103;
        private System.Windows.Forms.CheckBox _d102;
        private System.Windows.Forms.CheckBox _d101;
        private System.Windows.Forms.CheckBox _d100;
        private System.Windows.Forms.CheckBox _d99;
        private System.Windows.Forms.CheckBox _d98;
        private System.Windows.Forms.CheckBox _d97;
        private System.Windows.Forms.CheckBox _d96;
        private System.Windows.Forms.CheckBox _d95;
        private System.Windows.Forms.CheckBox _d94;
        private System.Windows.Forms.CheckBox _d93;
        private System.Windows.Forms.CheckBox _d92;
        private System.Windows.Forms.CheckBox _d91;
        private System.Windows.Forms.CheckBox _d90;
        private System.Windows.Forms.CheckBox _d89;
        private System.Windows.Forms.CheckBox _d88;
        private System.Windows.Forms.CheckBox _d87;
        private System.Windows.Forms.CheckBox _d86;
        private System.Windows.Forms.CheckBox _d85;
        private System.Windows.Forms.CheckBox _d84;
        private System.Windows.Forms.CheckBox _d83;
        private System.Windows.Forms.CheckBox _d82;
        private System.Windows.Forms.CheckBox _d81;
        private System.Windows.Forms.CheckBox _d80;
        private System.Windows.Forms.CheckBox _d79;
        private System.Windows.Forms.CheckBox _d78;
        private System.Windows.Forms.CheckBox _d77;
        private System.Windows.Forms.CheckBox _d76;
        private System.Windows.Forms.CheckBox _d75;
        private System.Windows.Forms.CheckBox _d74;
        private System.Windows.Forms.CheckBox _d73;
        private System.Windows.Forms.CheckBox _d72;
        private System.Windows.Forms.CheckBox _d71;
        private System.Windows.Forms.CheckBox _d70;
        private System.Windows.Forms.CheckBox _d69;
        private System.Windows.Forms.CheckBox _d68;
        private System.Windows.Forms.CheckBox _d67;
        private System.Windows.Forms.CheckBox _d66;
        private System.Windows.Forms.CheckBox _d65;
        private System.Windows.Forms.CheckBox _d64;
        private System.Windows.Forms.CheckBox _d63;
        private System.Windows.Forms.CheckBox _d62;
        private System.Windows.Forms.CheckBox _d61;
        private System.Windows.Forms.CheckBox _d60;
        private System.Windows.Forms.CheckBox _d59;
        private System.Windows.Forms.CheckBox _d58;
        private System.Windows.Forms.CheckBox _d57;
        private System.Windows.Forms.CheckBox _d56;
        private System.Windows.Forms.CheckBox _d55;
        private System.Windows.Forms.CheckBox _d54;
        private System.Windows.Forms.CheckBox _d53;
        private System.Windows.Forms.CheckBox _d52;
        private System.Windows.Forms.CheckBox _d51;
        private System.Windows.Forms.CheckBox _d50;
        private System.Windows.Forms.CheckBox _d49;
        private System.Windows.Forms.CheckBox _d48;
        private System.Windows.Forms.CheckBox _d47;
        private System.Windows.Forms.CheckBox _d46;
        private System.Windows.Forms.CheckBox _d45;
        private System.Windows.Forms.CheckBox _d44;
        private System.Windows.Forms.CheckBox _d43;
        private System.Windows.Forms.CheckBox _d42;
        private System.Windows.Forms.CheckBox _d41;
        private System.Windows.Forms.CheckBox _d40;
        private System.Windows.Forms.CheckBox _d39;
        private System.Windows.Forms.CheckBox _d38;
        private System.Windows.Forms.CheckBox _d37;
        private System.Windows.Forms.CheckBox _d36;
        private System.Windows.Forms.CheckBox _d35;
        private System.Windows.Forms.CheckBox _d34;
        private System.Windows.Forms.CheckBox _d33;
        private System.Windows.Forms.CheckBox _d32;
        private System.Windows.Forms.CheckBox _d31;
        private System.Windows.Forms.CheckBox _d30;
        private System.Windows.Forms.CheckBox _d29;
        private System.Windows.Forms.CheckBox _d28;
        private System.Windows.Forms.CheckBox _d27;
        private System.Windows.Forms.CheckBox _d26;
        private System.Windows.Forms.CheckBox _d25;
        private System.Windows.Forms.CheckBox _d24;
        private System.Windows.Forms.CheckBox _d23;
        private System.Windows.Forms.CheckBox _d22;
        private System.Windows.Forms.CheckBox _d21;
        private System.Windows.Forms.CheckBox _d20;
        private System.Windows.Forms.CheckBox _d19;
        private System.Windows.Forms.CheckBox _d18;
        private System.Windows.Forms.CheckBox _d17;
        private System.Windows.Forms.CheckBox _d16;
        private System.Windows.Forms.CheckBox _d15;
        private System.Windows.Forms.CheckBox _d14;
        private System.Windows.Forms.CheckBox _d13;
        private System.Windows.Forms.CheckBox _d12;
        private System.Windows.Forms.CheckBox _d11;
        private System.Windows.Forms.CheckBox _d10;
        private System.Windows.Forms.CheckBox _d9;
        private System.Windows.Forms.CheckBox _d8;
        private System.Windows.Forms.CheckBox _d7;
        private System.Windows.Forms.CheckBox _d6;
        private System.Windows.Forms.CheckBox _d5;
        private System.Windows.Forms.CheckBox _k2list;
        private System.Windows.Forms.CheckBox _k1list;
        private System.Windows.Forms.CheckBox _d110list;
        private System.Windows.Forms.CheckBox _d109list;
        private System.Windows.Forms.CheckBox _d108list;
        private System.Windows.Forms.CheckBox _d107list;
        private System.Windows.Forms.CheckBox _d106list;
        private System.Windows.Forms.CheckBox _d105list;
        private System.Windows.Forms.CheckBox _d104list;
        private System.Windows.Forms.CheckBox _d103list;
        private System.Windows.Forms.CheckBox _d102list;
        private System.Windows.Forms.CheckBox _d101list;
        private System.Windows.Forms.CheckBox _d100list;
        private System.Windows.Forms.CheckBox _d99list;
        private System.Windows.Forms.CheckBox _d98list;
        private System.Windows.Forms.CheckBox _d97list;
        private System.Windows.Forms.CheckBox _d96list;
        private System.Windows.Forms.CheckBox _d95list;
        private System.Windows.Forms.CheckBox _d94list;
        private System.Windows.Forms.CheckBox _d93list;
        private System.Windows.Forms.CheckBox _d92list;
        private System.Windows.Forms.CheckBox _d91list;
        private System.Windows.Forms.CheckBox _d90list;
        private System.Windows.Forms.CheckBox _d89list;
        private System.Windows.Forms.CheckBox _d88list;
        private System.Windows.Forms.CheckBox _d87list;
        private System.Windows.Forms.CheckBox _d86list;
        private System.Windows.Forms.CheckBox _d85list;
        private System.Windows.Forms.CheckBox _d84list;
        private System.Windows.Forms.CheckBox _d83list;
        private System.Windows.Forms.CheckBox _d82list;
        private System.Windows.Forms.CheckBox _d81list;
        private System.Windows.Forms.CheckBox _d80list;
        private System.Windows.Forms.CheckBox _d79list;
        private System.Windows.Forms.CheckBox _d78list;
        private System.Windows.Forms.CheckBox _d77list;
        private System.Windows.Forms.CheckBox _d76list;
        private System.Windows.Forms.CheckBox _d75list;
        private System.Windows.Forms.CheckBox _d74list;
        private System.Windows.Forms.CheckBox _d73list;
        private System.Windows.Forms.CheckBox _d72list;
        private System.Windows.Forms.CheckBox _d71list;
        private System.Windows.Forms.CheckBox _d70list;
        private System.Windows.Forms.CheckBox _d69list;
        private System.Windows.Forms.CheckBox _d68list;
        private System.Windows.Forms.CheckBox _d67list;
        private System.Windows.Forms.CheckBox _d66list;
        private System.Windows.Forms.CheckBox _d65list;
        private System.Windows.Forms.CheckBox _d64list;
        private System.Windows.Forms.CheckBox _d63list;
        private System.Windows.Forms.CheckBox _d62list;
        private System.Windows.Forms.CheckBox _d61list;
        private System.Windows.Forms.CheckBox _d60list;
        private System.Windows.Forms.CheckBox _d59list;
        private System.Windows.Forms.CheckBox _d58list;
        private System.Windows.Forms.CheckBox _d57list;
        private System.Windows.Forms.CheckBox _d56list;
        private System.Windows.Forms.CheckBox _d55list;
        private System.Windows.Forms.CheckBox _d54list;
        private System.Windows.Forms.CheckBox _d53list;
        private System.Windows.Forms.CheckBox _d52list;
        private System.Windows.Forms.CheckBox _d51list;
        private System.Windows.Forms.CheckBox _d50list;
        private System.Windows.Forms.CheckBox _d49list;
        private System.Windows.Forms.CheckBox _d48list;
        private System.Windows.Forms.CheckBox _d47list;
        private System.Windows.Forms.CheckBox _d46list;
        private System.Windows.Forms.CheckBox _d45list;
        private System.Windows.Forms.CheckBox _d44list;
        private System.Windows.Forms.CheckBox _d43list;
        private System.Windows.Forms.CheckBox _d42list;
        private System.Windows.Forms.CheckBox _d41list;
        private System.Windows.Forms.CheckBox _d40list;
        private System.Windows.Forms.CheckBox _d39list;
        private System.Windows.Forms.CheckBox _d38list;
        private System.Windows.Forms.CheckBox _d37list;
        private System.Windows.Forms.CheckBox _d36list;
        private System.Windows.Forms.CheckBox _d35list;
        private System.Windows.Forms.CheckBox _d34list;
        private System.Windows.Forms.CheckBox _d33list;
        private System.Windows.Forms.CheckBox _d32list;
        private System.Windows.Forms.CheckBox _d31list;
        private System.Windows.Forms.CheckBox _d30list;
        private System.Windows.Forms.CheckBox _d29list;
        private System.Windows.Forms.CheckBox _d28list;
        private System.Windows.Forms.CheckBox _d27list;
        private System.Windows.Forms.CheckBox _d26list;
        private System.Windows.Forms.CheckBox _d25list;
        private System.Windows.Forms.CheckBox _d24list;
        private System.Windows.Forms.CheckBox _d23list;
        private System.Windows.Forms.CheckBox _d22list;
        private System.Windows.Forms.CheckBox _d21list;
        private System.Windows.Forms.CheckBox _d20list;
        private System.Windows.Forms.CheckBox _d19list;
        private System.Windows.Forms.CheckBox _d18list;
        private System.Windows.Forms.CheckBox _d17list;
        private System.Windows.Forms.CheckBox _d16list;
        private System.Windows.Forms.CheckBox _d15list;
        private System.Windows.Forms.CheckBox _d14list;
        private System.Windows.Forms.CheckBox _d13list;
        private System.Windows.Forms.CheckBox _d12list;
        private System.Windows.Forms.CheckBox _d11list;
        private System.Windows.Forms.CheckBox _d10list;
        private System.Windows.Forms.CheckBox _d9list;
        private System.Windows.Forms.CheckBox _d8list;
        private System.Windows.Forms.CheckBox _d7list;
        private System.Windows.Forms.CheckBox _d6list;
        private System.Windows.Forms.CheckBox _d5list;
        private System.Windows.Forms.CheckBox _d4list;
        private System.Windows.Forms.CheckBox _d3list;
        private System.Windows.Forms.CheckBox _d2list;
        private System.Windows.Forms.CheckBox _d1list;
        private System.Windows.Forms.CheckBox _d112;
        private System.Windows.Forms.CheckBox _d111;
        private System.Windows.Forms.CheckBox _d112list;
        private System.Windows.Forms.CheckBox _d111list;
    }
}