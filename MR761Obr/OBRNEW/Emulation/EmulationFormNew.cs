﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR761Obr;
using BEMN.MR761Obr.OBRNEW.Configuration;
using BEMN.MR761Obr.Properties;
using BEMN.MR761OBR.OBRNEW.Emulation.Structures;

namespace BEMN.MR761OBR.OBRNEW.Emulation
{
    public partial class EmulationFormNew : Form, IFormView
    {
        private const string READ_FAIL = "Не удалось прочитать эмуляцию.";
        private const string WRITE_FAIL = "Не удалось записать эмуляцию.";
        
        private int _heightGpUTime;
        private int _heightGpAnalog;
        private int _heightGpDisTime;
        private int _heightGpItime;

        #region [Private fields]

        private readonly Mr761ObrDevice _device;

        private bool _isStart;
        private bool _isSelect;
        private bool _isWrite;
        private bool _isChange;
        private bool _isKvit;
        private bool _isExit;
        private bool _isBlock;
        private double _incriment;

        private List<Button> _listButtons;
        private MaskedTextBox _currentAdduction;
        private MemoryEntity<WriteStructEmul> _memoryEntityStructEmulation;
        private MemoryEntity<WriteStructEmul> _memoryEntityNullStructEmulation;
        private MemoryEntity<ReadStructEmul> _readMemoryEntity;

        private NewStructValidator<WriteStructEmul> _validatorEmulationStruct;
        private NewStructValidator<TimeAndSignalStruct> _validatorTimeAndSignal;
        private NewStructValidator<DiscretsSignals> _discrets;

        private WriteStructEmul _currentWriteStruct;
        private WriteStructEmul _tempWriteStruct;
        

        private List<CheckBox> _listStartControls;
        private List<CheckBox> _listDiscretsControls;
        private List<MaskedTextBox> _listAllControls;
        #endregion

        public EmulationFormNew()
        {
            this.InitializeComponent();
        }

        public EmulationFormNew(Mr761ObrDevice device)
        {
            try
            {
                this.InitializeComponent();
                this._device = device;

                StringsConfigNew.CurrentVersion = Common.VersionConverter(this._device.DeviceVersion);
                StringsConfigNew.DeviceType = this._device.Info.DeviceConfiguration;

                this._memoryEntityStructEmulation = this._device.WriteStructEmulation;
                this._memoryEntityNullStructEmulation = this._device.WriteStructEmulationNull;
                this._readMemoryEntity = this._device.ReadStructEmulation;

                this._memoryEntityStructEmulation.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.EmulationReadOk);
                this._memoryEntityStructEmulation.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.EmulationWriteOk);
                this._memoryEntityStructEmulation.WriteFail += HandlerHelper.CreateHandler(this, () =>
                {
                    this._memoryEntityStructEmulation.RemoveStructQueries();
                    this.EmulationWriteFail();
                });
                this._memoryEntityStructEmulation.ReadFail += HandlerHelper.CreateHandler(this, () =>
                {
                    this._memoryEntityStructEmulation.RemoveStructQueries();
                    this.EmulationReadFail();
                });

                this._memoryEntityNullStructEmulation.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.NullEmulationReadOk);
                this._memoryEntityNullStructEmulation.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.EmulationWriteOk);
                this._memoryEntityNullStructEmulation.WriteFail += HandlerHelper.CreateHandler(this, () =>
                {
                    this._memoryEntityNullStructEmulation.RemoveStructQueries();
                    this.EmulationWriteFail();
                    this._kvit.Enabled = true;
                });
                this._memoryEntityNullStructEmulation.ReadFail += HandlerHelper.CreateHandler(this, () =>
                {
                    this._memoryEntityStructEmulation.RemoveStructQueries();
                    this.EmulationReadFail();
                    this._kvit.Enabled = true;
                });
                
                this._readMemoryEntity.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadOk);
                this._readMemoryEntity.ReadFail += HandlerHelper.CreateHandler(this, () =>
                {
                    this._status.Text = "";
                    this.EmulationReadFail();
                });
                this._currentWriteStruct = new WriteStructEmul();

                if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode)
                {
                    _device.Info.DeviceConfiguration = _device.DevicePlant;
                }

                this.Init();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            
        }

        private void Init()
        {
            this.kvitTooltip.SetToolTip(this._kvit, "Применяется для сброса углов при изменении частоты");

            #region [ListCotrols]
            
            this._listDiscretsControls = new List<CheckBox>
            {
                this._d1, this._d2, this._d3, this._d4 ,this._d5, this._d6, this._d7, this._d8, this._d9, this._d10, this._d11, this._d12, this._d13,this._d14, this._d15,
                this._d16, this._d17, this._d18, this._d19, this._d20, this._d21, this._d22, this._d23, this._d24, this._d25, this._d26, this._d27, this._d28, this._d29,
                this._d30, this._d31, this._d32, this._d33, this._d34, this._d35, this._d36, this._d37, this._d38, this._d39, this._d40, this._d41, this._d42, this._d43,
                this._d44, this._d45, this._d46, this._d46, this._d47, this._d48, this._d49, this._d50, this._d51, this._d52, this._d53, this._d54, this._d55, this._d56,
                this._d57, this._d58, this._d59, this._d60, this._d61, this._d62, this._d63, this._d64, this._d65, this._d66, this._d67, this._d68, this._d69, this._d70,
                this._d71, this._d72, this._d73, this._d74, this._d75, this._d76, this._d77, this._d78, this._d79, this._d80, this._d81, this._d82, this._d83, this._d84,
                this._d85, this._d86, this._d87, this._d88, this._d89, this._d90, this._d91, this._d92, this._d93, this._d94, this._d95, this._d96, this._d97, this._d98,
                this._d99, this._d100, this._d101, this._d102, this._d103, this._d104, this._d105, this._d106, this._d107, this._d108, this._d109, this._d110, this._d111,
                this._d112, this._k1, this._k2
            };

            foreach (var control in this._listDiscretsControls)
            {
                control.CheckedChanged += this._discret_CheckedChanged;
            }

            this._listStartControls = new List<CheckBox>
            {
                this._d1list, this._d2list, this._d3list, this._d4list, this._d5list, this._d6list, this._d7list, this._d8list, this._d9list, this._d10list, this._d11list,
                this._d12list, this._d13list, this._d14list, this._d15list, this._d16list, this._d17list, this._d18list, this._d19list, this._d20list, this._d21list,
                this._d22list, this._d23list, this._d24list, this._d25list, this._d26list, this._d27list, this._d28list, this._d29list, this._d30list, this._d31list,
                this._d32list, this._d33list, this._d34list, this._d35list, this._d36list, this._d37list, this._d38list, this._d39list, this._d40list, this._d41list,
                this._d42list, this._d43list, this._d44list, this._d45list, this._d46list, this._d46list, this._d47list, this._d48list, this._d49list, this._d50list,
                this._d51list, this._d52list, this._d53list, this._d54list, this._d55list, this._d56list, this._d57list, this._d58list, this._d59list, this._d60list,
                this._d61list, this._d62list, this._d63list, this._d64list, this._d65list, this._d66list, this._d67list, this._d68list, this._d69list, this._d70list,
                this._d71list, this._d72list, this._d73list, this._d74list, this._d75list, this._d76list, this._d77list, this._d78list, this._d79list, this._d80list,
                this._d81list, this._d82list, this._d83list, this._d84list, this._d85list, this._d86list, this._d87list, this._d88list, this._d89list, this._d90list,
                this._d91list, this._d92list, this._d93list, this._d94list, this._d95list, this._d96list, this._d97list, this._d98list, this._d99list, this._d100list,
                this._d101list, this._d102list, this._d103list, this._d104list, this._d105list, this._d106list, this._d107list, this._d108list, this._d109list,
                this._d110list, this._d111list, this._d112list, this._k1list, this._k2list
            };

            foreach (var control in this._listStartControls)
            {
                control.Enabled = false;
            }

            this._discrets = new NewStructValidator<DiscretsSignals>(this.toolTip1,
                new ControlInfoCheck(this._d1),
                new ControlInfoCheck(this._d2),
                new ControlInfoCheck(this._d3),
                new ControlInfoCheck(this._d4),
                new ControlInfoCheck(this._d5),
                new ControlInfoCheck(this._d6),
                new ControlInfoCheck(this._d7),
                new ControlInfoCheck(this._d8),
                new ControlInfoCheck(this._d9),
                new ControlInfoCheck(this._d10),
                new ControlInfoCheck(this._d11),
                new ControlInfoCheck(this._d12),
                new ControlInfoCheck(this._d13),
                new ControlInfoCheck(this._d14),
                new ControlInfoCheck(this._d15),
                new ControlInfoCheck(this._d16),
                new ControlInfoCheck(this._d17),
                new ControlInfoCheck(this._d18),
                new ControlInfoCheck(this._d19),
                new ControlInfoCheck(this._d20),
                new ControlInfoCheck(this._d21),
                new ControlInfoCheck(this._d22),
                new ControlInfoCheck(this._d23),
                new ControlInfoCheck(this._d24),
                new ControlInfoCheck(this._d25),
                new ControlInfoCheck(this._d26),
                new ControlInfoCheck(this._d27),
                new ControlInfoCheck(this._d28),
                new ControlInfoCheck(this._d29),
                new ControlInfoCheck(this._d30),
                new ControlInfoCheck(this._d31),
                new ControlInfoCheck(this._d32),
                new ControlInfoCheck(this._d33),
                new ControlInfoCheck(this._d34),
                new ControlInfoCheck(this._d35),
                new ControlInfoCheck(this._d36),
                new ControlInfoCheck(this._d37),
                new ControlInfoCheck(this._d38),
                new ControlInfoCheck(this._d39),
                new ControlInfoCheck(this._d40),
                new ControlInfoCheck(this._d41),
                new ControlInfoCheck(this._d42),
                new ControlInfoCheck(this._d43),
                new ControlInfoCheck(this._d44),
                new ControlInfoCheck(this._d45),
                new ControlInfoCheck(this._d46),
                new ControlInfoCheck(this._d47),
                new ControlInfoCheck(this._d48),
                new ControlInfoCheck(this._d49),
                new ControlInfoCheck(this._d50),
                new ControlInfoCheck(this._d51),
                new ControlInfoCheck(this._d52),
                new ControlInfoCheck(this._d53),
                new ControlInfoCheck(this._d54),
                new ControlInfoCheck(this._d55),
                new ControlInfoCheck(this._d56),
                new ControlInfoCheck(this._d57),
                new ControlInfoCheck(this._d58),
                new ControlInfoCheck(this._d59),
                new ControlInfoCheck(this._d60),
                new ControlInfoCheck(this._d61),
                new ControlInfoCheck(this._d62),
                new ControlInfoCheck(this._d63),
                new ControlInfoCheck(this._d64),
                new ControlInfoCheck(this._d65),
                new ControlInfoCheck(this._d66),
                new ControlInfoCheck(this._d67),
                new ControlInfoCheck(this._d68),
                new ControlInfoCheck(this._d69),
                new ControlInfoCheck(this._d70),
                new ControlInfoCheck(this._d71),
                new ControlInfoCheck(this._d72),
                new ControlInfoCheck(this._d73),
                new ControlInfoCheck(this._d74),
                new ControlInfoCheck(this._d75),
                new ControlInfoCheck(this._d76),
                new ControlInfoCheck(this._d77),
                new ControlInfoCheck(this._d78),
                new ControlInfoCheck(this._d79),
                new ControlInfoCheck(this._d80),
                new ControlInfoCheck(this._d81),
                new ControlInfoCheck(this._d82),
                new ControlInfoCheck(this._d83),
                new ControlInfoCheck(this._d84),
                new ControlInfoCheck(this._d85),
                new ControlInfoCheck(this._d86),
                new ControlInfoCheck(this._d87),
                new ControlInfoCheck(this._d88),
                new ControlInfoCheck(this._d89),
                new ControlInfoCheck(this._d90),
                new ControlInfoCheck(this._d91),
                new ControlInfoCheck(this._d92),
                new ControlInfoCheck(this._d93),
                new ControlInfoCheck(this._d94),
                new ControlInfoCheck(this._d95),
                new ControlInfoCheck(this._d96),
                new ControlInfoCheck(this._d97),
                new ControlInfoCheck(this._d98),
                new ControlInfoCheck(this._d99),
                new ControlInfoCheck(this._d100),
                new ControlInfoCheck(this._d101),
                new ControlInfoCheck(this._d102),
                new ControlInfoCheck(this._d103),
                new ControlInfoCheck(this._d104),
                new ControlInfoCheck(this._d105),
                new ControlInfoCheck(this._d106),
                new ControlInfoCheck(this._d107),
                new ControlInfoCheck(this._d108),
                new ControlInfoCheck(this._d109),
                new ControlInfoCheck(this._d110),
                new ControlInfoCheck(this._d111),
                new ControlInfoCheck(this._d112),
                new ControlInfoCheck(this._k1),
                new ControlInfoCheck(this._k2)
                );

            #endregion

            this._startTime.Enabled = false;

            this._validatorEmulationStruct = new NewStructValidator<WriteStructEmul>
            (this.toolTip1,

                new ControlInfoValidator(this._validatorTimeAndSignal),
                new ControlInfoValidator(this._discrets)
            );

            this._validatorTimeAndSignal = new NewStructValidator<TimeAndSignalStruct>
            (
                this.toolTip1,
                new ControlInfoCheck(this._startTime),
                new ControlInfoCombo(this._signal, StringsConfigNew.RelaySignals)
            );
            
            this.InitStartControls();
            this.Enabledcontrols();
        }

        private void Enabledcontrols()
        {
            switch (StringsConfigNew.DeviceType)
            {
                case "T0N0D74R35":
                    this._d73.Text = "K1";
                    this._d74.Text = "K2";
                    this._d75.Visible = false;
                    this._d76.Visible = false;
                    this._d77.Visible = false;
                    this._d78.Visible = false;
                    this._d79.Visible = false;
                    this._d80.Visible = false;
                    this._d81.Visible = false;
                    this._d82.Visible = false;
                    this._d83.Visible = false;
                    this._d84.Visible = false;
                    this._d85.Visible = false;
                    this._d86.Visible = false;
                    this._d87.Visible = false;
                    this._d88.Visible = false;
                    this._d89.Visible = false;
                    this._d90.Visible = false;
                    this._d91.Visible = false;
                    this._d92.Visible = false;
                    this._d93.Visible = false;
                    this._d94.Visible = false;
                    this._d95.Visible = false;
                    this._d96.Visible = false;
                    this._d97.Visible = false;
                    this._d98.Visible = false;
                    this._d99.Visible = false;
                    this._d100.Visible = false;
                    this._d101.Visible = false;
                    this._d102.Visible = false;
                    this._d103.Visible = false;
                    this._d104.Visible = false;
                    this._d105.Visible = false;
                    this._d106.Visible = false;
                    this._d107.Visible = false;
                    this._d108.Visible = false;
                    this._d109.Visible = false;
                    this._d110.Visible = false;
                    this._d111.Visible = false;
                    this._d112.Visible = false;
                    this.Size = new Size(470, 677);
                    this.groupBoxInputDiskrets.Size = new Size(430, 190);
                    this.groupBoxDiskrets.Size = new Size(430, 190);
                    break;
                case "T0N0D106R67":
                    this._d104.Text = "K1";
                    this._d105.Text = "K2";
                    this._d106.Visible = false;
                    this._d107.Visible = false;
                    this._d108.Visible = false;
                    this._d109.Visible = false;
                    this._d110.Visible = false;
                    this._d111.Visible = false;
                    this._d112.Visible = false;
                    this.Size = new Size(531, 677);
                    this.groupBoxInputDiskrets.Size = new Size(498, 191);
                    this.groupBoxDiskrets.Size = new Size(498, 191);
                    break;
                default:
                    break;
            }
        }

        private void InitStartControls()
        {
            int count = 0;
            for (int i = 0; i < this._listDiscretsControls.Count; i++)
            {
                this._listDiscretsControls[i].Tag = this._listStartControls[count];
                count++;
            }
        }
        
        private void _kvit_Click(object sender, EventArgs e)
        {
            this._kvit.Enabled = false;
            this._memoryEntityNullStructEmulation.LoadStruct();
            
        }

        private void NullEmulationReadOk()
        {
            try
            {
                this._isKvit = true;

                this._tempWriteStruct = this._memoryEntityNullStructEmulation.Value;

                WriteStructEmul nul = new WriteStructEmul();
                nul.TimeSignal = new TimeAndSignalStruct();
                nul.TimeSignal.IsRestart = true;
                nul.DiscretInputs = new DiscretsSignals();
                
                this._memoryEntityNullStructEmulation.Value = nul;
                this._memoryEntityNullStructEmulation.SaveStruct();
            }
            catch (Exception)
            {
                this._isKvit = false;
            }
        }

        private void WriteEmulation()
        {
            if (this._isKvit)
            {
                this._isKvit = false;
                this._currentWriteStruct = this._tempWriteStruct;
            }
            else
            {
                this._currentWriteStruct.TimeSignal = _validatorTimeAndSignal.Get();
                this._currentWriteStruct.DiscretInputs = this._discrets.Get();
            }

            if (this._isSelect)
            {
                if (this._isStart)
                {
                    this._currentWriteStruct.TimeSignal.StatusTime = true;
                }

            }
            this._currentWriteStruct.TimeSignal.IsRestart = false;
            
            this._memoryEntityStructEmulation.Value = this._currentWriteStruct;
            this._memoryEntityStructEmulation.SaveStruct(new TimeSpan(1000));
            
        }
        
        private void EmulationReadOk()
        {
            if (this._isStart || this._memoryEntityStructEmulation.Value.TimeSignal.StatusTime)
            {
                this._memoryEntityStructEmulation.Value.TimeSignal.WriteTimeOk = true;
            }
            this._currentWriteStruct = this._memoryEntityStructEmulation.Value;
            this._validatorTimeAndSignal.Set(_currentWriteStruct.TimeSignal);
            this._discrets.Set(_currentWriteStruct.DiscretInputs);
        }

        private void EmulationWriteOk()
        {
            if (_isBlock = true)
            {

            }
            if (this._isKvit)
            {
                this._kvit.Enabled = true;
                this.WriteEmulation();
            }
            else
            {
                this._memoryEntityStructEmulation.LoadStruct();
            }
        }
      
        /// <summary>
        /// Ошибка записи эмуляции
        /// </summary>
        private void EmulationWriteFail()
        {
            this._statusLedControl.State = LedState.Off;
            this._labelStatus.Text = "Нет связи с устройством";
            MessageBox.Show(WRITE_FAIL);

        }

        private void EmulationReadFail()
        {
            this._statusLedControl.State = LedState.Off;
            this._labelStatus.Text = READ_FAIL+"\nНет связи с устройством.";
        }

        private void ReadOk()
        {
            this._timeSignal.Text = this._readMemoryEntity.Value.TimeSignal;
            this._time.Text = this._readMemoryEntity.Value.Time;
            this._status.Text = this._readMemoryEntity.Value.Status.ToString();
        }

        private void readEmulation_Click(object sender, EventArgs e)
        {
            this._memoryEntityStructEmulation.LoadStruct();
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            this._incriment = (double)this._step.Value;
        }
        
        private void EmulationForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if(!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._currentWriteStruct.TimeSignal = _validatorTimeAndSignal.Get();
            this._currentWriteStruct.DiscretInputs = _discrets.Get();
            this._currentWriteStruct.TimeSignal.StopTime = true;
            this._memoryEntityStructEmulation.Value = this._currentWriteStruct;
            this._memoryEntityStructEmulation.SaveStruct();
            this._readMemoryEntity.RemoveStructQueries();
        }

        private void EmulationForm_Load(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._readMemoryEntity.LoadStructCycle(new TimeSpan(100));
        }


        private void _writeEmulButton_CheckedChanged(object sender, EventArgs e)
        {
            if (this._writeEmulButton.Checked)
            {
                this._writeEmulButton.BackColor = Color.Black;
                this._writeEmulButton.ForeColor = Color.White;
                if (this._isSelect)
                {
                    foreach (var control in this._listStartControls)
                    {
                        control.Enabled = true;
                    }
                    this._startTime.Enabled = true;
                }
                else
                {
                    foreach (var control in this._listStartControls)
                    {
                        control.Enabled = false;
                    }
                    this._startTime.Enabled = false;
                }
                if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
                this._isWrite = true;
                this.WriteEmulation();
            }
            else
            {
                this._writeEmulButton.BackColor = Color.Empty;
                this._writeEmulButton.ForeColor = Color.Black;
                this._isWrite = false;
                if (this._isSelect)
                {
                    foreach (var control in this._listStartControls)
                    {
                        control.Enabled = false;
                    }
                    this._startTime.Enabled = true;
                }
            }

        }

        private void _discret_CheckedChanged(object sender, EventArgs e)
        {
            if (this._isWrite)
            {
                this._currentWriteStruct.DiscretInputs = _discrets.Get();
                this._currentWriteStruct.TimeSignal = _validatorTimeAndSignal.Get();

                var cheak = sender as CheckBox;
                var isstart = cheak.Tag as CheckBox;
                if (this._isSelect)
                {
                    if (isstart.Checked || this._isStart)
                    {
                        _currentWriteStruct.TimeSignal.StatusTime = true;
                    }

                }
                this._memoryEntityStructEmulation.Value = this._currentWriteStruct;
                this._memoryEntityStructEmulation.SaveStruct();
            }
        }
        
        private void _signal_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (this._signal.SelectedIndex == 0)
            {
                this._currentWriteStruct.TimeSignal = this._validatorTimeAndSignal.Get();
                this._currentWriteStruct.DiscretInputs = this._discrets.Get();
                this._currentWriteStruct.TimeSignal.StopTime = true;
                this._memoryEntityStructEmulation.Value = this._currentWriteStruct;
                this._memoryEntityStructEmulation.SaveStruct();

                this._isSelect = false;
                this._startTime.Enabled = false;
                foreach (var control in this._listStartControls)
                {
                    control.Enabled = false;
                }
            }
            else
            {
                if (!this._writeEmulButton.Checked)
                {
                    foreach (var control in this._listStartControls)
                    {
                        control.Enabled = false;
                        this._startTime.Enabled = true;
                    }
                }
                else
                {
                    foreach (var control in this._listStartControls)
                    {
                        control.Enabled = true;
                        this._startTime.Enabled = true;
                    }
                }

                this._isSelect = true;
            }
            this.Enabledcontrols();
        }

        private void _status_TextChanged(object sender, EventArgs e)
        {
            switch (this._status.Text)
            {
                case "1":
                    {
                        this._statusLedControl.State = LedState.Signaled;
                        this._labelStatus.Text = "Эмуляция 1 без блокировки выходных сигналов запущена";
                        break;
                    }
                case "2":
                    {
                        this._statusLedControl.State = LedState.Signaled;
                        this._labelStatus.Text = "Эмуляция 1 с блокировкой выходных сигналов запущена";
                        break;
                    }
                case "3":
                    {
                        this._statusLedControl.State = LedState.NoSignaled;
                        this._labelStatus.Text = "Эмуляция остановлена";
                        break;
                    }
                case "0":
                    {
                        this._statusLedControl.State = LedState.NoSignaled;
                        this._labelStatus.Text = "Эмуляция остановлена";
                        break;
                    }

            }
        }

        private void ToXML()
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                XmlSerializer serializer = new XmlSerializer(typeof(WriteStructEmul));

                using (StreamWriter writer = new StreamWriter(saveFileDialog.FileName))
                {
                    _currentWriteStruct.TimeSignal = _validatorTimeAndSignal.Get();
                    _currentWriteStruct.DiscretInputs = _discrets.Get();
                    this._memoryEntityStructEmulation.Value = this._currentWriteStruct;
                    serializer.Serialize(writer, _currentWriteStruct);
                }
            }
        }
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
        private void FromXml()
        {
            //XmlSerializer serializer = new XmlSerializer(typeof(WriteStructEmul));
            //XmlTextReader reader = new XmlTextReader(new System.IO.StringReader(Settings.Default.EmulationDefailtStruct));
            //reader.Read();
            //this._validatorEmulationStruct.Set(serializer.Deserialize(reader));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.ToXML();
        }

        private void _startTime_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox ch = sender as CheckBox;
            if (ch.Checked)
            {
                this._isStart = true;
            }
            else
            {
                this._isStart = false;
            }
        }

        private void _exitEmulation_CheckedChanged(object sender, EventArgs e)
        {
            if (this._exitEmulation.Checked)
            {
                this._isExit = true;
            }
            else
            {
                this._isExit = false;
            }
        }

        private void _time_TextChanged(object sender, EventArgs e)
        {
            if (this._isExit)
            {
                if (this._time.Text.Contains("0:05"))
                {
                    this.WriteEmulation();
                }
            }
        }

        private void EmulationForm_Activated(object sender, EventArgs e)
        {
            StringsConfigNew.CurrentVersion = Common.VersionConverter(this._device.DeviceVersion);
        }

        #region [IFormView Members]

        public Type ClassType => typeof(EmulationFormNew);

        public bool ForceShow => false;

        public Image NodeImage => Resources.emulation.ToBitmap();

        public string NodeName => "Эмуляция";

        public INodeView[] ChildNodes => new INodeView[] { };

        public bool Deletable => false;

        public Type FormDevice => typeof(Mr761ObrDevice);

        public bool Multishow { get; private set; }

        #endregion
    }
}
