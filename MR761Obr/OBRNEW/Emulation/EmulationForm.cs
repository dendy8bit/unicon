﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR761Obr;
using BEMN.MR761Obr.OBRNEW.Configuration;
using BEMN.MR761Obr.Properties;
using BEMN.MR761OBR.OBRNEW.Emulation.Structures;

namespace BEMN.MR761OBR.OBRNEW.Emulation
{
    public partial class EmulationForm : Form, IFormView
    {
        private const string READ_FAIL = "Не удалось прочитать эмуляцию.";
        private const string WRITE_FAIL = "Не удалось записать эмуляцию.";
        
        private int _heightGpUTime;
        private int _heightGpAnalog;
        private int _heightGpDisTime;
        private int _heightGpItime;

        #region [Private fields]

        private readonly Mr761ObrDevice _device;

        private bool IsStart;
        private bool IsSelect;
        private bool IsWrite;
        private bool IsChange;
        private bool IsKvit;
        private bool IsExit;
        private bool IsBlock;
        private double _incriment;

        private Size resolution = Screen.PrimaryScreen.Bounds.Size;

        private List<Button> _listButtons;
        private MaskedTextBox _currentAdduction;
        private MemoryEntity<WriteStructEmul> _memoryEntityStructEmulation;
        private MemoryEntity<WriteStructEmul> _memoryEntityNullStructEmulation;
        private MemoryEntity<ReadStructEmul> _readMemoryEntity;
        
        private WriteStructEmul _currentWriteStruct;
        private WriteStructEmul _tempWriteStruct;

        private NewStructValidator<TimeAndSignalStruct> _validatorTimeAndSignal;
        private NewStructValidator<DiscretsSignals> _discrets;
        
        private List<CheckBox> _listStartControls;
        private List<CheckBox> _listDiscretsControls;
        private List<MaskedTextBox> _listAllControls;
        #endregion

        public EmulationForm()
        {
            this.InitializeComponent();
        }

        public EmulationForm(Mr761ObrDevice device)
        {
            try
            {
                this.InitializeComponent();
                this._device = device;

                StringsConfigNew.CurrentVersion = Common.VersionConverter(this._device.DeviceVersion);

                this._memoryEntityStructEmulation = this._device.WriteStructEmulation;
                this._memoryEntityNullStructEmulation = this._device.WriteStructEmulationNull;
                this._readMemoryEntity = this._device.ReadStructEmulation;

                this._memoryEntityStructEmulation.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.EmulationReadOk);
                this._memoryEntityStructEmulation.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.EmulationWriteOk);
                this._memoryEntityStructEmulation.WriteFail += HandlerHelper.CreateHandler(this, () =>
                {
                    this._memoryEntityStructEmulation.RemoveStructQueries();
                    this.EmulationWriteFail();
                });
                this._memoryEntityStructEmulation.ReadFail += HandlerHelper.CreateHandler(this, () =>
                {
                    this._memoryEntityStructEmulation.RemoveStructQueries();
                    this.EmulationReadFail();
                });

                this._memoryEntityNullStructEmulation.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.NullEmulationReadOk);
                this._memoryEntityNullStructEmulation.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.EmulationWriteOk);
                this._memoryEntityNullStructEmulation.WriteFail += HandlerHelper.CreateHandler(this, () =>
                {
                    this._memoryEntityNullStructEmulation.RemoveStructQueries();
                    this.EmulationWriteFail();
                    this._kvit.Enabled = true;
                });
                this._memoryEntityNullStructEmulation.ReadFail += HandlerHelper.CreateHandler(this, () =>
                {
                    this._memoryEntityStructEmulation.RemoveStructQueries();
                    this.EmulationReadFail();
                    this._kvit.Enabled = true;
                });
                
                this._readMemoryEntity.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadOk);
                this._readMemoryEntity.ReadFail += HandlerHelper.CreateHandler(this, () =>
                {
                    this._status.Text = "";
                    this.EmulationReadFail();
                });
                this._currentWriteStruct = new WriteStructEmul();

                if (!this._device.DeviceDlgInfo.IsConnectionMode)
                {
                    _device.Info.DeviceConfiguration = _device.DevicePlant;
                }

                this.Init();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                throw;
            }
            
        }

        private void Init()
        {
            this.kvitTooltip.SetToolTip(this._kvit, "Применяется для сброса углов при изменении частоты");

            #region [ListCotrols]

            this._listDiscretsControls = new List<CheckBox>();
            this._listAllControls = new List<MaskedTextBox>();
            this._listStartControls = new List<CheckBox>();
            this._listButtons = new List<Button>();

            foreach (var control in this._listDiscretsControls)
            {
                control.CheckedChanged += this._discret_CheckedChanged;
            }

            this.InitCheckBoxDiskrets();
            this.InitChekBoxDiskretTime();

            #endregion

            foreach (CheckBox discretsControl in _listDiscretsControls)
            {
                discretsControl.CheckedChanged += _discret_CheckedChanged;
            }

            foreach (var control in this._listStartControls)
            {
                control.Enabled = false;
            }

            //foreach (var control in this._listAllControls)
            //{
            //    control.MouseDown += new MouseEventHandler(this.MouseRightClick);
            //}

            this._startTime.Enabled = false;
            
            ControlInfoCheck[] cntrDiskrValids = new ControlInfoCheck[_listDiscretsControls.Count];
            for (int i = 0; i < _listDiscretsControls.Count; i++)
            {
                cntrDiskrValids[i] = new ControlInfoCheck(_listDiscretsControls[i]);
            }

            this._discrets = new NewStructValidator<DiscretsSignals>(this.toolTip1,cntrDiskrValids);
            

            this._validatorTimeAndSignal = new NewStructValidator<TimeAndSignalStruct>
            (
                this.toolTip1,
                new ControlInfoCheck(this._startTime),
                new ControlInfoCombo(this._signal, StringsConfigNew.RelaySignals)
            );

            //this.InitButton();
            this.InitStartControls();

            //this.GotFocusInControl();
            //this.FromXml();
        }

        private void InitCheckBoxDiskrets()
        {
            if (_device.Info.DeviceConfiguration == "T0N0D74R35")
            {
                for (int i = 0; i < 73; i++)
                {
                    CheckBox c1 = new CheckBox();
                    c1.Text = "Д" + (i + 1);
                    c1.AutoSize = true;
                    c1.Tag = 0;

                    if (i <= 25)
                    {
                        c1.Location = new Point(3, 3 + 16 * i);
                    }

                    if (i >= 26 & i < 47)
                    {
                        c1.Location = new Point(50, 3 + 16 * (i - 20));
                    }

                    if (i >= 57)
                    {
                        if (i > 71)
                        {
                            c1.Text = "K" + (i - 55);
                        }
                        c1.Location = new Point(97, 3 + 16 * (i - 40));
                    }
                    this._listDiscretsControls.Add(c1);
                    panelDiscrets.Controls.Add(c1);
                }
            }
            else if (_device.Info.DeviceConfiguration == "T0N0D106R67")
            {
                for (int i = 0; i < 105; i++)
                {
                    CheckBox c1 = new CheckBox();
                    c1.Text = "Д" + (i + 1);
                    c1.AutoSize = true;
                    c1.Tag = 0;

                    if (i <= 30)
                    {
                        c1.Location = new Point(3, 3 + 16 * i);
                    }

                    if (i >= 31 & i < 62)
                    {
                        c1.Location = new Point(50, 3 + 16 * (i - 20));
                    }

                    if (i >= 63 & i < 94)
                    {
                        c1.Location = new Point(50, 3 + 16 * (i - 20));
                    }

                    if (i >= 95)
                    {
                        if (i > 103)
                        {
                            c1.Text = "K" + (i - 55);
                        }
                        c1.Location = new Point(97, 3 + 16 * (i - 40));
                    }
                    this._listDiscretsControls.Add(c1);
                    panelDiscrets.Controls.Add(c1);
                }
            }
            else
            {
                for (int i = 0; i < 113; i++)
                {
                    CheckBox c1 = new CheckBox();
                    c1.Text = "Д" + (i + 1);
                    c1.AutoSize = true;
                    c1.Tag = 0;

                    if (i <= 28)
                    {
                        c1.Location = new Point(3, 3 + 16 * i);
                    }

                    if (i >= 29 & i < 57)
                    {
                        c1.Location = new Point(50, 3 + 16 * (i - 28));
                    }

                    if (i >= 57 & i < 82)
                    {
                        c1.Location = new Point(87, 3 + 16 * (i - 57));
                    }

                    if (i >= 82)
                    {
                        if (i > 109)
                        {
                            c1.Text = "K" + (i - 82);
                        }
                        c1.Location = new Point(97, 3 + 16 * i);
                    }
                    this._listDiscretsControls.Add(c1);
                    panelDiscrets.Controls.Add(c1);
                }
            }
        }

        private void InitChekBoxDiskretTime()
        {
            this._listStartControls = new List<CheckBox>();

            for (int i = 0; i < 113; i++)
            {
                CheckBox c1 = new CheckBox();
                c1.Text = "Д" + (i + 1);
                c1.AutoSize = true;
                c1.Tag = 0;

                if (i <= 30)
                {
                    c1.Location = new Point(3, 3 + 16 * i);
                }

                if (i >= 31 & i < 62)
                {
                    c1.Location = new Point(50, 3 + 16 * (i - 20));
                }

                if (i >= 63 & i < 94)
                {
                    c1.Location = new Point(50, 3 + 16 * (i - 20));
                }

                if (i >= 95)
                {
                    if (i > 109)
                    {
                        c1.Text = "K" + (i - 55);
                    }
                    c1.Location = new Point(97, 3 + 16 * (i - 40));
                }

                _listStartControls.Add(c1);
                panelDiskretsTime.Controls.Add(c1);
            }
        }
        
        /// <summary>
        /// Связывает checkbox с m maskedtextbox
        /// </summary>
        private void InitStartControls()
        {
            int count = 0;
            for (int i = 0; i < this._listDiscretsControls.Count; i++)
            {
                this._listDiscretsControls[i].Tag = this._listStartControls[count];
                count++;
            }
        }

        private void GotFocusInControl()
        {
            foreach (var control in this._listAllControls)
            {
                control.GotFocus += this.OnGotFocused;
                control.LostFocus += this.LostFocus;
                control.KeyDown += this.ControlKeyDown;
            }
        }

        private void _kvit_Click(object sender, EventArgs e)
        {
            this._kvit.Enabled = false;
            this._memoryEntityNullStructEmulation.LoadStruct();
            
        }

        private void NullEmulationReadOk()
        {
            try
            {
                this.IsKvit = true;

                this._tempWriteStruct = this._memoryEntityNullStructEmulation.Value;

                WriteStructEmul nul = new WriteStructEmul();
                nul.TimeSignal = new TimeAndSignalStruct();
                nul.TimeSignal.IsRestart = true;
                nul.DiscretInputs = new DiscretsSignals();
                
                this._memoryEntityNullStructEmulation.Value = nul;
                this._memoryEntityNullStructEmulation.SaveStruct();
            }
            catch (Exception)
            {
                this.IsKvit = false;
            }
        }

        private void WriteEmulation()
        {
            if (this.IsKvit)
            {
                this.IsKvit = false;
                this._currentWriteStruct = this._tempWriteStruct;
            }
            else
            {
                this._currentWriteStruct.TimeSignal = _validatorTimeAndSignal.Get();
                this._currentWriteStruct.DiscretInputs = this._discrets.Get();
            }

            if (this.IsSelect)
            {
                if (this.IsStart)
                {
                    this._currentWriteStruct.TimeSignal.StatusTime = true;
                }

            }
            this._currentWriteStruct.TimeSignal.IsRestart = false;
            
            this._memoryEntityStructEmulation.Value = this._currentWriteStruct;
            this._memoryEntityStructEmulation.SaveStruct(new TimeSpan(1000));
            
        }
        
        private void EmulationReadOk()
        {
            this._currentWriteStruct = this._memoryEntityStructEmulation.Value;
            this._validatorTimeAndSignal.Set(_currentWriteStruct.TimeSignal);
            this._discrets.Set(_currentWriteStruct.DiscretInputs);
           
        }

        private void EmulationWriteOk()
        {
            if (IsBlock = true)
            {

            }
            if (this.IsKvit)
            {
                this._kvit.Enabled = true;
                this.WriteEmulation();
            }
            else
            {
                var write = this._memoryEntityStructEmulation.Value;
                write.TimeSignal.WriteTimeOk= true;
                this._memoryEntityStructEmulation.Value = write;
                this._memoryEntityStructEmulation.LoadStruct();
            }
        }
      
        /// <summary>
        /// Ошибка записи эмуляции
        /// </summary>
        private void EmulationWriteFail()
        {
            this._statusLedControl.State = LedState.Off;
            this._labelStatus.Text = "Нет связи с устройством";
            MessageBox.Show(WRITE_FAIL);

        }

        private void EmulationReadFail()
        {
            this._statusLedControl.State = LedState.Off;
            this._labelStatus.Text = READ_FAIL+"\nНет связи с устройством.";
        }

        private void ReadOk()
        {
            this._timeSignal.Text = this._readMemoryEntity.Value.TimeSignal;
            this._time.Text = this._readMemoryEntity.Value.Time;
            this._status.Text = this._readMemoryEntity.Value.Status.ToString();
        }

        private void readEmulation_Click(object sender, EventArgs e)
        {
            this._memoryEntityStructEmulation.LoadStruct();
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            this._incriment = (double)this._step.Value;
        }


        private void OnGotFocused(object sender, EventArgs e)
        {
            MaskedTextBox text = sender as MaskedTextBox;
            text?.SelectAll();
           
            if (this._writeEmulButton.Checked)
            {
                this._currentAdduction = sender as MaskedTextBox;
                this._currentAdduction.TextChanged += this.OnTextChanged;
            }
        }

        private void ControlKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                MaskedTextBox mt = sender as MaskedTextBox;
                if (mt.Text == String.Empty)
                {
                    mt.Text = "0";
                }
                int index = this._listAllControls.IndexOf(mt) + 1;
                if (index == this._listAllControls.Count)
                {
                    index = 0;
                }
                if (!this._listAllControls[index].Visible)
                {
                    while (!this._listAllControls[index].Visible)
                    {
                        index++;
                    }
                }


                this._listAllControls[index].Focus();
            }
        }

        private void OnTextChanged(object sender, EventArgs e)
        {
            this.IsChange = true;
        }

        private void LostFocus(object sender, EventArgs e)
        {
            if (!this._writeEmulButton.Checked || !(sender is MaskedTextBox)) return;

            this._currentAdduction = (MaskedTextBox) sender;
            
            if (this.IsChange)
            {
                var isstart = this._currentAdduction.Tag as CheckBox;
                this.IsChange = false;
                this._currentWriteStruct.TimeSignal = _validatorTimeAndSignal.Get();
                this._currentWriteStruct.DiscretInputs = _discrets.Get();
                if (this.IsSelect)
                {
                    if (isstart.Checked || this.IsStart)
                    {
                        _currentWriteStruct.TimeSignal.StatusTime = true;
                    }
                }
            }

            this._currentWriteStruct.TimeSignal.IsRestart = false;
            this._memoryEntityStructEmulation.Value = this._currentWriteStruct;
            this._memoryEntityStructEmulation.SaveStruct();
            this._currentAdduction.TextChanged -= this.OnTextChanged;
        }
        
        private void EmulationForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if(!this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._currentWriteStruct.TimeSignal = _validatorTimeAndSignal.Get();
            this._currentWriteStruct.DiscretInputs = _discrets.Get();
            this._currentWriteStruct.TimeSignal.StopTime = true;
            this._memoryEntityStructEmulation.Value = this._currentWriteStruct;
            this._memoryEntityStructEmulation.SaveStruct();
            this._readMemoryEntity.RemoveStructQueries();
        }

        private void EmulationForm_Load(object sender, EventArgs e)
        {
            if (!this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._readMemoryEntity.LoadStructCycle(new TimeSpan(100));
        }


        private void _writeEmulButton_CheckedChanged(object sender, EventArgs e)
        {
            if (this._writeEmulButton.Checked)
            {
                this._writeEmulButton.BackColor = Color.Black;
                this._writeEmulButton.ForeColor = Color.White;
                if (this.IsSelect)
                {
                    foreach (var control in this._listStartControls)
                    {
                        control.Enabled = true;
                    }
                    this._startTime.Enabled = true;
                }
                else
                {
                    foreach (var control in this._listStartControls)
                    {
                        control.Enabled = false;
                    }
                    this._startTime.Enabled = false;
                }
                if (!this._device.DeviceDlgInfo.IsConnectionMode) return;
                
                this.IsWrite = true;
                this.WriteEmulation();
            }
            else
            {
                this._writeEmulButton.BackColor = Color.Empty;
                this._writeEmulButton.ForeColor = Color.Black;
                this.IsWrite = false;
                if (this.IsSelect)
                {
                    foreach (var control in this._listStartControls)
                    {
                        control.Enabled = false;
                    }
                    this._startTime.Enabled = true;
                }
            }

        }


        private void _discret_CheckedChanged(object sender, EventArgs e)
        {
            this.IsBlock = true;
            if (this.IsWrite)
            {
                _currentWriteStruct.TimeSignal = _validatorTimeAndSignal.Get();
                _currentWriteStruct.DiscretInputs = _discrets.Get();
                
                var cheak = sender as CheckBox;
                var isstart = cheak.Tag as CheckBox;
                if (this.IsSelect)
                {
                    if (isstart.Checked || this.IsStart)
                    {
                        _currentWriteStruct.TimeSignal.StatusTime = true;
                    }

                }
                this._memoryEntityStructEmulation.Value = this._currentWriteStruct;
                this._memoryEntityStructEmulation.SaveStruct();
            }
        }


        private void MouseRightClick(object sender, MouseEventArgs e)
        {
            if (!(sender is MaskedTextBox tb))
            {
                return;
            }
            if (e.Button == MouseButtons.Left)
            {
                tb.SelectAll();
            }
            if (e.Button == MouseButtons.Right)
            {
                
            }
        }

        private void _signal_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this._signal.SelectedIndex == 0)
            {
                _currentWriteStruct.TimeSignal = _validatorTimeAndSignal.Get();
                _currentWriteStruct.DiscretInputs = _discrets.Get();
                _currentWriteStruct.TimeSignal.StopTime = true;
                this._memoryEntityStructEmulation.Value = this._currentWriteStruct;
                this._memoryEntityStructEmulation.SaveStruct();

                this.IsSelect = false;
                this._startTime.Enabled = false;
                foreach (var control in this._listStartControls)
                {
                    control.Enabled = false;
                }
            }
            else
            {
                if (!this._writeEmulButton.Checked)
                {
                    foreach (var control in this._listStartControls)
                    {
                        control.Enabled = false;
                        this._startTime.Enabled = true;
                    }
                }
                else
                {
                    foreach (var control in this._listStartControls)
                    {
                        control.Enabled = true;
                        this._startTime.Enabled = true;
                    }
                }

                this.IsSelect = true;
            }
        }

        private void _status_TextChanged(object sender, EventArgs e)
        {
            switch (this._status.Text)
            {
                case "1":
                    {
                        this._statusLedControl.State = LedState.Signaled;
                        this._labelStatus.Text = "Эмуляция 1 без блокировки выходных сигналов запущена";
                        //ReadStructEmul.isStatus = true;
                        break;
                    }
                case "2":
                    {
                        this._statusLedControl.State = LedState.Signaled;
                        this._labelStatus.Text = "Эмуляция 1 с блокировкой выходных сигналов запущена";
                        //ReadStructEmul.isStatus = true;
                        break;
                    }
                case "3":
                    {
                        this._statusLedControl.State = LedState.NoSignaled;
                        this._labelStatus.Text = "Эмуляция остановлена";
                        //ReadStructEmul.isStatus = false;
                        break;
                    }
                case "0":
                    {
                        this._statusLedControl.State = LedState.NoSignaled;
                        this._labelStatus.Text = "Эмуляция остановлена";
                        //ReadStructEmul.isStatus = false;
                        break;
                    }

            }
        }

        private void ToXML()
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                XmlSerializer serializer = new XmlSerializer(typeof(WriteStructEmul));

                using (StreamWriter writer = new StreamWriter(saveFileDialog.FileName))
                {
                    _currentWriteStruct.TimeSignal = _validatorTimeAndSignal.Get();
                    _currentWriteStruct.DiscretInputs = _discrets.Get();
                    this._memoryEntityStructEmulation.Value = this._currentWriteStruct;
                    serializer.Serialize(writer, _currentWriteStruct);
                }
            }
        }
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
        private void FromXml()
        {
            //XmlSerializer serializer = new XmlSerializer(typeof(WriteStructEmul));
            //XmlTextReader reader = new XmlTextReader(new System.IO.StringReader(Settings.Default.EmulationDefailtStruct));
            //reader.Read();
            //this._validatorEmulationStruct.Set(serializer.Deserialize(reader));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.ToXML();
        }

        private void _startTime_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox ch = sender as CheckBox;
            if (ch.Checked)
            {
                this.IsStart = true;
            }
            else
            {
                this.IsStart = false;
            }
        }

        private void _exitEmulation_CheckedChanged(object sender, EventArgs e)
        {
            if (this._exitEmulation.Checked)
            {
                this.IsExit = true;
            }
            else
            {
                this.IsExit = false;
            }
        }

        private void _time_TextChanged(object sender, EventArgs e)
        {
            if (this.IsExit)
            {
                if (this._time.Text.Contains("0:05"))
                {
                    this.WriteEmulation();
                }
            }
        }

        private void EmulationForm_Activated(object sender, EventArgs e)
        {
            StringsConfigNew.CurrentVersion = Common.VersionConverter(this._device.DeviceVersion);
        }

        #region [IFormView Members]

        public Type ClassType => typeof(EmulationForm);

        public bool ForceShow => false;

        public Image NodeImage => Resources.emulation.ToBitmap();

        public string NodeName => "Эмуляция";

        public INodeView[] ChildNodes => new INodeView[] { };

        public bool Deletable => false;

        public Type FormDevice => typeof(Mr761ObrDevice);

        public bool Multishow { get; private set; }

        #endregion
    }
}
