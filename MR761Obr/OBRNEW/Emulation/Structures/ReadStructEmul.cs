﻿using System;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR761OBR.OBRNEW.Emulation.Structures
{
    public class ReadStructEmul : StructBase
    {
        #region [Private Fiels]
        [Layout(0)] private int _timesignal; // время с момента подачи команды
        [Layout(1)] private int _time; // время режима эмуляции
        [Layout(2)] private ushort _status; // статус 0 - остановлен, 1 - без блокировки реле, 2 - с блокировкой реле

        #endregion
        
        /// <summary>
        ///Время с момента подачи команды
        ///</summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "время с момента подачи команды")]
        public string TimeSignal
        {
            get { return this._timesignal.ToString() + " мс"; }
            //set { _timesignal = value; }
        }

        /// <summary>
        /// Время режима эмуляции
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "время режима эмуляции")]
        public string Time
        {
            get { return this.ConvertToTime(this._time); }
            //set { _time = this.ConvertToTime(value); }
        }

        [BindingProperty(2)]
        [XmlElement(ElementName = "статус 0 - остановлен, 1 - без блокировки реле, 2 - с блокировкой реле")]
        public int Status
        {
            get { return this._status; }
            set { this._timesignal = value; }
        }


        private string ConvertToTime(int t)
        {
           
            TimeSpan result = TimeSpan.FromMilliseconds(t);
            string answer = string.Format("{0:D1}:{1:D2},{2:D3}",
            result.Minutes,
                result.Seconds,
                result.Milliseconds);
            return answer;
        }
    }
}
