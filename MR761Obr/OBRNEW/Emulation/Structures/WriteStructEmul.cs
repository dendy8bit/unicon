﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR761OBR.OBRNEW.Emulation.Structures
{
    public class WriteStructEmul : StructBase
    {
        #region [Private Fiels]
        
        [Layout(0)] private TimeAndSignalStruct _timeAndSignal;
        [Layout(1)] private DiscretsSignals _diskretInput; // дискретные данные (112 сигнала)

        #endregion
        
        public TimeAndSignalStruct TimeSignal
        {
            get { return this._timeAndSignal; }
            set { this._timeAndSignal = value; }
        }
        
        /// <summary>
        /// Список дискретных синалов
        /// </summary>
        [XmlElement(ElementName = "Список дискретных синалов")]
        public DiscretsSignals DiscretInputs
        {
            get { return this._diskretInput; }
            set { this._diskretInput = value; }
        }
    }
}


