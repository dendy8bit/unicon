﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR761OBR.OBRNEW.Emulation.Structures
{
    public class AllDiskretsSignalStruct : StructBase
    {
        public const int LOGIC_COUNT = 4;

        [Layout(0, Count = LOGIC_COUNT)] private DiskretsSignalStruct[] _logic;

        [BindingProperty(0)]
        [XmlIgnore]
        public DiskretsSignalStruct this[int index]
        {
            get { return this._logic[index]; }
            set { this._logic[index] = value; }
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {

        }

        //public void WriteXml(System.Xml.XmlWriter writer)
        //{
        //    for (int i = 0; i < LOGIC_COUNT; i++)
        //    {

        //        writer.WriteStartElement("ЛС");
        //        this[i].WriteXml(writer);
        //        writer.WriteEndElement();
        //    }
        //}

    }
}
