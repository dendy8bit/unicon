﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.MR761OBR.OBRNEW.Emulation.Structures
{
    public class DiscretsSignals : StructBase
    {
        #region[Constants]
        public const int LOGIC_COUNT = 8;
        #endregion [Constants]
        
        #region [Private fields]
        [Layout(0, Count = LOGIC_COUNT)]private ushort[] _discretsSignals; 
        #endregion [Private fields]

        #region Public Fields
        [BindingProperty(0)]
        [XmlElement(ElementName = "Дискрет 1")]
        public bool D1
        {
            get { return Common.GetBit(this._discretsSignals[0], 0); }
            set { this._discretsSignals[0]=Common.SetBit(this._discretsSignals[0], 0, value); }
        }

        [BindingProperty(1)]
        [XmlElement(ElementName = "Дискрет 2")]
        public bool D2
        {
            get { return Common.GetBit(this._discretsSignals[0], 1); }
            set { this._discretsSignals[0]=Common.SetBit(this._discretsSignals[0], 1, value); }
        }

        [BindingProperty(2)]
        [XmlElement(ElementName = "Дискрет 3")]
        public bool D3
        {
            get { return Common.GetBit(this._discretsSignals[0], 2); }
            set { this._discretsSignals[0]=Common.SetBit(this._discretsSignals[0], 2, value); }
        }

        [BindingProperty(3)]
        [XmlElement(ElementName = "Дискрет 4")]
        public bool D4
        {
            get { return Common.GetBit(this._discretsSignals[0], 3); }
            set { this._discretsSignals[0] = Common.SetBit(this._discretsSignals[0], 3, value); }
        }

        [BindingProperty(4)]
        [XmlElement(ElementName = "Дискрет 5")]
        public bool D5
        {
            get { return Common.GetBit(this._discretsSignals[0], 4); }
            set { this._discretsSignals[0] = Common.SetBit(this._discretsSignals[0], 4, value); }
        }

        [BindingProperty(5)]
        [XmlElement(ElementName = "Дискрет 6")]
        public bool D6
        {
            get { return Common.GetBit(this._discretsSignals[0], 5); }
            set { this._discretsSignals[0] = Common.SetBit(this._discretsSignals[0], 5, value); }
        }

        [BindingProperty(6)]
        [XmlElement(ElementName = "Дискрет 7")]
        public bool D7
        {
            get { return Common.GetBit(this._discretsSignals[0], 6); }
            set { this._discretsSignals[0] = Common.SetBit(this._discretsSignals[0], 6, value); }
        }

        [BindingProperty(7)]
        [XmlElement(ElementName = "Дискрет 8")]
        public bool D8
        {
            get { return Common.GetBit(this._discretsSignals[0], 7); }
            set { this._discretsSignals[0] = Common.SetBit(this._discretsSignals[0], 7, value); }
        }

        [BindingProperty(8)]
        [XmlElement(ElementName = "Дискрет 9")]
        public bool D9
        {
            get { return Common.GetBit(this._discretsSignals[0], 8); }
            set { this._discretsSignals[0] = Common.SetBit(this._discretsSignals[0], 8, value); }
        }

        [BindingProperty(9)]
        [XmlElement(ElementName = "Дискрет 10")]
        public bool D10
        {
            get { return Common.GetBit(this._discretsSignals[0], 9); }
            set { this._discretsSignals[0] = Common.SetBit(this._discretsSignals[0], 9, value); }
        }

        [BindingProperty(10)]
        [XmlElement(ElementName = "Дискрет 11")]
        public bool D11
        {
            get { return Common.GetBit(this._discretsSignals[0], 10); }
            set { this._discretsSignals[0] = Common.SetBit(this._discretsSignals[0], 10, value); }
        }

        [BindingProperty(11)]
        [XmlElement(ElementName = "Дискрет 12")]
        public bool D12
        {
            get { return Common.GetBit(this._discretsSignals[0], 11); }
            set { this._discretsSignals[0] = Common.SetBit(this._discretsSignals[0], 11, value); }
        }

        [BindingProperty(12)]
        [XmlElement(ElementName = "Дискрет 13")]
        public bool D13
        {
            get { return Common.GetBit(this._discretsSignals[0], 12); }
            set { this._discretsSignals[0] = Common.SetBit(this._discretsSignals[0], 12, value); }
        }

        [BindingProperty(13)]
        [XmlElement(ElementName = "Дискрет 14")]
        public bool D14
        {
            get { return Common.GetBit(this._discretsSignals[0], 13); }
            set { this._discretsSignals[0] = Common.SetBit(this._discretsSignals[0], 13, value); }
        }

        [BindingProperty(14)]
        [XmlElement(ElementName = "Дискрет 15")]
        public bool D15
        {
            get { return Common.GetBit(this._discretsSignals[0], 14); }
            set { this._discretsSignals[0] = Common.SetBit(this._discretsSignals[0], 14, value); }
        }

        [BindingProperty(15)]
        [XmlElement(ElementName = "Дискрет 16")]
        public bool D16
        {
            get { return Common.GetBit(this._discretsSignals[0], 15); }
            set { this._discretsSignals[0] = Common.SetBit(this._discretsSignals[0], 15, value); }
        }

        [BindingProperty(16)]
        [XmlElement(ElementName = "Дискрет 17")]
        public bool D17
        {
            get { return Common.GetBit(this._discretsSignals[1], 0); }
            set { this._discretsSignals[1] = Common.SetBit(this._discretsSignals[1], 0, value); }
        }

        [BindingProperty(17)]
        [XmlElement(ElementName = "Дискрет 18")]
        public bool D18
        {
            get { return Common.GetBit(this._discretsSignals[1], 1); }
            set { this._discretsSignals[1] = Common.SetBit(this._discretsSignals[1], 1, value); }
        }

        [BindingProperty(18)]
        [XmlElement(ElementName = "Дискрет 19")]
        public bool D19
        {
            get { return Common.GetBit(this._discretsSignals[1], 2); }
            set { this._discretsSignals[1] = Common.SetBit(this._discretsSignals[1], 2, value); }
        }

        [BindingProperty(19)]
        [XmlElement(ElementName = "Дискрет 20")]
        public bool D20
        {
            get { return Common.GetBit(this._discretsSignals[1], 3); }
            set { this._discretsSignals[1] = Common.SetBit(this._discretsSignals[1], 3, value); }
        }

        [BindingProperty(20)]
        [XmlElement(ElementName = "Дискрет 21")]
        public bool D21
        {
            get { return Common.GetBit(this._discretsSignals[1], 4); }
            set { this._discretsSignals[1] = Common.SetBit(this._discretsSignals[1], 4, value); }
        }

        [BindingProperty(21)]
        [XmlElement(ElementName = "Дискрет 22")]
        public bool D22
        {
            get { return Common.GetBit(this._discretsSignals[1], 5); }
            set { this._discretsSignals[1] = Common.SetBit(this._discretsSignals[1], 5, value); }
        }

        [BindingProperty(22)]
        [XmlElement(ElementName = "Дискрет 23")]
        public bool D23
        {
            get { return Common.GetBit(this._discretsSignals[1], 6); }
            set { this._discretsSignals[1] = Common.SetBit(this._discretsSignals[1], 6, value); }
        }

        [BindingProperty(23)]
        [XmlElement(ElementName = "Дискрет 24")]
        public bool D24
        {
            get { return Common.GetBit(this._discretsSignals[1], 7); }
            set { this._discretsSignals[1] = Common.SetBit(this._discretsSignals[1],7, value); }
        }

        [BindingProperty(24)]
        [XmlElement(ElementName = "Дискрет 25")]
        public bool D25
        {
            get { return Common.GetBit(this._discretsSignals[1], 8); }
            set { this._discretsSignals[1] = Common.SetBit(this._discretsSignals[1], 8, value); }
        }

        [BindingProperty(25)]
        [XmlElement(ElementName = "Дискрет 26")]
        public bool D26
        {
            get { return Common.GetBit(this._discretsSignals[1], 9); }
            set { this._discretsSignals[1] = Common.SetBit(this._discretsSignals[1], 9, value); }
        }

        [BindingProperty(26)]
        [XmlElement(ElementName = "Дискрет 27")]
        public bool D27
        {
            get { return Common.GetBit(this._discretsSignals[1], 10); }
            set { this._discretsSignals[1] = Common.SetBit(this._discretsSignals[1], 10, value); }
        }

        [BindingProperty(27)]
        [XmlElement(ElementName = "Дискрет 28")]
        public bool D28
        {
            get { return Common.GetBit(this._discretsSignals[1], 11); }
            set { this._discretsSignals[1] = Common.SetBit(this._discretsSignals[1], 11, value); }
        }

        [BindingProperty(28)]
        [XmlElement(ElementName = "Дискрет 29")]
        public bool D29
        {
            get { return Common.GetBit(this._discretsSignals[1], 12); }
            set { this._discretsSignals[1] = Common.SetBit(this._discretsSignals[1], 12, value); }
        }

        [BindingProperty(29)]
        [XmlElement(ElementName = "Дискрет 30")]
        public bool D30
        {
            get { return Common.GetBit(this._discretsSignals[1], 13); }
            set { this._discretsSignals[1] = Common.SetBit(this._discretsSignals[1], 13, value); }
        }
        [BindingProperty(30)]
        [XmlElement(ElementName = "Дискрет 31")]
        public bool D31
        {
            get { return Common.GetBit(this._discretsSignals[1], 14); }
            set { this._discretsSignals[1] = Common.SetBit(this._discretsSignals[1], 14, value); }
        }

        [BindingProperty(31)]
        [XmlElement(ElementName = "Дискрет 32")]
        public bool D32
        {
            get { return Common.GetBit(this._discretsSignals[1], 15); }
            set { this._discretsSignals[1] = Common.SetBit(this._discretsSignals[1], 15, value); }
        }

        [BindingProperty(32)]
        [XmlElement(ElementName = "Дискрет 33")]
        public bool D33
        {
            get { return Common.GetBit(this._discretsSignals[2], 0); }
            set { this._discretsSignals[2] = Common.SetBit(this._discretsSignals[2], 0, value); }
        }

        [BindingProperty(33)]
        [XmlElement(ElementName = "Дискрет 34")]
        public bool D34
        {
            get { return Common.GetBit(this._discretsSignals[2], 1); }
            set { this._discretsSignals[2] = Common.SetBit(this._discretsSignals[2], 1, value); }
        }

        [BindingProperty(34)]
        [XmlElement(ElementName = "Дискрет 35")]
        public bool D35
        {
            get { return Common.GetBit(this._discretsSignals[2], 2); }
            set { this._discretsSignals[2] = Common.SetBit(this._discretsSignals[2], 2, value); }
        }

        [BindingProperty(35)]
        [XmlElement(ElementName = "Дискрет 36")]
        public bool D36
        {
            get { return Common.GetBit(this._discretsSignals[2], 3); }
            set { this._discretsSignals[2] = Common.SetBit(this._discretsSignals[2], 3, value); }
        }

        [BindingProperty(36)]
        [XmlElement(ElementName = "Дискрет 37")]
        public bool D37
        {
            get { return Common.GetBit(this._discretsSignals[2], 4); }
            set { this._discretsSignals[2] = Common.SetBit(this._discretsSignals[2], 4, value); }
        }

        [BindingProperty(37)]
        [XmlElement(ElementName = "Дискрет 38")]
        public bool D38
        {
            get { return Common.GetBit(this._discretsSignals[2], 5); }
            set { this._discretsSignals[2] = Common.SetBit(this._discretsSignals[2], 5, value); }
        }

        [BindingProperty(38)]
        [XmlElement(ElementName = "Дискрет 39")]
        public bool D39
        {
            get { return Common.GetBit(this._discretsSignals[2], 6); }
            set { this._discretsSignals[2] = Common.SetBit(this._discretsSignals[2], 6, value); }
        }

        [BindingProperty(39)]
        [XmlElement(ElementName = "Дискрет 40")]
        public bool D40
        {
            get { return Common.GetBit(this._discretsSignals[2], 7); }
            set { this._discretsSignals[2] = Common.SetBit(this._discretsSignals[2], 7, value); }
        }
        
        [BindingProperty(40)]
        [XmlElement(ElementName = "Дискрет 41")]
        public bool D41
        {
            get { return Common.GetBit(this._discretsSignals[2], 8); }
            set { this._discretsSignals[2] = Common.SetBit(this._discretsSignals[2], 8, value); }
        }
        [BindingProperty(41)]
        [XmlElement(ElementName = "Дискрет 42")]
        public bool D42
        {
            get { return Common.GetBit(this._discretsSignals[2], 9); }
            set { this._discretsSignals[2] = Common.SetBit(this._discretsSignals[2], 9, value); }
        }
        [BindingProperty(42)]
        [XmlElement(ElementName = "Дискрет 43")]
        public bool D43
        {
            get { return Common.GetBit(this._discretsSignals[2], 10); }
            set { this._discretsSignals[2] = Common.SetBit(this._discretsSignals[2], 10, value); }
        }
        [BindingProperty(43)]
        [XmlElement(ElementName = "Дискрет 44")]
        public bool D44
        {
            get { return Common.GetBit(this._discretsSignals[2], 11); }
            set { this._discretsSignals[2] = Common.SetBit(this._discretsSignals[2], 11, value); }
        }
        [BindingProperty(44)]
        [XmlElement(ElementName = "Дискрет 45")]
        public bool D45
        {
            get { return Common.GetBit(this._discretsSignals[2], 12); }
            set { this._discretsSignals[2] = Common.SetBit(this._discretsSignals[2], 12, value); }
        }
        [BindingProperty(45)]
        [XmlElement(ElementName = "Дискрет 46")]
        public bool D46
        {
            get { return Common.GetBit(this._discretsSignals[2], 13); }
            set { this._discretsSignals[2] = Common.SetBit(this._discretsSignals[2], 13, value); }
        }
        [BindingProperty(46)]
        [XmlElement(ElementName = "Дискрет 47")]
        public bool D47
        {
            get { return Common.GetBit(this._discretsSignals[2], 14); }
            set { this._discretsSignals[2] = Common.SetBit(this._discretsSignals[2], 14, value); }
        }
        [BindingProperty(47)]
        [XmlElement(ElementName = "Дискрет 48")]
        public bool D48
        {
            get { return Common.GetBit(this._discretsSignals[2], 15); }
            set { this._discretsSignals[2] = Common.SetBit(this._discretsSignals[2], 15, value); }
        }
        [BindingProperty(48)]
        [XmlElement(ElementName = "Дискрет 49")]
        public bool D49
        {
            get { return Common.GetBit(this._discretsSignals[3], 0); }
            set { this._discretsSignals[3] = Common.SetBit(this._discretsSignals[3], 0, value); }
        }
        [BindingProperty(49)]
        [XmlElement(ElementName = "Дискрет 50")]
        public bool D50
        {
            get { return Common.GetBit(this._discretsSignals[3], 1); }
            set { this._discretsSignals[3] = Common.SetBit(this._discretsSignals[3], 1, value); }
        }
        [BindingProperty(50)]
        [XmlElement(ElementName = "Дискрет 51")]
        public bool D51
        {
            get { return Common.GetBit(this._discretsSignals[3], 2); }
            set { this._discretsSignals[3] = Common.SetBit(this._discretsSignals[3], 2, value); }
        }
        [BindingProperty(51)]
        [XmlElement(ElementName = "Дискрет 52")]
        public bool D52
        {
            get { return Common.GetBit(this._discretsSignals[3], 3); }
            set { this._discretsSignals[3] = Common.SetBit(this._discretsSignals[3], 3, value); }
        }
        [BindingProperty(52)]
        [XmlElement(ElementName = "Дискрет 53")]
        public bool D53
        {
            get { return Common.GetBit(this._discretsSignals[3], 4); }
            set { this._discretsSignals[3] = Common.SetBit(this._discretsSignals[3], 4, value); }
        }
        [BindingProperty(53)]
        [XmlElement(ElementName = "Дискрет 54")]
        public bool D54
        {
            get { return Common.GetBit(this._discretsSignals[3], 5); }
            set { this._discretsSignals[3] = Common.SetBit(this._discretsSignals[3], 5, value); }
        }
        [BindingProperty(54)]
        [XmlElement(ElementName = "Дискрет 55")]
        public bool D55
        {
            get { return Common.GetBit(this._discretsSignals[3], 6); }
            set { this._discretsSignals[3] = Common.SetBit(this._discretsSignals[3], 6, value); }
        }
        [BindingProperty(55)]
        [XmlElement(ElementName = "Дискрет 56")]
        public bool D56
        {
            get { return Common.GetBit(this._discretsSignals[3], 7); }
            set { this._discretsSignals[3] = Common.SetBit(this._discretsSignals[3], 7, value); }
        }

        [BindingProperty(56)]
        [XmlElement(ElementName = "Дискрет 57")]
        public bool D57
        {
            get { return Common.GetBit(this._discretsSignals[3], 8); }
            set { this._discretsSignals[3] = Common.SetBit(this._discretsSignals[3], 8, value); }
        }

        [BindingProperty(57)]
        [XmlElement(ElementName = "Дискрет 58")]
        public bool D58
        {
            get { return Common.GetBit(this._discretsSignals[3], 9); }
            set { this._discretsSignals[3] = Common.SetBit(this._discretsSignals[3], 9, value); }
        }

        [BindingProperty(58)]
        [XmlElement(ElementName = "Дискрет 59")]
        public bool D59
        {
            get { return Common.GetBit(this._discretsSignals[3], 10); }
            set { this._discretsSignals[3] = Common.SetBit(this._discretsSignals[3], 10, value); }
        }

        [BindingProperty(59)]
        [XmlElement(ElementName = "Дискрет 60")]
        public bool D60
        {
            get { return Common.GetBit(this._discretsSignals[3], 11); }
            set { this._discretsSignals[3] = Common.SetBit(this._discretsSignals[3], 11, value); }
        }

        [BindingProperty(60)]
        [XmlElement(ElementName = "Дискрет 61")]
        public bool D61
        {
            get { return Common.GetBit(this._discretsSignals[3], 12); }
            set { this._discretsSignals[3] = Common.SetBit(this._discretsSignals[3], 12, value); }
        }

        [BindingProperty(61)]
        [XmlElement(ElementName = "Дискрет 62")]
        public bool D62
        {
            get { return Common.GetBit(this._discretsSignals[3], 13); }
            set { this._discretsSignals[3] = Common.SetBit(this._discretsSignals[3], 13, value); }
        }

        [BindingProperty(62)]
        [XmlElement(ElementName = "Дискрет 63")]
        public bool D63
        {
            get { return Common.GetBit(this._discretsSignals[3], 14); }
            set { this._discretsSignals[3] = Common.SetBit(this._discretsSignals[3], 14, value); }
        }

        [BindingProperty(63)]
        [XmlElement(ElementName = "Дискрет 64")]
        public bool D64
        {
            get { return Common.GetBit(this._discretsSignals[3], 15); }
            set { this._discretsSignals[3] = Common.SetBit(this._discretsSignals[3], 15, value); }
        }

        [BindingProperty(64)]
        [XmlElement(ElementName = "Дискрет 65")]
        public bool D65
        {
            get { return Common.GetBit(this._discretsSignals[4], 0); }
            set { this._discretsSignals[4] = Common.SetBit(this._discretsSignals[4], 0, value); }
        }

        [BindingProperty(65)]
        [XmlElement(ElementName = "Дискрет 66")]
        public bool D66
        {
            get { return Common.GetBit(this._discretsSignals[4], 1); }
            set { this._discretsSignals[4] = Common.SetBit(this._discretsSignals[4], 1, value); }
        }

        [BindingProperty(66)]
        [XmlElement(ElementName = "Дискрет 67")]
        public bool D67
        {
            get { return Common.GetBit(this._discretsSignals[4], 2); }
            set { this._discretsSignals[4] = Common.SetBit(this._discretsSignals[4], 2, value); }
        }

        [BindingProperty(67)]
        [XmlElement(ElementName = "Дискрет 68")]
        public bool D68
        {
            get { return Common.GetBit(this._discretsSignals[4], 3); }
            set { this._discretsSignals[4] = Common.SetBit(this._discretsSignals[4], 3, value); }
        }

        [BindingProperty(68)]
        [XmlElement(ElementName = "Дискрет 69")]
        public bool D69
        {
            get { return Common.GetBit(this._discretsSignals[4], 4); }
            set { this._discretsSignals[4] = Common.SetBit(this._discretsSignals[4], 4, value); }
        }

        [BindingProperty(69)]
        [XmlElement(ElementName = "Дискрет 70")]
        public bool D70
        {
            get { return Common.GetBit(this._discretsSignals[4], 5); }
            set { this._discretsSignals[4] = Common.SetBit(this._discretsSignals[4], 5, value); }
        }

        [BindingProperty(70)]
        [XmlElement(ElementName = "Дискрет 71")]
        public bool D71
        {
            get { return Common.GetBit(this._discretsSignals[4], 6); }
            set { this._discretsSignals[4] = Common.SetBit(this._discretsSignals[4], 6, value); }
        }

        [BindingProperty(71)]
        [XmlElement(ElementName = "Дискрет 72")]
        public bool D72
        {
            get { return Common.GetBit(this._discretsSignals[4], 7); }
            set { this._discretsSignals[4] = Common.SetBit(this._discretsSignals[4], 7, value); }
        }

        [BindingProperty(72)]
        [XmlElement(ElementName = "Дискрет 73")]
        public bool D73
        {
            get { return Common.GetBit(this._discretsSignals[4], 8); }
            set { this._discretsSignals[4] = Common.SetBit(this._discretsSignals[4], 8, value); }
        }

        [BindingProperty(73)]
        [XmlElement(ElementName = "Дискрет 74")]
        public bool D74
        {
            get { return Common.GetBit(this._discretsSignals[4], 9); }
            set { this._discretsSignals[4] = Common.SetBit(this._discretsSignals[4], 9, value); }
        }

        [BindingProperty(74)]
        [XmlElement(ElementName = "Дискрет 75")]
        public bool D75
        {
            get { return Common.GetBit(this._discretsSignals[4], 10); }
            set { this._discretsSignals[4] = Common.SetBit(this._discretsSignals[4], 10, value); }
        }

        [BindingProperty(75)]
        [XmlElement(ElementName = "Дискрет 76")]
        public bool D76
        {
            get { return Common.GetBit(this._discretsSignals[4], 11); }
            set { this._discretsSignals[4] = Common.SetBit(this._discretsSignals[4], 11, value); }
        }

        [BindingProperty(76)]
        [XmlElement(ElementName = "Дискрет 77")]
        public bool D77
        {
            get { return Common.GetBit(this._discretsSignals[4], 12); }
            set { this._discretsSignals[4] = Common.SetBit(this._discretsSignals[4], 12, value); }
        }

        [BindingProperty(77)]
        [XmlElement(ElementName = "Дискрет 78")]
        public bool D78
        {
            get { return Common.GetBit(this._discretsSignals[4], 13); }
            set { this._discretsSignals[4] = Common.SetBit(this._discretsSignals[4], 13, value); }
        }

        [BindingProperty(78)]
        [XmlElement(ElementName = "Дискрет 79")]
        public bool D79
        {
            get { return Common.GetBit(this._discretsSignals[4], 14); }
            set { this._discretsSignals[4] = Common.SetBit(this._discretsSignals[4], 14, value); }
        }

        [BindingProperty(79)]
        [XmlElement(ElementName = "Дискрет 80")]
        public bool D80
        {
            get { return Common.GetBit(this._discretsSignals[4], 15); }
            set { this._discretsSignals[4] = Common.SetBit(this._discretsSignals[4], 15, value); }
        }

        [BindingProperty(80)]
        [XmlElement(ElementName = "Дискрет 81")]
        public bool D81
        {
            get { return Common.GetBit(this._discretsSignals[5], 0); }
            set { this._discretsSignals[5] = Common.SetBit(this._discretsSignals[5], 0, value); }
        }

        [BindingProperty(81)]
        [XmlElement(ElementName = "Дискрет 82")]
        public bool D82
        {
            get { return Common.GetBit(this._discretsSignals[5], 1); }
            set { this._discretsSignals[5] = Common.SetBit(this._discretsSignals[5], 1, value); }
        }

        [BindingProperty(82)]
        [XmlElement(ElementName = "Дискрет 83")]
        public bool D83
        {
            get { return Common.GetBit(this._discretsSignals[5], 2); }
            set { this._discretsSignals[5] = Common.SetBit(this._discretsSignals[5], 2, value); }
        }

        [BindingProperty(83)]
        [XmlElement(ElementName = "Дискрет 84")]
        public bool D84
        {
            get { return Common.GetBit(this._discretsSignals[5], 3); }
            set { this._discretsSignals[5] = Common.SetBit(this._discretsSignals[5], 3, value); }
        }

        [BindingProperty(84)]
        [XmlElement(ElementName = "Дискрет 85")]
        public bool D85
        {
            get { return Common.GetBit(this._discretsSignals[5], 4); }
            set { this._discretsSignals[5] = Common.SetBit(this._discretsSignals[5], 4, value); }
        }

        [BindingProperty(85)]
        [XmlElement(ElementName = "Дискрет 86")]
        public bool D86
        {
            get { return Common.GetBit(this._discretsSignals[5], 5); }
            set { this._discretsSignals[5] = Common.SetBit(this._discretsSignals[5], 5, value); }
        }
        [BindingProperty(86)]
        [XmlElement(ElementName = "Дискрет 87")]
        public bool D87
        {
            get { return Common.GetBit(this._discretsSignals[5], 6); }
            set { this._discretsSignals[5] = Common.SetBit(this._discretsSignals[5], 6, value); }
        }

        [BindingProperty(87)]
        [XmlElement(ElementName = "Дискрет 88")]
        public bool D88
        {
            get { return Common.GetBit(this._discretsSignals[5], 7); }
            set { this._discretsSignals[5] = Common.SetBit(this._discretsSignals[5], 7, value); }
        }

        [BindingProperty(88)]
        [XmlElement(ElementName = "Дискрет 89")]
        public bool D89
        {
            get { return Common.GetBit(this._discretsSignals[5], 8); }
            set { this._discretsSignals[5] = Common.SetBit(this._discretsSignals[5], 8, value); }
        }

        [BindingProperty(89)]
        [XmlElement(ElementName = "Дискрет 90")]
        public bool D90
        {
            get { return Common.GetBit(this._discretsSignals[5], 9); }
            set { this._discretsSignals[5] = Common.SetBit(this._discretsSignals[5], 9, value); }
        }

        [BindingProperty(90)]
        [XmlElement(ElementName = "Дискрет 91")]
        public bool D91
        {
            get { return Common.GetBit(this._discretsSignals[5], 10); }
            set { this._discretsSignals[5] = Common.SetBit(this._discretsSignals[5], 10, value); }
        }

        [BindingProperty(91)]
        [XmlElement(ElementName = "Дискрет 92")]
        public bool D92
        {
            get { return Common.GetBit(this._discretsSignals[5], 11); }
            set { this._discretsSignals[5] = Common.SetBit(this._discretsSignals[5], 11, value); }
        }

        [BindingProperty(92)]
        [XmlElement(ElementName = "Дискрет 93")]
        public bool D93
        {
            get { return Common.GetBit(this._discretsSignals[5], 12); }
            set { this._discretsSignals[5] = Common.SetBit(this._discretsSignals[5], 12, value); }
        }

        [BindingProperty(93)]
        [XmlElement(ElementName = "Дискрет 94")]
        public bool D94
        {
            get { return Common.GetBit(this._discretsSignals[5], 13); }
            set { this._discretsSignals[5] = Common.SetBit(this._discretsSignals[5], 13, value); }
        }

        [BindingProperty(94)]
        [XmlElement(ElementName = "Дискрет 95")]
        public bool D95
        {
            get { return Common.GetBit(this._discretsSignals[5], 14); }
            set { this._discretsSignals[5] = Common.SetBit(this._discretsSignals[5], 14, value); }
        }

        [BindingProperty(95)]
        [XmlElement(ElementName = "Дискрет 96")]
        public bool D96
        {
            get { return Common.GetBit(this._discretsSignals[5], 15); }
            set { this._discretsSignals[5] = Common.SetBit(this._discretsSignals[5], 15, value); }
        }

        [BindingProperty(96)]
        [XmlElement(ElementName = "Дискрет 97")]
        public bool D97
        {
            get { return Common.GetBit(this._discretsSignals[6], 0); }
            set { this._discretsSignals[6] = Common.SetBit(this._discretsSignals[6], 0, value); }
        }
        [BindingProperty(97)]
        [XmlElement(ElementName = "Дискрет 98")]
        public bool D98
        {
            get { return Common.GetBit(this._discretsSignals[6], 1); }
            set { this._discretsSignals[6] = Common.SetBit(this._discretsSignals[6], 1, value); }
        }
        [BindingProperty(98)]
        [XmlElement(ElementName = "Дискрет 99")]
        public bool D99
        {
            get { return Common.GetBit(this._discretsSignals[6], 2); }
            set { this._discretsSignals[6] = Common.SetBit(this._discretsSignals[6], 2, value); }
        }
        [BindingProperty(99)]
        [XmlElement(ElementName = "Дискрет 100")]
        public bool D100
        {
            get { return Common.GetBit(this._discretsSignals[6], 3); }
            set { this._discretsSignals[6] = Common.SetBit(this._discretsSignals[6], 3, value); }
        }
        [BindingProperty(100)]
        [XmlElement(ElementName = "Дискрет 101")]
        public bool D101
        {
            get { return Common.GetBit(this._discretsSignals[6], 4); }
            set { this._discretsSignals[6] = Common.SetBit(this._discretsSignals[6], 4, value); }
        }
        [BindingProperty(101)]
        [XmlElement(ElementName = "Дискрет 102")]
        public bool D102
        {
            get { return Common.GetBit(this._discretsSignals[6], 5); }
            set { this._discretsSignals[6] = Common.SetBit(this._discretsSignals[6], 5, value); }
        }
        [BindingProperty(102)]
        [XmlElement(ElementName = "Дискрет 103")]
        public bool D103
        {
            get { return Common.GetBit(this._discretsSignals[6], 6); }
            set { this._discretsSignals[6] = Common.SetBit(this._discretsSignals[6], 6, value); }
        }
        [BindingProperty(103)]
        [XmlElement(ElementName = "Дискрет 104")]
        public bool D104
        {
            get { return Common.GetBit(this._discretsSignals[6], 7); }
            set { this._discretsSignals[6] = Common.SetBit(this._discretsSignals[6], 7, value); }
        }
        [BindingProperty(104)]
        [XmlElement(ElementName = "Дискрет 105")]
        public bool D105
        {
            get { return Common.GetBit(this._discretsSignals[6], 8); }
            set { this._discretsSignals[6] = Common.SetBit(this._discretsSignals[6],8, value); }
        }
        [BindingProperty(105)]
        [XmlElement(ElementName = "Дискрет 106")]
        public bool D106
        {
            get { return Common.GetBit(this._discretsSignals[6], 9); }
            set { this._discretsSignals[6] = Common.SetBit(this._discretsSignals[6], 9, value); }
        }
        [BindingProperty(106)]
        [XmlElement(ElementName = "Дискрет 107")]
        public bool D107
        {
            get { return Common.GetBit(this._discretsSignals[6], 10); }
            set { this._discretsSignals[6] = Common.SetBit(this._discretsSignals[6], 10, value); }
        }
        [BindingProperty(107)]
        [XmlElement(ElementName = "Дискрет 108")]
        public bool D108
        {
            get { return Common.GetBit(this._discretsSignals[6], 11); }
            set { this._discretsSignals[6] = Common.SetBit(this._discretsSignals[6], 11, value); }
        }
        [BindingProperty(108)]
        [XmlElement(ElementName = "Дискрет 109")]
        public bool D109
        {
            get { return Common.GetBit(this._discretsSignals[6], 12); }
            set { this._discretsSignals[6] = Common.SetBit(this._discretsSignals[6], 12, value); }
        }
        [BindingProperty(109)]
        [XmlElement(ElementName = "Дискрет 110")]
        public bool D110
        {
            get { return Common.GetBit(this._discretsSignals[6], 13); }
            set { this._discretsSignals[6] = Common.SetBit(this._discretsSignals[6], 13, value); }
        }
        [BindingProperty(110)]
        [XmlElement(ElementName = "Дискрет 111")]
        public bool D111
        {
            get { return Common.GetBit(this._discretsSignals[6], 14); }
            set { this._discretsSignals[6] = Common.SetBit(this._discretsSignals[6], 14, value); }
        }
        [BindingProperty(111)]
        [XmlElement(ElementName = "Дискрет 112")]
        public bool D112
        {
            get { return Common.GetBit(this._discretsSignals[6], 15); }
            set { this._discretsSignals[6] = Common.SetBit(this._discretsSignals[6], 15, value); }
        }

        [BindingProperty(112)]
        [XmlElement(ElementName = "K1")]
        public bool K1
        {
            get { return Common.GetBit(this._discretsSignals[7], 0); }
            set { this._discretsSignals[7] = Common.SetBit(this._discretsSignals[7], 0, value); }
        }
        [BindingProperty(113)]
        [XmlElement(ElementName = "K2")]
        public bool K2
        {
            get { return Common.GetBit(this._discretsSignals[7], 1); }
            set { this._discretsSignals[7] = Common.SetBit(this._discretsSignals[7], 1, value); }
        }
        #endregion
    }
}
