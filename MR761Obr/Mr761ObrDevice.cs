﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.FreeLogicStructures;
using BEMN.Forms;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MBServer.Queries;
using BEMN.MR761.Version1.OldClasses;
using BEMN.MR761Obr.AlarmJournal;
using BEMN.MR761Obr.BSBGL;
using BEMN.MR761Obr.Configuration;
using BEMN.MR761Obr.Configuration.Structures;
using BEMN.MR761Obr.Configuration.Structures.Oscope;
using BEMN.MR761Obr.Measuring;
using BEMN.MR761Obr.Measuring.Structures;
using BEMN.MR761Obr.OBRNEW.AlarmJournal;
using BEMN.MR761Obr.OBRNEW.AlarmJournal.Structures;
using BEMN.MR761Obr.OBRNEW.BSBGL;
using BEMN.MR761Obr.OBRNEW.Configuration;
using BEMN.MR761Obr.OBRNEW.Configuration.Structures;
using BEMN.MR761Obr.OBRNEW.Configuration.Structures.Oscope;
using BEMN.MR761Obr.OBRNEW.Measuring;
using BEMN.MR761Obr.OBRNEW.Osc;
using BEMN.MR761Obr.OBRNEW.Osc.Structures;
using BEMN.MR761Obr.OBRNEW.SystemJournal;
using BEMN.MR761Obr.OBRNEW.SystemJournal.Structures;
using BEMN.MR761Obr.Osc;
using BEMN.MR761Obr.Osc.Structures;
using BEMN.MR761Obr.SystemJournal;
using BEMN.MR761Obr.SystemJournal.Structures;
using BEMN.MR761OBR.OBRNEW.AlarmJournal.Structures;
using BEMN.MR761OBR.OBRNEW.Emulation;
using BEMN.MR761OBR.OBRNEW.Emulation.Structures;
using BEMN.MR761OBR.OBRNEW.Measuring.Structures;

namespace BEMN.MR761Obr
{
    public class Mr761ObrDevice: Device, IDeviceView, IDeviceVersion
    {
        #region Const
        private const string NODE_NAME = "МР761ОБР";
        private const string ERROR_SAVE_CONFIG = "Ошибка сохранения конфигурации";
        private const string WRITE_OK = "Конфигурация записана в устройство";
        private const int SYSTEM_JOURNAL_START_ADRESS = 0x600;
        private const int DISCRET_DATABASE_START_ADRESS = 0xD00;
        private const int ANALOG_DATABASE_START_ADRESS = 0x0E00;
        public const ushort MEASURE_START_ADDRESS = 0x14FE;
        public const int BAUDE_RATE_MAX = 921600;
        private ushort _groupSetPointSize;
        private bool _isCheckVersion;
        #endregion

        #region Field

        private MemoryEntity<AlarmJournalRecordStruct> _alarmJournal;
        private MemoryEntity<OneWordStruct> _refreshAlarmJournal;
        private MemoryEntity<SystemJournalStruct> _systemJournal;
        private MemoryEntity<OneWordStruct> _refreshSysJournal;

        private MemoryEntity<ConfigurationStruct> _configuration;
        private MemoryEntity<ConfigurationStructNew> _configurationNew;

        private MemoryEntity<DateTimeStruct> _dateTime;
        private MemoryEntity<DiscretDataBaseStruct> _discretDataBase;
        private MemoryEntity<OneWordStruct> _groupUstavki;

        private MemoryEntity<OneWordStruct> _refreshOscJournal;
        private MemoryEntity<OscPage> _oscPage;
        private MemoryEntity<OscJournalStruct> _oscJournal;
        private MemoryEntity<SetOscStartPageStruct> _setOscStartPage;
        private MemoryEntity<OscOptionsStruct> _oscOptions;
        private MemoryEntity<OscopeAllChannelsStruct> _allChannels;

        private MemoryEntity<SystemJournalStructNew> _systemJournalNew;
        private MemoryEntity<FullSystemJournalStruct1024> _full1024;
        private MemoryEntity<FullSystemJournalStruct64> _full64;
        private MemoryEntity<OneWordStruct> _saveSysJournalIndex;

        private MemoryEntity<DiscretDataBaseStructNew> _discretsNew;
        private MemoryEntity<OneWordStruct> _groupUstavkiNew;

        private MemoryEntity<WriteStructEmul> _writeSructEmulation;
        private MemoryEntity<WriteStructEmul> _writeSructEmulationNull;
        private MemoryEntity<ReadStructEmul> _readSructEmulation;

        private Dictionary<string, StObj> _memoryMap;
        private bool _devicesInitialised;
        public event Action ConfigWriteOk;
        public event Action ConfigWriteFail;
        #endregion

        #region Programming

        private MemoryEntity<OneWordStruct> _programPageStruct;
        private MemoryEntity<SourceProgramStruct> _sourceProgramStruct;
        private MemoryEntity<StartStruct> _programStartStruct;
        private MemoryEntity<LogicProgramSignals> _programSignalsStruct;
        private MemoryEntity<OneWordStruct> _startSpl;
        private MemoryEntity<OneWordStruct> _stopSpl;
        private MemoryEntity<OneWordStruct> _isSplOn;
        private MemoryEntity<OneWordStruct> _stateSpl;

        public MemoryEntity<OneWordStruct> ProgramPage
        {
            get { return this._programPageStruct; }
        }

        public MemoryEntity<OneWordStruct> StopSpl
        {
            get { return this._stopSpl; }
        }

        public MemoryEntity<OneWordStruct> StartSpl
        {
            get { return this._startSpl; }
        }

        public MemoryEntity<OneWordStruct> StateSpl
        {
            get { return this._stateSpl; }
        }

        public MemoryEntity<OneWordStruct> IsSplOn
        {
            get { return this._isSplOn; }
        }


        public MemoryEntity<StartStruct> ProgramStartStruct
        {
            get { return this._programStartStruct; }
        }
        public MemoryEntity<LogicProgramSignals> ProgramSignalsStruct
        {
            get { return this._programSignalsStruct; }
        }

        public MemoryEntity<SourceProgramStruct> SourceProgramStruct
        {
            get { return this._sourceProgramStruct; }
        }
        #endregion

        #region Constructor

        public Mr761ObrDevice()
        {
            HaveVersion = true;
        }

        public Mr761ObrDevice(Modbus mb)
        {
            this.MB = mb;
            HaveVersion = true;
            //this.InitMemoryEntity();
        }

        public sealed override Modbus MB
        {
            get { return mb; }
            set
            {
                mb = value;
                if (mb != null)
                {
                    mb.CompleteExchange += this.CompleteExchange;
                }

                //if (value == null) return;
                //if (mb != null)
                //{
                //    mb.CompleteExchange -= this.CompleteExchange;
                //}
                //mb = value;
                //mb.CompleteExchange += this.CompleteExchange;

            }
        }
        
        private void InitMemoryEntity()
        {
            this._alarmJournal = new MemoryEntity<AlarmJournalRecordStruct>("Запись журнала аварий", this, 0x0700);
            this._refreshAlarmJournal = new MemoryEntity<OneWordStruct>("Обновление журнала аварий", this, 0x0700);
            
            this._systemJournal = new MemoryEntity<SystemJournalStruct>("Запись журнала системы", this, SYSTEM_JOURNAL_START_ADRESS);
            this._refreshSysJournal = new MemoryEntity<OneWordStruct>("Обновление журнала системы", this, SYSTEM_JOURNAL_START_ADRESS);

            this._configuration = new MemoryEntity<ConfigurationStruct>("Конфигурация устройства", this, 0x1000);
            this._dateTime = new MemoryEntity<DateTimeStruct>("Время и дата в устройстве", this, 0x0200);
            this._groupUstavki = new MemoryEntity<OneWordStruct>("Группа уставок", this, 0x0400);
            this._discretDataBase = new MemoryEntity<DiscretDataBaseStruct>("Дискретная база данных", this, 0x0D00);

            this._allChannels = new MemoryEntity<OscopeAllChannelsStruct>("Уставки осциллографа", this, 0x1D9C);
            this._refreshOscJournal = new MemoryEntity<OneWordStruct>("Индекс журнала осциллографа", this, 0x0800);
            this._oscJournal = new MemoryEntity<OscJournalStruct>("Журнал осциллографа", this, 0x0800);
            this._oscPage = new MemoryEntity<OscPage>("Страница осциллографа", this, 0x900);
            this._oscOptions = new MemoryEntity<OscOptionsStruct>("Параметры осциллографа", this, 0x05A0);
            this._setOscStartPage = new MemoryEntity<SetOscStartPageStruct>("Установка стартовой страницы осциллограммы", this, 0x900);

            this._sourceProgramStruct = new MemoryEntity<SourceProgramStruct>("SaveProgram", this, 0x4300);

            this._stopSpl = new MemoryEntity<OneWordStruct>("Останов логической программы", this, 0x0D0C);
            this._startSpl = new MemoryEntity<OneWordStruct>("Старт логической программы", this, 0x0D0D);
            this._stateSpl = new MemoryEntity<OneWordStruct>("Состояние ошибок логики", this, 0x0D18);
            this._isSplOn = new MemoryEntity<OneWordStruct>("Состояние логики", this, 0x0D14);

            this._programStartStruct = new MemoryEntity<StartStruct>("SaveProgramStart", this, 0x0E00);
            this._programSignalsStruct = new MemoryEntity<LogicProgramSignals>("LoadProgramSignals", this, 0x4100);
            this._programPageStruct = new MemoryEntity<OneWordStruct>("SaveProgrammPage", this, 0x4000);
          
        }

        private void InitNew()
        {
            int slotLen = this.MB.BaudeRate == BAUDE_RATE_MAX ? 1024 : 64;

            this.ConfigurationNew = new MemoryEntity<ConfigurationStructNew>("Конфигурация устройства", this, 0x1000, slotLen);
            this._dateTime = new MemoryEntity<DateTimeStruct>("Время и дата в устройстве", this, 0x0200);
            this._groupUstavkiNew = new MemoryEntity<OneWordStruct>("Группа уставок", this, 0x0400);
            this._discretsNew = new MemoryEntity<DiscretDataBaseStructNew>("Дискретная БД", this, DISCRET_DATABASE_START_ADRESS, slotLen);

            this.AllChannelsNew = new MemoryEntity<OscopeAllChannelsStructNew>("Уставки осциллографа", this, 0x1A90);
            this.RefreshOscJournalNew = new MemoryEntity<OneWordStruct>("Индекс журнала осциллографа", this, 0x0800);
            this.OscJournalNew = new MemoryEntity<OscJournalStructNew>("Журнал осциллографа", this, 0x0800);
            this.OscPageNew = new MemoryEntity<OscPage>("Страница осциллографа", this, 0x900);
            this.OscOptionsNew = new MemoryEntity<OscOptionsStructNew>("Параметры осциллографа", this, 0x05A0);
            this.SetOscStartPageNew = new MemoryEntity<SetOscStartPageStruct>("Установка стартовой страницы осциллограммы", this, 0x900);

            this.SourceProgramStructNew = new MemoryEntity<SourceProgramStruct>("SaveProgram", this, 0x4300, slotLen);
            this.ProgramSignalsStructNew = new MemoryEntity<LogicProgramSignals>("LoadProgramSignals", this, 0x4100, slotLen);
            this.ProgramPageStructNew = new MemoryEntity<OneWordStruct>("SaveProgrammPage", this, 0x4000, slotLen);
            this.ProgramStartStructNew = new MemoryEntity<StartStruct>("SaveProgramStart", this, 0x0E00);
            this._stopSpl = new MemoryEntity<OneWordStruct>("Останов логической программы", this, 0x0D0C);
            this._startSpl = new MemoryEntity<OneWordStruct>("Старт логической программы", this, 0x0D0D);

            this._saveSysJournalIndex = new MemoryEntity<OneWordStruct>("Обновление журнала системы", this, SYSTEM_JOURNAL_START_ADRESS);
            this.RefreshSysJournalNew = new MemoryEntity<OneWordStruct>("Обновление журнала системы", this, SYSTEM_JOURNAL_START_ADRESS);
            this._systemJournalNew = new MemoryEntity<SystemJournalStructNew>("Запись журнала системы", this, SYSTEM_JOURNAL_START_ADRESS);

            //if (this.MB.BaudeRate == BAUDE_RATE_MAX)
            //{
            //    //this.AllAlarmJournalNew = new MemoryEntity<AllAlarmJournalStruct>("Журнал аварий", this, 0x0700, slotLen);
            //    this._full1024 = new MemoryEntity<FullSystemJournalStruct1024>("Запись журнала системы 1024", this, 0x0600, slotLen);
            //    this._full64 = null;
            //}
            //else
            //{
            //    this._full1024 = null;
            //    this._full64 = new MemoryEntity<FullSystemJournalStruct64>("Запись журнала системы 64", this, 0x0600);
            //    //this.AlarmJournalNew = new MemoryEntity<AlarmJournalRecordStructNew>("Запись журнала аварий", this, 0x700, slotLen);
            //}

            this.AllAlarmJournalNew = new MemoryEntity<AllAlarmJournalStruct>("Журнал аварий", this, 0x0700, slotLen);
            this.AlarmJournalNew = new MemoryEntity<AlarmJournalRecordStructNew>("Запись журнала аварий", this, 0x700);
            this.RefreshAlarmJournalNew = new MemoryEntity<OneWordStruct>("Обновление журнала аварий", this, 0x0700);

            this._writeSructEmulation = new MemoryEntity<WriteStructEmul>("Запись аналоговых сигналов ", this, 0x5800, slotLen);
            this._writeSructEmulationNull = new MemoryEntity<WriteStructEmul>("Запись нулевых аналоговых сигналов ", this, 0x5800, slotLen);
            this._readSructEmulation = new MemoryEntity<ReadStructEmul>("Чтение времени и статуса эмуляции", this, 0x580A, slotLen);

            this.SetPageJa = new MemoryEntity<OneWordStruct>("Страница ЖА", this, 0x700, slotLen);
        }

        #endregion

        #region Properties

        public MemoryEntity<OscopeAllChannelsStructNew> AllChannelsNew { get; private set; }

        public MemoryEntity<OneWordStruct> RefreshOscJournalNew { get; private set; }

        public MemoryEntity<OscJournalStructNew> OscJournalNew { get; private set; }

        public MemoryEntity<OscPage> OscPageNew { get; private set; }

        public MemoryEntity<OscOptionsStructNew> OscOptionsNew { get; private set; }

        public MemoryEntity<SetOscStartPageStruct> SetOscStartPageNew { get; private set; }

        public MemoryEntity<SystemJournalStructNew> SystemJournalNew => this._systemJournalNew;

        public MemoryEntity<OneWordStruct> SaveSysJournalIndex => this._saveSysJournalIndex;

        public MemoryEntity<FullSystemJournalStruct1024> Full1SysJournal024 => this._full1024;

        public MemoryEntity<FullSystemJournalStruct64> FullSysJournal64 => this._full64;

        public MemoryEntity<ConfigurationStructNew> ConfigurationNew { get; private set; }

        public MemoryEntity<StartStruct> ProgramStartStructNew { get; private set; }

        public MemoryEntity<SourceProgramStruct> SourceProgramStructNew { get; private set; }

        public MemoryEntity<LogicProgramSignals> ProgramSignalsStructNew { get; private set; }

        public MemoryEntity<OneWordStruct> ProgramPageStructNew { get; private set; }

        public MemoryEntity<AllAlarmJournalStruct> AllAlarmJournalNew { get; private set; }

        public MemoryEntity<AlarmJournalRecordStructNew> AlarmJournalNew { get; private set; }

        public MemoryEntity<OneWordStruct> SetPageJa { get; private set; }

        public MemoryEntity<OneWordStruct> RefreshAlarmJournalNew { get; private set; }

        public MemoryEntity<OneWordStruct> RefreshSysJournalNew { get; private set; }

        public MemoryEntity<WriteStructEmul> WriteStructEmulation
        {
            get { return this._writeSructEmulation; }
        }

        public MemoryEntity<WriteStructEmul> WriteStructEmulationNull
        {
            get { return this._writeSructEmulationNull; }
        }
        public MemoryEntity<ReadStructEmul> ReadStructEmulation
        {
            get { return this._readSructEmulation; }
        }

        public MemoryEntity<DiscretDataBaseStructNew> DiscretNew
        {
            get { return this._discretsNew; }
        }

        public MemoryEntity<AlarmJournalRecordStruct> AlarmJournal
        {
            get { return this._alarmJournal; }
        }

        public MemoryEntity<OneWordStruct> RefreshAlarmJournal
        {
            get { return this._refreshAlarmJournal; }
        }

        public MemoryEntity<SystemJournalStruct> SystemJournal
        {
            get { return this._systemJournal; }
        }

        public MemoryEntity<OneWordStruct> RefreshSysJournal
        {
            get { return this._refreshSysJournal; }
        }

        public MemoryEntity<ConfigurationStruct> Configuration
        {
            get { return this._configuration; }
        }
        public MemoryEntity<DateTimeStruct> DateTime
        {
            get { return this._dateTime; }
        }

        public MemoryEntity<OneWordStruct> GroupUstavkiNew
        {
            get { return this._groupUstavkiNew; }
        }

        public MemoryEntity<OneWordStruct> GroupUstavki
        {
            get { return this._groupUstavki; }
        }
        
        public MemoryEntity<DiscretDataBaseStruct> DiscretDataBase
        {
            get { return this._discretDataBase; }
        }

        public MemoryEntity<OneWordStruct> RefreshOscJournal
        {
            get { return this._refreshOscJournal; }
        }

        public MemoryEntity<OscPage> OscPage
        {
            get { return this._oscPage; }
        }

        public MemoryEntity<OscJournalStruct> OscJournal
        {
            get { return this._oscJournal; }
        }

        public MemoryEntity<SetOscStartPageStruct> SetOscStartPage
        {
            get { return this._setOscStartPage; }
        }

        public MemoryEntity<OscOptionsStruct> OscOptions
        {
            get { return this._oscOptions; }
        }

        public MemoryEntity<OscopeAllChannelsStruct> AllChannels
        {
            get { return this._allChannels; }
        }

        public ushort GetStartAddrMeasTrans(int group)
        {
            if (string.IsNullOrEmpty(DeviceVersion)) return MEASURE_START_ADDRESS;
            return (ushort)(MEASURE_START_ADDRESS + this._groupSetPointSize * group);

            //return (ushort)(MEASURE_START_ADDRESS + GROUP_SETPOINT_SIZE * group);
        }

        #endregion

        #region Methods
        private void CompleteExchange(object sender, Query query)
        {
            if (query.name == "version" + this.DeviceNumber)
            {
                this.SetStartConnectionState(!query.error);
                DeviceInfo info = DeviceInfo.GetVersion(Common.SwapArrayItems(query.readBuffer));
                this.Info = info;
                if (Common.VersionConverter(info.Version) >= 3.03 && !_isCheckVersion)
                {
                    _isCheckVersion = true;
                    this._infoSlot = new slot(0x500, 0x500 + 0x20);
                    this.MB.RemoveQuery("version" + this.DeviceNumber);
                    base.LoadVersion(this);
                    return;
                }
                this.LoadVersionOk();
                _isCheckVersion = false;
                return;
            }
            this.InitDeviceNumbers();
            if ("МР761OBR запрос подтверждения" + this.DeviceNumber == query.name)
            {
                if (query.error)
                {
                    if (this.ConfigWriteFail != null)
                    {
                        this.ConfigWriteFail.Invoke();
                    }
                }
                else
                {
                    if (this.ConfigWriteOk != null)
                    {
                        this.ConfigWriteOk.Invoke();
                    }
                }
            }
            if (query.name == "ConfirmConfig" + this.DeviceNumber)
            {
                if (query.error)
                {
                    MessageBox.Show("Не удалось записать бит подтверждения изменения конфигурации. Конфигурация в устройстве не изменена",
                        "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    MessageBox.Show("Конфигурация записана успешно", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            base.mb_CompleteExchange(sender, query);
        }

        private void InitDeviceNumbers()
        {
            if (this._devicesInitialised || this._memoryMap == null) return;
            foreach (StObj item in this._memoryMap.Values)
            {
                item.DeviceNum = this.DeviceNumber;
            }
            this._devicesInitialised = true;
        }

        #endregion

        #region [IDeviceVersion members]

        public const string T0N0D74R35 = "T0N0D74R35";
        public const string T0N0D106R67 = "T0N0D106R67";
        public const string T0N0D114R59 = "T0N0D114R59";

        private static readonly string[] _deviceApparatConfig =
        {
            T0N0D74R35,
            T0N0D106R67,
            T0N0D114R59
        };

        private static readonly string[] _deviceOutString = { T0N0D74R35, T0N0D106R67, T0N0D114R59 };

        public static string GetConfigDevString(string deviceOutString)
        {
            int ind = _deviceOutString.ToList().IndexOf(deviceOutString);
            return _deviceApparatConfig[ind];
        }

        public Type[] Forms
        {
            get
            {
                StringsConfigNew.CurrentVersion = Common.VersionConverter(DeviceVersion);
                
                if (StringsConfigNew.CurrentVersion >= 3.00)
                {
                    //if ((!this.DeviceDlgInfo.IsConnectionMode || !this.IsConnect) && !Framework.Framework.IsProjectOpening)
                    //{
                    //    ChoiceDeviceType choice = new ChoiceDeviceType(_deviceApparatConfig, _deviceOutString);
                    //    choice.ShowDialog();
                    //    DevicePlant = choice.DeviceType;
                    //}

                    this._groupSetPointSize = new GroupSetpointStructNew()
                        .GetStructInfo(this.MB.BaudeRate == BAUDE_RATE_MAX ? 1024 : 64).FullSize;

                    InitNew();

                    return new[]
                    {
                        typeof(BSBGLEFNEW),
                        typeof(ConfigurationFormNew),
                        typeof(AlarmJournalFormNew),
                        typeof(Mr761ObrNewSystemJournalForm),
                        typeof(Mr761ObrMeasuringFormNew),
                        typeof(EmulationFormNew),
                        typeof(OscilloscopeFormNew)
                    };

                }
                
                InitMemoryEntity();
                return new[]
                {
                    typeof(BSBGLEF),
                    typeof(AlarmJournalForm),
                    typeof(SystemJournalForm),
                    typeof(ConfigurationForm),
                    typeof(MeasuringForm),
                    typeof(OscilloscopeForm)
                };
                
            }
        }

        public List<string> Versions
        {
            get
            {
                return new List<string>
                {
                    "1.00",
                    "1.01",
                    "3.00"
                };
            }
        }
        #endregion

        #region [IDeviceView members]
        public Type ClassType
        {
            get { return typeof(Mr761ObrDevice); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Framework.Properties.Resources.mrBig; }
        }

        public string NodeName
        {
            get { return NODE_NAME; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[]{}; }
        }

        public bool Deletable
        {
            get { return true; }
        }
        #endregion
    }
}
