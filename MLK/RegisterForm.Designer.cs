﻿namespace BEMN.MLK
{
    partial class RegisterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._ok = new System.Windows.Forms.Button();
            this._cancel = new System.Windows.Forms.Button();
            this._regName = new System.Windows.Forms.Label();
            this._regValue = new System.Windows.Forms.MaskedTextBox();
            this.SuspendLayout();
            // 
            // _ok
            // 
            this._ok.Location = new System.Drawing.Point(12, 44);
            this._ok.Name = "_ok";
            this._ok.Size = new System.Drawing.Size(75, 23);
            this._ok.TabIndex = 1;
            this._ok.Text = "Ok";
            this._ok.UseVisualStyleBackColor = true;
            this._ok.Click += new System.EventHandler(this._ok_Click);
            // 
            // _cancel
            // 
            this._cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._cancel.Location = new System.Drawing.Point(147, 44);
            this._cancel.Name = "_cancel";
            this._cancel.Size = new System.Drawing.Size(75, 23);
            this._cancel.TabIndex = 2;
            this._cancel.Text = "Cancel";
            this._cancel.UseVisualStyleBackColor = true;
            // 
            // _regName
            // 
            this._regName.AutoSize = true;
            this._regName.Location = new System.Drawing.Point(12, 9);
            this._regName.Name = "_regName";
            this._regName.Size = new System.Drawing.Size(35, 13);
            this._regName.TabIndex = 3;
            this._regName.Text = "label1";
            // 
            // _regValue
            // 
            this._regValue.Location = new System.Drawing.Point(81, 6);
            this._regValue.Name = "_regValue";
            this._regValue.PromptChar = ' ';
            this._regValue.Size = new System.Drawing.Size(105, 20);
            this._regValue.TabIndex = 0;
            this._regValue.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this._regValue_KeyPress);
            this._regValue.KeyUp += new System.Windows.Forms.KeyEventHandler(this._regValue_KeyUp);
            this._regValue.Validating += new System.ComponentModel.CancelEventHandler(this._regValue_Validating);
            // 
            // RegisterForm
            // 
            this.AcceptButton = this._ok;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._cancel;
            this.ClientSize = new System.Drawing.Size(234, 79);
            this.Controls.Add(this._regValue);
            this.Controls.Add(this._regName);
            this.Controls.Add(this._cancel);
            this.Controls.Add(this._ok);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RegisterForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RegisterForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button _ok;
        private System.Windows.Forms.Button _cancel;
        private System.Windows.Forms.Label _regName;
        private System.Windows.Forms.MaskedTextBox _regValue;
    }
}