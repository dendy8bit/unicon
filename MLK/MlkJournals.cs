﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Linq;
using System.Xml;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Framework.Properties;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MLK.HelpClasses;
using BEMN.MLK.Structures;

namespace BEMN.MLK
{
    public partial class MlkJournals : Form, IFormView
    {
        #region Поля

        private MLK _device;
        private MemoryEntity<JournalHeader> _sysJournalHeader;
        private MemoryEntity<JournalHeader> _logicJournalHeader;
        private MemoryEntity<Journal> _journal;
        private DataTable _dataSysTable;
        private DataTable _dataLogicTable;
        private DataSet _data;
        private string _logicJournalPath;
        private int _messagesCount;
        private int _current;
        private ushort _sysStartAddress = 0x3804;
        private ushort _logicStartAddress = 0x4804;
        private bool _isReadFromXml;
        private bool _isLogic;
        #endregion

        #region Константы

        private const string NOTICE = "Внимание";
        private const string READING_SYS_JOURNAL = "Идет чтение системного журнала";
        private const string READING_LOGIC_JOURNAL = "Идет чтение журнала логической программы";
        private const string SYS_JOURNAL_IS_EMPTY = "Системный журнал пуст";
        private const string LOGIC_JOURNAL_IS_EMPTY = "Журнал логической программы пуст";
        private const string JOURNAL_READ_SUCCSESS = "Журнал прочитан успешно";
        private const string SYS_JOURNAL_READ_FAIL = "Невозможно прочитать системный журнал";
        private const string LOGIC_JOURNAL_READ_FAIL = "Невозможно прочитать журнал логической программы";
        private const string JOURNAL_READ_FAIL = "Невозможно прочитать журнал";
        private const string LOGIC_JOURNAL = "Журнал логической программы";
        private const string SYS_JOURNAL = "Системный журнал";
        private const string SYS_TABLE_NAME = "SystemJournal";
        private const string LOGIC_TABLE_NAME = "LogicJournal";

        private const string MLK_JOURNAL_MESSAGES_XML = "\\MlkJournal.xml";
        private const string MLK_JOURNAL_MESSAGES_SCHEMA = "\\MlkJournalSchema.xsd";
        private const string PATH = "\\path";
        #endregion

        #region Конструкторы

        public MlkJournals()
        {
            InitializeComponent();
        }

        public MlkJournals(MLK device)
        {
            InitializeComponent();
            _device = device;

            _sysJournalHeader = _device.SysJournalHeader;
            _sysJournalHeader.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, SysJournalHeaderLoadComplite);
            _sysJournalHeader.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _statusLabel.Text = SYS_JOURNAL_READ_FAIL;
                _statusLabel.BackColor = Color.Red;
                _readSysJournalButton.Enabled = true;
            });
            _sysJournalHeader.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _statusLabel.Text = "Системный журнал очищен успешно";
                _statusLabel.BackColor = Color.Green;
            });
            _sysJournalHeader.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _statusLabel.Text = "Невозможно очистить системный журнал";
                _statusLabel.BackColor = Color.Red;
            });

            _logicJournalHeader = _device.LogicJournalHeader;
            _logicJournalHeader.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, LogicJournalHeaderLoadComplite);
            _logicJournalHeader.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _statusLabel.Text = LOGIC_JOURNAL_READ_FAIL;
                _statusLabel.BackColor = Color.Red;
                _readLogicJournalButton.Enabled = true;
            });
            _logicJournalHeader.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _statusLabel.Text = "Журнал лигической программы очищен успешно";
                _statusLabel.BackColor = Color.Green;
            });
            _logicJournalHeader.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _statusLabel.Text = "Невозможно очистить журнал логической программы";
                _statusLabel.BackColor = Color.Red;
            });
        }

        #endregion

        #region Системный журнал

        private void SysJournalLoadHeader()
        {
            _sysJournalHeader.LoadStruct();
        }

        private void SysJournalHeaderLoadComplite()
        {
            _dataSysTable.Rows.Clear();
            _progressBar.Value = 0;

            _messagesCount = _sysJournalHeader.Value.Counter;
            _countOfSysMes.Text = string.Format("Количество записей в системном журнале: {0}", _messagesCount);
            _current = _sysJournalHeader.Value.CurrentMessage;

            _readSysJournalButton.Enabled = false;
            if (_messagesCount != 0)
            {
                _statusLabel.Text = READING_SYS_JOURNAL;
                _statusLabel.BackColor = Color.White;
                LoadJournal(SYS_JOURNAL, _sysJournalHeader.Caption, _sysStartAddress);
            }
            else
            {
                MessageBox.Show(SYS_JOURNAL_IS_EMPTY, NOTICE, MessageBoxButtons.OK, MessageBoxIcon.Information);
                _readSysJournalButton.Enabled = true;
            }
        }

        private void _systemJournalButton_Click(object sender, EventArgs e)
        {
            // Пытаемся открыть XML-файл системного журнала и прочитать его в DataSet
            // при неудаче читаем все с класса Strings
            _isLogic = false;
            TryReadXmlFromFile(Environment.CurrentDirectory + MLK_JOURNAL_MESSAGES_XML);
            _countOfSysMes.Text = string.Empty;
            SysJournalLoadHeader();           
        }

        private void _clearSysJourBnt_Click(object sender, EventArgs e)
        {
            _countOfSysMes.Text = string.Empty;
            _dataSysTable.Rows.Clear();
            JournalHeader conf = _sysJournalHeader.Value;
            conf.Counter = 0;
            conf.CurrentMessage = 0;
            _sysJournalHeader.Value = conf;
            _sysJournalHeader.SaveStruct();
        }

        #endregion

        #region Журнал логической программы

        private void LogicJournalLoadHeader()
        {
            _logicJournalHeader.LoadStruct();
        }

        private void LogicJournalHeaderLoadComplite()
        {
            _dataLogicTable.Rows.Clear();
            _progressBar.Value = 0;

            _messagesCount = _logicJournalHeader.Value.Counter;
            _countOfLogicMes.Text = string.Format("Количество записей в журнале логической программы: {0}", _messagesCount);
            _current = _logicJournalHeader.Value.CurrentMessage;

            _readLogicJournalButton.Enabled = false;
            if (_messagesCount != 0)
            {
                _statusLabel.Text = READING_LOGIC_JOURNAL;
                _statusLabel.BackColor = Color.White;
                LoadJournal(LOGIC_JOURNAL,_logicJournalHeader.Caption, _logicStartAddress);
            }
            else
            {
                MessageBox.Show(LOGIC_JOURNAL_IS_EMPTY, NOTICE, MessageBoxButtons.OK, MessageBoxIcon.Information);
                _readLogicJournalButton.Enabled = true;
            }
        }

        private void _readLogicJournalButton_Click(object sender, EventArgs e)
        {
            // Пытаемся открыть XML-файл журнала и прочитать его в DataSet
            // при неудаче читаем все с класса Strings
            _isLogic = true;
            TryReadXmlFromFile(_logicJournalPath);
            _countOfLogicMes.Text = string.Empty;
            LogicJournalLoadHeader();
        }

        private void _clearLogicJourBtn_Click(object sender, EventArgs e)
        {
            _countOfLogicMes.Text = string.Empty;
            _dataLogicTable.Rows.Clear();
            JournalHeader conf = _logicJournalHeader.Value;
            conf.Counter = 0;
            conf.CurrentMessage = 0;
            _logicJournalHeader.Value = conf;
            _logicJournalHeader.SaveStruct();
        }

        private void _openXml_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            try
            {
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    _logicJournalPath = dlg.FileName;
                    string path = Environment.CurrentDirectory + PATH;
                    File.WriteAllText(path, _logicJournalPath);
                }
            }
            catch (Exception er)
            {
                MessageBox.Show(er.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof (MLK); }
        }

        public bool Multishow { get; private set; }

        #endregion

        #region INodeView Members

        public Type ClassType
        {
            get { return typeof (MlkJournals); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.ja; }
        }

        public string NodeName
        {
            get { return "Журналы"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] {}; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        #region Общие функции и обработчики событий

        private void MLK_Journals_Load(object sender, EventArgs e)
        {
            _dataSysTable = GetJournalDataTable(SYS_TABLE_NAME, _sysJournalGrid);
            _dataLogicTable = GetJournalDataTable(LOGIC_TABLE_NAME, _logicJournalGrid);
            _dataSysTable.Columns[0].DataType = typeof (int);
            _dataLogicTable.Columns[0].DataType = typeof (int);
            _sysJournalGrid.DataSource = _dataSysTable;
            _logicJournalGrid.DataSource = _dataLogicTable;
            
            if (File.Exists(Environment.CurrentDirectory + "\\path"))
            {
                _logicJournalPath = File.ReadAllText(Environment.CurrentDirectory + PATH);
            }
        }

        private DataTable GetJournalDataTable(string name, DataGridView grid)
        {
            var table = new DataTable(name);
            for (int j = 0; j < grid.Columns.Count; j++)
            {
                table.Columns.Add(grid.Columns[j].Name);
            }
            return table;
        }

        private void LoadJournal(string journalName, string caption, ushort startAddress)
        {
            this._journal = _device.GetJournal(journalName, caption, this._messagesCount, startAddress);
            this._journal.ReadOk += HandlerHelper.CreateHandler(this, () => _progressBar.PerformStep());
            this._journal.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, JournalLoadComplite);
            this._journal.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this._statusLabel.Text = JOURNAL_READ_FAIL;
                this._statusLabel.BackColor = Color.Red;
            });
            this._journal.LoadStruct();

            this._progressBar.Maximum = _journal.Slots.Count;
            this._progressBar.Step = 1;
        }

        private void JournalLoadComplite()
        {
            DataTable table;
            if (!_isLogic)
            {
                _readSysJournalButton.Enabled = true;
                table = _dataSysTable;
            }
            else
            {
                _readLogicJournalButton.Enabled = true;
                table = _dataLogicTable;
            }

            List<JournalRep> journal;
            if (_messagesCount == _current)
            {
                journal = new List<JournalRep>(_journal.Value.Jrep);
                journal.Reverse();
                ReadJournal(table, journal);
            }
            else
            {
                journal = new List<JournalRep>();
                for (int i = _current - 1; i >= 0; i--)
                {
                    journal.Add(_journal.Value.Jrep[i]);
                }
                for (int i = _messagesCount - 1; i > _current - 1; i--)
                {
                    journal.Add(_journal.Value.Jrep[i]);
                }
                ReadJournal(table, journal);
            }
            _statusLabel.Text = JOURNAL_READ_SUCCSESS;
            _statusLabel.BackColor = Color.Green;
        }

        private void ReadJournal(DataTable table, List<JournalRep> journal)
        {
            for (int i = 0; i < journal.Count; i++)
            {
                try
                {
                    table.Rows.Add((table.Rows.Count + 1),
                        Strings.Date[journal[i].DayOfWeek - 1],
                        PrepareGridParam(journal[i].Day.ToString()) + "." +
                        PrepareGridParam(journal[i].Month.ToString()) + "." +
                        PrepareGridParam(journal[i].Year.ToString()),
                        PrepareGridParam(journal[i].Hour.ToString()) + ":" +
                        PrepareGridParam(journal[i].Min.ToString()) + ":" +
                        PrepareGridParam(journal[i].Sec.ToString()) + "." +
                        PrepareMlSec(journal[i].Msec.ToString().PadLeft(3, '0')),
                        GetMessage(journal[i].Message),
                        GetParameterString(journal[i].HighParam, journal[i].LowParam));
                }
                catch
                {
                    table.Rows.Add((table.Rows.Count + 1).ToString(),
                        "ОШИБКА",
                        "--" + "." +
                        "--" + "." +
                        "--",
                        "--" + ":" +
                        "--" + ":" +
                        "--" + "." +
                        "--",
                        "-");
                }
            }
        }

        private string GetMessage(ushort value)
        {
            string retStr = Convert.ToString(value, 16).PadLeft(4, '0').ToUpper();
            byte key1 = Common.HIBYTE(value);
            byte key2 = Common.LOBYTE(value);
            if (_isReadFromXml)
            {
                string currentStr = GetMessageFromXml(key1, key2);
                return currentStr == string.Empty ? retStr : currentStr;
            }
            else
            {
                if (!Strings.Modules.ContainsKey(key1)) return retStr;
                string currentStr = GetMessageFromStrings(key1, key2);
                return currentStr == string.Empty ? retStr : currentStr;
            }
        }

        private string GetMessageFromXml(byte key1, byte key2)
        {
            string ret = string.Empty;
            DataTable source = _data.Tables["source"];
            var exists = (from sour in source.AsEnumerable()
                where String.Equals(sour.Field<string>("id"), string.Format("0x{0}", key1.ToString("X").PadLeft(2, '0')), StringComparison.CurrentCultureIgnoreCase)
                select sour.Field<string>("name")).ToArray();
            if (exists.Length == 0)
                return ret;
            if (key2 >= 0x80 || _isLogic)
            {
                DataTable statuses = _data.Tables["statuses"];
                DataTable status = _data.Tables["status"];
                var statusesId = (from sour in source.AsEnumerable()
                                join stat in statuses.AsEnumerable()
                                    on sour.Field<int>("source_Id") equals stat.Field<int>("source_Id")
                                where
                                    sour.Field<string>("id") ==
                                    string.Format("0x{0}", key1.ToString("X").ToUpper().PadLeft(2, '0'))
                                select stat.Field<int>("statuses_Id")).ToArray();
                if (statusesId.Length == 0)
                    return ret;
                var message = (from st in status.AsEnumerable()
                    where
                        st.Field<int>("statuses_Id") == statusesId.ToArray()[0] &&
                        st.Field<string>("id") == string.Format("0x{0}", key2.ToString("X").ToUpper().PadLeft(2, '0'))
                    select st.Field<string>("status_Text")).ToArray();
                ret = string.Format("{0}: {1}", exists[0], message[0]);
            }
            else
            {
                DataTable errors = _data.Tables["errors"];
                DataTable error = _data.Tables["error"];
                var errorsId = (from sour in source.AsEnumerable()
                    join err in errors.AsEnumerable()
                        on sour.Field<int>("source_Id") equals err.Field<int>("source_Id")
                    where
                        sour.Field<string>("id") ==
                        string.Format("0x{0}", key1.ToString("X").ToUpper().PadLeft(2, '0'))
                    select err.Field<int>("errors_Id")).ToArray();
                if (errorsId.Length == 0)
                    return ret;
                var message = (from er in error.AsEnumerable()
                    where
                        er.Field<int>("errors_Id") == errorsId.ToArray()[0] &&
                        er.Field<string>("id") == string.Format("0x{0}", key2.ToString("X").ToUpper().PadLeft(2, '0'))
                    select er.Field<string>("error_Text")).ToArray();
                ret = string.Format("{0}: {1}", exists[0], message[0]);
            }       
            return ret;
        }

        private void TryReadXmlFromFile(string path)
        {
            try
            {
                _data = new DataSet();
                XmlReaderSettings settings = new XmlReaderSettings();
                settings.Schemas.Add(null, Environment.CurrentDirectory + MLK_JOURNAL_MESSAGES_SCHEMA);
                settings.ValidationType = ValidationType.Schema;
                XmlReader reader = XmlReader.Create(path, settings);
                _data.ReadXml(reader);
                _isReadFromXml = true;
            }
            catch (Exception)
            {
                MessageBox.Show(
                    "Файл MlkLogicJournal.xml отсутствует или поврежден. Сообщения журнала будут сгенерированы программой",
                    "Ошибка доступа", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                _isReadFromXml = false;
            }
        }

        private string GetMessageFromStrings(byte key1, byte key2)
        {
            string str = string.Empty;
            switch (key1)
            {
                case 0x00:
                    str = Strings.Modules[key1];
                    break;
                case 0x01:
                    str = Strings.Modules[key1];
                    break;
                case 0x02:
                    str = Strings.Modules[key1];
                    break;
                case 0x03:
                    str = Strings.Modules[key1];
                    break;
                case 0x04:
                    str = Strings.Modules[key1];
                    break;
                case 0x05:
                    str = Strings.RtcMessages.ContainsKey(key2)
                        ? string.Format("{0}: {1}", Strings.Modules[key1], Strings.RtcMessages[key2])
                        : str;
                    break;
                case 0x06:
                    str = Strings.Modules[key1];
                    break;
                case 0x07:
                    str = Strings.Usart0Messages.ContainsKey(key2)
                        ? string.Format("{0}: {1}", Strings.Modules[key1],
                            Strings.Usart0Messages[key2])
                        : str;
                    break;
                case 0x08:
                    str = Strings.BaseMessages.ContainsKey(key2)
                        ? string.Format("{0}: {1}", Strings.Modules[key1], Strings.BaseMessages[key2])
                        : str;
                    break;
                case 0x09:
                    str = Strings.BaseMessages.ContainsKey(key2)
                        ? string.Format("{0}: {1}", Strings.Modules[key1], Strings.BaseMessages[key2])
                        : str;
                    break;
                case 0x0A:
                    str = Strings.DisRelayExMessages.ContainsKey(key2)
                        ? string.Format("{0}: {1}", Strings.Modules[key1],
                            Strings.DisRelayExMessages[key2])
                        : str;
                    break;
                case 0x0B:
                    str = Strings.DisRelayExMessages.ContainsKey(key2)
                        ? string.Format("{0}: {1}", Strings.Modules[key1],
                            Strings.DisRelayExMessages[key2])
                        : str;
                    break;
                case 0x0C:
                    str = Strings.BaseMessages.ContainsKey(key2)
                        ? string.Format("{0}: {1}", Strings.Modules[key1], Strings.BaseMessages[key2])
                        : str;
                    break;
                case 0x10:
                    str = Strings.LogicMessages.ContainsKey(key2)
                        ? string.Format("{0}: {1}", Strings.Modules[key1],
                            Strings.LogicMessages[key2])
                        : str;
                    break;
                case 0x40:
                    str = Strings.BaseMessages.ContainsKey(key2)
                        ? Strings.BaseMessages[key2]
                        : str;
                    break;
                case 0xFE:
                    str = Strings.Modules[key1];
                    break;
                case 0xFF:
                    str = key2 == 0x01
                        ? "Ошибка при выключении устройства"
                        : "Выключение устройства";
                    break;
                default:
                    return str;               
            }
            return str;
        }

        private string GetParameterString(ushort high, ushort low)
        {
            string hStr = high.ToString("X").PadLeft(4, '0').ToUpper();
            string lStr = low.ToString("X").PadLeft(4, '0').ToUpper();
            return hStr + ',' + lStr;
        }
        private string PrepareGridParam(string param)
        {
            if (param.Length < 2)
            {
                param = "0" + param;
            }
            return param;
        }

        private string PrepareMlSec(string param)
        {
            if (param.Length < 3)
            {
                param = "0" + param;
            }
            return param;
        }
        private void _saveSysJour_Click(object sender, EventArgs e)
        {
            saveFileDialog.Filter = "Системный журнал МЛК|*.xml";
            if (_dataSysTable.Rows.Count == 0)
            {
                MessageBox.Show("Системный журнал пуст", "Сохранение журнала", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                return;
            }
            if (saveFileDialog.ShowDialog() != DialogResult.OK) return;
            _dataSysTable.WriteXml(saveFileDialog.FileName);
            saveFileDialog.FileName = string.Empty;
        }
        private void _saveLogicJour_Click(object sender, EventArgs e)
        {
            saveFileDialog.Filter = "Журнал логической программы МЛК|*.xml";
            if (_dataLogicTable.Rows.Count == 0)
            {
                MessageBox.Show("Журнал логической программы пуст", "Сохранение журнала", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                return;
            }
            if (saveFileDialog.ShowDialog() != DialogResult.OK) return;
            _dataLogicTable.WriteXml(saveFileDialog.FileName);
            saveFileDialog.FileName = string.Empty;
        }

        private void MlkJournals_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Maximized)
            {
                _closeBtn.Visible = _maximizeBtn.Visible = true;
            }
            else
            {
                _closeBtn.Visible = _maximizeBtn.Visible = false;
            }
        }

        private void _closeBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void _maximizeBtn_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
        }

        #endregion 
    }
}
