﻿using System;
using System.ComponentModel;
using System.Drawing;
using BEMN.Forms.Queries;
using BEMN.Framework.Properties;
using BEMN.Interfaces;

namespace BEMN.MLK
{
    public class MlkQueriesForm : QueriesForm, IFormView
    {
        #region Constructors

        public MlkQueriesForm() : base()
        {
            Multishow = true;
        }
        public MlkQueriesForm(MLK device) : base(device)
        {
            Multishow = true;
        }

        #endregion

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MLK); }
        }

        public bool Multishow { get; private set; }

        #endregion

        #region INodeView Members
        [Browsable(false)]
        public Type ClassType
        {
            get { return typeof(MlkQueriesForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.calibrate; }
        }

        public string NodeName
        {
            get { return "Обмены"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion
    }
}
