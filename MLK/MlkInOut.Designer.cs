﻿using BEMN.Forms;
namespace BEMN.MLK
{
    partial class MlkInOut
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this._inOutTabPage = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._writeDateTimeButt = new System.Windows.Forms.Button();
            this._dateTimeNowButt = new System.Windows.Forms.Button();
            this._astrStopCB = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this._astrTimeClockTB = new System.Windows.Forms.MaskedTextBox();
            this._astrDateClockTB = new System.Windows.Forms.MaskedTextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this._analogU = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this._errorsDG = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.acceptMessage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sendMessage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.communicationQuality = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tabControl1.SuspendLayout();
            this._inOutTabPage.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._errorsDG)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this._inOutTabPage);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(427, 434);
            this.tabControl1.TabIndex = 0;
            // 
            // _inOutTabPage
            // 
            this._inOutTabPage.Controls.Add(this.groupBox1);
            this._inOutTabPage.Controls.Add(this.groupBox5);
            this._inOutTabPage.Location = new System.Drawing.Point(4, 22);
            this._inOutTabPage.Name = "_inOutTabPage";
            this._inOutTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._inOutTabPage.Size = new System.Drawing.Size(419, 408);
            this._inOutTabPage.TabIndex = 0;
            this._inOutTabPage.Text = "Ввод - вывод";
            this._inOutTabPage.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._writeDateTimeButt);
            this.groupBox1.Controls.Add(this._dateTimeNowButt);
            this.groupBox1.Controls.Add(this._astrStopCB);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this._astrTimeClockTB);
            this.groupBox1.Controls.Add(this._astrDateClockTB);
            this.groupBox1.Location = new System.Drawing.Point(46, 315);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(329, 85);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Астрономическое время";
            // 
            // _writeDateTimeButt
            // 
            this._writeDateTimeButt.Enabled = false;
            this._writeDateTimeButt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._writeDateTimeButt.Location = new System.Drawing.Point(165, 56);
            this._writeDateTimeButt.Name = "_writeDateTimeButt";
            this._writeDateTimeButt.Size = new System.Drawing.Size(156, 23);
            this._writeDateTimeButt.TabIndex = 6;
            this._writeDateTimeButt.Text = "Установить";
            this._writeDateTimeButt.UseVisualStyleBackColor = true;
            this._writeDateTimeButt.Click += new System.EventHandler(this._writeDateTimeButt_Click);
            // 
            // _dateTimeNowButt
            // 
            this._dateTimeNowButt.Enabled = false;
            this._dateTimeNowButt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._dateTimeNowButt.Location = new System.Drawing.Point(165, 19);
            this._dateTimeNowButt.Name = "_dateTimeNowButt";
            this._dateTimeNowButt.Size = new System.Drawing.Size(156, 23);
            this._dateTimeNowButt.TabIndex = 5;
            this._dateTimeNowButt.Text = "Системные дата и время";
            this._dateTimeNowButt.UseVisualStyleBackColor = true;
            this._dateTimeNowButt.Click += new System.EventHandler(this._dateTimeNowButt_Click);
            // 
            // _astrStopCB
            // 
            this._astrStopCB.AutoSize = true;
            this._astrStopCB.Location = new System.Drawing.Point(12, 62);
            this._astrStopCB.Name = "_astrStopCB";
            this._astrStopCB.Size = new System.Drawing.Size(112, 17);
            this._astrStopCB.TabIndex = 4;
            this._astrStopCB.Text = "Изменить время";
            this._astrStopCB.UseVisualStyleBackColor = true;
            this._astrStopCB.CheckedChanged += new System.EventHandler(this._astrStopCB_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(68, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Время";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Дата";
            // 
            // _astrTimeClockTB
            // 
            this._astrTimeClockTB.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
            this._astrTimeClockTB.Location = new System.Drawing.Point(71, 34);
            this._astrTimeClockTB.Mask = "90:00:00";
            this._astrTimeClockTB.Name = "_astrTimeClockTB";
            this._astrTimeClockTB.Size = new System.Drawing.Size(51, 20);
            this._astrTimeClockTB.TabIndex = 1;
            this._astrTimeClockTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _astrDateClockTB
            // 
            this._astrDateClockTB.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
            this._astrDateClockTB.Location = new System.Drawing.Point(12, 34);
            this._astrDateClockTB.Mask = "00/00/00";
            this._astrDateClockTB.Name = "_astrDateClockTB";
            this._astrDateClockTB.Size = new System.Drawing.Size(49, 20);
            this._astrDateClockTB.TabIndex = 0;
            this._astrDateClockTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this._analogU);
            this.groupBox5.Controls.Add(this.label3);
            this.groupBox5.Location = new System.Drawing.Point(6, 6);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(299, 53);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Аналоговый вход";
            // 
            // _analogU
            // 
            this._analogU.Enabled = false;
            this._analogU.Location = new System.Drawing.Point(115, 17);
            this._analogU.Name = "_analogU";
            this._analogU.Size = new System.Drawing.Size(178, 20);
            this._analogU.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Текущее значение:";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.groupBox4);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(419, 408);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Cтатистика";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this._errorsDG);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Location = new System.Drawing.Point(0, 0);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(419, 408);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Регистры логической программы";
            // 
            // _errorsDG
            // 
            this._errorsDG.AllowUserToAddRows = false;
            this._errorsDG.AllowUserToDeleteRows = false;
            this._errorsDG.BackgroundColor = System.Drawing.Color.White;
            this._errorsDG.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._errorsDG.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.acceptMessage,
            this.sendMessage,
            this.communicationQuality,
            this.status});
            this._errorsDG.Dock = System.Windows.Forms.DockStyle.Fill;
            this._errorsDG.Location = new System.Drawing.Point(3, 16);
            this._errorsDG.Name = "_errorsDG";
            this._errorsDG.RowHeadersVisible = false;
            this._errorsDG.Size = new System.Drawing.Size(413, 389);
            this._errorsDG.TabIndex = 16;
            // 
            // dataGridViewTextBoxColumn1
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewTextBoxColumn1.Frozen = true;
            this.dataGridViewTextBoxColumn1.HeaderText = "№";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 30;
            // 
            // acceptMessage
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.acceptMessage.DefaultCellStyle = dataGridViewCellStyle2;
            this.acceptMessage.HeaderText = "Принято правильных сообщений";
            this.acceptMessage.Name = "acceptMessage";
            this.acceptMessage.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.acceptMessage.Width = 80;
            // 
            // sendMessage
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.sendMessage.DefaultCellStyle = dataGridViewCellStyle3;
            this.sendMessage.HeaderText = "Послано сообщений";
            this.sendMessage.Name = "sendMessage";
            this.sendMessage.Width = 80;
            // 
            // communicationQuality
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.communicationQuality.DefaultCellStyle = dataGridViewCellStyle4;
            this.communicationQuality.HeaderText = "Процент по связи";
            this.communicationQuality.Name = "communicationQuality";
            this.communicationQuality.Width = 80;
            // 
            // status
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.status.DefaultCellStyle = dataGridViewCellStyle5;
            this.status.HeaderText = "Ошибки в реальном времени";
            this.status.Name = "status";
            this.status.Width = 180;
            // 
            // _contextMenu
            // 
            this._contextMenu.Name = "_contextMenu";
            this._contextMenu.Size = new System.Drawing.Size(61, 4);
            // 
            // MlkInOut
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(427, 434);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MlkInOut";
            this.Text = "Ввод-вывод";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MLK_InOut_FormClosing);
            this.Load += new System.EventHandler(this.MLK_InOut_Load);
            this.tabControl1.ResumeLayout(false);
            this._inOutTabPage.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._errorsDG)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage _inOutTabPage;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DataGridView _errorsDG;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox _discretsGroup;
        private System.Windows.Forms.GroupBox _relayGroup;
        private System.Windows.Forms.GroupBox _diodGroup;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button _writeDateTimeButt;
        private System.Windows.Forms.Button _dateTimeNowButt;
        private System.Windows.Forms.CheckBox _astrStopCB;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MaskedTextBox _astrTimeClockTB;
        private System.Windows.Forms.MaskedTextBox _astrDateClockTB;
        private System.Windows.Forms.TextBox _analogU;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn acceptMessage;
        private System.Windows.Forms.DataGridViewTextBoxColumn sendMessage;
        private System.Windows.Forms.DataGridViewTextBoxColumn communicationQuality;
        private System.Windows.Forms.DataGridViewTextBoxColumn status;
        private System.Windows.Forms.ContextMenuStrip _contextMenu;


    }
}