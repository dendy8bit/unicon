﻿using BEMN.Forms.RichTextBox;
using BEMN.MLK;

namespace BEMN.MLK
{
    partial class MlkConfiguration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            this._readButton = new System.Windows.Forms.Button();
            this._configurationTabControl = new System.Windows.Forms.TabControl();
            this._USBPage = new System.Windows.Forms.TabPage();
            this._USBWrite = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this._USBdataBitsCombo = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this._USBToSendAfterTB = new System.Windows.Forms.MaskedTextBox();
            this._USBToSendBeforeTB = new System.Windows.Forms.MaskedTextBox();
            this._USBAddressTB = new System.Windows.Forms.MaskedTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this._USBdoubleSpeedCombo = new System.Windows.Forms.ComboBox();
            this._USBparitetCHETCombo = new System.Windows.Forms.ComboBox();
            this._USBparitetYNCombo = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this._USBstopBitsCombo = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this._USBspeedsCombo = new System.Windows.Forms.ComboBox();
            this._RS485Page = new System.Windows.Forms.TabPage();
            this._rs485Write = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this._rs485modeCheck = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this._rs485dataBitsCombo = new System.Windows.Forms.ComboBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this._rs485AnswerTB = new System.Windows.Forms.MaskedTextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this._rs485ToSendAfterTB = new System.Windows.Forms.MaskedTextBox();
            this._rs485ToSendBeforeTB = new System.Windows.Forms.MaskedTextBox();
            this._rs485ToSendTB = new System.Windows.Forms.MaskedTextBox();
            this.label17 = new System.Windows.Forms.Label();
            this._rs485AddressTB = new System.Windows.Forms.MaskedTextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this._rs485doubleSpeedCombo = new System.Windows.Forms.ComboBox();
            this._rs485paritetCHETCombo = new System.Windows.Forms.ComboBox();
            this._rs485paritetYNCombo = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this._rs485stopBitsCombo = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this._rs485speedsCombo = new System.Windows.Forms.ComboBox();
            this._querysPage = new System.Windows.Forms.TabPage();
            this.label29 = new System.Windows.Forms.Label();
            this._querysCountBox = new System.Windows.Forms.MaskedTextBox();
            this.label23 = new System.Windows.Forms.Label();
            this._querysWrite = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this._querys = new System.Windows.Forms.DataGridView();
            this._indexCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._timeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._msgCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._commandCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._masterAddrCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._slaveAddrCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._numCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._analogPage = new System.Windows.Forms.TabPage();
            this.signalGroupBox = new System.Windows.Forms.GroupBox();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this._highLimitSignal = new System.Windows.Forms.MaskedTextBox();
            this._lowLimitSignal = new System.Windows.Forms.MaskedTextBox();
            this._calibrovkaBtn = new System.Windows.Forms.Button();
            this._gisterezisBtn = new System.Windows.Forms.Button();
            this._readFromDevice = new System.Windows.Forms.Button();
            this.outpValueGroupBox = new System.Windows.Forms.GroupBox();
            this._rBtn3 = new System.Windows.Forms.RadioButton();
            this._rBtn1 = new System.Windows.Forms.RadioButton();
            this._rBtn2 = new System.Windows.Forms.RadioButton();
            this.amplitudeGroupBox = new System.Windows.Forms.GroupBox();
            this.label44 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this._highLimit = new System.Windows.Forms.MaskedTextBox();
            this._lowLimit = new System.Windows.Forms.MaskedTextBox();
            this._descriptionGroup = new System.Windows.Forms.GroupBox();
            this._descriptionBox = new BEMN.Forms.RichTextBox.AdvRichTextBox();
            this._statusStep = new System.Windows.Forms.Label();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this._processed = new System.Windows.Forms.TextBox();
            this._limit = new System.Windows.Forms.MaskedTextBox();
            this.label30 = new System.Windows.Forms.Label();
            this._Uin = new System.Windows.Forms.MaskedTextBox();
            this._ADC = new System.Windows.Forms.MaskedTextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this._peripheryWrite = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this._koeffpA = new System.Windows.Forms.MaskedTextBox();
            this._koeffA = new System.Windows.Forms.MaskedTextBox();
            this._koeffB = new System.Windows.Forms.MaskedTextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this._clockPage = new System.Windows.Forms.TabPage();
            this._calendarSettingsGroup = new System.Windows.Forms.GroupBox();
            this._timeZoneCombo = new System.Windows.Forms.ComboBox();
            this._summerTime = new System.Windows.Forms.CheckBox();
            this._acceptBtn = new System.Windows.Forms.Button();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this._correctionButton = new System.Windows.Forms.Button();
            this._decRB = new System.Windows.Forms.RadioButton();
            this._incRB = new System.Windows.Forms.RadioButton();
            this.label12 = new System.Windows.Forms.Label();
            this._correctionTextBox = new System.Windows.Forms.MaskedTextBox();
            this._versionPage = new System.Windows.Forms.TabPage();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this._ldrRevisionLabel = new System.Windows.Forms.Label();
            this._ldrFuseProcLabel = new System.Windows.Forms.Label();
            this._ldrCodProcLabel = new System.Windows.Forms.Label();
            this._ldrNameLabel = new System.Windows.Forms.Label();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this._devVersionLabel = new System.Windows.Forms.Label();
            this._devModLabel = new System.Windows.Forms.Label();
            this._devPodtipLabel = new System.Windows.Forms.Label();
            this._devNameLabel = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._appVersionLabel = new System.Windows.Forms.Label();
            this._appModLabel = new System.Windows.Forms.Label();
            this._appPodtipLabel = new System.Windows.Forms.Label();
            this._appNameLabel = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this._statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._toolTip = new System.Windows.Forms.ToolTip(this.components);
            this._writeAllButton = new System.Windows.Forms.Button();
            this._configurationTabControl.SuspendLayout();
            this._USBPage.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this._RS485Page.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this._querysPage.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._querys)).BeginInit();
            this._analogPage.SuspendLayout();
            this.signalGroupBox.SuspendLayout();
            this.outpValueGroupBox.SuspendLayout();
            this.amplitudeGroupBox.SuspendLayout();
            this._descriptionGroup.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this._clockPage.SuspendLayout();
            this._calendarSettingsGroup.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this._versionPage.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _readButton
            // 
            this._readButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._readButton.Location = new System.Drawing.Point(12, 399);
            this._readButton.Name = "_readButton";
            this._readButton.Size = new System.Drawing.Size(75, 23);
            this._readButton.TabIndex = 0;
            this._readButton.Text = "Прочитать";
            this._readButton.UseVisualStyleBackColor = true;
            this._readButton.Click += new System.EventHandler(this._readButton_Click);
            // 
            // _configurationTabControl
            // 
            this._configurationTabControl.Controls.Add(this._USBPage);
            this._configurationTabControl.Controls.Add(this._RS485Page);
            this._configurationTabControl.Controls.Add(this._querysPage);
            this._configurationTabControl.Controls.Add(this._analogPage);
            this._configurationTabControl.Controls.Add(this._clockPage);
            this._configurationTabControl.Controls.Add(this._versionPage);
            this._configurationTabControl.Location = new System.Drawing.Point(1, 0);
            this._configurationTabControl.Name = "_configurationTabControl";
            this._configurationTabControl.SelectedIndex = 0;
            this._configurationTabControl.Size = new System.Drawing.Size(431, 393);
            this._configurationTabControl.TabIndex = 1;
            this._configurationTabControl.SelectedIndexChanged += new System.EventHandler(this._configurationTabControl_SelectedIndexChanged);
            // 
            // _USBPage
            // 
            this._USBPage.Controls.Add(this._USBWrite);
            this._USBPage.Controls.Add(this.groupBox3);
            this._USBPage.Location = new System.Drawing.Point(4, 22);
            this._USBPage.Name = "_USBPage";
            this._USBPage.Padding = new System.Windows.Forms.Padding(3);
            this._USBPage.Size = new System.Drawing.Size(423, 367);
            this._USBPage.TabIndex = 0;
            this._USBPage.Text = "USB";
            this._USBPage.UseVisualStyleBackColor = true;
            // 
            // _USBWrite
            // 
            this._USBWrite.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._USBWrite.Location = new System.Drawing.Point(7, 338);
            this._USBWrite.Name = "_USBWrite";
            this._USBWrite.Size = new System.Drawing.Size(75, 23);
            this._USBWrite.TabIndex = 10;
            this._USBWrite.Text = "Записать";
            this._USBWrite.UseVisualStyleBackColor = true;
            this._USBWrite.Click += new System.EventHandler(this._USBWrite_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this._USBdataBitsCombo);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this._USBToSendAfterTB);
            this.groupBox3.Controls.Add(this._USBToSendBeforeTB);
            this.groupBox3.Controls.Add(this._USBAddressTB);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this._USBdoubleSpeedCombo);
            this.groupBox3.Controls.Add(this._USBparitetCHETCombo);
            this.groupBox3.Controls.Add(this._USBparitetYNCombo);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this._USBstopBitsCombo);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this._USBspeedsCombo);
            this.groupBox3.Location = new System.Drawing.Point(7, 28);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(287, 210);
            this.groupBox3.TabIndex = 9;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Конфигурация USB";
            // 
            // _USBdataBitsCombo
            // 
            this._USBdataBitsCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._USBdataBitsCombo.FormattingEnabled = true;
            this._USBdataBitsCombo.Items.AddRange(new object[] {
            "1 бит",
            "2 бита"});
            this._USBdataBitsCombo.Location = new System.Drawing.Point(181, 42);
            this._USBdataBitsCombo.Name = "_USBdataBitsCombo";
            this._USBdataBitsCombo.Size = new System.Drawing.Size(97, 21);
            this._USBdataBitsCombo.TabIndex = 27;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 45);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(126, 13);
            this.label4.TabIndex = 26;
            this.label4.Text = "Количество бит данных";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 183);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(163, 13);
            this.label14.TabIndex = 25;
            this.label14.Text = "Таймаут после выдачи данных";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 164);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(145, 13);
            this.label13.TabIndex = 24;
            this.label13.Text = "Таймаут до выдачи данных";
            // 
            // _USBToSendAfterTB
            // 
            this._USBToSendAfterTB.Location = new System.Drawing.Point(181, 180);
            this._USBToSendAfterTB.Name = "_USBToSendAfterTB";
            this._USBToSendAfterTB.Size = new System.Drawing.Size(97, 20);
            this._USBToSendAfterTB.TabIndex = 23;
            this._USBToSendAfterTB.Tag = "255";
            this._USBToSendAfterTB.Text = "0";
            // 
            // _USBToSendBeforeTB
            // 
            this._USBToSendBeforeTB.Location = new System.Drawing.Point(181, 161);
            this._USBToSendBeforeTB.Name = "_USBToSendBeforeTB";
            this._USBToSendBeforeTB.Size = new System.Drawing.Size(97, 20);
            this._USBToSendBeforeTB.TabIndex = 22;
            this._USBToSendBeforeTB.Tag = "255";
            this._USBToSendBeforeTB.Text = "0";
            // 
            // _USBAddressTB
            // 
            this._USBAddressTB.Location = new System.Drawing.Point(181, 142);
            this._USBAddressTB.Name = "_USBAddressTB";
            this._USBAddressTB.Size = new System.Drawing.Size(97, 20);
            this._USBAddressTB.TabIndex = 19;
            this._USBAddressTB.Tag = "255";
            this._USBAddressTB.Text = "1";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 145);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(98, 13);
            this.label11.TabIndex = 18;
            this.label11.Text = "Адрес устройства";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 125);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(107, 13);
            this.label10.TabIndex = 17;
            this.label10.Text = "Удвоение скорости";
            // 
            // _USBdoubleSpeedCombo
            // 
            this._USBdoubleSpeedCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._USBdoubleSpeedCombo.FormattingEnabled = true;
            this._USBdoubleSpeedCombo.Items.AddRange(new object[] {
            "Без удвоения",
            "С удвоением"});
            this._USBdoubleSpeedCombo.Location = new System.Drawing.Point(181, 122);
            this._USBdoubleSpeedCombo.Name = "_USBdoubleSpeedCombo";
            this._USBdoubleSpeedCombo.Size = new System.Drawing.Size(97, 21);
            this._USBdoubleSpeedCombo.TabIndex = 16;
            // 
            // _USBparitetCHETCombo
            // 
            this._USBparitetCHETCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._USBparitetCHETCombo.FormattingEnabled = true;
            this._USBparitetCHETCombo.Items.AddRange(new object[] {
            "Четный",
            "Нечетный"});
            this._USBparitetCHETCombo.Location = new System.Drawing.Point(181, 102);
            this._USBparitetCHETCombo.Name = "_USBparitetCHETCombo";
            this._USBparitetCHETCombo.Size = new System.Drawing.Size(97, 21);
            this._USBparitetCHETCombo.TabIndex = 15;
            // 
            // _USBparitetYNCombo
            // 
            this._USBparitetYNCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._USBparitetYNCombo.FormattingEnabled = true;
            this._USBparitetYNCombo.Items.AddRange(new object[] {
            "Нет",
            "Есть"});
            this._USBparitetYNCombo.Location = new System.Drawing.Point(181, 82);
            this._USBparitetYNCombo.Name = "_USBparitetYNCombo";
            this._USBparitetYNCombo.Size = new System.Drawing.Size(97, 21);
            this._USBparitetYNCombo.TabIndex = 14;
            this._USBparitetYNCombo.SelectedIndexChanged += new System.EventHandler(this._USBparitetYNCombo_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 85);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 13);
            this.label9.TabIndex = 13;
            this.label9.Text = "Паритет";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 65);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(112, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "Количество стоп бит";
            // 
            // _USBstopBitsCombo
            // 
            this._USBstopBitsCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._USBstopBitsCombo.FormattingEnabled = true;
            this._USBstopBitsCombo.Items.AddRange(new object[] {
            "1 бит",
            "2 бита"});
            this._USBstopBitsCombo.Location = new System.Drawing.Point(181, 62);
            this._USBstopBitsCombo.Name = "_USBstopBitsCombo";
            this._USBstopBitsCombo.Size = new System.Drawing.Size(97, 21);
            this._USBstopBitsCombo.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 25);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(105, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Скорость передачи";
            // 
            // _USBspeedsCombo
            // 
            this._USBspeedsCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._USBspeedsCombo.FormattingEnabled = true;
            this._USBspeedsCombo.Items.AddRange(new object[] {
            "600",
            "1200",
            "2400",
            "4800",
            "9600",
            "19200",
            "38400",
            "76800",
            "900",
            "1800",
            "3600",
            "7200",
            "14400",
            "28800",
            "57600",
            "115200"});
            this._USBspeedsCombo.Location = new System.Drawing.Point(181, 22);
            this._USBspeedsCombo.Name = "_USBspeedsCombo";
            this._USBspeedsCombo.Size = new System.Drawing.Size(97, 21);
            this._USBspeedsCombo.TabIndex = 9;
            // 
            // _RS485Page
            // 
            this._RS485Page.Controls.Add(this._rs485Write);
            this._RS485Page.Controls.Add(this.groupBox4);
            this._RS485Page.Location = new System.Drawing.Point(4, 22);
            this._RS485Page.Name = "_RS485Page";
            this._RS485Page.Padding = new System.Windows.Forms.Padding(3);
            this._RS485Page.Size = new System.Drawing.Size(423, 367);
            this._RS485Page.TabIndex = 1;
            this._RS485Page.Text = "RS485";
            this._RS485Page.UseVisualStyleBackColor = true;
            // 
            // _rs485Write
            // 
            this._rs485Write.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._rs485Write.Location = new System.Drawing.Point(7, 338);
            this._rs485Write.Name = "_rs485Write";
            this._rs485Write.Size = new System.Drawing.Size(75, 23);
            this._rs485Write.TabIndex = 11;
            this._rs485Write.Text = "Записать";
            this._rs485Write.UseVisualStyleBackColor = true;
            this._rs485Write.Click += new System.EventHandler(this._rs485Write_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this._rs485modeCheck);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this._rs485dataBitsCombo);
            this.groupBox4.Controls.Add(this.label28);
            this.groupBox4.Controls.Add(this.label27);
            this.groupBox4.Controls.Add(this._rs485AnswerTB);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this._rs485ToSendAfterTB);
            this.groupBox4.Controls.Add(this._rs485ToSendBeforeTB);
            this.groupBox4.Controls.Add(this._rs485ToSendTB);
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Controls.Add(this._rs485AddressTB);
            this.groupBox4.Controls.Add(this.label18);
            this.groupBox4.Controls.Add(this.label19);
            this.groupBox4.Controls.Add(this._rs485doubleSpeedCombo);
            this.groupBox4.Controls.Add(this._rs485paritetCHETCombo);
            this.groupBox4.Controls.Add(this._rs485paritetYNCombo);
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Controls.Add(this.label21);
            this.groupBox4.Controls.Add(this._rs485stopBitsCombo);
            this.groupBox4.Controls.Add(this.label22);
            this.groupBox4.Controls.Add(this._rs485speedsCombo);
            this.groupBox4.Location = new System.Drawing.Point(7, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(405, 271);
            this.groupBox4.TabIndex = 10;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Конфигурация RS485";
            // 
            // _rs485modeCheck
            // 
            this._rs485modeCheck.AutoSize = true;
            this._rs485modeCheck.Location = new System.Drawing.Point(330, 18);
            this._rs485modeCheck.Name = "_rs485modeCheck";
            this._rs485modeCheck.Size = new System.Drawing.Size(45, 17);
            this._rs485modeCheck.TabIndex = 34;
            this._rs485modeCheck.Text = "Нет";
            this._rs485modeCheck.UseVisualStyleBackColor = true;
            this._rs485modeCheck.CheckedChanged += new System.EventHandler(this._rs485modeCheck_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 67);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(126, 13);
            this.label5.TabIndex = 33;
            this.label5.Text = "Количество бит данных";
            // 
            // _rs485dataBitsCombo
            // 
            this._rs485dataBitsCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._rs485dataBitsCombo.FormattingEnabled = true;
            this._rs485dataBitsCombo.Items.AddRange(new object[] {
            "1 бит",
            "2 бита"});
            this._rs485dataBitsCombo.Location = new System.Drawing.Point(181, 64);
            this._rs485dataBitsCombo.Name = "_rs485dataBitsCombo";
            this._rs485dataBitsCombo.Size = new System.Drawing.Size(97, 21);
            this._rs485dataBitsCombo.TabIndex = 32;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(6, 19);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(91, 13);
            this.label28.TabIndex = 31;
            this.label28.Text = "Режим главного";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(6, 245);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(96, 13);
            this.label27.TabIndex = 29;
            this.label27.Text = "Ожидание ответа";
            // 
            // _rs485AnswerTB
            // 
            this._rs485AnswerTB.Enabled = false;
            this._rs485AnswerTB.Location = new System.Drawing.Point(278, 242);
            this._rs485AnswerTB.Name = "_rs485AnswerTB";
            this._rs485AnswerTB.Size = new System.Drawing.Size(97, 20);
            this._rs485AnswerTB.TabIndex = 28;
            this._rs485AnswerTB.Tag = "65535";
            this._rs485AnswerTB.Text = "0";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 205);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(163, 13);
            this.label15.TabIndex = 25;
            this.label15.Text = "Таймаут после выдачи данных";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 186);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(145, 13);
            this.label16.TabIndex = 24;
            this.label16.Text = "Таймаут до выдачи данных";
            // 
            // _rs485ToSendAfterTB
            // 
            this._rs485ToSendAfterTB.Location = new System.Drawing.Point(181, 202);
            this._rs485ToSendAfterTB.Name = "_rs485ToSendAfterTB";
            this._rs485ToSendAfterTB.Size = new System.Drawing.Size(97, 20);
            this._rs485ToSendAfterTB.TabIndex = 23;
            this._rs485ToSendAfterTB.Tag = "255";
            this._rs485ToSendAfterTB.Text = "0";
            // 
            // _rs485ToSendBeforeTB
            // 
            this._rs485ToSendBeforeTB.Location = new System.Drawing.Point(181, 183);
            this._rs485ToSendBeforeTB.Name = "_rs485ToSendBeforeTB";
            this._rs485ToSendBeforeTB.Size = new System.Drawing.Size(97, 20);
            this._rs485ToSendBeforeTB.TabIndex = 22;
            this._rs485ToSendBeforeTB.Tag = "255";
            this._rs485ToSendBeforeTB.Text = "0";
            // 
            // _rs485ToSendTB
            // 
            this._rs485ToSendTB.Enabled = false;
            this._rs485ToSendTB.Location = new System.Drawing.Point(278, 223);
            this._rs485ToSendTB.Name = "_rs485ToSendTB";
            this._rs485ToSendTB.Size = new System.Drawing.Size(97, 20);
            this._rs485ToSendTB.TabIndex = 21;
            this._rs485ToSendTB.Tag = "255";
            this._rs485ToSendTB.Text = "0";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 226);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(100, 13);
            this.label17.TabIndex = 20;
            this.label17.Text = "Таймаут передачи";
            // 
            // _rs485AddressTB
            // 
            this._rs485AddressTB.Location = new System.Drawing.Point(181, 164);
            this._rs485AddressTB.Name = "_rs485AddressTB";
            this._rs485AddressTB.Size = new System.Drawing.Size(97, 20);
            this._rs485AddressTB.TabIndex = 19;
            this._rs485AddressTB.Tag = "255";
            this._rs485AddressTB.Text = "1";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 167);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(98, 13);
            this.label18.TabIndex = 18;
            this.label18.Text = "Адрес устройства";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 147);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(107, 13);
            this.label19.TabIndex = 17;
            this.label19.Text = "Удвоение скорости";
            // 
            // _rs485doubleSpeedCombo
            // 
            this._rs485doubleSpeedCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._rs485doubleSpeedCombo.FormattingEnabled = true;
            this._rs485doubleSpeedCombo.Items.AddRange(new object[] {
            "Без удвоения",
            "С удвоением"});
            this._rs485doubleSpeedCombo.Location = new System.Drawing.Point(181, 144);
            this._rs485doubleSpeedCombo.Name = "_rs485doubleSpeedCombo";
            this._rs485doubleSpeedCombo.Size = new System.Drawing.Size(97, 21);
            this._rs485doubleSpeedCombo.TabIndex = 16;
            // 
            // _rs485paritetCHETCombo
            // 
            this._rs485paritetCHETCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._rs485paritetCHETCombo.FormattingEnabled = true;
            this._rs485paritetCHETCombo.Items.AddRange(new object[] {
            "Четный",
            "Нечетный"});
            this._rs485paritetCHETCombo.Location = new System.Drawing.Point(181, 124);
            this._rs485paritetCHETCombo.Name = "_rs485paritetCHETCombo";
            this._rs485paritetCHETCombo.Size = new System.Drawing.Size(97, 21);
            this._rs485paritetCHETCombo.TabIndex = 15;
            // 
            // _rs485paritetYNCombo
            // 
            this._rs485paritetYNCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._rs485paritetYNCombo.FormattingEnabled = true;
            this._rs485paritetYNCombo.Items.AddRange(new object[] {
            "Нет",
            "Есть"});
            this._rs485paritetYNCombo.Location = new System.Drawing.Point(181, 104);
            this._rs485paritetYNCombo.Name = "_rs485paritetYNCombo";
            this._rs485paritetYNCombo.Size = new System.Drawing.Size(97, 21);
            this._rs485paritetYNCombo.TabIndex = 14;
            this._rs485paritetYNCombo.SelectedIndexChanged += new System.EventHandler(this._rs485paritetYNCombo_SelectedIndexChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 107);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(49, 13);
            this.label20.TabIndex = 13;
            this.label20.Text = "Паритет";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 87);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(112, 13);
            this.label21.TabIndex = 12;
            this.label21.Text = "Количество стоп бит";
            // 
            // _rs485stopBitsCombo
            // 
            this._rs485stopBitsCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._rs485stopBitsCombo.FormattingEnabled = true;
            this._rs485stopBitsCombo.Items.AddRange(new object[] {
            "1 бит",
            "2 бита"});
            this._rs485stopBitsCombo.Location = new System.Drawing.Point(181, 84);
            this._rs485stopBitsCombo.Name = "_rs485stopBitsCombo";
            this._rs485stopBitsCombo.Size = new System.Drawing.Size(97, 21);
            this._rs485stopBitsCombo.TabIndex = 11;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(6, 47);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(105, 13);
            this.label22.TabIndex = 10;
            this.label22.Text = "Скорость передачи";
            // 
            // _rs485speedsCombo
            // 
            this._rs485speedsCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._rs485speedsCombo.FormattingEnabled = true;
            this._rs485speedsCombo.Items.AddRange(new object[] {
            "600",
            "1200",
            "2400",
            "4800",
            "9600",
            "19200",
            "38400",
            "76800",
            "900",
            "1800",
            "3600",
            "7200",
            "14400",
            "28800",
            "57600",
            "115200"});
            this._rs485speedsCombo.Location = new System.Drawing.Point(181, 44);
            this._rs485speedsCombo.Name = "_rs485speedsCombo";
            this._rs485speedsCombo.Size = new System.Drawing.Size(97, 21);
            this._rs485speedsCombo.TabIndex = 9;
            // 
            // _querysPage
            // 
            this._querysPage.Controls.Add(this.label29);
            this._querysPage.Controls.Add(this._querysCountBox);
            this._querysPage.Controls.Add(this.label23);
            this._querysPage.Controls.Add(this._querysWrite);
            this._querysPage.Controls.Add(this.groupBox5);
            this._querysPage.Location = new System.Drawing.Point(4, 22);
            this._querysPage.Name = "_querysPage";
            this._querysPage.Size = new System.Drawing.Size(423, 367);
            this._querysPage.TabIndex = 2;
            this._querysPage.Text = "Запросы";
            this._querysPage.UseVisualStyleBackColor = true;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.label29.Location = new System.Drawing.Point(168, 14);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(40, 13);
            this.label29.TabIndex = 15;
            this.label29.Text = "[0 - 64]";
            // 
            // _querysCountBox
            // 
            this._querysCountBox.Location = new System.Drawing.Point(131, 11);
            this._querysCountBox.Name = "_querysCountBox";
            this._querysCountBox.Size = new System.Drawing.Size(37, 20);
            this._querysCountBox.TabIndex = 14;
            this._querysCountBox.Tag = "64";
            this._querysCountBox.Text = "0";
            this._querysCountBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._querysCountBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this._querysCount_KeyDown);
            this._querysCountBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this._querysCount_KeyPress);
            this._querysCountBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this._querysCount_KeyUp);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(7, 14);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(117, 13);
            this.label23.TabIndex = 13;
            this.label23.Text = "Количество запросов";
            // 
            // _querysWrite
            // 
            this._querysWrite.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._querysWrite.Location = new System.Drawing.Point(7, 338);
            this._querysWrite.Name = "_querysWrite";
            this._querysWrite.Size = new System.Drawing.Size(75, 23);
            this._querysWrite.TabIndex = 12;
            this._querysWrite.Text = "Записать";
            this._querysWrite.UseVisualStyleBackColor = true;
            this._querysWrite.Click += new System.EventHandler(this._querysWrite_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this._querys);
            this.groupBox5.Location = new System.Drawing.Point(4, 34);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(413, 301);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Запросы";
            // 
            // _querys
            // 
            this._querys.AllowUserToAddRows = false;
            this._querys.AllowUserToDeleteRows = false;
            this._querys.BackgroundColor = System.Drawing.Color.White;
            this._querys.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._querys.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._indexCol,
            this._timeCol,
            this._msgCol,
            this._commandCol,
            this._masterAddrCol,
            this._slaveAddrCol,
            this._numCol});
            this._querys.Dock = System.Windows.Forms.DockStyle.Fill;
            this._querys.Location = new System.Drawing.Point(3, 16);
            this._querys.Name = "_querys";
            this._querys.RowHeadersVisible = false;
            this._querys.Size = new System.Drawing.Size(407, 282);
            this._querys.TabIndex = 13;
            this._querys.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this._querys_CellBeginEdit);
            this._querys.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._querys_CellEndEdit);
            // 
            // _indexCol
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._indexCol.DefaultCellStyle = dataGridViewCellStyle1;
            this._indexCol.Frozen = true;
            this._indexCol.HeaderText = "№";
            this._indexCol.Name = "_indexCol";
            this._indexCol.ReadOnly = true;
            this._indexCol.Width = 30;
            // 
            // _timeCol
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._timeCol.DefaultCellStyle = dataGridViewCellStyle2;
            this._timeCol.HeaderText = "Фаза";
            this._timeCol.Name = "_timeCol";
            this._timeCol.Width = 40;
            // 
            // _msgCol
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._msgCol.DefaultCellStyle = dataGridViewCellStyle3;
            this._msgCol.HeaderText = "Адрес";
            this._msgCol.Name = "_msgCol";
            this._msgCol.Width = 40;
            // 
            // _commandCol
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._commandCol.DefaultCellStyle = dataGridViewCellStyle4;
            this._commandCol.HeaderText = "Команда";
            this._commandCol.Name = "_commandCol";
            this._commandCol.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._commandCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // _masterAddrCol
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._masterAddrCol.DefaultCellStyle = dataGridViewCellStyle5;
            this._masterAddrCol.HeaderText = "Адрес ведомого";
            this._masterAddrCol.Name = "_masterAddrCol";
            this._masterAddrCol.Width = 57;
            // 
            // _slaveAddrCol
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._slaveAddrCol.DefaultCellStyle = dataGridViewCellStyle6;
            this._slaveAddrCol.HeaderText = "Адрес ведущего";
            this._slaveAddrCol.Name = "_slaveAddrCol";
            this._slaveAddrCol.Width = 57;
            // 
            // _numCol
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._numCol.DefaultCellStyle = dataGridViewCellStyle7;
            this._numCol.HeaderText = "Количество параметров";
            this._numCol.Name = "_numCol";
            this._numCol.Width = 70;
            // 
            // _analogPage
            // 
            this._analogPage.Controls.Add(this.signalGroupBox);
            this._analogPage.Controls.Add(this._calibrovkaBtn);
            this._analogPage.Controls.Add(this._gisterezisBtn);
            this._analogPage.Controls.Add(this._readFromDevice);
            this._analogPage.Controls.Add(this.outpValueGroupBox);
            this._analogPage.Controls.Add(this.amplitudeGroupBox);
            this._analogPage.Controls.Add(this._descriptionGroup);
            this._analogPage.Controls.Add(this._statusStep);
            this._analogPage.Controls.Add(this.groupBox13);
            this._analogPage.Controls.Add(this._peripheryWrite);
            this._analogPage.Controls.Add(this.groupBox6);
            this._analogPage.Location = new System.Drawing.Point(4, 22);
            this._analogPage.Name = "_analogPage";
            this._analogPage.Size = new System.Drawing.Size(423, 367);
            this._analogPage.TabIndex = 3;
            this._analogPage.Text = "Аналоговый вход";
            this._analogPage.UseVisualStyleBackColor = true;
            // 
            // signalGroupBox
            // 
            this.signalGroupBox.Controls.Add(this.label45);
            this.signalGroupBox.Controls.Add(this.label46);
            this.signalGroupBox.Controls.Add(this._highLimitSignal);
            this.signalGroupBox.Controls.Add(this._lowLimitSignal);
            this.signalGroupBox.Location = new System.Drawing.Point(7, 125);
            this.signalGroupBox.Name = "signalGroupBox";
            this.signalGroupBox.Size = new System.Drawing.Size(175, 65);
            this.signalGroupBox.TabIndex = 19;
            this.signalGroupBox.TabStop = false;
            this.signalGroupBox.Text = "Гистерезис сигнала";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(6, 41);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(88, 13);
            this.label45.TabIndex = 10;
            this.label45.Text = "Верхний предел";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(6, 22);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(86, 13);
            this.label46.TabIndex = 9;
            this.label46.Text = "Нижний предел";
            // 
            // _highLimitSignal
            // 
            this._highLimitSignal.Location = new System.Drawing.Point(98, 38);
            this._highLimitSignal.Name = "_highLimitSignal";
            this._highLimitSignal.Size = new System.Drawing.Size(71, 20);
            this._highLimitSignal.TabIndex = 8;
            this._highLimitSignal.Tag = "255";
            this._highLimitSignal.Text = "0";
            // 
            // _lowLimitSignal
            // 
            this._lowLimitSignal.Location = new System.Drawing.Point(98, 19);
            this._lowLimitSignal.Name = "_lowLimitSignal";
            this._lowLimitSignal.Size = new System.Drawing.Size(71, 20);
            this._lowLimitSignal.TabIndex = 7;
            this._lowLimitSignal.Tag = "255";
            this._lowLimitSignal.Text = "0";
            // 
            // _calibrovkaBtn
            // 
            this._calibrovkaBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._calibrovkaBtn.Location = new System.Drawing.Point(231, 309);
            this._calibrovkaBtn.Name = "_calibrovkaBtn";
            this._calibrovkaBtn.Size = new System.Drawing.Size(181, 23);
            this._calibrovkaBtn.TabIndex = 21;
            this._calibrovkaBtn.Text = "Калибровка аналогового входа";
            this._calibrovkaBtn.UseVisualStyleBackColor = true;
            this._calibrovkaBtn.Click += new System.EventHandler(this._calibrovkaBtn_Click);
            // 
            // _gisterezisBtn
            // 
            this._gisterezisBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._gisterezisBtn.Location = new System.Drawing.Point(261, 338);
            this._gisterezisBtn.Name = "_gisterezisBtn";
            this._gisterezisBtn.Size = new System.Drawing.Size(151, 23);
            this._gisterezisBtn.TabIndex = 20;
            this._gisterezisBtn.Text = "Калибровка гистерезиса";
            this._gisterezisBtn.UseVisualStyleBackColor = true;
            this._gisterezisBtn.Click += new System.EventHandler(this._gisterezisBtn_Click);
            // 
            // _readFromDevice
            // 
            this._readFromDevice.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._readFromDevice.Location = new System.Drawing.Point(7, 309);
            this._readFromDevice.Name = "_readFromDevice";
            this._readFromDevice.Size = new System.Drawing.Size(175, 23);
            this._readFromDevice.TabIndex = 19;
            this._readFromDevice.Text = "Прочитать из уcтройства";
            this._readFromDevice.UseVisualStyleBackColor = true;
            this._readFromDevice.Click += new System.EventHandler(this._readFromDevice_Click);
            // 
            // outpValueGroupBox
            // 
            this.outpValueGroupBox.Controls.Add(this._rBtn3);
            this.outpValueGroupBox.Controls.Add(this._rBtn1);
            this.outpValueGroupBox.Controls.Add(this._rBtn2);
            this.outpValueGroupBox.Location = new System.Drawing.Point(7, 189);
            this.outpValueGroupBox.Name = "outpValueGroupBox";
            this.outpValueGroupBox.Size = new System.Drawing.Size(175, 76);
            this.outpValueGroupBox.TabIndex = 19;
            this.outpValueGroupBox.TabStop = false;
            this.outpValueGroupBox.Text = "Выводимое значение";
            // 
            // _rBtn3
            // 
            this._rBtn3.AutoSize = true;
            this._rBtn3.Location = new System.Drawing.Point(13, 55);
            this._rBtn3.Name = "_rBtn3";
            this._rBtn3.Size = new System.Drawing.Size(156, 17);
            this._rBtn3.TabIndex = 13;
            this._rBtn3.TabStop = true;
            this._rBtn3.Tag = "2";
            this._rBtn3.Text = "калиброванный диапазон";
            this._rBtn3.UseVisualStyleBackColor = true;
            this._rBtn3.CheckedChanged += new System.EventHandler(this._rBtn_CheckedChanged);
            // 
            // _rBtn1
            // 
            this._rBtn1.AutoSize = true;
            this._rBtn1.Location = new System.Drawing.Point(13, 15);
            this._rBtn1.Name = "_rBtn1";
            this._rBtn1.Size = new System.Drawing.Size(115, 17);
            this._rBtn1.TabIndex = 11;
            this._rBtn1.Tag = "0";
            this._rBtn1.Text = "преобразованное";
            this._rBtn1.UseVisualStyleBackColor = true;
            this._rBtn1.CheckedChanged += new System.EventHandler(this._rBtn_CheckedChanged);
            // 
            // _rBtn2
            // 
            this._rBtn2.AutoSize = true;
            this._rBtn2.Location = new System.Drawing.Point(13, 35);
            this._rBtn2.Name = "_rBtn2";
            this._rBtn2.Size = new System.Drawing.Size(60, 17);
            this._rBtn2.TabIndex = 12;
            this._rBtn2.Tag = "1";
            this._rBtn2.Text = "дельта";
            this._rBtn2.UseVisualStyleBackColor = true;
            this._rBtn2.CheckedChanged += new System.EventHandler(this._rBtn_CheckedChanged);
            // 
            // amplitudeGroupBox
            // 
            this.amplitudeGroupBox.Controls.Add(this.label44);
            this.amplitudeGroupBox.Controls.Add(this.label43);
            this.amplitudeGroupBox.Controls.Add(this._highLimit);
            this.amplitudeGroupBox.Controls.Add(this._lowLimit);
            this.amplitudeGroupBox.Location = new System.Drawing.Point(7, 58);
            this.amplitudeGroupBox.Name = "amplitudeGroupBox";
            this.amplitudeGroupBox.Size = new System.Drawing.Size(175, 65);
            this.amplitudeGroupBox.TabIndex = 18;
            this.amplitudeGroupBox.TabStop = false;
            this.amplitudeGroupBox.Text = "Гистерезис амплитуды";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(6, 41);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(88, 13);
            this.label44.TabIndex = 10;
            this.label44.Text = "Верхний предел";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(6, 22);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(86, 13);
            this.label43.TabIndex = 9;
            this.label43.Text = "Нижний предел";
            // 
            // _highLimit
            // 
            this._highLimit.Enabled = false;
            this._highLimit.Location = new System.Drawing.Point(98, 38);
            this._highLimit.Name = "_highLimit";
            this._highLimit.Size = new System.Drawing.Size(71, 20);
            this._highLimit.TabIndex = 8;
            this._highLimit.Tag = "65535";
            this._highLimit.Text = "0";
            // 
            // _lowLimit
            // 
            this._lowLimit.Enabled = false;
            this._lowLimit.Location = new System.Drawing.Point(98, 19);
            this._lowLimit.Name = "_lowLimit";
            this._lowLimit.Size = new System.Drawing.Size(71, 20);
            this._lowLimit.TabIndex = 7;
            this._lowLimit.Tag = "65535";
            this._lowLimit.Text = "0";
            // 
            // _descriptionGroup
            // 
            this._descriptionGroup.Controls.Add(this._descriptionBox);
            this._descriptionGroup.Location = new System.Drawing.Point(188, 110);
            this._descriptionGroup.Name = "_descriptionGroup";
            this._descriptionGroup.Size = new System.Drawing.Size(224, 193);
            this._descriptionGroup.TabIndex = 17;
            this._descriptionGroup.TabStop = false;
            this._descriptionGroup.Text = "groupBox14";
            this._descriptionGroup.Visible = false;
            // 
            // _descriptionBox
            // 
            this._descriptionBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this._descriptionBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this._descriptionBox.Location = new System.Drawing.Point(3, 16);
            this._descriptionBox.Name = "_descriptionBox";
            this._descriptionBox.ReadOnly = true;
            this._descriptionBox.SelectionAlignment = BEMN.Forms.RichTextBox.TextAlign.Justify;
            this._descriptionBox.Size = new System.Drawing.Size(218, 174);
            this._descriptionBox.TabIndex = 10;
            this._descriptionBox.Text = "";
            // 
            // _statusStep
            // 
            this._statusStep.AutoSize = true;
            this._statusStep.Location = new System.Drawing.Point(83, 343);
            this._statusStep.Name = "_statusStep";
            this._statusStep.Size = new System.Drawing.Size(41, 13);
            this._statusStep.TabIndex = 16;
            this._statusStep.Text = "label43";
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this._processed);
            this.groupBox13.Controls.Add(this._limit);
            this.groupBox13.Controls.Add(this.label30);
            this.groupBox13.Controls.Add(this._Uin);
            this.groupBox13.Controls.Add(this._ADC);
            this.groupBox13.Controls.Add(this.label42);
            this.groupBox13.Controls.Add(this.label40);
            this.groupBox13.Controls.Add(this.label41);
            this.groupBox13.Location = new System.Drawing.Point(188, 3);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(224, 101);
            this.groupBox13.TabIndex = 15;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Измереное значение";
            // 
            // _processed
            // 
            this._processed.Enabled = false;
            this._processed.Location = new System.Drawing.Point(133, 33);
            this._processed.Name = "_processed";
            this._processed.Size = new System.Drawing.Size(85, 20);
            this._processed.TabIndex = 17;
            // 
            // _limit
            // 
            this._limit.Enabled = false;
            this._limit.Location = new System.Drawing.Point(133, 53);
            this._limit.Name = "_limit";
            this._limit.Size = new System.Drawing.Size(85, 20);
            this._limit.TabIndex = 1;
            this._limit.Tag = "500";
            this._limit.Text = "0";
            this._limit.TextChanged += new System.EventHandler(this._limit_TextChanged);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(6, 76);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(90, 13);
            this.label30.TabIndex = 0;
            this.label30.Text = "Сигнал на входе";
            // 
            // _Uin
            // 
            this._Uin.Enabled = false;
            this._Uin.Location = new System.Drawing.Point(133, 73);
            this._Uin.Name = "_Uin";
            this._Uin.Size = new System.Drawing.Size(85, 20);
            this._Uin.TabIndex = 0;
            this._Uin.Tag = "300";
            this._Uin.Text = "0";
            // 
            // _ADC
            // 
            this._ADC.Enabled = false;
            this._ADC.Location = new System.Drawing.Point(133, 13);
            this._ADC.Name = "_ADC";
            this._ADC.Size = new System.Drawing.Size(85, 20);
            this._ADC.TabIndex = 1;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(6, 16);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(52, 13);
            this.label42.TabIndex = 0;
            this.label42.Text = "Текущее";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(6, 36);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(75, 13);
            this.label40.TabIndex = 0;
            this.label40.Text = "Приведенное";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(6, 56);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(82, 13);
            this.label41.TabIndex = 0;
            this.label41.Text = "Предел шкалы";
            // 
            // _peripheryWrite
            // 
            this._peripheryWrite.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._peripheryWrite.Location = new System.Drawing.Point(7, 338);
            this._peripheryWrite.Name = "_peripheryWrite";
            this._peripheryWrite.Size = new System.Drawing.Size(75, 23);
            this._peripheryWrite.TabIndex = 13;
            this._peripheryWrite.Text = "Записать";
            this._peripheryWrite.UseVisualStyleBackColor = true;
            this._peripheryWrite.Click += new System.EventHandler(this._peripheryWrite_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this._koeffpA);
            this.groupBox6.Controls.Add(this._koeffA);
            this.groupBox6.Controls.Add(this._koeffB);
            this.groupBox6.Controls.Add(this.label26);
            this.groupBox6.Controls.Add(this.label25);
            this.groupBox6.Controls.Add(this.label24);
            this.groupBox6.Location = new System.Drawing.Point(7, 3);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(175, 49);
            this.groupBox6.TabIndex = 0;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Коэффициенты";
            // 
            // _koeffpA
            // 
            this._koeffpA.Enabled = false;
            this._koeffpA.Location = new System.Drawing.Point(126, 23);
            this._koeffpA.Name = "_koeffpA";
            this._koeffpA.Size = new System.Drawing.Size(43, 20);
            this._koeffpA.TabIndex = 7;
            this._koeffpA.Tag = "65535";
            this._koeffpA.Text = "0";
            // 
            // _koeffA
            // 
            this._koeffA.Enabled = false;
            this._koeffA.Location = new System.Drawing.Point(66, 23);
            this._koeffA.Name = "_koeffA";
            this._koeffA.Size = new System.Drawing.Size(43, 20);
            this._koeffA.TabIndex = 6;
            this._koeffA.Tag = "65535";
            this._koeffA.Text = "0";
            // 
            // _koeffB
            // 
            this._koeffB.Enabled = false;
            this._koeffB.Location = new System.Drawing.Point(6, 23);
            this._koeffB.Name = "_koeffB";
            this._koeffB.Size = new System.Drawing.Size(43, 20);
            this._koeffB.TabIndex = 5;
            this._koeffB.Tag = "65535";
            this._koeffB.Text = "0";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(132, 10);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(20, 13);
            this.label26.TabIndex = 4;
            this.label26.Text = "pA";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(78, 11);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(14, 13);
            this.label25.TabIndex = 3;
            this.label25.Text = "А";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(10, 11);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(14, 13);
            this.label24.TabIndex = 2;
            this.label24.Text = "B";
            // 
            // _clockPage
            // 
            this._clockPage.Controls.Add(this._calendarSettingsGroup);
            this._clockPage.Controls.Add(this.groupBox11);
            this._clockPage.Location = new System.Drawing.Point(4, 22);
            this._clockPage.Name = "_clockPage";
            this._clockPage.Size = new System.Drawing.Size(423, 367);
            this._clockPage.TabIndex = 4;
            this._clockPage.Text = "Часы";
            this._clockPage.UseVisualStyleBackColor = true;
            // 
            // _calendarSettingsGroup
            // 
            this._calendarSettingsGroup.Controls.Add(this._timeZoneCombo);
            this._calendarSettingsGroup.Controls.Add(this._summerTime);
            this._calendarSettingsGroup.Controls.Add(this._acceptBtn);
            this._calendarSettingsGroup.Location = new System.Drawing.Point(7, 109);
            this._calendarSettingsGroup.Name = "_calendarSettingsGroup";
            this._calendarSettingsGroup.Size = new System.Drawing.Size(220, 100);
            this._calendarSettingsGroup.TabIndex = 3;
            this._calendarSettingsGroup.TabStop = false;
            this._calendarSettingsGroup.Text = "Настройки календаря";
            this._calendarSettingsGroup.Visible = false;
            // 
            // _timeZoneCombo
            // 
            this._timeZoneCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._timeZoneCombo.FormattingEnabled = true;
            this._timeZoneCombo.Items.AddRange(new object[] {
            "(UTC-12:00)",
            "(UTC-11:00)",
            "(UTC-10:00)",
            "(UTC-9:00)",
            "(UTC-8:00)",
            "(UTC-7:00)",
            "(UTC-6:00)",
            "(UTC-5:00)",
            "(UTC-4:00)",
            "(UTC-3:00)",
            "(UTC-2:00)",
            "(UTC-1:00)",
            "(UTC)",
            "(UTC+1:00)",
            "(UTC+2:00)",
            "(UTC+3:00)",
            "(UTC+4:00)",
            "(UTC+5:00)",
            "(UTC+6:00)",
            "(UTC+7:00)",
            "(UTC+8:00)",
            "(UTC+9:00)",
            "(UTC+10:00)",
            "(UTC+11:00)",
            "(UTC+12:00)",
            "(UTC+13:00)",
            "(UTC+14:00)"});
            this._timeZoneCombo.Location = new System.Drawing.Point(6, 19);
            this._timeZoneCombo.Name = "_timeZoneCombo";
            this._timeZoneCombo.Size = new System.Drawing.Size(121, 21);
            this._timeZoneCombo.TabIndex = 4;
            // 
            // _summerTime
            // 
            this._summerTime.AutoSize = true;
            this._summerTime.Location = new System.Drawing.Point(6, 46);
            this._summerTime.Name = "_summerTime";
            this._summerTime.Size = new System.Drawing.Size(98, 17);
            this._summerTime.TabIndex = 4;
            this._summerTime.Text = "Летнее время";
            this._summerTime.UseVisualStyleBackColor = true;
            // 
            // _acceptBtn
            // 
            this._acceptBtn.Location = new System.Drawing.Point(97, 69);
            this._acceptBtn.Name = "_acceptBtn";
            this._acceptBtn.Size = new System.Drawing.Size(117, 23);
            this._acceptBtn.TabIndex = 1;
            this._acceptBtn.Text = "Применить";
            this._acceptBtn.UseVisualStyleBackColor = true;
            this._acceptBtn.Click += new System.EventHandler(this._acceptBtn_Click);
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this._correctionButton);
            this.groupBox11.Controls.Add(this._decRB);
            this.groupBox11.Controls.Add(this._incRB);
            this.groupBox11.Controls.Add(this.label12);
            this.groupBox11.Controls.Add(this._correctionTextBox);
            this.groupBox11.Location = new System.Drawing.Point(7, 3);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(220, 100);
            this.groupBox11.TabIndex = 0;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Дневная коррекция";
            // 
            // _correctionButton
            // 
            this._correctionButton.Location = new System.Drawing.Point(97, 71);
            this._correctionButton.Name = "_correctionButton";
            this._correctionButton.Size = new System.Drawing.Size(117, 23);
            this._correctionButton.TabIndex = 1;
            this._correctionButton.Text = "Применить";
            this._correctionButton.UseVisualStyleBackColor = true;
            this._correctionButton.Click += new System.EventHandler(this._correctionButton_Click);
            // 
            // _decRB
            // 
            this._decRB.AutoSize = true;
            this._decRB.Location = new System.Drawing.Point(6, 77);
            this._decRB.Name = "_decRB";
            this._decRB.Size = new System.Drawing.Size(68, 17);
            this._decRB.TabIndex = 2;
            this._decRB.Text = "Убавить";
            this._decRB.UseVisualStyleBackColor = true;
            // 
            // _incRB
            // 
            this._incRB.AutoSize = true;
            this._incRB.Checked = true;
            this._incRB.Location = new System.Drawing.Point(6, 54);
            this._incRB.Name = "_incRB";
            this._incRB.Size = new System.Drawing.Size(75, 17);
            this._incRB.TabIndex = 1;
            this._incRB.TabStop = true;
            this._incRB.Text = "Добавить";
            this._incRB.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.label12.Location = new System.Drawing.Point(112, 22);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(31, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "[сек]";
            // 
            // _correctionTextBox
            // 
            this._correctionTextBox.Location = new System.Drawing.Point(6, 19);
            this._correctionTextBox.Name = "_correctionTextBox";
            this._correctionTextBox.Size = new System.Drawing.Size(100, 20);
            this._correctionTextBox.TabIndex = 1;
            this._correctionTextBox.Tag = "30";
            this._correctionTextBox.Text = "0";
            // 
            // _versionPage
            // 
            this._versionPage.Controls.Add(this.groupBox9);
            this._versionPage.Controls.Add(this.groupBox7);
            this._versionPage.Controls.Add(this.groupBox1);
            this._versionPage.Location = new System.Drawing.Point(4, 22);
            this._versionPage.Name = "_versionPage";
            this._versionPage.Padding = new System.Windows.Forms.Padding(3);
            this._versionPage.Size = new System.Drawing.Size(423, 367);
            this._versionPage.TabIndex = 5;
            this._versionPage.Text = "Версия";
            this._versionPage.UseVisualStyleBackColor = true;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this._ldrRevisionLabel);
            this.groupBox9.Controls.Add(this._ldrFuseProcLabel);
            this.groupBox9.Controls.Add(this._ldrCodProcLabel);
            this.groupBox9.Controls.Add(this._ldrNameLabel);
            this.groupBox9.Controls.Add(this.groupBox10);
            this.groupBox9.Controls.Add(this.label36);
            this.groupBox9.Controls.Add(this.label37);
            this.groupBox9.Controls.Add(this.label38);
            this.groupBox9.Controls.Add(this.label39);
            this.groupBox9.Location = new System.Drawing.Point(39, 137);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(344, 63);
            this.groupBox9.TabIndex = 16;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Версия загрузчика";
            // 
            // _ldrRevisionLabel
            // 
            this._ldrRevisionLabel.AutoSize = true;
            this._ldrRevisionLabel.ForeColor = System.Drawing.Color.Black;
            this._ldrRevisionLabel.Location = new System.Drawing.Point(287, 44);
            this._ldrRevisionLabel.Name = "_ldrRevisionLabel";
            this._ldrRevisionLabel.Size = new System.Drawing.Size(30, 13);
            this._ldrRevisionLabel.TabIndex = 11;
            this._ldrRevisionLabel.Text = "NAN";
            // 
            // _ldrFuseProcLabel
            // 
            this._ldrFuseProcLabel.AutoSize = true;
            this._ldrFuseProcLabel.ForeColor = System.Drawing.Color.Black;
            this._ldrFuseProcLabel.Location = new System.Drawing.Point(287, 21);
            this._ldrFuseProcLabel.Name = "_ldrFuseProcLabel";
            this._ldrFuseProcLabel.Size = new System.Drawing.Size(30, 13);
            this._ldrFuseProcLabel.TabIndex = 10;
            this._ldrFuseProcLabel.Text = "NAN";
            // 
            // _ldrCodProcLabel
            // 
            this._ldrCodProcLabel.AutoSize = true;
            this._ldrCodProcLabel.ForeColor = System.Drawing.Color.Black;
            this._ldrCodProcLabel.Location = new System.Drawing.Point(112, 44);
            this._ldrCodProcLabel.Name = "_ldrCodProcLabel";
            this._ldrCodProcLabel.Size = new System.Drawing.Size(30, 13);
            this._ldrCodProcLabel.TabIndex = 9;
            this._ldrCodProcLabel.Text = "NAN";
            // 
            // _ldrNameLabel
            // 
            this._ldrNameLabel.AutoSize = true;
            this._ldrNameLabel.ForeColor = System.Drawing.Color.Black;
            this._ldrNameLabel.Location = new System.Drawing.Point(112, 21);
            this._ldrNameLabel.Name = "_ldrNameLabel";
            this._ldrNameLabel.Size = new System.Drawing.Size(30, 13);
            this._ldrNameLabel.TabIndex = 8;
            this._ldrNameLabel.Text = "NAN";
            // 
            // groupBox10
            // 
            this.groupBox10.Location = new System.Drawing.Point(164, 10);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(5, 47);
            this.groupBox10.TabIndex = 7;
            this.groupBox10.TabStop = false;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(6, 21);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(68, 13);
            this.label36.TabIndex = 6;
            this.label36.Text = "Загрузчик : ";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(172, 44);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(103, 13);
            this.label37.TabIndex = 3;
            this.label37.Text = "Версия прошивки :";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(172, 21);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(91, 13);
            this.label38.TabIndex = 2;
            this.label38.Text = "Текущие фьюзы";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(6, 44);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(89, 13);
            this.label39.TabIndex = 1;
            this.label39.Text = "Код процессора";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this._devVersionLabel);
            this.groupBox7.Controls.Add(this._devModLabel);
            this.groupBox7.Controls.Add(this._devPodtipLabel);
            this.groupBox7.Controls.Add(this._devNameLabel);
            this.groupBox7.Controls.Add(this.groupBox8);
            this.groupBox7.Controls.Add(this.label32);
            this.groupBox7.Controls.Add(this.label33);
            this.groupBox7.Controls.Add(this.label34);
            this.groupBox7.Controls.Add(this.label35);
            this.groupBox7.Location = new System.Drawing.Point(214, 6);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(169, 112);
            this.groupBox7.TabIndex = 15;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Версия устройства";
            // 
            // _devVersionLabel
            // 
            this._devVersionLabel.AutoSize = true;
            this._devVersionLabel.ForeColor = System.Drawing.Color.Black;
            this._devVersionLabel.Location = new System.Drawing.Point(126, 90);
            this._devVersionLabel.Name = "_devVersionLabel";
            this._devVersionLabel.Size = new System.Drawing.Size(30, 13);
            this._devVersionLabel.TabIndex = 11;
            this._devVersionLabel.Text = "NAN";
            // 
            // _devModLabel
            // 
            this._devModLabel.AutoSize = true;
            this._devModLabel.ForeColor = System.Drawing.Color.Black;
            this._devModLabel.Location = new System.Drawing.Point(126, 67);
            this._devModLabel.Name = "_devModLabel";
            this._devModLabel.Size = new System.Drawing.Size(30, 13);
            this._devModLabel.TabIndex = 10;
            this._devModLabel.Text = "NAN";
            // 
            // _devPodtipLabel
            // 
            this._devPodtipLabel.AutoSize = true;
            this._devPodtipLabel.ForeColor = System.Drawing.Color.Black;
            this._devPodtipLabel.Location = new System.Drawing.Point(126, 44);
            this._devPodtipLabel.Name = "_devPodtipLabel";
            this._devPodtipLabel.Size = new System.Drawing.Size(30, 13);
            this._devPodtipLabel.TabIndex = 9;
            this._devPodtipLabel.Text = "NAN";
            // 
            // _devNameLabel
            // 
            this._devNameLabel.AutoSize = true;
            this._devNameLabel.ForeColor = System.Drawing.Color.Black;
            this._devNameLabel.Location = new System.Drawing.Point(126, 21);
            this._devNameLabel.Name = "_devNameLabel";
            this._devNameLabel.Size = new System.Drawing.Size(30, 13);
            this._devNameLabel.TabIndex = 8;
            this._devNameLabel.Text = "NAN";
            // 
            // groupBox8
            // 
            this.groupBox8.Location = new System.Drawing.Point(115, 10);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(5, 96);
            this.groupBox8.TabIndex = 7;
            this.groupBox8.TabStop = false;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(6, 21);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(76, 13);
            this.label32.TabIndex = 6;
            this.label32.Text = "Устройство : ";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(6, 90);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(56, 13);
            this.label33.TabIndex = 3;
            this.label33.Text = "Ревизия :";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(6, 67);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(78, 13);
            this.label34.TabIndex = 2;
            this.label34.Text = "Модификация";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(6, 44);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(44, 13);
            this.label35.TabIndex = 1;
            this.label35.Text = "Подтип";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._appVersionLabel);
            this.groupBox1.Controls.Add(this._appModLabel);
            this.groupBox1.Controls.Add(this._appPodtipLabel);
            this.groupBox1.Controls.Add(this._appNameLabel);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(39, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(169, 112);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Версия программы";
            // 
            // _appVersionLabel
            // 
            this._appVersionLabel.AutoSize = true;
            this._appVersionLabel.ForeColor = System.Drawing.Color.Black;
            this._appVersionLabel.Location = new System.Drawing.Point(126, 90);
            this._appVersionLabel.Name = "_appVersionLabel";
            this._appVersionLabel.Size = new System.Drawing.Size(30, 13);
            this._appVersionLabel.TabIndex = 11;
            this._appVersionLabel.Text = "NAN";
            // 
            // _appModLabel
            // 
            this._appModLabel.AutoSize = true;
            this._appModLabel.ForeColor = System.Drawing.Color.Black;
            this._appModLabel.Location = new System.Drawing.Point(126, 67);
            this._appModLabel.Name = "_appModLabel";
            this._appModLabel.Size = new System.Drawing.Size(30, 13);
            this._appModLabel.TabIndex = 10;
            this._appModLabel.Text = "NAN";
            // 
            // _appPodtipLabel
            // 
            this._appPodtipLabel.AutoSize = true;
            this._appPodtipLabel.ForeColor = System.Drawing.Color.Black;
            this._appPodtipLabel.Location = new System.Drawing.Point(126, 44);
            this._appPodtipLabel.Name = "_appPodtipLabel";
            this._appPodtipLabel.Size = new System.Drawing.Size(30, 13);
            this._appPodtipLabel.TabIndex = 9;
            this._appPodtipLabel.Text = "NAN";
            // 
            // _appNameLabel
            // 
            this._appNameLabel.AutoSize = true;
            this._appNameLabel.ForeColor = System.Drawing.Color.Black;
            this._appNameLabel.Location = new System.Drawing.Point(126, 21);
            this._appNameLabel.Name = "_appNameLabel";
            this._appNameLabel.Size = new System.Drawing.Size(30, 13);
            this._appNameLabel.TabIndex = 8;
            this._appNameLabel.Text = "NAN";
            // 
            // groupBox2
            // 
            this.groupBox2.Location = new System.Drawing.Point(115, 10);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(5, 96);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 21);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Устройство : ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Версия прошивки :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Модификация";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Подтип";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._statusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 425);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(429, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // _statusLabel
            // 
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.Size = new System.Drawing.Size(118, 17);
            this._statusLabel.Text = "toolStripStatusLabel1";
            // 
            // _toolTip
            // 
            this._toolTip.ShowAlways = true;
            this._toolTip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Warning;
            // 
            // _writeAllButton
            // 
            this._writeAllButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._writeAllButton.Location = new System.Drawing.Point(93, 399);
            this._writeAllButton.Name = "_writeAllButton";
            this._writeAllButton.Size = new System.Drawing.Size(88, 23);
            this._writeAllButton.TabIndex = 14;
            this._writeAllButton.Text = "Записать все";
            this._writeAllButton.UseVisualStyleBackColor = true;
            this._writeAllButton.Click += new System.EventHandler(this._writeAllButton_Click);
            // 
            // MlkConfiguration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(429, 447);
            this.Controls.Add(this._writeAllButton);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this._configurationTabControl);
            this.Controls.Add(this._readButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MlkConfiguration";
            this.Text = "Конфигурация";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MLK_Configuration_FormClosing);
            this.Load += new System.EventHandler(this.MLK_Configuration_Load);
            this._configurationTabControl.ResumeLayout(false);
            this._USBPage.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this._RS485Page.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this._querysPage.ResumeLayout(false);
            this._querysPage.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._querys)).EndInit();
            this._analogPage.ResumeLayout(false);
            this._analogPage.PerformLayout();
            this.signalGroupBox.ResumeLayout(false);
            this.signalGroupBox.PerformLayout();
            this.outpValueGroupBox.ResumeLayout(false);
            this.outpValueGroupBox.PerformLayout();
            this.amplitudeGroupBox.ResumeLayout(false);
            this.amplitudeGroupBox.PerformLayout();
            this._descriptionGroup.ResumeLayout(false);
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this._clockPage.ResumeLayout(false);
            this._calendarSettingsGroup.ResumeLayout(false);
            this._calendarSettingsGroup.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this._versionPage.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button _readButton;
        private System.Windows.Forms.TabControl _configurationTabControl;
        private System.Windows.Forms.TabPage _USBPage;
        private System.Windows.Forms.TabPage _RS485Page;
        private System.Windows.Forms.TabPage _querysPage;
        private System.Windows.Forms.TabPage _analogPage;
        private System.Windows.Forms.TabPage _clockPage;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox _USBdoubleSpeedCombo;
        private System.Windows.Forms.ComboBox _USBparitetCHETCombo;
        private System.Windows.Forms.ComboBox _USBparitetYNCombo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox _USBstopBitsCombo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox _USBspeedsCombo;
        private System.Windows.Forms.MaskedTextBox _USBAddressTB;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.MaskedTextBox _USBToSendAfterTB;
        private System.Windows.Forms.MaskedTextBox _USBToSendBeforeTB;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.MaskedTextBox _rs485ToSendAfterTB;
        private System.Windows.Forms.MaskedTextBox _rs485ToSendBeforeTB;
        private System.Windows.Forms.MaskedTextBox _rs485ToSendTB;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.MaskedTextBox _rs485AddressTB;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ComboBox _rs485doubleSpeedCombo;
        private System.Windows.Forms.ComboBox _rs485paritetCHETCombo;
        private System.Windows.Forms.ComboBox _rs485paritetYNCombo;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox _rs485stopBitsCombo;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox _rs485speedsCombo;
        private System.Windows.Forms.Button _USBWrite;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Button _rs485Write;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button _querysWrite;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.MaskedTextBox _querysCountBox;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.MaskedTextBox _koeffpA;
        private System.Windows.Forms.MaskedTextBox _koeffA;
        private System.Windows.Forms.MaskedTextBox _koeffB;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Button _peripheryWrite;
        private System.Windows.Forms.DataGridView _querys;
        private System.Windows.Forms.ToolTip _toolTip;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.MaskedTextBox _rs485AnswerTB;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Button _writeAllButton;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ToolStripStatusLabel _statusLabel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox _USBdataBitsCombo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox _rs485dataBitsCombo;
        private System.Windows.Forms.CheckBox _rs485modeCheck;
        private System.Windows.Forms.TabPage _versionPage;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label _ldrRevisionLabel;
        private System.Windows.Forms.Label _ldrFuseProcLabel;
        private System.Windows.Forms.Label _ldrCodProcLabel;
        private System.Windows.Forms.Label _ldrNameLabel;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label _devVersionLabel;
        private System.Windows.Forms.Label _devModLabel;
        private System.Windows.Forms.Label _devPodtipLabel;
        private System.Windows.Forms.Label _devNameLabel;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label _appVersionLabel;
        private System.Windows.Forms.Label _appModLabel;
        private System.Windows.Forms.Label _appPodtipLabel;
        private System.Windows.Forms.Label _appNameLabel;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Button _correctionButton;
        private System.Windows.Forms.RadioButton _decRB;
        private System.Windows.Forms.RadioButton _incRB;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.MaskedTextBox _correctionTextBox;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.MaskedTextBox _Uin;
        private System.Windows.Forms.Button _readFromDevice;
        private System.Windows.Forms.MaskedTextBox _limit;
        private System.Windows.Forms.MaskedTextBox _ADC;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label _statusStep;
        private System.Windows.Forms.TextBox _processed;
        private System.Windows.Forms.GroupBox _descriptionGroup;
        private AdvRichTextBox _descriptionBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn _indexCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _timeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _msgCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _commandCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _masterAddrCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _slaveAddrCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _numCol;
        private System.Windows.Forms.GroupBox outpValueGroupBox;
        private System.Windows.Forms.RadioButton _rBtn1;
        private System.Windows.Forms.RadioButton _rBtn2;
        private System.Windows.Forms.GroupBox amplitudeGroupBox;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.MaskedTextBox _highLimit;
        private System.Windows.Forms.MaskedTextBox _lowLimit;
        private System.Windows.Forms.Button _calibrovkaBtn;
        private System.Windows.Forms.Button _gisterezisBtn;
        private System.Windows.Forms.GroupBox signalGroupBox;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.MaskedTextBox _highLimitSignal;
        private System.Windows.Forms.MaskedTextBox _lowLimitSignal;
        private System.Windows.Forms.GroupBox _calendarSettingsGroup;
        private System.Windows.Forms.ComboBox _timeZoneCombo;
        private System.Windows.Forms.CheckBox _summerTime;
        private System.Windows.Forms.Button _acceptBtn;
        private System.Windows.Forms.RadioButton _rBtn3;

    }
}