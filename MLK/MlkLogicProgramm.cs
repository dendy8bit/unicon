﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using BEMN.Forms;
using BEMN.Forms.Queries;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Framework.Properties;
using BEMN.Interfaces;
using BEMN.MLK.Structures;
using BEMN.MBServer;
using BEMN.MLK.Structures.IntermediateStructures;

namespace BEMN.MLK
{
    public partial class MlkLogicProgramm : Form, IFormView
    {
        #region Поля
        private MLK _device;
        private MemoryEntity<SomeStruct> _logicProgFileConst; 
        private MemoryEntity<SomeStruct> _logicProgFile;
        private MemoryEntity<RegLogicProg> _regLogicProg;
        private MemoryEntity<SomeStruct> _signature;
        private MemoryEntity<SomeStruct> _sclp;
 
        private byte[] _logicProgValues;
        private bool _doubleClicked;
        private bool _isReplace;
        private bool? _isWriteFile;
        private int _fileHdrSize;
        private string _oldText;
        private string _filePath;

        private UInt16? _curSclp;
        private bool _canReadReg;
        private Dictionary<string, LedControl> _stateLeds;
        private Button _startBtn;
        private Button _debugBtn;
        private Button _stopBtn;
        private Button _continueBtn;
        private Button _stepBtn;
        private Button _byPointBtn;
        private Button _goToPointBtn;
        private MaskedTextBox _address;
        private bool _isConstantLP;
        private delegate bool Methods(ushort sclp);
        private Methods[] _methods;
        #endregion

        #region Константы
        private const ushort StartAdress = 0x8000;
        private const ushort StartAdressLogicConstV2 = 0x2000;
        private const string ERROR = "Ошибка";
        private const string ERROR_WRITE = "Ошибка записи";
        private const string ERROR_READ = "Ошибка чтения";
        private const string ERROR_WRITE_REG = "Невозможно записать регистр логической программы";
        private const string ERROR_WRITE_LP = "Невозможно записать файл логической программы";
        private const string ERROR_DATA_IN_DEVICE_LP = "Логическая программа, записанная в устройство, не совпадает с записываемым файлом";
        private const string INVALID_DATA_LP = "Программа логики в устройстве неверная или повреждена";
        private const string ERROR_READ_LP = "Невозможно прочитать файл логической программы";
        private const string ERROR_READ_LP_CONST = "Невозможно прочитать файл констант логической программы";
        private const string WRITE_SUCCSESS_LP = "Логическая программа записана успешно";
        private const string WRITE_SUCCSESS = "Запись успешно завершена";
        private const string SAVE_IN_FILE_SUCCSESS = "Логическая программа успешно сохранена в файл";
        private const string WRITE_SUCCSESS_LP_CONST = "Константы логической программы записаны успешно";
        private const string ERROR_SIZE_OVER = "Превышен допустимый размер файла";
        private const string SAVE_IN_FILE_CONST_SUCCSESS_ = "Константы логической программы успешно сохранены в файл";
        private const string CHECK_FILE = "Проверка файла...";
        private const string WRITING_FILE = "Запись файла...";
        private const string READING_FILE = "Чтение файла...";

        private const ushort RESET_VALUE = 0x0109;
        private const ushort START_VALUE = 0x0209;
        private const ushort START_DEBUG = 0x0005;
        #endregion

        #region Конструкторы
        public MlkLogicProgramm()
        {
            InitializeComponent();
        }

        public MlkLogicProgramm(MLK device)
        {
            InitializeComponent();
            _device = device;

            #region Registers
            _regLogicProg = _device.RegLogicProg;
            _regLogicProg.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, RegLogicProgReadOk);
            _regLogicProg.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                LedControl[] leds = _stateLeds.Values.ToArray();
                LedManager.TurnOffLeds(leds);
            });
            _regLogicProg.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () => _canReadReg = true);
            _regLogicProg.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this,
                () => MessageBox.Show(ERROR_WRITE_REG, ERROR_WRITE, MessageBoxButtons.OK, MessageBoxIcon.Error));
            _sclp = _device.SaveSCLP;
            _sclp.Slots = new List<Device.slot>
            {
                new Device.slot(_sclp.StartAddress, (ushort) (_sclp.StartAddress + 1))
            };
            _sclp.Value = new SomeStruct(1);
            _sclp.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                    MessageBox.Show("Не удалось записать значение регистра SCLP", "Ошибка записи", MessageBoxButtons.OK,
                        MessageBoxIcon.Warning));

            _signature = _device.LogicSignature;
            _signature.Slots = new List<Device.slot>
            {
                new Device.slot(_signature.StartAddress, (ushort) (_signature.StartAddress + 1))
            };
            _signature.Value = new SomeStruct(1);
            _signature.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                    MessageBox.Show("Не удалось записать сигнатуру логической программы", "Ошибка записи", MessageBoxButtons.OK,
                        MessageBoxIcon.Warning));
            #endregion Registers

            _logicProgFile = _device.LogicProg;
            _logicProgFile.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, ProgrammFileAllReadOk);
            _logicProgFile.AllReadFail += HandlerHelper.CreateReadArrayHandler(this,
                () =>
                {
                    MessageBox.Show(ERROR_READ_LP, ERROR_WRITE, MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    _writeProgrammBtn.Enabled = true;
                    _stateLableProgramFile.Text = "";
                    _saveInFileBtn2.Enabled = true;
                    _writeProgramBtn2.Enabled = true;
                    _readFileBtn2.Enabled = true;
                });
            _logicProgFile.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, ProgramFileAllWriteOk);
            _logicProgFile.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this,
                () =>
                {
                    MessageBox.Show(ERROR_WRITE_LP, ERROR_READ, MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    _writeProgrammBtn.Enabled = false;
                    _stateLableProgramFile.Text = "";
                    _saveInFileBtn2.Enabled = true;
                    _writeProgramBtn2.Enabled = true;
                    _readFileBtn2.Enabled = true;
                });
            _logicProgFile.WriteOk += HandlerHelper.CreateHandler(this, IncrementProgressBarFile);
            _logicProgFile.ReadOk += HandlerHelper.CreateHandler(this, IncrementProgressBarFile);

            _logicProgFileConst = _device.LogicProgConst;
            //константы
            _logicProgFileConst.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, ProgrammFileConstAllReadOk);
            _logicProgFileConst.AllReadFail += HandlerHelper.CreateReadArrayHandler(this,
                () =>
                {
                    MessageBox.Show(ERROR_READ_LP_CONST, ERROR_WRITE, MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    _writeProgrammBtn.Enabled = true;
                    _stateLableProgFileConst.Text = "";
                    _saveInFileBtn.Enabled = true;
                    _writeProgrammBtn.Enabled = true;
                    _readFileBtn.Enabled = true;
                });
            _logicProgFileConst.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, ProgrammFileConstAllWriteOk);
            _logicProgFileConst.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this,
                () =>
                {
                    MessageBox.Show(ERROR_WRITE_LP, ERROR_READ, MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    _writeProgrammBtn.Enabled = false;
                    _stateLableProgFileConst.Text = "";
                    _saveInFileBtn.Enabled = true;
                    _writeProgrammBtn.Enabled = true;
                    _readFileBtn.Enabled = true;
                });
            _logicProgFileConst.WriteOk += HandlerHelper.CreateHandler(this, IncrementProgressBarFileConst);
            _logicProgFileConst.ReadOk += HandlerHelper.CreateHandler(this, IncrementProgressBarFileConst);
            InitLeds();
            InitButtons();
            InitDelegates();
            _curSclp = null;
            _canReadReg = true;
            _isWriteFile = false;
            _fileHdrSize = Marshal.SizeOf(typeof (FileHdrStructure));
        }

        private void IncrementProgressBarFileConst()
        {
            _stateProgressBar2.Increment(1);
        }

        private void IncrementProgressBarFile()
        {
            _stateProgrBar.Increment(1);
        }
        private void InitLeds()
        {
            _stateLeds = new Dictionary<string, LedControl>()
            {
                {"Work", ledControl1},
                {"Debug", ledControl2},
                {"Stop", ledControl3},
                {"Start", ledControl4},
                {"Update", ledControl5},
                {"Error", ledControl6}
            };
            ClearLeds();
        }

        private void InitButtons()
        {
            _startBtn = new Button();
            _startBtn.Text = "Запуск";
            _startBtn.Location = new Point(6, 14);
            _startBtn.Size = new Size(125, 22);
            _startBtn.Click += new System.EventHandler(this._startBtn_Click);

            _debugBtn = new Button();
            _debugBtn.Text = "Отладка";
            _debugBtn.Location = new Point(6, 35);
            _debugBtn.Size = new Size(125, 22);
            _debugBtn.Click += new System.EventHandler(this._debugBtn_Click);

            _stopBtn = new Button();
            _stopBtn.Text = "Остановка";
            _stopBtn.Location = new Point(6, 35);
            _stopBtn.Size = new Size(125, 22);
            _stopBtn.Click += new System.EventHandler(this.PauseContinue);

            _continueBtn = new Button();
            _continueBtn.Text = "Продолжить";
            _continueBtn.Location = new Point(6, 35);
            _continueBtn.Size = new Size(125, 22);
            _continueBtn.Click += new System.EventHandler(this.PauseContinue);

            _stepBtn = new Button();
            _stepBtn.Text = "Пошаговая";
            _stepBtn.Location = new Point(6, 14);
            _stepBtn.Size = new Size(125, 22);
            _stepBtn.Click += new System.EventHandler(this._stepBtn_Click);

            _byPointBtn = new Button();
            _byPointBtn.Text = "До точки прерывания";
            _byPointBtn.Location = new Point(6, 35);
            _byPointBtn.Size = new Size(125,40);
            _byPointBtn.Click += new System.EventHandler(this._byPointBtn_Click);

            _goToPointBtn = new Button();
            _goToPointBtn.Text = "Продолжить";
            _goToPointBtn.Location = new Point(6, 14);
            _goToPointBtn.Size = new Size(125,22);
            _goToPointBtn.Enabled = false;
            _goToPointBtn.Click += new System.EventHandler(this._goToPointBtn_Click);

            _address = new MaskedTextBox("aaaa");
            _address.PromptChar = ' ';
            _address.Size = new Size(125, 23);
            _address.Location = new Point(6, 75);
            _address.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this._address_KeyPress);
            _address.KeyUp += new System.Windows.Forms.KeyEventHandler(this._address_KeyUp);
        }

        private void InitDelegates()
        {
            _methods = new Methods[6];
            _methods[0] += IsError;
            _methods[1] += IsUpdate;
            _methods[2] += IsWorking;
            _methods[3] += IsDebug;
            _methods[4] += IsStop;
            _methods[5] += IsStart;
        }
        #endregion

        #region Логическая программа

        private void ProgrammFileAllReadOk()
        {
            switch (_isWriteFile)
            {
                case true:
                    CheckReadedFile(_logicProgFile, WRITE_SUCCSESS_LP);
                    break;
                case false:
                    CheckHeaderFromDevice();
                    break;
                case null:
                    SaveProgInFile(_logicProgFile,SAVE_IN_FILE_SUCCSESS);
                    break;
            }
        }

        private void WriteProgrammInDeviceClick(object sender, EventArgs e)
        {
            _isConstantLP = false;
            bool res = ReadValuesFromFile(_isConstantLP);
            if (!res) return;
            ushort[] values = Common.TOWORDS(_logicProgValues, true);
            SomeStruct ss = new SomeStruct();
            ss.Values = values;
            _logicProgFile.Clear();
            _logicProgFile.Value = ss;
            _logicProgFile.Slots = QueriesForm.SetSlots(values, StartAdress);
            _logicProgFile.SaveStruct();

            _isWriteFile = true;
            _stateProgrBar.Maximum = _logicProgFile.Slots.Count;
            _stateProgrBar.Value = 0;
            _stateProgrBar.Visible = true;
            _writeProgrammBtn.Enabled = false;
            _stateLableProgramFile.Text = WRITING_FILE;
            _saveInFileBtn2.Enabled = false;
            _writeProgramBtn2.Enabled = false;
            _readFileBtn2.Enabled = false;
        }

        private bool ReadValuesFromFile(bool _isConstantLP)
        {
            _logicProgValues = File.ReadAllBytes(_filePath);
            Common.SwapArrayItems(ref _logicProgValues);
            try
            {
                FileHdrStructure fileHdr = new FileHdrStructure(_logicProgValues, 0);
                AoutHdrStructure aoutHdr = new AoutHdrStructure(_logicProgValues, _fileHdrSize);
                if (_logicProgValues.Length % 256 == 0 || CheckMagicValue(fileHdr, aoutHdr))
                {
                    if (!_isConstantLP)
                    {
                        _vstamp.Text = aoutHdr.Vstamp.ToString("X");
                        _verscompl.Text = aoutHdr.Verscompl.ToString("X");
                    }  
                    return true;
                }
                else
                {
                    throw new Exception("Неверный файл логической программы");
                }
            }
            catch (Exception ex)
            {
                _vstamp.Text = string.Empty;
                _verscompl.Text = string.Empty;
                MessageBox.Show(ex.Message, ERROR_READ, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private void ReadFileClick(object sender, EventArgs e)
        {
            if (DialogResult.OK != _openLogicProgDlg.ShowDialog()) return;
            _fileForWriting.Text = _openLogicProgDlg.FileName;
            _filePath = _openLogicProgDlg.FileName;
            _writeProgrammBtn.Enabled = true;
        }

        private void SaveInFileBtnClick(object sender, EventArgs e)
        {
            _isWriteFile = false;
            _saveInFileBtn2.Enabled = false;
            _writeProgramBtn2.Enabled = false;
            _readFileBtn2.Enabled = false;
            if (_saveLogicProgDlg.ShowDialog() != DialogResult.OK) return;
            SomeStruct ss =
                new SomeStruct((Marshal.SizeOf(typeof (FileHdrStructure)) + Marshal.SizeOf(typeof (AoutHdrStructure)))/2);
            _logicProgFile.Clear();
            _logicProgFile.Value = ss;
            _logicProgFile.Values = ss.Values;
            _logicProgFile.Slots = QueriesForm.SetSlots(ss.Values, StartAdress);
            _stateProgrBar.Maximum = _logicProgFile.Slots.Count;
            _stateProgrBar.Value = 0;
            _stateLableProgramFile.Text = READING_FILE;
            _logicProgFile.LoadStruct();
        }

        private void CheckReadedFile(MemoryEntity<SomeStruct> logicStruct, string messsage)
        {
            _saveInFileBtn2.Enabled = true;
            _writeProgramBtn2.Enabled = true;
            _readFileBtn2.Enabled = true;
            _saveInFileBtn.Enabled = true;
            _writeProgrammBtn.Enabled = true;
            _readFileBtn.Enabled = true;
            try
            {
                ushort[] values = logicStruct.Values;
                byte[] buffer = Common.TOBYTES(values, true);
                for (int i = 0; i < _logicProgValues.Length; i++)
                {
                    if (buffer[i] != _logicProgValues[i])
                        throw new Exception();
                }
                MessageBox.Show(messsage, WRITE_SUCCSESS, MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            catch (Exception)
            {
                MessageBox.Show(ERROR_DATA_IN_DEVICE_LP, ERROR_WRITE, MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
            }
            _stateProgrBar.Visible = true;
            _writeProgrammBtn.Enabled = true;
            _stateLableProgFileConst.Text = "";
            _stateLableProgramFile.Text = "";
        }

        private void CheckHeaderFromDevice()
        {
            ushort[] values = _logicProgFile.Values;
            byte[] buffer = Common.TOBYTES(values, true);
            FileHdrStructure fileHdr = new FileHdrStructure(buffer,0);
            AoutHdrStructure aoutHdr = new AoutHdrStructure(buffer, _fileHdrSize);
            if (!CheckMagicValue(fileHdr, aoutHdr))
            {
                MessageBox.Show(INVALID_DATA_LP, ERROR_WRITE,
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                _saveLogicProgDlg.FileName = string.Empty;
                _stateLableProgramFile.Text = "";
                return;
            }
            ushort allSize = (ushort)(_fileHdrSize + Marshal.SizeOf(aoutHdr.GetType())  //Весь размер в байтах
                                      + aoutHdr.TextSize + aoutHdr.DataSize);
            allSize = allSize%2 == 0 ? allSize : (ushort) (allSize + 1);
            _isWriteFile = null;
            SomeStruct ss = new SomeStruct(allSize/2);
            _logicProgFile.Clear();
            _logicProgFile.Value = ss;
            _logicProgFile.Values = ss.Values;
            _logicProgFile.Slots = QueriesForm.SetSlots(ss.Values, StartAdress);
            _logicProgFile.LoadStruct();

            _stateProgrBar.Maximum = _logicProgFile.Slots.Count;
            _stateProgrBar.Value = 0;
            _stateProgrBar.Visible = true;
        }

        private bool CheckMagicValue(FileHdrStructure fileHdr, AoutHdrStructure aoutHdr)
        {
            if (fileHdr.Magic != 0x0162 || (Marshal.SizeOf(fileHdr.GetType()) != _fileHdrSize))
                return false;
            if ((aoutHdr.Magic != 0x1000) || (Marshal.SizeOf(aoutHdr.GetType()) != fileHdr.OptHdr))
                return false;
            
            return true;
        }
        private void SaveProgInFile(MemoryEntity<SomeStruct> logicStruct, string message)
        {
            ushort[] values = logicStruct.Values;
            byte[] buffer = Common.TOBYTES(values, true);
            Common.SwapArrayItems(ref buffer);
            if (buffer.Length%256 == 0)
            {
                File.WriteAllBytes(_saveLogicProgDlg.FileName, buffer);
            }
            else
            {
                int len = buffer.Length/256 + 1;
                int flen = len*256 - buffer.Length;
                List<byte> writeList = new List<byte>(buffer);
                byte[] fbyte = new byte[flen];
                for (int i = 0; i < flen; i++)
                {
                    fbyte[i] = 0xFF;
                }
                writeList.AddRange(fbyte);
                File.WriteAllBytes(_saveLogicProgDlg.FileName, writeList.ToArray());
            }
            MessageBox.Show(message, "Сохранение логики", MessageBoxButtons.OK,
                MessageBoxIcon.Information);
            _stateProgrBar.Visible = true;
            _saveLogicProgDlg.FileName = string.Empty;
            _stateLableProgFileConst.Text = "";
            _stateLableProgramFile.Text = "";
            _saveInFileBtn2.Enabled = true;
            _writeProgramBtn2.Enabled = true;
            _readFileBtn2.Enabled = true;
            _saveInFileBtn.Enabled = true;
            _writeProgrammBtn.Enabled = true;
            _readFileBtn.Enabled = true;
        }
        #endregion

        #region Регистры логической программы 

        public void RegLogicProgReadOk()
        {
            if (_canReadReg)
            {
                ShowRegisters();
                _canReadReg = false;
            }
            ushort sclpValue = Convert.ToUInt16(this._regLogicProg.Value.SCLP, 16);
            if (_curSclp == sclpValue)
            {
                return;
            }
            if (sclpValue == 0)
            {
                foreach (var led in _stateLeds)
                {
                    led.Value.State = LedState.Signaled;
                }
                return;
            }
            foreach (var method in _methods)
            {
                if (method(sclpValue))
                    break;
            }
        }

        private bool IsError(ushort sclpValue)
        {
            bool ret = (Common.GetBits(sclpValue, 5) >> 5 == 1);
            if (ret)
            {
                ErrorEvent();
                _curSclp = sclpValue;
            }
            return ret;
        }

        private bool IsUpdate(ushort sclpValue)
        {
            bool ret = (Common.GetBits(sclpValue, 4) >> 4 == 1);
            if (ret)
            {
                UpdateEvent();
                _curSclp = sclpValue;
            }
            return ret;
        }
        private bool IsWorking(ushort sclpValue)
        {
            bool ret = Common.GetBits(sclpValue, 0, 1, 2) == 3;
            if (ret)
            {
                WorkEvent();
                _curSclp = sclpValue;
            }
            return ret;
        }

        private bool IsDebug(ushort sclpValue)
        {
            bool ret = Common.GetBits(sclpValue, 0, 1, 2) == 5;
            if (ret)
            {
                DebugEvent();
                _curSclp = sclpValue;
            }
            return ret;
        }

        private bool IsStop(ushort sclpValue)
        {
            bool ret = (Common.GetBits(sclpValue, 0, 1, 2) == 2 || 
                        Common.GetBits(sclpValue, 0, 1, 2) == 4 ||
                        Common.GetBits(sclpValue, 0, 1, 2) == 6);
            if (ret)
            {
                StopEvent();
                _curSclp = sclpValue;
            }
            return ret;
        }

        private bool IsStart(ushort sclpValue)
        {
            bool ret = sclpValue == 1;
            if (ret)
            {
                StartEvent();
                _curSclp = sclpValue;
            }
            return ret;
        }

        private void ErrorEvent()
        {
            this._buttonsGroup.Controls.Clear();
            Label message1 = new Label();
            message1.Text = "Ошибка работы";
            message1.Location = new Point(6, 19);
            message1.Size = new Size(130, 20);
            Label message2 = new Label();
            message2.Text = "логики.";
            message2.Location = new Point(6, 35);
            message2.Size = new Size(130, 20);
            this._buttonsGroup.Controls.AddRange(new Label[] { message1, message2 });
            ClearLeds();
            this._stateLeds["Error"].SetState(true);
            this._buttonsGroup.Update();

            _canReadReg = true;
        }

        private void UpdateEvent()
        {
            this._buttonsGroup.Controls.Clear();
            Label message1 = new Label();
            message1.Text = "Идет обновление";
            message1.Location = new Point(6, 19);
            message1.Size = new Size(130, 20);
            this._buttonsGroup.Controls.Add(message1);
            this._buttonsGroup.Update();
            ClearLeds();
            this._stateLeds["Update"].SetState(true);
        }

        private void WorkEvent()
        {
            this._buttonsGroup.Controls.Clear();
            Label message1 = new Label();
            message1.Text = "Идет работа логи-";
            message1.Location = new Point(6, 19);
            message1.Size = new Size(130, 20);
            Label message2 = new Label();
            message2.Text = "ческой программы";
            message2.Location = new Point(6, 35);
            message2.Size = new Size(130, 20);
            this._buttonsGroup.Controls.AddRange(new Label[] { message1, message2 });
            this._buttonsGroup.Update();
            ClearLeds();
            this._stateLeds["Work"].SetState(true);
        }

        private void DebugEvent()
        {
            this._buttonsGroup.Controls.Clear();
            this._buttonsGroup.Controls.Add(_stepBtn);
            this._buttonsGroup.Controls.Add(_byPointBtn);
            ClearLeds();
            this._stateLeds["Debug"].SetState(true);
        }

        private void StopEvent()
        {
            this._buttonsGroup.Controls.Clear();
            this._buttonsGroup.Controls.Add(_stopBtn);
            this._stopBtn.Text = "Продолжить";
            this._buttonsGroup.Update();
            ClearLeds();
            this._stateLeds["Stop"].SetState(true);
        }

        private void StartEvent()
        {
            this._buttonsGroup.Controls.Clear();
            this._buttonsGroup.Controls.Add(_startBtn);
            this._buttonsGroup.Controls.Add(_debugBtn);
            ClearLeds();
            this._stateLeds["Start"].SetState(true);
        }

        private void ShowRegisters()
        {
            RegLogicProg reg = _regLogicProg.Value;
            #region RAX
            _regTreeView.Nodes["_regAX"].Nodes["_eAXhigh"].Nodes["_AXhigh1"].Nodes["_Ahigh1"].Text = "Ah = 0x" + reg.Ahigh1;
            _regTreeView.Nodes["_regAX"].Nodes["_eAXhigh"].Nodes["_AXhigh1"].Nodes["_Alow1"].Text = "Al = 0x" + reg.Alow1;
            _regTreeView.Nodes["_regAX"].Nodes["_eAXhigh"].Nodes["_AXhigh1"].Text = "AXhigh = 0x" + reg.AXhigh1;
            _regTreeView.Nodes["_regAX"].Nodes["_eAXhigh"].Nodes["_AXlow1"].Nodes["_Ahigh2"].Text = "Ah = 0x" + reg.Ahigh2;
            _regTreeView.Nodes["_regAX"].Nodes["_eAXhigh"].Nodes["_AXlow1"].Nodes["_Alow2"].Text = "Al = 0x" + reg.Alow2;
            _regTreeView.Nodes["_regAX"].Nodes["_eAXhigh"].Nodes["_AXlow1"].Text = "AXlow = 0x" + reg.AXlow1;
            _regTreeView.Nodes["_regAX"].Nodes["_eAXhigh"].Text = "EAXhigh = 0x" + reg.EAXhigh;

            _regTreeView.Nodes["_regAX"].Nodes["_eAXlow"].Nodes["_AXhigh2"].Nodes["_Ahigh3"].Text = "Ah = 0x" + reg.Ahigh3;
            _regTreeView.Nodes["_regAX"].Nodes["_eAXlow"].Nodes["_AXhigh2"].Nodes["_Alow3"].Text = "Al = 0x" + reg.Alow3;
            _regTreeView.Nodes["_regAX"].Nodes["_eAXlow"].Nodes["_AXhigh2"].Text = "AXhigh = 0x" + reg.AXhigh2;
            _regTreeView.Nodes["_regAX"].Nodes["_eAXlow"].Nodes["_AXlow2"].Nodes["_Ahigh4"].Text = "Ah = 0x" + reg.Ahigh4;
            _regTreeView.Nodes["_regAX"].Nodes["_eAXlow"].Nodes["_AXlow2"].Nodes["_Alow4"].Text = "Al = 0x" + reg.Alow4;
            _regTreeView.Nodes["_regAX"].Nodes["_eAXlow"].Nodes["_AXlow2"].Text = "AXlow = 0x" + reg.AXlow2;
            _regTreeView.Nodes["_regAX"].Nodes["_eAXlow"].Text = "EAXlow = 0x" + reg.EAXlow;

            _regTreeView.Nodes["_regAX"].Text = "RAX = 0x" + reg.RAX;
            #endregion
            #region RBX
            _regTreeView.Nodes["_regBX"].Nodes["_eBXhigh"].Nodes["_BXhigh1"].Nodes["_Bhigh1"].Text = "Bh = 0x" + reg.Bhigh1;
            _regTreeView.Nodes["_regBX"].Nodes["_eBXhigh"].Nodes["_BXhigh1"].Nodes["_Blow1"].Text = "Bl = 0x" + reg.Blow1;
            _regTreeView.Nodes["_regBX"].Nodes["_eBXhigh"].Nodes["_BXhigh1"].Text = "BXhigh = 0x" + reg.BXhigh1;
            _regTreeView.Nodes["_regBX"].Nodes["_eBXhigh"].Nodes["_BXlow1"].Nodes["_Bhigh2"].Text = "Bh = 0x" + reg.Bhigh2;
            _regTreeView.Nodes["_regBX"].Nodes["_eBXhigh"].Nodes["_BXlow1"].Nodes["_Blow2"].Text = "Bl = 0x" + reg.Blow2;
            _regTreeView.Nodes["_regBX"].Nodes["_eBXhigh"].Nodes["_BXlow1"].Text = "BXlow = 0x" + reg.BXlow1;
            _regTreeView.Nodes["_regBX"].Nodes["_eBXhigh"].Text = "EBXhigh = 0x" + reg.EBXhigh;

            _regTreeView.Nodes["_regBX"].Nodes["_eBXlow"].Nodes["_BXhigh2"].Nodes["_Bhigh3"].Text = "Bh = 0x" + reg.Bhigh3;
            _regTreeView.Nodes["_regBX"].Nodes["_eBXlow"].Nodes["_BXhigh2"].Nodes["_Blow3"].Text = "Bl = 0x" + reg.Blow3;
            _regTreeView.Nodes["_regBX"].Nodes["_eBXlow"].Nodes["_BXhigh2"].Text = "BXhigh = 0x" + reg.BXhigh2;
            _regTreeView.Nodes["_regBX"].Nodes["_eBXlow"].Nodes["_BXlow2"].Nodes["_Bhigh4"].Text = "Bh = 0x" + reg.Bhigh4;
            _regTreeView.Nodes["_regBX"].Nodes["_eBXlow"].Nodes["_BXlow2"].Nodes["_Blow4"].Text = "Bl = 0x" + reg.Blow4;
            _regTreeView.Nodes["_regBX"].Nodes["_eBXlow"].Nodes["_BXlow2"].Text = "BXlow = 0x" + reg.BXlow2;
            _regTreeView.Nodes["_regBX"].Nodes["_eBXlow"].Text = "EBXlow = 0x" + reg.EBXlow;

            _regTreeView.Nodes["_regBX"].Text = "RBX = 0x" + reg.RBX;
            #endregion
            #region RCX
            _regTreeView.Nodes["_regCX"].Nodes["_eCXhigh"].Nodes["_CXhigh1"].Nodes["_Chigh1"].Text = "Ch = 0x" + reg.Chigh1;
            _regTreeView.Nodes["_regCX"].Nodes["_eCXhigh"].Nodes["_CXhigh1"].Nodes["_Clow1"].Text = "Cl = 0x" + reg.Clow1;
            _regTreeView.Nodes["_regCX"].Nodes["_eCXhigh"].Nodes["_CXhigh1"].Text = "CXhigh = 0x" + reg.CXhigh1;
            _regTreeView.Nodes["_regCX"].Nodes["_eCXhigh"].Nodes["_CXlow1"].Nodes["_Chigh2"].Text = "Ch = 0x" + reg.Chigh2;
            _regTreeView.Nodes["_regCX"].Nodes["_eCXhigh"].Nodes["_CXlow1"].Nodes["_Clow2"].Text = "Cl = 0x" + reg.Clow2;
            _regTreeView.Nodes["_regCX"].Nodes["_eCXhigh"].Nodes["_CXlow1"].Text = "CXlow = 0x" + reg.CXlow1;
            _regTreeView.Nodes["_regCX"].Nodes["_eCXhigh"].Text = "ECXhigh = 0x" + reg.ECXhigh;

            _regTreeView.Nodes["_regCX"].Nodes["_eCXlow"].Nodes["_CXhigh2"].Nodes["_Chigh3"].Text = "Ch = 0x" + reg.Chigh3;
            _regTreeView.Nodes["_regCX"].Nodes["_eCXlow"].Nodes["_CXhigh2"].Nodes["_Clow3"].Text = "Cl = 0x" + reg.Clow3;
            _regTreeView.Nodes["_regCX"].Nodes["_eCXlow"].Nodes["_CXhigh2"].Text = "CXhigh = 0x" + reg.CXhigh2;
            _regTreeView.Nodes["_regCX"].Nodes["_eCXlow"].Nodes["_CXlow2"].Nodes["_Chigh4"].Text = "Ch = 0x" + reg.Chigh4;
            _regTreeView.Nodes["_regCX"].Nodes["_eCXlow"].Nodes["_CXlow2"].Nodes["_Clow4"].Text = "Cl = 0x" + reg.Clow4;
            _regTreeView.Nodes["_regCX"].Nodes["_eCXlow"].Nodes["_CXlow2"].Text = "CXlow = 0x" + reg.CXlow2;
            _regTreeView.Nodes["_regCX"].Nodes["_eCXlow"].Text = "ECXlow = 0x" + reg.ECXlow;

            _regTreeView.Nodes["_regCX"].Text = "RCX = 0x" + reg.RCX;
            #endregion
            #region RDX
            _regTreeView.Nodes["_regDX"].Nodes["_eDXhigh"].Nodes["_DXhigh1"].Nodes["_Dhigh1"].Text = "Dh = 0x" + reg.Dhigh1;
            _regTreeView.Nodes["_regDX"].Nodes["_eDXhigh"].Nodes["_DXhigh1"].Nodes["_Dlow1"].Text = "Dl = 0x" + reg.Dlow1;
            _regTreeView.Nodes["_regDX"].Nodes["_eDXhigh"].Nodes["_DXhigh1"].Text = "DXhigh = 0x" + reg.DXhigh1;
            _regTreeView.Nodes["_regDX"].Nodes["_eDXhigh"].Nodes["_DXlow1"].Nodes["_Dhigh2"].Text = "Dh = 0x" + reg.Dhigh2;
            _regTreeView.Nodes["_regDX"].Nodes["_eDXhigh"].Nodes["_DXlow1"].Nodes["_Dlow2"].Text = "Dl = 0x" + reg.Dlow2;
            _regTreeView.Nodes["_regDX"].Nodes["_eDXhigh"].Nodes["_DXlow1"].Text = "DXlow = 0x" + reg.DXlow1;
            _regTreeView.Nodes["_regDX"].Nodes["_eDXhigh"].Text = "EDXhigh = 0x" + reg.EDXhigh;

            _regTreeView.Nodes["_regDX"].Nodes["_eDXlow"].Nodes["_DXhigh2"].Nodes["_Dhigh3"].Text = "Dh = 0x" + reg.Dhigh3;
            _regTreeView.Nodes["_regDX"].Nodes["_eDXlow"].Nodes["_DXhigh2"].Nodes["_Dlow3"].Text = "Dl = 0x" + reg.Dlow3;
            _regTreeView.Nodes["_regDX"].Nodes["_eDXlow"].Nodes["_DXhigh2"].Text = "DXhigh = 0x" + reg.DXhigh2;
            _regTreeView.Nodes["_regDX"].Nodes["_eDXlow"].Nodes["_DXlow2"].Nodes["_Dhigh4"].Text = "Dh = 0x" + reg.Dhigh4;
            _regTreeView.Nodes["_regDX"].Nodes["_eDXlow"].Nodes["_DXlow2"].Nodes["_Dlow4"].Text = "Dl = 0x" + reg.Dlow4;
            _regTreeView.Nodes["_regDX"].Nodes["_eDXlow"].Nodes["_DXlow2"].Text = "DXlow = 0x" + reg.DXlow2;
            _regTreeView.Nodes["_regDX"].Nodes["_eDXlow"].Text = "EDXlow = 0x" + reg.EDXlow;

            _regTreeView.Nodes["_regDX"].Text = "RDX = 0x" + reg.RDX;
            #endregion
            #region RSP, RDP
            _regTreeView.Nodes["_regSP"].Text = "SP = 0x" + reg.SP;
            _regTreeView.Nodes["_regDP"].Text = "DP = 0x" + reg.DP;
            #endregion
            #region SCLP
            _regTreeView.Nodes["_regSCLP"].Nodes["_SC"].Text = "SC = 0x" + reg.SC;
            _regTreeView.Nodes["_regSCLP"].Nodes["_LP"].Text = "LP = 0x" + reg.LP;
            _regTreeView.Nodes["_regSCLP"].Text = "SCLP = 0x" + reg.SCLP;
            #endregion
            _regTreeView.Nodes["_regSREG"].Text = "SREG = 0x" + reg.SREG;
            _regTreeView.Nodes["_regDEBUG"].Text = "DEBUG = 0x" + reg.DEBUG;
            #region CSIP
            _regTreeView.Nodes["_regCSIP"].Nodes["_CS"].Text = "CS = 0x" + reg.CS;
            _regTreeView.Nodes["_regCSIP"].Nodes["_IP"].Text = "IP = 0x" + reg.IP;
            _regTreeView.Nodes["_regCSIP"].Text = "CSIP = 0x" + reg.CSIP;
            #endregion
            _regTreeView.Nodes["_regCOMMAND"].Text = "COMMAND = 0x" + reg.COMMAND;
        }
        
        private void _readRegBtn_Click(object sender, EventArgs e)
        {
            ShowRegisters();
        }

        private void _startBtn_Click(object sender, EventArgs e)
        {
            _sclp.Value.Values[0] = START_VALUE;
            _sclp.SaveStruct();
            _signature.Value.Values[0] = START_VALUE;
            _signature.SaveStruct();
        }

        private void _debugBtn_Click(object sender, EventArgs e)
        {
            _sclp.Value.Values[0] = START_DEBUG;
            _sclp.SaveStruct();
        }

        private void PauseContinue(object sender, EventArgs e)
        {
            Button btn = (Button) sender;
            bool bit = btn == _continueBtn;
            ushort sclp = Convert.ToUInt16(_regLogicProg.Value.SCLP, 16);
            sclp = Common.SetBit(sclp, 0, bit);
            RegLogicProg reg = _regLogicProg.Value;
            reg.SCLP = Convert.ToString(sclp, 16).ToUpper().PadLeft(4, '0');
            _regLogicProg.Value = reg;
            _regLogicProg.SaveStruct();
        }

        private void _resetBtn_Click(object sender, EventArgs e)
        {
            _sclp.Value.Values[0] = RESET_VALUE;
            _sclp.SaveStruct();
            _signature.Value.Values[0] = RESET_VALUE;
            _signature.SaveStruct();
        }

        private void _stepBtn_Click(object sender, EventArgs e)
        {
            ushort value = Convert.ToUInt16(_regLogicProg.Value.SCLP, 16);
            _sclp.Value.Values[0] = Common.SetBit(value, 8, true);
            _sclp.SaveStruct();
        }

        private void _byPointBtn_Click(object sender, EventArgs e)
        {
            _buttonsGroup.Controls.Clear();
            _buttonsGroup.Controls.Add(_goToPointBtn);
            Label label = new Label();
            label.Text = "Введите адрес";
            label.Location = new Point(6,60);
            label.Size = new Size(125,15);
            _buttonsGroup.Controls.Add(label);
            _address.Text = string.Empty;
            _buttonsGroup.Controls.Add(_address);
            _stopBtn.Enabled = false;
            _buttonsGroup.Controls.Add(_stopBtn);
            _buttonsGroup.Update();
        }

        private void _goToPointBtn_Click(object sender, EventArgs e)
        {
            _stopBtn.Enabled = true;
            RegLogicProg reg = _regLogicProg.Value;
            reg.DEBUG = _address.Text;
            ushort value = Convert.ToUInt16(_regLogicProg.Value.SCLP,16);
            reg.SCLP = Convert.ToString(Common.SetBit(value, 9, true), 16).ToUpper().PadLeft(4, '0');
            _regLogicProg.Value = reg;
            _regLogicProg.SaveStruct();
        }
        #endregion

        #region Общие функции и обработчики событий

        private void ClearLeds()
        {
            for (int i = 0; i < _stateLeds.Count; i++)
            {
                _stateLeds.Values.ToArray()[i].SetState(false);
            }
        }

        private void _regTreeView_BeforeCollapse(object sender, TreeViewCancelEventArgs e)
        {
            if (_doubleClicked)
            {
                _doubleClicked = false;
                e.Cancel = true;
            }
        }

        private void _regTreeView_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            if (_doubleClicked)
            {
                _doubleClicked = false;
                e.Cancel = true;
            }
        }

        private void _regTreeView_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && e.Clicks == 2)
            {
                TreeNode node = _regTreeView.GetNodeAt(e.X, e.Y);
                if (node != null)
                {
                    _doubleClicked = true;
                }
            }
        }

        private void MLK_LogicProgramm_FormClosing(object sender, FormClosingEventArgs e)
        {
            _logicProgFile.RemoveStructQueries();
            _regLogicProg.RemoveStructQueries();
        }

        private void _regTreeView_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs ev)
        {
            try
            {
                int nodeNum = Convert.ToInt32(ev.Node.Tag);
                string[] regInfo = ev.Node.Text.Split(new string[] { " = 0x" }, StringSplitOptions.RemoveEmptyEntries);
                RegisterForm form = new RegisterForm(regInfo[0], regInfo[1].Length, nodeNum, _regLogicProg);
                if (form.ShowDialog() == DialogResult.OK)
                {
                    _regLogicProg.SaveStruct();
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, ERROR, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void _address_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 'g' && e.KeyChar <= 'z') || (e.KeyChar >= 'G' && e.KeyChar <= 'Z'))
            {
                MaskedTextBox textBox = sender as MaskedTextBox;
                if (textBox != null && !_isReplace)
                {
                    _oldText = textBox.Text;
                    _isReplace = true;
                }
            }
        }

        private void _address_KeyUp(object sender, KeyEventArgs e)
        {
            if (_isReplace)
            {
                _address.Text = _oldText;
                _isReplace = false;
            }

            _goToPointBtn.Enabled = _address.Text.Length == 4;
        }

        private void MLK_LogicProgramm_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            this.MdiParent.Refresh();
            _regLogicProg.LoadStructCycle();
        }

        private void QueryCheckBoxCheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = sender as CheckBox;
            if (cb == null) return;
            if (cb.Checked)
            {
                _regLogicProg.LoadStructCycle();
            }
            else
            {
                _regLogicProg.RemoveStructQueries();
            }
        }
        #endregion

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MLK); }
        }

        public bool Multishow { get; private set; }

        #endregion

        #region INodeView Members

        public Type ClassType
        {
            get { return typeof(MlkJournals); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.programming.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Логическая программа"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        //Коэффициенты логики
        private void _saveInFileBtn2_Click(object sender, EventArgs e)
        {
            _isWriteFile = false;
            _saveInFileBtn.Enabled = false;
            _writeProgrammBtn.Enabled = false;
            _readFileBtn.Enabled = false;
            if (_saveLogicProgDlg.ShowDialog() != DialogResult.OK) return;
            SomeStruct ss =
                new SomeStruct(6144); //12 килобайт
            _logicProgFileConst.Clear();
            _logicProgFileConst.Value = ss;
            _logicProgFileConst.Values = ss.Values;
            _logicProgFileConst.Slots = QueriesForm.SetSlots(ss.Values, StartAdressLogicConstV2);
            _logicProgFileConst.LoadStruct();
            _stateProgressBar2.Maximum = _logicProgFileConst.Slots.Count;
            _stateProgressBar2.Value = 0;
            _stateLableProgFileConst.Text = READING_FILE;
        }

        //Коэффициенты логики
        private void _writeProgramBtn2_Click(object sender, EventArgs e)
        {
            _isConstantLP = true;
            bool res = ReadValuesFromFile(_isConstantLP);
            if (!res) return;
            ushort[] values = Common.TOWORDS(_logicProgValues, true);
            if (values.Count() <= 6144)
            {
                SomeStruct ss = new SomeStruct();
                ss.Values = values;
                _logicProgFileConst.Clear();
                _logicProgFileConst.Value = ss;
                _logicProgFileConst.Slots = QueriesForm.SetSlots(values, StartAdressLogicConstV2);
                _logicProgFileConst.SaveStruct();

                _isWriteFile = true;
                _stateProgressBar2.Maximum = _logicProgFileConst.Slots.Count;
                _stateProgressBar2.Value = 0;
                _stateLableProgFileConst.Text = WRITING_FILE;
                _stateProgressBar2.Visible = true;
                _writeProgramBtn2.Enabled = false;
            }
            else
            {
                MessageBox.Show(ERROR_SIZE_OVER);
            }
            _saveInFileBtn.Enabled = false;
            _writeProgrammBtn.Enabled = false;
            _readFileBtn.Enabled = false;
        }
        //Коэффициенты логики
        private void _readFileBtn2_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK != _openLogicProgDlg.ShowDialog()) return;
            _fileForWriting2.Text = _openLogicProgDlg.FileName;
            _filePath = _openLogicProgDlg.FileName;
            _writeProgramBtn2.Enabled = true;
        }
        private void ProgrammFileConstAllReadOk()
        {
            if (_logicProgFileConst != null)
            {
                switch (_isWriteFile)
                {
                    case true:
                        CheckReadedFile(_logicProgFileConst, WRITE_SUCCSESS_LP_CONST);
                        break;
                    case false:
                        SaveProgInFile(_logicProgFileConst, SAVE_IN_FILE_CONST_SUCCSESS_);
                        break;
                    case null:
                        break;
                }
            }

        }

        private void ProgrammFileConstAllWriteOk()
        {
            _logicProgFileConst.LoadStruct();
            _stateProgressBar2.Maximum = _logicProgFileConst.Slots.Count;
            _stateProgressBar2.Value = 0;
            _stateLableProgFileConst.Text = CHECK_FILE;
        }

        private void ProgramFileAllWriteOk()
        {
            _logicProgFile.LoadStruct();
            _stateProgrBar.Maximum = _logicProgFile.Slots.Count;
            _stateProgrBar.Value = 0;
            _stateLableProgramFile.Text = CHECK_FILE;
        }
    }
}
