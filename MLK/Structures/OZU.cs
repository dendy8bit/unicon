﻿using System.Collections.Generic;
using System.Runtime.InteropServices;
using BEMN.Devices;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.MBServer;

namespace BEMN.MLK.Structures
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct OZU : IStruct, IStructInit
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2048)]
        private byte[] _LPRam;	//	оперативка для логики

        #region IStruct Members

        public StructInfo GetStructInfo(int slotLen)
        {
            return StructHelper.GetStructInfo(this.GetType(), slotLen);
        }

        public object GetSlots(ushort start, bool slotArray, int slotLen)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType(), slotLen);
        }

        #endregion

        #region [IStructInit]

        public void InitStruct(byte[] array)
        {
            _LPRam = array;
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.AddRange(Common.TOWORDS(_LPRam, true));
            return result.ToArray();
        }

        #endregion

        public byte[] LPRam
        {
            get { return _LPRam; }
            set { _LPRam = value; }
        }
    }
}
