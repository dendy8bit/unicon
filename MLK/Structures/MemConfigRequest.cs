﻿using System.Runtime.InteropServices;
using System.Collections.Generic;
using BEMN.Devices;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.MLK.Structures.IntermediateStructures;

namespace BEMN.MLK.Structures
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct MemConfigRequest : IStruct, IStructInit
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
        public QueryElem[] Request;

        #region [IStruct Members]
        public StructInfo GetStructInfo(int slotLen)
        {
            return StructHelper.GetStructInfo(this.GetType(), slotLen);
        }

        public object GetSlots(ushort start, bool slotArray, int slotLen)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType(), slotLen);
        }
        #endregion [IStruct Members]

        #region [IStructInit]

        public void InitStruct(byte[] array)
        {
            int ind = 0;
            Request = new QueryElem[64];

            StructHelper.GetArrayStruct(array, Request, ref ind);
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            foreach (var r in Request)
            {
                result.AddRange(r.GetValues());
            }
            return result.ToArray();
        }

        #endregion
    }
}
