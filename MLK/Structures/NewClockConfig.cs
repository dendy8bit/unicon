﻿using System.Runtime.InteropServices;
using System.Collections.Generic;
using BEMN.Devices;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.MBServer;


namespace BEMN.MLK.Structures
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct NewClockConfig : IStruct, IStructInit
    {
        private ushort _calendarSetting;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
        private ushort[] _reserved;
        private ushort _correction;

        #region [IStruct Members]
        public StructInfo GetStructInfo(int slotLen)
        {
            return StructHelper.GetStructInfo(this.GetType(), slotLen);
        }

        public object GetSlots(ushort start, bool slotArray, int slotLen)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType(), slotLen);
        }
        #endregion [IStruct Members]

        #region [IStructInit]
        public void InitStruct(byte[] array)
        {
            int ind = 0;
            _calendarSetting = Common.TOWORD(array[ind + 1], array[ind]);
            ind += sizeof (ushort);

            _reserved = new ushort[3];
            ind += sizeof(ushort) * 3;

            _correction = Common.TOWORD(array[ind + 1], array[ind]);
        }

        public ushort[] GetValues()
        {
            List<ushort> ret = new List<ushort>();
            ret.Add(_calendarSetting);
            ret.AddRange(_reserved);
            ret.Add(_correction);
            return ret.ToArray();
        }
        #endregion

        #region Properties

        public ushort TimeZone
        {
            get { return (ushort)(Common.GetBits(_calendarSetting, 8, 9, 10, 11, 12, 13, 14,15)>>8); }
            set { _calendarSetting = Common.SetBits(_calendarSetting, value, 8, 9, 10, 11, 12, 13, 14, 15); }
        }

        public bool SummerTime
        {
            get { return Common.GetBit(_calendarSetting, 2); }
            set { _calendarSetting = Common.SetBit(_calendarSetting, 2, value); }
        }

        public bool IncDec
        {
            get { return Common.GetBit(_correction, 15); }
            set { _correction = Common.SetBit(_correction, 15, value); }
        }

        public ushort Correction
        {
            get { return Common.GetBits(_correction, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14); }
            set { _correction = Common.SetBits(_correction, value, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14); }
        }
        #endregion
    }
}