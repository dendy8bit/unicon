﻿using System.Drawing;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using BEMN.Devices;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.MBServer;

namespace BEMN.MLK.Structures
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct LedStruct : IStruct, IStructInit
    {
        private byte _ledMask;
        private byte _reserve;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)] private ushort[] _leds;

        #region [IStruct Members]

        public StructInfo GetStructInfo(int slotLen)
        {
            return StructHelper.GetStructInfo(this.GetType(), slotLen);
        }

        public object GetSlots(ushort start, bool slotArray, int slotLen)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType(), slotLen);
        }

        #endregion [IStruct Members]

        #region [IStructInit]

        public void InitStruct(byte[] array)
        {
            int index = 0;
            _ledMask = array[index++];
            _reserve = array[index++];
            _leds = new ushort[4];
            for (int i = 0; i < _leds.Length; i++)
            {
                _leds[i] = Common.TOWORD(array[index + 1], array[index]);
                index += sizeof (ushort);
            }
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            ushort mask = Common.TOWORD(_reserve, _ledMask);
            result.Add(mask);
            result.AddRange(_leds);
            return result.ToArray();
        }

        #endregion

        public Color Leds_L1_1
        {
            get { return (Common.GetBit(_ledMask, 0)) ? Color.Red : Color.Gray; }
        }

        public Color Leds_L1_2
        {
            get { return (Common.GetBit(_ledMask, 1)) ? Color.LightGreen : Color.Gray; }
        }

        public Color Leds_L2
        {
            get { return (Common.GetBit(_ledMask, 2)) ? Color.Yellow : Color.Gray; }
        }

        public Color Leds_L3
        {
            get { return (Common.GetBit(_ledMask, 3)) ? Color.Yellow : Color.Gray; }
        }
    }
}
