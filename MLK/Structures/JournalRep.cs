﻿using System.Runtime.InteropServices;
using System.Collections.Generic;
using BEMN.Devices;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.MBServer;

namespace BEMN.MLK.Structures
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct JournalRep : IStruct, IStructInit
    {
        private ushort _message;
        private ushort _lowParam;
        private ushort _highParam;
        private byte _reserved;
        private byte _dayOfWeek;
        private byte _day;
        private byte _month;
        private byte _hour;
        private byte _year;
        private byte _sec;
        private byte _min;
        private ushort _msec;

        #region [IStruct Members]
        public StructInfo GetStructInfo(int slotLen)
        {
            return StructHelper.GetStructInfo(this.GetType(), slotLen);
        }

        public object GetSlots(ushort start, bool slotArray, int slotLen)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType(), slotLen);
        }
        #endregion [IStruct Members]

        #region [IStructInit]
        public void InitStruct(byte[] array)
        {
            int index = 0;

            _message = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            _lowParam = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof (ushort);

            _highParam = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof (ushort);

            _reserved = array[index];
            index += sizeof(byte);

            _dayOfWeek = array[index];
            index += sizeof(byte);
            
            _day = array[index];
            index += sizeof(byte);

            _month = array[index];
            index += sizeof(byte);
            
            _year = array[index];
            index += sizeof(byte);
            
            _hour = array[index];
            index += sizeof(byte);
            
            _min = array[index];
            index += sizeof(byte);

            _sec = array[index];
            index += sizeof(byte);

            _msec = Common.TOWORD(array[index + 1], array[index]);
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.Add(_message);
            result.Add(_lowParam);
            result.Add(_highParam);
            result.Add(Common.TOWORD(_dayOfWeek, _reserved));
            result.Add(Common.TOWORD(_month, _day));
            result.Add(Common.TOWORD(_hour, _year));
            result.Add(Common.TOWORD(_sec, _min));
            result.Add(_msec);
            return result.ToArray();
        }
        #endregion

        #region Property

        public ushort Message
        {
            get { return _message; }
        }

        public ushort LowParam
        {
            get { return _lowParam; }
        }

        public ushort HighParam
        {
            get { return _highParam; }
        }
        public byte DayOfWeek
        {
            get { return _dayOfWeek; }
        }

        public byte Day
        {
            get { return _day; }
        }

        public byte Month
        {
            get { return _month; }
        }

        public byte Hour
        {
            get { return _hour; }
        }

        public byte Year
        {
            get { return _year; }
        }

        public byte Sec
        {
            get { return _sec; }
        }

        public byte Min
        {
            get { return _min; }
        }

        public ushort Msec
        {
            get { return _msec; }
        }

        #endregion
    }
}
