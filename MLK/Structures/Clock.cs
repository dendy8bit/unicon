﻿using System.Runtime.InteropServices;
using System.Collections.Generic;
using BEMN.Devices;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.MBServer;

namespace BEMN.MLK.Structures
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct Clock : IStruct, IStructInit
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        private ushort[] timeAstronomical;	        //	время астрономическое
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        private ushort[] timeReserve;				//	резерв

        #region [IStruct Members]
        public StructInfo GetStructInfo(int slotLen)
        {
            return StructHelper.GetStructInfo(this.GetType(), slotLen);
        }

        public object GetSlots(ushort start, bool slotArray, int slotLen)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType(), slotLen);
        }
        #endregion [IStruct Members]

        #region [IStructInit]
        public void InitStruct(byte[] array)
        {
            timeAstronomical = new ushort[8];

            int index = 0;

            for (int i = 0; i < timeAstronomical.Length; i++)
            {
                timeAstronomical[i] = Common.TOWORD(array[index + 1], array[index]);
                index += sizeof(ushort);
            }

            timeReserve = new ushort[8];
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.AddRange(timeAstronomical);
            result.AddRange(timeReserve);
            return result.ToArray();
        }
        #endregion

        /// <summary>
        /// День
        /// </summary>
        public ushort Day
        {
            get
            {
                return timeAstronomical[1];
            }
            set
            {
                timeAstronomical[1] = value;
            }
        }
        /// <summary>
        /// Месяц
        /// </summary>
        public ushort Month
        {
            get
            {
                return timeAstronomical[2];
            }
            set
            {
                timeAstronomical[2] = value;
            }
        }
        /// <summary>
        /// Год
        /// </summary>
        public ushort Year
        {
            get
            {
                return timeAstronomical[3];
            }
            set
            {
                timeAstronomical[3] = value;
            }
        }
        /// <summary>
        /// Часы
        /// </summary>
        public ushort Hour
        {
            get
            {
                return timeAstronomical[4];
            }
            set
            {
                timeAstronomical[4] = value;
            }
        }
        /// <summary>
        /// Минуты
        /// </summary>
        public ushort Minutes
        {
            get
            {
                return timeAstronomical[5];
            }
            set
            {
                timeAstronomical[5] = value;
            }
        }
        /// <summary>
        /// Секунды
        /// </summary>
        public ushort Seconds
        {
            get
            {
                return timeAstronomical[6];
            }
            set
            {
                timeAstronomical[6] = value;
            }
        }
    }
}
