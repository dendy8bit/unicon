﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BEMN.Devices;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.MBServer;

namespace BEMN.MLK.Structures
{
    public struct UIORam : IStruct, IStructInit
    {
        private ushort _analog;
        private ushort _discret;
        private ushort _relay;
        public LedStruct Leds;

        #region [IStruct Members]
        public StructInfo GetStructInfo(int slotLen)
        {
            return StructHelper.GetStructInfo(this.GetType(), slotLen);
        }

        public object GetSlots(ushort start, bool slotArray, int slotLen)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType(), slotLen);
        }
        #endregion [IStruct Members]

        #region [IStructInit]
        public void InitStruct(byte[] array)
        {
            int index = 0;
            _analog = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof (ushort);

            _discret = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof (ushort);

            _relay = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof (ushort);

            Leds = StructHelper.GetOneStruct(array, ref index, Leds, 64);
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.Add(_analog);
            result.Add(_discret);
            result.Add(_relay);
            result.AddRange(Leds.GetValues());
            return result.ToArray();
        }
        #endregion

        public ushort Analog
        {
            get { return _analog; }
        }
        /// <summary>
        /// Дискреты, начиная с младшего бита
        /// </summary>
        public BitArray Discrets
        {
            get
            {
                byte[] buf = Common.TOBYTE(_discret).Reverse().ToArray();
                BitArray ret = new BitArray(buf);
                return ret;
            }
        }

        public BitArray Relay
        {
            get
            {
                byte[] buf = Common.TOBYTE(_relay).Reverse().ToArray();
                BitArray ret = new BitArray(buf);
                return ret;
            }
            set
            {
                _relay = Common.BitsToUshort(value);
            }
        }
    }
}
