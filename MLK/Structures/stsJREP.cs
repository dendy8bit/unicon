﻿using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using BEMN.Devices;
using BEMN.MBServer;

namespace BEMN.MLK.Structures
{
    /*19*/
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct stsJREP : IStruct, IStructInit
    {
        public UInt16 _event;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public byte[] reserved;
        public byte reserve;
        public byte year;
        public byte date;
        public byte month;
        public byte hour;
        public byte day;
        public byte sec;
        public byte min;
        public UInt16 msec;

        #region [IStruct Members]
        public StructInfo GetStructInfo()
        {
            return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool slotArray, out int arrayLength)
        {
            return StructHelper.GetSlots(start, slotArray, out arrayLength, this.GetType());
        }
        #endregion [IStruct Members]

        #region [IStructInit]
        public void InitStruct(byte[] array)
        {
            reserved = new byte[4];
            reserve = 0;

            int index = 0;

            _event = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            for (int i = 0; i < reserved.Length; i++)
            {
                index += sizeof(byte);
            }

            reserve = array[index];
            index += sizeof(byte);

            year = array[index];
            index += sizeof(byte);

            date = array[index];
            index += sizeof(byte);

            month = array[index];
            index += sizeof(byte);

            hour = array[index];
            index += sizeof(byte);

            day = array[index];
            index += sizeof(byte);

            sec = array[index];
            index += sizeof(byte);

            min = array[index];
            index += sizeof(byte);

            msec = Common.TOWORD(array[index + 1], array[index]);
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.Add(_event);
            result.AddRange(Common.TOWORDS(reserved, true));
            result.Add(Common.TOWORD(year, reserve));
            result.Add(Common.TOWORD(month, date));
            result.Add(Common.TOWORD(day, hour));
            result.Add(Common.TOWORD(min, sec));
            result.Add(msec);
            return result.ToArray();
        }
        #endregion
    }
}
