﻿using System.Runtime.InteropServices;
using System.Collections.Generic;
using BEMN.Devices;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.MBServer;

namespace BEMN.MLK.Structures
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct ConfigLogicProg : IStruct, IStructInit
    {
        private ushort _signature;
        private ushort _cycleTime;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)] 
        private ushort[] _reserved;

        #region [IStruct Members]
        public StructInfo GetStructInfo(int slotLen)
        {
            return StructHelper.GetStructInfo(this.GetType(), slotLen);
        }

        public object GetSlots(ushort start, bool slotArray, int slotLen)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType(), slotLen);
        }
        #endregion [IStruct Members]

        #region [IStuctInit]

        public void InitStruct(byte[] array)
        {
            int ind = 0;
            _signature = Common.TOWORD(array[ind + 1], array[ind]);
            ind += sizeof (ushort);
            _cycleTime = Common.TOWORD(array[ind + 1], array[ind]);
            _reserved = new ushort[6];
        }

        public ushort[] GetValues()
        {
            List<ushort> ret = new List<ushort>();
            ret.Add(_signature);
            ret.Add(_cycleTime);
            ret.AddRange(_reserved);
            return ret.ToArray();
        }

        #endregion
    }
}
