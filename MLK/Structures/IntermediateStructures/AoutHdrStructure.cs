﻿using System.Runtime.InteropServices;
using BEMN.MBServer;

namespace BEMN.MLK.Structures.IntermediateStructures
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct AoutHdrStructure
    {
        private ushort _magic;
        private ushort _vstamp;
        private ushort _verscompl;
        private ushort _versos;

        private uint _tsize;
        private uint _dsize;
        private uint _entry;
        private uint _text_start;
        private uint _data_start;
        private uint _CRC;

        public ushort Magic
        { get { return _magic; } }

        public ushort Vstamp
        { get { return _vstamp; } }

        public ushort Verscompl
        { get { return _verscompl; } }

        public uint TextSize
        { get { return _tsize; } }

        public uint DataSize
        { get { return _dsize; } }

        public AoutHdrStructure(byte[] buf, int ind)
        {
            int index = ind;
            _magic = Common.TOWORD(buf[index], buf[index + 1]);
            index += sizeof(ushort);
            _vstamp = Common.TOWORD(buf[index], buf[index + 1]);
            index += sizeof(ushort);
            _verscompl = Common.TOWORD(buf[index], buf[index + 1]);
            index += sizeof(ushort);
            _versos = Common.TOWORD(buf[index], buf[index + 1]);
            index += sizeof(ushort);

            _tsize = Common.ToWord32(buf[index + 2], buf[index + 3], buf[index], buf[index + 1]);
            index += sizeof(uint);
            _dsize = Common.ToWord32(buf[index + 2], buf[index + 3], buf[index], buf[index + 1]);
            index += sizeof(uint);
            _entry = Common.ToWord32(buf[index + 2], buf[index + 3], buf[index], buf[index + 1]);
            index += sizeof(uint);
            _text_start = Common.ToWord32(buf[index + 2], buf[index + 3], buf[index], buf[index + 1]);
            index += sizeof(uint);
            _data_start = Common.ToWord32(buf[index + 2], buf[index + 3], buf[index], buf[index + 1]);
            index += sizeof(uint);
            _CRC = Common.ToWord32(buf[index + 2], buf[index + 3], buf[index], buf[index + 1]);
        }
    }
}
