﻿using System.Collections.Generic;
using BEMN.Devices;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.MBServer;

namespace BEMN.MLK.Structures.IntermediateStructures
{
    public struct QueryElem : IStruct, IStructInit
    {
        private byte _phase;						//	фаза опроса
        private byte _devNum;					    //	сетевой номер устройства
        private byte _command;					    //	команда
        private byte _slaveLow;						//	адрес подчинённого
        private byte _slaveHi;
        private byte _masterLow;					//	адрес мастера
        private byte _masterHi;
        private byte _paramNum;						//	колчиство параметров


        #region [IStruct Members]
        public StructInfo GetStructInfo(int slotLen)
        {
            return StructHelper.GetStructInfo(this.GetType(), slotLen);
        }

        public object GetSlots(ushort start, bool slotArray, int slotLen)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType(), slotLen);
        }
        #endregion [IStruct Members]

        #region IStructInit Members

        public void InitStruct(byte[] array)
        {
            _phase = array[0];
            _devNum = array[1];
            _command = array[2];
            _slaveHi = array[3];
            _slaveLow = array[4];
            _masterHi = array[5];
            _masterLow = array[6];
            _paramNum = array[7];
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.Add(Common.TOWORD(_devNum, _phase));
            result.Add(Common.TOWORD(_slaveHi, _command));
            result.Add(Common.TOWORD(_masterHi, _slaveLow));
            result.Add(Common.TOWORD(_paramNum, _masterLow));
            return result.ToArray();
        }
        #endregion

        #region Properties
        public byte QuerysPhase
        {
            get
            {
                return _phase;
            }
            set
            {
                _phase = value;
            }
        }

        public byte QuerysDevNum
        {
            get
            {
                return _devNum;
            }
            set
            {
                _devNum = value;
            }
        }

        public byte QuerysCommand
        {
            get
            {
                return _command;
            }
            set
            {
                _command = value;
            }
        }

        public byte QuerysSlaveHi
        {
            get
            {
                return _slaveHi;
            }
            set
            {
                _slaveHi = value;
            }
        }

        public byte QuerysSlaveLow
        {
            get
            {
                return _slaveLow;
            }
            set
            {
                _slaveLow = value;
            }
        }

        public byte QuerysMasterHi
        {
            get
            {
                return _masterHi;
            }
            set
            {
                _masterHi = value;
            }
        }

        public byte QuerysMasterLow
        {
            get
            {
                return _masterLow;
            }
            set
            {
                _masterLow = value;
            }
        }

        public byte QuerysParamNum
        {
            get
            {
                return _paramNum;
            }
            set
            {
                _paramNum = value;
            }
        }
        #endregion
    }
}
