﻿using System.Runtime.InteropServices;
using BEMN.MBServer;

namespace BEMN.MLK.Structures.IntermediateStructures
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct FileHdrStructure
    {
        private ushort f_magic;
        private ushort f_nscns;
        private uint f_timdat;
        private uint f_symptr;
        private uint f_nsyms;
        private ushort f_opthdr;
        private ushort f_flags;

        public ushort Magic
        { get { return f_magic; } }

        public ushort OptHdr
        { get { return f_opthdr; } }

        public FileHdrStructure(byte[] buf, int ind)
        {
            int index = ind;
            f_magic = Common.TOWORD(buf[index], buf[index+1]);
            index += sizeof (ushort);

            f_nscns = Common.TOWORD(buf[index], buf[index + 1]);
            index += sizeof(ushort);

            f_timdat = Common.ToWord32(buf[index + 2], buf[index + 3], buf[index], buf[index + 1]);
            index += sizeof(uint);

            f_symptr = Common.ToWord32(buf[index + 2], buf[index + 3], buf[index], buf[index + 1]);
            index += sizeof(uint);

            f_nsyms = Common.ToWord32(buf[index + 2], buf[index + 3], buf[index], buf[index + 1]);
            index += sizeof(uint);

            f_opthdr = Common.TOWORD(buf[index], buf[index + 1]);
            index += sizeof(ushort);

            f_flags = Common.TOWORD(buf[index], buf[index + 1]);
        }
    }
}
