﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using BEMN.Devices;
using BEMN.MBServer;

namespace BEMN.MLK.Structures
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct SysJournal : IStruct, IStructInit
    {
        public stsJCP jcp;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 128)]
        public stsJREP[] jrep;

        #region IStruct Members
        public StructInfo GetStructInfo()
        {
            return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool slotArray, out int arrayLength)
        {
            return StructHelper.GetSlots(start, slotArray, out arrayLength, this.GetType());
        }
        #endregion

        #region [IStructInit]
        public void InitStruct(byte[] array)
        {
            int index = 0;
            var oneStruct = new byte[Marshal.SizeOf(typeof(stsJCP))];
            Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
            jcp.InitStruct(oneStruct);
            index += Marshal.SizeOf(typeof(stsJCP));

            jrep = new stsJREP[128];
            for (int i = 0; i < jrep.Length; i++)
            {
                oneStruct = new byte[Marshal.SizeOf(typeof(stsJREP))];
                Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
                jrep[i].InitStruct(oneStruct);
                index += Marshal.SizeOf(typeof(stsJREP));
            }
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.AddRange(jcp.GetValues());
            foreach (stsJREP j in jrep)
            {
                result.AddRange(j.GetValues());
            }
            return result.ToArray();
        } 
        #endregion
    }
}
