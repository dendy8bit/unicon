﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using BEMN.Forms.Controllers;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Framework.Properties;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MLK.HelpClasses;
using BEMN.MLK.Structures;
using BEMN.MLK.Structures.IntermediateStructures;

namespace BEMN.MLK
{
    public partial class MlkConfiguration : Form, IFormView
    {
        #region Поля
        private MLK _device;
        private ComboBox[]  _USBCombos,
                            _RS485Combos;

        private MaskedTextBox[] _ULONGUSBMaskedTextBoxes,
                                _ULONGRS485MaskedTextBoxes,
                                _ULONGPereferyMaskedTextBoxes,
                                _ULONGRequestCountMaskedTextBox,
                                _ULONGCorrectionMaskedTextBox,
                                _DOUBLEPercentMaskedTextBox ;

        private string _oldValue;
        private string _oldCellValue;

        private bool _validatingOk;
        private bool _readAnalog;
        private bool _readGister;
        private bool _isStart;
        private bool _isGister;
        private bool _bKoefSaving;
        private bool _aKoefSaving;

        private byte _step;
        private double _temp;
        private int _counter;
        private ReadConfigMessagesForm _messagesForm;
        private Button _continueBtn;
        private List<double> _tempList; 

        private MemoryEntity<MemConfigRS485> _configRS485; 
        private MemoryEntity<MemConfigUSB> _configUSB;
        private MemoryEntity<QueryCount> _queryCount; 
        private MemoryEntity<MemConfigRequest> _configRequest;
        private MemoryEntity<AnalogExtConfig> _calibrovkaAnalog;
        private MemoryEntity<GisterezisExtConfig> _calibrovkaGister;
        private MemoryEntity<SomeStruct> _outputSignal;
        private MemoryEntity<UIORam> _analog; 
        private MemoryEntity<ClockConfig> _clockConfig;
        private MemoryEntity<NewClockConfig> _newClockConfig; 
        private MemoryEntity<VersionDevice> _version;

        private AnalogExtConfig _currentConfig;
        private GisterezisExtConfig _currentGister;
        private byte _currentOutputSignal;
        #endregion

        #region Конструкторы
        public MlkConfiguration()
        {
            InitializeComponent();
        }

        public MlkConfiguration(MLK device)
        {
            InitializeComponent();
            _device = device;
            _tempList = new List<double>();
            #region USB
            _configUSB = _device.ConfigUSB;
            _configUSB.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, ReadUSBOk);
            _configUSB.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _statusLabel.Text = ConfigurationStringConst.ERROR_READ_USB;
                _statusLabel.BackColor = Color.Red;
                if (_messagesForm != null) _messagesForm.OperationCommplete(false);
            });
            _configUSB.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _statusLabel.Text = ConfigurationStringConst.WRITE_USB_OK;
                _statusLabel.BackColor = Color.Green;
            });
            _configUSB.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _statusLabel.Text = ConfigurationStringConst.WRITE_USB_FAIL;
                _statusLabel.BackColor = Color.Red;
            });
            #endregion
            #region RS-485
            _configRS485 = _device.ConfigRs485;
            _configRS485.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, ReadRS485Ok);
            _configRS485.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _statusLabel.Text = ConfigurationStringConst.ERROR_READ_RS485;
                _statusLabel.BackColor = Color.Red;
                if (_messagesForm != null) _messagesForm.OperationCommplete(false);
            });
            _configRS485.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _statusLabel.Text = ConfigurationStringConst.WRITE_RS485_OK;
                _statusLabel.BackColor = Color.Green;
            });
            _configRS485.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _statusLabel.Text = ConfigurationStringConst.WRITE_RS485_FAIL;
                _statusLabel.BackColor = Color.Red;
            });
            #endregion
            #region Request

            _queryCount = _device.QueryCount;
            _queryCount.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _configRequest.LoadStruct();
                _querysCountBox.Text = _queryCount.Value.CountOfRequest.ToString();
                if (_messagesForm != null) _messagesForm.OperationCommplete(true);
            });
            _queryCount.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _statusLabel.Text = ConfigurationStringConst.ERROR_READ_REQUESTS;
                _statusLabel.BackColor = Color.Red;
                if (_messagesForm != null) _messagesForm.OperationCommplete(false);
            });
            _queryCount.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this,
                () => _device.ReadNewCountOfRequests());

            _configRequest = _device.ConfigRequest;
            _configRequest.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, ReadRequestOk);
            _configRequest.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _statusLabel.Text = ConfigurationStringConst.ERROR_READ_REQUESTS;
                _statusLabel.BackColor = Color.Red;
                if (_messagesForm != null) _messagesForm.OperationCommplete(false);
            });
            _configRequest.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _statusLabel.Text = ConfigurationStringConst.WRITE_REQUEST_OK;
                _statusLabel.BackColor = Color.Green;
            });
            _configRequest.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _statusLabel.Text = ConfigurationStringConst.WRITE_REQUEST_FAIL;
                _statusLabel.BackColor = Color.Red;
            });
            #endregion
            #region Calibrovka
            _calibrovkaAnalog = _device.CalibrovkaAnalog;
            _calibrovkaAnalog.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, ReadAnalogConfigOk);
            _calibrovkaAnalog.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _statusLabel.Text = ConfigurationStringConst.ERROR_READ_ANALOG_CONFIG;
                _statusLabel.BackColor = Color.Red;
                if (_messagesForm != null) _messagesForm.OperationCommplete(false);
            });
            _calibrovkaAnalog.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, AnalogCalibrovkaWriteOk);
            _calibrovkaAnalog.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, WriteAnalogConfigFail);

            _calibrovkaGister = _device.CalibrovkaGister;
            _calibrovkaGister.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, ReadGisterConfigOk);
            _calibrovkaGister.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _statusLabel.Text = ConfigurationStringConst.ERROR_READ_GISTER_CONFIG;
                _statusLabel.BackColor = Color.Red;
            });
            _calibrovkaGister.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, WriteGisterConfigOk);
            _calibrovkaGister.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, WriteGisterConfigFail);
            _calibrovkaGister.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, GisterCalibrovkaWriteOk);
            _calibrovkaGister.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, GisterCalibrovkaWriteFail);

            _analog = _device.UioRam;
            _analog.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, AnalogChannelReadOk);
            _analog.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _statusLabel.Text = ConfigurationStringConst.READ_ANALOG_FAIL;
                _statusLabel.BackColor = Color.Red;
            });
            _continueBtn = new Button
            {
                Size = new Size(88, 23),
                Location = new Point(324, 314),
                Text = @"Продолжить",
                FlatStyle = FlatStyle.Flat,
                Visible = false,
                Enabled = true
            };
            _analogPage.Controls.Add(_continueBtn);

            _outputSignal = _device.OutputSignal;
            _outputSignal.Slots = new List<Device.slot>();
            _outputSignal.Slots.Add(new Device.slot((ushort)_outputSignal.StartAddress, (ushort)(_outputSignal.StartAddress+1)));
            _outputSignal.Values = new ushort[1];
            _outputSignal.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, ReadBitOk);
            _outputSignal.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, ReadBitFail);
            _outputSignal.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, WriteBitOk);
            _outputSignal.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, WriteBitFail);
            try
            {
                var vers = Common.VersionConverter(_device.DevicePlant);
                if ((vers >= 1.0 && vers < 2.0) || (vers >= 2.3)) SetNewPosition();
            }
            catch (Exception)
            {
                //
            }
            #endregion
            #region ClockConfig

            if (_device.Version.Value.AppPodtip.ToUpper() == "SX")
            {
                _calendarSettingsGroup.Visible = true;
                _newClockConfig = _device.NewClockConfig;
                _newClockConfig.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, ReadNewClockConfigOk);
                _newClockConfig.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                {
                    _statusLabel.Text = ConfigurationStringConst.ERROR_READ_CLOCKCONF;
                    _statusLabel.BackColor = Color.Red;
                    if (_messagesForm != null) _messagesForm.OperationCommplete(false);
                });
                _newClockConfig.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
                {
                    _statusLabel.Text = ConfigurationStringConst.WRITE_CLOCKCONF_OK;
                    _statusLabel.BackColor = Color.Green;
                });
                _newClockConfig.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                {
                    _statusLabel.Text = ConfigurationStringConst.WRITE_CLOCKCONF_FAIL;
                    _statusLabel.BackColor = Color.Red;
                });
            }
            else
            {
                _clockConfig = _device.ClockConfig;
                _clockConfig.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, ReadClockConfigOk);
                _clockConfig.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                {
                    _statusLabel.Text = ConfigurationStringConst.ERROR_READ_CLOCKCONF;
                    _statusLabel.BackColor = Color.Red;
                    if (_messagesForm != null) _messagesForm.OperationCommplete(false);
                });
                _clockConfig.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
                {
                    _statusLabel.Text = ConfigurationStringConst.WRITE_CLOCKCONF_OK;
                    _statusLabel.BackColor = Color.Green;
                });
                _clockConfig.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                {
                    _statusLabel.Text = ConfigurationStringConst.WRITE_CLOCKCONF_FAIL;
                    _statusLabel.BackColor = Color.Red;
                });
            }

            #endregion
            #region Version
            _version = _device.Version;
            _version.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, ReadVersionComplite);
            _version.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this.label6.Text = this._appModLabel.Text = this._appPodtipLabel.Text = this._appVersionLabel.Text = ConfigurationStringConst.NAN;
                this._devModLabel.Text = this._devNameLabel.Text = this._devPodtipLabel.Text = this._devVersionLabel.Text = ConfigurationStringConst.NAN;
                this._ldrCodProcLabel.Text = this._ldrFuseProcLabel.Text = this._ldrNameLabel.Text = this._ldrRevisionLabel.Text = ConfigurationStringConst.NAN;
                _statusLabel.Text = ConfigurationStringConst.READ_VERSION_FAIL;
                _statusLabel.BackColor = Color.Red;
                if (_messagesForm != null) _messagesForm.OperationCommplete(false);
            });
            #endregion
        }

        private void SetNewPosition()
        {
            amplitudeGroupBox.Visible = signalGroupBox.Visible =
                _rBtn2.Visible = _gisterezisBtn.Visible = outpValueGroupBox.Visible = false;
            _rBtn1.Location = new Point(13, 24);
            _rBtn3.Location = new Point(13, 47);
            //outpValueGroupBox.Location = new Point(7, 53);
        }
        #endregion

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MLK); }
        }

        public bool Multishow { get; private set; }

        #endregion

        #region INodeView Members

        public Type ClassType
        {
            get { return typeof(MlkConfiguration); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.config.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Конфигурация"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        #region Версия
        public void ReadVersionComplite()
        {
            #region AppVers
            _appNameLabel.Text = _version.Value.AppName;
            _appPodtipLabel.Text = _version.Value.AppPodtip;
            _appModLabel.Text = _version.Value.AppMod;
            _appVersionLabel.Text = _version.Value.AppVersion;
            #endregion

            #region DevVers
            _devNameLabel.Text = _version.Value.DevName;
            _devPodtipLabel.Text = _version.Value.DevPodtip;
            _devModLabel.Text = _version.Value.DevMod;
            _devVersionLabel.Text = _version.Value.DevVersion;
            #endregion

            #region LdrVers
            _ldrNameLabel.Text = _version.Value.LdrName;
            _ldrCodProcLabel.Text = _version.Value.LdrCodProc;
            _ldrFuseProcLabel.Text = _version.Value.LdrFuseProc;
            _ldrRevisionLabel.Text = _version.Value.LdrVersion;
            #endregion

            _statusLabel.Text = ConfigurationStringConst.READ_VERSION_OK;
            _statusLabel.BackColor = Color.Green;
            if (_messagesForm != null) _messagesForm.OperationCommplete(true);
        }
        #endregion

        #region USB
        public void PrepareUSB()
        {
            _USBCombos = new ComboBox[]
            {
                _USBspeedsCombo,
                _USBdataBitsCombo,
                _USBstopBitsCombo,
                _USBparitetCHETCombo,
                _USBparitetYNCombo,
                _USBdoubleSpeedCombo
            };

            _ULONGUSBMaskedTextBoxes = new MaskedTextBox[]
            {_USBAddressTB, _USBToSendBeforeTB, _USBToSendAfterTB};

            ClearCombos(_USBCombos);

            FillUSBCombo();

            SubscriptCombos(_USBCombos);

            PrepareMaskedBoxes(_ULONGUSBMaskedTextBoxes, typeof(ulong));

        }

        public void FillUSBCombo()
        {
            if (_USBspeedsCombo.Items.Count == 0)
            {
                _USBspeedsCombo.Items.AddRange(Strings.RS_SPEEDS.ToArray());
            }
            if (_USBdataBitsCombo.Items.Count == 0)
            {
                _USBdataBitsCombo.Items.AddRange(Strings.RS_DATA_BITS.ToArray());
            }
            if (_USBstopBitsCombo.Items.Count == 0)
            {
                _USBstopBitsCombo.Items.AddRange(Strings.RS_STOPBITS.ToArray());
            }
            if (_USBparitetYNCombo.Items.Count == 0)
            {
                _USBparitetYNCombo.Items.AddRange(Strings.RS_PARITET_YN.ToArray());
            }
            if (_USBparitetCHETCombo.Items.Count == 0)
            {
                _USBparitetCHETCombo.Items.AddRange(Strings.RS_PARITET_CHET.ToArray());
            }
            if (_USBdoubleSpeedCombo.Items.Count == 0)
            {
                _USBdoubleSpeedCombo.Items.AddRange(Strings.RS_DOUBLESPEED.ToArray());
            }
        }

        public void ReadUSBOk()
        {
            _USBspeedsCombo.SelectedItem = _configUSB.Value.USBSpeed;
            _USBdataBitsCombo.SelectedItem = _configUSB.Value.USBDataBits;
            _USBstopBitsCombo.SelectedItem = _configUSB.Value.USBStopBits;
            _USBparitetYNCombo.SelectedItem = _configUSB.Value.USBParitetOnOff;
            _USBparitetCHETCombo.SelectedItem = _configUSB.Value.USBParitetChet;
            _USBdoubleSpeedCombo.SelectedItem = _configUSB.Value.USBDoubleSpeed;
            _USBAddressTB.Text = _configUSB.Value.USBAddress.ToString();
            _USBToSendBeforeTB.Text = _configUSB.Value.USBToSendBefore.ToString();
            _USBToSendAfterTB.Text = _configUSB.Value.USBToSendAfter.ToString();
            if (_USBparitetYNCombo.SelectedIndex > 0)
            {
                _USBparitetCHETCombo.Enabled = true;
            }
            _statusLabel.Text = ConfigurationStringConst.READ_USB_OK;
            _statusLabel.BackColor = Color.Green;
            if (_messagesForm != null) _messagesForm.OperationCommplete(true);
        }

        public bool WriteUSB() 
        {
            bool res = true;
            try
            {
                MemConfigUSB config = new MemConfigUSB();
                config.InitStruct(new byte[_configUSB.Values.Length*2]);
                config.USBSpeed = _USBspeedsCombo.SelectedItem.ToString();
                config.USBDataBits = _USBdataBitsCombo.SelectedItem.ToString();
                config.USBStopBits = _USBstopBitsCombo.SelectedItem.ToString();
                config.USBParitetOnOff = _USBparitetYNCombo.SelectedItem.ToString();
                config.USBParitetChet = _USBparitetCHETCombo.SelectedItem.ToString();
                config.USBDoubleSpeed = _USBdoubleSpeedCombo.SelectedItem.ToString();
                config.USBAddress = Convert.ToByte(_USBAddressTB.Text);
                config.USBToSendBefore = Convert.ToByte(_USBToSendBeforeTB.Text);
                config.USBToSendAfter = Convert.ToByte(_USBToSendAfterTB.Text);
                _configUSB.Value = config;
            }
            catch 
            {
                res = false;
            }
            return res;
        }
        #endregion

        #region RS485
        public void PrepareRS485()
        {
            _RS485Combos = new ComboBox[]
            {
                _rs485dataBitsCombo,
                _rs485speedsCombo,
                _rs485stopBitsCombo,
                _rs485paritetCHETCombo,
                _rs485paritetYNCombo,
                _rs485doubleSpeedCombo
            };

            _ULONGRS485MaskedTextBoxes = new MaskedTextBox[]
            {_rs485AddressTB, _rs485ToSendTB, _rs485ToSendBeforeTB, _rs485ToSendAfterTB, _rs485AnswerTB};

            ClearCombos(_RS485Combos);

            FillRS485Combo();

            SubscriptCombos(_RS485Combos);

            PrepareMaskedBoxes(_ULONGRS485MaskedTextBoxes, typeof(ulong));

        }

        public void FillRS485Combo()
        {
            if (_rs485dataBitsCombo.Items.Count == 0)
            {
                _rs485dataBitsCombo.Items.AddRange(Strings.RS_DATA_BITS.ToArray());
            }
            if (_rs485speedsCombo.Items.Count == 0)
            {
                _rs485speedsCombo.Items.AddRange(Strings.RS_SPEEDS.ToArray());
            }
            if (_rs485stopBitsCombo.Items.Count == 0)
            {
                _rs485stopBitsCombo.Items.AddRange(Strings.RS_STOPBITS.ToArray());
            }
            if (_rs485paritetYNCombo.Items.Count == 0)
            {
                _rs485paritetYNCombo.Items.AddRange(Strings.RS_PARITET_YN.ToArray());
            }
            if (_rs485paritetCHETCombo.Items.Count == 0)
            {
                _rs485paritetCHETCombo.Items.AddRange(Strings.RS_PARITET_CHET.ToArray());
            }
            if (_rs485doubleSpeedCombo.Items.Count == 0)
            {
                _rs485doubleSpeedCombo.Items.AddRange(Strings.RS_DOUBLESPEED.ToArray());
            }
        }

        public void ReadRS485Ok()
        {
            try
            {
                _rs485modeCheck.Checked = _configRS485.Value.RS485Mode;
                _rs485speedsCombo.SelectedItem = _configRS485.Value.RS485Speed;
                _rs485dataBitsCombo.SelectedItem = _configRS485.Value.RS485DataBits;
                _rs485stopBitsCombo.SelectedItem = _configRS485.Value.RS485StopBits;
                _rs485paritetYNCombo.SelectedItem = _configRS485.Value.RS485ParitetOnOff;
                _rs485paritetCHETCombo.SelectedItem = _configRS485.Value.RS485ParitetChet;
                _rs485doubleSpeedCombo.SelectedItem = _configRS485.Value.RS485DoubleSpeed;
                _rs485AddressTB.Text = _configRS485.Value.RS485Address.ToString();
                _rs485ToSendTB.Text = _configRS485.Value.RS485ToSend.ToString();
                _rs485ToSendBeforeTB.Text = _configRS485.Value.RS485ToSendBefore.ToString();
                _rs485ToSendAfterTB.Text = _configRS485.Value.RS485ToSendAfter.ToString();
                _rs485AnswerTB.Text = _configRS485.Value.RS485Answer.ToString();
                if (_rs485paritetYNCombo.SelectedIndex > 0)
                {
                    _rs485paritetCHETCombo.Enabled = true;
                }
                _statusLabel.Text = ConfigurationStringConst.READ_RS485_OK;
                _statusLabel.BackColor = Color.Green;
                if (_messagesForm != null) _messagesForm.OperationCommplete(true);
            }
            catch (Exception)
            {}
        }

        public bool WriteRS485()
        {
            bool res = true;
            try
            {
                MemConfigRS485 config = new MemConfigRS485();
                config.InitStruct(new byte[_configRS485.Values.Length*2]);
                config.RS485Mode = _rs485modeCheck.Checked;
                config.RS485Speed = _rs485speedsCombo.SelectedItem.ToString();
                config.RS485DataBits = _rs485dataBitsCombo.SelectedItem.ToString();
                config.RS485StopBits = _rs485stopBitsCombo.SelectedItem.ToString();
                config.RS485ParitetOnOff = _rs485paritetYNCombo.SelectedItem.ToString();
                config.RS485ParitetChet = _rs485paritetCHETCombo.SelectedItem.ToString();
                config.RS485DoubleSpeed = _rs485doubleSpeedCombo.SelectedItem.ToString();
                config.RS485Address = Convert.ToByte(_rs485AddressTB.Text);
                config.RS485ToSend = Convert.ToByte(_rs485ToSendTB.Text);
                config.RS485ToSendBefore = Convert.ToByte(_rs485ToSendBeforeTB.Text);
                config.RS485ToSendAfter = Convert.ToByte(_rs485ToSendAfterTB.Text);
                config.RS485Answer = Convert.ToUInt16(_rs485AnswerTB.Text);
                _configRS485.Value = config;
            }
            catch
            {
                res = false;
            }
            return res;
        }
        #endregion

        #region Запросы

        #region Количество запросов

        public void PrepareRequestCount()
        {
            _ULONGRequestCountMaskedTextBox = new MaskedTextBox[] { _querysCountBox };

            PrepareMaskedBoxes(_ULONGRequestCountMaskedTextBox, typeof(ulong));
        }

        #endregion

        public void PrepareRequest()
        {
            _commandCol.Items.AddRange(Strings.Command.ToArray());
            if (_querys.Rows.Count == 0)
            {
                for (int i = 0; i < (int)_queryCount.Value.CountOfRequest; i++)
                {
                    _querys.Rows.Add(new object[]{(i + 1).ToString(),
                                                              0,
                                                              0,
                                                              0,
                                                              0,
                                                              0,
                                                              0
                    });
                }
            }
        }

        public void ReadRequestOk()
        {
            _querys.Rows.Clear();
            if (_queryCount.Value.CountOfRequest > Convert.ToInt32(_querysCountBox.Text))
            {
                for (int i = 0; i < Convert.ToInt32(_querysCountBox.Text); i++)
                {
                    _querys.Rows.Add((i + 1).ToString(),
                        PrepareGridParam(_configRequest.Value.Request[i].QuerysPhase.ToString("X")),
                        PrepareGridParam(_configRequest.Value.Request[i].QuerysDevNum.ToString("X")),
                        Strings.Command[_configRequest.Value.Request[i].QuerysCommand],
                        PrepareGridParam(_configRequest.Value.Request[i].QuerysSlaveHi.ToString("X")) +
                        PrepareGridParam(_configRequest.Value.Request[i].QuerysSlaveLow.ToString("X")),
                        PrepareGridParam(_configRequest.Value.Request[i].QuerysMasterHi.ToString("X")) +
                        PrepareGridParam(_configRequest.Value.Request[i].QuerysMasterLow.ToString("X")),
                        PrepareGridParam(_configRequest.Value.Request[i].QuerysParamNum.ToString("X")));
                }
            }
            if (_queryCount.Value.CountOfRequest < Convert.ToInt32(_querysCountBox.Text))
            {
                for (int i = 0; i < _queryCount.Value.CountOfRequest; i++)
                {
                    _querys.Rows.Add((i + 1).ToString(),
                        PrepareGridParam(_configRequest.Value.Request[i].QuerysPhase.ToString("X")),
                        PrepareGridParam(_configRequest.Value.Request[i].QuerysDevNum.ToString("X")),
                        Strings.Command[_configRequest.Value.Request[i].QuerysCommand],
                        PrepareGridParam(_configRequest.Value.Request[i].QuerysSlaveHi.ToString("X")) +
                        PrepareGridParam(_configRequest.Value.Request[i].QuerysSlaveLow.ToString("X")),
                        PrepareGridParam(_configRequest.Value.Request[i].QuerysMasterHi.ToString("X")) +
                        PrepareGridParam(_configRequest.Value.Request[i].QuerysMasterLow.ToString("X")),
                        PrepareGridParam(_configRequest.Value.Request[i].QuerysParamNum.ToString("X")));
                }
                for (int i = _queryCount.Value.CountOfRequest; i < Convert.ToInt32(_querysCountBox.Text); i++)
                {
                    _querys.Rows.Add((i + 1).ToString(), "00", "00", PrepareGridCombo(0), "0000", "0000", "00");
                }
            }
            if (_queryCount.Value.CountOfRequest == Convert.ToInt32(_querysCountBox.Text))
            {
                for (int i = 0; i < Convert.ToInt32(_querysCountBox.Text); i++)
                {
                    _querys.Rows.Add((i + 1).ToString(),
                        PrepareGridParam(_configRequest.Value.Request[i].QuerysPhase.ToString("X")),
                        PrepareGridParam(_configRequest.Value.Request[i].QuerysDevNum.ToString("X")),
                        Strings.Command[_configRequest.Value.Request[i].QuerysCommand],
                        PrepareGridParam(_configRequest.Value.Request[i].QuerysSlaveHi.ToString("X")) +
                        PrepareGridParam(_configRequest.Value.Request[i].QuerysSlaveLow.ToString("X")),
                        PrepareGridParam(_configRequest.Value.Request[i].QuerysMasterHi.ToString("X")) +
                        PrepareGridParam(_configRequest.Value.Request[i].QuerysMasterLow.ToString("X")),
                        PrepareGridParam(_configRequest.Value.Request[i].QuerysParamNum.ToString("X")));
                }
            }
            _statusLabel.Text = ConfigurationStringConst.READ_REQUESTS_OK;
            _statusLabel.BackColor = Color.Green;
            if (_messagesForm != null) _messagesForm.OperationCommplete(true);
        }

        public bool WriteConfigRequest()
        {
            bool res = true;
            MemConfigRequest config = _configRequest.Value;
            QueryCount count = _queryCount.Value;
            try
            {
                count.CountOfRequest = Convert.ToUInt16(_querysCountBox.Text);
                for (int i = 0; i < UInt16.Parse(_querysCountBox.Text); i++)
                {
                    config.Request[i].QuerysPhase =
                        Byte.Parse(_querys.Rows[i].Cells["_timeCol"].Value.ToString(),
                            System.Globalization.NumberStyles.HexNumber);
                    config.Request[i].QuerysDevNum =
                        Byte.Parse(_querys.Rows[i].Cells["_msgCol"].Value.ToString(),
                            System.Globalization.NumberStyles.HexNumber);
                    config.Request[i].QuerysCommand =
                        (byte) Strings.Command.IndexOf(_querys.Rows[i].Cells["_commandCol"].Value.ToString());
                    config.Request[i].QuerysSlaveHi =
                        Common.HIBYTE(UInt16.Parse(_querys.Rows[i].Cells["_masterAddrCol"].Value.ToString(),
                            System.Globalization.NumberStyles.HexNumber));
                    config.Request[i].QuerysSlaveLow =
                        Common.LOBYTE(UInt16.Parse(_querys.Rows[i].Cells["_masterAddrCol"].Value.ToString(),
                            System.Globalization.NumberStyles.HexNumber));
                    config.Request[i].QuerysMasterHi =
                        Common.HIBYTE(UInt16.Parse(_querys.Rows[i].Cells["_slaveAddrCol"].Value.ToString(),
                            System.Globalization.NumberStyles.HexNumber));
                    config.Request[i].QuerysMasterLow =
                        Common.LOBYTE(UInt16.Parse(_querys.Rows[i].Cells["_slaveAddrCol"].Value.ToString(),
                            System.Globalization.NumberStyles.HexNumber));
                    config.Request[i].QuerysParamNum =
                        Byte.Parse(_querys.Rows[i].Cells["_numCol"].Value.ToString(),
                            System.Globalization.NumberStyles.HexNumber);
                }
                _configRequest.Value = config;
                _queryCount.Value = count;
            }
            catch
            {
                res = false;
                MessageBox.Show(ConfigurationStringConst.ERROR_WRITE_REQUESTS, ConfigurationStringConst.ERROR_WRITE, MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
            }
            return res;
        }
        #endregion

        #region Калибровка (периферия)

        public void PreparePerefery()
        {
            _ULONGPereferyMaskedTextBoxes = new MaskedTextBox[] { _koeffB, _koeffA, _koeffpA, _limit, _lowLimit, _highLimit, _lowLimitSignal, _highLimitSignal };

            _DOUBLEPercentMaskedTextBox = new MaskedTextBox[] { _Uin };

            PrepareMaskedBoxes(_ULONGPereferyMaskedTextBoxes, typeof(ulong));
            PrepareMaskedBoxes(_DOUBLEPercentMaskedTextBox, typeof(double));
        }

        private void AnalogChannelReadOk()
        {
            _ADC.Text = _analog.Value.Analog.ToString();
            if (!ValidateLimit() || !ValidatePercent()) return;
            if (Common.HIBYTE(_outputSignal.Value.Values[0]) == 2)
            {
                _processed.Text = _analog.Value.Analog.ToString();
            }
            else
            {
                switch (_analog.Value.Analog)
                {
                    case (ushort)0xffff:
                        _processed.Text = ConfigurationStringConst.CANAL_ERR;
                        break;
                    case (ushort)0x8000:
                        _processed.Text = ConfigurationStringConst.LOWER_LIMIT;
                        break;
                    case (ushort)0x7fff:
                        _processed.Text = ConfigurationStringConst.UPPER_LIMIT;
                        break;
                    default:
                        _processed.Text =
                            Math.Round(double.Parse(_limit.Text) / 0x7ffe * (double)_analog.Value.Analog, 5).ToString();
                        if (_readAnalog)
                        {
                            MeasuringKoeffB();
                        }
                        if (_readGister)
                        {
                            MeasuringHighLimit();
                        }
                        break;
                }
            }
        }

        #region Аналоговая калибровка

        public void ReadAnalogConfigOk()
        {
            _koeffB.Text = _calibrovkaAnalog.Value.B.ToString();
            _koeffA.Text = _calibrovkaAnalog.Value.A.ToString();
            _koeffpA.Text = _calibrovkaAnalog.Value.Pa.ToString();
            if (_isStart) return;
            _statusLabel.Text = ConfigurationStringConst.READ_ANALOG_CONFIG_OK;
            _statusLabel.BackColor = Color.Green;
            if (_messagesForm != null) _messagesForm.OperationCommplete(true);
        }

        public bool WriteAnalogConfig()
        {
            bool res = true;
            try
            {
                AnalogExtConfig config = _calibrovkaAnalog.Value;
                config.B = Convert.ToUInt16(_koeffB.Text);
                config.A = Convert.ToUInt16(_koeffA.Text);
                config.Pa = Convert.ToUInt16(_koeffpA.Text);
                _calibrovkaAnalog.Value = config;
            }
            catch
            {
                res = false;
            }
            return res;
        }
        //подсчет среднего значения для коэффициента В
        //и переход к соответствующему шагу
        private void AnalogCalibrovkaWriteOk()
        {
            _statusLabel.Text = ConfigurationStringConst.WRITE_ANALOG_CONFIG_OK;
            _statusLabel.BackColor = Color.Green;
            if (_bKoefSaving)
            {
                _descriptionGroup.Text = string.Format(ConfigurationStringConst.STEP, _step);
                _descriptionBox.Text = ConfigurationStringConst.Info2;
                _statusStep.Text = ConfigurationStringConst.B_SAVED;
                _continueBtn.Enabled = true;
                _bKoefSaving = false;
            }
            if (_aKoefSaving)
            {

                _descriptionGroup.Text = string.Format(ConfigurationStringConst.STEP, _step);
                _descriptionBox.Text = ConfigurationStringConst.Info3;
                _statusStep.Text = ConfigurationStringConst.COMPLETE;
                _continueBtn.Enabled = true;
                _aKoefSaving = false;
            }
        }
        private void WriteAnalogConfigFail()
        {
            _statusLabel.Text = ConfigurationStringConst.WRITE_ANALOG_CONFIG_FAIL;
            _statusLabel.BackColor = Color.Red;
            _continueBtn.Enabled = true;
        }

        private void MeasuringKoeffB()
        {
            if (_counter != 100)
            {
                _temp += _analog.Value.Analog;
                _counter++;
            }
            else
            {
                _temp = _temp / 100;
                _readAnalog = false;
                switch (_step)
                {
                    case 2:
                        CalibrovkaStepTwo();
                        break;
                    case 3:
                        CalibrovkaStepThree();
                        break;
                }
            }
        }

        private void CalibrovkaStepOne()
        {
            AnalogExtConfig config = _calibrovkaAnalog.Value;
            config.B = 0x0000;
            config.A = 0x7FFF;
            config.Pa = 0x0000;
            _calibrovkaAnalog.Value = config;
            _calibrovkaAnalog.SaveStruct();
            _calibrovkaAnalog.LoadStruct();
        }

        private void CalibrovkaStepTwo()
        {
            AnalogExtConfig conf = _calibrovkaAnalog.Value;
            conf.B = (ushort)(_temp * 2);
            _calibrovkaAnalog.Value = conf;
            _calibrovkaAnalog.SaveStruct();
            _calibrovkaAnalog.LoadStruct();
            _bKoefSaving = true;
        }

        private void CalibrovkaStepThree()
        {
            _temp = (ulong)(double.Parse(_Uin.Text) * 0x8000 * 0x8000 / double.Parse(_limit.Text) / (double)_temp);
            if (Math.Abs(_temp) < 0.0001)
            {
                _temp = 0x8000;
            }
            ushort _pA = 0;
            while (_temp > 0xFFFF)
            {
                _temp /= 2;
                _pA++;
            }

            AnalogExtConfig conf = _calibrovkaAnalog.Value;
            conf.A = (ushort)_temp;
            conf.Pa = _pA;
            var vers = Common.VersionConverter(_device.DevicePlant);
            if (vers >= 2.0)
            {
                conf.EndOfScale = Convert.ToUInt16(_limit.Text); //Записываем предел шкалы
            }
            _calibrovkaAnalog.Value = conf;
            _calibrovkaAnalog.SaveStruct();
            _calibrovkaAnalog.LoadStruct();
            _aKoefSaving = true;
        }

        private void _continueAnalogCalibrovkaClick(object sender, EventArgs e)
        {
            try
            {
                switch (_step)
                {
                    case 1:
                        {
                            SetOutputSignalBit(true, 0);
                            CalibrovkaStepOne();

                            _continueBtn.Enabled = false;
                            _bKoefSaving = false;
                            _readAnalog = true;
                            _temp = 0;
                            _counter = 0;

                            _limit.Enabled = true;
                            _Uin.Enabled = true;
                            _statusStep.Text = ConfigurationStringConst.B_SAVING;
                            _step++;
                        }
                        break;
                    case 2:
                        {
                            if (!(ValidateLimit() && ValidatePercent()))
                            {
                                _statusStep.Text = ConfigurationStringConst.BAD_LIMIT_PERCENT;
                                return;
                            }
                            if (UInt16.Parse(_ADC.Text) == 0)
                            {
                                _statusStep.Text = ConfigurationStringConst.NO_POWER_SUPPLY;
                                return;
                            }
                            _statusStep.Text = ConfigurationStringConst.A_MEASSURING;

                            _continueBtn.Enabled = false;
                            _aKoefSaving = false;
                            _readAnalog = true;
                            _temp = 0;
                            _counter = 0;

                            _continueBtn.Text = ConfigurationStringConst.BREAK;
                            _koeffA.Enabled = _koeffB.Enabled = _koeffpA.Enabled = true;
                            _step++;
                        }
                        break;
                    case 3:
                        {
                            SetOutputSignalBit(false, 0);
                            _isStart = false;
                            _continueBtn.Text = ConfigurationStringConst.CONTINUE;
                            _continueBtn.Visible = false;
                            _continueBtn.Click -= _continueAnalogCalibrovkaClick;

                            if (_validatingOk && WriteAnalogConfig())
                            {
                                _calibrovkaAnalog.SaveStruct();
                            }
                            _descriptionBox.Text = "";
                            _descriptionGroup.Visible = false;
                            _statusStep.Text = "";
                            _koeffA.Enabled = _koeffB.Enabled = _koeffpA.Enabled = _limit.Enabled = _Uin.Enabled = false;

                            _rBtn1.Enabled = _rBtn2.Enabled = _rBtn3.Enabled = _gisterezisBtn.Enabled = _peripheryWrite.Enabled = true;
                            _calibrovkaBtn.Visible = true;
                        }
                        break;
                }
            }
            catch (Exception)
            {
            }
        }
        #endregion Аналоговая

        #region Гистерезисная калибровка
        public void ReadGisterConfigOk()
        {
            _lowLimit.Text = _calibrovkaGister.Value.LowLimit.ToString();
            _highLimit.Text = _calibrovkaGister.Value.HighLimit.ToString();
            _lowLimitSignal.Text = _calibrovkaGister.Value.LowLimitSignal.ToString();
            _highLimitSignal.Text = _calibrovkaGister.Value.HighLimitSignal.ToString();
            _limit.Text = _calibrovkaGister.Value.EndOfScale.ToString();
            if (_isStart) return;
            _statusLabel.Text = ConfigurationStringConst.READ_GISTER_CONFIG_OK;
            _statusLabel.BackColor = Color.Green;
        }
        public bool WriteGisterConfig()
        {
            bool res = true;
            try
            {
                GisterezisExtConfig config = _calibrovkaGister.Value;
                config.LowLimit = Convert.ToUInt16(_lowLimit.Text);
                config.HighLimit = Convert.ToUInt16(_highLimit.Text);
                config.LowLimitSignal = Convert.ToByte(_lowLimitSignal.Text);
                config.HighLimitSignal = Convert.ToByte(_highLimitSignal.Text);
                config.EndOfScale = Convert.ToUInt16(_limit.Text);
                _calibrovkaGister.Value = config;
            }
            catch
            {
                res = false;
            }
            return res;
        }
        private void _continueGisterezisCalibrovkaClick(object sender, EventArgs e)
        {
            try
            {
                switch (_step)
                {
                    case 1:
                        _statusLabel.Text = "";
                        _descriptionGroup.Text = string.Format(ConfigurationStringConst.STEP, _step + 1);
                        _descriptionBox.Text = ConfigurationStringConst.GistInfo2;
                        _continueBtn.Enabled = false;
                        SetOutputSignalBit(true, 8);
                        break;
                    case 2:
                        _continueBtn.Text = ConfigurationStringConst.BREAK;
                        _continueBtn.Enabled = false;
                        GisterStepTwo();
                        break;
                    case 3:
                        _isStart = false;
                        _continueBtn.Text = ConfigurationStringConst.CONTINUE;
                        _continueBtn.Visible = false;
                        _continueBtn.Click -= _continueGisterezisCalibrovkaClick;
                        _rBtn1.Enabled = _rBtn2.Enabled = _rBtn3.Enabled = true;
                        _descriptionBox.Text = "";
                        _descriptionGroup.Visible = false;
                        _statusStep.Text = "";
                        _lowLimit.Enabled = _highLimit.Enabled = false;
                        _calibrovkaBtn.Visible = true;
                        _gisterezisBtn.Visible = true;
                        _temp = 0;
                        _counter = 0;
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void GisterStepTwo()
        {
            GisterezisExtConfig config = _calibrovkaGister.Value;
            _lowLimit.ValidateText();
            _highLimit.ValidateText();
            if (_validatingOk)
            {
                config.LowLimit = Convert.ToUInt16(_lowLimit.Text);
                config.HighLimit = Convert.ToUInt16(_highLimit.Text);
                _calibrovkaGister.Value = config;
                SetOutputSignalBit(false, 8);
                _calibrovkaGister.SaveStruct();
                _step++;
            }
            else
            {
                _statusStep.Text = ConfigurationStringConst.INVALID_LOW_LIMIT;
            }
        }

        private void MeasuringHighLimit()
        {
            if (_counter != 200)
            {
                _counter++;
                _temp += _analog.Value.Analog;
                _tempList.Add(_analog.Value.Analog);
                return;
            }
            string str = _tempList.Aggregate(string.Empty, (current, val) => current + string.Format("{0}\n", val));
            File.AppendAllText(Environment.CurrentDirectory + "\\Analog.txt", str);
            File.AppendAllText(Environment.CurrentDirectory + "\\Analog.txt",         // Для отладки
                    "\n*******************СЛЕДУЮЩЕЕ ИЗМЕРЕНИЕ*********************\n");
            _highLimit.Text = ((ushort)(_temp / 200)).ToString();
            _statusStep.Text = ConfigurationStringConst.HIGH_LIMIT_MEASURING_COMPLETE;
            GisterCalibrovkaWriteOk();
        }

        private void GisterCalibrovkaWriteOk()
        {
            switch (_step)
            {
                case 1:
                    _statusLabel.Text = "";
                    _statusStep.Text = ConfigurationStringConst.MEASURING_HIGH_LIMIT;
                    _tempList.Clear();
                    _readGister = true;
                    _temp = 0;
                    _counter = 0;
                    _calibrovkaGister.LoadStruct();
                    _step++;
                    break;
                case 2:
                    _statusLabel.Text = "";
                    _continueBtn.Enabled = _lowLimit.Enabled = _highLimit.Enabled = true;
                    _readGister = false;
                    break;
                case 3:
                    _statusLabel.Text = "";
                    _continueBtn.Enabled = true;
                    _calibrovkaGister.LoadStruct();
                    _statusStep.Text = string.Empty;
                    _descriptionGroup.Text = string.Format(ConfigurationStringConst.STEP, _step);
                    _descriptionBox.Text = ConfigurationStringConst.CALIBROVKA_GISTEREZIS_SUCCSESS;
                    break;
                default:
                    _device.ReadNewEndOfScale();
                    break;
            }
        }

        private void WriteGisterConfigOk()
        {
            if (_step == 4) return;
            _statusLabel.Text = ConfigurationStringConst.WRITE_GISTER_CONFIG_OK;
            _statusLabel.BackColor = Color.Green;
        }

        private void GisterCalibrovkaWriteFail()
        {
            if (!_isStart) return;
            _step = 3;
            _continueBtn.Enabled = true;
            _statusStep.Text = ConfigurationStringConst.WRITING_GISTER_ERROR;
            _continueBtn.Text = ConfigurationStringConst.BREAK;
        }

        private void WriteGisterConfigFail()
        {
            _statusLabel.Text = ConfigurationStringConst.WRITE_GISTER_CONFIG_FAIL;
            _statusLabel.BackColor = Color.Red;
        }
        #endregion Гистерезисная
        #endregion

        #region Часы

        private void PrepareClockConfig()
        {
            _ULONGCorrectionMaskedTextBox = new MaskedTextBox[] {_correctionTextBox};

            PrepareMaskedBoxes(_ULONGCorrectionMaskedTextBox, typeof(ulong));
        }

        private string ConvertCorrection(string correctionVal)
        {
            var resultCorrect = Convert.ToUInt32(correctionVal) / 1000;
            return resultCorrect.ToString();
        }
        private void ReadClockConfigOk()
        {
            _correctionTextBox.Text = ConvertCorrection(_clockConfig.Value.Correction.ToString());

            if (_clockConfig.Value.IncDec)
            {
                _decRB.Checked = true;
            }
            else
            {
                _incRB.Checked = true;
            }
            _statusLabel.Text = ConfigurationStringConst.READ_CLOCKCONF_OK;
            _statusLabel.BackColor = Color.Green;
            if (_messagesForm != null) _messagesForm.OperationCommplete(true);
        }

        private void ReadNewClockConfigOk()
        {
            _correctionTextBox.Text = ConvertCorrection(_newClockConfig.Value.Correction.ToString());

            if (_newClockConfig.Value.IncDec)
            {
                _decRB.Checked = true;
            }
            else
            {
                _incRB.Checked = true;
            }
            _summerTime.Checked = _newClockConfig.Value.SummerTime;
            try
            {
                short index = (short)(_newClockConfig.Value.TimeZone-0xFFF4);
                _timeZoneCombo.SelectedIndex = index;
            }
            catch (Exception)
            {
                _timeZoneCombo.SelectedIndex = 0;
            }
            _statusLabel.Text = ConfigurationStringConst.READ_CLOCKCONF_OK;
            _statusLabel.BackColor = Color.Green;
            if (_messagesForm != null) _messagesForm.OperationCommplete(true);
        }
        private bool WriteClockConfig()
        {
            bool res = true;
            try
            {
                if (Int32.Parse(_correctionTextBox.Text) > 30 || Int32.Parse(_correctionTextBox.Text) < 0)
                {
                    res = false;
                    return res;
                }
                if (_device.Version.Value.AppPodtip.ToUpper() == "SX")
                {
                    var config = _newClockConfig.Value;
                    var tbxValue = Int32.Parse(_correctionTextBox.Text);
                    var resultCorrection = tbxValue * 1000;
                    config.Correction = Convert.ToUInt16(resultCorrection);
                    config.IncDec = _decRB.Checked;
                    _newClockConfig.Value = config;
                }
                else
                {
                    var config = _clockConfig.Value;
                    var tbxValue = Int32.Parse(_correctionTextBox.Text);
                    var resultCorrection = tbxValue * 1000;
                    config.Correction = Convert.ToUInt16(resultCorrection);
                    config.IncDec = _decRB.Checked;
                    _clockConfig.Value = config;
                }
            }
            catch (Exception)
            {
                res = false;
            }
            _validatingOk = true;
            return res;
        }

        private bool WriteNewClockConfig()
        {
            bool res = true;
            try
            {
                NewClockConfig conf = _newClockConfig.Value;
                conf.SummerTime = _summerTime.Checked;
                int ind = _timeZoneCombo.SelectedIndex;
                conf.TimeZone = (ushort)(ind + 0xFFF4);
                _newClockConfig.Value = conf;
            }
            catch (Exception)
            {
                res = false;
            }
            return res;
        }
        #endregion Часы

        #region Дополнительные функции
        private void LoadConfigurationBlocks()
        {
            _configUSB.LoadStruct();
            _configRS485.LoadStruct();
            _queryCount.LoadStruct();
 
            _calibrovkaAnalog.LoadStruct();
            _calibrovkaGister.LoadStruct();
            if (_device.Version.Value.AppPodtip.ToUpper() == "SX")
            {
                _newClockConfig.LoadStruct();
            }
            else
            {
                _clockConfig.LoadStruct();
            }
            _version.LoadStruct();
            _outputSignal.LoadStruct();

            _messagesForm = new ReadConfigMessagesForm(7);
            List<string> names = new List<string>()
            {
                "Конфигурация USB", "Конфигурация RS-485", "Параметры калибровки аналогового входа",
                "Конфигурация часов", "Версия устройства", "Количество запросов", "Запросы"
            };
            _messagesForm.ShowDialog(names);
        }

        private string PrepareGridParam(string param)
        {
            if (param.Length < 2)
            {
                param = "0" + param;
            }
            return param;
        }

        private string PrepareGridCombo(byte command)
        {
            return Strings.Command[command];
        }

        private void ClearCombos(ComboBox[] combos)
        {
            for (int i = 0; i < combos.Length; i++)
            {
                combos[i].Items.Clear();
            }
        }

        private void SubscriptCombos(ComboBox[] combos)
        {
            for (int i = 0; i < combos.Length; i++)
            {
                combos[i].DropDown += new EventHandler(Combo_DropDown);
                combos[i].SelectedIndex = 0;
            }
        }

        private void PrepareMaskedBoxes(MaskedTextBox[] boxes, Type validateType)
        {
            for (int i = 0; i < boxes.Length; i++)
            {
                boxes[i].TypeValidationCompleted += new TypeValidationEventHandler(TypeValidation);
                boxes[i].ValidatingType = validateType;
            }
        }
        
        private void TypeValidation(object sender, TypeValidationEventArgs e)
        {
            if (sender is MaskedTextBox)
            {
                MaskedTextBox box = sender as MaskedTextBox;

                if (!e.IsValidInput && box.ValidatingType == typeof(ulong))
                {
                    ShowToolTip(box);
                    return;
                }
                if (!e.IsValidInput && box.ValidatingType == typeof(double))
                {
                    ShowToolTipDouble(box);
                    return;
                }

                if (box.ValidatingType == typeof (ulong) && ulong.Parse(box.Text) > ulong.Parse(box.Tag.ToString()))
                {
                    ShowToolTip(box);
                }
                else if (box.ValidatingType == typeof (double) &&
                         (double.Parse(box.Text) > double.Parse(box.Tag.ToString())) || (double.Parse(box.Text) < 0))
                {
                    ShowToolTipDouble(box);
                }
            }
        }

        private void ShowToolTip(MaskedTextBox box)
        {
            //if (_validatingOk)
            //{
                if (box.Parent.Parent is TabPage)
                {
                    _configurationTabControl.SelectedTab = box.Parent.Parent as TabPage;
                }

                _toolTip.Show("Введите целое число в диапазоне [0-" + box.Tag + "]", box, 2000);
                box.Focus();
                box.SelectAll();
                _validatingOk = false;
            //}
        }

        private void ShowToolTipDouble(MaskedTextBox box)
        {
            //if (_validatingOk)
            //{
                if (box.Parent.Parent is TabPage)
                {
                    _configurationTabControl.SelectedTab = box.Parent.Parent as TabPage;
                }
                _toolTip.Show("Введите целое число в диапазоне [0,0-" + box.Tag + ".0]", box, 2000);
                box.Focus();
                box.SelectAll();
                _validatingOk = false;
            //}
        }

        private void ValidateAll()
        {
            _validatingOk = true;

            ValidateMaskedBoxes(_ULONGUSBMaskedTextBoxes);
            ValidateMaskedBoxes(_ULONGRS485MaskedTextBoxes);
            ValidateMaskedBoxes(_ULONGRequestCountMaskedTextBox);
            ValidateMaskedBoxes(_ULONGPereferyMaskedTextBoxes);
        }

        private void ValidateMaskedBoxes(MaskedTextBox[] boxes)
        {
            foreach (MaskedTextBox box in boxes)
            {
                box.ValidateText();
            }
        }

        private bool ValidateLimit()
        {
            _validatingOk = true;
            _limit.ValidateText();
            return _validatingOk;
        }

        private bool ValidatePercent()
        {
            _validatingOk = true;
            _Uin.ValidateText();
            return _validatingOk;
        }

        private bool ValidateCellValue(ref string value, int len)
        {
            if (value == "" || value.Length > len)
            {
                return false;
            }
            if (value.Length < len)
            {
                value = value.PadLeft(len, '0');
            }
            return value.All(ch => (ch >= 'A' && ch <= 'F') || (ch >= 'a' && ch <= 'f')||(ch>='0' && ch<='9'));
        }

        private void SetOutputSignalBit(bool bit, byte bitNum)
        {
            ushort slotValue = _outputSignal.Value.Values[0];

            switch (bitNum)
            {
                case 0:
                    byte high = Common.HIBYTE(slotValue);
                    if (bit)
                    {
                        _outputSignal.Value.Values[0] = Common.TOWORD(high, 1);
                    }
                    else
                    {
                        _outputSignal.Value.Values[0] = Common.TOWORD(high, 0);
                    }
                    _outputSignal.SaveStruct();
                    break;
                case 8:
                    byte low8 = Common.LOBYTE(slotValue);
                    if (bit)
                    {
                        _outputSignal.Value.Values[0] = Common.TOWORD(1, low8);
                    }
                    else
                    {
                        _outputSignal.Value.Values[0] = Common.TOWORD(0, low8);
                    }
                    _outputSignal.SaveStruct();
                    break;
                case 9:
                    byte low9 = Common.LOBYTE(slotValue);
                    _outputSignal.Value.Values[0] = Common.TOWORD(2, low9);
                    _outputSignal.SaveStruct();
                    break;
                default:
                    _outputSignal.Value.Values[0] = 0;
                    _outputSignal.SaveStruct();
                    break;
            }
        }
        #endregion

        #region Обработчики событий

        private void MLK_Configuration_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            this.MdiParent.Refresh();
            _readAnalog = _readGister = false; 
            _statusLabel.Text = "";
            _statusStep.Text = "";
            _validatingOk = true;
            _isStart = false;
            _step = 0;

            PrepareUSB();
            PrepareRS485();
            PrepareRequestCount();
            PrepareRequest();
            PrepareClockConfig();
            PreparePerefery();

            LoadConfigurationBlocks();
        }
        private void MLK_Configuration_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_isStart && MessageBox.Show(
                "Калибровка не была завершена. При закрытии окна будут восстановлены первоначальные значения.\nПродолжить?",
                "Внимание", MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK)
            {
                if (_isGister)
                {
                    _calibrovkaGister.Value = _currentGister;
                    _calibrovkaGister.SaveStruct();
                    switch (_currentOutputSignal)
                    {
                        case 0:
                            SetOutputSignalBit(false, 8);
                            break;
                        case 1:
                            SetOutputSignalBit(true, 8);
                            break;
                        case 2:
                            SetOutputSignalBit(true, 9);
                            break;
                        default:
                            SetOutputSignalBit(false, 8);
                            break;
                    }
                }
                else
                {
                    _calibrovkaAnalog.Value = _currentConfig;
                    _calibrovkaAnalog.SaveStruct();
                    _analog.RemoveStructQueries();
                }
                _analog.RemoveStructQueries();
            }
            else
            {
                _analog.RemoveStructQueries();
            }           
        }

        private void ReadBitOk()
        {
            try
            {
                var setState = new Action(() =>
                {
                    byte state = Common.HIBYTE(_outputSignal.Value.Values[0]);
                    switch (state)
                    {
                        case 0:
                            _rBtn1.Checked = true;
                            _rBtn2.Checked = _rBtn3.Checked = false;
                            break;
                        case 1:
                            _rBtn2.Checked = true;
                            _rBtn1.Checked = _rBtn3.Checked = false;
                            break;
                        case 2:
                            _rBtn3.Checked = true;
                            _rBtn1.Checked = _rBtn2.Checked = false;
                            break;
                    }
                });
                this.Invoke(setState);
                if (Common.HIBYTE(_outputSignal.Value.Values[0]) == 2)
                {
                    _device.CheckSettedBit(true);
                }
            }
            catch(Exception)
            { }
        }

        private void ReadBitFail()
        {
            try
            {
                var setState = new Action(() =>
                {
                    _statusStep.Text = "Произошла ошибка чтения бита";
                });
                this.Invoke(setState);
                _device.CheckSettedBit(false);          
            }
            catch (Exception)
            { }         
        }
        private void WriteBitOk()
        {
            _outputSignal.LoadStruct();
            if (_step!=1) return;
            this.BeginInvoke(new Action(GisterCalibrovkaWriteOk));
        }

        private void WriteBitFail()
        {
            try
            {
                var setState = new Action(() =>
                {
                    _statusStep.Text = "Произошла ошибка записи бита";
                });
                this.Invoke(setState);
            }
            catch (Exception)
            { }
        }
        private void _readButton_Click(object sender, EventArgs e)
        {
            LoadConfigurationBlocks();
        }

        private void Combo_DropDown(object sender, EventArgs e)
        {
            ComboBox combo = (ComboBox)sender;
            combo.DropDown -= new EventHandler(Combo_DropDown);
            if (combo.Items.Contains("ХХХХХ"))
            {
                combo.Items.Remove("ХХХХХ");
            }
        }

        private void _USBWrite_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes ==
                MessageBox.Show("Записать конфигурацию USB MLK №" + _device.DeviceNumber + " ?", "Внимание",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
            {
                _validatingOk = true;
                ValidateMaskedBoxes(_ULONGUSBMaskedTextBoxes);
                if (_validatingOk && WriteUSB())
                {
                    _statusLabel.Text = string.Empty;
                    _configUSB.SaveStruct();
                }
                else
                {
                    MessageBox.Show(ConfigurationStringConst.BAD_DATA, ConfigurationStringConst.ERROR_WRITE, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void _rs485Write_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes ==
                MessageBox.Show("Записать конфигурацию RS485 MLK №" + _device.DeviceNumber + " ?", "Внимание",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
            {
                _validatingOk = true;
                ValidateMaskedBoxes(_ULONGRS485MaskedTextBoxes);
                if (_validatingOk && WriteRS485())
                {
                    _statusLabel.Text = string.Empty;
                    _configRS485.SaveStruct();
                }
                else
                {
                    MessageBox.Show(ConfigurationStringConst.BAD_DATA, ConfigurationStringConst.ERROR_WRITE, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }
        
        private void _querysWrite_Click(object sender, EventArgs e)
        {

            if (DialogResult.Yes ==
                MessageBox.Show("Записать конфигурацию Запросов MLK №" + _device.DeviceNumber + " ?", "Внимание",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
            {
                _validatingOk = true;
                ValidateMaskedBoxes(_ULONGRequestCountMaskedTextBox);
                if (_validatingOk && WriteConfigRequest())
                {
                    _statusLabel.Text = string.Empty;
                    _queryCount.SaveStruct();
                    _configRequest.SaveStruct();
                }
                else
                {
                    MessageBox.Show(ConfigurationStringConst.BAD_DATA, ConfigurationStringConst.ERROR_WRITE, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void _peripheryWrite_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes ==
                MessageBox.Show("Записать конфигурацию Перефирии MLK №" + _device.DeviceNumber + " ?", "Внимание",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
            {
                _validatingOk = true;
                ValidateMaskedBoxes(_ULONGPereferyMaskedTextBoxes);
                if (_validatingOk && WriteAnalogConfig()&&WriteGisterConfig())
                {
                    _statusLabel.Text = string.Empty;
                    _calibrovkaAnalog.SaveStruct();
                    _calibrovkaGister.SaveStruct();
                    SetOutputSignalBit(_rBtn2.Checked, 8);
                }
                else
                {
                    MessageBox.Show(ConfigurationStringConst.BAD_DATA, ConfigurationStringConst.ERROR_WRITE, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void _writeAllButton_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes ==
                MessageBox.Show("Записать конфигурацию MLK №" + _device.DeviceNumber + " ?", "Внимание",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
            {
                _validatingOk = true;
                ValidateAll();
                if (_validatingOk && WriteUSB() && WriteRS485() && WriteConfigRequest() && WriteAnalogConfig() && WriteGisterConfig())
                {
                    _statusLabel.Text = string.Empty;
                    _configUSB.SaveStruct();
                    _configRS485.SaveStruct();
                    _queryCount.SaveStruct();
                    _configRequest.SaveStruct();
                    _calibrovkaAnalog.SaveStruct();
                    _calibrovkaGister.SaveStruct();
                    SetOutputSignalBit(_rBtn2.Checked, 8);
                }
                else
                {
                    MessageBox.Show(ConfigurationStringConst.BAD_DATA, ConfigurationStringConst.ERROR_WRITE, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void _querysCount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(Char.IsDigit(e.KeyChar)))
            {
                e.Handled = true;
            }
        }

        private void _querysCount_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                int temp = Convert.ToInt32(_querysCountBox.Text);
                if (temp > 64 || temp < 0 || temp.ToString().Length > 2)
                {
                    _querysCountBox.Text = _oldValue;
                }
                else
                {
                    ReadRequestOk();
                }
            }
            catch (Exception)
            {
                int temp = 0;
                _querysCountBox.Text = temp.ToString();
            }
        }
        
        private void _querysCount_KeyDown(object sender, KeyEventArgs e)
        {
            _oldValue = _querysCountBox.Text;
        }
       
        private void _querys_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            var value = dgv.CurrentCell.Value;
            if (value != null)
                _oldCellValue = value.ToString();
            dgv.CurrentCell.Value = "";
        }

        private void _querys_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            var dgv = (DataGridView) sender;
            string value = dgv.CurrentCell.Value.ToString();
            dgv.CurrentCell.Value = ValidateCellValue(ref value, _oldCellValue.Length) 
                ? value.ToUpper() 
                : _oldCellValue;
        }
        
        private void _USBparitetYNCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox combo = (ComboBox) sender;
            if (combo.SelectedIndex == 1)
            {
                _USBparitetCHETCombo.Enabled = true;
            }
            else
            {
                _USBparitetCHETCombo.Enabled = false;
            }
        }
        
        private void _rs485paritetYNCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox combo = (ComboBox)sender;
            if (combo.SelectedIndex == 1)
            {
                _rs485paritetCHETCombo.Enabled = true;
            }
            else
            {
                _rs485paritetCHETCombo.Enabled = false;
            }
        }

        private void _rs485modeCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (_rs485modeCheck.Checked)
            {
                _rs485modeCheck.Text = "Да";
                _rs485ToSendTB.Enabled = true;
                _rs485AnswerTB.Enabled = true;
            }
            else
            {
                _rs485modeCheck.Text = "Нет";
                _rs485ToSendTB.Enabled = false;
                _rs485AnswerTB.Enabled = false;
            }
        }

        private void _correctionButton_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes !=
                MessageBox.Show("Скорректировать часы MLK №" + _device.DeviceNumber + " ?", "Внимание",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)) return;
            ValidateMaskedBoxes(_ULONGCorrectionMaskedTextBox);
            if (WriteClockConfig() && _validatingOk && _device.Version.Value.AppPodtip.ToUpper() == "SX")
            {
                _newClockConfig.SaveStruct();
            }
            else if (WriteClockConfig() && _validatingOk)
            {
                _clockConfig.SaveStruct();
            }
            else
            {
                MessageBox.Show(ConfigurationStringConst.BAD_DATA, ConfigurationStringConst.ERROR_WRITE, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void _acceptBtn_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes !=
               MessageBox.Show("Применить настройки календаря MLK №" + _device.DeviceNumber + " ?", "Внимание",
                   MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)) return;
            if (WriteNewClockConfig() && _validatingOk)
            {
                _newClockConfig.SaveStruct();
            }
            else
            {
                MessageBox.Show(ConfigurationStringConst.BAD_DATA, ConfigurationStringConst.ERROR_WRITE, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        
        private void _readFromDevice_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == MessageBox.Show("Прочитать значения коэффициентов MLK №" + _device.DeviceNumber + " ?", "Внимание",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
            {
                _outputSignal.LoadStruct();
                _calibrovkaAnalog.LoadStruct();
                _calibrovkaGister.LoadStruct();
            }
        }

        private void _limit_TextChanged(object sender, EventArgs e)
        {
            _Uin.Tag = _limit.Text;
        }

        private void _configurationTabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            TabControl control = (TabControl) sender;
            TabPage page = control.SelectedTab;

            if (page.Name == _analogPage.Name)
            {
                MLK_PasswordForm passForm = new MLK_PasswordForm();
                if (passForm.ShowDialog() != DialogResult.OK)
                {
                    control.SelectTab(0);
                    return;
                }
                _analog.LoadStructCycle();
            }
            else
            {
                _analog.RemoveStructQueries();
                _isStart = false;
            }
            if (_step == 0)
                _step = 1;
        }
        
        private void _chanelCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            _continueBtn.Enabled = true;
        }
        
        private void _calibrovkaBtn_Click(object sender, EventArgs e)
        {
            _isStart = true;
            _descriptionGroup.Text = string.Format(ConfigurationStringConst.STEP, _step = 1);
            _descriptionBox.Text = ConfigurationStringConst.Info1;
            _descriptionGroup.Visible = true;
            _calibrovkaBtn.Visible =  false;
            _rBtn1.Enabled = _rBtn2.Enabled = _rBtn3.Enabled = _gisterezisBtn.Enabled = _peripheryWrite.Enabled = false;
            _isGister = false;
            _continueBtn.Visible = true;
            _continueBtn.Click += _continueAnalogCalibrovkaClick;

            _currentConfig = _calibrovkaAnalog.Value;
        }

        private void _gisterezisBtn_Click(object sender, EventArgs e)
        {
            _isStart = true;
            _descriptionGroup.Text = string.Format(ConfigurationStringConst.STEP, _step = 1);
            _descriptionBox.Text = ConfigurationStringConst.GistInfo1;
            _descriptionGroup.Visible = true;
            _calibrovkaBtn.Visible = _gisterezisBtn.Visible = false;
            _rBtn1.Enabled = _rBtn2.Enabled = _rBtn3.Enabled = false;
            _isGister = true;
            _continueBtn.Visible = true;
            _continueBtn.Click += _continueGisterezisCalibrovkaClick;

            _currentGister = _calibrovkaGister.Value;
            _currentOutputSignal = Common.HIBYTE(_outputSignal.Value.Values[0]);
        }
        
        private void _rBtn_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton current = (RadioButton)sender;
            if (current.Checked == false || _step == 0) return;
            byte number = Convert.ToByte(current.Tag);
            switch (number)
            {
                case 0:
                    SetOutputSignalBit(false, 8);
                    break;
                case 1:
                    SetOutputSignalBit(true, 8);
                    break;
                case 2:
                    SetOutputSignalBit(true, 9);
                    break;
                default:
                    SetOutputSignalBit(false, 8);
                    break;
            }
        }
        #endregion
    }
}