﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using BEMN.Devices;
using BEMN.Forms;
using BEMN.Forms.Structures;
using BEMN.Interfaces;
using BEMN.MBServer;

namespace BEMN.MLK.MlkQuerys
{
    public partial class MlkQuerys : Form, IFormView
    {
        #region Fields
        private const int WORD_BITS = 16;
        private int _wordCnt;
        private ushort _currentBoxIndex;
        private MLK _device;
        private MemoryEntity<SomeStruct> _query;
        private HexTextbox _currentBox;
        private Button _currentButton;
        private LedControl[][] _words;
        private AdductionSettings[] _adduction;
        private Label[] _addrLabels;
        private HexTextbox[] _hexBoxes;
        private HexTextbox[] _decBoxes;
        private TextBox[] _adductionBoxes;
        private bool _loop = false;
        #endregion

        #region Const

        private const string SET_BIT_PATTERN = "SetBit_{0}_{1}";
        #endregion

        #region Constructors
        public MlkQuerys()
        { 
            InitializeComponent();
            _countOfWord.SelectedIndex = 0;
            _wordCnt = Convert.ToInt32(_countOfWord.SelectedItem);
            _words = new LedControl[_wordCnt][];
            _adduction = new AdductionSettings[_wordCnt];
            _addrLabels = new Label[_wordCnt];
            _hexBoxes = new HexTextbox[_wordCnt];
            _decBoxes = new HexTextbox[_wordCnt];
            _adductionBoxes = new TextBox[_wordCnt];
        }

        public MlkQuerys(MLK device)
        {
            InitializeComponent();
            _device = device;
            _device.CaptionChanged += new CaptionPropertyChanged(OnDeviceCaptionChanged);
            _device.DeviceNumberChanged += DeviceOnDeviceNumberChanged;

            _countOfWord.SelectedIndex = 0;
            _countOfWord.SelectedIndexChanged += new System.EventHandler(this._countOfWord_SelectedIndexChanged);
            _addrHex.TextChanged += new EventHandler(_addrHex_TextChanged);

            InitNewQuery();
        }
        
        #endregion

        #region Init
        private void MLK_Querys_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            this.MdiParent.Refresh();

            InitControls();
            SetAddrLabelText();

            _addrHex.BorderStyle = BorderStyle.Fixed3D;
            _addrHex.Text = _query.StartAddress.ToString("X");
            _addrHex.Correct();

            _bitfRadio.Checked = _device.IsBit;
        }

        private void InitNewQuery()
        {
            string queryName = string.Format("Обмены{0}",_device.Counter);
            _query = new MemoryEntity<SomeStruct>(queryName, _device.MB, 0x0000)
            {
                DeviceNum = _device.DeviceNumber,
                Caption = _device.Caption
            };
            _query.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, MlkQueryReadComplete);
            _query.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, MlkQueryReadFail);
            _device.Counter++;
        }

        private void InitControls()
        {
            _addrHex.HexBase = true;
            _addrHex.Correct();
            
            _ledsGroup.Controls.Clear();
            _addrGroup.Controls.Clear();
            _decGroup.Controls.Clear();
            _hexGroup.Controls.Clear();
            _adductionGroup.Controls.Clear();

            _wordCnt = _countOfWord.SelectedIndex + 8;

            InitLedsControl();

            _adduction = new AdductionSettings[_wordCnt];
            _addrLabels = new Label[_wordCnt];
            _hexBoxes = new HexTextbox[_wordCnt];
            _decBoxes = new HexTextbox[_wordCnt];
            _adductionBoxes = new TextBox[_wordCnt];
            for (int i = 0; i < _wordCnt; i++)
            {
                CreateAddrLabel(i);
                CreateDecLabel(i);
                CreateHexLabel(i);
                CreateAdductionBox(i);

                _ledsGroup.Controls.AddRange(_words[i]);
                _wordIndexCombo.Items.Add(i + 1);
                _adduction[i] = new AdductionSettings(100.0, 65535, 3);
            }

            for (int j = 0; j < 4; j++)
            {
                _symbolsCombo.Items.Add(j);
            }

            _addrGroup.Controls.AddRange(_addrLabels);
            _decGroup.Controls.AddRange(_decBoxes);
            _hexGroup.Controls.AddRange(_hexBoxes);
            _adductionGroup.Controls.AddRange(_adductionBoxes);

            _wordIndexCombo.SelectedIndex = 0;
            _symbolsCombo.SelectedIndex = 3;
        }

        private void InitLedsControl()
        {
            _words = new LedControl[_wordCnt][];
            for (int i = 0; i < _wordCnt; i++)
            {
                _words[i] = new LedControl[WORD_BITS];
                for (int j = 0; j < WORD_BITS; j++)
                {
                    _words[i][j] = new LedControl();
                    _words[i][j].Location = j < 8
                        ? new Point(198 - j * 12, 14 + i * 18)
                        : new Point(198 - j * 12 - 6, 14 + i * 18);
                    _words[i][j].State = LedState.Off;
                    _words[i][j].pictureBox.MouseClick += new MouseEventHandler(Led_MouseClick);
                    _ledsGroup.Controls.Add(_words[i][j]);
                }
            }
        }
        #endregion

        #region ReadStruct

        public void MlkQueryReadFail()
        {
            if (false == Disposing && false == IsDisposed)
            {
                TurnOff();
            }
        }

        public void MlkQueryReadComplete() 
        {
            if (false == Disposing && false == IsDisposed)
            {
                byte[] buffer = Common.TOBYTES(_query.Values, true);

                for (int i = 0; i < buffer.Length; i += 2)
                {
                    LedManager.SetLeds(_words[i / 2], new BitArray(new byte[] { buffer[i + 1], buffer[i] }));
                    if ((bool)_hexBoxes[i / 2].Tag)
                    {
                        _hexBoxes[i / 2].Text = (Common.TOWORD(buffer[i], buffer[i + 1])).ToString("X4");
                    }
                    if ((bool)_decBoxes[i / 2].Tag)
                    {
                        _decBoxes[i / 2].Text = (Common.TOWORD(buffer[i], buffer[i + 1])).ToString("");
                    }

                    if ((bool)_adductionBoxes[i / 2].Tag)
                    {
                        ushort val = Common.TOWORD(buffer[i], buffer[i + 1]);
                        double adduct = val * _adduction[i / 2].Limit / _adduction[i / 2].Max;

                        _adductionBoxes[i / 2].Text = adduct.ToString("F" + _adduction[i / 2].Symbols);
                    }
                }
            }
        }
        #endregion

        #region Functions
        private void TurnOff()
        {
            if (LedManager.IsControlsCreated(_decBoxes))
            {
                for (int i = 0; i < _wordCnt; i++)
                {
                    LedManager.TurnOffLeds(_words[i]);
                    _hexBoxes[i].Text = "";
                    _decBoxes[i].Text = "";
                    _adductionBoxes[i].Text = "";
                }
            }
        }

        /// <summary>
        /// Отобразить адреса
        /// </summary>
        private void SetAddrLabelText()
        {
            //Если адресация словная
            if (_wordfRadio.Checked)
            {
                for (int i = 0; i < _addrLabels.Length; i++)
                {
                    //Адреса больше 0xFFFF не рисуем
                    if (_addrHex.Number + i <= ushort.MaxValue)
                    {
                        _addrLabels[i].Text = (_addrHex.Number + i).ToString("X4");
                    }
                    else
                    {
                        _addrLabels[i].Text = "";
                    }

                    //Строка слишком длинная, метку надо отодвинуть
                    if (50 > _addrLabels[i].Location.X)
                    {
                        _addrLabels[i].Location = new Point(_addrLabels[i].Location.X + 35, _addrLabels[i].Location.Y);
                    }
                }
            }
            else
            {
                //Если адресация битная
                for (int i = 0; i < _addrLabels.Length; i++)
                {
                    if (_addrHex.Number + i + 0xF <= ushort.MaxValue)
                    {
                        _addrLabels[i].Text = (_addrHex.Number + 0x10 * i + 0xF).ToString("X4") + "-" +
                                              (_addrHex.Number + 0x10 * i).ToString("X4");
                    }
                    else
                    {
                        _addrLabels[i].Text = "";
                    }

                    if (50 == _addrLabels[i].Location.X)
                    {
                        _addrLabels[i].Location = new Point(_addrLabels[i].Location.X - 35, _addrLabels[i].Location.Y);
                    }
                }
            }
        }

        private void LoadWithCurrentSettings()
        {
            if (!_queryEnableCheckBox.Checked) return;
            if (_bitfRadio.Checked)
            {
                if ((ushort)_addrHex.Number < (ushort)(_addrHex.Number + _wordCnt * 0x10))
                {
                    _query.Clear();
                    ushort[] values = new ushort[_wordCnt];
                    _query.Values = values;
                    _query.Slots = _device.SetSlots(values, _query.StartAddress);
                    _query.LoadBitsCicle();
                }
            }
            else
            {
                _query.Clear();
                ushort[] values = new ushort[_wordCnt];
                _query.Values = values;
                _query.Slots = _device.SetSlots(values, _query.StartAddress);
                _query.LoadStructCycle();
            }
        }

        private void CreateAddrLabel(int i)
        {
            _addrLabels[i] = new Label();
            _addrLabels[i].AutoSize = true;
            _addrLabels[i].Font = new Font("Microsoft Sans Serif", 8F);
            _addrLabels[i].Location = new Point(50, 11 + i * 18);
            _addrLabels[i].Size = new Size(34, 13);
            _addrLabels[i].TextAlign = ContentAlignment.MiddleRight;
        }

        private void CreateDecLabel(int i)
        {
            _decBoxes[i] = new HexTextbox();
            _decBoxes[i].AutoSize = true;
            _decBoxes[i].HexBase = false;
            _decBoxes[i].Font = new Font("Microsoft Sans Serif", 8F);
            _decBoxes[i].Location = new Point(3, 11 + i * 18);
            _decBoxes[i].Size = new Size(49, 13);
            _decBoxes[i].TextAlign = HorizontalAlignment.Center;
            _decBoxes[i].Text = i.ToString("D4");
            _decBoxes[i].Limit = ushort.MaxValue + 1;
            _decBoxes[i].Tag = true;
            _decBoxes[i].GotFocus += new EventHandler(TextBoxGotFocus);
            _decBoxes[i].LostFocus += new EventHandler(TextBoxLostFocus);
        }

        private void CreateHexLabel(int i)
        {
            _hexBoxes[i] = new HexTextbox();
            _hexBoxes[i].HexBase = true;
            _hexBoxes[i].AutoSize = true;
            _hexBoxes[i].Font = new Font("Microsoft Sans Serif", 8F);
            _hexBoxes[i].Location = new Point(3, 11 + i * 18);
            _hexBoxes[i].Size = new Size(49, 13);
            _hexBoxes[i].TextAlign = HorizontalAlignment.Center;
            _hexBoxes[i].Text = i.ToString("X4");
            _hexBoxes[i].Limit = ushort.MaxValue + 1;
            _hexBoxes[i].Tag = true;
            _hexBoxes[i].GotFocus += new EventHandler(TextBoxGotFocus);
            _hexBoxes[i].LostFocus += new EventHandler(TextBoxLostFocus);
        }

        private void CreateAdductionBox(int i)
        {
            _adductionBoxes[i] = new TextBox();
            _adductionBoxes[i].Size = new Size(49, 13);
            _adductionBoxes[i].Location = new Point(3, 11 + i * 18);
            _adductionBoxes[i].Tag = true;
        }

        private void CreateSetButton()
        {
            _currentButton = new Button();
            _currentButton.Size = new Size(30, 20 - 2);
            _currentButton.Text = "Уст.";
            _currentButton.TextAlign = ContentAlignment.MiddleLeft;
            _currentButton.MouseDown += new MouseEventHandler(_currentButtton_Click);
            this.Controls.Add(_currentButton);
            this.Controls.SetChildIndex(_currentButton, 0);
        }

        private ushort GetBitIndex(Point ledLocation)
        {
            int row;
            int column;

            if (0 == ledLocation.X % 12)
            {
                row = ledLocation.X / 12;
            }
            else
            {
                row = (ledLocation.X - 6) / 12;
            }

            column = (ledLocation.Y - 14) / 18;

            row = 16 - row;
            return (ushort)(column * 16 + row + _addrHex.Number);
        }

        private void OnHexSet()
        {
            ushort newValue = (ushort) (_hexBoxes[_currentBoxIndex].Number);
            _query.SaveOneWord(newValue, _currentBoxIndex);
        }

        private void OnDecSet()
        {
            ushort newValue = (ushort)(_decBoxes[_currentBoxIndex].Number);
            _query.SaveOneWord(newValue, _currentBoxIndex);
        }

        private void EnableValueBoxes(bool bEnable)
        {
            for (int i = 0; i < _wordCnt; i++)
            {
                _hexBoxes[i].Enabled = bEnable;
                _decBoxes[i].Enabled = bEnable;
                _adductionBoxes[i].Enabled = bEnable;
            }
        }

        private void ShowAdduction(int wordIndex)
        {
            if (wordIndex <= _wordCnt && wordIndex >= 0)
            {
                _maxHex.Text = _adduction[wordIndex].Max.ToString();
                _limitBox.Text = _adduction[wordIndex].Limit.ToString();
                _symbolsCombo.SelectedIndex = _adduction[wordIndex].Symbols;
            }
        }
        #endregion

        #region Event handlers
        private void _addrHex_TextChanged(object sender, EventArgs e)
        {
            _addrHex.Correct();
            //_device.Adress = (ushort)_addrHex.Number;
            _query.StartAddress = (ushort)_addrHex.Number;
            LoadWithCurrentSettings();
            SetAddrLabelText();
        }
        /// <summary>
        /// Клик по всплывающей кнопке
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _currentButtton_Click(object sender, EventArgs e)
        {
            _currentButton.Hide();
            if (null != _currentBox)
            {
                _currentBox.Correct();
                _currentBox.Tag = true;
                if (_currentBox.HexBase)
                {
                    OnHexSet();
                }
                else
                {
                    OnDecSet();
                }
            }

            _currentBox = null; ;
        }
        /// <summary>
        /// Поле ввода получило фокус
        /// </summary>
        /// <param name="sender">Выбранное поле ввода</param>
        /// <param name="e">Параметры ивента</param>
        private void TextBoxGotFocus(object sender, EventArgs e)
        {
            //Создаем кнопку, если ранее не была создана
            if (_currentButton == null)
            {
                CreateSetButton();
            }
            //Передвигаем кнопку к нужному полю ввода
            //CreateSetButton(sender);
            BindSetButton(sender);
            //Теперь поле ввода обновлять не требуется
            _currentBox = (sender as HexTextbox);
            if (_currentBox == null) return;
            _currentBox.Tag = false;
            _currentBoxIndex = _currentBox.HexBase
                ? (ushort)_hexBoxes.ToList().IndexOf(_currentBox)
                : (ushort)_decBoxes.ToList().IndexOf(_currentBox);
        }

        private void TextBoxLostFocus(object sender, EventArgs e)
        {
            if (_currentButton.Focused) return;
            _currentButton.Hide();
            (sender as Control).Tag = true;
        }

        private void BindSetButton(object sender)
        {
            GroupBox parent = (sender as Control).Parent as GroupBox;
            Point hexLocation = (sender as Control).Location;
            Size hexSize = (sender as Control).Size;
            Point buttonPoint =
                new Point(parent.Location.X + hexLocation.X + hexSize.Width, parent.Location.Y + hexLocation.Y);
            _currentButton.Location = buttonPoint;
            _currentButton.Show();
        }
        /// <summary>
        /// Клацнули мышой по лампочке
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Led_MouseClick(object sender, MouseEventArgs e)
        {
            //Только если битная адресация
            if (_bitfRadio.Checked)
            {
                LedControl led = (sender as PictureBox).Parent as LedControl;

                ushort bitInd = GetBitIndex(led.Location);

                switch (e.Button)
                {
                    case MouseButtons.Left:
                        //По левой кнопке инвертируем бит
                        if (LedState.Signaled == led.State)
                        {
                            _device.SetBit(_device.DeviceNumber, bitInd, true,
                                string.Format(SET_BIT_PATTERN, bitInd, _device.DeviceNumber), _device);
                        }
                        else if (LedState.NoSignaled == led.State)
                        {
                            _device.SetBit(_device.DeviceNumber, bitInd, false,
                                string.Format(SET_BIT_PATTERN, bitInd, _device.DeviceNumber), _device);
                        }
                        break;
                    //По правой показываем меню
                    case MouseButtons.Right:
                        _contextMenu.Items.Clear();
                        string set0 = "Сбросить     бит № - " + bitInd.ToString("X4") + "(" + bitInd + ")";
                        string set1 = "Установить бит № - " + bitInd.ToString("X4") + "(" + bitInd + ")";
                        _contextMenu.Items.Add(set0);
                        _contextMenu.Items.Add(set1);
                        _contextMenu.Items[0].Tag = _contextMenu.Items[1].Tag = bitInd;
                        _contextMenu.ItemClicked += new ToolStripItemClickedEventHandler(_contextMenu_ItemClicked);
                        _contextMenu.Show(MousePosition.X, MousePosition.Y);
                        break;
                }
            }
        }

        private void _symbolsCombo_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                _adduction[CurrentWordIndex].Symbols = _symbolsCombo.SelectedIndex;
            }

            catch (IndexOutOfRangeException)
            {
                return;
            }
        }

        /// <summary>
        /// Выбранный номер слова
        /// </summary>
        public int CurrentWordIndex
        {
            get
            {
                try
                {
                    int ret = Convert.ToInt32(_wordIndexCombo.Text);
                    if (ret < 1 || ret > 16)
                    {
                        _maxHex.Text = "";
                        _limitBox.Text = "";
                        _symbolsCombo.Text = "";
                    }
                    ret -= 1;

                    return ret;
                }
                catch (FormatException e)
                {
                    _maxHex.Text = "";
                    _limitBox.Text = "";
                    _symbolsCombo.Text = "";
                    throw e;
                }
            }
        }

        private void _contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ushort bitInd = (ushort)e.ClickedItem.Tag;
            try
            {
                if (e.ClickedItem == _contextMenu.Items[0])
                {
                    _device.SetBit(_device.DeviceNumber, bitInd, false,
                        string.Format(SET_BIT_PATTERN, bitInd, _device.DeviceNumber), _device);
                }
                if (e.ClickedItem == _contextMenu.Items[1])
                {
                    _device.SetBit(_device.DeviceNumber, bitInd, true,
                        string.Format(SET_BIT_PATTERN, bitInd, _device.DeviceNumber), _device);
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void _bitfRadio_CheckedChanged(object sender, EventArgs e)
        {
            _device.IsBit = _bitfRadio.Checked;
            EnableValueBoxes(_wordfRadio.Checked);
            SetAddrLabelText();
            if (_queryEnableCheckBox.Checked)
            {
                LoadWithCurrentSettings();
            }
        }

        private void _wordIndexCombo_TextUpdate(object sender, EventArgs e)
        {
            try
            {
                ShowAdduction(CurrentWordIndex);
            }
            catch (FormatException)
            {
                _maxHex.Text = "";
                _limitBox.Text = "";
                _symbolsCombo.Text = "";
            }
        }

        private void _wordIndexCombo_SelectedValueChanged(object sender, EventArgs e)
        {
            ShowAdduction(CurrentWordIndex);
        }

        private void _limitBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                _adduction[CurrentWordIndex].Limit = Convert.ToDouble(_limitBox.Text);
            }
            catch (FormatException)
            {
                return;
            }
            catch (IndexOutOfRangeException)
            {
                return;
            }
        }

        private void _maxHex_TextChanged(object sender, EventArgs e)
        {
            _maxHex.Correct();
            try
            {
                _adduction[CurrentWordIndex].Max = (ushort)_maxHex.Number;
            }
            catch (IndexOutOfRangeException)
            {
                return;
            }
        }

        private void _loopButton_Click(object sender, EventArgs e)
        {
            if (_loop)
            {
                this._loopButton.Text = ">";
                this.Width = 540;
                this._optionsGB.Location = new Point(560, 1);
                _loop = false;
            }
            else
            {
                this._loopButton.Text = "<";
                this.Width = 820;
                this._optionsGB.Location = new Point(525, 1);
                _loop = true;
            }
        }

        private void MLK_Querys_FormClosing(object sender, FormClosingEventArgs e)
        {
            _query.RemoveStructQuerys();
        }

        private void _countOfWord_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (_countOfWord.SelectedIndex == -1)
                {
                    this.Height = 200;
                }
                else
                {
                    int i = _countOfWord.SelectedIndex;
                    this.Height = 200 + i * 18;
                }
                _wordCnt = _countOfWord.SelectedIndex + 8;
                InitControls();
                SetAddrLabelText();
                for (int i = 0; i < _adduction.Length; i++)
                {
                    _adduction[i].Limit = double.Parse(_limitBox.Text);
                    _adduction[i].Max = ushort.Parse(_maxHex.Text);
                    _adduction[i].Symbols = _symbolsCombo.SelectedIndex;
                }
                LoadWithCurrentSettings();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, @"Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void _queryEnableCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (_queryEnableCheckBox.Checked)
            {
                LoadWithCurrentSettings();
            }
            else
            {
                _query.RemoveStructQuerys();
            }
        }
        
        private void OnDeviceCaptionChanged()
        {
            _query.Caption = _device.Caption;
        }
        
        private void DeviceOnDeviceNumberChanged(object sender, byte oldNumber, byte newNumber)
        {
            _query.DeviceNum = newNumber;
        }
        #endregion



        #region IFormView Members

        public Type FormDevice
        {
            get { return _device.GetType(); }
        }

        public bool Multishow { get; private set; }

        #endregion

        #region INodeView Members
        [Browsable(false)]
        public Type ClassType
        {
            get { return typeof(MlkQuerys); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return AssemblyResources.Resources.Calibrate; }
        }

        public string NodeName
        {
            get { return "Обмены"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

    }
}
