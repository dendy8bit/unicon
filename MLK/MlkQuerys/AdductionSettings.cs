﻿namespace BEMN.MLK.MlkQuerys
{
    public struct AdductionSettings
    {
        public AdductionSettings(double limit, ushort max, int symbols)
        {
            this._limit = limit;
            this._max = max;
            this._symbols = symbols;
        }

        private double _limit;
        private ushort _max;
        private int _symbols;

        public double Limit
        {
            get { return _limit; }
            set { _limit = value; }
        }

        public ushort Max
        {
            get { return _max; }
            set { _max = value; }
        }

        public int Symbols
        {
            get { return _symbols; }
            set { _symbols = value; }
        }
    };
}
