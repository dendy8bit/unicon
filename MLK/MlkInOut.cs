﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms;
using BEMN.Framework.Properties;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MLK.HelpClasses;
using BEMN.MLK.Structures;
using BEMN.MLK.Structures.IntermediateStructures;
using Timer = System.Windows.Forms.Timer;

namespace BEMN.MLK
{
    public partial class MlkInOut : Form, IFormView
    {
        #region Поля
        private readonly MLK _device;
        private LedControl[] _discretLeds;
        private LedControl[] _relayLeds;
        private LedControl[] _diodLeds;
        private Label[] _discretsLabels;
        private Label[] _relayLabels;
        private Label[] _diodsLabel;
        private MemoryEntity<UIORam> _uioRam;
        private MemoryEntity<UIORamV2_0> _uioRamV2;
        private MemoryEntity<SomeStruct> _signature; 
        private readonly MemoryEntity<Clock> _clock;
        private readonly MemoryEntity<SysErr> _sysErr;
        private readonly MemoryEntity<QueryCount> _countOfQuery;
        private readonly MemoryEntity<GisterezisExtConfig> _endOfScale;
        private readonly MemoryEntity<VersionDevice> _versionMkl;
        
        private bool _start;
        private bool _amplBit;

        private byte _countOfDiscrets;
        private byte _countOfRelays;
        private byte _countOfDiods;
        private double _limit;
        private int _countOfRequest;
        
        private readonly Timer _timer = new Timer();
        #endregion

        #region Константы
        private const string CANAL_ERR = "Неисправен канал";
        private const string LOWER_LIMIT = "Нижний предел";
        private const string UPPER_LIMIT = "Верхний предел";
        private const string ERROR_WRITING_DATE = "Невозможно записать дату/врумя";
        private const string ERROR_WRITING = "Ошибка записи";
        private const string CONNECTION_OK = "Ошибок соединения нет";
        #endregion

        #region Внутренняя структурка
        public struct BitInfo
        {
            public ushort BitStartAddress;
            public ushort BitIndex;
            public bool BitValue;
        }
        #endregion

        #region Конструкторы
        public MlkInOut()
        {
            this.InitializeComponent();
        }

        public MlkInOut(MLK device)
        {
            this.InitializeComponent();
            this._device = device;
            this._device.ReadNewCount += this.ReadNewValueCount;
            this._device.ReadNewLimit += this.ReadNewLimit;
            this._device.CheckBit += bit => this._amplBit = bit;

            this._countOfQuery = new MemoryEntity<QueryCount>("Количество запросов статистики", this._device, 0x1018);
            this._countOfQuery.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this._countOfRequest = this._countOfQuery.Value.CountOfRequest;
                this._errorsDG.Rows.Clear();
            });
            this._countOfQuery.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () => this._countOfRequest = 0);

            this._endOfScale = new MemoryEntity<GisterezisExtConfig>("Предел шкалы", this._device, 0x1503);
            this._endOfScale.AllReadOk += HandlerHelper.CreateReadArrayHandler(this,
                () => this._limit = this._endOfScale.Value.EndOfScale);
            this._endOfScale.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () => this._limit = 300);

            this._clock = this._device.Clock;
            this._clock.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ClockAstrReadComplete);
            this._clock.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.ClockAstrReadFail);

            this._sysErr = this._device.SysErr;
            this._sysErr.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.SysErrorReadCompleat);
            this._sysErr.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.SysErrorReadFail);

            this._timer.Interval = 100;
            this._timer.Tick += this._timer_Tick;
            this._start = true;

            this._versionMkl = new MemoryEntity<VersionDevice>("Версия прошивки, загрузчика и устройства", this._device, 0x1F00);
            this._versionMkl.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.VersionReadOk);
            this._versionMkl.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.VersionReadFail);
        }

        private void InitControls()
        {
            this.InitGroupBox(ref this._discretsGroup, this._countOfDiscrets, 65 - 6, 0, "Дискреты");
            this.InitGroupBox(ref this._relayGroup, this._countOfRelays, this._discretsGroup.Top, this._discretsGroup.Height, "Реле");

            this.InitLeds(ref this._discretLeds, this._discretsLabels, this._discretsGroup, this._countOfDiscrets, "Д{0}");
            this.InitLeds(ref this._relayLeds, this._relayLabels, this._relayGroup, this._countOfRelays, "Р{0}");
            foreach (var led in this._relayLeds)
            {
                led.LedClicked += this.RelayLedMouseClick;
            }
            if (this._countOfDiods == 0) return;
            this.InitGroupBox(ref this._diodGroup, this._countOfDiods, this._relayGroup.Top, this._relayGroup.Height, "Светодиоды");
            this.InitLeds(ref this._diodLeds, this._diodsLabel, this._diodGroup, this._countOfDiods, "C{0}");
            foreach (var led in this._diodLeds)
            {
                led.LedClicked += this.DiodLedMouseClick;
            }
        }
        private void InitGroupBox(ref GroupBox box, int count, int preY, int preHeigth, string title)
        {
            box = new GroupBox();
            box.Height = count > 16 ? 85 : 55;
            box.Width = count > 16 ? 405 : ((2 * count - 1) * 13 + 12);
            if (box.Width < 83)
                box.Width = 83;
            box.Location = new Point(6, preY + preHeigth + 6);
            box.Text = title;
            this._inOutTabPage.Controls.Add(box);
            this._inOutTabPage.Update();
        }
        private void InitLeds(ref LedControl[] leds, Label[] labels, GroupBox group, int count, string s)
        {
            leds = new LedControl[count];
            labels = new Label[count];
            for (int i = 0; i < leds.Length; i++)
            {
                if (i < 16)
                {
                    leds[i] = new LedControl {Location = new Point(6 + i*25, 19)};
                    labels[i] = new Label {Location = new Point(2 + i*25, 35), Width = i < 9 ? 20 : 26, Height = 15};
                }
                else
                {
                    leds[i] = new LedControl {Location = new Point(6 + (i - 16)*25, 51)};
                    labels[i] = new Label {Location = new Point(2 + (i - 16)*25, 67), Width = 26};
                }
                labels[i].Text = string.Format(s, i + 1);
                group.Controls.Add(leds[i]);
                group.Controls.Add(labels[i]);
            }
        }

        #endregion

        #region Аналоги, дискреты, реле и светодиоды
        #region Старые девайсы
        private void UioReadComplete()
        {
            #region Analog

                switch (this._uioRam.Value.Analog)
                {
                    case (ushort) 0xffff:
                        this._analogU.Text = CANAL_ERR;
                        break;
                    case (ushort) 0x8000:
                        this._analogU.Text = LOWER_LIMIT;
                        break;
                    case (ushort) 0x7fff:
                        this._analogU.Text = UPPER_LIMIT;
                        break;
                    default:
                        this._analogU.Text = Math.Round(this._limit/0x7ffe*(double) this._uioRam.Value.Analog, 5).ToString();
                        break;
                }

                #endregion
            #region Discrets

                LedManager.SetLeds(this._discretLeds, this._uioRam.Value.Discrets);

                #endregion
            #region Relay

                LedManager.SetLeds(this._relayLeds, this._uioRam.Value.Relay);

                #endregion
        }

        private void UioReadFail()
        {
            this._analogU.Text = string.Empty;

            LedManager.TurnOffLeds(this._discretLeds);
            LedManager.TurnOffLeds(this._relayLeds);
        }

        private byte[] GetDefaultCounts()
        {
            List<byte> ret = new List<byte>();
            switch (this._versionMkl.Value.AppPodtip.ToUpper())
            {
                case "SA":
                    ret.Add(8);
                    ret.Add(7);
                    ret.Add(0);
                    break;
                case "SB":
                    ret.Add(8);
                    ret.Add(7);
                    ret.Add(0);
                    break;
                case "SC":
                    ret.Add(11);
                    ret.Add(5);
                    ret.Add(0);
                    break;
                case "SD":
                    ret.Add(12);
                    ret.Add(3);
                    ret.Add(0);
                    break;
                default:
                    MessageBox.Show("Ошибка");
                    break;
            }
            return ret.ToArray();
        }
        #endregion
        #region Новые девайсы

        private void SignatureReadOk()
        {
            byte[] buf = Common.TOBYTES(this._signature.Values, true);
            if(!CRC16.VerifyRespCrc(buf))
            {
                MessageBox.Show("Значение CRC не совпадает", "Ошибка CRC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            Common.SwapArrayItems(ref buf);
            if (!this.VerifyConfigVersion(buf))
            {
                MessageBox.Show("В устройстве записана конфигурация версии не 1.0", "Ошибка версии конфигурации",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            byte sm1 = buf[16];
            byte sm2 = buf[17];
            byte sm3 = buf[18];
            this._countOfDiscrets = buf[sm1];
            this._countOfRelays = buf[sm2];
            this._countOfDiods = buf[sm3];
            this.InitControls();
            this._uioRamV2.LoadStructCycle();
        }

        private void SignatureReadFail()
        {
            MessageBox.Show("Не удалось прочитать конфигурацию дискрет, реле и светодиодов", "Ошибка чтения сигнатуры",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void UioRamV2ReadOk()
        {
            #region Analog

            if (this._amplBit)
            {
                this._analogU.Text = this._uioRamV2.Value.Analog.ToString();
            }
            else
            {
                switch (this._uioRamV2.Value.Analog)
                {
                    case (ushort) 0xffff:
                        this._analogU.Text = CANAL_ERR;
                        break;
                    case (ushort) 0x8000:
                        this._analogU.Text = LOWER_LIMIT;
                        break;
                    case (ushort) 0x7fff:
                        this._analogU.Text = UPPER_LIMIT;
                        break;
                    default:
                        this._analogU.Text = Math.Round(this._limit/0x7ffe*(double) this._uioRamV2.Value.Analog, 5).ToString();
                        break;
                }
            }

            #endregion

            BitArray setArray;
            #region Discret
            setArray = this.GetBitsArray(this._uioRamV2.Value.Discrets, this._countOfDiscrets);
            LedManager.SetLeds(this._discretLeds, setArray);
            #endregion
            #region Relay
            setArray = this.GetBitsArray(this._uioRamV2.Value.Relay, this._countOfRelays);
            LedManager.SetLeds(this._relayLeds, setArray);
            #endregion
            #region Diod
            setArray = this.GetBitsArray(this._uioRamV2.Value.Diods, this._countOfDiods);
            LedManager.SetLeds(this._diodLeds, setArray);
            #endregion
        }
        private void UioRamV2ReadFail()
        {
            this._analogU.Text = string.Empty;

            LedManager.TurnOffLeds(this._discretLeds);
            LedManager.TurnOffLeds(this._relayLeds);
            LedManager.TurnOffLeds(this._diodLeds);
        }
        #endregion

        #endregion

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MLK); }
        }

        public bool Multishow { get; private set; }

        #endregion

        #region INodeView Members

        public Type ClassType
        {
            get { return typeof(MlkInOut); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.oscilloscope.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Ввод - вывод"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        #region Астраномические дата/время

        public void ClockAstrReadComplete()
        {
            this._astrDateClockTB.TextMaskFormat = MaskFormat.IncludeLiterals;
            this._astrDateClockTB.Text = this.PrepareGridParam(this._clock.Value.Day.ToString(), 2, 0) + this.PrepareGridParam(this._clock.Value.Month.ToString(), 2, 0) + this.PrepareGridParam(this._clock.Value.Year.ToString(), 2, 0);
            this._astrTimeClockTB.TextMaskFormat = MaskFormat.IncludeLiterals;
            this._astrTimeClockTB.Text = this.PrepareGridParam(this._clock.Value.Hour.ToString(), 2, 0) + this.PrepareGridParam(this._clock.Value.Minutes.ToString(), 2, 0) + this.PrepareGridParam(this._clock.Value.Seconds.ToString(), 2, 0);
        }

        public void ClockAstrReadFail()
        {
            this._astrDateClockTB.TextMaskFormat = MaskFormat.IncludeLiterals;
            this._astrDateClockTB.Text = "00" + "00" + "00";
            this._astrTimeClockTB.TextMaskFormat = MaskFormat.IncludeLiterals;
            this._astrTimeClockTB.Text = "00" + "00" + "00";
        }

        #endregion

        #region Системные ошибки
        private void SysErrorReadCompleat() 
        {
            if (this._errorsDG.Rows.Count == 0)
            {
                this._errorsDG.Rows.Clear();
                for (int i = 0; i < this._countOfRequest; i++)
                {
                    byte buf = this._sysErr.Value.StatisticsRequest[i].Status;
                    string stat = string.Empty;
                    if (buf == 0)
                    {
                        stat = CONNECTION_OK;
                    }
                    else
                    {
                        BitArray bitStat = new BitArray(new byte[] { buf });
                        for (int j = 0; j < bitStat.Length; j++)
                        {
                            if (bitStat[j])
                                stat += Strings.SystemStatusErrors[j] + "\n";
                        }
                    }

                    this._errorsDG.Rows.Add((this._errorsDG.Rows.Count + 1).ToString(), this._sysErr.Value.StatisticsRequest[i].AcceptMessage.ToString(), this._sysErr.Value.StatisticsRequest[i].SendMessage.ToString(), this._sysErr.Value.StatisticsRequest[i].CommunicationQuality.ToString(),
                        stat);

                    this.SetCellColor(this._sysErr.Value.StatisticsRequest[i].CommunicationQuality, i);

                    this._errorsDG.Rows[i].Height = 20;
                }
            }
            else
            {
                for (int i = 0; i < this._countOfRequest; i++)
                {
                    byte buf = this._sysErr.Value.StatisticsRequest[i].Status;
                    string stat = string.Empty;
                    if (buf == 0)
                    {
                        stat = CONNECTION_OK;
                    }
                    else
                    {
                        BitArray bitStat = new BitArray(new byte[] { buf });
                        for (int j = 0; j < bitStat.Length; j++)
                        {
                            if (bitStat[j])
                                stat += Strings.SystemStatusErrors[j] + "/n";
                        }
                    }
                    this._errorsDG.Rows[i].Cells[1].Value = this._sysErr.Value.StatisticsRequest[i].AcceptMessage.ToString();
                    this._errorsDG.Rows[i].Cells[2].Value = this._sysErr.Value.StatisticsRequest[i].SendMessage.ToString();
                    this._errorsDG.Rows[i].Cells[3].Value = this._sysErr.Value.StatisticsRequest[i].CommunicationQuality.ToString();
                    this._errorsDG.Rows[i].Cells[4].Value = stat;

                    this.SetCellColor(this._sysErr.Value.StatisticsRequest[i].CommunicationQuality, i);
                }
            }
        }

        private void SysErrorReadFail()
        {
            this._errorsDG.Rows.Clear();
        }
        #endregion

        #region Дополнительные функции

        private void VersionReadOk()
        {
            double vers = Convert.ToDouble(this._versionMkl.Value.AppVersion, CultureInfo.InvariantCulture);
            if (vers < 2.0 && vers >= 1.0)
            {
                this._uioRam = this._device.UioRam;
                this._uioRam.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.UioReadComplete);
                this._uioRam.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.UioReadFail);
                byte[] counts = this.GetDefaultCounts();
                this._countOfDiscrets = counts[0];
                this._countOfRelays = counts[1];
                this._countOfDiods = counts[2];
                this.InitControls();
                this._uioRam.LoadStructCycle();
            }
            else if (vers >= 2.0)
            {
                this._uioRamV2 = this._device.UioRamV2;
                this._uioRamV2.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.UioRamV2ReadOk);
                this._uioRamV2.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.UioRamV2ReadFail);
                this._signature = this._device.Signature;
                this._signature.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.SignatureReadOk);
                this._signature.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.SignatureReadFail);
                this._signature.LoadStruct();
            }
            else
            {
                MessageBox.Show("Данная версия не поддерживается");
            }

        }

        private void VersionReadFail()
        {
            MessageBox.Show("Невозможно прочитать версию устройства", "Ошибка чтения версии", MessageBoxButtons.OK,
                MessageBoxIcon.Error);
        }
        private void RemoveAllQuerys()
        {
            if(this._uioRam!=null) this._uioRam.RemoveStructQueries();
            if(this._uioRamV2!=null) this._uioRamV2.RemoveStructQueries();

            this._clock.RemoveStructQueries();
            this._sysErr.RemoveStructQueries();
        }

        private string PrepareGridParam(string param, int count, int index)
        {
            if (param.Length < count)
            {
                param = "0" + param;
            }
            if (param.Length > count)
            {
                string temp = string.Empty;
                for (int i = index; i < param.Length; i++)
                {
                    temp += param[i];
                }
                param = temp;
            }
            return param;
        }

        private void SetCellColor(ushort communicationQuality, int i)
        {
            if (communicationQuality < 25)
            {
                this._errorsDG.Rows[i].Cells[3].Style.BackColor = Color.Red;
            }
            else if (communicationQuality >= 25 && communicationQuality < 50)
            {
                this._errorsDG.Rows[i].Cells[3].Style.BackColor = Color.LightCoral;
            }
            else if (communicationQuality >= 50 && communicationQuality < 75)
            {
                this._errorsDG.Rows[i].Cells[3].Style.BackColor = Color.LightGreen;
            }
            else if (communicationQuality >= 75 && communicationQuality <= 100)
            {
                this._errorsDG.Rows[i].Cells[3].Style.BackColor = Color.Green;
            }
        }
        private void SetBitState(LedControl currentLed, LedControl[] leds, ushort startAddr, BitArray currentState, MouseButtons click, bool relayOrDiod)
        {
            try
            {
                List<LedControl> ledList = new List<LedControl>();
                ledList.AddRange(leds);
                int ledInd = ledList.IndexOf(currentLed);
                bool value = currentLed.State == LedState.Signaled;
                switch (click)
                {
                    //По левой кнопке инвертируем бит
                    case MouseButtons.Left:
                        this._device.SetBit(this._device.DeviceNumber, (ushort)(startAddr + ledInd), value,
                                "SetRelayValue" + this._device.DeviceNumber, this._device);
                        break;
                    //По правой показываем меню
                    case MouseButtons.Right:
                        this._contextMenu.Items.Clear();

                        BitInfo info;
                        info.BitIndex = (ushort)ledInd;
                        info.BitValue = value;
                        info.BitStartAddress = startAddr;
                        string contextItem;
                        if (relayOrDiod)
                        {
                             contextItem = string.Format(currentState[ledInd]
                                ? "Выключить светодиод № - {0}"
                                : "Включить светодиод № - {0}", ledInd + 1);
                        }
                        else
                        {
                            contextItem = string.Format(currentState[ledInd]
                                ? "Выключить реле № - {0}"
                                : "Включить реле № - {0}", ledInd + 1);
                        }
                        this._contextMenu.Items.Add(contextItem);

                        this._contextMenu.Items[0].Tag = info;
                        this._contextMenu.ItemClicked += this._contextMenu_ItemClicked;
                        this._contextMenu.Show(MousePosition.X, MousePosition.Y);
                        break;
                }
            }
            catch
            {
                MessageBox.Show("Невозможно изменить состояние реле", "Ошибка");
            }
        }

        /// <summary>
        /// Устанавливает новое значение счетчика запросов
        /// </summary>
        private void ReadNewValueCount()
        {
            this._countOfQuery.LoadStruct();
        }
        /// <summary>
        /// Читает новый предел шкалы
        /// </summary>
        private void ReadNewLimit()
        {
            this._endOfScale.LoadStruct();
        }

        private bool VerifyConfigVersion(byte[] buf)
        {
            System.Text.Decoder dec = System.Text.Encoding.ASCII.GetDecoder();
            char[] charBuf = new char[16];
            int byteUsed = 0;
            int charUsed = 0;
            bool complete = false;
            dec.Convert(buf, 0, 16, charBuf, 0, 16, true, out byteUsed, out charUsed, out complete);
            string configStr = new string(charBuf, 0, 13);
            string vers = new string(charBuf,13,3);
            if ((configStr.ToUpper() == "CONFIGVERSION") && (vers == "1.0"))
            {
                complete = true;
            }
            else
            {
                complete = false;
            }
            return complete;
        }

        private BitArray GetBitsArray(BitArray array, int length)
        {
            BitArray ret = new BitArray(length);
            for (int i = 0; i < ret.Length; i++)
            {
                ret[i] = array[i];
            }
            return ret;
        }
        #endregion

        #region Обработчики событий

        private void MLK_InOut_Load(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Normal;
            MdiParent.Refresh();
            this._versionMkl.LoadStruct();;
            this._countOfQuery.LoadStruct();
            this._endOfScale.LoadStruct();
            this._clock.LoadStructCycle();
            this._sysErr.LoadStructCycle();        
        }

        private void _astrStopCB_CheckedChanged(object sender, EventArgs e)
        {
            if (this._astrStopCB.Checked)
            {
                this._clock.RemoveStructQueries();
                this._dateTimeNowButt.Enabled = true;
                this._writeDateTimeButt.Enabled = true;
            }
            else
            {
                this._clock.LoadStructCycle();
                this._dateTimeNowButt.Enabled = false;
                this._writeDateTimeButt.Enabled = false;
                if (!this._start)
                {
                    this._start = true;
                    this._timer.Stop();
                }
            }
        }

        private void MLK_InOut_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.RemoveAllQuerys();
        }
        
        private void RelayLedMouseClick(object sender, MouseEventArgs e)
        {
            LedControl led = sender as LedControl;
            double vers = Convert.ToDouble(this._versionMkl.Value.AppVersion, CultureInfo.InvariantCulture);
            if (vers >= 2.0)
            {
                this.SetBitState(led, this._relayLeds, 0x8030, this._uioRamV2.Value.Relay, e.Button, false);
            }
            else
            {
                this.SetBitState(led, this._relayLeds, 0x8020, this._uioRam.Value.Relay, e.Button, false);
            }
        }

        private void DiodLedMouseClick(object sender, MouseEventArgs e)
        {
            LedControl led = sender as LedControl;
            this.SetBitState(led, this._diodLeds, 0x8050, this._uioRamV2.Value.Diods, e.Button, true);
        }

        private void _contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            BitInfo info = (BitInfo)e.ClickedItem.Tag;

            this._device.SetBit(this._device.DeviceNumber, (ushort)(info.BitStartAddress + info.BitIndex), info.BitValue,
                "SetRelayValue" + this._device.DeviceNumber, this._device);
        }

        private void _dateTimeNowButt_Click(object sender, EventArgs e)
        {
            if (this._start)
            {
                this._start = false;
                this._timer.Start();
            }
            else
            {
                this._start = true;
                this._timer.Stop();
            }
        }

        private void _writeDateTimeButt_Click(object sender, EventArgs e)
        {
            try
            {
                List<string> dateTime = new List<string>();
                dateTime.AddRange(this._astrDateClockTB.Text.Split('.'));
                dateTime.AddRange(this._astrTimeClockTB.Text.Split(':', '.', ','));
                Clock clock = new Clock();
                clock.InitStruct(new byte[this._clock.Values.Length * 2]);
                clock.Day = Convert.ToUInt16(dateTime[0]);
                clock.Month = Convert.ToUInt16(dateTime[1]);
                clock.Year = Convert.ToUInt16(dateTime[2]);
                clock.Hour = Convert.ToUInt16(dateTime[3]);
                clock.Minutes = Convert.ToUInt16(dateTime[4]);
                clock.Seconds = Convert.ToUInt16(dateTime[5]);
                this._clock.Value = clock;
                this._clock.SaveStruct();
                this._astrStopCB.Checked = true;
            }
            catch
            {
                MessageBox.Show(ERROR_WRITING_DATE, ERROR_WRITING, MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private void _timer_Tick(object sender, EventArgs e)
        {
            this._astrDateClockTB.TextMaskFormat = MaskFormat.IncludeLiterals;
            this._astrDateClockTB.Text = this.PrepareGridParam(DateTime.Now.Day.ToString(), 2, 0) + this.PrepareGridParam(DateTime.Now.Month.ToString(), 2, 0) + this.PrepareGridParam(DateTime.Now.Year.ToString(), 2, 2);
            this._astrTimeClockTB.TextMaskFormat = MaskFormat.IncludeLiterals;
            this._astrTimeClockTB.Text = this.PrepareGridParam(DateTime.Now.Hour.ToString(), 2, 0) + this.PrepareGridParam(DateTime.Now.Minute.ToString(), 2, 0) + this.PrepareGridParam(DateTime.Now.Second.ToString(), 2, 0);
        }
        #endregion
    }
}