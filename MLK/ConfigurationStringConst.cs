﻿namespace BEMN.MLK
{
    public static class ConfigurationStringConst
    {
        #region Константы

        public const string NAN = "NaN";
        public const string BAD_DATA = "проверьте правильность введенных данных";
        public const string ERROR_WRITE = "Ошибка записи";
        public const string ERROR_READ_RS485 = "Не удалось загрузить параметры RS-485";
        public const string READ_RS485_OK = "Конфигурация RS-485 загружена успешно";
        public const string ERROR_READ_USB = "Не удалось загрузить параметры USB";
        public const string READ_USB_OK = "Конфигурация USB загружена успешно";
        public const string ERROR_READ_REQUESTS = "Не удалось загрузить запросы к модулям";
        public const string READ_REQUESTS_OK = "Конфигурация запросов заружена успешно";
        public const string ERROR_READ_ANALOG_CONFIG = "Не удалось загрузить конфигурацию аналогового канала";
        public const string READ_ANALOG_CONFIG_OK = "конфигурация аналогового канала загружены успешно";
        public const string ERROR_READ_GISTER_CONFIG = "Не удалось загрузить конфигурацию гистерезиса";
        public const string READ_GISTER_CONFIG_OK = "конфигурация гистерезиса загружены успешно";
        public const string ERROR_READ_CLOCKCONF = "Не удалось загрузить конфигурацию часов";
        public const string READ_CLOCKCONF_OK = "Конфигурация часов загружена успешно";
        public const string READ_VERSION_OK = "Версия загружена успешно";
        public const string READ_VERSION_FAIL = "Не удалось прочитать версию";
        public const string ERROR_WRITE_REQUESTS = "Проверьте введенные данные для запросов";
        public const string WRITE_USB_OK = "Конфигурация USB записана успешно";
        public const string WRITE_USB_FAIL = "Не удалось записать конфигурацию USB";
        public const string WRITE_RS485_OK = "Конфигурация RS-485 записана успешно";
        public const string WRITE_RS485_FAIL = "Не удалось записать конфигурацию RS-485";
        public const string WRITE_REQUEST_OK = "Конфигурация запросов записана успешно";
        public const string WRITE_REQUEST_FAIL = "Не удалось записать конфигурацию запросов";
        public const string WRITE_ANALOG_CONFIG_OK = "Конфигурация аналогового канала записана успешно";
        public const string WRITE_ANALOG_CONFIG_FAIL = "Не удалось записать конфигурацию аналогового канала";
        public const string WRITE_GISTER_CONFIG_OK = "Конфигурация гистерезиса записана успешно";
        public const string WRITE_GISTER_CONFIG_FAIL = "Не удалось записать конфигурацию гистерезиса";
        public const string WRITE_CLOCKCONF_OK = "Конфигурация часов записана успешно";
        public const string WRITE_CLOCKCONF_FAIL = "Не удалось записать конфигурацию часов";
        public const string READ_ANALOG_FAIL = "Не удалось прочитать аналоговый канал";
        public const string CANAL_ERR = "Неиспр. канал";
        public const string LOWER_LIMIT = "Нижний предел";
        public const string UPPER_LIMIT = "Верхний предел";
        public const string STEP = "Шаг {0}";
        public const string DefaultLimit = "100";
        public const string DefaultPercent = "0,0";

        //public const string Info1 =
        //    "   Для проведения процедуры калибровки выберите номер аналогового измерительного органа (АОИ) и нажмите кнопку \"Продолжить\".";

        public const string Info1 =
            "   Для получения \"коэффициента В\" убедитесь, что на калибруемый аналоговый вход нет физических подключений и нажмите кнопку \"Продолжить\".";

        public const string Info2 = "   Для получения \"коэффициента А\":\n" +
                                     "   а) установите величину верхнего предела шкалы измерения (поле \"Предел шкалы\").\n" +
                                     "   б) установите величину эталонного сигнала, который будет подан на аналоговый вход (поле \"Сигнал на входе\").\n" +
                                     "   в) создайте физическое подключение на аналоговый вход с источником эталонного сигнала и перейдите к следующему шагу.";

        public const string Info3 = "При необходимости коэффициенты можно подправить и сохранить в устройство";

        public const string GistInfo1 =
            "Подайте на калибруемый аналоговый вход постоянное напряжение, величина которого соответствует величине верхнего предела шкалы (по постоянному напряжению), и нажмите кнопку \"Продолжить\"";

        public const string GistInfo2 = "Установите нижний предел гистерезиса. При необходимости, отредактируйте верхний предел и нажмите кнопку \"Продолжить\"";

        public const string B_SAVED = "Коэффициент В сохранен";
        public const string B_SAVING = "Расчет и запись коэффициента В";
        public const string A_MEASSURING = "Идет расчет коеффициента А";
        public const string BAD_LIMIT_PERCENT = "Неверно введен лимит или процент";
        public const string NO_POWER_SUPPLY = "Нет подключения с источником сигнала!";
        public const string COMPLETE = "Калибровка завершена успешно";
        public const string CONTINUE = "Продолжить";
        public const string BREAK = "Завершить";
        public const string MEASURING_HIGH_LIMIT = "Нахождение верхнего предела";
        public const string INVALID_LOW_LIMIT = "Неверное значение нижнего предела";
        public const string CALIBROVKA_GISTEREZIS_SUCCSESS = "Калибровка гистерезиcа завершена";
        public const string WRITING_GISTER_ERROR = "Произошла ошибка записи";
        public const string HIGH_LIMIT_MEASURING_COMPLETE = "Нахождение верхнего предела завершено";

        #endregion
    }
}
