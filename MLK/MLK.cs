﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.InteropServices;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms.Queries;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MLK.Structures;
using BEMN.MLK.Structures.IntermediateStructures;
using BEMN.Framework;

namespace BEMN.MLK
{
    public class MLK : Device, IDeviceView, IDeviceVersion
    {
        #region Private fields
        private MemoryEntity<UIORam> _uioRam;
        private MemoryEntity<UIORamV2_0> _uioRamV2;
        private MemoryEntity<Clock> _clock;
        private MemoryEntity<SysErr> _sysErr;
        private MemoryEntity<VersionDevice> _version;
        private MemoryEntity<MemConfigUSB> _configUSB;
        private MemoryEntity<MemConfigRS485> _configRS485;
        private MemoryEntity<QueryCount> _queryCount;
        private MemoryEntity<MemConfigRequest> _configRequest;
        private MemoryEntity<ClockConfig> _clockConfig;
        private MemoryEntity<NewClockConfig> _newClockConfig; 
        private MemoryEntity<AnalogExtConfig> _calibrovkaAnalog;
        private MemoryEntity<GisterezisExtConfig> _calibrovkaGister;
        private MemoryEntity<SomeStruct> _outputSignal; 
        private MemoryEntity<SomeStruct> _logicProg;
        private MemoryEntity<SomeStruct> _logicProgConst;
        private MemoryEntity<RegLogicProg> _regLogicProg;
        private MemoryEntity<SomeStruct> _saveSCLP;
        private MemoryEntity<SomeStruct> _logicSignature;
        private MemoryEntity<JournalHeader> _sysJournalHeader;
        private MemoryEntity<JournalHeader> _logicJournalHeader;
        private MemoryEntity<SomeStruct> _signature; 
        #endregion

        #region delegates and events
        public delegate void ReadCountOfRequest();
        public event ReadCountOfRequest ReadNewCount;

        public delegate void ReadEndOfScale();
        public event ReadEndOfScale ReadNewLimit;

        public delegate void CheckBitAmpl(bool bit);
        public event CheckBitAmpl CheckBit;
        #endregion

        #region Constructors
        public MLK()
        {
            HaveVersion = true;
        }

        public MLK(Modbus mb) : this(mb, false){}

        public MLK(Modbus mb, bool log)
        {
            HaveVersion = true;
            MB = mb;
            MB.Logging = log;
        }

        [XmlIgnore]
        [TypeConverter(typeof(RussianExpandableObjectConverter))]
        public override Modbus MB
        {
            get
            {
                return mb;
            }
            set
            {
                mb = value;
                if (null == mb) return;
                mb.CompleteExchange += mb_CompleteExchange;
                this.InitStructures();
            }
        }

        private void InitStructures()
        {
            _uioRam = new MemoryEntity<UIORam>("RAM ввода-вывода", this, 0x0800);
            _uioRamV2 = new MemoryEntity<UIORamV2_0>("RAM ввода-вывода новых МЛК", this, 0x0800);
            _clock = new MemoryEntity<Clock>("Часы реального времени", this, 0x0810);
            _sysErr = new MemoryEntity<SysErr>("Системные ошибки", this, 0x0848);

            _configUSB = new MemoryEntity<MemConfigUSB>("USB", this, 0x1000);
            _configRS485 = new MemoryEntity<MemConfigRS485>("RS-485", this, 0x1008);
            _queryCount = new MemoryEntity<QueryCount>("Количество запросов", this, 0x1018);
            _configRequest = new MemoryEntity<MemConfigRequest>("Запросы к модулям", this, 0x101C);
            _calibrovkaAnalog = new MemoryEntity<AnalogExtConfig>("Конфигурация аналогового канала", this, 0x1500);
            _calibrovkaGister = new MemoryEntity<GisterezisExtConfig>("Калибровка гистерезиса", this, 0x1503);
            _outputSignal = new MemoryEntity<SomeStruct>("Выводимое значение", this, 0x1507);
            _clockConfig = new MemoryEntity<ClockConfig>("Конфигурация часов", this, 0x1520);
            _newClockConfig = new MemoryEntity<NewClockConfig>("Конфигурация часов", this, 0x1010);
            _version = new MemoryEntity<VersionDevice>("Версия прошивки, устройства и загрузчика", this, 0x1F00);

            _logicProgConst = new MemoryEntity<SomeStruct>("Логическая программа (константы)", this, 0x2000);
            _logicProg = new MemoryEntity<SomeStruct>("Логическая программа", this, 0x8000);
            _regLogicProg = new MemoryEntity<RegLogicProg>("Регистры логической программы", this, 0x0820);
            _saveSCLP = new MemoryEntity<SomeStruct>("Сохранение значения в регистр SCLP",this, 0x0840);
            _logicSignature = new MemoryEntity<SomeStruct>("Сигнатура логической программы", this, 0x1600);

            _sysJournalHeader = new MemoryEntity<JournalHeader>("Заголовок системного журнала", this, 0x3800);
            _logicJournalHeader = new MemoryEntity<JournalHeader>("Заголовок журнала логики", this, 0x4800);
        }
        #endregion

        #region Properties

        public MemoryEntity<UIORam> UioRam
        {
            get { return _uioRam; }
        }

        public MemoryEntity<UIORamV2_0> UioRamV2
        {
            get { return _uioRamV2; }
        }

        public MemoryEntity<SomeStruct> Signature
        {
            get
            {
                _signature = new MemoryEntity<SomeStruct>("Конфигурация дискрет, реле и светодиодов", this, 0x1E00);
                _signature.Values = new ushort[0x80];
                _signature.Slots = QueriesForm.SetSlots(_signature.Values, 0x1E00);
                return _signature;
            }
        }
        public MemoryEntity<Clock> Clock
        {
            get { return _clock; }
        }

        public MemoryEntity<SysErr> SysErr
        {
            get { return _sysErr; }
        }

        public MemoryEntity<VersionDevice> Version
        {
            get { return _version; }
        }

        public MemoryEntity<MemConfigUSB> ConfigUSB
        {
            get { return _configUSB; }
        }

        public MemoryEntity<MemConfigRS485> ConfigRs485
        {
            get { return _configRS485; }
        }

        public MemoryEntity<MemConfigRequest> ConfigRequest
        {
            get { return _configRequest; }
        }

        public MemoryEntity<QueryCount> QueryCount
        {
            get { return _queryCount; }
        }

        public MemoryEntity<AnalogExtConfig> CalibrovkaAnalog
        {
            get { return _calibrovkaAnalog; }
        }
        
        public MemoryEntity<GisterezisExtConfig> CalibrovkaGister
        {
            get { return _calibrovkaGister; }
        }

        public MemoryEntity<SomeStruct> OutputSignal
        {
            get { return _outputSignal; }
        }

        public MemoryEntity<ClockConfig> ClockConfig
        {
            get { return _clockConfig; }
        }

        public MemoryEntity<NewClockConfig> NewClockConfig
        {
            get { return _newClockConfig; }
        }

        public MemoryEntity<SomeStruct> LogicProg
        {
            get { return _logicProg; }
        }
        public MemoryEntity<SomeStruct> LogicProgConst
        {
            get { return _logicProgConst; }
        }
        public MemoryEntity<JournalHeader> SysJournalHeader
        {
            get { return _sysJournalHeader; }
        }

        public MemoryEntity<JournalHeader> LogicJournalHeader
        {
            get { return _logicJournalHeader; }
        }

        public MemoryEntity<RegLogicProg> RegLogicProg
        {
            get { return _regLogicProg; }
        }

        public MemoryEntity<SomeStruct> SaveSCLP
        {
            get { return _saveSCLP; }
        }

        public MemoryEntity<SomeStruct> LogicSignature
        {
            get { return _logicSignature; }
        }
        #endregion

        #region INodeView Members

        [XmlIgnore]
        [Browsable(false)]
        public Type ClassType
        {
            get { return typeof(MLK); }
        }
        [XmlIgnore]
        [Browsable(false)]
        public bool ForceShow
        {
            get { return false; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public Image NodeImage
        {
            get { return Framework.Properties.Resources.mlk; }
        }

        [Browsable(false)]
        public string NodeName
        {
            get { return "МЛК"; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }
        [XmlIgnore]
        [Browsable(false)]
        public bool Deletable
        {
            get { return true; }
        }

        #endregion
        
        #region IDeviceVersion Members
        public Type[] Forms
        {
            get
            {
                return new[]
                {
                    typeof (MlkInOut),
                    typeof (MlkConfiguration),
                    typeof (MlkJournals),
                    typeof (MlkLogicProgramm),
                    typeof (MlkQueriesForm)
                };
            }
        }

        public List<string> Versions
        {
            get
            {
                return new List<string>
                {
                    "МЛК10",
                    "МЛК12",
                    "МЛК13"
                };
            }
        }
        #endregion

        #region Read version
        public override void LoadVersion(object deviceObj)
        {
            System.Threading.Thread.Sleep(500);
            LoadSlot(DeviceNumber, new slot(0x1F00, 0x1F18), "version" + DeviceNumber, this);
            _version.LoadStruct();
        }
        #endregion

        #region Function
        /// <summary>
        /// Возвращает структуру журнала для считывания с указанного адреа и нужной длины
        /// </summary>
        /// <param name="name">Название считываемой структуры</param>
        /// <param name="length">Количество записей в журнале</param>
        /// <param name="startAddress">Начальный адрес</param>
        /// <returns></returns>
        public MemoryEntity<Journal> GetJournal(string name,string caption, int length, ushort startAddress)
        {
            Journal journalStruct = new Journal(length);
            journalStruct.InitStruct(new byte[Marshal.SizeOf(typeof(JournalRep)) * length]);
            MemoryEntity<Journal> retJournal = new MemoryEntity<Journal>(name, this, startAddress);
            retJournal.Slots = QueriesForm.SetSlots(journalStruct.GetValues(), startAddress);
            retJournal.Value = journalStruct;
            retJournal.Values = journalStruct.GetValues();
            return retJournal;
        }

        public void ReadNewCountOfRequests()
        {
            if (this.ReadNewCount != null)
                this.ReadNewCount();
        }

        public void ReadNewEndOfScale()
        {
            if (this.ReadNewLimit != null)
                this.ReadNewLimit();
        }

        public void CheckSettedBit(bool value)
        {
            if (this.CheckBit != null)
                this.CheckBit(value);
        }
        #endregion
    }
}