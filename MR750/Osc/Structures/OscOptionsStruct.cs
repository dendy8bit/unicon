﻿using BEMN.Devices.Structures;

namespace BEMN.MR750.Osc.Structures
{
    /// <summary>
    /// Параматры осцилографа
    /// </summary>
    public class OscOptionsStruct : StructBase
    {
        #region [Properties]

        /// <summary>
        /// Полный размер осцилографа в страницах
        /// </summary>
        public ushort FullOscSizeInPages
        {
            get { return 1008; }
        }

        /// <summary>
        /// Размер страницы в словах(const)
        /// </summary>
        public ushort PageSize
        {
            get { return 1024; }
        }

        /// <summary>
        /// Общий размер осциллографа
        /// </summary>
        public int LoadedFullOscSizeInWords
        {
            get { return FullOscSizeInPages * PageSize; }
        }

        #endregion [Properties]
    }
}
