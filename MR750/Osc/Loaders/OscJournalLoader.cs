﻿using System;
using System.Collections.Generic;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.MR750.Osc.Structures;

namespace BEMN.MR750.Osc.Loaders
{
    /// <summary>
    /// Загружает журнал осцилограммы
    /// </summary>
    public class OscJournalLoader
    {
        #region [Private fields]
        /// <summary>
        /// Записи журнала
        /// </summary>
        private readonly MemoryEntity<OscJournalStruct> _oscJournal;
        /// <summary>
        /// Сброс журнала на нулевую запись
        /// </summary>
        private readonly MemoryEntity<OneWordStruct> _refreshOscJournal;
        /// <summary>
        /// Длинна осцилограммы
        /// </summary>
    //    private readonly MemoryEntity<OscOptionsStruct> _oscOptions;
        /// <summary>
        /// Список структур "Запись журнала осциллографа"
        /// </summary>
        private readonly List<OscJournalStruct> _oscRecords;
        /// <summary>
        /// Текущий номер записи журнала осциллографа
        /// </summary>
        private int _recordNumber;

        #endregion [Private fields]


        #region [Events]
        /// <summary>
        /// Успешно прочитана одна запись журнала осциллографа
        /// </summary>
        public event Action ReadRecordOk;
        /// <summary>
        /// Возникла ошибка при чтении журнала осциллографа
        /// </summary>
        public event Action ReadJournalFail;
        /// <summary>
        /// Успешно прочитаны все записи
        /// </summary>
        public event Action AllJournalReadOk;
        #endregion [Events]


        #region [Ctor's]

        /// <summary>
        /// Создаёт загрузчик Журнала осциллографа
        /// </summary>
        public OscJournalLoader(Device device)
        {
            this._oscRecords = new List<OscJournalStruct>();
            //Сброс журнала на нулевую запись
            this._refreshOscJournal = new MemoryEntity<OneWordStruct>("Обновление журнала осциллографа", device, 0x800); 
            this._refreshOscJournal.AllWriteOk += HandlerHelper.CreateReadArrayHandler(StartReadOscJournal);
            this._refreshOscJournal.AllWriteFail += HandlerHelper.CreateReadArrayHandler(FailReadOscJournal);
            //Записи журнала
            this._oscJournal = new MemoryEntity<OscJournalStruct>("Журнал осциллографа", device, 0x800);
            this._oscJournal.AllReadOk += HandlerHelper.CreateReadArrayHandler(ReadRecord);
            this._oscJournal.AllReadFail += HandlerHelper.CreateReadArrayHandler(FailReadOscJournal);

    
        } 
        #endregion [Ctor's]

        #region [Properties]
        /// <summary>
        /// Номер текущей записи журнала осциллографа
        /// </summary>
        public int RecordNumber
        {
            get { return _recordNumber; }
        }
        /// <summary>
        /// Список структур "Запись журнала осциллографа"
        /// </summary>
        public List<OscJournalStruct> OscRecords
        {
            get { return _oscRecords; }
        }
        #endregion [Properties]


        #region [Private MemoryEntity Events Handlers]
        /// <summary>
        /// Невозможно прочитать журнал
        /// </summary>
        private void FailReadOscJournal()
        {
            if (this.ReadJournalFail != null)
                this.ReadJournalFail.Invoke();
        }

        /// <summary>
        /// Журнал сброшен. Запуск чтения записей
        /// </summary>
        private void StartReadOscJournal()
        {
            this._recordNumber = 0;
            this._oscJournal.LoadStructCycleWhile(a => a.IsEmpty);
        }

        public object[] GetRecord
        {
            get { return this._oscJournal.Value.GetRecord; }
        }

        /// <summary>
        /// Прочитана одна запись журнала
        /// </summary>
        private void ReadRecord()
        {
            if (!this._oscJournal.Value.IsEmpty)
            {
                OscJournalStruct.RecordIndex = this.RecordNumber;
                
                this.OscRecords.Add(this._oscJournal.Value.Clone<OscJournalStruct>());
                this._recordNumber = this.RecordNumber + 1;
                if (this.ReadRecordOk != null)
                    this.ReadRecordOk.Invoke();
            }
            else
            {
                if (this.AllJournalReadOk == null) return;
                this.AllJournalReadOk.Invoke();
            }
        } 
        #endregion [Private MemoryEntity Events Handlers]


        #region [Public members]
        /// <summary>
        /// Запуск чтения журнала осциллографа
        /// </summary>
        public void StartReadJournal()
        {
            this._refreshOscJournal.SaveStruct6();
        } 
        #endregion [Public members]

        internal void Reset()
        {
            this._oscRecords.Clear();
        }

        public void Close()
        {
            this._oscJournal.RemoveStructQueries();
            this._refreshOscJournal.RemoveStructQueries();
            //this._oscOptions.RemoveStructQuerys();
        }
    }
}
