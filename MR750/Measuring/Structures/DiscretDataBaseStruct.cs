﻿using System.Collections;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.MR750.Measuring.Structures
{
    /// <summary>
    /// МР 550 Дискретная база данных
    /// </summary>
    public class DiscretDataBaseStruct : StructBase
    {
        #region [Constants]
        private const int BASE_SIZE = 28;

        #endregion [Constants]


        #region [Private fields]

        [Layout(0, Count = BASE_SIZE)]
        private ushort[] _base; //бд общаяя

        #endregion [Private fields]

        
        #region [Properties]

        private string GetSign(int higt, int low)
        {
            if (Common.GetBit(this._base[17], higt))
            {
                return string.Empty;
            }
            return Common.GetBit(this._base[17], low) ? "-" : "+";
        }

        public string InSign
        {
            get { return GetSign(1, 0); }
        }

        public string IaSign
        {
            get { return GetSign(3, 2); }
        }

        public string IbSign
        {
            get { return GetSign(5, 4); }
        }

        public string IcSign
        {
            get { return GetSign(7, 6); }
        }

        public string I0Sign
        {
            get { return GetSign(9, 8); }
        }

        public string I1Sign
        {
            get { return GetSign(11, 10); }
        }
        public string I2Sign
        {
            get { return GetSign(13, 12); }
        }
        public BitArray ManageSignals
        {
            get
            {
                return new BitArray(new byte[] { Common.LOBYTE(_base[0]), Common.HIBYTE(_base[0]) });
            }
        }

        public BitArray AdditionalSignals
        {
            get
            {
                BitArray temp = new BitArray(new byte[] { Common.LOBYTE(_base[2]) });
                BitArray ret = new BitArray(4);
                for (int i = 0; i < 4; i++)
                {
                    ret[i] = temp[i + 4];
                }
                return ret;
            }
        }

        public BitArray Indicators
        {
            get
            {
                return new BitArray(new byte[] { Common.HIBYTE(_base[2])});
            }
        }

        public BitArray InputSignals
        {
            get
            {
                return new BitArray(new byte[] { Common.LOBYTE(_base[0x9]),
                                                 Common.LOBYTE(_base[0xA])});
            }
        }

        public BitArray OutputSignals
        {
            get
            {
                return new BitArray(new byte[] { Common.HIBYTE(_base[0xA]) });
            }
        }

        public BitArray Rele
        {
            get
            {
                return new BitArray(new byte[] { Common.LOBYTE(_base[3]),
                                                 Common.HIBYTE(_base[3])});
            }
        }

        public BitArray LimitSignals
        {
            get
            {
                return new BitArray(new byte[]
                {
                    Common.LOBYTE(_base[0xB]),
                    Common.HIBYTE(_base[0xB]),
                    Common.LOBYTE(_base[0xC]),
                    Common.HIBYTE(_base[0xC]),
                    Common.LOBYTE(_base[0xD]),
                    Common.HIBYTE(_base[0xD]),
                    Common.LOBYTE(_base[0xE])
                });
            }
        }

        public BitArray Automation
        {
            get
            {
                return new BitArray(new byte[] { Common.HIBYTE(_base[8]) });
            }
        }

        public BitArray FaultSignals
        {
            get
            {
                return new BitArray(new byte[] { Common.LOBYTE(_base[5]), Common.HIBYTE(_base[5]) });
            }
        }

        public BitArray FaultState
        {
            get
            {
                return new BitArray(new byte[] { Common.LOBYTE(_base[4]) });
            }
        }

        public BitArray Ssl
        {
            get
            {
                return new BitArray(new[]
                {
                    Common.HIBYTE(this._base[14]), Common.LOBYTE(this._base[15]), Common.HIBYTE(this._base[15])
                });
            }
        }
        #endregion [Properties]
    }
}
