﻿using System;
using System.Collections.Generic;
using System.Drawing;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.FreeLogicStructures;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR750.AlarmJournal;
using BEMN.MR750.AlarmJournal.Structures;
using BEMN.MR750.BSBGL;
using BEMN.MR750.Configuration;
using BEMN.MR750.Configuration.Structures;
using BEMN.MR750.Configuration.Structures.MeasuringTransformer;
using BEMN.MR750.Measuring;
using BEMN.MR750.Measuring.Structures;
using BEMN.MR750.Osc;
using BEMN.MR750.SystemJournal;
using BEMN.MR750.SystemJournal.Structures;


namespace BEMN.MR750
{
    public class Mr750Device : Device, IDeviceView, IDeviceVersion
    {
        #region Fields

        private MemoryEntity<DateTimeStruct> _dateTime;
        private MemoryEntity<MeasureTransStruct> _measureTrans;
        private MemoryEntity<DiscretDataBaseStruct> _discretDataBase;
        private MemoryEntity<AnalogDataBaseStruct> _analogDataBase;
        private MemoryEntity<MeasureTransStruct> _measureTransAj;
        private MemoryEntity<AlarmJournalStruct> _alarmRecord;
        private MemoryEntity<ConfigurationStructV2> _configurationV1;
        private MemoryEntity<SystemJournalStruct> _systemJournal;

        #endregion

        #region [Properties]
        public MemoryEntity<DiscretDataBaseStruct> DiscretDataBase
        {
            get { return this._discretDataBase; }
        }

        public MemoryEntity<DateTimeStruct> DateAndTime
        {
            get { return this._dateTime; }
        }

        public MemoryEntity<AnalogDataBaseStruct> AnalogDataBase
        {
            get { return this._analogDataBase; }
        }

        public MemoryEntity<MeasureTransStruct> MeasureTrans
        {
            get { return this._measureTrans; }
        }

        public MemoryEntity<MeasureTransStruct> MeasureTransAj
        {
            get { return this._measureTransAj; }
        }

        public MemoryEntity<AlarmJournalStruct> AlarmRecord
        {
            get { return this._alarmRecord; }
        }

        public MemoryEntity<SystemJournalStruct> SystemJournal
        {
            get { return this._systemJournal; }
        }

        public MemoryEntity<ConfigurationStructV2> ConfigurationV1
        {
            get { return this._configurationV1; }
        }
        #endregion

        #region Programming

        private MemoryEntity<SourceProgramStruct> _sourceProgramStruct;
        private MemoryEntity<StartStruct> _programStartStruct;
        private MemoryEntity<ProgramStorageStruct> _programStorageStruct;
        private MemoryEntity<ProgramSignalsStruct> _programSignalsStruct;
        private MemoryEntity<OneWordStruct> _stopSpl;
        private MemoryEntity<OneWordStruct> _startSpl;
        private MemoryEntity<OneWordStruct> _stateSpl;

        public MemoryEntity<ProgramStorageStruct> ProgramStorageStruct
        {
            get { return this._programStorageStruct; }
        }

        public MemoryEntity<StartStruct> ProgramStartStruct
        {
            get { return this._programStartStruct; }
        }

        public MemoryEntity<ProgramSignalsStruct> ProgramSignalsStruct
        {
            get { return this._programSignalsStruct; }
        }

        public MemoryEntity<SourceProgramStruct> SourceProgramStruct
        {
            get { return this._sourceProgramStruct; }
        }

        public MemoryEntity<OneWordStruct> StopSpl
        {
            get { return this._stopSpl; }
        }
        public MemoryEntity<OneWordStruct> StartSpl
        {
            get { return this._startSpl; }
        }
        public MemoryEntity<OneWordStruct> StateSpl
        {
            get { return this._stateSpl; }
        }

        #endregion
        
        public Mr750Device()
        {
            HaveVersion = true;
        }

        public Mr750Device(Modbus mb)
        {
            this.MB = mb;
            this.Init();
        }

        private void Init()
        {
            HaveVersion = true;
            this._configurationV1 = new MemoryEntity<ConfigurationStructV2>("КонфигурацияV1", this, 0x1000);
            this._systemJournal = new MemoryEntity<SystemJournalStruct>("ЖС", this, 0x2000);
            this._alarmRecord = new MemoryEntity<AlarmJournalStruct>("ЖА", this, 0x2800);
            this._measureTransAj = new MemoryEntity<MeasureTransStruct>("Параметры измерений ЖА", this, 0x1000);
            this._dateTime = new MemoryEntity<DateTimeStruct>("DateAndTime", this, 0x200);
            this._discretDataBase = new MemoryEntity<DiscretDataBaseStruct>("Дискретная БД", this, 0x1800);
            this._analogDataBase = new MemoryEntity<AnalogDataBaseStruct>("Аналоговая БД", this, 0x1900);
            this._measureTrans = new MemoryEntity<MeasureTransStruct>("Параметры измерений", this, 0x1000);
            this._sourceProgramStruct = new MemoryEntity<SourceProgramStruct>("SaveProgram", this, 0xB000);
            this._programStartStruct = new MemoryEntity<StartStruct>("SaveProgramStart", this, 0x0001);
            this._programStorageStruct = new MemoryEntity<ProgramStorageStruct>("SaveLoadProgramStorage", this, 0xC000);
            this._programSignalsStruct = new MemoryEntity<ProgramSignalsStruct>("LoadProgramSignals_", this, 0xA000);
            this._stopSpl = new MemoryEntity<OneWordStruct>("StopSpl", this, 0x1808);
            this._startSpl = new MemoryEntity<OneWordStruct>("StartSpl", this, 0x1809);
            this._stateSpl = new MemoryEntity<OneWordStruct>("StatetSpl", this, 0x1805);
        }

        public override Modbus MB
        {
            get { return mb; }
            set { base.MB = value; }
        }
        
        public void WriteConfiguration()
        {
            SetBit(DeviceNumber, 0, true, "МР750 запрос подтверждения", this);
        }

        public void ReadConfiguration()
        {
            SetBit(DeviceNumber, 0, false, "МР750 запрос обновления", this);
        }

        private slot _constraintSelector = new slot(0x400, 0x401);

        public void SelectConstraintGroup(bool mainGroup)
        {
            this._constraintSelector.Value[0] = mainGroup ? (ushort) 0 : (ushort) 1;
            SaveSlot(DeviceNumber, this._constraintSelector, "МР750 №" + DeviceNumber + " переключение группы уставок", this);
        }
        
        #region [INodeViewMembers]
        public Type ClassType
        {
            get { return typeof (Mr750Device); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Framework.Properties.Resources.mr700; }
        }

        public string NodeName
        {
            get { return "МР750"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] {}; }
        }

        public bool Deletable
        {
            get { return true; }
        }
        #endregion

        #region [IDeviceVersion]

        public Type[] Forms
        {
            get
            {
                return new []
                {
                    typeof (Mr750AlarmJournalForm),
                    typeof (Mr750ConfigurationForm),
                    typeof (Mr750MeasuringForm),
                    typeof (Mr750SystemJournalForm),
                    typeof (Mr750NewOscilloscopeForm),
                    typeof (Bsbglef)
                };
            }
        }

        public List<string> Versions
        {
            get
            {
                return new List<string>
                {
                    "1.00",
                    "1.01",
                    "1.02"
                };
            }
        }

        #endregion

    }
}
