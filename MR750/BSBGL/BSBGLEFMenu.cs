﻿using System.Collections.Generic;
using System.Windows.Forms;
using AssemblyResources;
using Crownwood.Magic.Common;
using Crownwood.Magic.Menus;

namespace BEMN.MR750.BSBGL
{
    //Инициализация полосок меню
    partial class Bsbglef
    {
        private System.Windows.Forms.ToolStripButton _newToolStripButton;
        private System.Windows.Forms.ToolStripButton openToolStripButton;
        private System.Windows.Forms.ToolStripButton openProjToolStripButton;
        private System.Windows.Forms.ToolStripButton saveToolStripButton;
        private System.Windows.Forms.ToolStripButton saveProjToolStripButton;
        private System.Windows.Forms.ToolStripButton undoToolStripButton;
        private System.Windows.Forms.ToolStripButton redoToolStripButton;
        private System.Windows.Forms.ToolStripButton cutToolStripButton;
        private System.Windows.Forms.ToolStripButton copyToolStripButton;
        private System.Windows.Forms.ToolStripButton pasteToolStripButton;
        private System.Windows.Forms.ToolStripButton CompilToDeviceToolStripButton;
        private System.Windows.Forms.ToolStripButton StopToolStripButton;
        private System.Windows.Forms.ToolStripButton openFromDeviceToolStripButton;

        #region Toolbox
        protected void CreateToolStrip()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Bsbglef));

            List<ToolStripSeparator> toolStripSepList = new List<ToolStripSeparator>();
            for (int i = 0; i < 5; i++)
            {
                ToolStripSeparator tls = new ToolStripSeparator();
                toolStripSepList.Add(tls);
                tls.Name = "toolStripSeparator" + i;
                tls.Size = new System.Drawing.Size(6, 25);
            }

            this._mainToolStrip = new System.Windows.Forms.ToolStrip();
            this._newToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.openToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.openProjToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.openFromDeviceToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.saveToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.saveProjToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.undoToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.redoToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.cutToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.copyToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.pasteToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.CompilToDeviceToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.StopToolStripButton = new System.Windows.Forms.ToolStripButton();

            ToolStripButton startLogicBtn = new ToolStripButton
            {
                DisplayStyle = ToolStripItemDisplayStyle.Image,
                Image = Resources.go1,
                ImageTransparentColor = System.Drawing.Color.Magenta,
                Size = new System.Drawing.Size(23, 22),
                Text = "Запустить СПЛ в устройстве",
            };
            startLogicBtn.Click += StartLogicProgram;

            ToolStripButton stopLogicBtn = new ToolStripButton
            {
                DisplayStyle = ToolStripItemDisplayStyle.Image,
                Image = Resources.stop1,
                ImageTransparentColor = System.Drawing.Color.Magenta,
                Size = new System.Drawing.Size(23, 22),
                Text = "Остановить выполнение СПЛ в устройстве",
            };
            stopLogicBtn.Click += StopLogicProgram;
            this._mainToolStrip.Items.Add(this._newToolStripButton);
            this._mainToolStrip.Items.Add(toolStripSepList[0]);
            this._mainToolStrip.Items.Add(this.openToolStripButton);
            this._mainToolStrip.Items.Add(this.openProjToolStripButton);
            this._mainToolStrip.Items.Add(toolStripSepList[1]);
            this._mainToolStrip.Items.Add(this.saveToolStripButton);
            this._mainToolStrip.Items.Add(this.saveProjToolStripButton);
            this._mainToolStrip.Items.Add(toolStripSepList[2]);
            this._mainToolStrip.Items.Add(this.undoToolStripButton);
            this._mainToolStrip.Items.Add(this.redoToolStripButton);
            this._mainToolStrip.Items.Add(toolStripSepList[3]);
            this._mainToolStrip.Items.Add(this.cutToolStripButton);
            this._mainToolStrip.Items.Add(this.copyToolStripButton);
            this._mainToolStrip.Items.Add(this.pasteToolStripButton);
            this._mainToolStrip.Items.Add(toolStripSepList[4]);
            this._mainToolStrip.Items.Add(this.CompilToDeviceToolStripButton);
            this._mainToolStrip.Items.Add(this.openFromDeviceToolStripButton);
            this._mainToolStrip.Items.Add(new ToolStripSeparator { Margin = new Padding(20, 0, 0, 0) });
            this._mainToolStrip.Items.Add(this.StopToolStripButton);
            this._mainToolStrip.Items.Add(new ToolStripSeparator { Margin = new Padding(20, 0, 0, 0) });
            this._mainToolStrip.Items.Add(startLogicBtn);
            this._mainToolStrip.Items.Add(stopLogicBtn);

            this._mainToolStrip.Location = new System.Drawing.Point(0, 0);
            this._mainToolStrip.Name = "MainToolStrip";
            this._mainToolStrip.Size = new System.Drawing.Size(816, 25);
            this._mainToolStrip.TabIndex = 0;
            this._mainToolStrip.Text = "toolStrip1";

            this._newToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._newToolStripButton.Image = (System.Drawing.Image)resources.GetObject("filenew");
            this._newToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._newToolStripButton.Name = "_newToolStripButton";
            this._newToolStripButton.Size = new System.Drawing.Size(23, 22);
            this._newToolStripButton.Text = "Новая схема";
            this._newToolStripButton.Click += this.OnFileNew;

            this.openToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.openToolStripButton.Image = (System.Drawing.Image)resources.GetObject("fileopen");
            this.openToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openToolStripButton.Name = "openToolStripButton";
            this.openToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.openToolStripButton.Text = "Открыть документ";
            this.openToolStripButton.Click += this.OnFileOpen;

            this.openProjToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.openProjToolStripButton.Image = (System.Drawing.Image)resources.GetObject("fileopenproject");
            this.openProjToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openProjToolStripButton.Name = "openProjToolStripButton";
            this.openProjToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.openProjToolStripButton.Text = "Открыть проект";
            this.openProjToolStripButton.Click += this.OnFileOpenProject;

            this.openFromDeviceToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.openFromDeviceToolStripButton.Image = Resources.openfromdevicebmp;
            this.openFromDeviceToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openFromDeviceToolStripButton.Name = "openFromDeviceToolStripButton";
            this.openFromDeviceToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.openFromDeviceToolStripButton.Text = "Загрузить архив СПЛ из устройства";
            this.openFromDeviceToolStripButton.Click += this.OnFileOpenFromDevice;

            this.saveToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveToolStripButton.Image = (System.Drawing.Image)resources.GetObject("filesave");
            this.saveToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveToolStripButton.Name = "saveToolStripButton";
            this.saveToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.saveToolStripButton.Text = "Сохранить документ";
            this.saveToolStripButton.Click += this.OnFileSave;

            this.saveProjToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveProjToolStripButton.Image = (System.Drawing.Image)resources.GetObject("filesaveproject");
            this.saveProjToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveProjToolStripButton.Name = "saveProjToolStripButton";
            this.saveProjToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.saveProjToolStripButton.Text = "Сохранить проект";
            this.saveProjToolStripButton.Click += this.OnFileSaveProject;
            
            this.undoToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.undoToolStripButton.Image = (System.Drawing.Image)resources.GetObject("undo");
            this.undoToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.undoToolStripButton.Name = "undoToolStripButton";
            this.undoToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.undoToolStripButton.Text = "Отмена";
            this.undoToolStripButton.Click += this.OnUndo;

            this.redoToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.redoToolStripButton.Image = (System.Drawing.Image)resources.GetObject("redo");
            this.redoToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.redoToolStripButton.Name = "redoToolStripButton";
            this.redoToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.redoToolStripButton.Text = "Восстановить отмененое";
            this.redoToolStripButton.Click += this.OnRedo;

            this.cutToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.cutToolStripButton.Image = (System.Drawing.Image)resources.GetObject("cut");
            this.cutToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cutToolStripButton.Name = "cutToolStripButton";
            this.cutToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.cutToolStripButton.Text = "Вырезать";
            this.cutToolStripButton.Click += this.OnEditCut;

            this.copyToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.copyToolStripButton.Image = (System.Drawing.Image)resources.GetObject("copy");
            this.copyToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.copyToolStripButton.Name = "copyToolStripButton";
            this.copyToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.copyToolStripButton.Text = "Копировать";
            this.copyToolStripButton.Click += this.OnEditCopy;

            this.pasteToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.pasteToolStripButton.Image = (System.Drawing.Image)resources.GetObject("paste");
            this.pasteToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.pasteToolStripButton.Name = "pasteToolStripButton";
            this.pasteToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.pasteToolStripButton.Text = "Вставить";
            this.pasteToolStripButton.Click += this.OnEditPaste;

            this.CompilToDeviceToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.CompilToDeviceToolStripButton.Image = (System.Drawing.Image)resources.GetObject("compileanddownload");
            this.CompilToDeviceToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.CompilToDeviceToolStripButton.Name = "CompilToDeviceToolStripButton";
            this.CompilToDeviceToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.CompilToDeviceToolStripButton.Text = "Загрузить программу и архив СПЛ в устройство, запустить эмулятор";
            this.CompilToDeviceToolStripButton.Click += this.OnCompileUpload;

            this.StopToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.StopToolStripButton.Image = (System.Drawing.Image)resources.GetObject("stop");
            this.StopToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.StopToolStripButton.Name = "CompilToDeviceToolStripButton";
            this.StopToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.StopToolStripButton.Text = "Остановка эмуляции";
            this.StopToolStripButton.Click += OnStop;
        }

        private void CreateMenus()
        {
            MenuControl topMenu = new MenuControl {Style = VisualStyle.IDE, MultiLine = false};
            MenuCommand topFile = new MenuCommand("&Файл");
            MenuCommand topEdit = new MenuCommand("Правка");
            MenuCommand topView = new MenuCommand("Вид");

            topMenu.MenuCommands.AddRange(new [] { topFile, topEdit, topView });

            //File
            MenuCommand topFileNew = new MenuCommand("Новый", this.OnFileNew);
            MenuCommand topFileOpen = new MenuCommand("Открыть документ", this.OnFileOpen);
            MenuCommand topFileOpenProject = new MenuCommand("Открыть проект", this.OnFileOpenProject);
            MenuCommand topFileClose = new MenuCommand("Закрыть документ", this.OnFileClose);
            MenuCommand topFileCloseProject = new MenuCommand("Закрыть проект", this.OnFileCloseProject);
            MenuCommand topFileSave = new MenuCommand("Сохранить документ", this.OnFileSave);
            MenuCommand topFileSaveProject = new MenuCommand("Сохранить проект", this.OnFileSaveProject);
            topFile.MenuCommands.AddRange(new[]
            {
                topFileNew, topFileOpen, topFileOpenProject,
                topFileClose, topFileCloseProject,
                topFileSave, topFileSaveProject
            });
            //Edit
            MenuCommand topEditCopy = new MenuCommand("Копировать", this.OnEditCopy);
            MenuCommand topEditPaste = new MenuCommand("Вставить", this.OnEditPaste);

            topEdit.MenuCommands.AddRange(new [] { topEditCopy, topEditPaste });
            //View
            MenuCommand topViewOutputWindow = new MenuCommand("Сообщения", this.OnViewOutputWindow);
            MenuCommand topViewToolWindow = new MenuCommand("Библиотека", this.OnViewToolWindow);
            topView.MenuCommands.AddRange(new [] {topViewOutputWindow, topViewToolWindow});
            
            topMenu.Dock = DockStyle.Top;
            Controls.Add(topMenu);
        }
        #endregion
    }
}
