namespace BEMN.MR750.Configuration
{
    partial class Mr750ConfigurationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param pinName="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle41 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle46 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle47 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle48 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle42 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle43 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle44 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle45 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle49 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle53 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle54 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle55 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle50 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle51 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle52 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle56 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle60 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle61 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle62 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle57 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle58 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle59 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle63 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle67 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle68 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle69 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle64 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle65 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle66 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle70 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle74 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle75 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle76 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle71 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle72 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle73 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle77 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle83 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle84 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle78 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle79 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle80 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle81 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle82 = new System.Windows.Forms.DataGridViewCellStyle();
            this.apv_blocking = new System.Windows.Forms.ComboBox();
            this.apv_conf = new System.Windows.Forms.ComboBox();
            this._saveConfigBut = new System.Windows.Forms.Button();
            this._loadConfigBut = new System.Windows.Forms.Button();
            this._toolTip = new System.Windows.Forms.ToolTip(this.components);
            this._TT_Box = new System.Windows.Forms.MaskedTextBox();
            this._writeConfigBut = new System.Windows.Forms.Button();
            this._saveConfigurationDlg = new System.Windows.Forms.SaveFileDialog();
            this._statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._exchangeProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this._openConfigurationDlg = new System.Windows.Forms.OpenFileDialog();
            this._statusStrip = new System.Windows.Forms.StatusStrip();
            this._readConfigBut = new System.Windows.Forms.Button();
            this.label56 = new System.Windows.Forms.Label();
            this.apv_time_4krat = new System.Windows.Forms.MaskedTextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.apv_time_3krat = new System.Windows.Forms.MaskedTextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.apv_time_2krat = new System.Windows.Forms.MaskedTextBox();
            this._tabControl = new System.Windows.Forms.TabControl();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.readFromDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearSetpointsItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readFromFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToHtmlItem = new System.Windows.Forms.ToolStripMenuItem();
            this._inSignalsPage = new System.Windows.Forms.TabPage();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.connectionPort = new System.Windows.Forms.ComboBox();
            this.label60 = new System.Windows.Forms.Label();
            this.groupBox29 = new System.Windows.Forms.GroupBox();
            this._keysCheckedListBox = new System.Windows.Forms.CheckedListBox();
            this._box = new System.Windows.Forms.GroupBox();
            this.noUaccLabel = new System.Windows.Forms.Label();
            this.noUaccBox = new System.Windows.Forms.MaskedTextBox();
            this._f = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this._uca = new System.Windows.Forms.ComboBox();
            this.label123 = new System.Windows.Forms.Label();
            this._TNNP_dispepairCombo = new System.Windows.Forms.ComboBox();
            this.label41 = new System.Windows.Forms.Label();
            this._TN_dispepairCombo = new System.Windows.Forms.ComboBox();
            this.label39 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this._TN_Box = new System.Windows.Forms.MaskedTextBox();
            this._TNNP_Box = new System.Windows.Forms.MaskedTextBox();
            this.groupBox24 = new System.Windows.Forms.GroupBox();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this._inputSignals9 = new System.Windows.Forms.DataGridView();
            this._signalValueNumILI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._signalValueColILI = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this._inputSignals10 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn8 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this._inputSignals11 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn9 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage12 = new System.Windows.Forms.TabPage();
            this._inputSignals12 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn10 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.groupBox27 = new System.Windows.Forms.GroupBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this._inputSignals1 = new System.Windows.Forms.DataGridView();
            this._lsChannelCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._signalValueCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this._inputSignals2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this._inputSignals3 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this._inputSignals4 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.gb3 = new System.Windows.Forms.GroupBox();
            this.label28 = new System.Windows.Forms.Label();
            this.oscPercent = new System.Windows.Forms.MaskedTextBox();
            this.gb2 = new System.Windows.Forms.GroupBox();
            this.oscFix = new System.Windows.Forms.ComboBox();
            this.gb1 = new System.Windows.Forms.GroupBox();
            this.oscLength = new System.Windows.Forms.ComboBox();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this._ompCheckBox = new System.Windows.Forms.CheckBox();
            this._HUD_Box = new System.Windows.Forms.MaskedTextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this._manageSignalsSDTU_Combo = new System.Windows.Forms.ComboBox();
            this.label43 = new System.Windows.Forms.Label();
            this._manageSignalsExternalCombo = new System.Windows.Forms.ComboBox();
            this.label44 = new System.Windows.Forms.Label();
            this._manageSignalsKeyCombo = new System.Windows.Forms.ComboBox();
            this.label45 = new System.Windows.Forms.Label();
            this._manageSignalsButtonCombo = new System.Windows.Forms.ComboBox();
            this.label46 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.groupBox22 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this._releDispepairBox = new System.Windows.Forms.MaskedTextBox();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this._dispepairCheckList = new System.Windows.Forms.CheckedListBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this._switcherDurationBox = new System.Windows.Forms.MaskedTextBox();
            this._switcherTokBox = new System.Windows.Forms.MaskedTextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this._switcherTimeBox = new System.Windows.Forms.MaskedTextBox();
            this._switcherImpulseBox = new System.Windows.Forms.MaskedTextBox();
            this._switcherBlockCombo = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this._switcherErrorCombo = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this._switcherStateOnCombo = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this._switcherStateOffCombo = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.disableComboBox = new System.Windows.Forms.ComboBox();
            this.disableLabel = new System.Windows.Forms.Label();
            this._constraintGroupCombo = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this._signalizationCombo = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this._extOnCombo = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this._extOffCombo = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this._keyOnCombo = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this._keyOffCombo = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._TT_typeCombo = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this._maxTok_Box = new System.Windows.Forms.MaskedTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this._TTNP_Box = new System.Windows.Forms.MaskedTextBox();
            this._outputSignalsPage = new System.Windows.Forms.TabPage();
            this.VLSTabControl = new System.Windows.Forms.TabControl();
            this.VLS1 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.VLS2 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox2 = new System.Windows.Forms.CheckedListBox();
            this.VLS3 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox3 = new System.Windows.Forms.CheckedListBox();
            this.VLS4 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox4 = new System.Windows.Forms.CheckedListBox();
            this.VLS5 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox5 = new System.Windows.Forms.CheckedListBox();
            this.VLS6 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox6 = new System.Windows.Forms.CheckedListBox();
            this.VLS7 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox7 = new System.Windows.Forms.CheckedListBox();
            this.VLS8 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox8 = new System.Windows.Forms.CheckedListBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this._outputIndicatorsGrid = new System.Windows.Forms.DataGridView();
            this._outIndNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._outIndTypeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._outIndSignalCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._outIndResetCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._outIndAlarmCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._outIndSystemCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._externalDefensePage = new System.Windows.Forms.TabPage();
            this._externalDefenseGrid = new System.Windows.Forms.DataGridView();
            this._extDefNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._exDefModeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._exDefBlockingCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._exDefWorkingCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._exDefWorkingTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._exDefReturnCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._exDefAPVreturnCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._exDefReturnNumberCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._exDefReturnTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._exDefUROVcol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._exDefAPVcol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._exDefAVRcol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._exDefOSCcol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._exDefResetcol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._automaticPage = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.avr_mode = new System.Windows.Forms.CheckBox();
            this.label34 = new System.Windows.Forms.Label();
            this._umax2 = new System.Windows.Forms.MaskedTextBox();
            this.label35 = new System.Windows.Forms.Label();
            this._umin2 = new System.Windows.Forms.MaskedTextBox();
            this.label32 = new System.Windows.Forms.Label();
            this._umax1 = new System.Windows.Forms.MaskedTextBox();
            this.label33 = new System.Windows.Forms.Label();
            this._umin1 = new System.Windows.Forms.MaskedTextBox();
            this.label58 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.avr_disconnection = new System.Windows.Forms.MaskedTextBox();
            this.label59 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.avr_time_return = new System.Windows.Forms.MaskedTextBox();
            this._side2 = new System.Windows.Forms.ComboBox();
            this._side1 = new System.Windows.Forms.ComboBox();
            this.avr_blocking = new System.Windows.Forms.ComboBox();
            this.label66 = new System.Windows.Forms.Label();
            this.avr_reset_blocking = new System.Windows.Forms.ComboBox();
            this.label65 = new System.Windows.Forms.Label();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label57 = new System.Windows.Forms.Label();
            this.lzsh_constraint = new System.Windows.Forms.MaskedTextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this._apvStartCheckBox = new System.Windows.Forms.CheckBox();
            this.label54 = new System.Windows.Forms.Label();
            this.apv_time_1krat = new System.Windows.Forms.MaskedTextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.apv_time_ready = new System.Windows.Forms.MaskedTextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.apv_time_blocking = new System.Windows.Forms.MaskedTextBox();
            this._newDefendingTabPage = new System.Windows.Forms.TabPage();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.tabControl3 = new System.Windows.Forms.TabControl();
            this._tokDefendingTabPage = new System.Windows.Forms.TabPage();
            this._tokDefenseGrid3 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn11 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn12 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewCheckBoxColumn5 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn13 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn14 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn15 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn16 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn8 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn9 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn10 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewComboBoxColumn17 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this._tokDefenseInbox = new System.Windows.Forms.MaskedTextBox();
            this.label24 = new System.Windows.Forms.Label();
            this._tokDefenseI2box = new System.Windows.Forms.MaskedTextBox();
            this.label23 = new System.Windows.Forms.Label();
            this._tokDefenseI0box = new System.Windows.Forms.MaskedTextBox();
            this.label22 = new System.Windows.Forms.Label();
            this._tokDefenseIbox = new System.Windows.Forms.MaskedTextBox();
            this.label21 = new System.Windows.Forms.Label();
            this._tokDefenseGrid4 = new System.Windows.Forms.DataGridView();
            this._tokDefense4NameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense4ModeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense4BlockNumberCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense4UpuskCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense4PuskConstraintCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense4WorkConstraintCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense4WorkTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense4SpeedupCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense4SpeedupTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense4UROVCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense4APVCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense4OSCv11Col = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseGrid2 = new System.Windows.Forms.DataGridView();
            this._tokDefense3NameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense3ModeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense3BlockingNumberCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense3UpuskCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense3PuskConstraintCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense3DirectionCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense3BlockingExistCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense3ParameterCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense3WorkConstraintCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense3FeatureCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense3WorkTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense3SpeedupCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense3SpeedupTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense3UROVCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense3APVCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense3OSCv11Col = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseGrid1 = new System.Windows.Forms.DataGridView();
            this._tokDefenseNameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefenseModeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseBlockNumberCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseU_PuskCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefensePuskConstraintCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefenseDirectionCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseBlockExistCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseParameterCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefenseWorkConstraintCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefenseFeatureCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseWorkTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefenseSpeedUpCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefenseSpeedupTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefenseUROVCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefenseAPVCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefenseOSCv11Col = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefendingTabPage = new System.Windows.Forms.TabPage();
            this._voltageDefensesGrid11 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn4 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn5 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn6 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn4 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewComboBoxColumn7 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewCheckBoxColumn6 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn7 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesGrid2 = new System.Windows.Forms.DataGridView();
            this._voltageDefensesNameCol2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesModeCol2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesBlockingNumberCol2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesParameterCol2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesWorkConstraintCol2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesWorkTimeCol2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesReturnCol2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesAPVreturnCol2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesReturnConstraintCol2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesReturnTimeCol2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesUROVcol2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesAPVcol2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesOSC�11Col2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesResetCol2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesGrid1 = new System.Windows.Forms.DataGridView();
            this._voltageDefensesNameCol1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesModeCol1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesBlockingNumberCol1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesParameterCol1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesWorkConstraintCol1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesWorkTimeCol1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesReturnCol1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesAPVReturlCol1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesReturnConstraintCol1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesReturnTimeCol1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesUROVCol1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesAPVCol1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesOSCv11Col1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesResetCol1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._u5vColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesGrid3 = new System.Windows.Forms.DataGridView();
            this._voltageDefensesNameCol3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesModeCol3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesBlockingNumberCol3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesParameterCol3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesWorkConstraintCol3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesTimeConstraintCol3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesReturnCol3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesAPVreturnCol3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesReturnConstraintCol3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesReturnTimeCol3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesUROVcol3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesAPVcol3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesOSCv11Col3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesResetcol3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._frequencyDefendingTabPage = new System.Windows.Forms.TabPage();
            this._frequenceDefensesGrid = new System.Windows.Forms.DataGridView();
            this._frequenceDefensesName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._frequenceDefensesMode = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._frequenceDefensesBlockingNumber = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._frequenceDefensesWorkConstraint = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._frequenceDefensesWorkTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._frequenceDefensesReturn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._frequenceDefensesAPVReturn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._frequenceDefensesConstraintAPV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._frequenceDefensesReturnTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._frequenceDefensesUROV = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._frequenceDefensesAPV = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._frequenceDefensesOSCv11 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._frequenceDefensesReset = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this._reserveRadioButtonGroup = new System.Windows.Forms.RadioButton();
            this._ChangeSetPointsButton = new System.Windows.Forms.Button();
            this._mainRadioButtonGroup = new System.Windows.Forms.RadioButton();
            this._resetConfigBut = new System.Windows.Forms.Button();
            this._saveToXmlButton = new System.Windows.Forms.Button();
            this._saveXmlDialog = new System.Windows.Forms.SaveFileDialog();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this._statusStrip.SuspendLayout();
            this._tabControl.SuspendLayout();
            this.contextMenu.SuspendLayout();
            this._inSignalsPage.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox29.SuspendLayout();
            this._box.SuspendLayout();
            this.groupBox24.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals9)).BeginInit();
            this.tabPage10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals10)).BeginInit();
            this.tabPage11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals11)).BeginInit();
            this.tabPage12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals12)).BeginInit();
            this.groupBox27.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals1)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals2)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals3)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals4)).BeginInit();
            this.gb3.SuspendLayout();
            this.gb2.SuspendLayout();
            this.gb1.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox22.SuspendLayout();
            this.groupBox21.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this._outputSignalsPage.SuspendLayout();
            this.VLSTabControl.SuspendLayout();
            this.VLS1.SuspendLayout();
            this.VLS2.SuspendLayout();
            this.VLS3.SuspendLayout();
            this.VLS4.SuspendLayout();
            this.VLS5.SuspendLayout();
            this.VLS6.SuspendLayout();
            this.VLS7.SuspendLayout();
            this.VLS8.SuspendLayout();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._outputIndicatorsGrid)).BeginInit();
            this._externalDefensePage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._externalDefenseGrid)).BeginInit();
            this._automaticPage.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this._newDefendingTabPage.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.tabControl3.SuspendLayout();
            this._tokDefendingTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._tokDefenseGrid3)).BeginInit();
            this.groupBox15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._tokDefenseGrid4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._tokDefenseGrid2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._tokDefenseGrid1)).BeginInit();
            this._voltageDefendingTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._voltageDefensesGrid11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._voltageDefensesGrid2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._voltageDefensesGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._voltageDefensesGrid3)).BeginInit();
            this._frequencyDefendingTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._frequenceDefensesGrid)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // apv_blocking
            // 
            this.apv_blocking.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.apv_blocking.FormattingEnabled = true;
            this.apv_blocking.Location = new System.Drawing.Point(146, 40);
            this.apv_blocking.Name = "apv_blocking";
            this.apv_blocking.Size = new System.Drawing.Size(87, 21);
            this.apv_blocking.TabIndex = 3;
            // 
            // apv_conf
            // 
            this.apv_conf.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.apv_conf.FormattingEnabled = true;
            this.apv_conf.Location = new System.Drawing.Point(146, 19);
            this.apv_conf.Name = "apv_conf";
            this.apv_conf.Size = new System.Drawing.Size(87, 21);
            this.apv_conf.TabIndex = 1;
            // 
            // _saveConfigBut
            // 
            this._saveConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._saveConfigBut.Location = new System.Drawing.Point(625, 676);
            this._saveConfigBut.Name = "_saveConfigBut";
            this._saveConfigBut.Size = new System.Drawing.Size(125, 23);
            this._saveConfigBut.TabIndex = 10;
            this._saveConfigBut.Text = "��������� � ����";
            this.toolTip1.SetToolTip(this._saveConfigBut, "��������� ������������ � ���� (CTRL+S)");
            this._saveConfigBut.UseVisualStyleBackColor = true;
            this._saveConfigBut.Click += new System.EventHandler(this._saveConfigBut_Click);
            // 
            // _loadConfigBut
            // 
            this._loadConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._loadConfigBut.Location = new System.Drawing.Point(486, 676);
            this._loadConfigBut.Name = "_loadConfigBut";
            this._loadConfigBut.Size = new System.Drawing.Size(125, 23);
            this._loadConfigBut.TabIndex = 9;
            this._loadConfigBut.Text = "��������� �� �����";
            this.toolTip1.SetToolTip(this._loadConfigBut, "��������� ������������ �� ����� (CTRL+O)");
            this._loadConfigBut.UseVisualStyleBackColor = true;
            this._loadConfigBut.Click += new System.EventHandler(this._loadConfigBut_Click);
            // 
            // _toolTip
            // 
            this._toolTip.IsBalloon = true;
            this._toolTip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Warning;
            // 
            // _TT_Box
            // 
            this._TT_Box.Location = new System.Drawing.Point(148, 12);
            this._TT_Box.Name = "_TT_Box";
            this._TT_Box.Size = new System.Drawing.Size(47, 20);
            this._TT_Box.TabIndex = 0;
            this._TT_Box.Tag = "0,1500";
            this._TT_Box.Text = "0";
            this._TT_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._toolTip.SetToolTip(this._TT_Box, "��������� ��� ��������������");
            // 
            // _writeConfigBut
            // 
            this._writeConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._writeConfigBut.Location = new System.Drawing.Point(167, 676);
            this._writeConfigBut.Name = "_writeConfigBut";
            this._writeConfigBut.Size = new System.Drawing.Size(149, 23);
            this._writeConfigBut.TabIndex = 8;
            this._writeConfigBut.Text = "�������� � ����������";
            this.toolTip1.SetToolTip(this._writeConfigBut, "�������� ������������ � ���������� (CTRL+W)");
            this._writeConfigBut.UseVisualStyleBackColor = true;
            this._writeConfigBut.Click += new System.EventHandler(this._writeConfigBut_Click);
            // 
            // _saveConfigurationDlg
            // 
            this._saveConfigurationDlg.DefaultExt = "bin";
            this._saveConfigurationDlg.FileName = "��750_�������";
            this._saveConfigurationDlg.Filter = "(*.bin) | *.bin";
            this._saveConfigurationDlg.Title = "���������  ������� ��� ��750";
            // 
            // _statusLabel
            // 
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // _exchangeProgressBar
            // 
            this._exchangeProgressBar.Maximum = 80;
            this._exchangeProgressBar.Name = "_exchangeProgressBar";
            this._exchangeProgressBar.Size = new System.Drawing.Size(100, 16);
            this._exchangeProgressBar.Step = 1;
            // 
            // _openConfigurationDlg
            // 
            this._openConfigurationDlg.DefaultExt = "bin";
            this._openConfigurationDlg.Filter = "(*.bin) | *.bin";
            this._openConfigurationDlg.RestoreDirectory = true;
            this._openConfigurationDlg.Title = "������� ������� ��� ��750";
            // 
            // _statusStrip
            // 
            this._statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._exchangeProgressBar,
            this._statusLabel});
            this._statusStrip.Location = new System.Drawing.Point(0, 702);
            this._statusStrip.Name = "_statusStrip";
            this._statusStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this._statusStrip.Size = new System.Drawing.Size(906, 22);
            this._statusStrip.TabIndex = 11;
            this._statusStrip.Text = "statusStrip1";
            // 
            // _readConfigBut
            // 
            this._readConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._readConfigBut.Location = new System.Drawing.Point(12, 676);
            this._readConfigBut.Name = "_readConfigBut";
            this._readConfigBut.Size = new System.Drawing.Size(149, 23);
            this._readConfigBut.TabIndex = 7;
            this._readConfigBut.Text = "��������� �� ����������";
            this.toolTip1.SetToolTip(this._readConfigBut, "��������� ������������ �� ���������� (CTRL+R)");
            this._readConfigBut.UseVisualStyleBackColor = true;
            this._readConfigBut.Click += new System.EventHandler(this._readConfigBut_Click);
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(8, 166);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(101, 13);
            this.label56.TabIndex = 33;
            this.label56.Text = "����� 4 �����, ��";
            // 
            // apv_time_4krat
            // 
            this.apv_time_4krat.Location = new System.Drawing.Point(146, 161);
            this.apv_time_4krat.Name = "apv_time_4krat";
            this.apv_time_4krat.Size = new System.Drawing.Size(85, 20);
            this.apv_time_4krat.TabIndex = 23;
            this.apv_time_4krat.Tag = "0,3000000";
            this.apv_time_4krat.Text = "0";
            this.apv_time_4krat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(8, 146);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(101, 13);
            this.label52.TabIndex = 32;
            this.label52.Text = "����� 3 �����, ��";
            // 
            // apv_time_3krat
            // 
            this.apv_time_3krat.Location = new System.Drawing.Point(146, 141);
            this.apv_time_3krat.Name = "apv_time_3krat";
            this.apv_time_3krat.Size = new System.Drawing.Size(85, 20);
            this.apv_time_3krat.TabIndex = 22;
            this.apv_time_3krat.Tag = "0,3000000";
            this.apv_time_3krat.Text = "0";
            this.apv_time_3krat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(8, 126);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(101, 13);
            this.label53.TabIndex = 31;
            this.label53.Text = "����� 2 �����, ��";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(8, 186);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(132, 13);
            this.label55.TabIndex = 34;
            this.label55.Text = "������ ��� �� �������.";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(8, 26);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(80, 13);
            this.label47.TabIndex = 25;
            this.label47.Text = "������������";
            // 
            // apv_time_2krat
            // 
            this.apv_time_2krat.Location = new System.Drawing.Point(146, 121);
            this.apv_time_2krat.Name = "apv_time_2krat";
            this.apv_time_2krat.Size = new System.Drawing.Size(85, 20);
            this.apv_time_2krat.TabIndex = 21;
            this.apv_time_2krat.Tag = "0,3000000";
            this.apv_time_2krat.Text = "0";
            this.apv_time_2krat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _tabControl
            // 
            this._tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tabControl.ContextMenuStrip = this.contextMenu;
            this._tabControl.Controls.Add(this._inSignalsPage);
            this._tabControl.Controls.Add(this._outputSignalsPage);
            this._tabControl.Controls.Add(this._externalDefensePage);
            this._tabControl.Controls.Add(this._automaticPage);
            this._tabControl.Controls.Add(this._newDefendingTabPage);
            this._tabControl.Location = new System.Drawing.Point(0, 0);
            this._tabControl.Name = "_tabControl";
            this._tabControl.SelectedIndex = 0;
            this._tabControl.Size = new System.Drawing.Size(907, 671);
            this._tabControl.TabIndex = 6;
            // 
            // contextMenu
            // 
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.readFromDeviceItem,
            this.writeToDeviceItem,
            this.clearSetpointsItem,
            this.readFromFileItem,
            this.writeToFileItem,
            this.writeToHtmlItem});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(213, 136);
            this.contextMenu.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.contextMenu_ItemClicked);
            // 
            // readFromDeviceItem
            // 
            this.readFromDeviceItem.Name = "readFromDeviceItem";
            this.readFromDeviceItem.Size = new System.Drawing.Size(212, 22);
            this.readFromDeviceItem.Text = "��������� �� ����������";
            // 
            // writeToDeviceItem
            // 
            this.writeToDeviceItem.Name = "writeToDeviceItem";
            this.writeToDeviceItem.Size = new System.Drawing.Size(212, 22);
            this.writeToDeviceItem.Text = "�������� � ����������";
            // 
            // clearSetpointsItem
            // 
            this.clearSetpointsItem.Name = "clearSetpointsItem";
            this.clearSetpointsItem.Size = new System.Drawing.Size(212, 22);
            this.clearSetpointsItem.Text = "�������� �������";
            // 
            // readFromFileItem
            // 
            this.readFromFileItem.Name = "readFromFileItem";
            this.readFromFileItem.Size = new System.Drawing.Size(212, 22);
            this.readFromFileItem.Text = "��������� �� �����";
            // 
            // writeToFileItem
            // 
            this.writeToFileItem.Name = "writeToFileItem";
            this.writeToFileItem.Size = new System.Drawing.Size(212, 22);
            this.writeToFileItem.Text = "��������� � ����";
            // 
            // writeToHtmlItem
            // 
            this.writeToHtmlItem.Name = "writeToHtmlItem";
            this.writeToHtmlItem.Size = new System.Drawing.Size(212, 22);
            this.writeToHtmlItem.Text = "��������� � HTML";
            // 
            // _inSignalsPage
            // 
            this._inSignalsPage.Controls.Add(this.groupBox10);
            this._inSignalsPage.Controls.Add(this.groupBox29);
            this._inSignalsPage.Controls.Add(this._box);
            this._inSignalsPage.Controls.Add(this.groupBox24);
            this._inSignalsPage.Controls.Add(this.groupBox27);
            this._inSignalsPage.Controls.Add(this.gb3);
            this._inSignalsPage.Controls.Add(this.gb2);
            this._inSignalsPage.Controls.Add(this.gb1);
            this._inSignalsPage.Controls.Add(this.groupBox18);
            this._inSignalsPage.Controls.Add(this.groupBox7);
            this._inSignalsPage.Controls.Add(this.groupBox6);
            this._inSignalsPage.Controls.Add(this.groupBox4);
            this._inSignalsPage.Controls.Add(this.groupBox3);
            this._inSignalsPage.Controls.Add(this.groupBox1);
            this._inSignalsPage.Location = new System.Drawing.Point(4, 22);
            this._inSignalsPage.Name = "_inSignalsPage";
            this._inSignalsPage.Padding = new System.Windows.Forms.Padding(3);
            this._inSignalsPage.Size = new System.Drawing.Size(899, 645);
            this._inSignalsPage.TabIndex = 0;
            this._inSignalsPage.Text = "������� �������";
            this._inSignalsPage.UseVisualStyleBackColor = true;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.connectionPort);
            this.groupBox10.Controls.Add(this.label60);
            this.groupBox10.Location = new System.Drawing.Point(426, 365);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(203, 44);
            this.groupBox10.TabIndex = 24;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "��������� �����";
            this.groupBox10.Visible = false;
            // 
            // connectionPort
            // 
            this.connectionPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.connectionPort.FormattingEnabled = true;
            this.connectionPort.Location = new System.Drawing.Point(71, 17);
            this.connectionPort.Name = "connectionPort";
            this.connectionPort.Size = new System.Drawing.Size(126, 21);
            this.connectionPort.TabIndex = 17;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(6, 22);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(32, 13);
            this.label60.TabIndex = 16;
            this.label60.Text = "����";
            // 
            // groupBox29
            // 
            this.groupBox29.Controls.Add(this._keysCheckedListBox);
            this.groupBox29.Location = new System.Drawing.Point(635, 253);
            this.groupBox29.Name = "groupBox29";
            this.groupBox29.Size = new System.Drawing.Size(144, 271);
            this.groupBox29.TabIndex = 35;
            this.groupBox29.TabStop = false;
            this.groupBox29.Text = "�����";
            // 
            // _keysCheckedListBox
            // 
            this._keysCheckedListBox.CheckOnClick = true;
            this._keysCheckedListBox.FormattingEnabled = true;
            this._keysCheckedListBox.Location = new System.Drawing.Point(6, 19);
            this._keysCheckedListBox.Name = "_keysCheckedListBox";
            this._keysCheckedListBox.ScrollAlwaysVisible = true;
            this._keysCheckedListBox.Size = new System.Drawing.Size(130, 229);
            this._keysCheckedListBox.TabIndex = 7;
            // 
            // _box
            // 
            this._box.Controls.Add(this.noUaccLabel);
            this._box.Controls.Add(this.noUaccBox);
            this._box.Controls.Add(this._f);
            this._box.Controls.Add(this.label29);
            this._box.Controls.Add(this._uca);
            this._box.Controls.Add(this.label123);
            this._box.Controls.Add(this._TNNP_dispepairCombo);
            this._box.Controls.Add(this.label41);
            this._box.Controls.Add(this._TN_dispepairCombo);
            this._box.Controls.Add(this.label39);
            this._box.Controls.Add(this.label4);
            this._box.Controls.Add(this.label6);
            this._box.Controls.Add(this._TN_Box);
            this._box.Controls.Add(this._TNNP_Box);
            this._box.Location = new System.Drawing.Point(8, 107);
            this._box.MinimumSize = new System.Drawing.Size(203, 171);
            this._box.Name = "_box";
            this._box.Size = new System.Drawing.Size(203, 171);
            this._box.TabIndex = 11;
            this._box.TabStop = false;
            this._box.Text = "��������� ���������� � �������";
            // 
            // noUaccLabel
            // 
            this.noUaccLabel.AutoSize = true;
            this.noUaccLabel.Location = new System.Drawing.Point(9, 149);
            this.noUaccLabel.Name = "noUaccLabel";
            this.noUaccLabel.Size = new System.Drawing.Size(90, 13);
            this.noUaccLabel.TabIndex = 18;
            this.noUaccLabel.Text = "�������� U��, �";
            this.noUaccLabel.Visible = false;
            // 
            // noUaccBox
            // 
            this.noUaccBox.Location = new System.Drawing.Point(137, 146);
            this.noUaccBox.Name = "noUaccBox";
            this.noUaccBox.Size = new System.Drawing.Size(60, 20);
            this.noUaccBox.TabIndex = 17;
            this.noUaccBox.Tag = "0,256";
            this.noUaccBox.Text = "0";
            this.noUaccBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.noUaccBox.Visible = false;
            // 
            // _f
            // 
            this._f.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._f.FormattingEnabled = true;
            this._f.Location = new System.Drawing.Point(96, 125);
            this._f.Name = "_f";
            this._f.Size = new System.Drawing.Size(101, 21);
            this._f.TabIndex = 16;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(9, 128);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(13, 13);
            this.label29.TabIndex = 15;
            this.label29.Text = "F";
            // 
            // _uca
            // 
            this._uca.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._uca.FormattingEnabled = true;
            this._uca.Location = new System.Drawing.Point(96, 104);
            this._uca.Name = "_uca";
            this._uca.Size = new System.Drawing.Size(101, 21);
            this._uca.TabIndex = 14;
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Location = new System.Drawing.Point(9, 107);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(27, 13);
            this.label123.TabIndex = 13;
            this.label123.Text = "Uca";
            // 
            // _TNNP_dispepairCombo
            // 
            this._TNNP_dispepairCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._TNNP_dispepairCombo.FormattingEnabled = true;
            this._TNNP_dispepairCombo.Location = new System.Drawing.Point(96, 83);
            this._TNNP_dispepairCombo.Name = "_TNNP_dispepairCombo";
            this._TNNP_dispepairCombo.Size = new System.Drawing.Size(101, 21);
            this._TNNP_dispepairCombo.TabIndex = 10;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(8, 88);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(82, 13);
            this.label41.TabIndex = 9;
            this.label41.Text = "������. ����";
            // 
            // _TN_dispepairCombo
            // 
            this._TN_dispepairCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._TN_dispepairCombo.FormattingEnabled = true;
            this._TN_dispepairCombo.Location = new System.Drawing.Point(96, 42);
            this._TN_dispepairCombo.Name = "_TN_dispepairCombo";
            this._TN_dispepairCombo.Size = new System.Drawing.Size(101, 21);
            this._TN_dispepairCombo.TabIndex = 8;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(9, 46);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(66, 13);
            this.label39.TabIndex = 7;
            this.label39.Text = "������. ��";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 66);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "����������� ����";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 26);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "����������� ��";
            // 
            // _TN_Box
            // 
            this._TN_Box.Location = new System.Drawing.Point(137, 22);
            this._TN_Box.Name = "_TN_Box";
            this._TN_Box.Size = new System.Drawing.Size(60, 20);
            this._TN_Box.TabIndex = 2;
            this._TN_Box.Tag = "0,128000";
            this._TN_Box.Text = "0";
            this._TN_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _TNNP_Box
            // 
            this._TNNP_Box.Location = new System.Drawing.Point(137, 63);
            this._TNNP_Box.Name = "_TNNP_Box";
            this._TNNP_Box.Size = new System.Drawing.Size(60, 20);
            this._TNNP_Box.TabIndex = 5;
            this._TNNP_Box.Tag = "0,128000";
            this._TNNP_Box.Text = "0";
            this._TNNP_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox24
            // 
            this.groupBox24.Controls.Add(this.tabControl2);
            this.groupBox24.Location = new System.Drawing.Point(625, 8);
            this.groupBox24.Name = "groupBox24";
            this.groupBox24.Size = new System.Drawing.Size(193, 244);
            this.groupBox24.TabIndex = 33;
            this.groupBox24.TabStop = false;
            this.groupBox24.Text = "���������� ������� ���";
            // 
            // tabControl2
            // 
            this.tabControl2.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabControl2.Controls.Add(this.tabPage9);
            this.tabControl2.Controls.Add(this.tabPage10);
            this.tabControl2.Controls.Add(this.tabPage11);
            this.tabControl2.Controls.Add(this.tabPage12);
            this.tabControl2.Location = new System.Drawing.Point(6, 18);
            this.tabControl2.Multiline = true;
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(181, 219);
            this.tabControl2.TabIndex = 2;
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this._inputSignals9);
            this.tabPage9.Location = new System.Drawing.Point(4, 25);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage9.Size = new System.Drawing.Size(173, 190);
            this.tabPage9.TabIndex = 0;
            this.tabPage9.Text = "��5";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // _inputSignals9
            // 
            this._inputSignals9.AllowUserToAddRows = false;
            this._inputSignals9.AllowUserToDeleteRows = false;
            this._inputSignals9.AllowUserToResizeColumns = false;
            this._inputSignals9.AllowUserToResizeRows = false;
            this._inputSignals9.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals9.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals9.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals9.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._signalValueNumILI,
            this._signalValueColILI});
            this._inputSignals9.Location = new System.Drawing.Point(3, 3);
            this._inputSignals9.MultiSelect = false;
            this._inputSignals9.Name = "_inputSignals9";
            this._inputSignals9.RowHeadersVisible = false;
            this._inputSignals9.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals9.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this._inputSignals9.RowTemplate.Height = 20;
            this._inputSignals9.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals9.ShowCellErrors = false;
            this._inputSignals9.ShowCellToolTips = false;
            this._inputSignals9.ShowEditingIcon = false;
            this._inputSignals9.ShowRowErrors = false;
            this._inputSignals9.Size = new System.Drawing.Size(167, 183);
            this._inputSignals9.TabIndex = 2;
            // 
            // _signalValueNumILI
            // 
            this._signalValueNumILI.HeaderText = "�";
            this._signalValueNumILI.Name = "_signalValueNumILI";
            this._signalValueNumILI.ReadOnly = true;
            this._signalValueNumILI.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._signalValueNumILI.Width = 24;
            // 
            // _signalValueColILI
            // 
            this._signalValueColILI.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._signalValueColILI.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._signalValueColILI.HeaderText = "��������";
            this._signalValueColILI.Items.AddRange(new object[] {
            "���",
            "��",
            "������"});
            this._signalValueColILI.Name = "_signalValueColILI";
            // 
            // tabPage10
            // 
            this.tabPage10.Controls.Add(this._inputSignals10);
            this.tabPage10.Location = new System.Drawing.Point(4, 25);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage10.Size = new System.Drawing.Size(173, 190);
            this.tabPage10.TabIndex = 1;
            this.tabPage10.Text = "��6";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // _inputSignals10
            // 
            this._inputSignals10.AllowUserToAddRows = false;
            this._inputSignals10.AllowUserToDeleteRows = false;
            this._inputSignals10.AllowUserToResizeColumns = false;
            this._inputSignals10.AllowUserToResizeRows = false;
            this._inputSignals10.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals10.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals10.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals10.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewComboBoxColumn8});
            this._inputSignals10.Location = new System.Drawing.Point(3, 3);
            this._inputSignals10.MultiSelect = false;
            this._inputSignals10.Name = "_inputSignals10";
            this._inputSignals10.RowHeadersVisible = false;
            this._inputSignals10.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals10.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this._inputSignals10.RowTemplate.Height = 20;
            this._inputSignals10.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals10.ShowCellErrors = false;
            this._inputSignals10.ShowCellToolTips = false;
            this._inputSignals10.ShowEditingIcon = false;
            this._inputSignals10.ShowRowErrors = false;
            this._inputSignals10.Size = new System.Drawing.Size(167, 183);
            this._inputSignals10.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "�";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn8.Width = 24;
            // 
            // dataGridViewComboBoxColumn8
            // 
            this.dataGridViewComboBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn8.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn8.HeaderText = "��������";
            this.dataGridViewComboBoxColumn8.Items.AddRange(new object[] {
            "���",
            "��",
            "������"});
            this.dataGridViewComboBoxColumn8.Name = "dataGridViewComboBoxColumn8";
            // 
            // tabPage11
            // 
            this.tabPage11.Controls.Add(this._inputSignals11);
            this.tabPage11.Location = new System.Drawing.Point(4, 25);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Size = new System.Drawing.Size(173, 190);
            this.tabPage11.TabIndex = 2;
            this.tabPage11.Text = "��7";
            this.tabPage11.UseVisualStyleBackColor = true;
            // 
            // _inputSignals11
            // 
            this._inputSignals11.AllowUserToAddRows = false;
            this._inputSignals11.AllowUserToDeleteRows = false;
            this._inputSignals11.AllowUserToResizeColumns = false;
            this._inputSignals11.AllowUserToResizeRows = false;
            this._inputSignals11.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals11.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals11.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals11.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewComboBoxColumn9});
            this._inputSignals11.Location = new System.Drawing.Point(3, 3);
            this._inputSignals11.MultiSelect = false;
            this._inputSignals11.Name = "_inputSignals11";
            this._inputSignals11.RowHeadersVisible = false;
            this._inputSignals11.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals11.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this._inputSignals11.RowTemplate.Height = 20;
            this._inputSignals11.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals11.ShowCellErrors = false;
            this._inputSignals11.ShowCellToolTips = false;
            this._inputSignals11.ShowEditingIcon = false;
            this._inputSignals11.ShowRowErrors = false;
            this._inputSignals11.Size = new System.Drawing.Size(167, 183);
            this._inputSignals11.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.HeaderText = "�";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn9.Width = 24;
            // 
            // dataGridViewComboBoxColumn9
            // 
            this.dataGridViewComboBoxColumn9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn9.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn9.HeaderText = "��������";
            this.dataGridViewComboBoxColumn9.Items.AddRange(new object[] {
            "���",
            "��",
            "������"});
            this.dataGridViewComboBoxColumn9.Name = "dataGridViewComboBoxColumn9";
            // 
            // tabPage12
            // 
            this.tabPage12.Controls.Add(this._inputSignals12);
            this.tabPage12.Location = new System.Drawing.Point(4, 25);
            this.tabPage12.Name = "tabPage12";
            this.tabPage12.Size = new System.Drawing.Size(173, 190);
            this.tabPage12.TabIndex = 3;
            this.tabPage12.Text = "��8";
            this.tabPage12.UseVisualStyleBackColor = true;
            // 
            // _inputSignals12
            // 
            this._inputSignals12.AllowUserToAddRows = false;
            this._inputSignals12.AllowUserToDeleteRows = false;
            this._inputSignals12.AllowUserToResizeColumns = false;
            this._inputSignals12.AllowUserToResizeRows = false;
            this._inputSignals12.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals12.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals12.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals12.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewComboBoxColumn10});
            this._inputSignals12.Location = new System.Drawing.Point(3, 3);
            this._inputSignals12.MultiSelect = false;
            this._inputSignals12.Name = "_inputSignals12";
            this._inputSignals12.RowHeadersVisible = false;
            this._inputSignals12.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals12.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this._inputSignals12.RowTemplate.Height = 20;
            this._inputSignals12.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals12.ShowCellErrors = false;
            this._inputSignals12.ShowCellToolTips = false;
            this._inputSignals12.ShowEditingIcon = false;
            this._inputSignals12.ShowRowErrors = false;
            this._inputSignals12.Size = new System.Drawing.Size(167, 183);
            this._inputSignals12.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.HeaderText = "�";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn10.Width = 24;
            // 
            // dataGridViewComboBoxColumn10
            // 
            this.dataGridViewComboBoxColumn10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn10.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn10.HeaderText = "��������";
            this.dataGridViewComboBoxColumn10.Items.AddRange(new object[] {
            "���",
            "��",
            "������"});
            this.dataGridViewComboBoxColumn10.Name = "dataGridViewComboBoxColumn10";
            // 
            // groupBox27
            // 
            this.groupBox27.Controls.Add(this.tabControl1);
            this.groupBox27.Location = new System.Drawing.Point(426, 8);
            this.groupBox27.Name = "groupBox27";
            this.groupBox27.Size = new System.Drawing.Size(193, 244);
            this.groupBox27.TabIndex = 32;
            this.groupBox27.TabStop = false;
            this.groupBox27.Text = "���������� ������� �";
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(6, 18);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(181, 219);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this._inputSignals1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(173, 190);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "��1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // _inputSignals1
            // 
            this._inputSignals1.AllowUserToAddRows = false;
            this._inputSignals1.AllowUserToDeleteRows = false;
            this._inputSignals1.AllowUserToResizeColumns = false;
            this._inputSignals1.AllowUserToResizeRows = false;
            this._inputSignals1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals1.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._lsChannelCol,
            this._signalValueCol});
            this._inputSignals1.Location = new System.Drawing.Point(3, 3);
            this._inputSignals1.MultiSelect = false;
            this._inputSignals1.Name = "_inputSignals1";
            this._inputSignals1.RowHeadersVisible = false;
            this._inputSignals1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals1.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this._inputSignals1.RowTemplate.Height = 20;
            this._inputSignals1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals1.ShowCellErrors = false;
            this._inputSignals1.ShowCellToolTips = false;
            this._inputSignals1.ShowEditingIcon = false;
            this._inputSignals1.ShowRowErrors = false;
            this._inputSignals1.Size = new System.Drawing.Size(167, 183);
            this._inputSignals1.TabIndex = 2;
            // 
            // _lsChannelCol
            // 
            this._lsChannelCol.HeaderText = "�";
            this._lsChannelCol.Name = "_lsChannelCol";
            this._lsChannelCol.ReadOnly = true;
            this._lsChannelCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._lsChannelCol.Width = 24;
            // 
            // _signalValueCol
            // 
            this._signalValueCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._signalValueCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._signalValueCol.HeaderText = "��������";
            this._signalValueCol.Items.AddRange(new object[] {
            "���",
            "��",
            "������"});
            this._signalValueCol.Name = "_signalValueCol";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this._inputSignals2);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(173, 190);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "��2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // _inputSignals2
            // 
            this._inputSignals2.AllowUserToAddRows = false;
            this._inputSignals2.AllowUserToDeleteRows = false;
            this._inputSignals2.AllowUserToResizeColumns = false;
            this._inputSignals2.AllowUserToResizeRows = false;
            this._inputSignals2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals2.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewComboBoxColumn1});
            this._inputSignals2.Location = new System.Drawing.Point(3, 3);
            this._inputSignals2.MultiSelect = false;
            this._inputSignals2.Name = "_inputSignals2";
            this._inputSignals2.RowHeadersVisible = false;
            this._inputSignals2.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals2.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this._inputSignals2.RowTemplate.Height = 20;
            this._inputSignals2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals2.ShowCellErrors = false;
            this._inputSignals2.ShowCellToolTips = false;
            this._inputSignals2.ShowEditingIcon = false;
            this._inputSignals2.ShowRowErrors = false;
            this._inputSignals2.Size = new System.Drawing.Size(167, 183);
            this._inputSignals2.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "�";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn2.Width = 24;
            // 
            // dataGridViewComboBoxColumn1
            // 
            this.dataGridViewComboBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn1.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn1.HeaderText = "��������";
            this.dataGridViewComboBoxColumn1.Items.AddRange(new object[] {
            "���",
            "��",
            "������"});
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this._inputSignals3);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(173, 190);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "��3";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // _inputSignals3
            // 
            this._inputSignals3.AllowUserToAddRows = false;
            this._inputSignals3.AllowUserToDeleteRows = false;
            this._inputSignals3.AllowUserToResizeColumns = false;
            this._inputSignals3.AllowUserToResizeRows = false;
            this._inputSignals3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals3.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewComboBoxColumn2});
            this._inputSignals3.Location = new System.Drawing.Point(3, 3);
            this._inputSignals3.MultiSelect = false;
            this._inputSignals3.Name = "_inputSignals3";
            this._inputSignals3.RowHeadersVisible = false;
            this._inputSignals3.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals3.RowsDefaultCellStyle = dataGridViewCellStyle7;
            this._inputSignals3.RowTemplate.Height = 20;
            this._inputSignals3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals3.ShowCellErrors = false;
            this._inputSignals3.ShowCellToolTips = false;
            this._inputSignals3.ShowEditingIcon = false;
            this._inputSignals3.ShowRowErrors = false;
            this._inputSignals3.Size = new System.Drawing.Size(167, 183);
            this._inputSignals3.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "�";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn3.Width = 24;
            // 
            // dataGridViewComboBoxColumn2
            // 
            this.dataGridViewComboBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn2.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn2.HeaderText = "��������";
            this.dataGridViewComboBoxColumn2.Items.AddRange(new object[] {
            "���",
            "��",
            "������"});
            this.dataGridViewComboBoxColumn2.Name = "dataGridViewComboBoxColumn2";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this._inputSignals4);
            this.tabPage4.Location = new System.Drawing.Point(4, 25);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(173, 190);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "��4";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // _inputSignals4
            // 
            this._inputSignals4.AllowUserToAddRows = false;
            this._inputSignals4.AllowUserToDeleteRows = false;
            this._inputSignals4.AllowUserToResizeColumns = false;
            this._inputSignals4.AllowUserToResizeRows = false;
            this._inputSignals4.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals4.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals4.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewComboBoxColumn3});
            this._inputSignals4.Location = new System.Drawing.Point(3, 3);
            this._inputSignals4.MultiSelect = false;
            this._inputSignals4.Name = "_inputSignals4";
            this._inputSignals4.RowHeadersVisible = false;
            this._inputSignals4.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals4.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this._inputSignals4.RowTemplate.Height = 20;
            this._inputSignals4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals4.ShowCellErrors = false;
            this._inputSignals4.ShowCellToolTips = false;
            this._inputSignals4.ShowEditingIcon = false;
            this._inputSignals4.ShowRowErrors = false;
            this._inputSignals4.Size = new System.Drawing.Size(167, 183);
            this._inputSignals4.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "�";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn4.Width = 24;
            // 
            // dataGridViewComboBoxColumn3
            // 
            this.dataGridViewComboBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn3.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn3.HeaderText = "��������";
            this.dataGridViewComboBoxColumn3.Items.AddRange(new object[] {
            "���",
            "��",
            "������"});
            this.dataGridViewComboBoxColumn3.Name = "dataGridViewComboBoxColumn3";
            // 
            // gb3
            // 
            this.gb3.Controls.Add(this.label28);
            this.gb3.Controls.Add(this.oscPercent);
            this.gb3.Location = new System.Drawing.Point(342, 474);
            this.gb3.Name = "gb3";
            this.gb3.Size = new System.Drawing.Size(146, 46);
            this.gb3.TabIndex = 23;
            this.gb3.TabStop = false;
            this.gb3.Text = "����. ���������� ���.";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(122, 23);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(15, 13);
            this.label28.TabIndex = 1;
            this.label28.Text = "%";
            // 
            // oscPercent
            // 
            this.oscPercent.Location = new System.Drawing.Point(6, 20);
            this.oscPercent.Name = "oscPercent";
            this.oscPercent.Size = new System.Drawing.Size(110, 20);
            this.oscPercent.TabIndex = 0;
            this.oscPercent.Tag = "1,100";
            // 
            // gb2
            // 
            this.gb2.Controls.Add(this.oscFix);
            this.gb2.Location = new System.Drawing.Point(171, 474);
            this.gb2.Name = "gb2";
            this.gb2.Size = new System.Drawing.Size(164, 46);
            this.gb2.TabIndex = 22;
            this.gb2.TabStop = false;
            this.gb2.Text = "�������� ���.";
            // 
            // oscFix
            // 
            this.oscFix.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.oscFix.FormattingEnabled = true;
            this.oscFix.Location = new System.Drawing.Point(6, 19);
            this.oscFix.Name = "oscFix";
            this.oscFix.Size = new System.Drawing.Size(147, 21);
            this.oscFix.TabIndex = 0;
            // 
            // gb1
            // 
            this.gb1.Controls.Add(this.oscLength);
            this.gb1.Location = new System.Drawing.Point(8, 474);
            this.gb1.Name = "gb1";
            this.gb1.Size = new System.Drawing.Size(157, 46);
            this.gb1.TabIndex = 21;
            this.gb1.TabStop = false;
            this.gb1.Text = "����. ������� ���.";
            // 
            // oscLength
            // 
            this.oscLength.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.oscLength.FormattingEnabled = true;
            this.oscLength.Location = new System.Drawing.Point(6, 20);
            this.oscLength.Name = "oscLength";
            this.oscLength.Size = new System.Drawing.Size(140, 21);
            this.oscLength.TabIndex = 0;
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this._ompCheckBox);
            this.groupBox18.Controls.Add(this._HUD_Box);
            this.groupBox18.Controls.Add(this.label26);
            this.groupBox18.Controls.Add(this.label27);
            this.groupBox18.Location = new System.Drawing.Point(217, 183);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(203, 69);
            this.groupBox18.TabIndex = 17;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "����������� ����� �����������";
            // 
            // _ompCheckBox
            // 
            this._ompCheckBox.AutoSize = true;
            this._ompCheckBox.Location = new System.Drawing.Point(116, 21);
            this._ompCheckBox.Name = "_ompCheckBox";
            this._ompCheckBox.Size = new System.Drawing.Size(15, 14);
            this._ompCheckBox.TabIndex = 21;
            this._ompCheckBox.UseVisualStyleBackColor = true;
            // 
            // _HUD_Box
            // 
            this._HUD_Box.Location = new System.Drawing.Point(116, 42);
            this._HUD_Box.Name = "_HUD_Box";
            this._HUD_Box.Size = new System.Drawing.Size(81, 20);
            this._HUD_Box.TabIndex = 20;
            this._HUD_Box.Tag = "0,1";
            this._HUD_Box.Text = "0";
            this._HUD_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(6, 45);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(70, 13);
            this.label26.TabIndex = 18;
            this.label26.Text = "��� (��/��)";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(6, 23);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(32, 13);
            this.label27.TabIndex = 16;
            this.label27.Text = "���";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this._manageSignalsSDTU_Combo);
            this.groupBox7.Controls.Add(this.label43);
            this.groupBox7.Controls.Add(this._manageSignalsExternalCombo);
            this.groupBox7.Controls.Add(this.label44);
            this.groupBox7.Controls.Add(this._manageSignalsKeyCombo);
            this.groupBox7.Controls.Add(this.label45);
            this.groupBox7.Controls.Add(this._manageSignalsButtonCombo);
            this.groupBox7.Controls.Add(this.label46);
            this.groupBox7.Location = new System.Drawing.Point(426, 258);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(203, 101);
            this.groupBox7.TabIndex = 16;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "������� ����������";
            // 
            // _manageSignalsSDTU_Combo
            // 
            this._manageSignalsSDTU_Combo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._manageSignalsSDTU_Combo.FormattingEnabled = true;
            this._manageSignalsSDTU_Combo.Location = new System.Drawing.Point(71, 75);
            this._manageSignalsSDTU_Combo.Name = "_manageSignalsSDTU_Combo";
            this._manageSignalsSDTU_Combo.Size = new System.Drawing.Size(126, 21);
            this._manageSignalsSDTU_Combo.TabIndex = 23;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(6, 79);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(54, 13);
            this.label43.TabIndex = 22;
            this.label43.Text = "�� ����";
            // 
            // _manageSignalsExternalCombo
            // 
            this._manageSignalsExternalCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._manageSignalsExternalCombo.FormattingEnabled = true;
            this._manageSignalsExternalCombo.Location = new System.Drawing.Point(71, 56);
            this._manageSignalsExternalCombo.Name = "_manageSignalsExternalCombo";
            this._manageSignalsExternalCombo.Size = new System.Drawing.Size(126, 21);
            this._manageSignalsExternalCombo.TabIndex = 21;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(6, 60);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(52, 13);
            this.label44.TabIndex = 20;
            this.label44.Text = "�������";
            // 
            // _manageSignalsKeyCombo
            // 
            this._manageSignalsKeyCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._manageSignalsKeyCombo.FormattingEnabled = true;
            this._manageSignalsKeyCombo.Location = new System.Drawing.Point(71, 37);
            this._manageSignalsKeyCombo.Name = "_manageSignalsKeyCombo";
            this._manageSignalsKeyCombo.Size = new System.Drawing.Size(126, 21);
            this._manageSignalsKeyCombo.TabIndex = 19;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(6, 41);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(54, 13);
            this.label45.TabIndex = 18;
            this.label45.Text = "�� �����";
            // 
            // _manageSignalsButtonCombo
            // 
            this._manageSignalsButtonCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._manageSignalsButtonCombo.FormattingEnabled = true;
            this._manageSignalsButtonCombo.Location = new System.Drawing.Point(71, 17);
            this._manageSignalsButtonCombo.Name = "_manageSignalsButtonCombo";
            this._manageSignalsButtonCombo.Size = new System.Drawing.Size(126, 21);
            this._manageSignalsButtonCombo.TabIndex = 17;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(6, 22);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(59, 13);
            this.label46.TabIndex = 16;
            this.label46.Text = "�� ������";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.groupBox22);
            this.groupBox6.Controls.Add(this.groupBox21);
            this.groupBox6.Location = new System.Drawing.Point(217, 258);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(203, 201);
            this.groupBox6.TabIndex = 15;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "���� �������������";
            // 
            // groupBox22
            // 
            this.groupBox22.Controls.Add(this.label11);
            this.groupBox22.Controls.Add(this._releDispepairBox);
            this.groupBox22.Location = new System.Drawing.Point(6, 12);
            this.groupBox22.Name = "groupBox22";
            this.groupBox22.Size = new System.Drawing.Size(190, 38);
            this.groupBox22.TabIndex = 8;
            this.groupBox22.TabStop = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(13, 15);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(110, 13);
            this.label11.TabIndex = 19;
            this.label11.Text = "����� ��������, ��";
            // 
            // _releDispepairBox
            // 
            this._releDispepairBox.Location = new System.Drawing.Point(129, 11);
            this._releDispepairBox.Name = "_releDispepairBox";
            this._releDispepairBox.Size = new System.Drawing.Size(55, 20);
            this._releDispepairBox.TabIndex = 9;
            this._releDispepairBox.Tag = "0,3000000";
            this._releDispepairBox.Text = "0";
            this._releDispepairBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox21
            // 
            this.groupBox21.Controls.Add(this._dispepairCheckList);
            this.groupBox21.Location = new System.Drawing.Point(6, 50);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(190, 145);
            this.groupBox21.TabIndex = 8;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "������ �������������";
            // 
            // _dispepairCheckList
            // 
            this._dispepairCheckList.CheckOnClick = true;
            this._dispepairCheckList.FormattingEnabled = true;
            this._dispepairCheckList.Items.AddRange(new object[] {
            "������������� 1",
            "������������� 2",
            "������������� 3",
            "������������� 4",
            "������������� 5"});
            this._dispepairCheckList.Location = new System.Drawing.Point(9, 14);
            this._dispepairCheckList.Name = "_dispepairCheckList";
            this._dispepairCheckList.Size = new System.Drawing.Size(175, 109);
            this._dispepairCheckList.TabIndex = 5;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label19);
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Controls.Add(this._switcherDurationBox);
            this.groupBox4.Controls.Add(this._switcherTokBox);
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Controls.Add(this.label18);
            this.groupBox4.Controls.Add(this._switcherTimeBox);
            this.groupBox4.Controls.Add(this._switcherImpulseBox);
            this.groupBox4.Controls.Add(this._switcherBlockCombo);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this._switcherErrorCombo);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this._switcherStateOnCombo);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this._switcherStateOffCombo);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Location = new System.Drawing.Point(217, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(203, 171);
            this.groupBox4.TabIndex = 13;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "�����������";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(0, 148);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(112, 13);
            this.label19.TabIndex = 23;
            this.label19.Text = "����. ���������, ��";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(0, 110);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(74, 13);
            this.label17.TabIndex = 19;
            this.label17.Text = "��� ����, I�";
            // 
            // _switcherDurationBox
            // 
            this._switcherDurationBox.Location = new System.Drawing.Point(125, 147);
            this._switcherDurationBox.Name = "_switcherDurationBox";
            this._switcherDurationBox.Size = new System.Drawing.Size(71, 20);
            this._switcherDurationBox.TabIndex = 22;
            this._switcherDurationBox.Tag = "0,3000000";
            this._switcherDurationBox.Text = "0";
            this._switcherDurationBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _switcherTokBox
            // 
            this._switcherTokBox.Location = new System.Drawing.Point(125, 107);
            this._switcherTokBox.Name = "_switcherTokBox";
            this._switcherTokBox.Size = new System.Drawing.Size(71, 20);
            this._switcherTokBox.TabIndex = 18;
            this._switcherTokBox.Tag = "0,40";
            this._switcherTokBox.Text = "0";
            this._switcherTokBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(0, 129);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(90, 13);
            this.label20.TabIndex = 21;
            this.label20.Text = "������� ��, ��";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(0, 91);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(93, 13);
            this.label18.TabIndex = 17;
            this.label18.Text = "����� ����, ��";
            // 
            // _switcherTimeBox
            // 
            this._switcherTimeBox.Location = new System.Drawing.Point(125, 87);
            this._switcherTimeBox.Name = "_switcherTimeBox";
            this._switcherTimeBox.Size = new System.Drawing.Size(71, 20);
            this._switcherTimeBox.TabIndex = 16;
            this._switcherTimeBox.Tag = "0,3000000";
            this._switcherTimeBox.Text = "0";
            this._switcherTimeBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _switcherImpulseBox
            // 
            this._switcherImpulseBox.Location = new System.Drawing.Point(125, 127);
            this._switcherImpulseBox.Name = "_switcherImpulseBox";
            this._switcherImpulseBox.Size = new System.Drawing.Size(71, 20);
            this._switcherImpulseBox.TabIndex = 20;
            this._switcherImpulseBox.Tag = "0,3000000";
            this._switcherImpulseBox.Text = "0";
            this._switcherImpulseBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _switcherBlockCombo
            // 
            this._switcherBlockCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switcherBlockCombo.FormattingEnabled = true;
            this._switcherBlockCombo.Location = new System.Drawing.Point(125, 68);
            this._switcherBlockCombo.Name = "_switcherBlockCombo";
            this._switcherBlockCombo.Size = new System.Drawing.Size(71, 21);
            this._switcherBlockCombo.TabIndex = 15;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(0, 72);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(68, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "����������";
            // 
            // _switcherErrorCombo
            // 
            this._switcherErrorCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switcherErrorCombo.FormattingEnabled = true;
            this._switcherErrorCombo.Location = new System.Drawing.Point(125, 49);
            this._switcherErrorCombo.Name = "_switcherErrorCombo";
            this._switcherErrorCombo.Size = new System.Drawing.Size(71, 21);
            this._switcherErrorCombo.TabIndex = 13;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(0, 53);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(86, 13);
            this.label14.TabIndex = 12;
            this.label14.Text = "�������������";
            // 
            // _switcherStateOnCombo
            // 
            this._switcherStateOnCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switcherStateOnCombo.FormattingEnabled = true;
            this._switcherStateOnCombo.Location = new System.Drawing.Point(125, 30);
            this._switcherStateOnCombo.Name = "_switcherStateOnCombo";
            this._switcherStateOnCombo.Size = new System.Drawing.Size(71, 21);
            this._switcherStateOnCombo.TabIndex = 11;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(0, 34);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(113, 13);
            this.label15.TabIndex = 10;
            this.label15.Text = "��������� ��������";
            // 
            // _switcherStateOffCombo
            // 
            this._switcherStateOffCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switcherStateOffCombo.FormattingEnabled = true;
            this._switcherStateOffCombo.Location = new System.Drawing.Point(125, 11);
            this._switcherStateOffCombo.Name = "_switcherStateOffCombo";
            this._switcherStateOffCombo.Size = new System.Drawing.Size(71, 21);
            this._switcherStateOffCombo.TabIndex = 9;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(0, 15);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(118, 13);
            this.label16.TabIndex = 8;
            this.label16.Text = "��������� ���������";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.disableComboBox);
            this.groupBox3.Controls.Add(this.disableLabel);
            this.groupBox3.Controls.Add(this._constraintGroupCombo);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this._signalizationCombo);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this._extOnCombo);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this._extOffCombo);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this._keyOnCombo);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this._keyOffCombo);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Location = new System.Drawing.Point(8, 281);
            this.groupBox3.MinimumSize = new System.Drawing.Size(203, 153);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(203, 153);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "������� �������";
            // 
            // disableComboBox
            // 
            this.disableComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.disableComboBox.FormattingEnabled = true;
            this.disableComboBox.Location = new System.Drawing.Point(124, 127);
            this.disableComboBox.Name = "disableComboBox";
            this.disableComboBox.Size = new System.Drawing.Size(71, 21);
            this.disableComboBox.TabIndex = 13;
            this.disableComboBox.Visible = false;
            // 
            // disableLabel
            // 
            this.disableLabel.AutoSize = true;
            this.disableLabel.Location = new System.Drawing.Point(7, 130);
            this.disableLabel.Name = "disableLabel";
            this.disableLabel.Size = new System.Drawing.Size(102, 13);
            this.disableLabel.TabIndex = 12;
            this.disableLabel.Text = "���������� ����";
            this.disableLabel.Visible = false;
            // 
            // _constraintGroupCombo
            // 
            this._constraintGroupCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._constraintGroupCombo.FormattingEnabled = true;
            this._constraintGroupCombo.Location = new System.Drawing.Point(124, 107);
            this._constraintGroupCombo.Name = "_constraintGroupCombo";
            this._constraintGroupCombo.Size = new System.Drawing.Size(71, 21);
            this._constraintGroupCombo.TabIndex = 11;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(7, 110);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(85, 13);
            this.label12.TabIndex = 10;
            this.label12.Text = "������ �������";
            // 
            // _signalizationCombo
            // 
            this._signalizationCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._signalizationCombo.FormattingEnabled = true;
            this._signalizationCombo.Location = new System.Drawing.Point(124, 88);
            this._signalizationCombo.Name = "_signalizationCombo";
            this._signalizationCombo.Size = new System.Drawing.Size(71, 21);
            this._signalizationCombo.TabIndex = 9;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 91);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(112, 13);
            this.label13.TabIndex = 8;
            this.label13.Text = "����� ������������";
            // 
            // _extOnCombo
            // 
            this._extOnCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._extOnCombo.FormattingEnabled = true;
            this._extOnCombo.Location = new System.Drawing.Point(124, 69);
            this._extOnCombo.Name = "_extOnCombo";
            this._extOnCombo.Size = new System.Drawing.Size(71, 21);
            this._extOnCombo.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 53);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(93, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "����. ���������";
            // 
            // _extOffCombo
            // 
            this._extOffCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._extOffCombo.FormattingEnabled = true;
            this._extOffCombo.Location = new System.Drawing.Point(124, 50);
            this._extOffCombo.Name = "_extOffCombo";
            this._extOffCombo.Size = new System.Drawing.Size(71, 21);
            this._extOffCombo.TabIndex = 5;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 72);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(88, 13);
            this.label9.TabIndex = 4;
            this.label9.Text = "����. ��������";
            // 
            // _keyOnCombo
            // 
            this._keyOnCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._keyOnCombo.FormattingEnabled = true;
            this._keyOnCombo.Location = new System.Drawing.Point(124, 31);
            this._keyOnCombo.Name = "_keyOnCombo";
            this._keyOnCombo.Size = new System.Drawing.Size(71, 21);
            this._keyOnCombo.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 15);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(89, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "���� ���������";
            // 
            // _keyOffCombo
            // 
            this._keyOffCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._keyOffCombo.FormattingEnabled = true;
            this._keyOffCombo.Location = new System.Drawing.Point(124, 12);
            this._keyOffCombo.Name = "_keyOffCombo";
            this._keyOffCombo.Size = new System.Drawing.Size(71, 21);
            this._keyOffCombo.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 34);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "���� ��������";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._TT_typeCombo);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this._maxTok_Box);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this._TTNP_Box);
            this.groupBox1.Controls.Add(this._TT_Box);
            this.groupBox1.Location = new System.Drawing.Point(8, 6);
            this.groupBox1.MinimumSize = new System.Drawing.Size(203, 95);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(203, 95);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "��������� ����";
            // 
            // _TT_typeCombo
            // 
            this._TT_typeCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._TT_typeCombo.FormattingEnabled = true;
            this._TT_typeCombo.Location = new System.Drawing.Point(58, 71);
            this._TT_typeCombo.Name = "_TT_typeCombo";
            this._TT_typeCombo.Size = new System.Drawing.Size(139, 21);
            this._TT_typeCombo.TabIndex = 14;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(8, 74);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(43, 13);
            this.label25.TabIndex = 13;
            this.label25.Text = "��� �T";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(121, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "����. ��� ��������, I�";
            // 
            // _maxTok_Box
            // 
            this._maxTok_Box.Location = new System.Drawing.Point(148, 52);
            this._maxTok_Box.Name = "_maxTok_Box";
            this._maxTok_Box.Size = new System.Drawing.Size(47, 20);
            this._maxTok_Box.TabIndex = 5;
            this._maxTok_Box.Tag = "0,40";
            this._maxTok_Box.Text = "0";
            this._maxTok_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "��������� ��� ��, A";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(130, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "��������� ��� ����, A";
            // 
            // _TTNP_Box
            // 
            this._TTNP_Box.Location = new System.Drawing.Point(148, 32);
            this._TTNP_Box.Name = "_TTNP_Box";
            this._TTNP_Box.Size = new System.Drawing.Size(47, 20);
            this._TTNP_Box.TabIndex = 2;
            this._TTNP_Box.Tag = "0,1000";
            this._TTNP_Box.Text = "0";
            this._TTNP_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _outputSignalsPage
            // 
            this._outputSignalsPage.Controls.Add(this.VLSTabControl);
            this._outputSignalsPage.Controls.Add(this.groupBox9);
            this._outputSignalsPage.Location = new System.Drawing.Point(4, 22);
            this._outputSignalsPage.Name = "_outputSignalsPage";
            this._outputSignalsPage.Padding = new System.Windows.Forms.Padding(3);
            this._outputSignalsPage.Size = new System.Drawing.Size(899, 645);
            this._outputSignalsPage.TabIndex = 1;
            this._outputSignalsPage.Text = "�������� �������";
            this._outputSignalsPage.UseVisualStyleBackColor = true;
            // 
            // VLSTabControl
            // 
            this.VLSTabControl.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.VLSTabControl.Controls.Add(this.VLS1);
            this.VLSTabControl.Controls.Add(this.VLS2);
            this.VLSTabControl.Controls.Add(this.VLS3);
            this.VLSTabControl.Controls.Add(this.VLS4);
            this.VLSTabControl.Controls.Add(this.VLS5);
            this.VLSTabControl.Controls.Add(this.VLS6);
            this.VLSTabControl.Controls.Add(this.VLS7);
            this.VLSTabControl.Controls.Add(this.VLS8);
            this.VLSTabControl.Location = new System.Drawing.Point(441, 18);
            this.VLSTabControl.Multiline = true;
            this.VLSTabControl.Name = "VLSTabControl";
            this.VLSTabControl.SelectedIndex = 0;
            this.VLSTabControl.Size = new System.Drawing.Size(186, 467);
            this.VLSTabControl.TabIndex = 31;
            // 
            // VLS1
            // 
            this.VLS1.Controls.Add(this.VLScheckedListBox1);
            this.VLS1.Location = new System.Drawing.Point(4, 49);
            this.VLS1.Name = "VLS1";
            this.VLS1.Padding = new System.Windows.Forms.Padding(3);
            this.VLS1.Size = new System.Drawing.Size(178, 414);
            this.VLS1.TabIndex = 0;
            this.VLS1.Text = "��� 1";
            this.VLS1.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox1
            // 
            this.VLScheckedListBox1.CheckOnClick = true;
            this.VLScheckedListBox1.FormattingEnabled = true;
            this.VLScheckedListBox1.Location = new System.Drawing.Point(4, 3);
            this.VLScheckedListBox1.Name = "VLScheckedListBox1";
            this.VLScheckedListBox1.ScrollAlwaysVisible = true;
            this.VLScheckedListBox1.Size = new System.Drawing.Size(174, 394);
            this.VLScheckedListBox1.TabIndex = 6;
            // 
            // VLS2
            // 
            this.VLS2.Controls.Add(this.VLScheckedListBox2);
            this.VLS2.Location = new System.Drawing.Point(4, 49);
            this.VLS2.Name = "VLS2";
            this.VLS2.Padding = new System.Windows.Forms.Padding(3);
            this.VLS2.Size = new System.Drawing.Size(178, 414);
            this.VLS2.TabIndex = 1;
            this.VLS2.Text = "��� 2";
            this.VLS2.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox2
            // 
            this.VLScheckedListBox2.CheckOnClick = true;
            this.VLScheckedListBox2.FormattingEnabled = true;
            this.VLScheckedListBox2.Location = new System.Drawing.Point(4, 3);
            this.VLScheckedListBox2.Name = "VLScheckedListBox2";
            this.VLScheckedListBox2.ScrollAlwaysVisible = true;
            this.VLScheckedListBox2.Size = new System.Drawing.Size(174, 394);
            this.VLScheckedListBox2.TabIndex = 6;
            // 
            // VLS3
            // 
            this.VLS3.Controls.Add(this.VLScheckedListBox3);
            this.VLS3.Location = new System.Drawing.Point(4, 49);
            this.VLS3.Name = "VLS3";
            this.VLS3.Size = new System.Drawing.Size(178, 414);
            this.VLS3.TabIndex = 2;
            this.VLS3.Text = "��� 3";
            this.VLS3.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox3
            // 
            this.VLScheckedListBox3.CheckOnClick = true;
            this.VLScheckedListBox3.FormattingEnabled = true;
            this.VLScheckedListBox3.Location = new System.Drawing.Point(4, 3);
            this.VLScheckedListBox3.Name = "VLScheckedListBox3";
            this.VLScheckedListBox3.ScrollAlwaysVisible = true;
            this.VLScheckedListBox3.Size = new System.Drawing.Size(174, 394);
            this.VLScheckedListBox3.TabIndex = 6;
            // 
            // VLS4
            // 
            this.VLS4.Controls.Add(this.VLScheckedListBox4);
            this.VLS4.Location = new System.Drawing.Point(4, 49);
            this.VLS4.Name = "VLS4";
            this.VLS4.Size = new System.Drawing.Size(178, 414);
            this.VLS4.TabIndex = 3;
            this.VLS4.Text = "��� 4";
            this.VLS4.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox4
            // 
            this.VLScheckedListBox4.CheckOnClick = true;
            this.VLScheckedListBox4.FormattingEnabled = true;
            this.VLScheckedListBox4.Location = new System.Drawing.Point(4, 3);
            this.VLScheckedListBox4.Name = "VLScheckedListBox4";
            this.VLScheckedListBox4.ScrollAlwaysVisible = true;
            this.VLScheckedListBox4.Size = new System.Drawing.Size(174, 394);
            this.VLScheckedListBox4.TabIndex = 6;
            // 
            // VLS5
            // 
            this.VLS5.Controls.Add(this.VLScheckedListBox5);
            this.VLS5.Location = new System.Drawing.Point(4, 49);
            this.VLS5.Name = "VLS5";
            this.VLS5.Size = new System.Drawing.Size(178, 414);
            this.VLS5.TabIndex = 4;
            this.VLS5.Text = "��� 5";
            this.VLS5.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox5
            // 
            this.VLScheckedListBox5.CheckOnClick = true;
            this.VLScheckedListBox5.FormattingEnabled = true;
            this.VLScheckedListBox5.Location = new System.Drawing.Point(4, 3);
            this.VLScheckedListBox5.Name = "VLScheckedListBox5";
            this.VLScheckedListBox5.ScrollAlwaysVisible = true;
            this.VLScheckedListBox5.Size = new System.Drawing.Size(174, 394);
            this.VLScheckedListBox5.TabIndex = 6;
            // 
            // VLS6
            // 
            this.VLS6.Controls.Add(this.VLScheckedListBox6);
            this.VLS6.Location = new System.Drawing.Point(4, 49);
            this.VLS6.Name = "VLS6";
            this.VLS6.Size = new System.Drawing.Size(178, 414);
            this.VLS6.TabIndex = 5;
            this.VLS6.Text = "��� 6";
            this.VLS6.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox6
            // 
            this.VLScheckedListBox6.CheckOnClick = true;
            this.VLScheckedListBox6.FormattingEnabled = true;
            this.VLScheckedListBox6.Location = new System.Drawing.Point(4, 3);
            this.VLScheckedListBox6.Name = "VLScheckedListBox6";
            this.VLScheckedListBox6.ScrollAlwaysVisible = true;
            this.VLScheckedListBox6.Size = new System.Drawing.Size(174, 394);
            this.VLScheckedListBox6.TabIndex = 6;
            // 
            // VLS7
            // 
            this.VLS7.Controls.Add(this.VLScheckedListBox7);
            this.VLS7.Location = new System.Drawing.Point(4, 49);
            this.VLS7.Name = "VLS7";
            this.VLS7.Size = new System.Drawing.Size(178, 414);
            this.VLS7.TabIndex = 6;
            this.VLS7.Text = "��� 7";
            this.VLS7.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox7
            // 
            this.VLScheckedListBox7.CheckOnClick = true;
            this.VLScheckedListBox7.FormattingEnabled = true;
            this.VLScheckedListBox7.Location = new System.Drawing.Point(4, 3);
            this.VLScheckedListBox7.Name = "VLScheckedListBox7";
            this.VLScheckedListBox7.ScrollAlwaysVisible = true;
            this.VLScheckedListBox7.Size = new System.Drawing.Size(174, 394);
            this.VLScheckedListBox7.TabIndex = 6;
            // 
            // VLS8
            // 
            this.VLS8.Controls.Add(this.VLScheckedListBox8);
            this.VLS8.Location = new System.Drawing.Point(4, 49);
            this.VLS8.Name = "VLS8";
            this.VLS8.Size = new System.Drawing.Size(178, 414);
            this.VLS8.TabIndex = 7;
            this.VLS8.Text = "��� 8";
            this.VLS8.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox8
            // 
            this.VLScheckedListBox8.CheckOnClick = true;
            this.VLScheckedListBox8.FormattingEnabled = true;
            this.VLScheckedListBox8.Location = new System.Drawing.Point(4, 3);
            this.VLScheckedListBox8.Name = "VLScheckedListBox8";
            this.VLScheckedListBox8.ScrollAlwaysVisible = true;
            this.VLScheckedListBox8.Size = new System.Drawing.Size(174, 394);
            this.VLScheckedListBox8.TabIndex = 6;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this._outputIndicatorsGrid);
            this.groupBox9.Location = new System.Drawing.Point(8, 18);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(424, 250);
            this.groupBox9.TabIndex = 2;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "����������";
            // 
            // _outputIndicatorsGrid
            // 
            this._outputIndicatorsGrid.AllowUserToAddRows = false;
            this._outputIndicatorsGrid.AllowUserToDeleteRows = false;
            this._outputIndicatorsGrid.AllowUserToResizeColumns = false;
            this._outputIndicatorsGrid.AllowUserToResizeRows = false;
            this._outputIndicatorsGrid.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._outputIndicatorsGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this._outputIndicatorsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._outputIndicatorsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._outIndNumberCol,
            this._outIndTypeCol,
            this._outIndSignalCol,
            this._outIndResetCol,
            this._outIndAlarmCol,
            this._outIndSystemCol});
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._outputIndicatorsGrid.DefaultCellStyle = dataGridViewCellStyle10;
            this._outputIndicatorsGrid.Location = new System.Drawing.Point(8, 14);
            this._outputIndicatorsGrid.Name = "_outputIndicatorsGrid";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._outputIndicatorsGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this._outputIndicatorsGrid.RowHeadersVisible = false;
            this._outputIndicatorsGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._outputIndicatorsGrid.RowsDefaultCellStyle = dataGridViewCellStyle12;
            this._outputIndicatorsGrid.RowTemplate.Height = 24;
            this._outputIndicatorsGrid.ShowCellErrors = false;
            this._outputIndicatorsGrid.ShowRowErrors = false;
            this._outputIndicatorsGrid.Size = new System.Drawing.Size(410, 230);
            this._outputIndicatorsGrid.TabIndex = 0;
            // 
            // _outIndNumberCol
            // 
            this._outIndNumberCol.HeaderText = "�";
            this._outIndNumberCol.Name = "_outIndNumberCol";
            this._outIndNumberCol.ReadOnly = true;
            this._outIndNumberCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._outIndNumberCol.Width = 20;
            // 
            // _outIndTypeCol
            // 
            this._outIndTypeCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._outIndTypeCol.HeaderText = "���";
            this._outIndTypeCol.Items.AddRange(new object[] {
            "�����������",
            "�������",
            "XXXXX"});
            this._outIndTypeCol.Name = "_outIndTypeCol";
            this._outIndTypeCol.Width = 120;
            // 
            // _outIndSignalCol
            // 
            this._outIndSignalCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._outIndSignalCol.HeaderText = "������";
            this._outIndSignalCol.Name = "_outIndSignalCol";
            this._outIndSignalCol.Width = 140;
            // 
            // _outIndResetCol
            // 
            this._outIndResetCol.HeaderText = "����� ���.";
            this._outIndResetCol.Name = "_outIndResetCol";
            this._outIndResetCol.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._outIndResetCol.Width = 40;
            // 
            // _outIndAlarmCol
            // 
            this._outIndAlarmCol.HeaderText = "����� ��";
            this._outIndAlarmCol.Name = "_outIndAlarmCol";
            this._outIndAlarmCol.Width = 40;
            // 
            // _outIndSystemCol
            // 
            this._outIndSystemCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._outIndSystemCol.HeaderText = "����� ��";
            this._outIndSystemCol.Name = "_outIndSystemCol";
            // 
            // _externalDefensePage
            // 
            this._externalDefensePage.Controls.Add(this._externalDefenseGrid);
            this._externalDefensePage.Location = new System.Drawing.Point(4, 22);
            this._externalDefensePage.Name = "_externalDefensePage";
            this._externalDefensePage.Size = new System.Drawing.Size(899, 645);
            this._externalDefensePage.TabIndex = 2;
            this._externalDefensePage.Text = "������� ������";
            this._externalDefensePage.UseVisualStyleBackColor = true;
            // 
            // _externalDefenseGrid
            // 
            this._externalDefenseGrid.AllowUserToAddRows = false;
            this._externalDefenseGrid.AllowUserToDeleteRows = false;
            this._externalDefenseGrid.AllowUserToResizeColumns = false;
            this._externalDefenseGrid.AllowUserToResizeRows = false;
            this._externalDefenseGrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._externalDefenseGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._externalDefenseGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._externalDefenseGrid.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._externalDefenseGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this._externalDefenseGrid.ColumnHeadersHeight = 30;
            this._externalDefenseGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._externalDefenseGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._extDefNumberCol,
            this._exDefModeCol,
            this._exDefBlockingCol,
            this._exDefWorkingCol,
            this._exDefWorkingTimeCol,
            this._exDefReturnCol,
            this._exDefAPVreturnCol,
            this._exDefReturnNumberCol,
            this._exDefReturnTimeCol,
            this._exDefUROVcol,
            this._exDefAPVcol,
            this._exDefAVRcol,
            this._exDefOSCcol,
            this._exDefResetcol});
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDefenseGrid.DefaultCellStyle = dataGridViewCellStyle15;
            this._externalDefenseGrid.Location = new System.Drawing.Point(3, 3);
            this._externalDefenseGrid.Name = "_externalDefenseGrid";
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._externalDefenseGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle16;
            this._externalDefenseGrid.RowHeadersVisible = false;
            this._externalDefenseGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDefenseGrid.RowsDefaultCellStyle = dataGridViewCellStyle17;
            this._externalDefenseGrid.RowTemplate.Height = 24;
            this._externalDefenseGrid.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this._externalDefenseGrid.Size = new System.Drawing.Size(893, 230);
            this._externalDefenseGrid.TabIndex = 0;
            // 
            // _extDefNumberCol
            // 
            this._extDefNumberCol.Frozen = true;
            this._extDefNumberCol.HeaderText = "�";
            this._extDefNumberCol.Name = "_extDefNumberCol";
            this._extDefNumberCol.ReadOnly = true;
            this._extDefNumberCol.Width = 43;
            // 
            // _exDefModeCol
            // 
            this._exDefModeCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._exDefModeCol.HeaderText = "�����";
            this._exDefModeCol.Name = "_exDefModeCol";
            this._exDefModeCol.Width = 48;
            // 
            // _exDefBlockingCol
            // 
            this._exDefBlockingCol.HeaderText = "����������";
            this._exDefBlockingCol.Name = "_exDefBlockingCol";
            this._exDefBlockingCol.Width = 74;
            // 
            // _exDefWorkingCol
            // 
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this._exDefWorkingCol.DefaultCellStyle = dataGridViewCellStyle14;
            this._exDefWorkingCol.HeaderText = "������������";
            this._exDefWorkingCol.Name = "_exDefWorkingCol";
            this._exDefWorkingCol.Width = 87;
            // 
            // _exDefWorkingTimeCol
            // 
            this._exDefWorkingTimeCol.HeaderText = "����� ������., ��";
            this._exDefWorkingTimeCol.Name = "_exDefWorkingTimeCol";
            this._exDefWorkingTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._exDefWorkingTimeCol.Width = 107;
            // 
            // _exDefReturnCol
            // 
            this._exDefReturnCol.HeaderText = "����.";
            this._exDefReturnCol.Name = "_exDefReturnCol";
            this._exDefReturnCol.Width = 41;
            // 
            // _exDefAPVreturnCol
            // 
            this._exDefAPVreturnCol.HeaderText = "��� ��";
            this._exDefAPVreturnCol.Name = "_exDefAPVreturnCol";
            this._exDefAPVreturnCol.Width = 52;
            // 
            // _exDefReturnNumberCol
            // 
            this._exDefReturnNumberCol.HeaderText = "���� ��������";
            this._exDefReturnNumberCol.Name = "_exDefReturnNumberCol";
            this._exDefReturnNumberCol.Width = 87;
            // 
            // _exDefReturnTimeCol
            // 
            this._exDefReturnTimeCol.HeaderText = "����� ��������, ��";
            this._exDefReturnTimeCol.Name = "_exDefReturnTimeCol";
            this._exDefReturnTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._exDefReturnTimeCol.Width = 116;
            // 
            // _exDefUROVcol
            // 
            this._exDefUROVcol.HeaderText = "����";
            this._exDefUROVcol.Name = "_exDefUROVcol";
            this._exDefUROVcol.Width = 43;
            // 
            // _exDefAPVcol
            // 
            this._exDefAPVcol.HeaderText = "���";
            this._exDefAPVcol.Name = "_exDefAPVcol";
            this._exDefAPVcol.Width = 35;
            // 
            // _exDefAVRcol
            // 
            this._exDefAVRcol.HeaderText = "���";
            this._exDefAVRcol.Name = "_exDefAVRcol";
            this._exDefAVRcol.Width = 34;
            // 
            // _exDefOSCcol
            // 
            this._exDefOSCcol.HeaderText = "���.";
            this._exDefOSCcol.Name = "_exDefOSCcol";
            this._exDefOSCcol.Width = 36;
            // 
            // _exDefResetcol
            // 
            this._exDefResetcol.HeaderText = "�����";
            this._exDefResetcol.Name = "_exDefResetcol";
            this._exDefResetcol.Width = 44;
            // 
            // _automaticPage
            // 
            this._automaticPage.Controls.Add(this.groupBox2);
            this._automaticPage.Controls.Add(this.groupBox13);
            this._automaticPage.Controls.Add(this.groupBox14);
            this._automaticPage.Location = new System.Drawing.Point(4, 22);
            this._automaticPage.Name = "_automaticPage";
            this._automaticPage.Size = new System.Drawing.Size(899, 645);
            this._automaticPage.TabIndex = 3;
            this._automaticPage.Text = "����������";
            this._automaticPage.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.avr_mode);
            this.groupBox2.Controls.Add(this.label34);
            this.groupBox2.Controls.Add(this._umax2);
            this.groupBox2.Controls.Add(this.label35);
            this.groupBox2.Controls.Add(this._umin2);
            this.groupBox2.Controls.Add(this.label32);
            this.groupBox2.Controls.Add(this._umax1);
            this.groupBox2.Controls.Add(this.label33);
            this.groupBox2.Controls.Add(this._umin1);
            this.groupBox2.Controls.Add(this.label58);
            this.groupBox2.Controls.Add(this.label30);
            this.groupBox2.Controls.Add(this.avr_disconnection);
            this.groupBox2.Controls.Add(this.label59);
            this.groupBox2.Controls.Add(this.label61);
            this.groupBox2.Controls.Add(this.label31);
            this.groupBox2.Controls.Add(this.avr_time_return);
            this.groupBox2.Controls.Add(this._side2);
            this.groupBox2.Controls.Add(this._side1);
            this.groupBox2.Controls.Add(this.avr_blocking);
            this.groupBox2.Controls.Add(this.label66);
            this.groupBox2.Controls.Add(this.avr_reset_blocking);
            this.groupBox2.Controls.Add(this.label65);
            this.groupBox2.Location = new System.Drawing.Point(265, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(256, 281);
            this.groupBox2.TabIndex = 25;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "������������ ���";
            // 
            // avr_mode
            // 
            this.avr_mode.AutoSize = true;
            this.avr_mode.Location = new System.Drawing.Point(158, 32);
            this.avr_mode.Name = "avr_mode";
            this.avr_mode.Size = new System.Drawing.Size(15, 14);
            this.avr_mode.TabIndex = 51;
            this.avr_mode.UseVisualStyleBackColor = true;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(15, 251);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(53, 13);
            this.label34.TabIndex = 50;
            this.label34.Text = "Umax2, �";
            // 
            // _umax2
            // 
            this._umax2.Location = new System.Drawing.Point(158, 249);
            this._umax2.Name = "_umax2";
            this._umax2.Size = new System.Drawing.Size(85, 20);
            this._umax2.TabIndex = 48;
            this._umax2.Tag = "0,3000000";
            this._umax2.Text = "0";
            this._umax2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(15, 231);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(50, 13);
            this.label35.TabIndex = 49;
            this.label35.Text = "Umin2, �";
            // 
            // _umin2
            // 
            this._umin2.Location = new System.Drawing.Point(158, 229);
            this._umin2.Name = "_umin2";
            this._umin2.Size = new System.Drawing.Size(85, 20);
            this._umin2.TabIndex = 47;
            this._umin2.Tag = "0,3000000";
            this._umin2.Text = "0";
            this._umin2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(15, 212);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(53, 13);
            this.label32.TabIndex = 46;
            this.label32.Text = "Umax1, �";
            // 
            // _umax1
            // 
            this._umax1.Location = new System.Drawing.Point(158, 210);
            this._umax1.Name = "_umax1";
            this._umax1.Size = new System.Drawing.Size(85, 20);
            this._umax1.TabIndex = 44;
            this._umax1.Tag = "0,3000000";
            this._umax1.Text = "0";
            this._umax1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(15, 192);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(50, 13);
            this.label33.TabIndex = 45;
            this.label33.Text = "Umin1, �";
            // 
            // _umin1
            // 
            this._umin1.Location = new System.Drawing.Point(158, 190);
            this._umin1.Name = "_umin1";
            this._umin1.Size = new System.Drawing.Size(85, 20);
            this._umin1.TabIndex = 43;
            this._umin1.Tag = "0,3000000";
            this._umin1.Text = "0";
            this._umin1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(15, 173);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(120, 13);
            this.label58.TabIndex = 42;
            this.label58.Text = "����� ����������, ��";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(13, 59);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(58, 13);
            this.label30.TabIndex = 39;
            this.label30.Text = "������� 1";
            // 
            // avr_disconnection
            // 
            this.avr_disconnection.Location = new System.Drawing.Point(158, 171);
            this.avr_disconnection.Name = "avr_disconnection";
            this.avr_disconnection.Size = new System.Drawing.Size(85, 20);
            this.avr_disconnection.TabIndex = 31;
            this.avr_disconnection.Tag = "0,3000000";
            this.avr_disconnection.Text = "0";
            this.avr_disconnection.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(13, 32);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(80, 13);
            this.label59.TabIndex = 35;
            this.label59.Text = "������������";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(15, 153);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(113, 13);
            this.label61.TabIndex = 41;
            this.label61.Text = "����� ��������, ��";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(13, 82);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(58, 13);
            this.label31.TabIndex = 40;
            this.label31.Text = "������� 2";
            // 
            // avr_time_return
            // 
            this.avr_time_return.Location = new System.Drawing.Point(158, 151);
            this.avr_time_return.Name = "avr_time_return";
            this.avr_time_return.Size = new System.Drawing.Size(85, 20);
            this.avr_time_return.TabIndex = 30;
            this.avr_time_return.Tag = "0,3000000";
            this.avr_time_return.Text = "0";
            this.avr_time_return.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _side2
            // 
            this._side2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._side2.FormattingEnabled = true;
            this._side2.Location = new System.Drawing.Point(156, 76);
            this._side2.Name = "_side2";
            this._side2.Size = new System.Drawing.Size(87, 21);
            this._side2.TabIndex = 38;
            // 
            // _side1
            // 
            this._side1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._side1.FormattingEnabled = true;
            this._side1.Location = new System.Drawing.Point(156, 52);
            this._side1.Name = "_side1";
            this._side1.Size = new System.Drawing.Size(87, 21);
            this._side1.TabIndex = 37;
            // 
            // avr_blocking
            // 
            this.avr_blocking.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.avr_blocking.FormattingEnabled = true;
            this.avr_blocking.Location = new System.Drawing.Point(156, 100);
            this.avr_blocking.Name = "avr_blocking";
            this.avr_blocking.Size = new System.Drawing.Size(87, 21);
            this.avr_blocking.TabIndex = 3;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(13, 106);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(136, 13);
            this.label66.TabIndex = 36;
            this.label66.Text = "����������(����������)";
            // 
            // avr_reset_blocking
            // 
            this.avr_reset_blocking.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.avr_reset_blocking.FormattingEnabled = true;
            this.avr_reset_blocking.Location = new System.Drawing.Point(156, 124);
            this.avr_reset_blocking.Name = "avr_reset_blocking";
            this.avr_reset_blocking.Size = new System.Drawing.Size(87, 21);
            this.avr_reset_blocking.TabIndex = 24;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(13, 129);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(101, 13);
            this.label65.TabIndex = 37;
            this.label65.Text = "����� ����������";
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.comboBox1);
            this.groupBox13.Controls.Add(this.label57);
            this.groupBox13.Controls.Add(this.lzsh_constraint);
            this.groupBox13.Controls.Add(this.label48);
            this.groupBox13.Location = new System.Drawing.Point(3, 228);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(256, 86);
            this.groupBox13.TabIndex = 23;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "��������� ���";
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(6, 36);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(87, 21);
            this.comboBox1.TabIndex = 29;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(164, 15);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(65, 13);
            this.label57.TabIndex = 27;
            this.label57.Text = "�������, I�";
            // 
            // lzsh_constraint
            // 
            this.lzsh_constraint.Location = new System.Drawing.Point(146, 36);
            this.lzsh_constraint.Name = "lzsh_constraint";
            this.lzsh_constraint.Size = new System.Drawing.Size(85, 20);
            this.lzsh_constraint.TabIndex = 24;
            this.lzsh_constraint.Tag = "0,40";
            this.lzsh_constraint.Text = "0";
            this.lzsh_constraint.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(9, 20);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(80, 13);
            this.label48.TabIndex = 26;
            this.label48.Text = "������������";
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this._apvStartCheckBox);
            this.groupBox14.Controls.Add(this.label55);
            this.groupBox14.Controls.Add(this.label47);
            this.groupBox14.Controls.Add(this.label56);
            this.groupBox14.Controls.Add(this.apv_time_4krat);
            this.groupBox14.Controls.Add(this.label52);
            this.groupBox14.Controls.Add(this.apv_time_3krat);
            this.groupBox14.Controls.Add(this.label53);
            this.groupBox14.Controls.Add(this.apv_time_2krat);
            this.groupBox14.Controls.Add(this.label54);
            this.groupBox14.Controls.Add(this.apv_time_1krat);
            this.groupBox14.Controls.Add(this.label50);
            this.groupBox14.Controls.Add(this.label51);
            this.groupBox14.Controls.Add(this.apv_time_ready);
            this.groupBox14.Controls.Add(this.label49);
            this.groupBox14.Controls.Add(this.apv_time_blocking);
            this.groupBox14.Controls.Add(this.apv_blocking);
            this.groupBox14.Controls.Add(this.apv_conf);
            this.groupBox14.Location = new System.Drawing.Point(3, 1);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(256, 221);
            this.groupBox14.TabIndex = 22;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "������������ ���";
            // 
            // _apvStartCheckBox
            // 
            this._apvStartCheckBox.AutoSize = true;
            this._apvStartCheckBox.Location = new System.Drawing.Point(146, 185);
            this._apvStartCheckBox.Name = "_apvStartCheckBox";
            this._apvStartCheckBox.Size = new System.Drawing.Size(15, 14);
            this._apvStartCheckBox.TabIndex = 35;
            this._apvStartCheckBox.UseVisualStyleBackColor = true;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(8, 106);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(101, 13);
            this.label54.TabIndex = 30;
            this.label54.Text = "����� 1 �����, ��";
            // 
            // apv_time_1krat
            // 
            this.apv_time_1krat.Location = new System.Drawing.Point(146, 101);
            this.apv_time_1krat.Name = "apv_time_1krat";
            this.apv_time_1krat.Size = new System.Drawing.Size(85, 20);
            this.apv_time_1krat.TabIndex = 20;
            this.apv_time_1krat.Tag = "0,3000000";
            this.apv_time_1krat.Text = "0";
            this.apv_time_1krat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(8, 86);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(120, 13);
            this.label50.TabIndex = 29;
            this.label50.Text = "����� ����������, ��";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(8, 66);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(123, 13);
            this.label51.TabIndex = 28;
            this.label51.Text = "����� ����������, ��";
            // 
            // apv_time_ready
            // 
            this.apv_time_ready.Location = new System.Drawing.Point(146, 81);
            this.apv_time_ready.Name = "apv_time_ready";
            this.apv_time_ready.Size = new System.Drawing.Size(85, 20);
            this.apv_time_ready.TabIndex = 19;
            this.apv_time_ready.Tag = "0,3000000";
            this.apv_time_ready.Text = "0";
            this.apv_time_ready.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(8, 46);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(68, 13);
            this.label49.TabIndex = 27;
            this.label49.Text = "����������";
            // 
            // apv_time_blocking
            // 
            this.apv_time_blocking.Location = new System.Drawing.Point(146, 61);
            this.apv_time_blocking.Name = "apv_time_blocking";
            this.apv_time_blocking.Size = new System.Drawing.Size(85, 20);
            this.apv_time_blocking.TabIndex = 18;
            this.apv_time_blocking.Tag = "0,3000000";
            this.apv_time_blocking.Text = "0";
            this.apv_time_blocking.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _newDefendingTabPage
            // 
            this._newDefendingTabPage.Controls.Add(this.groupBox8);
            this._newDefendingTabPage.Controls.Add(this.groupBox5);
            this._newDefendingTabPage.Location = new System.Drawing.Point(4, 22);
            this._newDefendingTabPage.Name = "_newDefendingTabPage";
            this._newDefendingTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._newDefendingTabPage.Size = new System.Drawing.Size(899, 645);
            this._newDefendingTabPage.TabIndex = 8;
            this._newDefendingTabPage.Text = "������";
            this._newDefendingTabPage.UseVisualStyleBackColor = true;
            // 
            // groupBox8
            // 
            this.groupBox8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox8.Controls.Add(this.tabControl3);
            this.groupBox8.Location = new System.Drawing.Point(3, 63);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(893, 579);
            this.groupBox8.TabIndex = 1;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "������";
            // 
            // tabControl3
            // 
            this.tabControl3.Controls.Add(this._tokDefendingTabPage);
            this.tabControl3.Controls.Add(this._voltageDefendingTabPage);
            this.tabControl3.Controls.Add(this._frequencyDefendingTabPage);
            this.tabControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl3.Location = new System.Drawing.Point(3, 16);
            this.tabControl3.Name = "tabControl3";
            this.tabControl3.SelectedIndex = 0;
            this.tabControl3.Size = new System.Drawing.Size(887, 560);
            this.tabControl3.TabIndex = 0;
            // 
            // _tokDefendingTabPage
            // 
            this._tokDefendingTabPage.Controls.Add(this._tokDefenseGrid3);
            this._tokDefendingTabPage.Controls.Add(this.groupBox15);
            this._tokDefendingTabPage.Controls.Add(this._tokDefenseGrid4);
            this._tokDefendingTabPage.Controls.Add(this._tokDefenseGrid2);
            this._tokDefendingTabPage.Controls.Add(this._tokDefenseGrid1);
            this._tokDefendingTabPage.Location = new System.Drawing.Point(4, 22);
            this._tokDefendingTabPage.Name = "_tokDefendingTabPage";
            this._tokDefendingTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._tokDefendingTabPage.Size = new System.Drawing.Size(879, 534);
            this._tokDefendingTabPage.TabIndex = 0;
            this._tokDefendingTabPage.Text = "������� ������";
            this._tokDefendingTabPage.UseVisualStyleBackColor = true;
            // 
            // _tokDefenseGrid3
            // 
            this._tokDefenseGrid3.AllowUserToAddRows = false;
            this._tokDefenseGrid3.AllowUserToDeleteRows = false;
            this._tokDefenseGrid3.AllowUserToResizeColumns = false;
            this._tokDefenseGrid3.AllowUserToResizeRows = false;
            this._tokDefenseGrid3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tokDefenseGrid3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._tokDefenseGrid3.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._tokDefenseGrid3.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._tokDefenseGrid3.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle18;
            this._tokDefenseGrid3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._tokDefenseGrid3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewComboBoxColumn11,
            this.dataGridViewComboBoxColumn12,
            this.dataGridViewCheckBoxColumn5,
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewComboBoxColumn13,
            this.dataGridViewComboBoxColumn14,
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewComboBoxColumn15,
            this.Column13,
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewComboBoxColumn16,
            this.dataGridViewTextBoxColumn18,
            this.dataGridViewCheckBoxColumn8,
            this.dataGridViewTextBoxColumn19,
            this.dataGridViewCheckBoxColumn9,
            this.dataGridViewCheckBoxColumn10,
            this.dataGridViewComboBoxColumn17});
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle23.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle23.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._tokDefenseGrid3.DefaultCellStyle = dataGridViewCellStyle23;
            this._tokDefenseGrid3.Location = new System.Drawing.Point(6, 337);
            this._tokDefenseGrid3.Name = "_tokDefenseGrid3";
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle24.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle24.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle24.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle24.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._tokDefenseGrid3.RowHeadersDefaultCellStyle = dataGridViewCellStyle24;
            this._tokDefenseGrid3.RowHeadersVisible = false;
            this._tokDefenseGrid3.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._tokDefenseGrid3.RowsDefaultCellStyle = dataGridViewCellStyle25;
            this._tokDefenseGrid3.RowTemplate.Height = 24;
            this._tokDefenseGrid3.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this._tokDefenseGrid3.Size = new System.Drawing.Size(867, 97);
            this._tokDefenseGrid3.TabIndex = 14;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.Frozen = true;
            this.dataGridViewTextBoxColumn14.HeaderText = "";
            this.dataGridViewTextBoxColumn14.MinimumWidth = 50;
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            this.dataGridViewTextBoxColumn14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn14.Width = 50;
            // 
            // dataGridViewComboBoxColumn11
            // 
            this.dataGridViewComboBoxColumn11.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn11.HeaderText = "�����";
            this.dataGridViewComboBoxColumn11.Name = "dataGridViewComboBoxColumn11";
            this.dataGridViewComboBoxColumn11.Width = 48;
            // 
            // dataGridViewComboBoxColumn12
            // 
            this.dataGridViewComboBoxColumn12.HeaderText = "����������";
            this.dataGridViewComboBoxColumn12.Name = "dataGridViewComboBoxColumn12";
            this.dataGridViewComboBoxColumn12.Width = 74;
            // 
            // dataGridViewCheckBoxColumn5
            // 
            this.dataGridViewCheckBoxColumn5.HeaderText = "���� �� U";
            this.dataGridViewCheckBoxColumn5.Name = "dataGridViewCheckBoxColumn5";
            this.dataGridViewCheckBoxColumn5.Width = 50;
            // 
            // dataGridViewTextBoxColumn15
            // 
            dataGridViewCellStyle19.NullValue = null;
            this.dataGridViewTextBoxColumn15.DefaultCellStyle = dataGridViewCellStyle19;
            this.dataGridViewTextBoxColumn15.HeaderText = "���. �����, �";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn15.Width = 63;
            // 
            // dataGridViewComboBoxColumn13
            // 
            this.dataGridViewComboBoxColumn13.HeaderText = "�����������";
            this.dataGridViewComboBoxColumn13.Name = "dataGridViewComboBoxColumn13";
            this.dataGridViewComboBoxColumn13.Width = 81;
            // 
            // dataGridViewComboBoxColumn14
            // 
            this.dataGridViewComboBoxColumn14.HeaderText = "����������";
            this.dataGridViewComboBoxColumn14.Name = "dataGridViewComboBoxColumn14";
            this.dataGridViewComboBoxColumn14.Width = 74;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.HeaderText = "Column10";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.Visible = false;
            this.dataGridViewTextBoxColumn16.Width = 79;
            // 
            // dataGridViewComboBoxColumn15
            // 
            this.dataGridViewComboBoxColumn15.HeaderText = "��������";
            this.dataGridViewComboBoxColumn15.Name = "dataGridViewComboBoxColumn15";
            this.dataGridViewComboBoxColumn15.Width = 64;
            // 
            // Column13
            // 
            this.Column13.HeaderText = "Column13";
            this.Column13.Name = "Column13";
            this.Column13.Visible = false;
            this.Column13.Width = 79;
            // 
            // dataGridViewTextBoxColumn17
            // 
            dataGridViewCellStyle20.NullValue = null;
            this.dataGridViewTextBoxColumn17.DefaultCellStyle = dataGridViewCellStyle20;
            this.dataGridViewTextBoxColumn17.HeaderText = "���.����., I�(��)";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn17.Width = 86;
            // 
            // dataGridViewComboBoxColumn16
            // 
            this.dataGridViewComboBoxColumn16.HeaderText = "���-��";
            this.dataGridViewComboBoxColumn16.Name = "dataGridViewComboBoxColumn16";
            this.dataGridViewComboBoxColumn16.Visible = false;
            this.dataGridViewComboBoxColumn16.Width = 47;
            // 
            // dataGridViewTextBoxColumn18
            // 
            dataGridViewCellStyle21.NullValue = null;
            this.dataGridViewTextBoxColumn18.DefaultCellStyle = dataGridViewCellStyle21;
            this.dataGridViewTextBoxColumn18.HeaderText = "t����, ��/����.";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn18.Width = 85;
            // 
            // dataGridViewCheckBoxColumn8
            // 
            this.dataGridViewCheckBoxColumn8.HeaderText = "���.";
            this.dataGridViewCheckBoxColumn8.Name = "dataGridViewCheckBoxColumn8";
            this.dataGridViewCheckBoxColumn8.Width = 36;
            // 
            // dataGridViewTextBoxColumn19
            // 
            dataGridViewCellStyle22.NullValue = null;
            this.dataGridViewTextBoxColumn19.DefaultCellStyle = dataGridViewCellStyle22;
            this.dataGridViewTextBoxColumn19.HeaderText = "t���, ��";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn19.Width = 48;
            // 
            // dataGridViewCheckBoxColumn9
            // 
            this.dataGridViewCheckBoxColumn9.HeaderText = "����";
            this.dataGridViewCheckBoxColumn9.Name = "dataGridViewCheckBoxColumn9";
            this.dataGridViewCheckBoxColumn9.Width = 43;
            // 
            // dataGridViewCheckBoxColumn10
            // 
            this.dataGridViewCheckBoxColumn10.HeaderText = "���";
            this.dataGridViewCheckBoxColumn10.Name = "dataGridViewCheckBoxColumn10";
            this.dataGridViewCheckBoxColumn10.Width = 35;
            // 
            // dataGridViewComboBoxColumn17
            // 
            this.dataGridViewComboBoxColumn17.HeaderText = "�����������";
            this.dataGridViewComboBoxColumn17.Name = "dataGridViewComboBoxColumn17";
            this.dataGridViewComboBoxColumn17.Width = 82;
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this._tokDefenseInbox);
            this.groupBox15.Controls.Add(this.label24);
            this.groupBox15.Controls.Add(this._tokDefenseI2box);
            this.groupBox15.Controls.Add(this.label23);
            this.groupBox15.Controls.Add(this._tokDefenseI0box);
            this.groupBox15.Controls.Add(this.label22);
            this.groupBox15.Controls.Add(this._tokDefenseIbox);
            this.groupBox15.Controls.Add(this.label21);
            this.groupBox15.Location = new System.Drawing.Point(6, 6);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(261, 41);
            this.groupBox15.TabIndex = 13;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "������������ ���� (���� ��)";
            // 
            // _tokDefenseInbox
            // 
            this._tokDefenseInbox.Location = new System.Drawing.Point(216, 15);
            this._tokDefenseInbox.Name = "_tokDefenseInbox";
            this._tokDefenseInbox.Size = new System.Drawing.Size(37, 20);
            this._tokDefenseInbox.TabIndex = 7;
            this._tokDefenseInbox.Tag = "0,360";
            this._tokDefenseInbox.Text = "0";
            this._tokDefenseInbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(200, 18);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(16, 13);
            this.label24.TabIndex = 6;
            this.label24.Text = "In";
            // 
            // _tokDefenseI2box
            // 
            this._tokDefenseI2box.Location = new System.Drawing.Point(152, 15);
            this._tokDefenseI2box.Name = "_tokDefenseI2box";
            this._tokDefenseI2box.Size = new System.Drawing.Size(37, 20);
            this._tokDefenseI2box.TabIndex = 5;
            this._tokDefenseI2box.Tag = "0,360";
            this._tokDefenseI2box.Text = "0";
            this._tokDefenseI2box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(136, 18);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(16, 13);
            this.label23.TabIndex = 4;
            this.label23.Text = "I2";
            // 
            // _tokDefenseI0box
            // 
            this._tokDefenseI0box.Location = new System.Drawing.Point(91, 15);
            this._tokDefenseI0box.Name = "_tokDefenseI0box";
            this._tokDefenseI0box.Size = new System.Drawing.Size(37, 20);
            this._tokDefenseI0box.TabIndex = 3;
            this._tokDefenseI0box.Tag = "0,360";
            this._tokDefenseI0box.Text = "0";
            this._tokDefenseI0box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(75, 18);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(16, 13);
            this.label22.TabIndex = 2;
            this.label22.Text = "I0";
            // 
            // _tokDefenseIbox
            // 
            this._tokDefenseIbox.Location = new System.Drawing.Point(31, 15);
            this._tokDefenseIbox.Name = "_tokDefenseIbox";
            this._tokDefenseIbox.Size = new System.Drawing.Size(37, 20);
            this._tokDefenseIbox.TabIndex = 1;
            this._tokDefenseIbox.Tag = "0,360";
            this._tokDefenseIbox.Text = "0";
            this._tokDefenseIbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(15, 18);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(10, 13);
            this.label21.TabIndex = 0;
            this.label21.Text = "I";
            // 
            // _tokDefenseGrid4
            // 
            this._tokDefenseGrid4.AllowUserToAddRows = false;
            this._tokDefenseGrid4.AllowUserToDeleteRows = false;
            this._tokDefenseGrid4.AllowUserToResizeColumns = false;
            this._tokDefenseGrid4.AllowUserToResizeRows = false;
            this._tokDefenseGrid4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tokDefenseGrid4.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._tokDefenseGrid4.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle26.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle26.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle26.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle26.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle26.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._tokDefenseGrid4.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle26;
            this._tokDefenseGrid4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._tokDefenseGrid4.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._tokDefense4NameCol,
            this._tokDefense4ModeCol,
            this._tokDefense4BlockNumberCol,
            this._tokDefense4UpuskCol,
            this._tokDefense4PuskConstraintCol,
            this._tokDefense4WorkConstraintCol,
            this._tokDefense4WorkTimeCol,
            this._tokDefense4SpeedupCol,
            this._tokDefense4SpeedupTimeCol,
            this._tokDefense4UROVCol,
            this._tokDefense4APVCol,
            this._tokDefense4OSCv11Col});
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle30.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle30.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle30.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle30.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle30.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._tokDefenseGrid4.DefaultCellStyle = dataGridViewCellStyle30;
            this._tokDefenseGrid4.Location = new System.Drawing.Point(6, 435);
            this._tokDefenseGrid4.Name = "_tokDefenseGrid4";
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle31.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle31.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle31.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle31.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle31.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._tokDefenseGrid4.RowHeadersDefaultCellStyle = dataGridViewCellStyle31;
            this._tokDefenseGrid4.RowHeadersVisible = false;
            this._tokDefenseGrid4.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._tokDefenseGrid4.RowsDefaultCellStyle = dataGridViewCellStyle32;
            this._tokDefenseGrid4.RowTemplate.Height = 24;
            this._tokDefenseGrid4.Size = new System.Drawing.Size(867, 97);
            this._tokDefenseGrid4.TabIndex = 12;
            // 
            // _tokDefense4NameCol
            // 
            this._tokDefense4NameCol.Frozen = true;
            this._tokDefense4NameCol.HeaderText = "";
            this._tokDefense4NameCol.MinimumWidth = 50;
            this._tokDefense4NameCol.Name = "_tokDefense4NameCol";
            this._tokDefense4NameCol.ReadOnly = true;
            this._tokDefense4NameCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense4NameCol.Width = 50;
            // 
            // _tokDefense4ModeCol
            // 
            this._tokDefense4ModeCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._tokDefense4ModeCol.HeaderText = "�����";
            this._tokDefense4ModeCol.Name = "_tokDefense4ModeCol";
            this._tokDefense4ModeCol.Width = 48;
            // 
            // _tokDefense4BlockNumberCol
            // 
            this._tokDefense4BlockNumberCol.HeaderText = "����������";
            this._tokDefense4BlockNumberCol.Name = "_tokDefense4BlockNumberCol";
            this._tokDefense4BlockNumberCol.Width = 74;
            // 
            // _tokDefense4UpuskCol
            // 
            this._tokDefense4UpuskCol.HeaderText = "���� �� U";
            this._tokDefense4UpuskCol.Name = "_tokDefense4UpuskCol";
            this._tokDefense4UpuskCol.Width = 50;
            // 
            // _tokDefense4PuskConstraintCol
            // 
            dataGridViewCellStyle27.NullValue = null;
            this._tokDefense4PuskConstraintCol.DefaultCellStyle = dataGridViewCellStyle27;
            this._tokDefense4PuskConstraintCol.HeaderText = "������� �����, �";
            this._tokDefense4PuskConstraintCol.Name = "_tokDefense4PuskConstraintCol";
            this._tokDefense4PuskConstraintCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense4PuskConstraintCol.Width = 82;
            // 
            // _tokDefense4WorkConstraintCol
            // 
            dataGridViewCellStyle28.NullValue = null;
            this._tokDefense4WorkConstraintCol.DefaultCellStyle = dataGridViewCellStyle28;
            this._tokDefense4WorkConstraintCol.HeaderText = "���.����., I�";
            this._tokDefense4WorkConstraintCol.Name = "_tokDefense4WorkConstraintCol";
            this._tokDefense4WorkConstraintCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense4WorkConstraintCol.Width = 69;
            // 
            // _tokDefense4WorkTimeCol
            // 
            dataGridViewCellStyle29.NullValue = null;
            this._tokDefense4WorkTimeCol.DefaultCellStyle = dataGridViewCellStyle29;
            this._tokDefense4WorkTimeCol.HeaderText = "t����, ��/����.";
            this._tokDefense4WorkTimeCol.Name = "_tokDefense4WorkTimeCol";
            this._tokDefense4WorkTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense4WorkTimeCol.Width = 85;
            // 
            // _tokDefense4SpeedupCol
            // 
            this._tokDefense4SpeedupCol.HeaderText = "���.";
            this._tokDefense4SpeedupCol.Name = "_tokDefense4SpeedupCol";
            this._tokDefense4SpeedupCol.Width = 36;
            // 
            // _tokDefense4SpeedupTimeCol
            // 
            this._tokDefense4SpeedupTimeCol.HeaderText = "t���., ��";
            this._tokDefense4SpeedupTimeCol.Name = "_tokDefense4SpeedupTimeCol";
            this._tokDefense4SpeedupTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense4SpeedupTimeCol.Width = 50;
            // 
            // _tokDefense4UROVCol
            // 
            this._tokDefense4UROVCol.HeaderText = "����";
            this._tokDefense4UROVCol.Name = "_tokDefense4UROVCol";
            this._tokDefense4UROVCol.Width = 43;
            // 
            // _tokDefense4APVCol
            // 
            this._tokDefense4APVCol.HeaderText = "���";
            this._tokDefense4APVCol.Name = "_tokDefense4APVCol";
            this._tokDefense4APVCol.Width = 35;
            // 
            // _tokDefense4OSCv11Col
            // 
            this._tokDefense4OSCv11Col.HeaderText = "�����������";
            this._tokDefense4OSCv11Col.Name = "_tokDefense4OSCv11Col";
            this._tokDefense4OSCv11Col.Width = 82;
            // 
            // _tokDefenseGrid2
            // 
            this._tokDefenseGrid2.AllowUserToAddRows = false;
            this._tokDefenseGrid2.AllowUserToDeleteRows = false;
            this._tokDefenseGrid2.AllowUserToResizeColumns = false;
            this._tokDefenseGrid2.AllowUserToResizeRows = false;
            this._tokDefenseGrid2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tokDefenseGrid2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._tokDefenseGrid2.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._tokDefenseGrid2.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle33.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle33.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle33.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle33.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle33.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._tokDefenseGrid2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle33;
            this._tokDefenseGrid2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._tokDefenseGrid2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._tokDefense3NameCol,
            this._tokDefense3ModeCol,
            this._tokDefense3BlockingNumberCol,
            this._tokDefense3UpuskCol,
            this._tokDefense3PuskConstraintCol,
            this._tokDefense3DirectionCol,
            this._tokDefense3BlockingExistCol,
            this.Column10,
            this._tokDefense3ParameterCol,
            this._tokDefense3WorkConstraintCol,
            this.Column12,
            this._tokDefense3FeatureCol,
            this._tokDefense3WorkTimeCol,
            this._tokDefense3SpeedupCol,
            this._tokDefense3SpeedupTimeCol,
            this._tokDefense3UROVCol,
            this._tokDefense3APVCol,
            this._tokDefense3OSCv11Col});
            dataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle38.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle38.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle38.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle38.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle38.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._tokDefenseGrid2.DefaultCellStyle = dataGridViewCellStyle38;
            this._tokDefenseGrid2.Location = new System.Drawing.Point(6, 195);
            this._tokDefenseGrid2.Name = "_tokDefenseGrid2";
            dataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle39.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle39.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle39.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle39.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle39.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._tokDefenseGrid2.RowHeadersDefaultCellStyle = dataGridViewCellStyle39;
            this._tokDefenseGrid2.RowHeadersVisible = false;
            this._tokDefenseGrid2.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._tokDefenseGrid2.RowsDefaultCellStyle = dataGridViewCellStyle40;
            this._tokDefenseGrid2.RowTemplate.Height = 24;
            this._tokDefenseGrid2.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this._tokDefenseGrid2.Size = new System.Drawing.Size(867, 141);
            this._tokDefenseGrid2.TabIndex = 11;
            // 
            // _tokDefense3NameCol
            // 
            this._tokDefense3NameCol.Frozen = true;
            this._tokDefense3NameCol.HeaderText = "";
            this._tokDefense3NameCol.MinimumWidth = 50;
            this._tokDefense3NameCol.Name = "_tokDefense3NameCol";
            this._tokDefense3NameCol.ReadOnly = true;
            this._tokDefense3NameCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense3NameCol.Width = 50;
            // 
            // _tokDefense3ModeCol
            // 
            this._tokDefense3ModeCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._tokDefense3ModeCol.HeaderText = "�����";
            this._tokDefense3ModeCol.Name = "_tokDefense3ModeCol";
            this._tokDefense3ModeCol.Width = 48;
            // 
            // _tokDefense3BlockingNumberCol
            // 
            this._tokDefense3BlockingNumberCol.HeaderText = "����������";
            this._tokDefense3BlockingNumberCol.Name = "_tokDefense3BlockingNumberCol";
            this._tokDefense3BlockingNumberCol.Width = 74;
            // 
            // _tokDefense3UpuskCol
            // 
            this._tokDefense3UpuskCol.HeaderText = "���� �� U";
            this._tokDefense3UpuskCol.Name = "_tokDefense3UpuskCol";
            this._tokDefense3UpuskCol.Width = 50;
            // 
            // _tokDefense3PuskConstraintCol
            // 
            dataGridViewCellStyle34.NullValue = null;
            this._tokDefense3PuskConstraintCol.DefaultCellStyle = dataGridViewCellStyle34;
            this._tokDefense3PuskConstraintCol.HeaderText = "���. �����, �";
            this._tokDefense3PuskConstraintCol.Name = "_tokDefense3PuskConstraintCol";
            this._tokDefense3PuskConstraintCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense3PuskConstraintCol.Width = 63;
            // 
            // _tokDefense3DirectionCol
            // 
            this._tokDefense3DirectionCol.HeaderText = "�����������";
            this._tokDefense3DirectionCol.Name = "_tokDefense3DirectionCol";
            this._tokDefense3DirectionCol.Width = 81;
            // 
            // _tokDefense3BlockingExistCol
            // 
            this._tokDefense3BlockingExistCol.HeaderText = "����������";
            this._tokDefense3BlockingExistCol.Name = "_tokDefense3BlockingExistCol";
            this._tokDefense3BlockingExistCol.Width = 74;
            // 
            // Column10
            // 
            this.Column10.HeaderText = "Column10";
            this.Column10.Name = "Column10";
            this.Column10.Visible = false;
            this.Column10.Width = 79;
            // 
            // _tokDefense3ParameterCol
            // 
            this._tokDefense3ParameterCol.HeaderText = "��������";
            this._tokDefense3ParameterCol.Name = "_tokDefense3ParameterCol";
            this._tokDefense3ParameterCol.Width = 64;
            // 
            // _tokDefense3WorkConstraintCol
            // 
            dataGridViewCellStyle35.NullValue = null;
            this._tokDefense3WorkConstraintCol.DefaultCellStyle = dataGridViewCellStyle35;
            this._tokDefense3WorkConstraintCol.HeaderText = "���.����., I�(��)";
            this._tokDefense3WorkConstraintCol.Name = "_tokDefense3WorkConstraintCol";
            this._tokDefense3WorkConstraintCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense3WorkConstraintCol.Width = 86;
            // 
            // Column12
            // 
            this.Column12.HeaderText = "Column12";
            this.Column12.Name = "Column12";
            this.Column12.Visible = false;
            this.Column12.Width = 79;
            // 
            // _tokDefense3FeatureCol
            // 
            this._tokDefense3FeatureCol.HeaderText = "���-��";
            this._tokDefense3FeatureCol.Name = "_tokDefense3FeatureCol";
            this._tokDefense3FeatureCol.Visible = false;
            this._tokDefense3FeatureCol.Width = 47;
            // 
            // _tokDefense3WorkTimeCol
            // 
            dataGridViewCellStyle36.NullValue = null;
            this._tokDefense3WorkTimeCol.DefaultCellStyle = dataGridViewCellStyle36;
            this._tokDefense3WorkTimeCol.HeaderText = "t����, ��/����.";
            this._tokDefense3WorkTimeCol.Name = "_tokDefense3WorkTimeCol";
            this._tokDefense3WorkTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense3WorkTimeCol.Width = 85;
            // 
            // _tokDefense3SpeedupCol
            // 
            this._tokDefense3SpeedupCol.HeaderText = "���.";
            this._tokDefense3SpeedupCol.Name = "_tokDefense3SpeedupCol";
            this._tokDefense3SpeedupCol.Width = 36;
            // 
            // _tokDefense3SpeedupTimeCol
            // 
            dataGridViewCellStyle37.NullValue = null;
            this._tokDefense3SpeedupTimeCol.DefaultCellStyle = dataGridViewCellStyle37;
            this._tokDefense3SpeedupTimeCol.HeaderText = "t���, ��";
            this._tokDefense3SpeedupTimeCol.Name = "_tokDefense3SpeedupTimeCol";
            this._tokDefense3SpeedupTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense3SpeedupTimeCol.Width = 48;
            // 
            // _tokDefense3UROVCol
            // 
            this._tokDefense3UROVCol.HeaderText = "����";
            this._tokDefense3UROVCol.Name = "_tokDefense3UROVCol";
            this._tokDefense3UROVCol.Width = 43;
            // 
            // _tokDefense3APVCol
            // 
            this._tokDefense3APVCol.HeaderText = "���";
            this._tokDefense3APVCol.Name = "_tokDefense3APVCol";
            this._tokDefense3APVCol.Width = 35;
            // 
            // _tokDefense3OSCv11Col
            // 
            this._tokDefense3OSCv11Col.HeaderText = "�����������";
            this._tokDefense3OSCv11Col.Name = "_tokDefense3OSCv11Col";
            this._tokDefense3OSCv11Col.Width = 82;
            // 
            // _tokDefenseGrid1
            // 
            this._tokDefenseGrid1.AllowUserToAddRows = false;
            this._tokDefenseGrid1.AllowUserToDeleteRows = false;
            this._tokDefenseGrid1.AllowUserToResizeColumns = false;
            this._tokDefenseGrid1.AllowUserToResizeRows = false;
            this._tokDefenseGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tokDefenseGrid1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._tokDefenseGrid1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._tokDefenseGrid1.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle41.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle41.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle41.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle41.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle41.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle41.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle41.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._tokDefenseGrid1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle41;
            this._tokDefenseGrid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._tokDefenseGrid1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._tokDefenseNameCol,
            this._tokDefenseModeCol,
            this._tokDefenseBlockNumberCol,
            this._tokDefenseU_PuskCol,
            this._tokDefensePuskConstraintCol,
            this._tokDefenseDirectionCol,
            this._tokDefenseBlockExistCol,
            this._tokDefenseParameterCol,
            this.Column9,
            this._tokDefenseWorkConstraintCol,
            this.Column11,
            this._tokDefenseFeatureCol,
            this._tokDefenseWorkTimeCol,
            this._tokDefenseSpeedUpCol,
            this._tokDefenseSpeedupTimeCol,
            this._tokDefenseUROVCol,
            this._tokDefenseAPVCol,
            this._tokDefenseOSCv11Col});
            dataGridViewCellStyle46.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle46.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle46.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle46.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle46.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle46.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle46.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._tokDefenseGrid1.DefaultCellStyle = dataGridViewCellStyle46;
            this._tokDefenseGrid1.Location = new System.Drawing.Point(6, 53);
            this._tokDefenseGrid1.Name = "_tokDefenseGrid1";
            dataGridViewCellStyle47.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle47.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle47.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle47.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle47.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle47.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle47.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._tokDefenseGrid1.RowHeadersDefaultCellStyle = dataGridViewCellStyle47;
            this._tokDefenseGrid1.RowHeadersVisible = false;
            this._tokDefenseGrid1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle48.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._tokDefenseGrid1.RowsDefaultCellStyle = dataGridViewCellStyle48;
            this._tokDefenseGrid1.RowTemplate.Height = 24;
            this._tokDefenseGrid1.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this._tokDefenseGrid1.Size = new System.Drawing.Size(867, 141);
            this._tokDefenseGrid1.TabIndex = 10;
            // 
            // _tokDefenseNameCol
            // 
            this._tokDefenseNameCol.Frozen = true;
            this._tokDefenseNameCol.HeaderText = "";
            this._tokDefenseNameCol.MinimumWidth = 50;
            this._tokDefenseNameCol.Name = "_tokDefenseNameCol";
            this._tokDefenseNameCol.ReadOnly = true;
            this._tokDefenseNameCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefenseNameCol.Width = 50;
            // 
            // _tokDefenseModeCol
            // 
            this._tokDefenseModeCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._tokDefenseModeCol.HeaderText = "�����";
            this._tokDefenseModeCol.Name = "_tokDefenseModeCol";
            this._tokDefenseModeCol.Width = 48;
            // 
            // _tokDefenseBlockNumberCol
            // 
            this._tokDefenseBlockNumberCol.HeaderText = "����������";
            this._tokDefenseBlockNumberCol.Name = "_tokDefenseBlockNumberCol";
            this._tokDefenseBlockNumberCol.Width = 74;
            // 
            // _tokDefenseU_PuskCol
            // 
            this._tokDefenseU_PuskCol.HeaderText = "���� �� U";
            this._tokDefenseU_PuskCol.Name = "_tokDefenseU_PuskCol";
            this._tokDefenseU_PuskCol.Width = 50;
            // 
            // _tokDefensePuskConstraintCol
            // 
            dataGridViewCellStyle42.NullValue = null;
            this._tokDefensePuskConstraintCol.DefaultCellStyle = dataGridViewCellStyle42;
            this._tokDefensePuskConstraintCol.HeaderText = "���. �����, �";
            this._tokDefensePuskConstraintCol.Name = "_tokDefensePuskConstraintCol";
            this._tokDefensePuskConstraintCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefensePuskConstraintCol.Width = 63;
            // 
            // _tokDefenseDirectionCol
            // 
            this._tokDefenseDirectionCol.HeaderText = "�����������";
            this._tokDefenseDirectionCol.Name = "_tokDefenseDirectionCol";
            this._tokDefenseDirectionCol.Width = 81;
            // 
            // _tokDefenseBlockExistCol
            // 
            this._tokDefenseBlockExistCol.HeaderText = "����������";
            this._tokDefenseBlockExistCol.Name = "_tokDefenseBlockExistCol";
            this._tokDefenseBlockExistCol.Width = 74;
            // 
            // _tokDefenseParameterCol
            // 
            this._tokDefenseParameterCol.HeaderText = "��������";
            this._tokDefenseParameterCol.Name = "_tokDefenseParameterCol";
            this._tokDefenseParameterCol.Width = 64;
            // 
            // Column9
            // 
            this.Column9.HeaderText = "Column9";
            this.Column9.Name = "Column9";
            this.Column9.Visible = false;
            this.Column9.Width = 73;
            // 
            // _tokDefenseWorkConstraintCol
            // 
            dataGridViewCellStyle43.NullValue = null;
            this._tokDefenseWorkConstraintCol.DefaultCellStyle = dataGridViewCellStyle43;
            this._tokDefenseWorkConstraintCol.HeaderText = "���.����., I�";
            this._tokDefenseWorkConstraintCol.Name = "_tokDefenseWorkConstraintCol";
            this._tokDefenseWorkConstraintCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefenseWorkConstraintCol.Width = 69;
            // 
            // Column11
            // 
            this.Column11.HeaderText = "Column11";
            this.Column11.Name = "Column11";
            this.Column11.Visible = false;
            this.Column11.Width = 79;
            // 
            // _tokDefenseFeatureCol
            // 
            this._tokDefenseFeatureCol.HeaderText = "���-��";
            this._tokDefenseFeatureCol.Name = "_tokDefenseFeatureCol";
            this._tokDefenseFeatureCol.Width = 47;
            // 
            // _tokDefenseWorkTimeCol
            // 
            dataGridViewCellStyle44.NullValue = null;
            this._tokDefenseWorkTimeCol.DefaultCellStyle = dataGridViewCellStyle44;
            this._tokDefenseWorkTimeCol.HeaderText = "t����, ��/����.";
            this._tokDefenseWorkTimeCol.Name = "_tokDefenseWorkTimeCol";
            this._tokDefenseWorkTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefenseWorkTimeCol.Width = 85;
            // 
            // _tokDefenseSpeedUpCol
            // 
            this._tokDefenseSpeedUpCol.HeaderText = "���.";
            this._tokDefenseSpeedUpCol.Name = "_tokDefenseSpeedUpCol";
            this._tokDefenseSpeedUpCol.Width = 36;
            // 
            // _tokDefenseSpeedupTimeCol
            // 
            dataGridViewCellStyle45.NullValue = null;
            this._tokDefenseSpeedupTimeCol.DefaultCellStyle = dataGridViewCellStyle45;
            this._tokDefenseSpeedupTimeCol.HeaderText = "t���, ��";
            this._tokDefenseSpeedupTimeCol.Name = "_tokDefenseSpeedupTimeCol";
            this._tokDefenseSpeedupTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefenseSpeedupTimeCol.Width = 48;
            // 
            // _tokDefenseUROVCol
            // 
            this._tokDefenseUROVCol.HeaderText = "����";
            this._tokDefenseUROVCol.Name = "_tokDefenseUROVCol";
            this._tokDefenseUROVCol.Width = 43;
            // 
            // _tokDefenseAPVCol
            // 
            this._tokDefenseAPVCol.HeaderText = "���";
            this._tokDefenseAPVCol.Name = "_tokDefenseAPVCol";
            this._tokDefenseAPVCol.Width = 35;
            // 
            // _tokDefenseOSCv11Col
            // 
            this._tokDefenseOSCv11Col.HeaderText = "����������";
            this._tokDefenseOSCv11Col.Name = "_tokDefenseOSCv11Col";
            this._tokDefenseOSCv11Col.Width = 76;
            // 
            // _voltageDefendingTabPage
            // 
            this._voltageDefendingTabPage.Controls.Add(this._voltageDefensesGrid11);
            this._voltageDefendingTabPage.Controls.Add(this._voltageDefensesGrid2);
            this._voltageDefendingTabPage.Controls.Add(this._voltageDefensesGrid1);
            this._voltageDefendingTabPage.Controls.Add(this._voltageDefensesGrid3);
            this._voltageDefendingTabPage.Location = new System.Drawing.Point(4, 22);
            this._voltageDefendingTabPage.Name = "_voltageDefendingTabPage";
            this._voltageDefendingTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._voltageDefendingTabPage.Size = new System.Drawing.Size(879, 534);
            this._voltageDefendingTabPage.TabIndex = 1;
            this._voltageDefendingTabPage.Text = "������ �� ����������";
            this._voltageDefendingTabPage.UseVisualStyleBackColor = true;
            // 
            // _voltageDefensesGrid11
            // 
            this._voltageDefensesGrid11.AllowUserToAddRows = false;
            this._voltageDefensesGrid11.AllowUserToDeleteRows = false;
            this._voltageDefensesGrid11.AllowUserToResizeColumns = false;
            this._voltageDefensesGrid11.AllowUserToResizeRows = false;
            this._voltageDefensesGrid11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._voltageDefensesGrid11.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._voltageDefensesGrid11.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle49.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle49.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle49.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle49.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle49.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle49.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle49.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._voltageDefensesGrid11.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle49;
            this._voltageDefensesGrid11.ColumnHeadersHeight = 30;
            this._voltageDefensesGrid11.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._voltageDefensesGrid11.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewComboBoxColumn4,
            this.dataGridViewComboBoxColumn5,
            this.dataGridViewComboBoxColumn6,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewCheckBoxColumn1,
            this.dataGridViewCheckBoxColumn2,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewCheckBoxColumn3,
            this.dataGridViewCheckBoxColumn4,
            this.dataGridViewComboBoxColumn7,
            this.dataGridViewCheckBoxColumn6,
            this.dataGridViewCheckBoxColumn7});
            dataGridViewCellStyle53.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle53.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle53.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle53.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle53.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle53.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle53.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._voltageDefensesGrid11.DefaultCellStyle = dataGridViewCellStyle53;
            this._voltageDefensesGrid11.Location = new System.Drawing.Point(6, 111);
            this._voltageDefensesGrid11.Name = "_voltageDefensesGrid11";
            dataGridViewCellStyle54.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle54.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle54.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle54.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle54.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle54.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle54.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._voltageDefensesGrid11.RowHeadersDefaultCellStyle = dataGridViewCellStyle54;
            this._voltageDefensesGrid11.RowHeadersVisible = false;
            this._voltageDefensesGrid11.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle55.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._voltageDefensesGrid11.RowsDefaultCellStyle = dataGridViewCellStyle55;
            this._voltageDefensesGrid11.RowTemplate.Height = 24;
            this._voltageDefensesGrid11.Size = new System.Drawing.Size(867, 99);
            this._voltageDefensesGrid11.TabIndex = 14;
            // 
            // dataGridViewTextBoxColumn1
            // 
            dataGridViewCellStyle50.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle50;
            this.dataGridViewTextBoxColumn1.Frozen = true;
            this.dataGridViewTextBoxColumn1.HeaderText = "";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn1.Width = 5;
            // 
            // dataGridViewComboBoxColumn4
            // 
            this.dataGridViewComboBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewComboBoxColumn4.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn4.HeaderText = "�����";
            this.dataGridViewComboBoxColumn4.Name = "dataGridViewComboBoxColumn4";
            // 
            // dataGridViewComboBoxColumn5
            // 
            this.dataGridViewComboBoxColumn5.HeaderText = "����������";
            this.dataGridViewComboBoxColumn5.Name = "dataGridViewComboBoxColumn5";
            this.dataGridViewComboBoxColumn5.Width = 74;
            // 
            // dataGridViewComboBoxColumn6
            // 
            dataGridViewCellStyle51.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.dataGridViewComboBoxColumn6.DefaultCellStyle = dataGridViewCellStyle51;
            this.dataGridViewComboBoxColumn6.HeaderText = "��������";
            this.dataGridViewComboBoxColumn6.Name = "dataGridViewComboBoxColumn6";
            this.dataGridViewComboBoxColumn6.Width = 64;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "Column1";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Visible = false;
            this.dataGridViewTextBoxColumn5.Width = 73;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "Column2";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Visible = false;
            this.dataGridViewTextBoxColumn6.Width = 73;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "������� ����., �";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn7.Width = 99;
            // 
            // dataGridViewTextBoxColumn11
            // 
            dataGridViewCellStyle52.NullValue = "0";
            this.dataGridViewTextBoxColumn11.DefaultCellStyle = dataGridViewCellStyle52;
            this.dataGridViewTextBoxColumn11.HeaderText = "����� ����., ��";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn11.Width = 96;
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.HeaderText = "����.";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.Width = 41;
            // 
            // dataGridViewCheckBoxColumn2
            // 
            this.dataGridViewCheckBoxColumn2.HeaderText = "��� ��";
            this.dataGridViewCheckBoxColumn2.Name = "dataGridViewCheckBoxColumn2";
            this.dataGridViewCheckBoxColumn2.Width = 52;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.HeaderText = "������� ��������, �";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn12.Width = 119;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.HeaderText = "����� ��������, ��";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn13.Width = 116;
            // 
            // dataGridViewCheckBoxColumn3
            // 
            this.dataGridViewCheckBoxColumn3.HeaderText = "����";
            this.dataGridViewCheckBoxColumn3.Name = "dataGridViewCheckBoxColumn3";
            this.dataGridViewCheckBoxColumn3.Width = 43;
            // 
            // dataGridViewCheckBoxColumn4
            // 
            this.dataGridViewCheckBoxColumn4.HeaderText = "���";
            this.dataGridViewCheckBoxColumn4.Name = "dataGridViewCheckBoxColumn4";
            this.dataGridViewCheckBoxColumn4.Width = 35;
            // 
            // dataGridViewComboBoxColumn7
            // 
            this.dataGridViewComboBoxColumn7.HeaderText = "�����������";
            this.dataGridViewComboBoxColumn7.Name = "dataGridViewComboBoxColumn7";
            this.dataGridViewComboBoxColumn7.Width = 82;
            // 
            // dataGridViewCheckBoxColumn6
            // 
            this.dataGridViewCheckBoxColumn6.HeaderText = "�����";
            this.dataGridViewCheckBoxColumn6.Name = "dataGridViewCheckBoxColumn6";
            this.dataGridViewCheckBoxColumn6.Width = 44;
            // 
            // dataGridViewCheckBoxColumn7
            // 
            this.dataGridViewCheckBoxColumn7.HeaderText = "U<5B";
            this.dataGridViewCheckBoxColumn7.Name = "dataGridViewCheckBoxColumn7";
            this.dataGridViewCheckBoxColumn7.Visible = false;
            this.dataGridViewCheckBoxColumn7.Width = 40;
            // 
            // _voltageDefensesGrid2
            // 
            this._voltageDefensesGrid2.AllowUserToAddRows = false;
            this._voltageDefensesGrid2.AllowUserToDeleteRows = false;
            this._voltageDefensesGrid2.AllowUserToResizeColumns = false;
            this._voltageDefensesGrid2.AllowUserToResizeRows = false;
            this._voltageDefensesGrid2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._voltageDefensesGrid2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._voltageDefensesGrid2.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._voltageDefensesGrid2.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle56.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle56.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle56.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle56.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle56.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle56.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle56.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._voltageDefensesGrid2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle56;
            this._voltageDefensesGrid2.ColumnHeadersHeight = 30;
            this._voltageDefensesGrid2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._voltageDefensesGrid2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._voltageDefensesNameCol2,
            this._voltageDefensesModeCol2,
            this._voltageDefensesBlockingNumberCol2,
            this.Column3,
            this._voltageDefensesParameterCol2,
            this.Column4,
            this._voltageDefensesWorkConstraintCol2,
            this._voltageDefensesWorkTimeCol2,
            this._voltageDefensesReturnCol2,
            this._voltageDefensesAPVreturnCol2,
            this._voltageDefensesReturnConstraintCol2,
            this._voltageDefensesReturnTimeCol2,
            this._voltageDefensesUROVcol2,
            this._voltageDefensesAPVcol2,
            this._voltageDefensesOSC�11Col2,
            this._voltageDefensesResetCol2,
            this.Column7});
            dataGridViewCellStyle60.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle60.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle60.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle60.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle60.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle60.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle60.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._voltageDefensesGrid2.DefaultCellStyle = dataGridViewCellStyle60;
            this._voltageDefensesGrid2.Location = new System.Drawing.Point(6, 216);
            this._voltageDefensesGrid2.Name = "_voltageDefensesGrid2";
            dataGridViewCellStyle61.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle61.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle61.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle61.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle61.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle61.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle61.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._voltageDefensesGrid2.RowHeadersDefaultCellStyle = dataGridViewCellStyle61;
            this._voltageDefensesGrid2.RowHeadersVisible = false;
            this._voltageDefensesGrid2.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle62.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._voltageDefensesGrid2.RowsDefaultCellStyle = dataGridViewCellStyle62;
            this._voltageDefensesGrid2.RowTemplate.Height = 24;
            this._voltageDefensesGrid2.Size = new System.Drawing.Size(867, 129);
            this._voltageDefensesGrid2.TabIndex = 12;
            // 
            // _voltageDefensesNameCol2
            // 
            dataGridViewCellStyle57.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._voltageDefensesNameCol2.DefaultCellStyle = dataGridViewCellStyle57;
            this._voltageDefensesNameCol2.Frozen = true;
            this._voltageDefensesNameCol2.HeaderText = "";
            this._voltageDefensesNameCol2.Name = "_voltageDefensesNameCol2";
            this._voltageDefensesNameCol2.ReadOnly = true;
            this._voltageDefensesNameCol2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesNameCol2.Width = 5;
            // 
            // _voltageDefensesModeCol2
            // 
            this._voltageDefensesModeCol2.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._voltageDefensesModeCol2.HeaderText = "�����";
            this._voltageDefensesModeCol2.Name = "_voltageDefensesModeCol2";
            this._voltageDefensesModeCol2.Width = 48;
            // 
            // _voltageDefensesBlockingNumberCol2
            // 
            this._voltageDefensesBlockingNumberCol2.HeaderText = "����������";
            this._voltageDefensesBlockingNumberCol2.Name = "_voltageDefensesBlockingNumberCol2";
            this._voltageDefensesBlockingNumberCol2.Width = 74;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Column3";
            this.Column3.Name = "Column3";
            this.Column3.Visible = false;
            this.Column3.Width = 73;
            // 
            // _voltageDefensesParameterCol2
            // 
            dataGridViewCellStyle58.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this._voltageDefensesParameterCol2.DefaultCellStyle = dataGridViewCellStyle58;
            this._voltageDefensesParameterCol2.HeaderText = "��������";
            this._voltageDefensesParameterCol2.Name = "_voltageDefensesParameterCol2";
            this._voltageDefensesParameterCol2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._voltageDefensesParameterCol2.Visible = false;
            this._voltageDefensesParameterCol2.Width = 64;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Column4";
            this.Column4.Name = "Column4";
            this.Column4.Visible = false;
            this.Column4.Width = 73;
            // 
            // _voltageDefensesWorkConstraintCol2
            // 
            this._voltageDefensesWorkConstraintCol2.HeaderText = "������� ����., �";
            this._voltageDefensesWorkConstraintCol2.Name = "_voltageDefensesWorkConstraintCol2";
            this._voltageDefensesWorkConstraintCol2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesWorkConstraintCol2.Width = 99;
            // 
            // _voltageDefensesWorkTimeCol2
            // 
            dataGridViewCellStyle59.NullValue = "0";
            this._voltageDefensesWorkTimeCol2.DefaultCellStyle = dataGridViewCellStyle59;
            this._voltageDefensesWorkTimeCol2.HeaderText = "����� ����.";
            this._voltageDefensesWorkTimeCol2.Name = "_voltageDefensesWorkTimeCol2";
            this._voltageDefensesWorkTimeCol2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesWorkTimeCol2.Width = 76;
            // 
            // _voltageDefensesReturnCol2
            // 
            this._voltageDefensesReturnCol2.HeaderText = "����.";
            this._voltageDefensesReturnCol2.Name = "_voltageDefensesReturnCol2";
            this._voltageDefensesReturnCol2.Width = 41;
            // 
            // _voltageDefensesAPVreturnCol2
            // 
            this._voltageDefensesAPVreturnCol2.HeaderText = "��� ��";
            this._voltageDefensesAPVreturnCol2.Name = "_voltageDefensesAPVreturnCol2";
            this._voltageDefensesAPVreturnCol2.Width = 52;
            // 
            // _voltageDefensesReturnConstraintCol2
            // 
            this._voltageDefensesReturnConstraintCol2.HeaderText = "������� ��������, �";
            this._voltageDefensesReturnConstraintCol2.Name = "_voltageDefensesReturnConstraintCol2";
            this._voltageDefensesReturnConstraintCol2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesReturnConstraintCol2.Width = 119;
            // 
            // _voltageDefensesReturnTimeCol2
            // 
            this._voltageDefensesReturnTimeCol2.HeaderText = "����� ��������";
            this._voltageDefensesReturnTimeCol2.Name = "_voltageDefensesReturnTimeCol2";
            this._voltageDefensesReturnTimeCol2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesReturnTimeCol2.Width = 96;
            // 
            // _voltageDefensesUROVcol2
            // 
            this._voltageDefensesUROVcol2.HeaderText = "����";
            this._voltageDefensesUROVcol2.Name = "_voltageDefensesUROVcol2";
            this._voltageDefensesUROVcol2.Width = 43;
            // 
            // _voltageDefensesAPVcol2
            // 
            this._voltageDefensesAPVcol2.HeaderText = "���";
            this._voltageDefensesAPVcol2.Name = "_voltageDefensesAPVcol2";
            this._voltageDefensesAPVcol2.Width = 35;
            // 
            // _voltageDefensesOSC�11Col2
            // 
            this._voltageDefensesOSC�11Col2.HeaderText = "�����������";
            this._voltageDefensesOSC�11Col2.Name = "_voltageDefensesOSC�11Col2";
            this._voltageDefensesOSC�11Col2.Width = 82;
            // 
            // _voltageDefensesResetCol2
            // 
            this._voltageDefensesResetCol2.HeaderText = "�����";
            this._voltageDefensesResetCol2.Name = "_voltageDefensesResetCol2";
            this._voltageDefensesResetCol2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._voltageDefensesResetCol2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this._voltageDefensesResetCol2.Width = 63;
            // 
            // Column7
            // 
            this.Column7.HeaderText = "Column7";
            this.Column7.Name = "Column7";
            this.Column7.Visible = false;
            this.Column7.Width = 54;
            // 
            // _voltageDefensesGrid1
            // 
            this._voltageDefensesGrid1.AllowUserToAddRows = false;
            this._voltageDefensesGrid1.AllowUserToDeleteRows = false;
            this._voltageDefensesGrid1.AllowUserToResizeColumns = false;
            this._voltageDefensesGrid1.AllowUserToResizeRows = false;
            this._voltageDefensesGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._voltageDefensesGrid1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._voltageDefensesGrid1.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle63.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle63.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle63.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle63.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle63.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle63.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle63.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._voltageDefensesGrid1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle63;
            this._voltageDefensesGrid1.ColumnHeadersHeight = 30;
            this._voltageDefensesGrid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._voltageDefensesGrid1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._voltageDefensesNameCol1,
            this._voltageDefensesModeCol1,
            this._voltageDefensesBlockingNumberCol1,
            this._voltageDefensesParameterCol1,
            this.Column1,
            this.Column2,
            this._voltageDefensesWorkConstraintCol1,
            this._voltageDefensesWorkTimeCol1,
            this._voltageDefensesReturnCol1,
            this._voltageDefensesAPVReturlCol1,
            this._voltageDefensesReturnConstraintCol1,
            this._voltageDefensesReturnTimeCol1,
            this._voltageDefensesUROVCol1,
            this._voltageDefensesAPVCol1,
            this._voltageDefensesOSCv11Col1,
            this._voltageDefensesResetCol1,
            this._u5vColumn});
            dataGridViewCellStyle67.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle67.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle67.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle67.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle67.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle67.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle67.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._voltageDefensesGrid1.DefaultCellStyle = dataGridViewCellStyle67;
            this._voltageDefensesGrid1.Location = new System.Drawing.Point(6, 6);
            this._voltageDefensesGrid1.Name = "_voltageDefensesGrid1";
            dataGridViewCellStyle68.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle68.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle68.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle68.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle68.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle68.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle68.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._voltageDefensesGrid1.RowHeadersDefaultCellStyle = dataGridViewCellStyle68;
            this._voltageDefensesGrid1.RowHeadersVisible = false;
            this._voltageDefensesGrid1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle69.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._voltageDefensesGrid1.RowsDefaultCellStyle = dataGridViewCellStyle69;
            this._voltageDefensesGrid1.RowTemplate.Height = 24;
            this._voltageDefensesGrid1.Size = new System.Drawing.Size(867, 99);
            this._voltageDefensesGrid1.TabIndex = 11;
            // 
            // _voltageDefensesNameCol1
            // 
            dataGridViewCellStyle64.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._voltageDefensesNameCol1.DefaultCellStyle = dataGridViewCellStyle64;
            this._voltageDefensesNameCol1.Frozen = true;
            this._voltageDefensesNameCol1.HeaderText = "";
            this._voltageDefensesNameCol1.Name = "_voltageDefensesNameCol1";
            this._voltageDefensesNameCol1.ReadOnly = true;
            this._voltageDefensesNameCol1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesNameCol1.Width = 5;
            // 
            // _voltageDefensesModeCol1
            // 
            this._voltageDefensesModeCol1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._voltageDefensesModeCol1.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._voltageDefensesModeCol1.HeaderText = "�����";
            this._voltageDefensesModeCol1.Name = "_voltageDefensesModeCol1";
            // 
            // _voltageDefensesBlockingNumberCol1
            // 
            this._voltageDefensesBlockingNumberCol1.HeaderText = "����������";
            this._voltageDefensesBlockingNumberCol1.Name = "_voltageDefensesBlockingNumberCol1";
            this._voltageDefensesBlockingNumberCol1.Width = 74;
            // 
            // _voltageDefensesParameterCol1
            // 
            dataGridViewCellStyle65.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this._voltageDefensesParameterCol1.DefaultCellStyle = dataGridViewCellStyle65;
            this._voltageDefensesParameterCol1.HeaderText = "��������";
            this._voltageDefensesParameterCol1.Name = "_voltageDefensesParameterCol1";
            this._voltageDefensesParameterCol1.Width = 64;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Column1";
            this.Column1.Name = "Column1";
            this.Column1.Visible = false;
            this.Column1.Width = 73;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Column2";
            this.Column2.Name = "Column2";
            this.Column2.Visible = false;
            this.Column2.Width = 73;
            // 
            // _voltageDefensesWorkConstraintCol1
            // 
            this._voltageDefensesWorkConstraintCol1.HeaderText = "������� ����., �";
            this._voltageDefensesWorkConstraintCol1.Name = "_voltageDefensesWorkConstraintCol1";
            this._voltageDefensesWorkConstraintCol1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesWorkConstraintCol1.Width = 99;
            // 
            // _voltageDefensesWorkTimeCol1
            // 
            dataGridViewCellStyle66.NullValue = "0";
            this._voltageDefensesWorkTimeCol1.DefaultCellStyle = dataGridViewCellStyle66;
            this._voltageDefensesWorkTimeCol1.HeaderText = "����� ����., ��";
            this._voltageDefensesWorkTimeCol1.Name = "_voltageDefensesWorkTimeCol1";
            this._voltageDefensesWorkTimeCol1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesWorkTimeCol1.Width = 96;
            // 
            // _voltageDefensesReturnCol1
            // 
            this._voltageDefensesReturnCol1.HeaderText = "����.";
            this._voltageDefensesReturnCol1.Name = "_voltageDefensesReturnCol1";
            this._voltageDefensesReturnCol1.Width = 41;
            // 
            // _voltageDefensesAPVReturlCol1
            // 
            this._voltageDefensesAPVReturlCol1.HeaderText = "��� ��";
            this._voltageDefensesAPVReturlCol1.Name = "_voltageDefensesAPVReturlCol1";
            this._voltageDefensesAPVReturlCol1.Width = 52;
            // 
            // _voltageDefensesReturnConstraintCol1
            // 
            this._voltageDefensesReturnConstraintCol1.HeaderText = "������� ��������, �";
            this._voltageDefensesReturnConstraintCol1.Name = "_voltageDefensesReturnConstraintCol1";
            this._voltageDefensesReturnConstraintCol1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesReturnConstraintCol1.Width = 119;
            // 
            // _voltageDefensesReturnTimeCol1
            // 
            this._voltageDefensesReturnTimeCol1.HeaderText = "����� ��������, ��";
            this._voltageDefensesReturnTimeCol1.Name = "_voltageDefensesReturnTimeCol1";
            this._voltageDefensesReturnTimeCol1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesReturnTimeCol1.Width = 116;
            // 
            // _voltageDefensesUROVCol1
            // 
            this._voltageDefensesUROVCol1.HeaderText = "����";
            this._voltageDefensesUROVCol1.Name = "_voltageDefensesUROVCol1";
            this._voltageDefensesUROVCol1.Width = 43;
            // 
            // _voltageDefensesAPVCol1
            // 
            this._voltageDefensesAPVCol1.HeaderText = "���";
            this._voltageDefensesAPVCol1.Name = "_voltageDefensesAPVCol1";
            this._voltageDefensesAPVCol1.Width = 35;
            // 
            // _voltageDefensesOSCv11Col1
            // 
            this._voltageDefensesOSCv11Col1.HeaderText = "�����������";
            this._voltageDefensesOSCv11Col1.Name = "_voltageDefensesOSCv11Col1";
            this._voltageDefensesOSCv11Col1.Width = 82;
            // 
            // _voltageDefensesResetCol1
            // 
            this._voltageDefensesResetCol1.HeaderText = "�����";
            this._voltageDefensesResetCol1.Name = "_voltageDefensesResetCol1";
            this._voltageDefensesResetCol1.Width = 44;
            // 
            // _u5vColumn
            // 
            this._u5vColumn.HeaderText = "U<5B";
            this._u5vColumn.Name = "_u5vColumn";
            this._u5vColumn.Visible = false;
            this._u5vColumn.Width = 40;
            // 
            // _voltageDefensesGrid3
            // 
            this._voltageDefensesGrid3.AllowUserToAddRows = false;
            this._voltageDefensesGrid3.AllowUserToDeleteRows = false;
            this._voltageDefensesGrid3.AllowUserToResizeColumns = false;
            this._voltageDefensesGrid3.AllowUserToResizeRows = false;
            this._voltageDefensesGrid3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._voltageDefensesGrid3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._voltageDefensesGrid3.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._voltageDefensesGrid3.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle70.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle70.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle70.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle70.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle70.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle70.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle70.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._voltageDefensesGrid3.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle70;
            this._voltageDefensesGrid3.ColumnHeadersHeight = 30;
            this._voltageDefensesGrid3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._voltageDefensesGrid3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._voltageDefensesNameCol3,
            this._voltageDefensesModeCol3,
            this._voltageDefensesBlockingNumberCol3,
            this.Column5,
            this.Column6,
            this._voltageDefensesParameterCol3,
            this._voltageDefensesWorkConstraintCol3,
            this._voltageDefensesTimeConstraintCol3,
            this._voltageDefensesReturnCol3,
            this._voltageDefensesAPVreturnCol3,
            this._voltageDefensesReturnConstraintCol3,
            this._voltageDefensesReturnTimeCol3,
            this._voltageDefensesUROVcol3,
            this._voltageDefensesAPVcol3,
            this._voltageDefensesOSCv11Col3,
            this._voltageDefensesResetcol3,
            this.Column8});
            dataGridViewCellStyle74.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle74.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle74.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle74.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle74.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle74.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle74.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._voltageDefensesGrid3.DefaultCellStyle = dataGridViewCellStyle74;
            this._voltageDefensesGrid3.Location = new System.Drawing.Point(6, 351);
            this._voltageDefensesGrid3.Name = "_voltageDefensesGrid3";
            dataGridViewCellStyle75.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle75.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle75.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle75.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle75.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle75.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle75.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._voltageDefensesGrid3.RowHeadersDefaultCellStyle = dataGridViewCellStyle75;
            this._voltageDefensesGrid3.RowHeadersVisible = false;
            this._voltageDefensesGrid3.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle76.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._voltageDefensesGrid3.RowsDefaultCellStyle = dataGridViewCellStyle76;
            this._voltageDefensesGrid3.RowTemplate.Height = 24;
            this._voltageDefensesGrid3.Size = new System.Drawing.Size(867, 118);
            this._voltageDefensesGrid3.TabIndex = 13;
            // 
            // _voltageDefensesNameCol3
            // 
            dataGridViewCellStyle71.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._voltageDefensesNameCol3.DefaultCellStyle = dataGridViewCellStyle71;
            this._voltageDefensesNameCol3.Frozen = true;
            this._voltageDefensesNameCol3.HeaderText = "";
            this._voltageDefensesNameCol3.Name = "_voltageDefensesNameCol3";
            this._voltageDefensesNameCol3.ReadOnly = true;
            this._voltageDefensesNameCol3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesNameCol3.Width = 5;
            // 
            // _voltageDefensesModeCol3
            // 
            this._voltageDefensesModeCol3.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._voltageDefensesModeCol3.HeaderText = "�����";
            this._voltageDefensesModeCol3.Name = "_voltageDefensesModeCol3";
            this._voltageDefensesModeCol3.Width = 48;
            // 
            // _voltageDefensesBlockingNumberCol3
            // 
            this._voltageDefensesBlockingNumberCol3.HeaderText = "����������";
            this._voltageDefensesBlockingNumberCol3.Name = "_voltageDefensesBlockingNumberCol3";
            this._voltageDefensesBlockingNumberCol3.Width = 74;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Column5";
            this.Column5.Name = "Column5";
            this.Column5.Visible = false;
            this.Column5.Width = 73;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Column6";
            this.Column6.Name = "Column6";
            this.Column6.Visible = false;
            this.Column6.Width = 73;
            // 
            // _voltageDefensesParameterCol3
            // 
            dataGridViewCellStyle72.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this._voltageDefensesParameterCol3.DefaultCellStyle = dataGridViewCellStyle72;
            this._voltageDefensesParameterCol3.HeaderText = "��������";
            this._voltageDefensesParameterCol3.Name = "_voltageDefensesParameterCol3";
            this._voltageDefensesParameterCol3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._voltageDefensesParameterCol3.Visible = false;
            this._voltageDefensesParameterCol3.Width = 64;
            // 
            // _voltageDefensesWorkConstraintCol3
            // 
            this._voltageDefensesWorkConstraintCol3.HeaderText = "������� ����., �";
            this._voltageDefensesWorkConstraintCol3.Name = "_voltageDefensesWorkConstraintCol3";
            this._voltageDefensesWorkConstraintCol3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesWorkConstraintCol3.Width = 99;
            // 
            // _voltageDefensesTimeConstraintCol3
            // 
            dataGridViewCellStyle73.NullValue = "0";
            this._voltageDefensesTimeConstraintCol3.DefaultCellStyle = dataGridViewCellStyle73;
            this._voltageDefensesTimeConstraintCol3.HeaderText = "����� ����.";
            this._voltageDefensesTimeConstraintCol3.Name = "_voltageDefensesTimeConstraintCol3";
            this._voltageDefensesTimeConstraintCol3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesTimeConstraintCol3.Width = 76;
            // 
            // _voltageDefensesReturnCol3
            // 
            this._voltageDefensesReturnCol3.HeaderText = "����.";
            this._voltageDefensesReturnCol3.Name = "_voltageDefensesReturnCol3";
            this._voltageDefensesReturnCol3.Width = 41;
            // 
            // _voltageDefensesAPVreturnCol3
            // 
            this._voltageDefensesAPVreturnCol3.HeaderText = "��� ��";
            this._voltageDefensesAPVreturnCol3.Name = "_voltageDefensesAPVreturnCol3";
            this._voltageDefensesAPVreturnCol3.Width = 52;
            // 
            // _voltageDefensesReturnConstraintCol3
            // 
            this._voltageDefensesReturnConstraintCol3.HeaderText = "������� ��������, �";
            this._voltageDefensesReturnConstraintCol3.Name = "_voltageDefensesReturnConstraintCol3";
            this._voltageDefensesReturnConstraintCol3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesReturnConstraintCol3.Width = 119;
            // 
            // _voltageDefensesReturnTimeCol3
            // 
            this._voltageDefensesReturnTimeCol3.HeaderText = "����� ��������";
            this._voltageDefensesReturnTimeCol3.Name = "_voltageDefensesReturnTimeCol3";
            this._voltageDefensesReturnTimeCol3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesReturnTimeCol3.Width = 96;
            // 
            // _voltageDefensesUROVcol3
            // 
            this._voltageDefensesUROVcol3.HeaderText = "����";
            this._voltageDefensesUROVcol3.Name = "_voltageDefensesUROVcol3";
            this._voltageDefensesUROVcol3.Width = 43;
            // 
            // _voltageDefensesAPVcol3
            // 
            this._voltageDefensesAPVcol3.HeaderText = "���";
            this._voltageDefensesAPVcol3.Name = "_voltageDefensesAPVcol3";
            this._voltageDefensesAPVcol3.Width = 35;
            // 
            // _voltageDefensesOSCv11Col3
            // 
            this._voltageDefensesOSCv11Col3.HeaderText = "�����������";
            this._voltageDefensesOSCv11Col3.Name = "_voltageDefensesOSCv11Col3";
            this._voltageDefensesOSCv11Col3.Width = 82;
            // 
            // _voltageDefensesResetcol3
            // 
            this._voltageDefensesResetcol3.HeaderText = "�����";
            this._voltageDefensesResetcol3.Name = "_voltageDefensesResetcol3";
            this._voltageDefensesResetcol3.Width = 44;
            // 
            // Column8
            // 
            this.Column8.HeaderText = "Column8";
            this.Column8.Name = "Column8";
            this.Column8.Visible = false;
            this.Column8.Width = 54;
            // 
            // _frequencyDefendingTabPage
            // 
            this._frequencyDefendingTabPage.Controls.Add(this._frequenceDefensesGrid);
            this._frequencyDefendingTabPage.Location = new System.Drawing.Point(4, 22);
            this._frequencyDefendingTabPage.Name = "_frequencyDefendingTabPage";
            this._frequencyDefendingTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._frequencyDefendingTabPage.Size = new System.Drawing.Size(879, 534);
            this._frequencyDefendingTabPage.TabIndex = 2;
            this._frequencyDefendingTabPage.Text = "������ �� �������";
            this._frequencyDefendingTabPage.UseVisualStyleBackColor = true;
            // 
            // _frequenceDefensesGrid
            // 
            this._frequenceDefensesGrid.AllowUserToAddRows = false;
            this._frequenceDefensesGrid.AllowUserToDeleteRows = false;
            this._frequenceDefensesGrid.AllowUserToResizeColumns = false;
            this._frequenceDefensesGrid.AllowUserToResizeRows = false;
            this._frequenceDefensesGrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._frequenceDefensesGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this._frequenceDefensesGrid.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle77.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle77.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle77.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle77.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle77.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle77.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle77.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._frequenceDefensesGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle77;
            this._frequenceDefensesGrid.ColumnHeadersHeight = 30;
            this._frequenceDefensesGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._frequenceDefensesGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._frequenceDefensesName,
            this._frequenceDefensesMode,
            this._frequenceDefensesBlockingNumber,
            this._frequenceDefensesWorkConstraint,
            this._frequenceDefensesWorkTime,
            this._frequenceDefensesReturn,
            this._frequenceDefensesAPVReturn,
            this._frequenceDefensesConstraintAPV,
            this._frequenceDefensesReturnTime,
            this._frequenceDefensesUROV,
            this._frequenceDefensesAPV,
            this._frequenceDefensesOSCv11,
            this._frequenceDefensesReset});
            this._frequenceDefensesGrid.Cursor = System.Windows.Forms.Cursors.Default;
            dataGridViewCellStyle83.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle83.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle83.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle83.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle83.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle83.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle83.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesGrid.DefaultCellStyle = dataGridViewCellStyle83;
            this._frequenceDefensesGrid.Location = new System.Drawing.Point(6, 6);
            this._frequenceDefensesGrid.Name = "_frequenceDefensesGrid";
            dataGridViewCellStyle84.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle84.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle84.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle84.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle84.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle84.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle84.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._frequenceDefensesGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle84;
            this._frequenceDefensesGrid.RowHeadersVisible = false;
            this._frequenceDefensesGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._frequenceDefensesGrid.RowTemplate.Height = 24;
            this._frequenceDefensesGrid.Size = new System.Drawing.Size(867, 150);
            this._frequenceDefensesGrid.TabIndex = 6;
            // 
            // _frequenceDefensesName
            // 
            this._frequenceDefensesName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle78.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._frequenceDefensesName.DefaultCellStyle = dataGridViewCellStyle78;
            this._frequenceDefensesName.Frozen = true;
            this._frequenceDefensesName.HeaderText = "";
            this._frequenceDefensesName.MaxInputLength = 1;
            this._frequenceDefensesName.Name = "_frequenceDefensesName";
            this._frequenceDefensesName.ReadOnly = true;
            this._frequenceDefensesName.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._frequenceDefensesName.Width = 45;
            // 
            // _frequenceDefensesMode
            // 
            this._frequenceDefensesMode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._frequenceDefensesMode.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._frequenceDefensesMode.HeaderText = "�����";
            this._frequenceDefensesMode.Name = "_frequenceDefensesMode";
            this._frequenceDefensesMode.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // _frequenceDefensesBlockingNumber
            // 
            this._frequenceDefensesBlockingNumber.HeaderText = "����������";
            this._frequenceDefensesBlockingNumber.Name = "_frequenceDefensesBlockingNumber";
            this._frequenceDefensesBlockingNumber.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesBlockingNumber.Width = 74;
            // 
            // _frequenceDefensesWorkConstraint
            // 
            dataGridViewCellStyle79.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._frequenceDefensesWorkConstraint.DefaultCellStyle = dataGridViewCellStyle79;
            this._frequenceDefensesWorkConstraint.HeaderText = "������� ����., ��";
            this._frequenceDefensesWorkConstraint.MaxInputLength = 8;
            this._frequenceDefensesWorkConstraint.Name = "_frequenceDefensesWorkConstraint";
            this._frequenceDefensesWorkConstraint.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesWorkConstraint.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._frequenceDefensesWorkConstraint.Width = 104;
            // 
            // _frequenceDefensesWorkTime
            // 
            dataGridViewCellStyle80.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle80.NullValue = "0";
            this._frequenceDefensesWorkTime.DefaultCellStyle = dataGridViewCellStyle80;
            this._frequenceDefensesWorkTime.HeaderText = "����� ����., ��";
            this._frequenceDefensesWorkTime.MaxInputLength = 8;
            this._frequenceDefensesWorkTime.Name = "_frequenceDefensesWorkTime";
            this._frequenceDefensesWorkTime.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesWorkTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._frequenceDefensesWorkTime.Width = 96;
            // 
            // _frequenceDefensesReturn
            // 
            this._frequenceDefensesReturn.HeaderText = "����.";
            this._frequenceDefensesReturn.Name = "_frequenceDefensesReturn";
            this._frequenceDefensesReturn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesReturn.Width = 41;
            // 
            // _frequenceDefensesAPVReturn
            // 
            this._frequenceDefensesAPVReturn.HeaderText = "��� ��";
            this._frequenceDefensesAPVReturn.Name = "_frequenceDefensesAPVReturn";
            this._frequenceDefensesAPVReturn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesAPVReturn.Width = 52;
            // 
            // _frequenceDefensesConstraintAPV
            // 
            dataGridViewCellStyle81.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._frequenceDefensesConstraintAPV.DefaultCellStyle = dataGridViewCellStyle81;
            this._frequenceDefensesConstraintAPV.HeaderText = "������� ��, ��";
            this._frequenceDefensesConstraintAPV.Name = "_frequenceDefensesConstraintAPV";
            this._frequenceDefensesConstraintAPV.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesConstraintAPV.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._frequenceDefensesConstraintAPV.Width = 91;
            // 
            // _frequenceDefensesReturnTime
            // 
            dataGridViewCellStyle82.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._frequenceDefensesReturnTime.DefaultCellStyle = dataGridViewCellStyle82;
            this._frequenceDefensesReturnTime.HeaderText = "����� ��������, ��";
            this._frequenceDefensesReturnTime.Name = "_frequenceDefensesReturnTime";
            this._frequenceDefensesReturnTime.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesReturnTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._frequenceDefensesReturnTime.Width = 116;
            // 
            // _frequenceDefensesUROV
            // 
            this._frequenceDefensesUROV.HeaderText = "����";
            this._frequenceDefensesUROV.Name = "_frequenceDefensesUROV";
            this._frequenceDefensesUROV.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesUROV.Width = 43;
            // 
            // _frequenceDefensesAPV
            // 
            this._frequenceDefensesAPV.HeaderText = "���";
            this._frequenceDefensesAPV.Name = "_frequenceDefensesAPV";
            this._frequenceDefensesAPV.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesAPV.Width = 35;
            // 
            // _frequenceDefensesOSCv11
            // 
            this._frequenceDefensesOSCv11.HeaderText = "�����������";
            this._frequenceDefensesOSCv11.Name = "_frequenceDefensesOSCv11";
            this._frequenceDefensesOSCv11.Width = 82;
            // 
            // _frequenceDefensesReset
            // 
            this._frequenceDefensesReset.HeaderText = "�����";
            this._frequenceDefensesReset.Name = "_frequenceDefensesReset";
            this._frequenceDefensesReset.Width = 44;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this._reserveRadioButtonGroup);
            this.groupBox5.Controls.Add(this._ChangeSetPointsButton);
            this.groupBox5.Controls.Add(this._mainRadioButtonGroup);
            this.groupBox5.Location = new System.Drawing.Point(3, 7);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(393, 50);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "������ �������";
            // 
            // _reserveRadioButtonGroup
            // 
            this._reserveRadioButtonGroup.AutoSize = true;
            this._reserveRadioButtonGroup.Location = new System.Drawing.Point(88, 19);
            this._reserveRadioButtonGroup.Name = "_reserveRadioButtonGroup";
            this._reserveRadioButtonGroup.Size = new System.Drawing.Size(80, 17);
            this._reserveRadioButtonGroup.TabIndex = 3;
            this._reserveRadioButtonGroup.TabStop = true;
            this._reserveRadioButtonGroup.Text = "���������";
            this._reserveRadioButtonGroup.UseVisualStyleBackColor = true;
            // 
            // _ChangeSetPointsButton
            // 
            this._ChangeSetPointsButton.Location = new System.Drawing.Point(188, 17);
            this._ChangeSetPointsButton.Name = "_ChangeSetPointsButton";
            this._ChangeSetPointsButton.Size = new System.Drawing.Size(177, 23);
            this._ChangeSetPointsButton.TabIndex = 2;
            this._ChangeSetPointsButton.Text = "�������� --> ���������";
            this._ChangeSetPointsButton.UseVisualStyleBackColor = true;
            // 
            // _mainRadioButtonGroup
            // 
            this._mainRadioButtonGroup.AutoSize = true;
            this._mainRadioButtonGroup.Checked = true;
            this._mainRadioButtonGroup.Location = new System.Drawing.Point(7, 20);
            this._mainRadioButtonGroup.Name = "_mainRadioButtonGroup";
            this._mainRadioButtonGroup.Size = new System.Drawing.Size(75, 17);
            this._mainRadioButtonGroup.TabIndex = 0;
            this._mainRadioButtonGroup.TabStop = true;
            this._mainRadioButtonGroup.Text = "��������";
            this._mainRadioButtonGroup.UseVisualStyleBackColor = true;
            // 
            // _resetConfigBut
            // 
            this._resetConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._resetConfigBut.Location = new System.Drawing.Point(322, 676);
            this._resetConfigBut.Name = "_resetConfigBut";
            this._resetConfigBut.Size = new System.Drawing.Size(158, 23);
            this._resetConfigBut.TabIndex = 12;
            this._resetConfigBut.Text = "�������� �������";
            this._resetConfigBut.UseVisualStyleBackColor = true;
            this._resetConfigBut.Click += new System.EventHandler(this._resetConfigBut_Click);
            // 
            // _saveToXmlButton
            // 
            this._saveToXmlButton.AllowDrop = true;
            this._saveToXmlButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._saveToXmlButton.AutoSize = true;
            this._saveToXmlButton.Location = new System.Drawing.Point(756, 676);
            this._saveToXmlButton.Name = "_saveToXmlButton";
            this._saveToXmlButton.Size = new System.Drawing.Size(136, 23);
            this._saveToXmlButton.TabIndex = 32;
            this._saveToXmlButton.Text = "��������� � HTML";
            this._saveToXmlButton.UseVisualStyleBackColor = true;
            this._saveToXmlButton.Click += new System.EventHandler(this._saveToXmlButton_Click);
            // 
            // _saveXmlDialog
            // 
            this._saveXmlDialog.Filter = "(*.html) | *.html";
            // 
            // Mr750ConfigurationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(906, 724);
            this.Controls.Add(this._saveToXmlButton);
            this.Controls.Add(this._resetConfigBut);
            this.Controls.Add(this._saveConfigBut);
            this.Controls.Add(this._loadConfigBut);
            this.Controls.Add(this._writeConfigBut);
            this.Controls.Add(this._statusStrip);
            this.Controls.Add(this._readConfigBut);
            this.Controls.Add(this._tabControl);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(920, 726);
            this.Name = "Mr750ConfigurationForm";
            this.Text = "ConfigurationForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Mr700ConfigurationFormV2_FormClosing);
            this.Load += new System.EventHandler(this.ConfigurationForm_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Mr750ConfigurationForm_KeyUp);
            this._statusStrip.ResumeLayout(false);
            this._statusStrip.PerformLayout();
            this._tabControl.ResumeLayout(false);
            this.contextMenu.ResumeLayout(false);
            this._inSignalsPage.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox29.ResumeLayout(false);
            this._box.ResumeLayout(false);
            this._box.PerformLayout();
            this.groupBox24.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals9)).EndInit();
            this.tabPage10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals10)).EndInit();
            this.tabPage11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals11)).EndInit();
            this.tabPage12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals12)).EndInit();
            this.groupBox27.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals2)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals3)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals4)).EndInit();
            this.gb3.ResumeLayout(false);
            this.gb3.PerformLayout();
            this.gb2.ResumeLayout(false);
            this.gb1.ResumeLayout(false);
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox22.ResumeLayout(false);
            this.groupBox22.PerformLayout();
            this.groupBox21.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this._outputSignalsPage.ResumeLayout(false);
            this.VLSTabControl.ResumeLayout(false);
            this.VLS1.ResumeLayout(false);
            this.VLS2.ResumeLayout(false);
            this.VLS3.ResumeLayout(false);
            this.VLS4.ResumeLayout(false);
            this.VLS5.ResumeLayout(false);
            this.VLS6.ResumeLayout(false);
            this.VLS7.ResumeLayout(false);
            this.VLS8.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._outputIndicatorsGrid)).EndInit();
            this._externalDefensePage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._externalDefenseGrid)).EndInit();
            this._automaticPage.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this._newDefendingTabPage.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.tabControl3.ResumeLayout(false);
            this._tokDefendingTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._tokDefenseGrid3)).EndInit();
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._tokDefenseGrid4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._tokDefenseGrid2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._tokDefenseGrid1)).EndInit();
            this._voltageDefendingTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._voltageDefensesGrid11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._voltageDefensesGrid2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._voltageDefensesGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._voltageDefensesGrid3)).EndInit();
            this._frequencyDefendingTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._frequenceDefensesGrid)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox apv_blocking;
        private System.Windows.Forms.ComboBox apv_conf;
        private System.Windows.Forms.Button _saveConfigBut;
        private System.Windows.Forms.Button _loadConfigBut;
        private System.Windows.Forms.ToolTip _toolTip;
        private System.Windows.Forms.Button _writeConfigBut;
        private System.Windows.Forms.SaveFileDialog _saveConfigurationDlg;
        private System.Windows.Forms.ToolStripStatusLabel _statusLabel;
        private System.Windows.Forms.ToolStripProgressBar _exchangeProgressBar;
        private System.Windows.Forms.OpenFileDialog _openConfigurationDlg;
        private System.Windows.Forms.StatusStrip _statusStrip;
        private System.Windows.Forms.Button _readConfigBut;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.MaskedTextBox apv_time_4krat;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.MaskedTextBox apv_time_3krat;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.MaskedTextBox apv_time_2krat;
        private System.Windows.Forms.TabControl _tabControl;
        private System.Windows.Forms.TabPage _outputSignalsPage;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.DataGridView _outputIndicatorsGrid;
        private System.Windows.Forms.TabPage _externalDefensePage;
        private System.Windows.Forms.DataGridView _externalDefenseGrid;
        private System.Windows.Forms.TabPage _automaticPage;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.MaskedTextBox avr_disconnection;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.MaskedTextBox avr_time_return;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.ComboBox avr_reset_blocking;
        private System.Windows.Forms.ComboBox avr_blocking;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.MaskedTextBox lzsh_constraint;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.MaskedTextBox apv_time_1krat;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.MaskedTextBox apv_time_ready;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.MaskedTextBox apv_time_blocking;
        private System.Windows.Forms.TabPage _inSignalsPage;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.MaskedTextBox _HUD_Box;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.ComboBox _manageSignalsSDTU_Combo;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.ComboBox _manageSignalsExternalCombo;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.ComboBox _manageSignalsKeyCombo;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.ComboBox _manageSignalsButtonCombo;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox22;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.MaskedTextBox _releDispepairBox;
        private System.Windows.Forms.GroupBox groupBox21;
        private System.Windows.Forms.CheckedListBox _dispepairCheckList;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.MaskedTextBox _switcherDurationBox;
        private System.Windows.Forms.MaskedTextBox _switcherTokBox;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.MaskedTextBox _switcherTimeBox;
        private System.Windows.Forms.MaskedTextBox _switcherImpulseBox;
        private System.Windows.Forms.ComboBox _switcherBlockCombo;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox _switcherErrorCombo;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox _switcherStateOnCombo;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox _switcherStateOffCombo;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox _constraintGroupCombo;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox _signalizationCombo;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox _extOnCombo;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox _extOffCombo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox _keyOnCombo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox _keyOffCombo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox _box;
        private System.Windows.Forms.ComboBox _TNNP_dispepairCombo;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.ComboBox _TN_dispepairCombo;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox _TNNP_Box;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.MaskedTextBox _TN_Box;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.MaskedTextBox _maxTok_Box;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MaskedTextBox _TTNP_Box;
        private System.Windows.Forms.MaskedTextBox _TT_Box;
        private System.Windows.Forms.ComboBox _TT_typeCombo;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.GroupBox gb1;
        private System.Windows.Forms.ComboBox oscLength;
        private System.Windows.Forms.GroupBox gb3;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.MaskedTextBox oscPercent;
        private System.Windows.Forms.GroupBox gb2;
        private System.Windows.Forms.ComboBox oscFix;
        private System.Windows.Forms.GroupBox groupBox24;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.DataGridView _inputSignals9;
        private System.Windows.Forms.DataGridViewTextBoxColumn _signalValueNumILI;
        private System.Windows.Forms.DataGridViewComboBoxColumn _signalValueColILI;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.DataGridView _inputSignals10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn8;
        private System.Windows.Forms.TabPage tabPage11;
        private System.Windows.Forms.DataGridView _inputSignals11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn9;
        private System.Windows.Forms.TabPage tabPage12;
        private System.Windows.Forms.DataGridView _inputSignals12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn10;
        private System.Windows.Forms.GroupBox groupBox27;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView _inputSignals1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _lsChannelCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _signalValueCol;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView _inputSignals2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView _inputSignals3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn2;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.DataGridView _inputSignals4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn3;
        private System.Windows.Forms.CheckBox _apvStartCheckBox;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TabControl VLSTabControl;
        private System.Windows.Forms.TabPage VLS1;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox1;
        private System.Windows.Forms.TabPage VLS2;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox2;
        private System.Windows.Forms.TabPage VLS3;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox3;
        private System.Windows.Forms.TabPage VLS4;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox4;
        private System.Windows.Forms.TabPage VLS5;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox5;
        private System.Windows.Forms.TabPage VLS6;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox6;
        private System.Windows.Forms.TabPage VLS7;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox7;
        private System.Windows.Forms.TabPage VLS8;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox8;
        private System.Windows.Forms.GroupBox groupBox29;
        private System.Windows.Forms.CheckedListBox _keysCheckedListBox;
        private System.Windows.Forms.Button _resetConfigBut;
        private System.Windows.Forms.CheckBox _ompCheckBox;
        private System.Windows.Forms.Button _saveToXmlButton;
        private System.Windows.Forms.SaveFileDialog _saveXmlDialog;
        private System.Windows.Forms.DataGridViewTextBoxColumn _outIndNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _outIndTypeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _outIndSignalCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _outIndResetCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _outIndAlarmCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _outIndSystemCol;
        private System.Windows.Forms.ComboBox _f;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox _uca;
        private System.Windows.Forms.Label label123;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.ComboBox _side2;
        private System.Windows.Forms.ComboBox _side1;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.MaskedTextBox _umax2;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.MaskedTextBox _umin2;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.MaskedTextBox _umax1;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.MaskedTextBox _umin1;
        private System.Windows.Forms.CheckBox avr_mode;
        private System.Windows.Forms.TabPage _newDefendingTabPage;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button _ChangeSetPointsButton;
        private System.Windows.Forms.RadioButton _mainRadioButtonGroup;
        private System.Windows.Forms.RadioButton _reserveRadioButtonGroup;
        private System.Windows.Forms.TabControl tabControl3;
        private System.Windows.Forms.TabPage _tokDefendingTabPage;
        private System.Windows.Forms.TabPage _voltageDefendingTabPage;
        private System.Windows.Forms.TabPage _frequencyDefendingTabPage;
        private System.Windows.Forms.DataGridView _frequenceDefensesGrid;
        private System.Windows.Forms.DataGridView _voltageDefensesGrid11;
        private System.Windows.Forms.DataGridView _voltageDefensesGrid2;
        private System.Windows.Forms.DataGridView _voltageDefensesGrid1;
        private System.Windows.Forms.DataGridView _voltageDefensesGrid3;
        private System.Windows.Forms.DataGridView _tokDefenseGrid3;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.MaskedTextBox _tokDefenseInbox;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.MaskedTextBox _tokDefenseI2box;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.MaskedTextBox _tokDefenseI0box;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.MaskedTextBox _tokDefenseIbox;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.DataGridView _tokDefenseGrid4;
        private System.Windows.Forms.DataGridView _tokDefenseGrid2;
        private System.Windows.Forms.DataGridView _tokDefenseGrid1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefenseNameCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseModeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseBlockNumberCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefenseU_PuskCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefensePuskConstraintCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseDirectionCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseBlockExistCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseParameterCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefenseWorkConstraintCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseFeatureCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefenseWorkTimeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefenseSpeedUpCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefenseSpeedupTimeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefenseUROVCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefenseAPVCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseOSCv11Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn11;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn12;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn13;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn9;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn10;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense3NameCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense3ModeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense3BlockingNumberCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense3UpuskCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense3PuskConstraintCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense3DirectionCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense3BlockingExistCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense3ParameterCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense3WorkConstraintCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense3FeatureCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense3WorkTimeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense3SpeedupCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense3SpeedupTimeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense3UROVCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense3APVCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense3OSCv11Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesNameCol2;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesModeCol2;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesBlockingNumberCol2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesParameterCol2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesWorkConstraintCol2;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesWorkTimeCol2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesReturnCol2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesAPVreturnCol2;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesReturnConstraintCol2;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesReturnTimeCol2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesUROVcol2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesAPVcol2;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesOSC�11Col2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesResetCol2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesNameCol3;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesModeCol3;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesBlockingNumberCol3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesParameterCol3;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesWorkConstraintCol3;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesTimeConstraintCol3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesReturnCol3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesAPVreturnCol3;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesReturnConstraintCol3;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesReturnTimeCol3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesUROVcol3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesAPVcol3;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesOSCv11Col3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesResetcol3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense4NameCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense4ModeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense4BlockNumberCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense4UpuskCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense4PuskConstraintCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense4WorkConstraintCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense4WorkTimeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense4SpeedupCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense4SpeedupTimeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense4UROVCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense4APVCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense4OSCv11Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesNameCol1;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesModeCol1;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesBlockingNumberCol1;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesParameterCol1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesWorkConstraintCol1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesWorkTimeCol1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesReturnCol1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesAPVReturlCol1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesReturnConstraintCol1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesReturnTimeCol1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesUROVCol1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesAPVCol1;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesOSCv11Col1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesResetCol1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _u5vColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _frequenceDefensesName;
        private System.Windows.Forms.DataGridViewComboBoxColumn _frequenceDefensesMode;
        private System.Windows.Forms.DataGridViewComboBoxColumn _frequenceDefensesBlockingNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn _frequenceDefensesWorkConstraint;
        private System.Windows.Forms.DataGridViewTextBoxColumn _frequenceDefensesWorkTime;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _frequenceDefensesReturn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _frequenceDefensesAPVReturn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _frequenceDefensesConstraintAPV;
        private System.Windows.Forms.DataGridViewTextBoxColumn _frequenceDefensesReturnTime;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _frequenceDefensesUROV;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _frequenceDefensesAPV;
        private System.Windows.Forms.DataGridViewComboBoxColumn _frequenceDefensesOSCv11;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _frequenceDefensesReset;
        private System.Windows.Forms.Label noUaccLabel;
        private System.Windows.Forms.MaskedTextBox noUaccBox;
        private System.Windows.Forms.ComboBox disableComboBox;
        private System.Windows.Forms.Label disableLabel;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.ComboBox connectionPort;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn4;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn5;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn4;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn7;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn6;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn _extDefNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _exDefModeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _exDefBlockingCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _exDefWorkingCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _exDefWorkingTimeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _exDefReturnCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _exDefAPVreturnCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _exDefReturnNumberCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _exDefReturnTimeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _exDefUROVcol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _exDefAPVcol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _exDefAVRcol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _exDefOSCcol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _exDefResetcol;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem readFromDeviceItem;
        private System.Windows.Forms.ToolStripMenuItem writeToDeviceItem;
        private System.Windows.Forms.ToolStripMenuItem clearSetpointsItem;
        private System.Windows.Forms.ToolStripMenuItem readFromFileItem;
        private System.Windows.Forms.ToolStripMenuItem writeToFileItem;
        private System.Windows.Forms.ToolStripMenuItem writeToHtmlItem;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}