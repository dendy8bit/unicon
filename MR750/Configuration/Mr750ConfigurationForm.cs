using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Xml;
using AssemblyResources;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms.Export;
using BEMN.Forms.ValidatingClasses;
using BEMN.Forms.ValidatingClasses.Dgw;
using BEMN.Forms.ValidatingClasses.New;
using BEMN.Forms.ValidatingClasses.New.ColumnsInfos;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Forms.ValidatingClasses.New.Validators.TurnOff;
using BEMN.Forms.ValidatingClasses.Rules;
using BEMN.Forms.ValidatingClasses.Rules.Ushort;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR750.Configuration.Structures;
using BEMN.MR750.Configuration.Structures.Apv;
using BEMN.MR750.Configuration.Structures.Avr;
using BEMN.MR750.Configuration.Structures.CurrentDefenses;
using BEMN.MR750.Configuration.Structures.ExternalDefenses;
using BEMN.MR750.Configuration.Structures.ExternalSignals;
using BEMN.MR750.Configuration.Structures.FaultSignal;
using BEMN.MR750.Configuration.Structures.FrequencyDefenses;
using BEMN.MR750.Configuration.Structures.Indicators;
using BEMN.MR750.Configuration.Structures.Keys;
using BEMN.MR750.Configuration.Structures.Ls;
using BEMN.MR750.Configuration.Structures.Lzsh;
using BEMN.MR750.Configuration.Structures.MeasuringTransformer;
using BEMN.MR750.Configuration.Structures.Switch;
using BEMN.MR750.Configuration.Structures.SystenConfig;
using BEMN.MR750.Configuration.Structures.Vls;
using BEMN.MR750.Configuration.Structures.VoltageDefenses;

namespace BEMN.MR750.Configuration
{
    public partial class Mr750ConfigurationForm : Form, IFormView
    {
        #region [Private fields]
        private readonly Mr750Device _device;
        private readonly MemoryEntity<ConfigurationStructV2> _configuration;
        /// <summary>
        /// ������ ��������� ��� ��
        /// </summary>
        private DataGridView[] _lsBoxes;

        private ConfigurationStructV2 _currentSetpointsStructV2;

        #region [Validators]
        private NewStructValidator<MeasureTransStruct> _measureTransValidator;
        private NewCheckedListBoxValidator<KeysStruct> _keysValidator;
        private NewStructValidator<ExternalSignalStruct> _externalSignalsValidator;
        private NewStructValidator<FaultStruct> _faultValidator;

        private NewDgwValidatior<InputLogicStruct>[] _inputLogicValidator;
        private StructUnion<InputLogicSignalStruct> _inputLogicUnion;

        private NewStructValidator<SwitchStruct> _switchValidator;
        private NewStructValidator<ApvStruct> _apvValidator;
        private NewStructValidator<AvrStruct> _avrValidator;
        private NewStructValidator<LpbStruct> _lpbValidator;
        private NewDgwValidatior<AllExternalDefensesStruct, DefenseExternalStruct> _externalDefenseValidatior;

        private NewStructValidator<CornerStruct> _cornerValidator;
        private NewDgwValidatior<AllCurrentDefensesStruct, CurrentDefenseStruct> _currentDefenseValidatior;
        private NewDgwValidatior<AllAddCurrentDefensesStruct, AddCurrentDefenseStruct> _addCurrentDefenseValidator;
        private SetpointsValidator<AllSetpointsStruct, SetpointStruct> _currentSetpointsValidator;
        private StructUnion<SetpointStruct> _currentSetpointUnion;
        private SetpointsValidator<AllAddSetpointsStruct, AllAddCurrentDefensesStruct> _currentAddSetpointsValidator;

        private NewDgwValidatior<AllFrequencyDefensesStruct, FrequencyDefenseStruct> _frequencyDefenseValidatior;
        private SetpointsValidator<FrequencySetpointsStruct, AllFrequencyDefensesStruct> _frequencySetpoints;


        private NewDgwValidatior<AllVoltageDefensesStruct, VoltageDefenseStruct> _voltageDefenseValidatior;
        private SetpointsValidator<VoltageSetpointsStruct, AllVoltageDefensesStruct> _voltageSetpoints;

        #region [���]
        /// <summary>
        /// ������ ��������� ��� ���
        /// </summary>
        private CheckedListBox[] _vlsBoxes;
        private NewCheckedListBoxValidator<OutputLogicStruct>[] _vlsValidator;
        private StructUnion<OutputLogicSignalStruct> _vlsUnion;
        #endregion [���]

        // private NewDgwValidatior<AllReleOutputStruct, ReleOutputStruct> _releyValidator;
        private NewDgwValidatior<AllIndicatorsStruct, IndicatorsStruct> _indicatorValidator;
        private NewStructValidator<SystemConfigStruct> _oscValidator;
        private StructUnion<ConfigurationStructV2> _configurationUnion;
        #endregion [Validators]

        #endregion [Private fields]

        #region C'tor
        public Mr750ConfigurationForm()
        {
            this.InitializeComponent();
        }
        
        public Mr750ConfigurationForm(Mr750Device device)
        {
            this.InitializeComponent();
            this._device = device;
            this._configuration = this._device.ConfigurationV1;
            this._configuration.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadConfigOk);
            this._configuration.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this.IsProcess = false;
                this._statusLabel.Text = ERROR_READ_CONFIG;
                MessageBox.Show(ERROR_READ_CONFIG);
            });
            this._configuration.ReadOk += HandlerHelper.CreateHandler(this, () => this._exchangeProgressBar.PerformStep());
            this._configuration.WriteOk += HandlerHelper.CreateHandler(this, () => this._exchangeProgressBar.PerformStep());
            this._configuration.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this.IsProcess = false;
                this._statusLabel.Text = ERROR_WRITE_CONFIG;
                MessageBox.Show(ERROR_WRITE_CONFIG);

            });
            this._configuration.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {

                this._device.WriteConfiguration();
                this.IsProcess = false;
                this._statusLabel.Text = "������������ ������� ��������";

            });
            this._exchangeProgressBar.Maximum = this._configuration.Slots.Count;

            this.Init();
        }

        private void Init()
        {
            this._lsBoxes = new[]
                {
                    this._inputSignals1, this._inputSignals2, this._inputSignals3, this._inputSignals4, this._inputSignals9, this._inputSignals10, this._inputSignals11, this._inputSignals12

                };

            this._measureTransValidator = new NewStructValidator<MeasureTransStruct>
                (
                this._toolTip,

                new ControlInfoText(this._TT_Box, RulesContainer.UshortTo1500),
                new ControlInfoText(this._TTNP_Box, RulesContainer.UshortTo1000),
                new ControlInfoText(this._maxTok_Box, RulesContainer.Ustavka40),
                new ControlInfoCombo(this._TT_typeCombo, StringsConfig.TtType),
                new ControlInfoText(this._TN_Box, RulesContainer.Ustavka128000),
                 new ControlInfoCombo(this._TN_dispepairCombo, StringsConfig.LogicSignalsMr750),
                 new ControlInfoText(this._TNNP_Box, RulesContainer.Ustavka128000),
                 new ControlInfoCombo(this._TNNP_dispepairCombo, StringsConfig.LogicSignalsMr750),
                 new ControlInfoCheck(this._ompCheckBox),
                 new ControlInfoText(this._HUD_Box, RulesContainer.DoubleTo1)
                );
            this._externalSignalsValidator = new NewStructValidator<ExternalSignalStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this._keyOffCombo, StringsConfig.ExternalSignalsMr750),
                new ControlInfoCombo(this._keyOnCombo, StringsConfig.ExternalSignalsMr750),
                new ControlInfoCombo(this._extOffCombo, StringsConfig.ExternalSignalsMr750),
                new ControlInfoCombo(this._extOnCombo, StringsConfig.ExternalSignalsMr750),
                new ControlInfoCombo(this._signalizationCombo, StringsConfig.ExternalSignalsMr750),
                new ControlInfoCombo(this._constraintGroupCombo, StringsConfig.ExternalSignalsMr750),
                new ControlInfoCombo(this._uca, StringsConfig.Uca),
                new ControlInfoCombo(this._f, StringsConfig.F),
                new ControlInfoCombo(this._side1, StringsConfig.Side),
                new ControlInfoCombo(this._side2, StringsConfig.Side),
                new ControlInfoText(this.noUaccBox, RulesContainer.Ustavka256),
                new ControlInfoCombo(this.disableComboBox, StringsConfig.ExternalSignalsMr750),
                new ControlInfoCombo(this.connectionPort, StringsConfig.ConnectionPort)
                );

            this._faultValidator = new NewStructValidator<FaultStruct>
                (
                this._toolTip,
                new ControlInfoValidator(new NewCheckedListBoxValidator<FaultSignalStruct>(this._dispepairCheckList, this.GetFaults())),
                new ControlInfoText(this._releDispepairBox, RulesContainer.IntTo3M)
                );

            this._inputLogicValidator = new NewDgwValidatior<InputLogicStruct>[InputLogicSignalStruct.LOGIC_COUNT];
            for (int i = 0; i < InputLogicSignalStruct.LOGIC_COUNT; i++)
            {
                this._inputLogicValidator[i] = new NewDgwValidatior<InputLogicStruct>
              (
              this._lsBoxes[i],
             8,
              this._toolTip,
              new ColumnInfoCombo(StringsConfig.LsSignals, ColumnsType.NAME),
              new ColumnInfoCombo(StringsConfig.LsState)
              );
            }
            this._inputLogicUnion = new StructUnion<InputLogicSignalStruct>(this._inputLogicValidator);

            this._switchValidator = new NewStructValidator<SwitchStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this._switcherStateOffCombo, StringsConfig.ExternalSignalsMr750),
                new ControlInfoCombo(this._switcherStateOnCombo, StringsConfig.ExternalSignalsMr750),
                new ControlInfoCombo(this._switcherErrorCombo, StringsConfig.ExternalSignalsMr750),
                new ControlInfoCombo(this._switcherBlockCombo, StringsConfig.ExternalSignalsMr750),
                new ControlInfoText(this._switcherTimeBox, RulesContainer.IntTo3M),
                new ControlInfoText(this._switcherTokBox, RulesContainer.Ustavka40),
                new ControlInfoText(this._switcherImpulseBox, RulesContainer.IntTo3M),
                new ControlInfoText(this._switcherDurationBox, RulesContainer.IntTo3M),

                new ControlInfoCombo(this._manageSignalsButtonCombo, StringsConfig.Zr),
                new ControlInfoCombo(this._manageSignalsKeyCombo, StringsConfig.Cr),
                new ControlInfoCombo(this._manageSignalsExternalCombo, StringsConfig.Cr),
                new ControlInfoCombo(this._manageSignalsSDTU_Combo, StringsConfig.Zr)
                );

            this._apvValidator = new NewStructValidator<ApvStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this.apv_conf, StringsConfig.ApvModes),
                new ControlInfoCombo(this.apv_blocking, StringsConfig.ExternalSignalsMr750),
                new ControlInfoText(this.apv_time_blocking, RulesContainer.IntTo3M),
                new ControlInfoText(this.apv_time_ready, RulesContainer.IntTo3M),
                new ControlInfoText(this.apv_time_1krat, RulesContainer.IntTo3M),
                new ControlInfoText(this.apv_time_2krat, RulesContainer.IntTo3M),
                new ControlInfoText(this.apv_time_3krat, RulesContainer.IntTo3M),
                new ControlInfoText(this.apv_time_4krat, RulesContainer.IntTo3M),
                new ControlInfoCheck(this._apvStartCheckBox)

                );
            this._avrValidator = new NewStructValidator<AvrStruct>
                (
                this._toolTip,
                new ControlInfoCheck(this.avr_mode),
                new ControlInfoCombo(this.avr_blocking, StringsConfig.ExternalSignalsMr750),
                new ControlInfoCombo(this.avr_reset_blocking, StringsConfig.ExternalSignalsMr750),

                new ControlInfoText(this._umin1, RulesContainer.Ustavka256),
                new ControlInfoText(this._umax1, RulesContainer.Ustavka256),
                new ControlInfoText(this._umin2, RulesContainer.Ustavka256),
                new ControlInfoText(this._umax2, RulesContainer.Ustavka256),
                new ControlInfoText(this.avr_time_return, RulesContainer.TimeRule),
                new ControlInfoText(this.avr_disconnection, RulesContainer.TimeRule)
                );
            this._lpbValidator = new NewStructValidator<LpbStruct>
                  (
                  this._toolTip,
                  new ControlInfoCombo(this.comboBox1, StringsConfig.Lzs),
                  new ControlInfoText(this.lzsh_constraint, RulesContainer.Ustavka40)
                  );

            this._externalDefenseValidatior = new NewDgwValidatior<AllExternalDefensesStruct, DefenseExternalStruct>
                (this._externalDefenseGrid,
                8,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.ExternalDefensesNames, ColumnsType.NAME),//1
                new ColumnInfoCombo(StringsConfig.CurModesV1),//2
                new ColumnInfoCombo(StringsConfig.ExternalDefenceSignalsMr750),//3
                new ColumnInfoCombo(StringsConfig.ExternalDefenceSignalsMr750),//4
                new ColumnInfoText(RulesContainer.IntTo3M),//5
                new ColumnInfoCheck(),

                new ColumnInfoCheck(),
                new ColumnInfoCombo(StringsConfig.ExternalDefenceSignalsMr750),
                new ColumnInfoText(RulesContainer.IntTo3M),

                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck()
                )
            {
                TurnOff = new[]{new TurnOffDgv
                        (
                        this._externalDefenseGrid,
                        new TurnOffRule(1, StringsConfig.CurModesV1[0],true,2,3,4,5,6,7,8,9,10,11,12,13), 
                        new TurnOffRule(5, false,false,6, 7,8)
                        )}
            };
            var currentDefenseFunc = new Func<IValidatingRule>(() =>
            {
                try
                {
                    if (this._tokDefenseGrid1[11, this._tokDefenseGrid1.CurrentCell.RowIndex].Value.ToString() ==
                        StringsConfig.FeatureLight[1])
                    {
                        return new CustomUshortRule(0, 4000);
                    }
                    return RulesContainer.IntTo3M;
                }
                catch (Exception)
                {
                    return RulesContainer.IntTo3M;
                }


            });

            this._cornerValidator = new NewStructValidator<CornerStruct>
                (
                this._toolTip,
                new ControlInfoText(this._tokDefenseIbox, RulesContainer.UshortTo360),
                new ControlInfoText(this._tokDefenseI0box, RulesContainer.UshortTo360),
                new ControlInfoText(this._tokDefenseI2box, RulesContainer.UshortTo360),
                new ControlInfoText(this._tokDefenseInbox, RulesContainer.UshortTo360)
                );

            this._currentDefenseValidatior = new NewDgwValidatior<AllCurrentDefensesStruct, CurrentDefenseStruct>
                (
                new[] {this._tokDefenseGrid1, this._tokDefenseGrid2, this._tokDefenseGrid3 },
                new[] { 4, 4, 2 },
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.CurrentDefensesNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.CurModesV1), //0
                new ColumnInfoCombo(StringsConfig.ExternalSignalsMr750), //1
                new ColumnInfoCheck(), //2 
                new ColumnInfoText(RulesContainer.Ustavka256), //3
                new ColumnInfoCombo(StringsConfig.Direction), //4
                new ColumnInfoCombo(StringsConfig.DirectionBlock), //5
                new ColumnInfoCombo(StringsConfig.TokParameter, ColumnsType.COMBO, true, false, false), //6
                new ColumnInfoCombo(StringsConfig.TokParameter2, ColumnsType.COMBO, false, true, true), //6
                new ColumnInfoText(RulesContainer.Ustavka40,true,true,false), //7        
                new ColumnInfoText(RulesContainer.Ustavka5,false,false,true),
                new ColumnInfoCombo(StringsConfig.FeatureLight, ColumnsType.COMBO, true, false, false), //8              
                new ColumnInfoTextDependent(currentDefenseFunc), //9
                new ColumnInfoCheck(), //10
                new ColumnInfoText(RulesContainer.IntTo3M), //11
                new ColumnInfoCheck(), //12
                new ColumnInfoCheck(), //13
                new ColumnInfoCombo(StringsConfig.OscV111) //15
                )
                {
                    TurnOff = new[]
                        {
                            new TurnOffDgv
                                (
                                this._tokDefenseGrid1,
                                new TurnOffRule(1, StringsConfig.CurModesV1[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,12,13,14,15,16,17),
                                new TurnOffRule(3, false, false, 4),
                                new TurnOffRule(5, StringsConfig.Direction[0], false, 6),
                                new TurnOffRule(12, false, false, 13)
                                ),
                            new TurnOffDgv
                                (
                                this._tokDefenseGrid2,
                                new TurnOffRule(1, StringsConfig.CurModesV1[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,12,13,14,15,16,17),
                                new TurnOffRule(3, false, false, 4),
                                new TurnOffRule(5, StringsConfig.Direction[0], false, 6),
                                new TurnOffRule(12, false, false, 13)
                                ),
                                new TurnOffDgv
                                (
                                this._tokDefenseGrid3,
                                new TurnOffRule(1, StringsConfig.CurModesV1[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,12,13,14,15,16,17),
                                new TurnOffRule(3, false, false, 4),
                                new TurnOffRule(5, StringsConfig.Direction[0], false, 6),
                                new TurnOffRule(12, false, false, 13)
                                )

                        }
                };

            this._addCurrentDefenseValidator = new NewDgwValidatior<AllAddCurrentDefensesStruct, AddCurrentDefenseStruct>
                (this._tokDefenseGrid4,
                2,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.AddCurrentDefensesNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.CurModesV1), //0
                new ColumnInfoCombo(StringsConfig.ExternalSignalsMr750), //1
                new ColumnInfoCheck(), //2 

                new ColumnInfoText(RulesContainer.Ustavka256), //3

                new ColumnInfoText(RulesContainer.Ustavka5), //4 � ����������� �� ������!
                new ColumnInfoText(RulesContainer.IntTo3M), //5
                new ColumnInfoCheck(), //6
                new ColumnInfoText(RulesContainer.IntTo3M), //7
                new ColumnInfoCheck(), //8
                new ColumnInfoCheck(), //9
                new ColumnInfoCombo(StringsConfig.OscV111) //10
                )
                {
                    TurnOff = new[]
                        {
                            new TurnOffDgv
                                (
                                this._tokDefenseGrid4,
                                new TurnOffRule(1, StringsConfig.CurModesV1[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11),
                                new TurnOffRule(3, false, false, 4),
                                new TurnOffRule(7, false, false, 8)
                                )
                        },
                    AddRules = new[] { new TextboxCellRule(1, 5, RulesContainer.Ustavka100) },
                    Disabled = new[] { new Point(3, 1), new Point(4, 1), new Point(7, 1), new Point(8, 1) }
                };


            this._currentSetpointUnion = new StructUnion<SetpointStruct>
                (
                this._cornerValidator,
                this._currentDefenseValidatior
                );

            this._currentSetpointsValidator = new SetpointsValidator<AllSetpointsStruct, SetpointStruct>
                (
            new RadioButtonSelector(this._mainRadioButtonGroup, this._reserveRadioButtonGroup), this._currentSetpointUnion
                );

            this._currentAddSetpointsValidator = new SetpointsValidator<AllAddSetpointsStruct, AllAddCurrentDefensesStruct>
                (
                   new RadioButtonSelector(this._mainRadioButtonGroup, this._reserveRadioButtonGroup), this._addCurrentDefenseValidator
                );

            this._frequencyDefenseValidatior = new NewDgwValidatior<AllFrequencyDefensesStruct, FrequencyDefenseStruct>
                (this._frequenceDefensesGrid,
                4,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.FrequencyDefensesNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.CurModesV1), //0
                new ColumnInfoCombo(StringsConfig.ExternalDefenceSignalsMr750), //1
                new ColumnInfoText(RulesContainer.Ustavka40To60), //2 
                new ColumnInfoText(RulesContainer.IntTo3M), //3
                new ColumnInfoCheck(), //4
                new ColumnInfoCheck(), //5
                new ColumnInfoText(RulesContainer.Ustavka40To60), //6
                new ColumnInfoText(RulesContainer.IntTo3M), //7        
                new ColumnInfoCheck(), //8              
                new ColumnInfoCheck(), //9
                new ColumnInfoCombo(StringsConfig.OscV111), //11
                new ColumnInfoCheck() //12
                )
                {
                    TurnOff = new[]
                        {
                            new TurnOffDgv
                                (
                                this._frequenceDefensesGrid,
                                new TurnOffRule(1, StringsConfig.CurModesV1[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12),
                                new TurnOffRule(5, false, false, 6, 7, 8)
                                )
                        }
                };

            this._frequencySetpoints = new SetpointsValidator<FrequencySetpointsStruct, AllFrequencyDefensesStruct>
                (
                     new RadioButtonSelector(this._mainRadioButtonGroup, this._reserveRadioButtonGroup), this._frequencyDefenseValidatior

                );

            this._voltageDefenseValidatior = new NewDgwValidatior<AllVoltageDefensesStruct, VoltageDefenseStruct>
                (
                new[] {this._voltageDefensesGrid1, this._voltageDefensesGrid11, this._voltageDefensesGrid2, this._voltageDefensesGrid3 },
                new[] { 2, 2, 2, 2 },
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.VoltageDefensesNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.CurModesV1), //0
                new ColumnInfoCombo(StringsConfig.ExternalSignalsMr750), //1

                new ColumnInfoCombo(StringsConfig.VoltageParameterU, ColumnsType.COMBO, true, true, false, false), //1
                new ColumnInfoCombo(StringsConfig.VoltageParameter1, ColumnsType.COMBO, false, false, false, false), //1
                new ColumnInfoCombo(StringsConfig.VoltageParameter2, ColumnsType.COMBO, false, false, false, false), //1
                
                new ColumnInfoText(RulesContainer.Ustavka256), //2 
                new ColumnInfoText(RulesContainer.IntTo3M), //3
                new ColumnInfoCheck(), //4
                new ColumnInfoCheck(), //5
                new ColumnInfoText(RulesContainer.Ustavka256), //6
                new ColumnInfoText(RulesContainer.IntTo3M), //7        
                new ColumnInfoCheck(), //8              
                new ColumnInfoCheck(), //9
                new ColumnInfoCombo(StringsConfig.OscV111), //11
                new ColumnInfoCheck(), //12
                new ColumnInfoCheck(false, true, false, false)
                )
                {
                    TurnOff = new[]
                        {
                            new TurnOffDgv
                                (
                                this._voltageDefensesGrid1,
                                new TurnOffRule(1, StringsConfig.CurModesV1[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,14,15,16),
                                new TurnOffRule(8, false, false, 9, 10, 11)
                                ),
                                 new TurnOffDgv
                                (
                                this._voltageDefensesGrid11,
                                new TurnOffRule(1, StringsConfig.CurModesV1[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,14,15,16),
                                new TurnOffRule(8, false, false, 9, 10, 11)
                                ),
                                new TurnOffDgv
                                (
                                this._voltageDefensesGrid2,
                                new TurnOffRule(1, StringsConfig.CurModesV1[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,14,15),
                                new TurnOffRule(8, false, false, 9, 10, 11)
                                ),
                                new TurnOffDgv
                                (
                                this._voltageDefensesGrid3,
                                new TurnOffRule(1, StringsConfig.CurModesV1[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,14,15),
                                new TurnOffRule(8, false, false, 9, 10, 11)
                                )
                        }//,
                    //  Disabled = new[] { new Point(17, 0), new Point(17, 1) }
                };

            this._voltageSetpoints = new SetpointsValidator<VoltageSetpointsStruct, AllVoltageDefensesStruct>
                (
                     new RadioButtonSelector(this._mainRadioButtonGroup, this._reserveRadioButtonGroup), this._voltageDefenseValidatior

                );

            #region [���]
            this._vlsBoxes = new[]
                {
                    this.VLScheckedListBox1, this.VLScheckedListBox2, this.VLScheckedListBox3, this.VLScheckedListBox4, this.VLScheckedListBox5, this.VLScheckedListBox6, this.VLScheckedListBox7, this.VLScheckedListBox8
                };
            this._vlsValidator = new NewCheckedListBoxValidator<OutputLogicStruct>[OutputLogicSignalStruct.LOGIC_COUNT];
            for (int i = 0; i < OutputLogicSignalStruct.LOGIC_COUNT; i++)
            {
                this._vlsValidator[i] = new NewCheckedListBoxValidator<OutputLogicStruct>(this._vlsBoxes[i], StringsConfig.VlsSignalsMr750);
            }
            this._vlsUnion = new StructUnion<OutputLogicSignalStruct>(this._vlsValidator);
            #endregion [���]

            this._indicatorValidator = new NewDgwValidatior<AllIndicatorsStruct, IndicatorsStruct>
                (this._outputIndicatorsGrid,
                AllIndicatorsStruct.INDICATORS_COUNT,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.IndicatorNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.ReleyType),
                new ColumnInfoCombo(StringsConfig.RelaySignalsMr750),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck()
                );

            this._oscValidator = new NewStructValidator<SystemConfigStruct>
                (
                this._toolTip,
                new ControlInfoText(this.oscPercent, RulesContainer.Ushort1To100),
                new ControlInfoCombo(this.oscFix, StringsConfig.OscFixation),
                new ControlInfoCombo(this.oscLength, StringsConfig.OscSize)
                );

            this._keysValidator = new NewCheckedListBoxValidator<KeysStruct>(this._keysCheckedListBox, StringsConfig.KeyNumbers);

            this._configurationUnion = new StructUnion<ConfigurationStructV2>
                (this._measureTransValidator, this._externalSignalsValidator, this._faultValidator, this._inputLogicUnion, this._switchValidator, this._apvValidator, this._avrValidator, this._lpbValidator, this._externalDefenseValidatior, this._currentSetpointsValidator, this._currentAddSetpointsValidator, this._frequencySetpoints, this._voltageSetpoints, this._vlsUnion, this._indicatorValidator, this._oscValidator, this._keysValidator
                );
            this._currentSetpointsStructV2 = new ConfigurationStructV2();
            this._configurationUnion.Set(this._currentSetpointsStructV2);
            this._configurationUnion.Reset();

            this._groupSelector = new RadioButtonSelector(this._mainRadioButtonGroup, this._reserveRadioButtonGroup, this._ChangeSetPointsButton);
            this._groupSelector.NeedCopyGroup += this._groupSelector_NeedCopyGroup;

            if (Common.VersionConverter(this._device.DeviceVersion) > 1.0)
            {
                this.groupBox10.Visible = true;
                this._box.Size = new Size(203, 171);
                this.noUaccBox.Visible = this.noUaccLabel.Visible = true;
                this.groupBox3.Size = new Size(203, 153);
                this.disableComboBox.Visible = this.disableLabel.Visible = true;
            }
        }
        #endregion C'tor

        void _groupSelector_NeedCopyGroup()
        {
            this.WriteConfiguration();
            //�������� � ���������, ����� ��������
            if (this._groupSelector.SelectedGroup == 0)
            {
                this._currentSetpointsStructV2.AllAddSetpoints.Reserve = this._currentSetpointsStructV2.AllAddSetpoints.Main.Clone<AllAddCurrentDefensesStruct>();
                this._currentSetpointsStructV2.AllSetpoints.Reserv = this._currentSetpointsStructV2.AllSetpoints.Main.Clone<SetpointStruct>();
                this._currentSetpointsStructV2.AllVoltage.Reserve = this._currentSetpointsStructV2.AllVoltage.Main.Clone<AllVoltageDefensesStruct>();
                this._currentSetpointsStructV2.AllFrequency.Reserve = this._currentSetpointsStructV2.AllFrequency.Main.Clone<AllFrequencyDefensesStruct>();
            }
            else
            {
                this._currentSetpointsStructV2.AllAddSetpoints.Main = this._currentSetpointsStructV2.AllAddSetpoints.Reserve.Clone<AllAddCurrentDefensesStruct>();
                this._currentSetpointsStructV2.AllSetpoints.Main = this._currentSetpointsStructV2.AllSetpoints.Reserv.Clone<SetpointStruct>();
                this._currentSetpointsStructV2.AllVoltage.Main = this._currentSetpointsStructV2.AllVoltage.Reserve.Clone<AllVoltageDefensesStruct>();
                this._currentSetpointsStructV2.AllFrequency.Main = this._currentSetpointsStructV2.AllFrequency.Reserve.Clone<AllFrequencyDefensesStruct>();
            }
        }

        private void ReadConfigOk()
        {
            this.IsProcess = false;
            this._statusLabel.Text = CONFIG_READ_OK;
            string message;
            this._configurationUnion.Set(this._configuration.Value.Clone<ConfigurationStructV2>());
            if (!this._configurationUnion.Check(out message, false))
            {
                MessageBox.Show("� ���������� ���������� ������������ �������");
            }
        }

        /// <summary>
        /// ������ ������
        /// </summary>
        private void StartReadConfiguration()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._exchangeProgressBar.Value = 0;
            if (!this._device.MB.IsDisconnect)
            {
                this._device.ReadConfiguration();
                this.IsProcess = true;
                this._configuration.LoadStruct();
            }
            else
            {
                MessageBox.Show(INVALID_PORT);
            }
        }

        /// <summary>
        /// ������ ��� ������ � ������
        /// </summary>
        private bool WriteConfiguration()
        {
            string message;

            if (this._configurationUnion.Check(out message, true))
            {

                this._currentSetpointsStructV2 = this._configurationUnion.Get();
                return true;
            }
            else
            {
                MessageBox.Show("���������� ������������ �������. ������������ �� ����� ���� ��������.");
                return false;
            }
        }

        #region Misc
        private void _loadConfigBut_Click(object sender, EventArgs e)
        {
            this.ReadFromFile();
        }

        private void ReadFromFile()
        {
            this._exchangeProgressBar.Value = 0;
            if (DialogResult.OK == this._openConfigurationDlg.ShowDialog())
            {
                this.Deserialize(this._openConfigurationDlg.FileName, MR750);
            } 
        }

        private const string FILE_LOAD_FAIL = "���������� ��������� ����";
        /// <summary>
        /// �������� ������������ �� �����
        /// </summary>
        /// <param name="binFileName">��� �����</param>
        public void Deserialize(string binFileName, string head)
        {
            try
            {
                var doc = new XmlDocument();
                doc.Load(binFileName);

                var a = doc.FirstChild.SelectSingleNode(string.Format("{0}_SET_POINTS", head));

                byte[] values;
                bool old = doc.DocumentElement.Name == "��700_�������" || doc.DocumentElement.Name == "��74X_�������" || doc.DocumentElement.Name == "��750_�������";
                values = old ? this.LoadOldConfig(doc, doc.DocumentElement.Name) : Convert.FromBase64String(a.InnerText);


                var configurationStruct = new ConfigurationStructV2();
                configurationStruct.InitStruct(values);
                this._configurationUnion.Set(configurationStruct);

                this._statusLabel.Text = string.Format("���� {0} ������� ��������", binFileName);

            }
            catch
            {
                MessageBox.Show(FILE_LOAD_FAIL);
            }
        }

        private byte[] LoadOldConfig(XmlDocument doc, string docElement)
        {
            List<byte> result = new List<byte>();
            docElement = string.Format("/{0}/", docElement);
            result.AddRange(this.DeserializeSlot(doc, docElement + "�������_�������"));
            result.AddRange(this.DeserializeSlot(doc, docElement + "����������"));
            result.AddRange(this.DeserializeSlot(doc, docElement + "������_�������"));
            result.AddRange(this.DeserializeSlot(doc, docElement + "������_�������_��������"));
            result.AddRange(this.DeserializeSlot(doc, docElement + "������_�������_���������"));
            result.AddRange(this.DeserializeSlot(doc, docElement + "������_�������2_��������"));
            result.AddRange(this.DeserializeSlot(doc, docElement + "������_�������2_���������"));
            result.AddRange(this.DeserializeSlot(doc, docElement + "������_�������_��������"));
            result.AddRange(this.DeserializeSlot(doc, docElement + "������_�������_���������"));
            result.AddRange(this.DeserializeSlot(doc, docElement + "������_����������_��������"));
            result.AddRange(this.DeserializeSlot(doc, docElement + "������_����������_���������"));
            result.AddRange(this.DeserializeSlot(doc, docElement + "��������_�������"));
            result.AddRange(new byte[] { 0, 0, 0, 0, 0, 0, 0, 0 });
            result.AddRange(this.DeserializeSlot(doc, docElement + "������������_�����������"));
            var res = result.ToArray();
            Common.SwapArrayItems(ref res);
            return res;
        }

        private byte[] DeserializeSlot(XmlDocument doc, string nodePath)
        {
            return Convert.FromBase64String(doc.SelectSingleNode(nodePath).InnerText);
        }

        private void _saveConfigBut_Click(object sender, EventArgs e)
        {
            this.SaveinFile();
        }

        private void SaveinFile()
        {
            string message;
            if (this._configurationUnion.Check(out message, true))
            {
                var currentStruct = this._configurationUnion.Get();
                this._saveConfigurationDlg.FileName = string.Format("��750_�������_������ {0:F1}.bin", this._device.DeviceVersion);
                if (this._saveConfigurationDlg.ShowDialog() == DialogResult.OK)
                {
                    this.Serialize(this._saveConfigurationDlg.FileName, currentStruct, MR750);
                }

            }
            else
            {
                if (message == null)
                {
                    MessageBox.Show("���������� ������������ �������. ������������ �� ����� ���� ���������.");
                }
                else
                {
                    MessageBox.Show(message);
                }
            } 
        }

        private const string FILE_SAVE_FAIL = "���������� ��������� ����";
        private const string MR750 = "MR750";
        private const string INVALID_PORT = "���� ����������.";
        private const string ERROR_READ_CONFIG = "������ ������ ������������";
        private const string ERROR_WRITE_CONFIG = "������ ������ ������������";
        private const string CONFIG_READ_OK = "������������ ���������";
        private RadioButtonSelector _groupSelector;


        private bool IsProcess
        {
            set
            {
                this._readConfigBut.Enabled = !value;
                this._writeConfigBut.Enabled = !value;
                this._resetConfigBut.Enabled = !value;
                this._loadConfigBut.Enabled = !value;
                this._saveConfigBut.Enabled = !value;
                this._saveToXmlButton.Enabled = !value;
            }
        }


        /// <summary>
        /// ���������� ������������ � ����
        /// </summary>
        /// <param name="binFileName">��� �����</param>
        public void Serialize(string binFileName, StructBase config, string head)
        {
            try
            {
                var doc = new XmlDocument();
                doc.AppendChild(doc.CreateElement(head));


                var values = config.GetValues();

                XmlElement element = doc.CreateElement(string.Format("{0}_SET_POINTS", head));
                element.InnerText = Convert.ToBase64String(Common.TOBYTES(values, false));
                if (doc.DocumentElement == null)
                {
                    throw new NullReferenceException();
                }
                doc.DocumentElement.AppendChild(element);

                doc.Save(binFileName);
                this._statusLabel.Text = string.Format("���� {0} ������� ��������", binFileName);
            }
            catch
            {
                MessageBox.Show(FILE_SAVE_FAIL);
            }
        }

        private void _writeConfigBut_Click(object sender, EventArgs e)
        {
            this.WriteConfig();  
        }

        private void WriteConfig()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            DialogResult result = MessageBox.Show(string.Format("�������� ������������ �� 750 �{0}?", 
                this._device.DeviceNumber), "������", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                bool res;
                try
                {
                    res = this.WriteConfiguration();
                }
                catch (Exception ex)
                {
                    return;
                }
                if (res)
                {
                    this._statusLabel.Text = "��� ������ ������������";
                    this.IsProcess = true;
                    this._exchangeProgressBar.Value = 0;
                    this._configuration.Value = this._currentSetpointsStructV2;
                    this._configuration.SaveStruct();
                }
            }  
        }
        private void _readConfigBut_Click(object sender, EventArgs e)
        {
            this.StartReadConfiguration();
        }
        
        private void ConfigurationForm_Load(object sender, EventArgs e)
        {
            if(Device.AutoloadConfig)
                this.StartReadConfiguration();
        }

        private Dictionary<int, string> GetFaults()
        {
            return Common.VersionConverter(this._device.DeviceVersion) > 1.0 ? StringsConfig.FaultsV1_01 : StringsConfig.Faults;
        }

        #endregion

        private void _resetConfigBut_Click(object sender, EventArgs e)
        {
            this._configurationUnion.Reset();
        }

        private void Mr700ConfigurationFormV2_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._configuration.RemoveStructQueries();
        }

        private void _saveToXmlButton_Click(object sender, EventArgs e)
        {
            this.SaveToHtmlFile();
        }

        private void SaveToHtmlFile()
        {
            try
            {
                if (this.WriteConfiguration())
                {
                    this._currentSetpointsStructV2.DeviceVersion = this._device.DeviceVersion;
                    this._currentSetpointsStructV2.DeviceNumber = this._device.DeviceNumber.ToString();
                    this._statusLabel.Text = HtmlExport.Export(Resources.MR750Main,
                                      Resources.MR750Res, this._currentSetpointsStructV2, this._currentSetpointsStructV2.DeviceType, this._currentSetpointsStructV2.DeviceVersion);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("������ ����������");
            }
        }

        private void contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ((ContextMenuStrip)sender).Close();
            if (e.ClickedItem == this.readFromDeviceItem)
            {
                this.StartReadConfiguration();
                return;
            }
            if (e.ClickedItem == this.writeToDeviceItem)
            {
                this.WriteConfig();
                return;
            }
            if (e.ClickedItem == this.clearSetpointsItem)
            {
                this._configurationUnion.Reset();
                return;
            }
            if (e.ClickedItem == this.readFromFileItem)
            {
                this.ReadFromFile();
                return;
            }
            if (e.ClickedItem == this.writeToFileItem)
            {
                this.SaveinFile();
                return;
            }
            
            if (e.ClickedItem == this.writeToHtmlItem)
            {
                this.SaveToHtmlFile();
            }
        }

        private void Mr750ConfigurationForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers != Keys.Control) return;
            switch (e.KeyCode)
            {
                case Keys.W:
                    this.WriteConfig();
                    break;
                case Keys.R:
                    this.StartReadConfiguration();
                    break;
                case Keys.S:
                    this.SaveinFile();
                    break;
                case Keys.O:
                    this.ReadFromFile();
                    break;
            }
            e.Handled = true;
        }



        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(Mr750Device); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(Mr750ConfigurationForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get
            {
                return Resources.config.ToBitmap();
            }
        }

        public string NodeName
        {
            get { return "������������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion
    }
}