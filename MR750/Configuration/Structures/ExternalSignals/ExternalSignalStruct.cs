﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR750.Configuration.Structures.ExternalSignals
{
    public class ExternalSignalStruct : StructBase
    {
        [Layout(0)] private ushort _keyOff;
        [Layout(1)] private ushort _keyOn;
        [Layout(2)] private ushort _extOff;
        [Layout(3)] private ushort _extOn;
        [Layout(4)] private ushort _inpClear;
        [Layout(5)] private ushort _inpGroup;
        [Layout(6)] private ushort _uca;
        [Layout(7)] private ushort _f;

        [Layout(8)] private ushort _side1;
        [Layout(9)] private ushort _side2;

        [Layout(10)] private ushort _noUacc;
        [Layout(11)] private ushort _disable; //вход запрет команд управления по СДТУ
        [Layout(12)] private ushort _connectionPort;     //0-RS485, 1-M12 (GSM)

        [BindingProperty(0)]
        [XmlElement(ElementName = "ключ_ВЫКЛ")]
        public string KeyOff
        {
            get { return Validator.Get(_keyOff, StringsConfig.LogicSignalsMr750); }
            set { _keyOff = Validator.Set(value, StringsConfig.LogicSignalsMr750); }
        }

        [BindingProperty(1)]
        [XmlElement(ElementName = "ключ_ВКЛ")]
        public string KeyOn
        {
            get { return Validator.Get(_keyOn, StringsConfig.LogicSignalsMr750); }
            set { _keyOn = Validator.Set(value, StringsConfig.LogicSignalsMr750); }
        }

        [BindingProperty(2)]
        [XmlElement(ElementName = "вход_внеш_выключить")]
        public string ExtOff
        {
            get { return Validator.Get(_extOff, StringsConfig.LogicSignalsMr750); }
            set { _extOff = Validator.Set(value, StringsConfig.LogicSignalsMr750); }
        }

        [BindingProperty(3)]
        [XmlElement(ElementName = "вход_внеш_включить")]
        public string ExtOn
        {
            get { return Validator.Get(_extOn, StringsConfig.LogicSignalsMr750); }
            set { _extOn = Validator.Set(value, StringsConfig.LogicSignalsMr750); }
        }

        [BindingProperty(4)]
        [XmlElement(ElementName = "сброс_сигнализации")]
        public string InpClear
        {
            get { return Validator.Get(_inpClear, StringsConfig.LogicSignalsMr750); }
            set { _inpClear = Validator.Set(value, StringsConfig.LogicSignalsMr750); }
        }

        [BindingProperty(5)]
        [XmlElement(ElementName = "группа_уставок")]
        public string InpGroup
        {
            get { return Validator.Get(_inpGroup, StringsConfig.LogicSignalsMr750); }
            set { _inpGroup = Validator.Set(value, StringsConfig.LogicSignalsMr750); }
        }

        
        [BindingProperty(6)]
        [XmlElement(ElementName = "Uca")]
        public string Uca
        {
            get { return Validator.Get(_uca, StringsConfig.Uca); }
            set { _uca = Validator.Set(value, StringsConfig.Uca); }
        }

        [BindingProperty(7)]
        [XmlElement(ElementName = "F")]
        public string F
        {
            get { return Validator.Get(_f, StringsConfig.F); }
            set { _f = Validator.Set(value, StringsConfig.F); }
        }
        

        [BindingProperty(8)]
        [XmlElement(ElementName = "Сторона1")]
        public string Side1
        {
            get { return Validator.Get(_side1, StringsConfig.Side, 0, 1); }
            set { _side1 = Validator.Set(value, StringsConfig.Side, _side1, 0, 1); }
        }

        [BindingProperty(9)]
        [XmlElement(ElementName = "Сторона2")]
        public string Side2
        {
            get { return Validator.Get(_side2, StringsConfig.Side, 0, 1); }
            set { _side2 = Validator.Set(value, StringsConfig.Side, _side2, 0, 1); }
        }

        [BindingProperty(10)]
        [XmlElement(ElementName = "Низкий_заряд_на_аккумуляторе")]
        public double NoUacc
        {
            get { return ValuesConverterCommon.GetU(_noUacc); }
            set { _noUacc = ValuesConverterCommon.SetU(value); }
        }

        [BindingProperty(11)]
        [XmlElement(ElementName = "Запрет_команд_по_СДТУ")]
        public string Disable
        {
            get { return Validator.Get(_disable, StringsConfig.LogicSignalsMr750); }
            set { _disable = Validator.Set(value, StringsConfig.LogicSignalsMr750); }
        }

        [BindingProperty(12)]
        [XmlElement(ElementName = "Параметры порта (R485 или M12)")]
        public string ConnectionPort
        {
            get { return Validator.Get(_connectionPort, StringsConfig.ConnectionPort); }
            set { _connectionPort = Validator.Set(value, StringsConfig.ConnectionPort); }
        }
    }
}
