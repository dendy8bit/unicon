﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR750.Configuration.Structures.SystenConfig
{
    public class SystemConfigStruct : StructBase
    {
       

        [Layout(0 ,Ignore = true)] private ushort _address ;
        [Layout(1, Ignore = true)] private ushort _speed;
        [Layout(2, Ignore = true)] private ushort _delay;
        [Layout(3, Ignore = true)] private ushort _status;
        [Layout(4)] private ushort _oscConf;





        /// <summary>
        /// Длит. предзаписи
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "Предзапись")]
        public ushort Percent
        {
            get { return (ushort)(Common.GetBits(this._oscConf, 8, 9, 10, 11, 12, 13, 14, 15) >> 8); }
            set { this._oscConf = Common.SetBits(this._oscConf, value, 8, 9, 10, 11, 12, 13, 14, 15); }
        }

        /// <summary>
        /// Фиксация
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "Фиксация")]
        public string FixationXml
        {
            get { return Validator.Get(this._oscConf, StringsConfig.OscFixation, 7); }
            set { this._oscConf = Validator.Set(value, StringsConfig.OscFixation, this._oscConf, 7); }
        }

        /// <summary>
        /// количество_осциллограм
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "Количество_осциллограм")]
        public string SizeXml
        {
            get { return Validator.Get(this._oscConf, StringsConfig.OscSize, 0, 1, 2, 3, 4, 5, 6); }
            set { this._oscConf = Validator.Set(value, StringsConfig.OscSize, this._oscConf, 0, 1, 2, 3, 4, 5, 6); }
        }
     /*   public override void InitStruct(byte[] array)
        {
            int index = 0;
            this._oscConf = StructHelper.GetUshort(array, ref index);
        }*/

      /*  public override ushort[] GetValues()
        {
            return new ushort[] { _oscConf };
        }*/
       /* [XmlIgnore]
        protected override int OtherSize
        {
            get { return 1; }
        }*/
    }
}
