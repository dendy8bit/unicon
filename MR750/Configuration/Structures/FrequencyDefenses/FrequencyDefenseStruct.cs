﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR750.Configuration.Structures.FrequencyDefenses
{
    public class FrequencyDefenseStruct : StructBase
    {
        [Layout(0)] private ushort _config; //конфигурация выведено/введено (УРОВ - выведено/введено)...
        [Layout(1)] private ushort _block; //вход блокировки
        [Layout(2)] private ushort _srab;
        [Layout(3)] private ushort _tSrab; //уставка срабатывания_
        [Layout(4)] private ushort _return; //время срабатывания_
        [Layout(5)] private ushort _tReturn; //уставка возврата
        [Layout(6)] private ushort _res1;
        [Layout(7)] private ushort _res2;

        [BindingProperty(0)]
        public string Mode
        {
            get { return Validator.Get(this._config, StringsConfig.CurModesV1, 0, 1); }
            set { this._config = Validator.Set(value, StringsConfig.CurModesV1, this._config, 0, 1); }
        }

        /// <summary>
        /// Блокировка
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "Блокировка")]
        public string Bloc
        {
            get { return Validator.Get(this._block, StringsConfig.ExternalDefenceSignalsMr750); }
            set { this._block = Validator.Set(value, StringsConfig.ExternalDefenceSignalsMr750); }
        }

        /// <summary>
        /// 
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "Уставка_сраб")]
        public double Srab
        {
            get { return ValuesConverterCommon.GetU(this._srab); }
            set { this._srab = ValuesConverterCommon.SetU(value); }
        }

        /// <summary>
        /// tср, время срабатывания
        /// </summary>
        [BindingProperty(3)]
        [XmlElement(ElementName = "Уставка_срабатывания")]
        public int TimeSrab
        {
            get { return ValuesConverterCommon.GetWaitTime(this._tSrab); }
            set { this._tSrab = ValuesConverterCommon.SetWaitTime(value); }
        }


        /// <summary>
        /// Возврат
        /// </summary>
        [BindingProperty(4)]
        [XmlElement(ElementName = "Возврат")]
        public bool Return
        {
            get { return Common.GetBit(this._config, 14); }
            set { this._config = Common.SetBit(this._config, 14, value); }
        }

      
        [BindingProperty(5)]
        [XmlElement(ElementName = "АПВ_ВЗ")]
        public bool ApvReturn
        {
            get { return Common.GetBit(this._config, 15); }
            set { this._config = Common.SetBit(this._config, 15, value); }
        }


        [BindingProperty(6)]
        [XmlElement(ElementName = "Уставка_ВЗ")]
        public double InputReturn
        {
            get { return ValuesConverterCommon.GetU(this._return); }
            set { this._return =ValuesConverterCommon.SetU(value) ; }
        }


        [BindingProperty(7)]
        [XmlElement(ElementName = "Время_ВЗ")]
        public int UstavkaReturn
        {
            get { return ValuesConverterCommon.GetWaitTime(this._tReturn); }
            set { this._tReturn = ValuesConverterCommon.SetWaitTime(value); }
        }


        /// <summary>
        /// Уров
        /// </summary>
        [BindingProperty(8)]
        [XmlElement(ElementName = "Уров")]
        public bool Urov
        {
            get { return Common.GetBit(this._config, 7); }
            set { this._config = Common.SetBit(this._config, 7, value); }
        }


        /// <summary>
        /// АПВ
        /// </summary>
        [BindingProperty(9)]
        [XmlElement(ElementName = "АПВ")]
        public bool Apv
        {
            get { return Common.GetBit(this._config, 6); }
            set { this._config = Common.SetBit(this._config, 6, value); }
        }

        /// <summary>
        /// АВР
        /// </summary>
        [BindingProperty(10)]
        [XmlElement(ElementName = "Осц")]
        public string Osc
        {

            get { return Validator.Get(this._config, StringsConfig.OscV111, 3, 4); }
            set { this._config = Validator.Set(value, StringsConfig.OscV111, this._config, 3, 4); }
        }
        /// <summary>
        /// Сброс ступени
        /// </summary>
        [BindingProperty(11)]
        [XmlElement(ElementName = "Сброс_ступени")]
        public bool Reset
        {
            get { return Common.GetBit(this._config, 13); }
            set { this._config = Common.SetBit(this._config, 13, value); }
        }
    }
}
