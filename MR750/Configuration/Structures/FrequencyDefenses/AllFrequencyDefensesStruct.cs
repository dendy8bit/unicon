﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR750.Configuration.Structures.FrequencyDefenses
{
    public class AllFrequencyDefensesStruct : StructBase, IDgvRowsContainer<FrequencyDefenseStruct>
    {
        [Layout(0, Count = 4)]
        private FrequencyDefenseStruct[] _externalDefenses;

      //  [Layout(1, Count = 8)] private ushort[] _res;

        public FrequencyDefenseStruct[] Rows
        {
            get { return _externalDefenses; }
            set { _externalDefenses = value; }
        }
    }
}
