﻿using System;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MR750.Configuration.Structures.Apv;
using BEMN.MR750.Configuration.Structures.Avr;
using BEMN.MR750.Configuration.Structures.CurrentDefenses;
using BEMN.MR750.Configuration.Structures.ExternalDefenses;
using BEMN.MR750.Configuration.Structures.ExternalSignals;
using BEMN.MR750.Configuration.Structures.FaultSignal;
using BEMN.MR750.Configuration.Structures.FrequencyDefenses;
using BEMN.MR750.Configuration.Structures.Indicators;
using BEMN.MR750.Configuration.Structures.Keys;
using BEMN.MR750.Configuration.Structures.Ls;
using BEMN.MR750.Configuration.Structures.Lzsh;
using BEMN.MR750.Configuration.Structures.MeasuringTransformer;
using BEMN.MR750.Configuration.Structures.Relay;
using BEMN.MR750.Configuration.Structures.Switch;
using BEMN.MR750.Configuration.Structures.SystenConfig;
using BEMN.MR750.Configuration.Structures.Vls;
using BEMN.MR750.Configuration.Structures.VoltageDefenses;

namespace BEMN.MR750.Configuration.Structures
{
    [Serializable]
    [XmlRoot(ElementName = "МР750")]
    public class ConfigurationStructV2 : StructBase
    {
        [XmlElement(ElementName = "Версия")]
        public string DeviceVersion { get; set; }
        [XmlElement(ElementName = "Номер_устройства")]
        public string DeviceNumber { get; set; }
        [XmlElement(ElementName = "Тип_устройства")]
        public string DeviceType { get => "МР750";
            set {} }

        #region [Private fields]
        [Layout(0)] private MeasureTransStruct _measureTrans;
        [Layout(1)] private KeysStruct _keys;
        [Layout(2)] private ExternalSignalStruct _externalSignal;
        [Layout(3)] private FaultStruct _fault;
        [Layout(4)] private InputLogicSignalStruct _inputLogicSignal;
        [Layout(5)] private SwitchStruct _switch;
        [Layout(6)] private ApvStruct _apv;
        [Layout(7)] private AvrStruct _avr;
        [Layout(8)] private LpbStruct _lzsh;
        [Layout(9)] private AllExternalDefensesStruct _allExternalDefenses;
        [Layout(10)] private AllSetpointsStruct _allSetpoints;
        [Layout(11)] private AllAddSetpointsStruct _allAddSetpoints;
        [Layout(12)] private FrequencySetpointsStruct _allFrequency;
        [Layout(13)] private VoltageSetpointsStruct _allVoltage;
        [Layout(14)] private OutputLogicSignalStruct _vls;
        [Layout(15)] private AllReleOutputStruct _reley;
        [Layout(16)] private AllIndicatorsStruct _indicators;
        [Layout(17)] private SystemConfigStruct _systemConfig; 
        #endregion [Private fields]
        
        #region [Properties]
        [XmlElement(ElementName = "Измерительный_трансформатор")]
        [BindingProperty(0)]
        public MeasureTransStruct MeasureTrans
        {
            get { return _measureTrans; }
            set { _measureTrans = value; }
        }
        [XmlElement(ElementName = "Внешние_сигналы")]
        [BindingProperty(1)]
        public ExternalSignalStruct ExternalSignal
        {
            get { return _externalSignal; }
            set { _externalSignal = value; }
        }
        [XmlElement(ElementName = "Реле_неисправности")]
        [BindingProperty(2)]
        public FaultStruct Fault
        {
            get { return _fault; }
            set { _fault = value; }
        }
        [XmlElement(ElementName = "Входные_логические_сигналы")]
        [BindingProperty(3)]
        public InputLogicSignalStruct InputLogicSignal
        {
            get { return _inputLogicSignal; }
            set { _inputLogicSignal = value; }
        }
        [XmlElement(ElementName = "Конфигурация_выключателя")]
        [BindingProperty(4)]
        public SwitchStruct Switch
        {
            get { return _switch; }
            set { _switch = value; }
        }
        [XmlElement(ElementName = "АПВ")]
        [BindingProperty(5)]
        public ApvStruct Apv
        {
            get { return _apv; }
            set { _apv = value; }
        }
        [XmlElement(ElementName = "АВР")]
        [BindingProperty(6)]
        public AvrStruct Avr
        {
            get { return _avr; }
            set { _avr = value; }
        }
        [XmlElement(ElementName = "ЛЗШ")]
        [BindingProperty(7)]
        public LpbStruct Lzsh
        {
            get { return _lzsh; }
            set { _lzsh = value; }
        }

        [BindingProperty(8)]
        [XmlElement(ElementName = "Внешние")]
        public AllExternalDefensesStruct AllExternalDefenses
        {
            get { return _allExternalDefenses; }
            set { _allExternalDefenses = value; }
        }

        [BindingProperty(9)]
        [XmlElement(ElementName = "Токовые")]
        public AllSetpointsStruct AllSetpoints
        {
            get { return _allSetpoints; }
            set { _allSetpoints = value; }
        }
        [BindingProperty(10)]
        [XmlElement(ElementName = "Токовые_дополнительные")]
        public AllAddSetpointsStruct AllAddSetpoints
        {
            get { return _allAddSetpoints; }
            set { _allAddSetpoints = value; }
        }

        [BindingProperty(11)]
        [XmlElement(ElementName = "Частотные")]
        public FrequencySetpointsStruct AllFrequency
        {
            get { return _allFrequency; }
            set { _allFrequency = value; }
        }
        [BindingProperty(12)]
        [XmlElement(ElementName = "Напряжения")]
        public VoltageSetpointsStruct AllVoltage
        {
            get { return _allVoltage; }
            set { _allVoltage = value; }
        }
        [XmlElement(ElementName = "Все_ВЛС")]
        [BindingProperty(13)]
        public OutputLogicSignalStruct Vls
        {
            get { return _vls; }
            set { _vls = value; }
        }
        [XmlIgnore]
        //[XmlElement(ElementName = "Реле")]
        //[BindingProperty(14)]
        public AllReleOutputStruct Reley
        {
            get { return _reley; }
            set { _reley = value; }
        }
        [XmlElement(ElementName = "Индикаторы")]
        [BindingProperty(14)]
        public AllIndicatorsStruct Indicators
        {
            get { return _indicators; }
            set { _indicators = value; }
        }
        [XmlElement(ElementName = "Осц")]
        [BindingProperty(15)]
        public SystemConfigStruct SystemConfig
        {
            get { return _systemConfig; }
            set { _systemConfig = value; }
        }
        [XmlElement(ElementName = "Ключи")]
        [BindingProperty(16)]
        public KeysStruct Keys
        {
            get { return _keys; }
            set { _keys = value; }
        } 
        #endregion [Properties]

       /*  protected override Devices.Device.slot EmptySlot
         {
             get
             {
                 return new Device.slot(0x1270,0x1274);
             }
         }*/
    }
}
