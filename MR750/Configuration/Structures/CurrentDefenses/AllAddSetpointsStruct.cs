﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;

namespace BEMN.MR750.Configuration.Structures.CurrentDefenses
{
    public class AllAddSetpointsStruct : StructBase, ISetpointContainer<AllAddCurrentDefensesStruct>
    {
        [Layout(0)] public AllAddCurrentDefensesStruct _addCurrentDefenses1;
        [Layout(1, Count = 16)] private ushort[] _res1;
        [Layout(2)] public AllAddCurrentDefensesStruct _addCurrentDefenses2;
        [Layout(3, Count = 16)] private ushort[] _res2;

        [XmlIgnore]
        public AllAddCurrentDefensesStruct[] Setpoints
        {
            get
            {
                return new[]
                    {
                        _addCurrentDefenses1.Clone<AllAddCurrentDefensesStruct>(),
                        _addCurrentDefenses2.Clone<AllAddCurrentDefensesStruct>()
                    };
            }
            set
            {
                _addCurrentDefenses1 = value[0];
                _addCurrentDefenses2 = value[1];
            }
        }

        [XmlElement(ElementName = "Основная")]
        public AllAddCurrentDefensesStruct Main
        {
            get { return _addCurrentDefenses1; }
            set { _addCurrentDefenses1 = value; }
        }
        [XmlElement(ElementName = "Резервная")]
        public AllAddCurrentDefensesStruct Reserve
        {
            get { return _addCurrentDefenses2; }
            set { _addCurrentDefenses2 = value; }
        }
    }
}
