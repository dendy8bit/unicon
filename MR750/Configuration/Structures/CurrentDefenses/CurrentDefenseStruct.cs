﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR750.Configuration.Structures.CurrentDefenses
{
    public class CurrentDefenseStruct : StructBase
    {
        [Layout(0)] private ushort _config; //конфигурация выведено/введено (УРОВ - выведено/введено)...
        [Layout(1)] private ushort _block; //вход блокировки
        [Layout(2)] private ushort _srab;//конфигурация дополнительная (АПВ - выведено/введено, АВР - выведено/введено)
        [Layout(3)] private ushort _ust; //уставка срабатывания_
        [Layout(4)] private ushort _time; //время срабатывания_
        [Layout(5)] private ushort _u; //уставка возврата
      

        [BindingProperty(0)]
        public string Mode
        {
            get { return Validator.Get(this._config, StringsConfig.CurModesV1, 0, 1); }
            set { this._config = Validator.Set(value, StringsConfig.CurModesV1, this._config, 0, 1); }
        }

        /// <summary>
        /// Блокировка
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "Блокировка")]
        public string Bloc
        {
            get { return Validator.Get(this._block, StringsConfig.ExternalSignalsMr750); }
            set { this._block = Validator.Set(value, StringsConfig.ExternalSignalsMr750); }
        }

        [BindingProperty(2)]
        public bool Ustart
        {
            get { return Common.GetBit(this._config, 14); }
            set { this._config = Common.SetBit(this._config, 14, value); }
        }
        [BindingProperty(3)]
        public double Uustavka
        {
            get { return ValuesConverterCommon.GetU(this._time); }
            set { this._time = ValuesConverterCommon.SetU(value); }
        }

        [BindingProperty(4)]
        [XmlElement(ElementName = "Направление")]
        public string Direction
        {
            get
            {
                return Validator.Get(this._config, StringsConfig.Direction, 10,11);

            }
            set
            {
                this._config = Validator.Set(value, StringsConfig.Direction, this._config, 10,11);
            }
        }

        [BindingProperty(5)]
        [XmlElement(ElementName = "Направление_блокировка")]
        public string DirectionBlock
        {
            get
            {
                return Validator.Get(this._config, StringsConfig.DirectionBlock, 9);

            }
            set
            {
                this._config = Validator.Set(value, StringsConfig.DirectionBlock, this._config, 9);
            }
        }


        [BindingProperty(6)]
        [XmlElement(ElementName = "Параметр_1")]
        public string Parameter
        {
            get
            {
                return Validator.Get(this._config, StringsConfig.TokParameter, 8);
               
            }
            set
            {
                this._config = Validator.Set(value, StringsConfig.TokParameter, this._config, 8);
            }
        }

        [BindingProperty(7)]
        [XmlElement(ElementName = "Параметр_2")]
        public string Parameter2
        {
            get
            {
                return Validator.Get(this._config, StringsConfig.TokParameter2, 8);

            }
            set
            {
                this._config = Validator.Set(value, StringsConfig.TokParameter2, this._config, 8);
            }
        }
        
        /// <summary>
        /// Уставка
        /// </summary>
        [BindingProperty(8)]
        [XmlElement(ElementName = "Уставка_срабатывания")]
        public double Srab
        {
            get { return ValuesConverterCommon.GetIn(this._srab); }
            set { this._srab = ValuesConverterCommon.SetIn(value); }
        }

        [BindingProperty(9)]
        [XmlElement(ElementName = "Уставка_срабатыванияIn")]
        public double SrabIn
        {
            get { return ValuesConverterCommon.GetUstavka5(this._srab); }
            set { this._srab = ValuesConverterCommon.SetUstavka5(value); }
        }

        [BindingProperty(10)]
        [XmlElement(ElementName = "Характеристика")]
        public string Feature
        {
            get { return Validator.Get(this._config, StringsConfig.FeatureLight, 12); }
            set
            {
                this._config = Validator.Set(value, StringsConfig.FeatureLight, this._config, 12);
            }
        }





        /// <summary>
        /// tср, время срабатывания
        /// </summary>
        [BindingProperty(11)]
        [XmlElement(ElementName = "время_срабатывания")]
        public int TimeSrab
        {
            get
            {
                if (Common.GetBit(this._config, 12))
                {
                    return this._ust;
                }
                else
                {
                    return ValuesConverterCommon.GetWaitTime(this._ust);
                }

            }
            set
            {
                if (Common.GetBit(this._config, 12))
                {
                    this._ust = (ushort) value;
                }
                else
                {
                  this._ust = ValuesConverterCommon.SetWaitTime(value);  
                }
                
            }
        }


        /// <summary>
        /// Уров
        /// </summary>
        [BindingProperty(12)]
        [XmlElement(ElementName = "Ускорение")]
        public bool Speedup
        {
            get { return Common.GetBit(this._config, 15); }
            set { this._config = Common.SetBit(this._config, 15, value); }
        }

        /// <summary>
        /// Уставка
        /// </summary>
        [BindingProperty(13)]
        [XmlElement(ElementName = "Уставка_ускорения")]
        public int Tspeedup
        {
            get { return ValuesConverterCommon.GetWaitTime(this._u); }
            set { this._u = ValuesConverterCommon.SetWaitTime(value); }
        }

        /// <summary>
        /// Уров
        /// </summary>
        [BindingProperty(14)]
        [XmlElement(ElementName = "Уров")]
        public bool Urov
        {
            get { return Common.GetBit(this._config, 7); }
            set { this._config = Common.SetBit(this._config, 7, value); }
        }


        /// <summary>
        /// АПВ
        /// </summary>
        [BindingProperty(15)]
        [XmlElement(ElementName = "АПВ")]
        public bool Apv
        {
            get { return Common.GetBit(this._config, 6); }
            set { this._config = Common.SetBit(this._config, 6, value); }
        }






        /// <summary>
        /// АВР
        /// </summary>
        [BindingProperty(16)]
        [XmlElement(ElementName = "Осц")]
        public string Osc
        {
            
            get { return Validator.Get(this._config,StringsConfig.OscV111,3, 4) ; }
            set { this._config = Validator.Set(value, StringsConfig.OscV111,this._config, 3, 4); }
        }
      
         
    }
}
