﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR750.Configuration.Structures.ExternalDefenses
{
    public class DefenseExternalStruct : StructBase
    {
        [Layout(0)] private ushort _config; //конфигурация выведено/введено (УРОВ - выведено/введено)...
        [Layout(1)] private ushort _block; //вход блокировки
        [Layout(2)] private ushort _srab;//конфигурация дополнительная (АПВ - выведено/введено, АВР - выведено/введено)
        [Layout(3)] private ushort _ust; //уставка срабатывания_
        [Layout(4)] private ushort _time; //время срабатывания_
        [Layout(5)] private ushort _u; //уставка возврата
      
        [BindingProperty(0)]
        [XmlElement(ElementName = "Режим")]
        public string Mode
        {
            get { return Validator.Get(this._config, StringsConfig.CurModesV1, 0, 1); }
            set { this._config = Validator.Set(value, StringsConfig.CurModesV1, this._config, 0, 1); }
        }

        /// <summary>
        /// Блокировка
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "Блокировка")]
        public string Bloc
        {
            get { return Validator.Get(this._block, StringsConfig.ExternalDefenceSignalsMr750); }
            set { this._block = Validator.Set(value, StringsConfig.ExternalDefenceSignalsMr750); }
        }

        /// <summary>
        /// Блокировка
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "Вход_срабатывания")]
        public string Srab
        {
            get { return Validator.Get(this._srab, StringsConfig.ExternalDefenceSignalsMr750); }
            set { this._srab = Validator.Set(value, StringsConfig.ExternalDefenceSignalsMr750); }
        }

        /// <summary>
        /// tср, время срабатывания
        /// </summary>
        [BindingProperty(3)]
        [XmlElement(ElementName = "Уставка_срабатывания")]
        public int TimeSrab
        {
            get { return ValuesConverterCommon.GetWaitTime(this._ust); }
            set { this._ust = ValuesConverterCommon.SetWaitTime(value); }
        }

        /// <summary>
        /// Возврат
        /// </summary>
        [BindingProperty(4)]
        [XmlElement(ElementName = "Возврат")]
        public bool Return
        {
            get { return Common.GetBit(this._config, 14); }
            set { this._config = Common.SetBit(this._config, 14, value); }
        }

      
        [BindingProperty(5)]
        [XmlElement(ElementName = "АПВ_ВЗ")]
        public bool ApvReturn
        {
            get { return Common.GetBit(this._config, 15); }
            set { this._config = Common.SetBit(this._config, 15, value); }
        }
        
        [BindingProperty(6)]
        [XmlElement(ElementName = "Вход_ВЗ")]
        public string InputReturn
        {
            get { return Validator.Get(this._time, StringsConfig.ExternalDefenceSignalsMr750); }
            set { this._time = Validator.Set(value, StringsConfig.ExternalDefenceSignalsMr750); }
        }
        
        [BindingProperty(7)]
        [XmlElement(ElementName = "Уставка_ВЗ")]
        public int UstavkaReturn
        {
            get { return ValuesConverterCommon.GetWaitTime(this._u); }
            set { this._u = ValuesConverterCommon.SetWaitTime(value); }
        }

        /// <summary>
        /// Уров
        /// </summary>
        [BindingProperty(8)]
        [XmlElement(ElementName = "Уров")]
        public bool Urov
        {
            get { return Common.GetBit(this._config, 7); }
            set { this._config = Common.SetBit(this._config, 7, value); }
        }

        /// <summary>
        /// АПВ
        /// </summary>
        [BindingProperty(9)]
        [XmlElement(ElementName = "АПВ")]
        public bool Apv
        {
            get { return Common.GetBit(this._config, 6); }
            set { this._config = Common.SetBit(this._config, 6, value); }
        }

        /// <summary>
        /// АВР
        /// </summary>
        [BindingProperty(10)]
        [XmlElement(ElementName = "АВР")]
        public bool Avr
        {
            get { return Common.GetBit(this._config, 5); }
            set { this._config = Common.SetBit(this._config, 5, value); }
        }
        /// <summary>
        /// ПРОВЕРИТЬ!!!
        /// </summary>
        [BindingProperty(11)]
        [XmlElement(ElementName = "Осц")]
        public bool Osc
        {
            get { return Common.GetBit(this._config, 4); }
            set { this._config = Common.SetBit(this._config, 4, value); }
        }
        /// <summary>
        /// ПРОВЕРИТЬ!!!
        /// </summary>
        [BindingProperty(12)]
        [XmlElement(ElementName = "Сброс_ступени")]
        public bool Reset
        {
            get { return Common.GetBit(this._config, 13); }
            set { this._config = Common.SetBit(this._config, 13, value); }
        }
    }
}
