﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR750.Configuration.Structures.Avr
{
    /// <summary>
    /// Конфигурация АВР
    /// </summary>
    public class AvrStruct : StructBase
    {
        #region [Private fields]

        [Layout(0)] private ushort _config; //конфигурация
        [Layout(1)] private ushort _block; //вход блокировки АВР
        [Layout(2)] private ushort _clear; //вход сброс блокировки АВР
        [Layout(3)] private ushort _umin1; //вход сигнала запуск АВР
        [Layout(4)] private ushort _umax1; //вход АВР срабатывания
        [Layout(5)] private ushort _umin2; //время АВР срабатывания
        [Layout(6)] private ushort _umax2; //вход АВР возврат
        [Layout(7)] private ushort _timeOff; //время АВР возврат
        [Layout(8)] private ushort _timeOtkl; //задержка отключения резерва
        [Layout(9)] private ushort _rez;

        #endregion [Private fields]


        #region [Properties]

        /// <summary>
        /// от сигнала
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "Конфигурация")]
        public bool BySignalXml
        {
            get { return Common.GetBit(this._config,0); }
            set { this._config = Common.SetBit(this._config,0,value) ; }
        }

   



        /// <summary>
        /// вход блокировки АВР
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "вход_блокировки_АВР")]
        public string BlockingXml
        {
            get { return Validator.Get(this._block, StringsConfig.ExternalSignalsMr750); }
            set { this._block = Validator.Set(value, StringsConfig.ExternalSignalsMr750); }
        }

        /// <summary>
        /// вход сброс блокировки АВР
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "вход_сброс_блокировки_АВР")]
        public string ClearXml
        {
            get { return Validator.Get(this._clear, StringsConfig.ExternalSignalsMr750); }
            set { this._clear = Validator.Set(value, StringsConfig.ExternalSignalsMr750); }
        }


        [BindingProperty(3)]
        [XmlElement(ElementName = "Umin1")]
        public double Umin1
        {
            get { return ValuesConverterCommon.GetU(this._umin1); }
            set { this._umin1 = ValuesConverterCommon.SetU(value); }
        }

                [BindingProperty(4)]
        [XmlElement(ElementName = "Umax1")]
        public double Umax1
        {
            get { return ValuesConverterCommon.GetU(this._umax1); }
            set { this._umax1 = ValuesConverterCommon.SetU(value); }
        }

                [BindingProperty(5)]
        [XmlElement(ElementName = "Umin2")]
        public double Umin2
        {
            get { return ValuesConverterCommon.GetU(this._umin2); }
            set { this._umin2 = ValuesConverterCommon.SetU(value); }
        }

                [BindingProperty(6)]
        [XmlElement(ElementName = "Umax2")]
                public double Umax2
        {
            get { return ValuesConverterCommon.GetU(this._umax2); }
            set { this._umax2 = ValuesConverterCommon.SetU(value); }
        }

        /// <summary>
        /// время АВР возврат
        /// </summary>
        [BindingProperty(7)]
        [XmlElement(ElementName = "время_АВР_возврат")]
        public int TimeBack
        {
            get { return ValuesConverterCommon.GetWaitTime(this._timeOff); }
            set { this._timeOff = ValuesConverterCommon.SetWaitTime(value); }
        }

        /// <summary>
        /// задержка отключения резерва
        /// </summary>
        [BindingProperty(8)]
        [XmlElement(ElementName = "задержка_отключения_резерва")]
        public int TimeOtkl
        {
            get { return ValuesConverterCommon.GetWaitTime(this._timeOtkl); }
            set { this._timeOtkl = ValuesConverterCommon.SetWaitTime(value); }
        }
        #endregion [Properties]



    }
}
