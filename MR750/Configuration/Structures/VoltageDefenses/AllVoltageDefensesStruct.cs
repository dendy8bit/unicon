﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR750.Configuration.Structures.VoltageDefenses
{
    public class AllVoltageDefensesStruct : StructBase, IDgvRowsContainer<VoltageDefenseStruct>
    {
        [Layout(0, Count = 8)]
        private VoltageDefenseStruct[] _externalDefenses;



        public VoltageDefenseStruct[] Rows
        {
            get { return _externalDefenses; }
            set { _externalDefenses = value; }
        }
    }
}
