﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR750.Configuration.Structures.Relay
{
    /// <summary>
    /// параметры выходных реле
    /// </summary>
    [XmlType(TypeName = "Одно_реле")]
    public class ReleOutputStruct : StructBase
    {
        #region [Private fields]

        [Layout(0)] private ushort _signal;
        [Layout(1)] private ushort _pulse;
    //    [Layout(2)] private ushort _wait;
    //    [Layout(3)] private ushort _rez;

        #endregion [Private fields]


        #region [Properties]

        /// <summary>
        /// Тип
        /// </summary>
        [BindingProperty(0)]
        [XmlAttribute(AttributeName = "Тип")]
        public string TypeXml
        {
            get { return Validator.Get(this._signal, StringsConfig.ReleyType,15); }
            set { this._signal = Validator.Set(value, StringsConfig.ReleyType, this._signal, 15); }
        }

        /// <summary>
        /// Сигнал
        /// </summary>
        [BindingProperty(1)]
        [XmlAttribute(AttributeName = "Сигнал")]
        public string SignalXml
        {
            get { return Validator.Get(this._signal, StringsConfig.RelaySignalsMr750,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14); }
            set { this._signal = Validator.Set(value, StringsConfig.RelaySignalsMr750, this._signal, 0, 1, 2, 3, 4, 5, 6, 7,8,9,10,11,12,13,14); }
        }
        
        /// <summary>
        /// Время
        /// </summary>
        [BindingProperty(2)]
        [XmlAttribute(AttributeName = "Время")]
        public int Wait
        {
            get { return ValuesConverterCommon.GetWaitTime(this._pulse); }
            set { this._pulse = ValuesConverterCommon.SetWaitTime(value); }
        }
        
        #endregion [Properties]
    }
}
