using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Xml.Serialization;
using BEMN.CS.Data;
using BEMN.CS.Utils;
using BEMN.Forms;

namespace BEMN.CS
{
    public class ReleItem
    {
        public const int LENGTH = 4;
        private ushort[] _values;

        public ReleItem()
        {
            SetBuffer(new ushort[LENGTH]);
        }

        private string _name;

        [XmlAttribute("��������")]
        [DisplayName("��������")]
        [Category("��������� ���")]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }


        public override string ToString()
        {
            return "���������� �����";
        }

        public ReleItem(ushort[] buffer)
        {
            SetBuffer(buffer);
        }

        public void SetBuffer(ushort[] buffer)
        {
            if (LENGTH != buffer.Length)
            {
                throw new ArgumentOutOfRangeException("buffer.Length", buffer.Length,
                                                      "Buffer of ReleItem must be " + LENGTH);
            }
            _values = buffer;
        }

        [XmlAttribute("���")]
        [DisplayName("���")]
        [Description("��� ����������� �����")]
        [Category("��������� ���")]
        [TypeConverter(typeof(ReleTypeConverter))]
        public string Mode
        {
            get
            {
                ushort index = _values[1];
                if (index >= Strings.releType.Count)
                {
                    index = (byte)(Strings.releType.Count - 1);
                }
                return Strings.releType[index];
            }
            set
            {
                try
                {
                    _values[1] = (ushort)Strings.releType.IndexOf(value);
                }
                catch (ArgumentOutOfRangeException)
                {
                    _values[1] = (ushort)(Strings.releType.Count - 1);
                }
            }
        }

        [XmlAttribute("�������")]
        [DisplayName("�������")]
        [Description("������������ �������� (��)")]
        [Category("��������� ���")]
        public double Impulse
        {
            get { return Measuring.GetTime(_values[0]); }
            set { _values[0] = Measuring.SetTime(value); }
        }

    
        [XmlAttribute("��������")]
        [DisplayName("��������")]
        [Description("������ � ������� ����")]
        [Category("������� ���")]
        [Editor(typeof(RussianCollectionEditor), typeof(UITypeEditor))]
        public ushort[] Values
        {
            get { return _values; }
            set { _values = value; }
        }
    }
}