using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using BEMN.CS.Data;
using BEMN.Devices;
using BEMN.Forms.Export;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Forms.ValidatingClasses.Rules;
using BEMN.Forms.ValidatingClasses.Rules.Double;
using BEMN.Interfaces;
using CS;
using CS.Structures;

namespace BEMN.CS.Forms
{
    public partial class CS_ConfigurationForm : Form, IFormView
    {
        private NewStructValidator<CorrectionValidationStruct> _timeValidator;

        private CS _device;
        private bool _validatingOk = true;
        public const string TIMELIMIT_ERROR_MSG = "������� ����� � ��������� [0 - 60]";
        private bool _connectingErrors = false;

        public CS_ConfigurationForm()
        {
            InitializeComponent();
        }


        public void Init()
        {
            _timeValidator = new NewStructValidator<CorrectionValidationStruct>
                (
                _toolTip,
                new ControlInfoText(_timeValidation, new CustomDoubleRule(0, 86.0))
                );
        }

        #region ���������


        private bool TimeValidate(string time, out string message)
        {
            message = string.Empty;
            double value;

            var flag = double.TryParse(time, out value);

            if ((value > 60) | (!flag))
            {
                message = TIMELIMIT_ERROR_MSG;
                return false;
            }

            if (value == 0)
            {
                return true;
            }

            return true;
        }

        /// <summary>
        /// ����
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _dataGridReleView_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.ColumnIndex != 2)
                return;

            string message;
            if (this.TimeValidate(e.FormattedValue.ToString(), out message))
                return;

            e.Cancel = true;

            MessageBox.Show(message);
        }

        /// <summary>
        /// ���
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _dataGridKISView_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if ((e.ColumnIndex != 3) & (e.ColumnIndex != 4))
                return;

            string message;
            if (this.TimeValidate(e.FormattedValue.ToString(), out message)) 
                return;

            e.Cancel = true;

            MessageBox.Show(message);
        }

        /// <summary>
        /// �������
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _dataGridDiskretView_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if ((e.ColumnIndex != 3) & (e.ColumnIndex != 4))
                return;

            string message;
            if (this.TimeValidate(e.FormattedValue.ToString(), out message))
                return;

            e.Cancel = true;

            MessageBox.Show(message);
        }

        #endregion

        public CS_ConfigurationForm(CS device)
        {
            _device = device;
            InitializeComponent();
            Init();

            _device.Version.AllReadFail += HandlerHelper.CreateReadArrayHandler(this,VersionReadFail) ;
            _device.Version.AllReadOk += HandlerHelper.CreateReadArrayHandler(this,VersionReadOk);
        }

        private void VersionReadOk()
        {
            DiskretSaveFromGrid();
            ReleSaveFromGrid();
            KisSaveFromGrid();
            WriteTimeValidation();
            _connectingErrors = false;
            _processLabel.Text = "���� ������";
            _device.SendLogicCommand(CS.LogicCommand.STOP);
            _device.SaveDiskretConfig();
            _device.SaveKISConfig();
            _device.SaveRele();
            _device.SaveTimeValidation();
            if (_device.Version.Value.Is6_84AndMore)
            {
                _device.SendEndCommand(CS.LogicCommand.END);
            }
            _device.LoadLogicCommand();
        }

        private void VersionReadFail()
        {
            _processLabel.Text = "���������� �������� ������������";
            MessageBox.Show("���������� �������� ������������.");
        }

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof (CS); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof (CS_ConfigurationForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get
            {
                return AssemblyResourses.Resources.config.ToBitmap();
            }
        }

        public string NodeName
        {
            get { return "������������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] {}; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        private void CS_ConfigurationForm_Load(object sender, EventArgs e)
        {
            ((DataGridViewComboBoxColumn) _dataGridDiskretView.Columns[1]).Items.AddRange(Strings.diskretIn.ToArray());
            ((DataGridViewComboBoxColumn) _dataGridDiskretView.Columns[2]).Items.AddRange(Strings.diskretIndication.ToArray());
            ((DataGridViewComboBoxColumn) _dataGridReleView.Columns[1]).Items.AddRange(Strings.releType.ToArray());
            ((DataGridViewComboBoxColumn) _dataGridKISView.Columns[1]).Items.AddRange(Strings.kisType.ToArray());
            ((DataGridViewComboBoxColumn) _dataGridKISView.Columns[2]).Items.AddRange(Strings.kisKontrol.ToArray());
            _device.ReleLoadOk += new Handler(_device_ReleLoadOk);
            _device.DiskretLoadOk += new Handler(_device_DiskretLoadOk);
            _device.KISLoadOk += new Handler(_device_KISLoadOk);
            _device.ReleSaveOk += new Handler(_device_ReleSaveOk);
            _device.DiskretSaveOk += new Handler(_device_DiskretSaveOk);
            _device.KISSaveOk += new Handler(_device_KISSaveOk);
            _device.ReleLoadFail += new Handler(_device_ReleLoadFail);
            _device.DiagnosticLoadFail += new Handler(_device_DiagnosticLoadFail);
            _device.KISLoadFail += new Handler(_device_KISLoadFail);
            _device.ReleSaveFail += new Handler(_device_ReleSaveFail);
            _device.DiskretSaveFail += new Handler(_device_DiskretSaveFail);
            _device.KISSaveFail += new Handler(_device_KISSaveFail);
            _device.TimeValidLoadOk += new Handler(_device_TimeValidLoadOk);
            _device.TimeValidLoadFail += new Handler(_device_TimeValidLoadFail);
            _device.LogicCommandLoadOk += new Handler(_device_LogicCommandLoadOk);

            PrepareValidation();

            _dataGridDiskretView.EditingControlShowing +=
                new DataGridViewEditingControlShowingEventHandler(Grid_EditingControlShowing);
            _dataGridKISView.EditingControlShowing +=
                new DataGridViewEditingControlShowingEventHandler(Grid_EditingControlShowing);
            _dataGridReleView.EditingControlShowing +=
                new DataGridViewEditingControlShowingEventHandler(Grid_EditingControlShowing);

            DiskretLoadToGrid();
            ReleLoadToGrid();
            KISLoadToGrid();
            _exchangeProgressBar.Value = 0;
            _processLabel.Text = "";
            readConfig();
        }

        void _device_LogicCommandLoadOk(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(Compleate));
            }
            catch (InvalidOperationException)
            {
            }
        }

        void _device_TimeValidLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(OnLoadFail));
            }
            catch (InvalidOperationException)
            {
            }
        }

        void _device_TimeValidLoadOk(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(OnTimeValidLoadOk));
            }
            catch (InvalidOperationException)
            {
            }
        }

        private void PrepareValidation() 
        {
            _timeValidationSign.SelectedIndex = 0;
        }

        private void OnTimeValidLoadOk() 
        {
            _timeValidation.Text = _device.TimeValidation.ToString();
            _timeValidationSign.SelectedItem = _device.TimeValidationSign;
        }

        private void WriteTimeValidation() 
        {
            try
            {
                _device.TimeValidation = Convert.ToDouble(_timeValidation.Text);
                _device.TimeValidationSign = _timeValidationSign.SelectedItem.ToString();
            }
            catch { }
        }

        private void OnSaveFail()
        {
            if (!_connectingErrors)
            {
                MessageBox.Show("���������� �������� ������������. ��������� �����", "���100. ������", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            _connectingErrors = true;
        }

        private bool _readCommandFlag = false;
        private void Compleate()
        {
            if (/*_connectingErrors == false &&*/ _device.Command == 0)
            {
                _processLabel.Text = "������ ������� ���������.";
                _device.RemoveLogicCommand();
            }
            else
            {
                if (!_readCommandFlag)
                {
                    _readCommandFlag = true;
                }
                else
                {
                    _device.RemoveLogicCommand();
                    OnSaveFail();
                }
                
            }
           // _connectingErrors = true;
        }

        private void OnLoadFail()
        {
            if (!_connectingErrors)
            {
                MessageBox.Show("���������� �������� ������������. ��������� �����", "���100. ������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            _connectingErrors = true;
        }

        void _device_KISSaveFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(OnSaveFail));
            }
            catch (InvalidOperationException)
            {
            }
        }

        void _device_DiskretSaveFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(OnSaveFail));
            }
            catch (InvalidOperationException)
            {
            }
        }

        void _device_ReleSaveFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(OnSaveFail));
            }
            catch (InvalidOperationException)
            {
            }
        }

        void _device_KISLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(OnLoadFail));
            }
            catch (InvalidOperationException)
            {
            }
        }

        void _device_DiagnosticLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(OnLoadFail));
            }
            catch (InvalidOperationException)
            {
            }
        }

        void _device_ReleLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(OnLoadFail));
            }
            catch (InvalidOperationException)
            {
            }
        }

        void _device_KISSaveOk(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(KisSaveFromGrid));
            }
            catch (InvalidOperationException)
            {
            }
        }

        void _device_DiskretSaveOk(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(DiskretLoadToGrid));
            }
            catch (InvalidOperationException)
            {
            }
        }

        void _device_ReleSaveOk(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(ReleSaveFromGrid));
            }
            catch (InvalidOperationException)
            {
            }
        }
 
        private void DiskretSaveFromGrid()
        {
            for (int j = 0; j < _dataGridDiskretView.Rows.Count; j++)
            {
                BitArray commutation = new BitArray(8);
                BitArray blocking = new BitArray(8);
                _device.Diskrets[j].Mode = _dataGridDiskretView.Rows[j].Cells[1].Value.ToString();
                _device.Diskrets[j].Indication = _dataGridDiskretView.Rows[j].Cells[2].Value.ToString();
                _device.Diskrets[j].FrontTime = double.Parse(_dataGridDiskretView.Rows[j].Cells[3].Value.ToString());
                _device.Diskrets[j].DeclineTime = double.Parse(_dataGridDiskretView.Rows[j].Cells[4].Value.ToString());
                commutation[0] = Convert.ToBoolean(_dataGridDiskretView.Rows[j].Cells[5].Value.ToString());
                commutation[1] = Convert.ToBoolean(_dataGridDiskretView.Rows[j].Cells[6].Value.ToString());
                commutation[2] = Convert.ToBoolean(_dataGridDiskretView.Rows[j].Cells[7].Value.ToString());
                commutation[3] = Convert.ToBoolean(_dataGridDiskretView.Rows[j].Cells[8].Value.ToString());
                commutation[4] = Convert.ToBoolean(_dataGridDiskretView.Rows[j].Cells[9].Value.ToString());
                commutation[5] = Convert.ToBoolean(_dataGridDiskretView.Rows[j].Cells[10].Value.ToString());
                commutation[6] = Convert.ToBoolean(_dataGridDiskretView.Rows[j].Cells[11].Value.ToString());
                blocking[0] = Convert.ToBoolean(_dataGridDiskretView.Rows[j].Cells[12].Value.ToString());
                blocking[1] = Convert.ToBoolean(_dataGridDiskretView.Rows[j].Cells[13].Value.ToString());
                blocking[2] = Convert.ToBoolean(_dataGridDiskretView.Rows[j].Cells[14].Value.ToString());
                blocking[3] = Convert.ToBoolean(_dataGridDiskretView.Rows[j].Cells[15].Value.ToString());
                blocking[4] = Convert.ToBoolean(_dataGridDiskretView.Rows[j].Cells[16].Value.ToString());
                blocking[5] = Convert.ToBoolean(_dataGridDiskretView.Rows[j].Cells[17].Value.ToString());
                blocking[6] = Convert.ToBoolean(_dataGridDiskretView.Rows[j].Cells[18].Value.ToString());

                _device.Diskrets.SetCommunication(commutation, j);
                _device.Diskrets.SetBlocking(blocking, j);

                List<bool> _res  = new List<bool>();
                for (int i = 0; i < 7; i++)
                {
                    _res.Add(commutation[i]);
                }
                for (int i = 0; i < 7; i++)
                {
                    _res.Add(blocking[i]);
                }
                _device.Diskrets[j].Params = _res.ToArray();
            }
            _exchangeProgressBar.PerformStep();
        }
        
        private void ReleSaveFromGrid()
        {
            for (int j = 0; j < _dataGridReleView.Rows.Count; j++)
            {
                _device.Rele[j].Mode = _dataGridReleView.Rows[j].Cells[1].Value.ToString();
                _device.Rele[j].Impulse = double.Parse(_dataGridReleView.Rows[j].Cells[2].Value.ToString());
            }
            _exchangeProgressBar.PerformStep();
        }

        private void KisSaveFromGrid()
        {
            for (int j = 0; j < _dataGridKISView.Rows.Count; j++)
            {
                BitArray commutation = new BitArray(8);
                BitArray blocking = new BitArray(8);
                _device.KIS[j].Mode = _dataGridKISView.Rows[j].Cells[1].Value.ToString();
                _device.KIS[j].Control = _dataGridKISView.Rows[j].Cells[2].Value.ToString();
                _device.KIS[j].FrontDelay = Convert.ToUInt16(_dataGridKISView.Rows[j].Cells[3].Value.ToString());
                _device.KIS[j].BackDelay = Convert.ToUInt16(_dataGridKISView.Rows[j].Cells[4].Value.ToString());
                commutation[0] = Convert.ToBoolean(_dataGridKISView.Rows[j].Cells[5].Value.ToString());
                commutation[1] = Convert.ToBoolean(_dataGridKISView.Rows[j].Cells[6].Value.ToString());
                commutation[2] = Convert.ToBoolean(_dataGridKISView.Rows[j].Cells[7].Value.ToString());
                commutation[3] = Convert.ToBoolean(_dataGridKISView.Rows[j].Cells[8].Value.ToString());
                commutation[4] = Convert.ToBoolean(_dataGridKISView.Rows[j].Cells[9].Value.ToString());
                commutation[5] = Convert.ToBoolean(_dataGridKISView.Rows[j].Cells[10].Value.ToString());
                commutation[6] = Convert.ToBoolean(_dataGridKISView.Rows[j].Cells[11].Value.ToString());
                blocking[0] = Convert.ToBoolean(_dataGridKISView.Rows[j].Cells[12].Value.ToString());
                blocking[1] = Convert.ToBoolean(_dataGridKISView.Rows[j].Cells[13].Value.ToString());
                blocking[2] = Convert.ToBoolean(_dataGridKISView.Rows[j].Cells[14].Value.ToString());
                blocking[3] = Convert.ToBoolean(_dataGridKISView.Rows[j].Cells[15].Value.ToString());
                blocking[4] = Convert.ToBoolean(_dataGridKISView.Rows[j].Cells[16].Value.ToString());
                blocking[5] = Convert.ToBoolean(_dataGridKISView.Rows[j].Cells[17].Value.ToString());
                blocking[6] = Convert.ToBoolean(_dataGridKISView.Rows[j].Cells[18].Value.ToString());

                _device.KIS.SetCommunication(commutation, j);
                _device.KIS.SetBlocking(blocking, j);

                List<bool> _res = new List<bool>();
                for (int i = 0; i < 7; i++)
                {
                    _res.Add(commutation[i]);
                }
                for (int i = 0; i < 7; i++)
                {
                    _res.Add(blocking[i]);
                }
                _device.KIS[j].Params = _res.ToArray();
            }
            _exchangeProgressBar.PerformStep();
        }

        private void DiskretLoadToGrid()
        {
            _dataGridDiskretView.Rows.Clear();

            for (int j = 0; j < _device.Diskrets.Count; j++)
            {
                BitArray commutation = _device.Diskrets.GetCommutation(j);
                BitArray blocking = _device.Diskrets.GetBlocking(j);

                _dataGridDiskretView.Rows.Add(new object[]
                                                  {
                                                      (j + 1).ToString(),
                                                      _device.Diskrets[j].Mode,
                                                      _device.Diskrets[j].Indication,
                                                      _device.Diskrets[j].FrontTime,
                                                      _device.Diskrets[j].DeclineTime,
                                                      commutation[0],
                                                      commutation[1],
                                                      commutation[2],
                                                      commutation[3],
                                                      commutation[4],
                                                      commutation[5],
                                                      commutation[6],
                                                      blocking[0],
                                                      blocking[1],
                                                      blocking[2],
                                                      blocking[3],
                                                      blocking[4],
                                                      blocking[5],
                                                      blocking[6]
                                                  });
                
            }
            _exchangeProgressBar.PerformStep();
        }

        private void ReleLoadToGrid()
        {
            _dataGridReleView.Rows.Clear();
            for (int j = 0; j < _device.Rele.Count; j++)
            {
                _dataGridReleView.Rows.Add(new object[]
                                               {
                                                    ((j + 1) == _device.Rele.Count)? "3C" : (j + 1).ToString(),
                                                   _device.Rele[j].Mode,
                                                   _device.Rele[j].Impulse
                                               });
                
            }
            _exchangeProgressBar.PerformStep();
        }

        private void KISLoadToGrid() 
        {
            _dataGridKISView.Rows.Clear();

            for (int j = 0; j < _device.KIS.Count; j++)
            {
                BitArray commutation = _device.KIS.GetCommutation(j);
                BitArray blocking = _device.KIS.GetBlocking(j);

                _dataGridKISView.Rows.Add(new object[]
                                              {
                                                  (j + 1).ToString(),
                                                  _device.KIS[j].Mode,
                                                  _device.KIS[j].Control,
                                                  _device.KIS[j].FrontDelay.ToString(),
                                                  _device.KIS[j].BackDelay.ToString(),
                                                  commutation[0],
                                                  commutation[1],
                                                  commutation[2],
                                                  commutation[3],
                                                  commutation[4],
                                                  commutation[5],
                                                  commutation[6],
                                                  blocking[0],
                                                  blocking[1],
                                                  blocking[2],
                                                  blocking[3],
                                                  blocking[4],
                                                  blocking[5],
                                                  blocking[6]
                                              });
                
            }
            _exchangeProgressBar.PerformStep();
            if (_connectingErrors == false)
            {
                _processLabel.Text = "������ ������� ���������.";
                _exchangeProgressBar.Value = _exchangeProgressBar.Maximum;
            }
            else
            {
                _processLabel.Text = "������ ��������� ���������.";
            }
            _connectingErrors = true;

        }

        private void _device_DiskretLoadOk(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(DiskretLoadToGrid));
            }
            catch (InvalidOperationException)
            {
            }
        }

        private void _device_ReleLoadOk(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(ReleLoadToGrid));
            }
            catch (InvalidOperationException)
            {
            }
        }

        private void _device_KISLoadOk(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(KISLoadToGrid));
            }
            catch (InvalidOperationException)
            {
            }
        }

        private void _readConfigBut_Click(object sender, EventArgs e)
        {
           this.readConfig();
            
        }

        private void readConfig()
        {
             _exchangeProgressBar.Value = 0;
            _processLabel.Text = "���� ������...";
            _connectingErrors = false;
            PrepareValidation();
            _device.LoadDiskretConfig();            
            _device.LoadRele();
            _device.LoadKISConfig();
            _device.LoadTimeValidation();
        }

        private void _writeConfigBut_Click(object sender, EventArgs e)
        {
            WriteConfig();
        }

        private void WriteConfig()
        {
            _readCommandFlag = false;
            _exchangeProgressBar.Value = 0;
            _validatingOk = true;
            for (int i = 3; i < 5; i++)
            {
                for (int j = 0; j < _dataGridDiskretView.RowCount; j++)
                {
                    _validatingOk &= DataGridCellEditing(_dataGridDiskretView, i, j);
                }
            }

            for (int i = 0; i < _dataGridReleView.RowCount; i++)
            {
                _validatingOk &= DataGridCellEditing(_dataGridReleView, 2, i);
            }
            for (int i = 3; i < 5; i++)
            {
                for (int j = 0; j < _dataGridKISView.RowCount; j++)
                {
                    _validatingOk &= DataGridCellEditing(_dataGridKISView, i, j);
                }
            }

            if (_validatingOk &&
                DialogResult.Yes ==
                MessageBox.Show("�������� ������������?", "������", MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                                MessageBoxDefaultButton.Button2))
            {
                _device.Version.LoadStruct();

            } 
        }
        private void ValidateAll()
        {
            _validatingOk = true;
            for (int i = 3; i < 5; i++)
            {
                for (int j = 0; j < _dataGridDiskretView.RowCount; j++)
                {
                    DataGridCellEditing(_dataGridDiskretView, i, j);
                }
            }
            for (int i = 0; i < _dataGridReleView.RowCount; i++)
            {
                _validatingOk &= DataGridCellEditing(_dataGridReleView, 2, i);
            }
            for (int i = 3; i < 5; i++)
            {
                for (int j = 0; j < _dataGridKISView.RowCount; j++)
                {
                    _validatingOk &= DataGridCellEditing(_dataGridKISView, i, j);
                }
            }
            WriteTimeValidation();
            DiskretSaveFromGrid();
            ReleSaveFromGrid();
            KisSaveFromGrid();
            _connectingErrors = false;
        }

        private void _saveConfigBut_Click(object sender, EventArgs e)
        {
            SaveinFile();
        }

        private void SaveinFile()
        {
            this.ValidateAll();
            if (!_validatingOk)
            {
                return;
            }
            if (DialogResult.OK == _saveConfigurationDlg.ShowDialog())
            {
                _exchangeProgressBar.Value = 0;
                _device.Serialize(_saveConfigurationDlg.FileName);
                _processLabel.Text = "���� " + _openConfigurationDlg.FileName + " ��������";
            } 
        }
        private void _loadConfigBut_Click(object sender, EventArgs e)
        {
            ReadFromFile();
        }

        private void ReadFromFile()
        {
            _exchangeProgressBar.Value = 0;
            if (DialogResult.OK == _openConfigurationDlg.ShowDialog())
            {
                try
                {
                    _device.Deserialize(_openConfigurationDlg.FileName);
                }
                catch (System.IO.FileLoadException exc)
                {
                    _processLabel.Text = "���� " + System.IO.Path.GetFileName(exc.FileName) + " �� �������� ������ ������� ��500 ��� ���������";
                    return;
                }
                _exchangeProgressBar.Value = 0;
                DiskretLoadToGrid();
                KISLoadToGrid();
                ReleLoadToGrid();

                _connectingErrors = false;
                _processLabel.Text = "���� " + _openConfigurationDlg.FileName + " ��������";
                _exchangeProgressBar.Value = _exchangeProgressBar.Maximum;
            } 
        }
        private void ShowToolTip(string msg, DataGridViewCell cell, TabPage page)
        {
            cell.OwningColumn.DataGridView.CurrentCell.Selected = false;
            if (_validatingOk)
            {
                _tabControl.SelectedTab = page;
                _toolTip.Show(msg, this, Location, 2000);
                cell.Selected = true;
                _validatingOk = false;
            }
        }
        
        private bool DataGridCellEditing(DataGridView grid, int colIndex, int rowIndex)
        {
            bool ret = true;
            try
            {
                double value = double.Parse(grid[colIndex, rowIndex].Value.ToString());
                if (value > 60 || value < 0)
                {
                    ShowToolTip(TIMELIMIT_ERROR_MSG, grid[colIndex, rowIndex], _konfDiscretPage);
                    ret = false;
                }
            }
            catch (Exception)
            {
                ShowToolTip(TIMELIMIT_ERROR_MSG, grid[colIndex, rowIndex], _konfDiscretPage);
                ret = false;
            }

            return ret;
        }

        private static void Grid_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is DataGridViewComboBoxEditingControl)
            {
                DataGridViewComboBoxEditingControl combo = e.Control as DataGridViewComboBoxEditingControl;
                if (null != combo)
                {
                    combo.SelectedIndex = 0;
                    combo.Items.RemoveAt(combo.Items.Count - 1);
                }
            }
        }

        private void _typeValidWrite_Click(object sender, EventArgs e)
        {
            WriteTimeValidation();
            _device.SaveTimeValidation();
        }

        private void _saveToXmlButton_Click(object sender, EventArgs e)
        {
            ValidateAll();
            if (!_validatingOk)
            {
                return;
            }
       
            try
            {
    
                _processLabel.Text = HtmlExport.ExportHtml(AssemblyResourses.Resources.���, _device,"���",_device.DeviceVersion);
            }
            catch (Exception)
            {
                MessageBox.Show("������ ����������");
            }
        }

        private void CSConfigurationForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers != Keys.Control) return;
            switch (e.KeyCode)
            {
                case Keys.W:
                    WriteConfig();
                    break;
                case Keys.R:
                    this.readConfig();
                    break;
                case Keys.S:
                    this.SaveinFile();
                    break;
                case Keys.O:
                    this.ReadFromFile();
                    break;
            }
            e.Handled = true;
        }
    }
}