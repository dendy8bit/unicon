using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Xml.Serialization;
using BEMN.CS.Data;
using BEMN.CS.Utils;
using BEMN.Forms;
using BEMN.MBServer;

namespace BEMN.CS
{
    public class KISItem
    {
        public const int LENGTH = 4;
        private ushort[] _values;

        public KISItem()
        {
            SetBuffer(new ushort[LENGTH]);
        }

        private string _name;

        [XmlAttribute("��������")]
        [DisplayName("��������")]
        [Category("��������� ���")]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }


        public override string ToString()
        {
            return "���";
        }

        public KISItem(ushort[] buffer)
        {
            SetBuffer(buffer);
        }

        public void SetBuffer(ushort[] buffer)
        {
            if (LENGTH != buffer.Length)
            {
                throw new ArgumentOutOfRangeException("buffer.Length", buffer.Length,
                                                      "Buffer of KISItem must be " + LENGTH);
            }
            _values = buffer;
        }

        [XmlAttribute("���")]
        [DisplayName("���")]
        [Description("��� ���")]
        [Category("��������� ���")]
        [TypeConverter(typeof(KISModeTypeConverter))]
        public string Mode
        {
            get
            {
                byte index = Common.LOBYTE(_values[1]);
                if (index >= Strings.kisType.Count)
                {
                    index = (byte)(Strings.kisType.Count - 1);
                }
                return Strings.kisType[index];
            }
            set
            {
                try
                {
                    _values[1] = Common.TOWORD(Common.HIBYTE(_values[1]), (byte)Strings.kisType.IndexOf(value));
                }
                catch (ArgumentOutOfRangeException)
                {
                    _values[1] = Common.TOWORD(Common.HIBYTE(_values[1]), (byte)(Strings.kisType.Count - 1));
                }
            }
        }

        [XmlAttribute("��������")]
        [DisplayName("��������")]
        [Description("�������� ���")]
        [Category("��������� ���")]
        [TypeConverter(typeof(KISControlTypeConverter))]
        public string Control
        {
            get
            {
                byte index = Common.HIBYTE(_values[1]);
                if (index >= Strings.kisKontrol.Count)
                {
                    index = (byte)(Strings.kisKontrol.Count - 1);
                }
                return Strings.kisKontrol[index];
            }
            set
            {
                try
                {
                    _values[1] = Common.TOWORD((byte)Strings.kisKontrol.IndexOf(value), Common.LOBYTE(_values[1]));
                }
                catch (ArgumentOutOfRangeException)
                {
                    _values[1] =
                        Common.TOWORD((byte)Strings.kisKontrol.IndexOf(value),
                                      (byte)(Strings.kisKontrol.Count - 1));
                }
            }
        }

        [XmlAttribute("�����_�����")]
        [DisplayName("�����_�����")]
        [Description("����� �����")]
        [Category("��������� ���")]
        [TypeConverter(typeof(KISControlTypeConverter))]
        public ushort BackDelay
        {
            get
            {
                ushort time = (ushort)Common.HIBYTE(_values[2]);
                return time;
            }
            set
            {
                _values[2] = Common.TOWORD((byte)value, Common.LOBYTE(_values[2]));
            }
        }

        [XmlAttribute("�����_����")]
        [DisplayName("�����_����")]
        [Description("����� ����")]
        [Category("��������� ���")]
        [TypeConverter(typeof(KISControlTypeConverter))]
        public ushort FrontDelay
        {
            get
            {
                return _values[3];
            }
            set
            {
                _values[3] = value;
            }
        }

        [XmlAttribute("��������")]
        [DisplayName("��������")]
        [Description("������ � ������� ����")]
        [Category("������� ���")]
        [Editor(typeof(RussianCollectionEditor), typeof(UITypeEditor))]
        public ushort[] Values
        {
            get { return _values; }
            set { _values = value; }
        }

        private bool[] _params;
        [XmlElement("���������")]
       // [XmlAttribute("���������")]
        public bool[] Params
        {
            get { return _params; }
            set { _params = value; }
        }
    }
}