namespace BEMN.CS
{
    public class Measuring
    {
        public static double GetTime(ushort value)
        {
            return (double) value/1000;
        }

        public static ushort SetTime(double value)
        {
            return (ushort) (value*1000);
        }
    }
}