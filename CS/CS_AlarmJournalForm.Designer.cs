using System.Windows.Forms;

namespace BEMN.CS
{
    partial class CsAlarmJournalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            this._journalCntLabel = new System.Windows.Forms.Label();
            this._deserializeBut = new System.Windows.Forms.Button();
            this._serializeBut = new System.Windows.Forms.Button();
            this._readBut = new System.Windows.Forms.Button();
            this._openSysJounralDlg = new System.Windows.Forms.OpenFileDialog();
            this._saveSysJournalDlg = new System.Windows.Forms.SaveFileDialog();
            this._journalGrid = new System.Windows.Forms.DataGridView();
            this._journalProgress = new System.Windows.Forms.ProgressBar();
            this._numberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._dateCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._mesColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._valueCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._diskret1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._diskret2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._diskret3Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._diskret4Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._releCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._kisCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this._journalGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // _journalCntLabel
            // 
            this._journalCntLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._journalCntLabel.AutoSize = true;
            this._journalCntLabel.Location = new System.Drawing.Point(218, 400);
            this._journalCntLabel.Name = "_journalCntLabel";
            this._journalCntLabel.Size = new System.Drawing.Size(16, 13);
            this._journalCntLabel.TabIndex = 23;
            this._journalCntLabel.Text = "...";
            // 
            // _deserializeBut
            // 
            this._deserializeBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._deserializeBut.Location = new System.Drawing.Point(935, 388);
            this._deserializeBut.Name = "_deserializeBut";
            this._deserializeBut.Size = new System.Drawing.Size(126, 23);
            this._deserializeBut.TabIndex = 21;
            this._deserializeBut.Text = "��������� �� �����";
            this._deserializeBut.UseVisualStyleBackColor = true;
            this._deserializeBut.Click += new System.EventHandler(this._deserializeBut_Click);
            // 
            // _serializeBut
            // 
            this._serializeBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._serializeBut.Location = new System.Drawing.Point(803, 388);
            this._serializeBut.Name = "_serializeBut";
            this._serializeBut.Size = new System.Drawing.Size(126, 23);
            this._serializeBut.TabIndex = 20;
            this._serializeBut.Text = "��������� � ����";
            this._serializeBut.UseVisualStyleBackColor = true;
            this._serializeBut.Click += new System.EventHandler(this._serializeBut_Click);
            // 
            // _readBut
            // 
            this._readBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._readBut.Location = new System.Drawing.Point(5, 390);
            this._readBut.Name = "_readBut";
            this._readBut.Size = new System.Drawing.Size(101, 23);
            this._readBut.TabIndex = 19;
            this._readBut.Text = "���������";
            this._readBut.UseVisualStyleBackColor = true;
            this._readBut.Click += new System.EventHandler(this._readBut_Click);
            // 
            // _openSysJounralDlg
            // 
            this._openSysJounralDlg.DefaultExt = "xml";
            this._openSysJounralDlg.Filter = "(*.xml) | *.xml";
            this._openSysJounralDlg.RestoreDirectory = true;
            this._openSysJounralDlg.Title = "������� ������  ������� ��� ��";
            // 
            // _saveSysJournalDlg
            // 
            this._saveSysJournalDlg.DefaultExt = "xml";
            this._saveSysJournalDlg.FileName = "��_������_������";
            this._saveSysJournalDlg.Filter = "(*.xml) | *.xml";
            this._saveSysJournalDlg.Title = "���������  ������ ������� ��� ��";
            // 
            // _journalGrid
            // 
            this._journalGrid.AllowUserToAddRows = false;
            this._journalGrid.AllowUserToDeleteRows = false;
            this._journalGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._journalGrid.BackgroundColor = System.Drawing.Color.White;
            this._journalGrid.ColumnHeadersHeight = 34;
            this._journalGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._journalGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._numberCol,
            this._dateCol,
            this._mesColumn,
            this._valueCol,
            this._diskret1Col,
            this._diskret2Col,
            this._diskret3Col,
            this._diskret4Col,
            this._releCol,
            this._kisCol});
            this._journalGrid.Location = new System.Drawing.Point(0, 0);
            this._journalGrid.Name = "_journalGrid";
            this._journalGrid.RowHeadersVisible = false;
            this._journalGrid.RowHeadersWidth = 40;
            this._journalGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._journalGrid.Size = new System.Drawing.Size(1073, 382);
            this._journalGrid.TabIndex = 24;
            // 
            // _journalProgress
            // 
            this._journalProgress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._journalProgress.Location = new System.Drawing.Point(112, 396);
            this._journalProgress.Name = "_journalProgress";
            this._journalProgress.Size = new System.Drawing.Size(100, 17);
            this._journalProgress.TabIndex = 22;
            // 
            // _numberCol
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._numberCol.DefaultCellStyle = dataGridViewCellStyle1;
            this._numberCol.HeaderText = "�";
            this._numberCol.Name = "_numberCol";
            this._numberCol.ReadOnly = true;
            this._numberCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._numberCol.Width = 30;
            // 
            // _dateCol
            // 
            this._dateCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._dateCol.HeaderText = "����";
            this._dateCol.Name = "_dateCol";
            this._dateCol.ReadOnly = true;
            this._dateCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._dateCol.Width = 130;
            // 
            // _mesColumn
            // 
            this._mesColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._mesColumn.HeaderText = "���������";
            this._mesColumn.Name = "_mesColumn";
            this._mesColumn.ReadOnly = true;
            this._mesColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._mesColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._mesColumn.Width = 190;
            // 
            // _valueCol
            // 
            this._valueCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this._valueCol.HeaderText = "��.";
            this._valueCol.Name = "_valueCol";
            this._valueCol.ReadOnly = true;
            this._valueCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._valueCol.Width = 29;
            // 
            // _diskret1Col
            // 
            this._diskret1Col.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._diskret1Col.DefaultCellStyle = dataGridViewCellStyle2;
            this._diskret1Col.HeaderText = "�. ��                     [1-10]";
            this._diskret1Col.Name = "_diskret1Col";
            this._diskret1Col.ReadOnly = true;
            this._diskret1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._diskret1Col.Width = 73;
            // 
            // _diskret2Col
            // 
            this._diskret2Col.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._diskret2Col.DefaultCellStyle = dataGridViewCellStyle3;
            this._diskret2Col.HeaderText = "�. ��                   [11-20]";
            this._diskret2Col.Name = "_diskret2Col";
            this._diskret2Col.ReadOnly = true;
            this._diskret2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._diskret2Col.Width = 76;
            // 
            // _diskret3Col
            // 
            this._diskret3Col.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._diskret3Col.DefaultCellStyle = dataGridViewCellStyle4;
            this._diskret3Col.HeaderText = "�. ��                   [21-30]";
            this._diskret3Col.Name = "_diskret3Col";
            this._diskret3Col.ReadOnly = true;
            this._diskret3Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._diskret3Col.Width = 76;
            // 
            // _diskret4Col
            // 
            this._diskret4Col.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._diskret4Col.DefaultCellStyle = dataGridViewCellStyle5;
            this._diskret4Col.HeaderText = "�. ��                   [31-40]";
            this._diskret4Col.Name = "_diskret4Col";
            this._diskret4Col.ReadOnly = true;
            this._diskret4Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._diskret4Col.Width = 76;
            // 
            // _releCol
            // 
            this._releCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._releCol.DefaultCellStyle = dataGridViewCellStyle6;
            this._releCol.HeaderText = "����                                             1   2   3   4   5   6  3C  H ";
            this._releCol.Name = "_releCol";
            this._releCol.ReadOnly = true;
            this._releCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._releCol.Width = 171;
            // 
            // _kisCol
            // 
            this._kisCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._kisCol.DefaultCellStyle = dataGridViewCellStyle7;
            this._kisCol.HeaderText = "���                                 1 2 3 4 5 6";
            this._kisCol.Name = "_kisCol";
            this._kisCol.ReadOnly = true;
            this._kisCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._kisCol.Width = 123;
            // 
            // CsAlarmJournalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1073, 423);
            this.Controls.Add(this._journalGrid);
            this.Controls.Add(this._journalCntLabel);
            this.Controls.Add(this._journalProgress);
            this.Controls.Add(this._deserializeBut);
            this.Controls.Add(this._serializeBut);
            this.Controls.Add(this._readBut);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(946, 411);
            this.Name = "CsAlarmJournalForm";
            this.Text = "���100_AlarmJournalForm";
            this.Load += new System.EventHandler(this.CS_AlarmJournalForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this._journalGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label _journalCntLabel;
        private System.Windows.Forms.Button _deserializeBut;
        private System.Windows.Forms.Button _serializeBut;
        private System.Windows.Forms.Button _readBut;
        private System.Windows.Forms.OpenFileDialog _openSysJounralDlg;
        private System.Windows.Forms.SaveFileDialog _saveSysJournalDlg;
        private System.Windows.Forms.DataGridView _journalGrid;
        private System.Windows.Forms.ProgressBar _journalProgress;
        private DataGridViewTextBoxColumn _numberCol;
        private DataGridViewTextBoxColumn _dateCol;
        private DataGridViewTextBoxColumn _mesColumn;
        private DataGridViewTextBoxColumn _valueCol;
        private DataGridViewTextBoxColumn _diskret1Col;
        private DataGridViewTextBoxColumn _diskret2Col;
        private DataGridViewTextBoxColumn _diskret3Col;
        private DataGridViewTextBoxColumn _diskret4Col;
        private DataGridViewTextBoxColumn _releCol;
        private DataGridViewTextBoxColumn _kisCol;
    }
}