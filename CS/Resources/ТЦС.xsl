<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- Edited by XMLSpy� -->
<xsl:stylesheet version="1.1"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <html>
      <head>
        <script type="text/javascript">
          function translateBoolean(value, elementId){
          var result = "";
          if(value.toString().toLowerCase() == "true")
          result = " + ";
          else if(value.toString().toLowerCase() == "false")
          result = " - ";
          else
          result = "������������ ��������"
          document.getElementById(elementId).innerHTML = result;
          }
        </script>
      </head>
      <body>
        <h3>���������� <xsl:value-of select="���100/���_����������"/> �<xsl:value-of select="���100/�����_����������"/> ������ <xsl:value-of select="���100/������_����������"/></h3>
        <b>��������</b>
        <table border="0">
          <tr bgcolor="#d1ced5" align="center">
            <td>������������ ���������</td>
            <td>����������</td>
            <td>����������</td>
          </tr>
          <tr>
            <td>
              <table border="1">
                <tr bgcolor="#c1ced5" align="center">
                  <td>�</td>
                  <td>���</td>
                  <td>��� ���������</td>
                  <td>��, �</td>
                  <td>��, �</td>
                </tr>
                <xsl:for-each select="���100/�����������_����_���������/Rows/ConDiscrets">
                  <tr>
                    <td><xsl:value-of select="position()"/></td>
                    <td><xsl:value-of select="���"/></td>
                    <td><xsl:value-of select="���_���"/></td>
                    <td><xsl:value-of select="��"/></td>
                    <td><xsl:value-of select="��"/></td>
                  </tr>
                </xsl:for-each>
              </table>
            </td>
            <td>
              <table border="1">
                <tr bgcolor="#c1ced5" align="center">
                  <td>1</td>
                  <td>2</td>
                  <td>3</td>
                  <td>4</td>
                  <td>5</td>
                  <td>6</td>
                  <td>��</td>
                </tr>
                <xsl:for-each select="���100/����������_�������/Rows/ConfigCommutation">
                  <tr align="center">
                    <xsl:element name="td">
                      <xsl:attribute name="id">kom1_<xsl:value-of select="position()"/></xsl:attribute>
                      <script>translateBoolean(<xsl:value-of select="����������_1"/>,"kom1_<xsl:value-of select="position()"/>");</script>
                    </xsl:element>
                    <xsl:element name="td">
                      <xsl:attribute name="id">kom2_<xsl:value-of select="position()"/></xsl:attribute>
                      <script>translateBoolean(<xsl:value-of select="����������_2"/>,"kom2_<xsl:value-of select="position()"/>");</script>
                    </xsl:element>
                    <xsl:element name="td">
                      <xsl:attribute name="id">kom3_<xsl:value-of select="position()"/></xsl:attribute>
                      <script>translateBoolean(<xsl:value-of select="����������_3"/>,"kom3_<xsl:value-of select="position()"/>");</script>
                    </xsl:element>
                    <xsl:element name="td">
                      <xsl:attribute name="id">kom4_<xsl:value-of select="position()"/></xsl:attribute>
                      <script>translateBoolean(<xsl:value-of select="����������_4"/>,"kom4_<xsl:value-of select="position()"/>");</script>
                    </xsl:element>
                    <xsl:element name="td">
                      <xsl:attribute name="id">kom5_<xsl:value-of select="position()"/></xsl:attribute>
                      <script>translateBoolean(<xsl:value-of select="����������_5"/>,"kom5_<xsl:value-of select="position()"/>");</script>
                    </xsl:element>
                    <xsl:element name="td">
                      <xsl:attribute name="id">kom6_<xsl:value-of select="position()"/></xsl:attribute>
                      <script>translateBoolean(<xsl:value-of select="����������_6"/>,"kom6_<xsl:value-of select="position()"/>");</script>
                    </xsl:element>
                    <xsl:element name="td">
                      <xsl:attribute name="id">kom7_<xsl:value-of select="position()"/></xsl:attribute>
                      <script>translateBoolean(<xsl:value-of select="����������_3C"/>,"kom7_<xsl:value-of select="position()"/>");</script>
                    </xsl:element>
                  </tr>
                </xsl:for-each>
              </table>
            </td>
            <td>
              <table border="1">
                <tr bgcolor="#c1ced5" align="center">
                  <td>1</td>
                  <td>2</td>
                  <td>3</td>
                  <td>4</td>
                  <td>5</td>
                  <td>6</td>
                  <td>��</td>
                </tr>
                <xsl:for-each select="���100/����������_�������/Rows/ConfigBlock">
                  <tr align="center">
                    <xsl:element name="td">
                      <xsl:attribute name="id">block1_<xsl:value-of select="position()"/></xsl:attribute>
                      <script>translateBoolean(<xsl:value-of select="����������_1"/>,"block1_<xsl:value-of select="position()"/>");</script>
                    </xsl:element>
                    <xsl:element name="td">
                      <xsl:attribute name="id">block2_<xsl:value-of select="position()"/></xsl:attribute>
                      <script>translateBoolean(<xsl:value-of select="����������_2"/>,"block2_<xsl:value-of select="position()"/>");</script>
                    </xsl:element>
                    <xsl:element name="td">
                      <xsl:attribute name="id">block3_<xsl:value-of select="position()"/></xsl:attribute>
                      <script>translateBoolean(<xsl:value-of select="����������_3"/>,"block3_<xsl:value-of select="position()"/>");</script>
                    </xsl:element>
                    <xsl:element name="td">
                      <xsl:attribute name="id">block4_<xsl:value-of select="position()"/></xsl:attribute>
                      <script>translateBoolean(<xsl:value-of select="����������_4"/>,"block4_<xsl:value-of select="position()"/>");</script>
                    </xsl:element>
                    <xsl:element name="td">
                      <xsl:attribute name="id">block5_<xsl:value-of select="position()"/></xsl:attribute>
                      <script>translateBoolean(<xsl:value-of select="����������_5"/>,"block5_<xsl:value-of select="position()"/>");</script>
                    </xsl:element>
                    <xsl:element name="td">
                      <xsl:attribute name="id">block6_<xsl:value-of select="position()"/></xsl:attribute>
                      <script>translateBoolean(<xsl:value-of select="����������_6"/>,"block6_<xsl:value-of select="position()"/>");</script>
                    </xsl:element>
                    <xsl:element name="td">
                      <xsl:attribute name="id">block7_<xsl:value-of select="position()"/></xsl:attribute>
                      <script>translateBoolean(<xsl:value-of select="����������_��"/>,"block7_<xsl:value-of select="position()"/>");</script>
                    </xsl:element>
                  </tr>
                </xsl:for-each>
              </table>
            </td>
          </tr>
        </table>
        <p></p>

        <b>����� ���</b>
        <table border="0">
          <tr bgcolor="#d1ced5" align="center">
            <td>����� ���</td>
            <td>����������</td>
            <td>����������</td>
          </tr>
          <tr>
            <td>
              <table border="1">
                <tr bgcolor="#c1ced5" align="center">
                  <td>�</td>
                  <td>���</td>
                  <td>��� ���������</td>
                  <td>��, �</td>
                  <td>��, �</td>
                </tr>
                <xsl:for-each select="���100/�����������_���/Rows/ConnectionConf">
                  <tr>
                    <td><xsl:value-of select="position()"/></td>
                    <td><xsl:value-of select="���"/></td>
                    <td><xsl:value-of select="��������"/></td>
                    <td><xsl:value-of select="�����_������������"/></td>
                    <td><xsl:value-of select="�����_��������"/></td>
                  </tr>
                </xsl:for-each>
              </table>
            </td>
            <td>
              <table border="1">
                <tr bgcolor="#c1ced5" align="center">
                  <td>1</td>
                  <td>2</td>
                  <td>3</td>
                  <td>4</td>
                  <td>5</td>
                  <td>6</td>
                  <td>��</td>
                </tr>
                <xsl:for-each select="���100/����������_���/Rows/ConfigCommutation">
                  <tr align="center">
                    <xsl:element name="td">
                      <xsl:attribute name="id">kis1_<xsl:value-of select="position()"/></xsl:attribute>
                      <script>translateBoolean(<xsl:value-of select="����������_1"/>,"kis1_<xsl:value-of select="position()"/>");</script>
                    </xsl:element>
                    <xsl:element name="td">
                      <xsl:attribute name="id">kis2_<xsl:value-of select="position()"/></xsl:attribute>
                      <script>translateBoolean(<xsl:value-of select="����������_2"/>,"kis2_<xsl:value-of select="position()"/>");</script>
                    </xsl:element>
                    <xsl:element name="td">
                      <xsl:attribute name="id">kis3_<xsl:value-of select="position()"/></xsl:attribute>
                      <script>translateBoolean(<xsl:value-of select="����������_3"/>,"kis3_<xsl:value-of select="position()"/>");</script>
                    </xsl:element>
                    <xsl:element name="td">
                      <xsl:attribute name="id">kis4_<xsl:value-of select="position()"/></xsl:attribute>
                      <script>translateBoolean(<xsl:value-of select="����������_4"/>,"kis4_<xsl:value-of select="position()"/>");</script>
                    </xsl:element>
                    <xsl:element name="td">
                      <xsl:attribute name="id">kis5_<xsl:value-of select="position()"/></xsl:attribute>
                      <script>translateBoolean(<xsl:value-of select="����������_5"/>,"kis5_<xsl:value-of select="position()"/>");</script>
                    </xsl:element>
                    <xsl:element name="td">
                      <xsl:attribute name="id">kis6_<xsl:value-of select="position()"/></xsl:attribute>
                      <script>translateBoolean(<xsl:value-of select="����������_6"/>,"kis6_<xsl:value-of select="position()"/>");</script>
                    </xsl:element>
                    <xsl:element name="td">
                      <xsl:attribute name="id">kis7_<xsl:value-of select="position()"/></xsl:attribute>
                      <script>translateBoolean(<xsl:value-of select="����������_3C"/>,"kis7_<xsl:value-of select="position()"/>");</script>
                    </xsl:element>
                  </tr>
                </xsl:for-each>
              </table>
            </td>
            <td>
              <table border="1">
                <tr bgcolor="#c1ced5" align="center">
                  <td>1</td>
                  <td>2</td>
                  <td>3</td>
                  <td>4</td>
                  <td>5</td>
                  <td>6</td>
                  <td>��</td>
                </tr>
                <xsl:for-each select="���100/����������_���/Rows/ConfigBlock">
                  <tr align="center">
                    <xsl:element name="td">
                      <xsl:attribute name="id">bkis1_<xsl:value-of select="position()"/></xsl:attribute>
                      <script>translateBoolean(<xsl:value-of select="����������_1"/>,"bkis1_<xsl:value-of select="position()"/>");</script>
                    </xsl:element>
                    <xsl:element name="td">
                      <xsl:attribute name="id">bkis2_<xsl:value-of select="position()"/></xsl:attribute>
                      <script>translateBoolean(<xsl:value-of select="����������_2"/>,"bkis2_<xsl:value-of select="position()"/>");</script>
                    </xsl:element>
                    <xsl:element name="td">
                      <xsl:attribute name="id">bkis3_<xsl:value-of select="position()"/></xsl:attribute>
                      <script>translateBoolean(<xsl:value-of select="����������_3"/>,"bkis3_<xsl:value-of select="position()"/>");</script>
                    </xsl:element>
                    <xsl:element name="td">
                      <xsl:attribute name="id">bkis4_<xsl:value-of select="position()"/></xsl:attribute>
                      <script>translateBoolean(<xsl:value-of select="����������_4"/>,"bkis4_<xsl:value-of select="position()"/>");</script>
                    </xsl:element>
                    <xsl:element name="td">
                      <xsl:attribute name="id">bkis5_<xsl:value-of select="position()"/></xsl:attribute>
                      <script>translateBoolean(<xsl:value-of select="����������_5"/>,"bkis5_<xsl:value-of select="position()"/>");</script>
                    </xsl:element>
                    <xsl:element name="td">
                      <xsl:attribute name="id">bkis6_<xsl:value-of select="position()"/></xsl:attribute>
                      <script>translateBoolean(<xsl:value-of select="����������_6"/>,"bkis6_<xsl:value-of select="position()"/>");</script>
                    </xsl:element>
                    <xsl:element name="td">
                      <xsl:attribute name="id">bkis7_<xsl:value-of select="position()"/></xsl:attribute>
                      <script>translateBoolean(<xsl:value-of select="����������_��"/>,"bkis7_<xsl:value-of select="position()"/>");</script>
                    </xsl:element>
                  </tr>
                </xsl:for-each>
              </table>
            </td>
          </tr>
        </table>
        <p></p>

        <b>����</b>
        <table border="1"  >
          <tr bgcolor="#CCFFFF" align="center">
            <td >�</td>
            <td >���</td>
            <td >����., �</td>
          </tr>
          <xsl:for-each select="���100/�����������_����_����/Rows/ConfigRelay">
            <tr>
              <td><xsl:value-of select="position()"/></td>
              <td><xsl:value-of select="���_�����������_������"/></td>
              <td><xsl:value-of select="������������_��������"/></td>
            </tr>
          </xsl:for-each>
        </table>
        <p></p>
        
        <b>��������� �����</b>
        <br />
        <xsl:value-of select="���100/���������_�������/����"/><xsl:value-of select="���100/���������_�������/���������_�������"/> ������ � �����
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
