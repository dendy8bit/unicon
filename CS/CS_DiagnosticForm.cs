using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using AssemblyResourses;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms;
using BEMN.Interfaces;

namespace BEMN.CS
{
    public partial class CS_DiagnosticForm : Form, IFormView
    {
        private CS _device;

        public CS_DiagnosticForm()
        {
            this.InitializeComponent();
        }


        private LedControl[][] _diskretInputs;
        private LedControl[] _diskretOutputs;

        public CS_DiagnosticForm(CS device)
        {
            this.InitializeComponent();
            this._device = device;
            this._device.ConnectionModeChanged += this.StartStopLoad;
            this._device.DiagnosticLoadOk += HandlerHelper.CreateHandler(this, this.OnDiagnosticLoadOk);
            this._device.DiagnosticLoadFail += HandlerHelper.CreateHandler(this, this.OnDiagnosticLoadFail);
            this._device.CvitCommandSaveOk += HandlerHelper.CreateHandler(this, this.OnCvitSaveOk);
            this._device.TimeLoadOk += HandlerHelper.CreateHandler(this, this.ReadDateTime);
            this.Init();
        }

        private void Init()
        {
            this._diskretInputs = new[]
            {
                new[]
                {
                    this._led1_1, this._led1_2, this._led1_3, this._led1_4, this._led1_5, this._led1_6, this._led1_7,
                    this._led1_8, this._led1_9, this._led1_10
                },
                new[]
                {
                    this._led2_1, this._led2_2, this._led2_3, this._led2_4, this._led2_5, this._led2_6, this._led2_7,
                    this._led2_8, this._led2_9, this._led2_10
                },
                new[]
                {
                    this._led3_1, this._led3_2, this._led3_3, this._led3_4, this._led3_5, this._led3_6, this._led3_7,
                    this._led3_8, this._led3_9, this._led3_10
                },
                new[]
                {
                    this._led4_1, this._led4_2, this._led4_3, this._led4_4, this._led4_5, this._led4_6, this._led4_7,
                    this._led4_8, this._led4_9, this._led4_10
                }
            };
            this._diskretOutputs =
                new[]
                {
                    this._outLed1, this._outLed2, this._outLed3, this._outLed4, this._outLed5, this._outLed6, this._outLed7,
                    this._outLed8
                };
        }

        public void OnDiagnosticLoadFail()
        {
            for (int i = 0; i < CS.DISKRETINPUTS_CNT; i++)
            {
                LedManager.TurnOffLeds(this._diskretInputs[i]);
            }
            this._kisInput.Text = "000000000000";
            LedManager.TurnOffLeds(this._diskretOutputs);
        }

        public void OnCvitSaveOk()
        {
            MessageBox.Show("������������ ���������.");
        }

        public void OnDiagnosticLoadOk()
        {
            for (int i = 0; i < CS.DISKRETINPUTS_CNT; i++)
            {
                BitArray bits = this._device.GetDiskretInputs(i);
                LedManager.SetLeds(this._diskretInputs[i], bits);
            }
            byte[] kisInputs = this._device.GetKISInputs();
            string kis = string.Empty;
            kis = kisInputs[0].ToString();
            for (int i = 1; i < kisInputs.Length; i++)
            {
                kis += "-" + kisInputs[i].ToString();
            }
            this._kisInput.Text = kis;
            BitArray diskretOutputs = this._device.GetDiskretOutputs();
            LedManager.SetLedsReverse(this._diskretOutputs, diskretOutputs);
        }

        private void CS_DiagnosticForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._device.StopReadingTime();
            this._device.RemoveDiagnostic();
            this._device.ConnectionModeChanged -= this.StartStopLoad;
        }

        private void CS_DiagnosticForm_Deactivate(object sender, EventArgs e)
        {
            this._device.MakeDiagnosticSlow();
        }

        private void CS_DiagnosticForm_Activated(object sender, EventArgs e)
        {
            this._device.MakeDiagnosticQuick();
        }

        private void CS_DiagnosticForm_Load(object sender, EventArgs e)
        {
            this.StartStopLoad();
        }

        private void StartStopLoad()
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._device.LoadTimeCycle();
                this._device.LoadDiagnosticCycle();
                this._dateTimeNowButt.Enabled = false;
                this._writeDateTimeButt.Enabled = false;
            }
            else
            {
                this._device.StopReadingTime();
                this._device.RemoveDiagnostic();
                this.OnDiagnosticLoadFail();
            }
        }

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof (CS); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof (CS_DiagnosticForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.diag; }
        }

        public string NodeName
        {
            get { return "���������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] {}; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        private void _dateTimeNowButt_Click(object sender, EventArgs e)
        {
            this._dateClockTB.TextMaskFormat = MaskFormat.IncludeLiterals;
            this._dateClockTB.Text = DateTime.Now.Day.ToString("00") +
                                     DateTime.Now.Month.ToString("00") +
                                     DateTime.Now.Year.ToString("0000").Substring(2);
            this._timeClockTB.TextMaskFormat = MaskFormat.IncludeLiterals;
            this._timeClockTB.Text = DateTime.Now.Hour.ToString("00") +
                                     DateTime.Now.Minute.ToString("00") +
                                     DateTime.Now.Second.ToString("00") +
                                     DateTime.Now.Millisecond.ToString("000");
            this.WriteTime();
        }

        private void WriteTime()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            List<string> dateTime = new List<string>();
            dateTime.AddRange(this._dateClockTB.Text.Split('.'));
            dateTime.AddRange(this._timeClockTB.Text.Split(':', ','));
            this._device.TIME_DATE = Convert.ToUInt16(dateTime[0]);
            this._device.TIME_MONTH = Convert.ToUInt16(dateTime[1]);
            this._device.TIME_YEAR = Convert.ToUInt16(dateTime[2]);
            this._device.TIME_HOUR = Convert.ToUInt16(dateTime[3]);
            this._device.TIME_MINUTES = Convert.ToUInt16(dateTime[4]);
            this._device.TIME_SECONDS = Convert.ToUInt16(dateTime[5]);
            this._device.TIME_MSECONDS = Convert.ToUInt16(dateTime[6]);
            this._device.WriteTime();
            this._stopCB.Checked = true;
        }

        private void _writeDateTimeButt_Click(object sender, EventArgs e)
        {
            try
            {
                this.WriteTime();
            }
            catch
            {
                MessageBox.Show(@"���������� �������� ����/�����", "��������!", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
            }
        }

        private void _stopCB_CheckedChanged(object sender, EventArgs e)
        {
            if (this._stopCB.Checked)
            {
                this._device.StopReadingTime();
                this._dateTimeNowButt.Enabled = true;
                this._writeDateTimeButt.Enabled = true;
            }
            else
            {
                this._device.LoadTimeCycle();
                this._dateTimeNowButt.Enabled = false;
                this._writeDateTimeButt.Enabled = false;
            }
        }

        private void ReadDateTime()
        {
            this._dateClockTB.TextMaskFormat = MaskFormat.IncludeLiterals;
            this._dateClockTB.Text = this._device.TIME_DATE.ToString("00") + this._device.TIME_MONTH.ToString("00") +
                                     this._device.TIME_YEAR.ToString("0000").Substring(2);
            this._timeClockTB.TextMaskFormat = MaskFormat.IncludeLiterals;
            this._timeClockTB.Text = this._device.TIME_HOUR.ToString("00") + this._device.TIME_MINUTES.ToString("00") +
                                     this._device.TIME_SECONDS.ToString("00") +
                                     this._device.TIME_MSECONDS.ToString("000");
        }

        private void _cvitButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._device.SendCvitCommand(CS.LogicCommand.CVIT);
        }
    }
}