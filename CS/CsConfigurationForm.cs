using System;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using AssemblyResourses;
using BEMN.CS.Structures;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms.Export;
using BEMN.Forms.ValidatingClasses;
using BEMN.Forms.ValidatingClasses.New.ColumnsInfos;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Forms.ValidatingClasses.Rules.Double;
using BEMN.Forms.ValidatingClasses.Rules.Ushort;
using BEMN.Interfaces;
using BEMN.MBServer;

namespace BEMN.CS
{
    public partial class CsConfigurationForm : Form, IFormView
    {
        private string TCS_SETPOINTS_NAME = "���_�������";

        private CS _device;

        private readonly MemoryEntity<ConfigurationStruct> _configuration;
        private readonly MemoryEntity<ConfigurationStructNew> _configurationNew;
        private MemoryEntity<OneWordStruct> _command;
        private ushort[] _netCongfig;
        private NewDgwValidatior<AllConDiscrets, ConDiscrets> _configDiskretsValidator;
        private NewDgwValidatior<AllConnectionConf, ConnectionConf> _connectionConf;
        private NewDgwValidatior<AllRelays, ConfigRelay> _releValidator;
        private NewDgwValidatior<AllCommutationDiscret, ConfigCommutation> _commutationDiscretValidator;
        private NewDgwValidatior<AllCommutationKis, ConfigCommutation> _commutationKisValidator;
        private NewDgwValidatior<AllBlockDiscret, ConfigBlock> _discretBlockValidator;
        private NewDgwValidatior<AllBlockKis, ConfigBlock> _kisBlockValidator;
        private NewStructValidator<CorrectionValidationStruct> _timeValidator;
        private StructUnion<ConfigurationStruct> _configurationUnion;
        private StructUnion<ConfigurationStructNew> _configurationNewUnion;

        private CS.LogicCommand _currentCommand;

        public CsConfigurationForm()
        {
            this.InitializeComponent();
        }

        #region ���������
        
        public CsConfigurationForm(CS device)
        {
            this._device = device;
            this.InitializeComponent();
            this.Init();

            this._command = this._device.Command;
            this._command.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.CommandReadOk);
            this._command.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.CommandReadFail);
            this._command.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.CommandWriteOk);
            this._command.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.CommandWriteFail);
            this._command.WriteOk += HandlerHelper.CreateHandler(this, this._exchangeProgressBar.PerformStep);
            this._command.ReadOk += HandlerHelper.CreateHandler(this, this._exchangeProgressBar.PerformStep);

            if (this._device.Version.Value.Version1 >= 8 && this._device.Version.Value.Version2 >= 1)
            {
                this._configurationNew = this._device.ConfigurationNew;
                this._configurationNew.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationReadOk);
                this._configurationNew.AllReadFail += HandlerHelper.CreateReadArrayHandler(this,this.ConfigurationReadFail);
                this._configurationNew.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this,this.ConfigurationWriteOk);
                this._configurationNew.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this,this.ConfigurationWriteFail);
                this._configurationNew.WriteOk += HandlerHelper.CreateHandler(this,this._exchangeProgressBar.PerformStep);
                this._configurationNew.ReadOk += HandlerHelper.CreateHandler(this, this._exchangeProgressBar.PerformStep);
            }
            else
            {
                this._configuration = this._device.Configuration;
                this._configuration.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationReadOk);
                this._configuration.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationReadFail);
                this._configuration.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationWriteOk);
                this._configuration.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this,this.ConfigurationWriteFail);
                this._configuration.WriteOk += HandlerHelper.CreateHandler(this, this._exchangeProgressBar.PerformStep);
                this._configuration.ReadOk += HandlerHelper.CreateHandler(this, this._exchangeProgressBar.PerformStep);
            }

        }

        public void Init()
        {
            Strings.Version_6_84 = this._device.Version.Value.Is6_84AndMore;
            Strings.Version_8_1 = this._device.Version.Value.Is8_1AndMore;
            ToolTip toolTip = new ToolTip();
            Func<IValidatingRule> timeFunc = () =>
            {
                try
                {
                    if (this._device.Version.Value.Is6_84AndMore)
                    {
                        return new CustomDoubleRule(0, 86.4);
                    }
                    return new CustomDoubleRule(0, 86.0);
                }
                catch (Exception)
                {
                    return new CustomDoubleRule(0, 86.0);
                }
            };

            this._configDiskretsValidator = new NewDgwValidatior<AllConDiscrets, ConDiscrets>
               (this._dataGridDis�retView,
                   AllConDiscrets.CONFIG_DISCRETS_COUNT,
                   toolTip,
                   new ColumnInfoCombo(Strings.DiscretNumbers, ColumnsType.NAME),
                   new ColumnInfoCombo(Strings.DiskretIn),
                   new ColumnInfoCombo(Strings.DiskretIndication),
                   new ColumnInfoText(new CustomDoubleRule(0, 60.0)),
                   new ColumnInfoText(new CustomDoubleRule(0, 60.0))
               );

            this._releValidator = new NewDgwValidatior<AllRelays, ConfigRelay>
                (
                    this._dataGridReleView,
                    AllRelays.CONFIG_RELAYS_COUNT,
                    toolTip,
                    new ColumnInfoCombo(Strings.RelayNames, ColumnsType.NAME),
                    new ColumnInfoCombo(Strings.ReleType),
                    new ColumnInfoText(new CustomDoubleRule(0, 60.0))

                );

            this._connectionConf = new NewDgwValidatior<AllConnectionConf, ConnectionConf>
                (
                    this._dataGridKISView,
                    AllConnectionConf.CONNECTIONS_KIS_COUNT,
                    toolTip,
                    new ColumnInfoCombo(Strings.KisNames, ColumnsType.NAME),
                    new ColumnInfoCombo(Strings.KisType),
                    new ColumnInfoCombo(Strings.KisKontrol),
                    new ColumnInfoText(new CustomUshortRule(0, 60)),
                    new ColumnInfoText(new CustomUshortRule(0, 60))
                );

            this._commutationDiscretValidator = new NewDgwValidatior<AllCommutationDiscret, ConfigCommutation>
                (
                this._commutationGrid,
                AllCommutationDiscret.COUNT,
                toolTip,
                new ColumnInfoCombo(Strings.DiscretNumbers, ColumnsType.NAME),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(), 
                new ColumnInfoCheck(), 
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(), 
                new ColumnInfoCheck()
                );

            this._commutationKisValidator = new NewDgwValidatior<AllCommutationKis, ConfigCommutation>
                (
                this._commutationKisGrid,
                AllCommutationKis.COUNT,
                toolTip,
                new ColumnInfoCombo(Strings.KisNames, ColumnsType.NAME),
                new ColumnInfoCheck(), new ColumnInfoCheck(),
                new ColumnInfoCheck(), new ColumnInfoCheck(),
                new ColumnInfoCheck(), new ColumnInfoCheck(),
                new ColumnInfoCheck()
                );

            this._discretBlockValidator = new NewDgwValidatior<AllBlockDiscret, ConfigBlock>
                (
                this._blockDiscretGrid,
                AllBlockDiscret.COUNT,
                toolTip,
                new ColumnInfoCombo(Strings.DiscretNumbers, ColumnsType.NAME),
                new ColumnInfoCheck(), new ColumnInfoCheck(),
                new ColumnInfoCheck(), new ColumnInfoCheck(),
                new ColumnInfoCheck(), new ColumnInfoCheck(),
                new ColumnInfoCheck()
                );

            this._kisBlockValidator = new NewDgwValidatior<AllBlockKis, ConfigBlock>
                (
                this._blockKisGrid,
                AllBlockKis.COUNT,
                toolTip,
                new ColumnInfoCombo(Strings.KisNames, ColumnsType.NAME),
                new ColumnInfoCheck(), new ColumnInfoCheck(),
                new ColumnInfoCheck(), new ColumnInfoCheck(),
                new ColumnInfoCheck(), new ColumnInfoCheck(),
                new ColumnInfoCheck()
                );

            NewStructValidator<NetConfigStruct> netValidator = new NewStructValidator<NetConfigStruct>(
                toolTip, 
                new ControlInfoText(this._pass, new CustomUshortRule(0, 9999)),
                new ControlInfoCombo(this._speed, Strings.Speed),
                new ControlInfoText(this._addr, new CustomUshortRule(1, 255)));

            this._timeValidator = new NewStructValidator<CorrectionValidationStruct>
                (
                toolTip,
                new ControlInfoTextDependent(this._timeValidation, timeFunc),
                new ControlInfoCombo(this._timeValidationSign, Strings.Sign)
                );

            if (this._device.Version.Value.Version1 >= 8 && this._device.Version.Value.Version2 >= 1)
            {
                this._tabControl.TabPages.Remove(this._conPage);
                this._configurationNewUnion = new StructUnion<ConfigurationStructNew>
                (
                this._configDiskretsValidator,
                this._releValidator,
                this._connectionConf,
                this._commutationDiscretValidator,
                this._commutationKisValidator,
                this._discretBlockValidator,
                this._kisBlockValidator,
                netValidator,
                this._timeValidator
                );
            }
            else
            {
                this._configurationUnion = new StructUnion<ConfigurationStruct>
                (
                this._configDiskretsValidator,
                this._releValidator,
                this._connectionConf,
                this._commutationDiscretValidator,
                this._commutationKisValidator,
                this._discretBlockValidator,
                this._kisBlockValidator,
                netValidator,
                this._timeValidator
                );
            }
        }

        private void CS_ConfigurationForm_Load(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._processLabel.Text = string.Empty;
            if (Device.AutoloadConfig)
            {
                this.ReadConfig();
            }
        }
        #endregion

        private void ConfigurationReadOk()
        {
            CorrectionValidationStruct.MAX_VALUE_TIME = this._device.Version.Value.Is6_84AndMore ? 86400 : 86000;
            if (this._device.Version.Value.Version1 >= 8 && this._device.Version.Value.Version2 >= 1)
            {
                this._configurationNewUnion.Set(this._configurationNew.Value);
            }
            else
            {
                this._configurationUnion.Set(this._configuration.Value);
            }
            
            this._statusStrip.Text = "������������ ��������� �������";
            MessageBox.Show("������������ ��������� �������", "������ ������������", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void ConfigurationReadFail()
        {
            this._processLabel.Text = "���������� ��������� ������������";
            MessageBox.Show("���������� ��������� ������������", "���100. ������", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void ConfigurationWriteOk()
        {
            if (this._device.Version.Value.Is6_84AndMore)
            {
                this._currentCommand = CS.LogicCommand.END;
                this._command.Value.Word = (ushort) this._currentCommand;
                this._command.SaveStruct();
            }
            else
            {
                this._command.LoadStruct();
            }
        }

        private void ConfigurationWriteFail()
        {
            this._processLabel.Text = "���������� �������� ������������";
            MessageBox.Show("���������� �������� ������������", "���100. ������", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void CommandWriteOk()
        {
            if (this._currentCommand == CS.LogicCommand.STOP)
            {
                if (this._device.Version.Value.Version1 >= 8 && this._device.Version.Value.Version2 >= 1)
                {
                    this._configurationNew.SaveStruct();
                }
                else
                {
                    this._configuration.SaveStruct();
                }

            }
            else if (this._currentCommand == CS.LogicCommand.END)
            {
                Thread.Sleep(100);
                this._command.LoadStruct();
            }
        }

        private void CommandWriteFail()
        {
            this._processLabel.Text = "���������� �������� ������������";
            MessageBox.Show("���������� ������ ���������� ������� ��� ����������", "���100. ������", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void CommandReadOk()
        {
            if (this._device.Command .Value.Word == (ushort)CS.LogicCommand.START)
            {
                this._processLabel.Text = "������ ������������ ������� ���������";
                MessageBox.Show("������ ������������ ������� ���������", "������ ������������", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("���������� �������� ������������", "���100. ������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void CommandReadFail()
        {
            this._processLabel.Text = "���������� ��������� ���������� �������";
            MessageBox.Show("���������� ������ ���������� ������� ��������� ������ ���100.", "���100. ������", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void _readConfigBut_Click(object sender, EventArgs e)
        {
            this.ReadConfig();
        }

        private void ReadConfig()
        {
            this._exchangeProgressBar.Value = 0;
            this._processLabel.Text = "���� ������";
            if (this._device.Version.Value.Version1 >= 8 && this._device.Version.Value.Version2 >= 1)
            {
                this._exchangeProgressBar.Maximum = this._configurationNew.Slots.Count;
                this._configurationNew.LoadStruct();
            }
            else
            {
                this._exchangeProgressBar.Maximum = this._configuration.Slots.Count;
                this._configuration.LoadStruct();
            }
        }

        private void _writeConfigBut_Click(object sender, EventArgs e)
        {
            this.StartWriteConfig();
        }

        private void StartWriteConfig()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;

            if (DialogResult.Yes != MessageBox.Show("�������� ������������ ��� 100?", "��������", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                return;

            string mes;
            if (this._device.Version.Value.Version1 >= 8 && this._device.Version.Value.Version2 >= 1)
            {
                if (this._configurationNewUnion.Check(out mes, true))
                {
                    this._exchangeProgressBar.Maximum = this._configurationNew.Slots.Count + this._command.Slots.Count*2;
                    ConfigurationStructNew config = this._configurationNewUnion.Get();
                    ushort[] crcBuffer = config.GetValues();
                    config.CRC = Common.SwapByte(CRC16.CalcCrcFast(Common.TOBYTES(crcBuffer, false), 0x5F0 - 2));
                    this._configurationNew.Value = config;
                }
                else
                {
                    MessageBox.Show("���������� �������� ������������. ���������� ������������ �������.", "��������",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }
            else
            {
                if (this._configurationUnion.Check(out mes, true))
                {
                    this._exchangeProgressBar.Maximum = this._configuration.Slots.Count + this._command.Slots.Count*2;
                    ConfigurationStruct config = this._configurationUnion.Get();
                    ushort[] crcBuffer = config.GetValues();
                    config.CRC = Common.SwapByte(CRC16.CalcCrcFast(Common.TOBYTES(crcBuffer, false), 0x600 - 2));
                    this._configuration.Value = config;
                }
                else
                {
                    MessageBox.Show("���������� �������� ������������. ���������� ������������ �������.", "��������",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }
            this._exchangeProgressBar.Value = 0;
            this._processLabel.Text = "���� ������";
            CorrectionValidationStruct.MAX_VALUE_TIME = this._device.Version.Value.Is6_84AndMore
                ? 86400
                : 86000;
            this._currentCommand = CS.LogicCommand.STOP;
            this._command.Value.Word = (ushort)this._currentCommand;
            this._command.SaveStruct();
        }


        private void _saveConfigBut_Click(object sender, EventArgs e)
        {
            this.SaveInFile();
        }

        private void SaveInFile()
        {
            this._saveConfigurationDlg.FileName =
                    string.Format("���100_�������_������_{0}.xml", this._device.DeviceVersion);
            if (DialogResult.OK == this._saveConfigurationDlg.ShowDialog())
            {
                this._exchangeProgressBar.Value = 0;
                XmlDocument doc = new XmlDocument();
                doc.AppendChild(doc.CreateElement(this.TCS_SETPOINTS_NAME));
                CorrectionValidationStruct.MAX_VALUE_TIME = this._device.Version.Value.Is6_84AndMore ? 86400 : 86000;

                ushort[] values;
                if (this._device.Version.Value.Version1 >= 8 && this._device.Version.Value.Version2 >= 1)
                {
                    ConfigurationStructNew config = this._configurationNewUnion.Get();
                    ushort[] crcBuffer = config.GetValues();
                    config.CRC = Common.SwapByte(CRC16.CalcCrcFast(Common.TOBYTES(crcBuffer, false), 0x5F0 - 2));
                    values = config.GetValues();
                }
                else
                {
                    ConfigurationStruct config = this._configurationUnion.Get();
                    ushort[] crcBuffer = config.GetValues();
                    config.CRC = Common.SwapByte(CRC16.CalcCrcFast(Common.TOBYTES(crcBuffer, false), 0x600 - 2));
                    values = config.GetValues();
                }

                XmlElement element = doc.CreateElement(this.TCS_SETPOINTS_NAME);
                element.InnerText = Convert.ToBase64String(Common.TOBYTES(values, false));
                if (doc.DocumentElement == null)
                {
                    throw new NullReferenceException();
                }
                doc.DocumentElement.AppendChild(element);
                doc.Save(this._saveConfigurationDlg.FileName);
                this._processLabel.Text = "���� " + this._openConfigurationDlg.FileName + " ��������";
            } 
        }

        private void _loadConfigBut_Click(object sender, EventArgs e)
        {
            this.ReadFromFile();
        }

        private void ReadFromFile()
        {
            if (DialogResult.OK == this._openConfigurationDlg.ShowDialog())
            {
                this._exchangeProgressBar.Value = 0;
                try
                {
                    byte[] values;
                    string extension = Path.GetExtension(this._openConfigurationDlg.FileName).ToLower();
                    if (extension == ".xml")
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.Load(this._openConfigurationDlg.FileName);

                        XmlNode a = doc.FirstChild.SelectSingleNode(this.TCS_SETPOINTS_NAME);
                        if (a == null)
                            throw new NullReferenceException();

                        values = Convert.FromBase64String(a.InnerText);
                    }
                    else
                    {
                        values = this._device.Deserialize(this._openConfigurationDlg.FileName);
                    }
                    CorrectionValidationStruct.MAX_VALUE_TIME = this._device.Version.Value.Is6_84AndMore
                            ? 86400
                            : 86000;
                    if (this._device.Version.Value.Version1 >= 8 && this._device.Version.Value.Version2 >= 1)
                    {
                        ConfigurationStructNew config = new ConfigurationStructNew();
                        config.InitStruct(values);
                        this._configurationNewUnion.Set(config);
                    }
                    else
                    {
                        ConfigurationStruct config = new ConfigurationStruct();
                        config.InitStruct(values);
                        this._configurationUnion.Set(config);
                    }
                    this._processLabel.Text = "���� " + this._openConfigurationDlg.FileName + " ��������";
                   
                }
                catch (Exception)
                {
                    this._processLabel.Text = "���� " + Path.GetFileName(this._openConfigurationDlg.FileName) + " �� �������� ������ ������� ��� 100 ��� ���������";
                }
            } 
        }
       
        private void _saveToXmlButton_Click(object sender, EventArgs e)
        {
            this.SaveToHtmlFile();
        }

        private void SaveToHtmlFile()
        {
            try
            {
                string message;
                if (this._device.Version.Value.Version1 >= 8 && this._device.Version.Value.Version2 >= 1)
                {
                    if (this._configurationNewUnion.Check(out message, true))
                    {
                        ConfigurationStructNew config = this._configurationNewUnion.Get();
                        config.DeviceVersion = this._device.Version.DeviceVersion;
                        config.DeviceNumber = this._device.DeviceNumber.ToString();
                        this._processLabel.Text = HtmlExport.ExportHtml(Resources.���, config, "���", this._device.Version.DeviceVersion);
                    }
                    else
                    {
                        MessageBox.Show("���������� ���������� ������������ � HTML. ���������� ������������ �������.",
                            "��������", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                else
                {
                    if (this._configurationUnion.Check(out message, true))
                    {
                        ConfigurationStruct config = this._configurationUnion.Get();
                        config.DeviceVersion = this._device.Version.DeviceVersion;
                        config.DeviceNumber = this._device.DeviceNumber.ToString();
                        this._processLabel.Text = HtmlExport.ExportHtml(Resources.���, config, "���", this._device.Version.DeviceVersion);
                    }
                    else
                    {
                        MessageBox.Show("���������� ���������� ������������ � HTML. ���������� ������������ �������.",
                            "��������", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }

            }
            catch (Exception)
            {
                MessageBox.Show("������ ����������");
            }
        }

        private void contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ((ContextMenuStrip)sender).Close();
            if (e.ClickedItem == this.readFromDeviceItem)
            {
                this.ReadConfig();
                return;
            }
            if (e.ClickedItem == this.writeToDeviceItem)
            {
                this.StartWriteConfig();
                return;
            }
            if (e.ClickedItem == this.readFromFileItem)
            {
                this.ReadFromFile();
                return;
            }
            if (e.ClickedItem == this.writeToFileItem)
            {
                this.SaveInFile();
                return;
            }
            if (e.ClickedItem == this.writeToHtmlItem)
            {
                this.SaveToHtmlFile();
            }
        }

        private void CSConfigurationForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers != Keys.Control) return;
            switch (e.KeyCode)
            {
                case Keys.W:
                    this.StartWriteConfig();
                    break;
                case Keys.R:
                    this.ReadConfig();
                    break;
                case Keys.S:
                    this.SaveInFile();
                    break;
                case Keys.O:
                    this.ReadFromFile();
                    break;
            }
            e.Handled = true;
        }

        private void _blockGrid_Scroll(object sender, ScrollEventArgs e)
        {
            this._commutationGrid.FirstDisplayedScrollingRowIndex = this._blockDiscretGrid.FirstDisplayedScrollingRowIndex;
            this._dataGridDis�retView.FirstDisplayedScrollingRowIndex = this._blockDiscretGrid.FirstDisplayedScrollingRowIndex;
        }

        private void CsConfigurationForm_Activated(object sender, EventArgs e)
        {
            Strings.Version_6_84 = this._device.Version.Value.Is6_84AndMore;
            Strings.Version_8_1 = this._device.Version.Value.Is8_1AndMore;
        }


        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(CS); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(CsConfigurationForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get
            {
                return Resources.config.ToBitmap();
            }
        }

        public string NodeName
        {
            get { return "������������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion
    }
}