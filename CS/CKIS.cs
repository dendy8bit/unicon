using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using BEMN.MBServer;

namespace BEMN.CS
{
    public class CKIS : ICollection
    {
        public const int COUNT = 6;
        public const int CONFIG_LENGTH = 0x20;
        public const int CONFIG_OFFSET = 0;
        public const int COMMUTATION_LENGTH = 6;
        public const int BLOCKING_LENGTH = 6;
        public const int COMMUTATION_OFFSET = 40;
        public const int BLOCKING_OFFSET = 40;

        private List<KISItem> _itemsList = new List<KISItem>(COUNT);

        public CKIS()
        {
            SetConfigBuffer(new ushort[CONFIG_LENGTH]);
        }

        private ushort[] _commutation = new ushort[COMMUTATION_LENGTH];
        private ushort[] _blocking = new ushort[BLOCKING_LENGTH];

        public void Add(KISItem item)
        {
            _itemsList.Add(item);
        }

        public void SetCommunication(BitArray communication, int index)
        {
            _commutation[index] = Common.BitsToUshort(communication);
        }

        public void SetBlocking(BitArray blocking, int index)
        {
            _blocking[index] = Common.BitsToUshort(blocking);
        }

        public BitArray GetCommutation(int index)
        {
            return GetLogic(index, _commutation);
        }

        private static BitArray GetLogic(int index, ushort[] buffer)
        {
            if (index >= COUNT)
            {
                throw new ArgumentOutOfRangeException("index", index,
                                                      "LogicIndex of KIS must less than " + COUNT);
            }

            return new BitArray(new byte[] { Common.LOBYTE(buffer[index]) });
        }

        public BitArray GetBlocking(int index)
        {
            return GetLogic(index, _blocking);
        }

        public void SetCommutationBuffer(ushort[] buffer)
        {
            if (COMMUTATION_LENGTH != buffer.Length)
            {
                throw new ArgumentOutOfRangeException("buffer.Length", buffer.Length,
                                                      "CommutationBuffer of KIS must be " + COMMUTATION_LENGTH);
            }
            _commutation = buffer;
        }

        public void SetBlockingBuffer(ushort[] buffer)
        {
            if (BLOCKING_LENGTH != buffer.Length)
            {
                throw new ArgumentOutOfRangeException("buffer.Length", buffer.Length,
                                                      "BlockingBuffer of KIS must be " + BLOCKING_LENGTH);
            }
            _blocking = buffer;
        }

        public void SetConfigBuffer(ushort[] buffer)
        {
            if (CONFIG_LENGTH != buffer.Length)
            {
                throw new ArgumentOutOfRangeException("buffer.Length", buffer.Length,
                                                      "Buffer of Diskrets must be " + CONFIG_LENGTH);
            }
            _itemsList.Clear();
            for (int i = 0; i < COUNT; i++)
            {
                ushort[] itemBuffer = new ushort[KISItem.LENGTH];
                Array.ConstrainedCopy(buffer, i * KISItem.LENGTH, itemBuffer, 0, KISItem.LENGTH);
                _itemsList.Add(new KISItem(itemBuffer));
                _itemsList[i].Name = "��� �" + i;
            }
        }

        public ushort[] ConfigToUshort()
        {
            ushort[] ret = new ushort[CONFIG_LENGTH];
            for (int i = 0; i < COUNT; i++)
            {
                Array.ConstrainedCopy(this[i].Values, 0, ret, i * KISItem.LENGTH, KISItem.LENGTH);
            }
            return ret;
        }

        public ushort[] BlockingToUshort()
        {
            return _blocking;
        }

        public ushort[] CommutationToUshort()
        {
            return _commutation;
        }

        public KISItem this[int i]
        {
            get { return _itemsList[i]; }
            set { _itemsList[i] = value; }
        }

        #region ICollection Members

        public void CopyTo(Array array, int index)
        {
        }

        [DisplayName("����������")]
        public int Count
        {
            get { return _itemsList.Count; }
        }

        [Browsable(false)]
        public bool IsSynchronized
        {
            get { return false; }
        }

        [Browsable(false)]
        public object SyncRoot
        {
            get { return _itemsList; }
        }

        #endregion

        #region IEnumerable Members

        public IEnumerator GetEnumerator()
        {
            return _itemsList.GetEnumerator();
        }

        #endregion
    }
}