using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace BEMN.CS
{
    public class CRele : ICollection
    {
        public const int LENGTH = 0x20;
        public const int COUNT = 7;


        private List<ReleItem> _itemsList = new List<ReleItem>(COUNT);

        public CRele()
        {
            SetBuffer(new ushort[LENGTH]);
        }

        public void SetBuffer(ushort[] buffer)
        {
            if (LENGTH != buffer.Length)
            {
                throw new ArgumentOutOfRangeException("buffer.Length", buffer.Length,
                                                      "Buffer of Rele must be " + LENGTH);
            }
            _itemsList.Clear();
            for (int i = 0; i < COUNT; i++)
            {
                ushort[] itemBuffer = new ushort[ReleItem.LENGTH];
                Array.ConstrainedCopy(buffer, i * ReleItem.LENGTH, itemBuffer, 0, ReleItem.LENGTH);
                _itemsList.Add(new ReleItem(itemBuffer));
                _itemsList[i].Name = "���� � " + i;
            }
        }
        public void Add(ReleItem item)
        {
            _itemsList.Add(item);
        }
        public ushort[] ToUshort()
        {
            ushort[] ret = new ushort[LENGTH];
            for (int i = 0; i < COUNT; i++)
            {
                Array.ConstrainedCopy(this[i].Values, 0, ret, i * ReleItem.LENGTH, ReleItem.LENGTH);
            }
            return ret;
        }

        public ReleItem this[int i]
        {
            get { return _itemsList[i]; }
            set { _itemsList[i] = value; }
        }

        #region ICollection Members

        public void CopyTo(Array array, int index)
        {
        }

        [DisplayName("����������")]
        public int Count
        {
            get { return _itemsList.Count; }
        }

        [Browsable(false)]
        public bool IsSynchronized
        {
            get { return false; }
        }

        [Browsable(false)]
        public object SyncRoot
        {
            get { return _itemsList; }
        }

        #endregion

        #region IEnumerable Members

        public IEnumerator GetEnumerator()
        {
            return _itemsList.GetEnumerator();
        }

        #endregion
    }
}