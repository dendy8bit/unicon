using System.Collections.Generic;
using BEMN.CS.Structures;

namespace BEMN.CS
{
    public class Strings
    {
        public static bool Version_6_84 { get; set; }
        public static bool Version_8_1 { get; set; }

        public static List<string> Speed => Version_8_1
            ? new List<string>
            {
                "9600",
                "14400",
                "19200",
                "38400",
                "57600",
                "115200"
            }
            : new List<string>
            {

                "300",
                "600",
                "1200",
                "2400",
                "4800",
                "9600",
                "14400",
                "19200",
                "38400",
                "57600",
                "115200",
                "192000",
                "288000",
                "576000"
            };

        public static List<string> Sign
        {
            get
            {
                return new List<string>(new string[]
                {
                    "-",
                    "+"
                });
            }
        }

        public static List<string> DiskretIn
        {
            get
            {
                return new List<string>(new string[]
                {
                    "������",
                    "���������",
                    "�����",
                    "����",
                    "�����"
                });
            }
        }

        public static List<string> DiskretIndication
        {
            get
            {
                return new List<string>(new string[]
                {
                    "�����������",
                    "�������"
                });

            }
        }

        public static List<string> ReleType
        {
            get
            {
                List<string> ret = new List<string>
                {
                    "�����������",
                    "�����������",
                    "�������"
                };
                if (Version_6_84)
                {
                    ret.Add("������������");
                }
                return ret;
            }
        }

        public static List<string> KisType
        {
            get
            {
                return new List<string>(new string[]
                {
                    "����.",
                    "���."
                });
            }
        }

        public static List<string> KisKontrol
        {
            get
            {
                return new List<string>(new string[]
                {
                    "����.",
                    "���."
                });
            }
        }

        public static List<string> DiskretOut
        {
            get
            {
                return new List<string>(new string[]
                {
                    "�����������",
                    "�������",
                    "�������"
                });
            }
        }
        public static List<string> DiscretNumbers
        {
            get
            {
                List<string> relayNames = new List<string>();
                for (int i = 0; i < AllCommutationDiscret.COUNT; i++)
                {
                    relayNames.Add((i + 1).ToString());
                }
                return relayNames;
            }
        }
        /// <summary>
        /// �������� ����
        /// </summary>
        public static List<string> RelayNames
        {
            get
            {
                List<string> relayNames = new List<string>();
                for (int i = 0; i < AllRelays.CONFIG_RELAYS_COUNT-1; i++)
                {
                    relayNames.Add((i + 1).ToString());
                }
                relayNames.Add("��");
                return relayNames;
            }
        }
        
        public static List<string> KisNames
        {
            get
            {
                List<string> relayNames = new List<string>();
                for (int i = 0; i < AllConnectionConf.CONNECTIONS_KIS_COUNT; i++)
                {
                    relayNames.Add((i + 1).ToString());
                }
                return relayNames;
            }
        }
    }
}
