﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.CS.Structures
{
    public class AllConnectionConf : StructBase, IDgvRowsContainer<ConnectionConf>
    {
        public const int CONNECTIONS_KIS_COUNT = 6;

        [Layout(0, Count = CONNECTIONS_KIS_COUNT)] private ConnectionConf[] _connectStruct;
        [Layout(1, Count = 2)] private ConnectionConf[] _reserve;

        public ConnectionConf[] Rows
        {
            get { return this._connectStruct; }
            set { this._connectStruct = value; }
        }
    }
}
