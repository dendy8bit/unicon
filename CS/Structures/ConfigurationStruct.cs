﻿using System;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.CS.Structures
{   
    /// <summary>
    /// Вся конфигурация
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "ТЦС100")]
    public class ConfigurationStruct : StructBase
    {
        [XmlElement(ElementName = "Тип_устройства")]
        public string DeviceType
        {
            get { return "ТЦС100"; }
            set { }
        }
        [XmlElement(ElementName = "Номер_устройства")]
        public string DeviceNumber { get; set; }
        
        [XmlElement(ElementName = "Версия_устройства")]
        public string DeviceVersion { get; set; }

        /// <summary>
        /// Конфигурация всех дискретов 
        /// </summary>
        [Layout(0)] private AllConDiscrets _allConDiscrets;
        /// <summary>
        /// Конфигурация всех реле
        /// </summary>
        [Layout(1)] private AllRelays _allRelays;
        /// <summary>
        /// Конфигурация КИС count
        /// </summary>
        [Layout(2)] private AllConnectionConf _allConnectionConf;
        /// <summary>
        /// Все коммутации дискрет
        /// </summary>
        [Layout(3)] private AllCommutationDiscret _allCommutationDiscret;
        /// <summary>
        /// Все коммутации КИС
        /// </summary>
        [Layout(4)] private AllCommutationKis _allCommutationKis;
        /// <summary>
        /// Все блокировки дискрет
        /// </summary>
        [Layout(5)] private AllBlockDiscret _allBlockDiscret;
        /// <summary>
        /// Все блокировки дискрет
        /// </summary>
        [Layout(6)] private AllBlockKis _allBlockKis;

        [Layout(7)] private NetConfigStruct _netConfig;
        /// <summary>
        /// Коррекция времени
        /// </summary>
        [Layout(8)] private CorrectionValidationStruct _correctionValidation;

        [Layout(9, Count = 90)] private ushort[] _reserve2;
        [Layout(10)] private ushort _crc;

        [BindingProperty(0)]
        [XmlElement(ElementName = "Кофигурация_всех_дискретов")]
        public AllConDiscrets AllConfigDiscrets
        {
            get { return this._allConDiscrets; }
            set { this._allConDiscrets = value; }
        }
        [BindingProperty(1)]
        [XmlElement(ElementName = "Кофигурация_всех_реле")]
        public AllRelays ConfigAllRelays
        {
            get { return this._allRelays; }
            set { this._allRelays = value; }
        }
        [BindingProperty(2)]
        [XmlElement(ElementName = "Кофигурация_КИС")]
        public AllConnectionConf AllConnectionConfig
        {
            get { return this._allConnectionConf; }
            set { this._allConnectionConf = value; }
        }

        [BindingProperty(3)]
        [XmlElement(ElementName = "Коммутации_дискрет")]
        public AllCommutationDiscret CommutatoDiscret
        {
            get { return this._allCommutationDiscret; }
            set { this._allCommutationDiscret = value; }
        }

        [BindingProperty(4)]
        [XmlElement(ElementName = "Коммутации_КИС")]
        public AllCommutationKis CommutatoKis
        {
            get { return this._allCommutationKis; }
            set { this._allCommutationKis = value; }
        }

        [BindingProperty(5)]
        [XmlElement(ElementName = "Блокировки_дискрет")]
        public AllBlockDiscret BlockDiscret
        {
            get { return this._allBlockDiscret; }
            set { this._allBlockDiscret = value; }
        }

        [BindingProperty(6)]
        [XmlElement(ElementName = "Блокировки_КИС")]
        public AllBlockKis BlockKis
        {
            get { return this._allBlockKis; }
            set { this._allBlockKis = value; }
        }

        [BindingProperty(7)]
        [XmlElement(ElementName = "Настройки_RS485")]
        public NetConfigStruct NetConfig
        {
            get { return this._netConfig; }
            set { this._netConfig = value; }
        }

        [BindingProperty(8)]
        [XmlElement(ElementName = "Коррекция_времени")]
        public CorrectionValidationStruct CorrectionValidStruct
        {
            get { return this._correctionValidation; }
            set { this._correctionValidation = value; }
        }

        [XmlIgnore]
        public ushort CRC
        {
            set { this._crc = value; }
        }
    }
}
