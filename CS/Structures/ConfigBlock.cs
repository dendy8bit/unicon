﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.CS.Structures
{
    public class ConfigBlock : StructBase
    {
        [Layout(0)] private ushort _block;

        [BindingProperty(0)]
        [XmlElement(ElementName = "Блокировка_1")]
        public bool Block1
        {
            get { return Common.GetBit(this._block, 0); }
            set { this._block = Common.SetBit(this._block, 0, value); }
        }
        [BindingProperty(1)]
        [XmlElement(ElementName = "Блокировка_2")]
        public bool Block2
        {
            get { return Common.GetBit(this._block, 1); }
            set { this._block = Common.SetBit(this._block, 1, value); }
        }
        [BindingProperty(2)]
        [XmlElement(ElementName = "Блокировка_3")]
        public bool Block3
        {
            get { return Common.GetBit(this._block, 2); }
            set { this._block = Common.SetBit(this._block, 2, value); }
        }
        [BindingProperty(3)]
        [XmlElement(ElementName = "Блокировка_4")]
        public bool Block4
        {
            get { return Common.GetBit(this._block, 3); }
            set { this._block = Common.SetBit(this._block, 3, value); }
        }
        [BindingProperty(4)]
        [XmlElement(ElementName = "Блокировка_5")]
        public bool Block5
        {
            get { return Common.GetBit(this._block, 4); }
            set { this._block = Common.SetBit(this._block, 4, value); }
        }
        [BindingProperty(5)]
        [XmlElement(ElementName = "Блокировка_6")]
        public bool Block6
        {
            get { return Common.GetBit(this._block, 5); }
            set { this._block = Common.SetBit(this._block, 5, value); }
        }
        [BindingProperty(6)]
        [XmlElement(ElementName = "Блокировка_ЗС")]
        public bool Block3C
        {
            get { return Common.GetBit(this._block, 6); }
            set { this._block = Common.SetBit(this._block, 6, value); }
        }
    }
}