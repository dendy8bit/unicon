﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.CS.Structures
{
    public class AllConDiscrets : StructBase, IDgvRowsContainer<ConDiscrets>
    {
        public const int CONFIG_DISCRETS_COUNT = 40;
        private const int RESERVE = 24;

        [Layout(0, Count = CONFIG_DISCRETS_COUNT)] private ConDiscrets[] _configsStruct;
        [Layout(1, Count = RESERVE)] private ConDiscrets[] _res;
        
        public ConDiscrets[] Rows
        {
            get { return this._configsStruct; }
            set { this._configsStruct = value; }
        }
    }
}
