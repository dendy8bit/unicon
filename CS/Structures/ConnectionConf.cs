﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.CS.Structures
{
    public class ConnectionConf : StructBase
    {
        [Layout(0)] private ushort _res;
        [Layout(1)] private ushort _type;
        [Layout(2)] private ushort _tv;
        [Layout(3)] private ushort _ts;
        
        [BindingProperty(0)]
        [XmlElement(ElementName = "Тип")]
        public string Type
        {
            get { return Validator.Get(this._type, Strings.KisType, 0); }
            set { this._type = Validator.Set(value, Strings.KisType, this._type, 0); }
        }

        [BindingProperty(1)]
        [XmlElement(ElementName = "Контроль")]
        public string Contrl
        {
            get { return Validator.Get(this._type, Strings.KisKontrol, 8); }
            set { this._type = Validator.Set(value, Strings.KisKontrol, this._type, 8); }
        }

        [BindingProperty(2)]
        [XmlElement(ElementName = "Время_срабатывания")]
        public ushort TimeSrab
        {
            get { return this._ts; }
            set { this._ts = value; }
        }

        [BindingProperty(3)]
        [XmlElement(ElementName = "Время_возврата")]
        public ushort TimeVozv
        {
            get { return (ushort)(this._tv/256); }
            set { this._tv = (ushort)(value * 256); }
        }
    }
}
