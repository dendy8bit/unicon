﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.CS.Structures
{
    public class AllCommutationDiscret : StructBase, IDgvRowsContainer<ConfigCommutation>
    {
        public const int COUNT = 40;

        [Layout(0, Count = COUNT)] private ConfigCommutation[] _configsStruct;

        public ConfigCommutation[] Rows
        {
            get { return this._configsStruct; }
            set { this._configsStruct = value; }
        }

    }
}