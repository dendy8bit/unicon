﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.CS.Structures
{
    public class AllBlock : StructBase, IDgvRowsContainer<ConfigBlock>
    {
        public const int CONFIG_BLOCK_COUNT = 40;

        [Layout(0, Count = CONFIG_BLOCK_COUNT)]
        private ConfigBlock[] _configsStruct;

        public ConfigBlock[] Rows
        {
            get { return this._configsStruct; }
            set { this._configsStruct = value; }
        }
    }
}