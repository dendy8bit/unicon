﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.CS.Structures
{
    public class AllBlockDiscret : StructBase, IDgvRowsContainer<ConfigBlock>
    {
        public const int COUNT = 40;

        [Layout(0, Count = COUNT)] private ConfigBlock[] _configsStruct;

        public ConfigBlock[] Rows
        {
            get { return this._configsStruct; }
            set { this._configsStruct = value; }
        }
    }
}