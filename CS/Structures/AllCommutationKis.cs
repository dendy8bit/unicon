﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.CS.Structures
{
    public class AllCommutationKis : StructBase, IDgvRowsContainer<ConfigCommutation>
    {
        public const int COUNT = 6;
        [Layout(0, Count = COUNT)] private ConfigCommutation[] _configsStruct;
        [Layout(1, Count = 18)] private ushort[] _reserve;

        public ConfigCommutation[] Rows
        {
            get { return this._configsStruct; }
            set { this._configsStruct = value; }
        }
    }
}
