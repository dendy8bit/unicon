﻿using System;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.CS.Structures
{
    public class CorrectionValidationStruct : StructBase
    {
        public static int MAX_VALUE_TIME;

        [Layout(0)] private ushort _timeCorrect;
        [Layout(1)] private ushort _sign;

        [BindingProperty(0)]
        [XmlElement(ElementName = "Коррекция_времени")]
        public double TimeCorrect
        {
            get
            {
                if (this._timeCorrect == 0) return 0;
                double d = Math.Floor(MAX_VALUE_TIME*10/(double) this._timeCorrect);
                return d/10;
            }
            set { this._timeCorrect = (ushort)(MAX_VALUE_TIME/value); }
        }

        [BindingProperty(1)]
        [XmlElement(ElementName = "Знак")]
        public string Sign
        {
            get { return Validator.Get(this._sign, Strings.Sign); }
            set { this._sign = Validator.Set(value, Strings.Sign); }
        }
    }
}
