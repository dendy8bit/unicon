﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.CS.Structures
{
    public class ConfigRelay : StructBase
    {
        [Layout(0)] private ushort _wait;
        [Layout(1)] private ushort _type;
        [Layout(2)] private ushort _res;
        [Layout(3)] private ushort _res1;

        [BindingProperty(0)]
        [XmlElement(ElementName = "Тип_дискретного_выхода")]
        public string TypeXml
        {
            get { return Validator.Get(this._type, Strings.ReleType, 0, 1); }
            set { this._type = Validator.Set(value, Strings.ReleType, this._type, 0, 1); }
        }

        [BindingProperty(1)]
        [XmlElement(ElementName = "Длительность_импульса")]
        public double Wait
        {
            get { return (double)this._wait / 1000; } //get { return ValuesConverterCommon.GetWaitTime(this._wait); }
            set { this._wait = (ushort)(value * 1000); } //set { this._wait = ValuesConverterCommon.SetWaitTime(value); }
        }
    }
}
