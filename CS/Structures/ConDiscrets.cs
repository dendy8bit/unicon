﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.CS.Structures
{
    public class ConDiscrets : StructBase
    {
        [Layout(0)] private ushort _ts;//Время_выдержки_фронта
        [Layout(1)] private ushort _tv;//Время_спада
        [Layout(2)] private ushort _res;//резерв
        [Layout(3)] private ushort _type;//Тип_дискр_входа, Тип_индикации

        [BindingProperty(0)]
        [XmlElement(ElementName = "Тип")]
        public string Type
        {
            get { return Validator.Get(this._type, Strings.DiskretIn, 0, 1 , 2); }
            set { this._type = Validator.Set(value, Strings.DiskretIn, this._type, 0, 1 , 2); }
        }
        [BindingProperty(1)]
        [XmlElement(ElementName = "Тип_инд")]
        public string TypeInd
        {
            get { return Validator.Get(this._type, Strings.DiskretIndication, 8); }
            set { this._type = Validator.Set(value, Strings.DiskretIndication, this._type, 8); }
        }
        [BindingProperty(2)]
        [XmlElement(ElementName = "Тс")]
        public double TimeSrab
        {
            get { return this._ts/1000.0; } //ValuesConverterCommon.GetWaitTime(this._ts); }
            set { this._ts = (ushort)(value*1000); } //ValuesConverterCommon.SetWaitTime(value); }
        }
        [BindingProperty(3)]
        [XmlElement(ElementName = "Тв")]
        public double TimeVozv
        {
            get { return this._tv / 1000.0; } //ValuesConverterCommon.GetWaitTime(this._tf); }
            set { this._tv = (ushort)(value * 1000); } //ValuesConverterCommon.SetWaitTime(value); }
        }
    }
}
