﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.CS.Structures
{
    public class AllRelays : StructBase, IDgvRowsContainer<ConfigRelay>
    {
        public const int CONFIG_RELAYS_COUNT = 7;
        private const int RESERVE = 57;

        [Layout(0, Count = CONFIG_RELAYS_COUNT)] private ConfigRelay[] _relaysStruct;
        [Layout(1, Count = RESERVE)] private ConfigRelay[] _reserve;

        public ConfigRelay[] Rows
        {
            get { return this._relaysStruct;  }
            set { this._relaysStruct = value; }
        }
    }
}
