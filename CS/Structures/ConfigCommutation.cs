﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.CS.Structures
{
    public class ConfigCommutation : StructBase
    {
        [Layout(0)] private ushort _switch;

        [BindingProperty(0)]
        [XmlElement(ElementName = "Коммутация_1")]
        public bool Switch1
        {
            get { return Common.GetBit(this._switch, 0); }
            set { this._switch = Common.SetBit(this._switch, 0, value); }
        }
        [BindingProperty(1)]
        [XmlElement(ElementName = "Коммутация_2")]
        public bool Switch2
        {
            get { return Common.GetBit(this._switch, 1); }
            set { this._switch = Common.SetBit(this._switch, 1, value); }
        }
        [BindingProperty(2)]
        [XmlElement(ElementName = "Коммутация_3")]
        public bool Switch3
        {
            get { return Common.GetBit(this._switch, 2); }
            set { this._switch = Common.SetBit(this._switch, 2, value); }
        }
        [BindingProperty(3)]
        [XmlElement(ElementName = "Коммутация_4")]
        public bool Switch4
        {
            get { return Common.GetBit(this._switch, 3); }
            set { this._switch = Common.SetBit(this._switch, 3, value); }
        }
        [BindingProperty(4)]
        [XmlElement(ElementName = "Коммутация_5")]
        public bool Switch5
        {
            get { return Common.GetBit(this._switch, 4); }
            set { this._switch = Common.SetBit(this._switch, 4, value); }
        }
        [BindingProperty(5)]
        [XmlElement(ElementName = "Коммутация_6")]
        public bool Switch6
        {
            get { return Common.GetBit(this._switch, 5); }
            set { this._switch = Common.SetBit(this._switch, 5, value); }
        }
        [BindingProperty(6)]
        [XmlElement(ElementName = "Коммутация_3C")]
        public bool Switch3C
        {
            get { return Common.GetBit(this._switch, 6); }
            set { this._switch = Common.SetBit(this._switch, 6, value); }
        }
    }
}