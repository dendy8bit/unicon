﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.CS.Structures
{
    public class AllBlockKis : StructBase, IDgvRowsContainer<ConfigBlock>
    {
        public const int COUNT = 6;

        [Layout(0, Count = COUNT)] private ConfigBlock[] _configsStruct;
        [Layout(1, Count = 18)] private ushort[] _reserve;

        public ConfigBlock[] Rows
        {
            get { return this._configsStruct; }
            set { this._configsStruct = value; }
        }
    }
}
