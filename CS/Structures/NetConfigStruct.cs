﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.CS.Structures
{
    public class NetConfigStruct : StructBase
    {
        [Layout(0)] private ushort _password;
        [Layout(1)] private ushort _speed;
        [Layout(2)] private ushort _addr;

        [BindingProperty(0)]
        public ushort Password
        {
            get { return this._password; }
            set { this._password = 0; }
        }

        [BindingProperty(1)]
        public string Speed
        {
            get { return Validator.Get(this._speed, Strings.Speed); }
            set { this._speed = Validator.Set(value, Strings.Speed); }
        }

        [BindingProperty(2)]
        public ushort Address
        {
            get { return this._addr; }
            set { this._addr = value; }
        }
    }
}
