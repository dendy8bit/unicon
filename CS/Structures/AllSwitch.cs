﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.CS.Structures
{
    public class AllSwitch : StructBase, IDgvRowsContainer<ConfigSwitch>
    {
        public const int CONFIG_SWITCH_COUNT = 40;

        [Layout(0, Count = CONFIG_SWITCH_COUNT)] private ConfigSwitch[] _configsStruct;

        public ConfigSwitch[] Rows
        {
            get { return this._configsStruct; }
            set { this._configsStruct = value; }
        }

    }
}