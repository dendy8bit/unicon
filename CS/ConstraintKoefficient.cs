namespace BEMN.CS
{
    public enum ConstraintKoefficient
    {
        K_25600 = 25600,
        K_10000 = 10000,
        K_4000 = 4002,
        K_500 = 500,
        K_Undefine
    } ;
}