namespace BEMN.CS.Forms
{
    partial class CS_ConfigurationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this._tabControl = new System.Windows.Forms.TabControl();
            this._konfDiscretPage = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this._dataGridDiskretView = new System.Windows.Forms.DataGridView();
            this._extDiskretNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._extDiskreTypeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._extDiskretTypeIndCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._extDiskretTcCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._extDiskretTvCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._extDiskretKomut1Col = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._extDiskretKomut2Col = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._extDiskretKomut3Col = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._extDiskretKomut4Col = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._extDiskretKomut5Col = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._extDiskretKomut6Col = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._extDiskretKomut3cCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._extDiskretBlock1Col = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._extDiskretBlock2Col = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._extDiskretBlock3Col = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._extDiskretBlock4Col = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._extDiskretBlock5Col = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._extDiskretBlock6Col = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._extDiskretBlock3cCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._konfKISPage = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this._dataGridKISView = new System.Windows.Forms.DataGridView();
            this._extKisNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._extKisTypeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._extKisKonfCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tsCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tvCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._extKisKomut1Col = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._extKisKomut2Col = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._extKisKomut3Col = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._extKisKomut4Col = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._extKisKomut5Col = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._extKisKomut6Col = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._extKisKomut3cCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._extKisBlock1Col = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._extKisBlock2Col = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._extKisBlock3Col = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._extKisBlock4Col = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._extKisBlock5Col = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._extKisBlock6Col = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._extKisBlock3cCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._konfRelePage = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._dataGridReleView = new System.Windows.Forms.DataGridView();
            this._extReleNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._extReleTypeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._extReleTypeImpCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._conPage = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this._timeValidation = new System.Windows.Forms.MaskedTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this._timeValidationSign = new System.Windows.Forms.ComboBox();
            this._statusStrip = new System.Windows.Forms.StatusStrip();
            this._exchangeProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this._processLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._saveConfigBut = new System.Windows.Forms.Button();
            this._loadConfigBut = new System.Windows.Forms.Button();
            this._writeConfigBut = new System.Windows.Forms.Button();
            this._readConfigBut = new System.Windows.Forms.Button();
            this._toolTip = new System.Windows.Forms.ToolTip(this.components);
            this._saveConfigurationDlg = new System.Windows.Forms.SaveFileDialog();
            this._openConfigurationDlg = new System.Windows.Forms.OpenFileDialog();
            this._saveToXmlButton = new System.Windows.Forms.Button();
            this._saveXmlDialog = new System.Windows.Forms.SaveFileDialog();
            this._tabControl.SuspendLayout();
            this._konfDiscretPage.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._dataGridDiskretView)).BeginInit();
            this._konfKISPage.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._dataGridKISView)).BeginInit();
            this._konfRelePage.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._dataGridReleView)).BeginInit();
            this._conPage.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this._statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // _tabControl
            // 
            this._tabControl.Controls.Add(this._konfDiscretPage);
            this._tabControl.Controls.Add(this._konfKISPage);
            this._tabControl.Controls.Add(this._konfRelePage);
            this._tabControl.Controls.Add(this._conPage);
            this._tabControl.Location = new System.Drawing.Point(0, 2);
            this._tabControl.Name = "_tabControl";
            this._tabControl.SelectedIndex = 0;
            this._tabControl.Size = new System.Drawing.Size(787, 429);
            this._tabControl.TabIndex = 0;
            // 
            // _konfDiscretPage
            // 
            this._konfDiscretPage.Controls.Add(this.groupBox3);
            this._konfDiscretPage.Location = new System.Drawing.Point(4, 22);
            this._konfDiscretPage.Name = "_konfDiscretPage";
            this._konfDiscretPage.Padding = new System.Windows.Forms.Padding(3);
            this._konfDiscretPage.Size = new System.Drawing.Size(779, 403);
            this._konfDiscretPage.TabIndex = 0;
            this._konfDiscretPage.Text = "��������";
            this._konfDiscretPage.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this._dataGridDiskretView);
            this.groupBox3.Location = new System.Drawing.Point(6, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(765, 381);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "������������ ���������";
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.SystemColors.Info;
            this.label4.Location = new System.Drawing.Point(534, -2);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(207, 22);
            this.label4.TabIndex = 3;
            this.label4.Text = "����������";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(330, -2);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(208, 22);
            this.label3.TabIndex = 2;
            this.label3.Text = "����������";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _dataGridDiskretView
            // 
            this._dataGridDiskretView.AllowUserToAddRows = false;
            this._dataGridDiskretView.AllowUserToDeleteRows = false;
            this._dataGridDiskretView.AllowUserToResizeColumns = false;
            this._dataGridDiskretView.AllowUserToResizeRows = false;
            dataGridViewCellStyle7.NullValue = null;
            this._dataGridDiskretView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle7;
            this._dataGridDiskretView.BackgroundColor = System.Drawing.SystemColors.Control;
            this._dataGridDiskretView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._extDiskretNumberCol,
            this._extDiskreTypeCol,
            this._extDiskretTypeIndCol,
            this._extDiskretTcCol,
            this._extDiskretTvCol,
            this._extDiskretKomut1Col,
            this._extDiskretKomut2Col,
            this._extDiskretKomut3Col,
            this._extDiskretKomut4Col,
            this._extDiskretKomut5Col,
            this._extDiskretKomut6Col,
            this._extDiskretKomut3cCol,
            this._extDiskretBlock1Col,
            this._extDiskretBlock2Col,
            this._extDiskretBlock3Col,
            this._extDiskretBlock4Col,
            this._extDiskretBlock5Col,
            this._extDiskretBlock6Col,
            this._extDiskretBlock3cCol});
            this._dataGridDiskretView.Location = new System.Drawing.Point(8, 19);
            this._dataGridDiskretView.Name = "_dataGridDiskretView";
            this._dataGridDiskretView.RowHeadersVisible = false;
            this._dataGridDiskretView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._dataGridDiskretView.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this._dataGridDiskretView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._dataGridDiskretView.Size = new System.Drawing.Size(752, 356);
            this._dataGridDiskretView.TabIndex = 1;
            this._dataGridDiskretView.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this._dataGridDiskretView_CellValidating);
            // 
            // _extDiskretNumberCol
            // 
            this._extDiskretNumberCol.HeaderText = "�";
            this._extDiskretNumberCol.Name = "_extDiskretNumberCol";
            this._extDiskretNumberCol.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._extDiskretNumberCol.Width = 30;
            // 
            // _extDiskreTypeCol
            // 
            this._extDiskreTypeCol.HeaderText = "���";
            this._extDiskreTypeCol.Name = "_extDiskreTypeCol";
            this._extDiskreTypeCol.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._extDiskreTypeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this._extDiskreTypeCol.Width = 70;
            // 
            // _extDiskretTypeIndCol
            // 
            this._extDiskretTypeIndCol.HeaderText = "��� ���.";
            this._extDiskretTypeIndCol.Name = "_extDiskretTypeIndCol";
            this._extDiskretTypeIndCol.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._extDiskretTypeIndCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // _extDiskretTcCol
            // 
            this._extDiskretTcCol.HeaderText = "Tc (c)";
            this._extDiskretTcCol.Name = "_extDiskretTcCol";
            this._extDiskretTcCol.Width = 60;
            // 
            // _extDiskretTvCol
            // 
            this._extDiskretTvCol.HeaderText = "�� (�)";
            this._extDiskretTvCol.Name = "_extDiskretTvCol";
            this._extDiskretTvCol.Width = 60;
            // 
            // _extDiskretKomut1Col
            // 
            this._extDiskretKomut1Col.HeaderText = "1";
            this._extDiskretKomut1Col.Name = "_extDiskretKomut1Col";
            this._extDiskretKomut1Col.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._extDiskretKomut1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this._extDiskretKomut1Col.Width = 29;
            // 
            // _extDiskretKomut2Col
            // 
            this._extDiskretKomut2Col.HeaderText = "2";
            this._extDiskretKomut2Col.Name = "_extDiskretKomut2Col";
            this._extDiskretKomut2Col.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._extDiskretKomut2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this._extDiskretKomut2Col.Width = 29;
            // 
            // _extDiskretKomut3Col
            // 
            this._extDiskretKomut3Col.HeaderText = "3";
            this._extDiskretKomut3Col.Name = "_extDiskretKomut3Col";
            this._extDiskretKomut3Col.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._extDiskretKomut3Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this._extDiskretKomut3Col.Width = 29;
            // 
            // _extDiskretKomut4Col
            // 
            this._extDiskretKomut4Col.HeaderText = "4";
            this._extDiskretKomut4Col.Name = "_extDiskretKomut4Col";
            this._extDiskretKomut4Col.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._extDiskretKomut4Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this._extDiskretKomut4Col.Width = 29;
            // 
            // _extDiskretKomut5Col
            // 
            this._extDiskretKomut5Col.HeaderText = "5";
            this._extDiskretKomut5Col.Name = "_extDiskretKomut5Col";
            this._extDiskretKomut5Col.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._extDiskretKomut5Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this._extDiskretKomut5Col.Width = 29;
            // 
            // _extDiskretKomut6Col
            // 
            this._extDiskretKomut6Col.HeaderText = "6";
            this._extDiskretKomut6Col.Name = "_extDiskretKomut6Col";
            this._extDiskretKomut6Col.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._extDiskretKomut6Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this._extDiskretKomut6Col.Width = 29;
            // 
            // _extDiskretKomut3cCol
            // 
            this._extDiskretKomut3cCol.HeaderText = "��";
            this._extDiskretKomut3cCol.Name = "_extDiskretKomut3cCol";
            this._extDiskretKomut3cCol.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._extDiskretKomut3cCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this._extDiskretKomut3cCol.Width = 32;
            // 
            // _extDiskretBlock1Col
            // 
            this._extDiskretBlock1Col.HeaderText = "1";
            this._extDiskretBlock1Col.Name = "_extDiskretBlock1Col";
            this._extDiskretBlock1Col.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._extDiskretBlock1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this._extDiskretBlock1Col.Width = 29;
            // 
            // _extDiskretBlock2Col
            // 
            this._extDiskretBlock2Col.HeaderText = "2";
            this._extDiskretBlock2Col.Name = "_extDiskretBlock2Col";
            this._extDiskretBlock2Col.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._extDiskretBlock2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this._extDiskretBlock2Col.Width = 29;
            // 
            // _extDiskretBlock3Col
            // 
            this._extDiskretBlock3Col.HeaderText = "3";
            this._extDiskretBlock3Col.Name = "_extDiskretBlock3Col";
            this._extDiskretBlock3Col.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._extDiskretBlock3Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this._extDiskretBlock3Col.Width = 29;
            // 
            // _extDiskretBlock4Col
            // 
            this._extDiskretBlock4Col.HeaderText = "4";
            this._extDiskretBlock4Col.Name = "_extDiskretBlock4Col";
            this._extDiskretBlock4Col.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._extDiskretBlock4Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this._extDiskretBlock4Col.Width = 29;
            // 
            // _extDiskretBlock5Col
            // 
            this._extDiskretBlock5Col.HeaderText = "5";
            this._extDiskretBlock5Col.Name = "_extDiskretBlock5Col";
            this._extDiskretBlock5Col.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._extDiskretBlock5Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this._extDiskretBlock5Col.Width = 29;
            // 
            // _extDiskretBlock6Col
            // 
            this._extDiskretBlock6Col.HeaderText = "6";
            this._extDiskretBlock6Col.Name = "_extDiskretBlock6Col";
            this._extDiskretBlock6Col.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._extDiskretBlock6Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this._extDiskretBlock6Col.Width = 29;
            // 
            // _extDiskretBlock3cCol
            // 
            this._extDiskretBlock3cCol.HeaderText = "��";
            this._extDiskretBlock3cCol.Name = "_extDiskretBlock3cCol";
            this._extDiskretBlock3cCol.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._extDiskretBlock3cCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this._extDiskretBlock3cCol.Width = 32;
            // 
            // _konfKISPage
            // 
            this._konfKISPage.Controls.Add(this.groupBox2);
            this._konfKISPage.Location = new System.Drawing.Point(4, 22);
            this._konfKISPage.Name = "_konfKISPage";
            this._konfKISPage.Padding = new System.Windows.Forms.Padding(3);
            this._konfKISPage.Size = new System.Drawing.Size(779, 403);
            this._konfKISPage.TabIndex = 1;
            this._konfKISPage.Text = "����� ���";
            this._konfKISPage.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this._dataGridKISView);
            this.groupBox2.Location = new System.Drawing.Point(8, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(697, 184);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "������������ ���";
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.SystemColors.Info;
            this.label5.Location = new System.Drawing.Point(476, -5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(182, 22);
            this.label5.TabIndex = 5;
            this.label5.Text = "����������";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(297, -3);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(183, 22);
            this.label6.TabIndex = 4;
            this.label6.Text = "����������";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _dataGridKISView
            // 
            this._dataGridKISView.AllowUserToAddRows = false;
            this._dataGridKISView.AllowUserToDeleteRows = false;
            this._dataGridKISView.AllowUserToResizeColumns = false;
            this._dataGridKISView.AllowUserToResizeRows = false;
            dataGridViewCellStyle9.NullValue = null;
            this._dataGridKISView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle9;
            this._dataGridKISView.BackgroundColor = System.Drawing.SystemColors.Control;
            this._dataGridKISView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._extKisNumberCol,
            this._extKisTypeCol,
            this._extKisKonfCol,
            this._tsCol,
            this._tvCol,
            this._extKisKomut1Col,
            this._extKisKomut2Col,
            this._extKisKomut3Col,
            this._extKisKomut4Col,
            this._extKisKomut5Col,
            this._extKisKomut6Col,
            this._extKisKomut3cCol,
            this._extKisBlock1Col,
            this._extKisBlock2Col,
            this._extKisBlock3Col,
            this._extKisBlock4Col,
            this._extKisBlock5Col,
            this._extKisBlock6Col,
            this._extKisBlock3cCol});
            this._dataGridKISView.Location = new System.Drawing.Point(6, 19);
            this._dataGridKISView.Name = "_dataGridKISView";
            this._dataGridKISView.RowHeadersVisible = false;
            this._dataGridKISView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._dataGridKISView.RowsDefaultCellStyle = dataGridViewCellStyle10;
            this._dataGridKISView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._dataGridKISView.Size = new System.Drawing.Size(657, 157);
            this._dataGridKISView.TabIndex = 1;
            this._dataGridKISView.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this._dataGridKISView_CellValidating);
            // 
            // _extKisNumberCol
            // 
            this._extKisNumberCol.HeaderText = "�";
            this._extKisNumberCol.Name = "_extKisNumberCol";
            this._extKisNumberCol.ReadOnly = true;
            this._extKisNumberCol.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._extKisNumberCol.Width = 30;
            // 
            // _extKisTypeCol
            // 
            this._extKisTypeCol.HeaderText = "���";
            this._extKisTypeCol.Name = "_extKisTypeCol";
            this._extKisTypeCol.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._extKisTypeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this._extKisTypeCol.Width = 70;
            // 
            // _extKisKonfCol
            // 
            this._extKisKonfCol.HeaderText = "�����.";
            this._extKisKonfCol.Name = "_extKisKonfCol";
            this._extKisKonfCol.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._extKisKonfCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this._extKisKonfCol.Width = 70;
            // 
            // _tsCol
            // 
            this._tsCol.HeaderText = "Tc (�)";
            this._tsCol.Name = "_tsCol";
            this._tsCol.Width = 60;
            // 
            // _tvCol
            // 
            this._tvCol.HeaderText = "T� (�)";
            this._tvCol.Name = "_tvCol";
            this._tvCol.Width = 60;
            // 
            // _extKisKomut1Col
            // 
            this._extKisKomut1Col.HeaderText = "1";
            this._extKisKomut1Col.Name = "_extKisKomut1Col";
            this._extKisKomut1Col.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._extKisKomut1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this._extKisKomut1Col.Width = 25;
            // 
            // _extKisKomut2Col
            // 
            this._extKisKomut2Col.HeaderText = "2";
            this._extKisKomut2Col.Name = "_extKisKomut2Col";
            this._extKisKomut2Col.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._extKisKomut2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this._extKisKomut2Col.Width = 25;
            // 
            // _extKisKomut3Col
            // 
            this._extKisKomut3Col.HeaderText = "3";
            this._extKisKomut3Col.Name = "_extKisKomut3Col";
            this._extKisKomut3Col.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._extKisKomut3Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this._extKisKomut3Col.Width = 25;
            // 
            // _extKisKomut4Col
            // 
            this._extKisKomut4Col.HeaderText = "4";
            this._extKisKomut4Col.Name = "_extKisKomut4Col";
            this._extKisKomut4Col.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._extKisKomut4Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this._extKisKomut4Col.Width = 25;
            // 
            // _extKisKomut5Col
            // 
            this._extKisKomut5Col.HeaderText = "5";
            this._extKisKomut5Col.Name = "_extKisKomut5Col";
            this._extKisKomut5Col.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._extKisKomut5Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this._extKisKomut5Col.Width = 25;
            // 
            // _extKisKomut6Col
            // 
            this._extKisKomut6Col.HeaderText = "6";
            this._extKisKomut6Col.Name = "_extKisKomut6Col";
            this._extKisKomut6Col.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._extKisKomut6Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this._extKisKomut6Col.Width = 25;
            // 
            // _extKisKomut3cCol
            // 
            this._extKisKomut3cCol.HeaderText = "��";
            this._extKisKomut3cCol.Name = "_extKisKomut3cCol";
            this._extKisKomut3cCol.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._extKisKomut3cCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this._extKisKomut3cCol.Width = 30;
            // 
            // _extKisBlock1Col
            // 
            this._extKisBlock1Col.HeaderText = "1";
            this._extKisBlock1Col.Name = "_extKisBlock1Col";
            this._extKisBlock1Col.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._extKisBlock1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this._extKisBlock1Col.Width = 25;
            // 
            // _extKisBlock2Col
            // 
            this._extKisBlock2Col.HeaderText = "2";
            this._extKisBlock2Col.Name = "_extKisBlock2Col";
            this._extKisBlock2Col.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._extKisBlock2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this._extKisBlock2Col.Width = 25;
            // 
            // _extKisBlock3Col
            // 
            this._extKisBlock3Col.HeaderText = "3";
            this._extKisBlock3Col.Name = "_extKisBlock3Col";
            this._extKisBlock3Col.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._extKisBlock3Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this._extKisBlock3Col.Width = 25;
            // 
            // _extKisBlock4Col
            // 
            this._extKisBlock4Col.HeaderText = "4";
            this._extKisBlock4Col.Name = "_extKisBlock4Col";
            this._extKisBlock4Col.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._extKisBlock4Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this._extKisBlock4Col.Width = 25;
            // 
            // _extKisBlock5Col
            // 
            this._extKisBlock5Col.HeaderText = "5";
            this._extKisBlock5Col.Name = "_extKisBlock5Col";
            this._extKisBlock5Col.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._extKisBlock5Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this._extKisBlock5Col.Width = 25;
            // 
            // _extKisBlock6Col
            // 
            this._extKisBlock6Col.HeaderText = "6";
            this._extKisBlock6Col.Name = "_extKisBlock6Col";
            this._extKisBlock6Col.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._extKisBlock6Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this._extKisBlock6Col.Width = 25;
            // 
            // _extKisBlock3cCol
            // 
            this._extKisBlock3cCol.HeaderText = "��";
            this._extKisBlock3cCol.Name = "_extKisBlock3cCol";
            this._extKisBlock3cCol.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._extKisBlock3cCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this._extKisBlock3cCol.Width = 30;
            // 
            // _konfRelePage
            // 
            this._konfRelePage.Controls.Add(this.groupBox1);
            this._konfRelePage.Location = new System.Drawing.Point(4, 22);
            this._konfRelePage.Name = "_konfRelePage";
            this._konfRelePage.Size = new System.Drawing.Size(779, 403);
            this._konfRelePage.TabIndex = 3;
            this._konfRelePage.Text = "����";
            this._konfRelePage.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._dataGridReleView);
            this.groupBox1.Location = new System.Drawing.Point(8, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(212, 205);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "������������ ����";
            // 
            // _dataGridReleView
            // 
            this._dataGridReleView.AllowUserToAddRows = false;
            this._dataGridReleView.AllowUserToDeleteRows = false;
            this._dataGridReleView.AllowUserToResizeColumns = false;
            this._dataGridReleView.AllowUserToResizeRows = false;
            dataGridViewCellStyle11.NullValue = null;
            this._dataGridReleView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle11;
            this._dataGridReleView.BackgroundColor = System.Drawing.SystemColors.Control;
            this._dataGridReleView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._extReleNumberCol,
            this._extReleTypeCol,
            this._extReleTypeImpCol});
            this._dataGridReleView.Location = new System.Drawing.Point(7, 19);
            this._dataGridReleView.Name = "_dataGridReleView";
            this._dataGridReleView.RowHeadersVisible = false;
            this._dataGridReleView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._dataGridReleView.RowsDefaultCellStyle = dataGridViewCellStyle12;
            this._dataGridReleView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._dataGridReleView.Size = new System.Drawing.Size(198, 179);
            this._dataGridReleView.TabIndex = 2;
            this._dataGridReleView.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this._dataGridReleView_CellValidating);
            // 
            // _extReleNumberCol
            // 
            this._extReleNumberCol.HeaderText = "�";
            this._extReleNumberCol.Name = "_extReleNumberCol";
            this._extReleNumberCol.ReadOnly = true;
            this._extReleNumberCol.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._extReleNumberCol.Width = 30;
            // 
            // _extReleTypeCol
            // 
            this._extReleTypeCol.HeaderText = "���";
            this._extReleTypeCol.Name = "_extReleTypeCol";
            this._extReleTypeCol.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._extReleTypeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this._extReleTypeCol.Width = 98;
            // 
            // _extReleTypeImpCol
            // 
            this._extReleTypeImpCol.HeaderText = "� ���. (�)";
            this._extReleTypeImpCol.Name = "_extReleTypeImpCol";
            this._extReleTypeImpCol.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._extReleTypeImpCol.Width = 67;
            // 
            // _conPage
            // 
            this._conPage.Controls.Add(this.groupBox5);
            this._conPage.Location = new System.Drawing.Point(4, 22);
            this._conPage.Name = "_conPage";
            this._conPage.Size = new System.Drawing.Size(779, 403);
            this._conPage.TabIndex = 2;
            this._conPage.Text = "��������� �����";
            this._conPage.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this._timeValidation);
            this.groupBox5.Controls.Add(this.label1);
            this.groupBox5.Controls.Add(this._timeValidationSign);
            this.groupBox5.Location = new System.Drawing.Point(8, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(271, 56);
            this.groupBox5.TabIndex = 6;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "��������� �����";
            // 
            // _timeValidation
            // 
            this._timeValidation.Location = new System.Drawing.Point(46, 27);
            this._timeValidation.Name = "_timeValidation";
            this._timeValidation.Size = new System.Drawing.Size(66, 20);
            this._timeValidation.TabIndex = 5;
            this._timeValidation.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(119, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "������ � �����";
            // 
            // _timeValidationSign
            // 
            this._timeValidationSign.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._timeValidationSign.Items.AddRange(new object[] {
            "+",
            "-"});
            this._timeValidationSign.Location = new System.Drawing.Point(6, 27);
            this._timeValidationSign.Name = "_timeValidationSign";
            this._timeValidationSign.Size = new System.Drawing.Size(34, 21);
            this._timeValidationSign.TabIndex = 3;
            // 
            // _statusStrip
            // 
            this._statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._exchangeProgressBar,
            this._processLabel});
            this._statusStrip.Location = new System.Drawing.Point(0, 463);
            this._statusStrip.Name = "_statusStrip";
            this._statusStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this._statusStrip.Size = new System.Drawing.Size(782, 22);
            this._statusStrip.TabIndex = 6;
            this._statusStrip.Text = "statusStrip1";
            // 
            // _exchangeProgressBar
            // 
            this._exchangeProgressBar.Maximum = 33;
            this._exchangeProgressBar.Name = "_exchangeProgressBar";
            this._exchangeProgressBar.Size = new System.Drawing.Size(100, 16);
            // 
            // _processLabel
            // 
            this._processLabel.Name = "_processLabel";
            this._processLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // _saveConfigBut
            // 
            this._saveConfigBut.Location = new System.Drawing.Point(456, 437);
            this._saveConfigBut.Name = "_saveConfigBut";
            this._saveConfigBut.Size = new System.Drawing.Size(122, 23);
            this._saveConfigBut.TabIndex = 10;
            this._saveConfigBut.Text = "��������� � ����";
            this._saveConfigBut.UseVisualStyleBackColor = true;
            this._saveConfigBut.Click += new System.EventHandler(this._saveConfigBut_Click);
            // 
            // _loadConfigBut
            // 
            this._loadConfigBut.Location = new System.Drawing.Point(328, 437);
            this._loadConfigBut.Name = "_loadConfigBut";
            this._loadConfigBut.Size = new System.Drawing.Size(122, 23);
            this._loadConfigBut.TabIndex = 9;
            this._loadConfigBut.Text = "��������� �� �����";
            this._loadConfigBut.UseVisualStyleBackColor = true;
            this._loadConfigBut.Click += new System.EventHandler(this._loadConfigBut_Click);
            // 
            // _writeConfigBut
            // 
            this._writeConfigBut.Location = new System.Drawing.Point(164, 437);
            this._writeConfigBut.Name = "_writeConfigBut";
            this._writeConfigBut.Size = new System.Drawing.Size(158, 23);
            this._writeConfigBut.TabIndex = 8;
            this._writeConfigBut.Text = "�������� � ����������";
            this._writeConfigBut.UseVisualStyleBackColor = true;
            this._writeConfigBut.Click += new System.EventHandler(this._writeConfigBut_Click);
            // 
            // _readConfigBut
            // 
            this._readConfigBut.Location = new System.Drawing.Point(0, 437);
            this._readConfigBut.Name = "_readConfigBut";
            this._readConfigBut.Size = new System.Drawing.Size(158, 23);
            this._readConfigBut.TabIndex = 7;
            this._readConfigBut.Text = "��������� �� ����������";
            this._readConfigBut.UseVisualStyleBackColor = true;
            this._readConfigBut.Click += new System.EventHandler(this._readConfigBut_Click);
            // 
            // _toolTip
            // 
            this._toolTip.IsBalloon = true;
            this._toolTip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Warning;
            // 
            // _saveConfigurationDlg
            // 
            this._saveConfigurationDlg.DefaultExt = "bin";
            this._saveConfigurationDlg.FileName = "CS";
            this._saveConfigurationDlg.Filter = "(*.bin) | *.bin";
            // 
            // _openConfigurationDlg
            // 
            this._openConfigurationDlg.DefaultExt = "bin";
            this._openConfigurationDlg.Filter = "(*.bin) | *.bin";
            this._openConfigurationDlg.RestoreDirectory = true;
            this._openConfigurationDlg.Title = "������� ������� ��� CS";
            // 
            // _saveToXmlButton
            // 
            this._saveToXmlButton.AllowDrop = true;
            this._saveToXmlButton.AutoSize = true;
            this._saveToXmlButton.Location = new System.Drawing.Point(584, 437);
            this._saveToXmlButton.Name = "_saveToXmlButton";
            this._saveToXmlButton.Size = new System.Drawing.Size(136, 23);
            this._saveToXmlButton.TabIndex = 34;
            this._saveToXmlButton.Text = "��������� � HTML";
            this._saveToXmlButton.UseVisualStyleBackColor = true;
            this._saveToXmlButton.Click += new System.EventHandler(this._saveToXmlButton_Click);
            // 
            // _saveXmlDialog
            // 
            this._saveXmlDialog.Filter = "(*.html) | *.html";
            // 
            // CS_ConfigurationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(782, 485);
            this.Controls.Add(this._saveToXmlButton);
            this.Controls.Add(this._saveConfigBut);
            this.Controls.Add(this._loadConfigBut);
            this.Controls.Add(this._writeConfigBut);
            this.Controls.Add(this._readConfigBut);
            this.Controls.Add(this._statusStrip);
            this.Controls.Add(this._tabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "CS_ConfigurationForm";
            this.Text = "���100_ConfigurationForm";
            this.Load += new System.EventHandler(this.CS_ConfigurationForm_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.CSConfigurationForm_KeyUp);
            this._tabControl.ResumeLayout(false);
            this._konfDiscretPage.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._dataGridDiskretView)).EndInit();
            this._konfKISPage.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._dataGridKISView)).EndInit();
            this._konfRelePage.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._dataGridReleView)).EndInit();
            this._conPage.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this._statusStrip.ResumeLayout(false);
            this._statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl _tabControl;
        private System.Windows.Forms.TabPage _konfDiscretPage;
        private System.Windows.Forms.TabPage _konfKISPage;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView _dataGridDiskretView;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView _dataGridKISView;
        private System.Windows.Forms.StatusStrip _statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel _processLabel;
        private System.Windows.Forms.ToolStripProgressBar _exchangeProgressBar;
        private System.Windows.Forms.Button _saveConfigBut;
        private System.Windows.Forms.Button _loadConfigBut;
        private System.Windows.Forms.Button _writeConfigBut;
        private System.Windows.Forms.Button _readConfigBut;
        private System.Windows.Forms.ToolTip _toolTip;
        private System.Windows.Forms.DataGridViewTextBoxColumn _extDiskretNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _extDiskreTypeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _extDiskretTypeIndCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _extDiskretTcCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _extDiskretTvCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _extDiskretKomut1Col;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _extDiskretKomut2Col;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _extDiskretKomut3Col;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _extDiskretKomut4Col;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _extDiskretKomut5Col;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _extDiskretKomut6Col;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _extDiskretKomut3cCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _extDiskretBlock1Col;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _extDiskretBlock2Col;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _extDiskretBlock3Col;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _extDiskretBlock4Col;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _extDiskretBlock5Col;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _extDiskretBlock6Col;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _extDiskretBlock3cCol;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.SaveFileDialog _saveConfigurationDlg;
        private System.Windows.Forms.OpenFileDialog _openConfigurationDlg;
        private System.Windows.Forms.DataGridViewTextBoxColumn _extKisNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _extKisTypeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _extKisKonfCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tsCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tvCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _extKisKomut1Col;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _extKisKomut2Col;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _extKisKomut3Col;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _extKisKomut4Col;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _extKisKomut5Col;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _extKisKomut6Col;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _extKisKomut3cCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _extKisBlock1Col;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _extKisBlock2Col;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _extKisBlock3Col;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _extKisBlock4Col;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _extKisBlock5Col;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _extKisBlock6Col;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _extKisBlock3cCol;
        private System.Windows.Forms.TabPage _conPage;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.ComboBox _timeValidationSign;
        private System.Windows.Forms.TabPage _konfRelePage;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView _dataGridReleView;
        private System.Windows.Forms.DataGridViewTextBoxColumn _extReleNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _extReleTypeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _extReleTypeImpCol;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button _saveToXmlButton;
        private System.Windows.Forms.SaveFileDialog _saveXmlDialog;
        private System.Windows.Forms.MaskedTextBox _timeValidation;



    }
}