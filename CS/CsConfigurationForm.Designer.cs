namespace BEMN.CS
{
    partial class CsConfigurationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            this._tabControl = new System.Windows.Forms.TabControl();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.readFromDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readFromFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToHtmlItem = new System.Windows.Forms.ToolStripMenuItem();
            this._konfDiscretPage = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this._blockDiscretGrid = new System.Windows.Forms.DataGridView();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn4 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn5 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn6 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn7 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._commutationGrid = new System.Windows.Forms.DataGridView();
            this.nameD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this._dataGridDis�retView = new System.Windows.Forms.DataGridView();
            this._extDiskretNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._extDiskreTypeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._extDiskretTypeIndCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._extDiskretTcCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._extDiskretTvCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._konfKISPage = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._blockKisGrid = new System.Windows.Forms.DataGridView();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn15 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn16 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn17 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn18 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn19 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn20 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn21 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._commutationKisGrid = new System.Windows.Forms.DataGridView();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn8 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn9 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn10 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn11 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn12 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn13 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn14 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this._dataGridKISView = new System.Windows.Forms.DataGridView();
            this._extKisNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._extKisTypeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._extKisKonfCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tsCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tvCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._konfRelePage = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._dataGridReleView = new System.Windows.Forms.DataGridView();
            this._extReleNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._extReleTypeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._extReleTypeImpCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._conPage = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this._timeValidation = new System.Windows.Forms.MaskedTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this._timeValidationSign = new System.Windows.Forms.ComboBox();
            this._netConfigPage = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this._pass = new System.Windows.Forms.MaskedTextBox();
            this._addr = new System.Windows.Forms.MaskedTextBox();
            this._speed = new System.Windows.Forms.ComboBox();
            this._statusStrip = new System.Windows.Forms.StatusStrip();
            this._exchangeProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this._processLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._saveConfigBut = new System.Windows.Forms.Button();
            this._loadConfigBut = new System.Windows.Forms.Button();
            this._writeConfigBut = new System.Windows.Forms.Button();
            this._readConfigBut = new System.Windows.Forms.Button();
            this._toolTip = new System.Windows.Forms.ToolTip(this.components);
            this._saveConfigurationDlg = new System.Windows.Forms.SaveFileDialog();
            this._openConfigurationDlg = new System.Windows.Forms.OpenFileDialog();
            this._saveToXmlButton = new System.Windows.Forms.Button();
            this._saveXmlDialog = new System.Windows.Forms.SaveFileDialog();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this._tabControl.SuspendLayout();
            this.contextMenu.SuspendLayout();
            this._konfDiscretPage.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._blockDiscretGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._commutationGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._dataGridDis�retView)).BeginInit();
            this._konfKISPage.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._blockKisGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._commutationKisGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._dataGridKISView)).BeginInit();
            this._konfRelePage.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._dataGridReleView)).BeginInit();
            this._conPage.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this._netConfigPage.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this._statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // _tabControl
            // 
            this._tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tabControl.ContextMenuStrip = this.contextMenu;
            this._tabControl.Controls.Add(this._konfDiscretPage);
            this._tabControl.Controls.Add(this._konfKISPage);
            this._tabControl.Controls.Add(this._konfRelePage);
            this._tabControl.Controls.Add(this._conPage);
            this._tabControl.Controls.Add(this._netConfigPage);
            this._tabControl.Location = new System.Drawing.Point(0, 2);
            this._tabControl.Name = "_tabControl";
            this._tabControl.SelectedIndex = 0;
            this._tabControl.Size = new System.Drawing.Size(782, 429);
            this._tabControl.TabIndex = 0;
            // 
            // contextMenu
            // 
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.readFromDeviceItem,
            this.writeToDeviceItem,
            this.readFromFileItem,
            this.writeToFileItem,
            this.writeToHtmlItem});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(213, 114);
            this.contextMenu.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.contextMenu_ItemClicked);
            // 
            // readFromDeviceItem
            // 
            this.readFromDeviceItem.Name = "readFromDeviceItem";
            this.readFromDeviceItem.Size = new System.Drawing.Size(212, 22);
            this.readFromDeviceItem.Text = "��������� �� ����������";
            // 
            // writeToDeviceItem
            // 
            this.writeToDeviceItem.Name = "writeToDeviceItem";
            this.writeToDeviceItem.Size = new System.Drawing.Size(212, 22);
            this.writeToDeviceItem.Text = "�������� � ����������";
            // 
            // readFromFileItem
            // 
            this.readFromFileItem.Name = "readFromFileItem";
            this.readFromFileItem.Size = new System.Drawing.Size(212, 22);
            this.readFromFileItem.Text = "��������� �� �����";
            // 
            // writeToFileItem
            // 
            this.writeToFileItem.Name = "writeToFileItem";
            this.writeToFileItem.Size = new System.Drawing.Size(212, 22);
            this.writeToFileItem.Text = "��������� � ����";
            // 
            // writeToHtmlItem
            // 
            this.writeToHtmlItem.Name = "writeToHtmlItem";
            this.writeToHtmlItem.Size = new System.Drawing.Size(212, 22);
            this.writeToHtmlItem.Text = "��������� � HTML";
            // 
            // _konfDiscretPage
            // 
            this._konfDiscretPage.Controls.Add(this.groupBox3);
            this._konfDiscretPage.Location = new System.Drawing.Point(4, 22);
            this._konfDiscretPage.Name = "_konfDiscretPage";
            this._konfDiscretPage.Padding = new System.Windows.Forms.Padding(3);
            this._konfDiscretPage.Size = new System.Drawing.Size(774, 403);
            this._konfDiscretPage.TabIndex = 0;
            this._konfDiscretPage.Text = "��������";
            this._konfDiscretPage.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this._blockDiscretGrid);
            this.groupBox3.Controls.Add(this._commutationGrid);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this._dataGridDis�retView);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(3, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(768, 397);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "������������ ���������";
            // 
            // _blockDiscretGrid
            // 
            this._blockDiscretGrid.AllowUserToAddRows = false;
            this._blockDiscretGrid.AllowUserToDeleteRows = false;
            this._blockDiscretGrid.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.NullValue = null;
            this._blockDiscretGrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this._blockDiscretGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            this._blockDiscretGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._blockDiscretGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column8,
            this.dataGridViewCheckBoxColumn1,
            this.dataGridViewCheckBoxColumn2,
            this.dataGridViewCheckBoxColumn3,
            this.dataGridViewCheckBoxColumn4,
            this.dataGridViewCheckBoxColumn5,
            this.dataGridViewCheckBoxColumn6,
            this.dataGridViewCheckBoxColumn7});
            this._blockDiscretGrid.Dock = System.Windows.Forms.DockStyle.Left;
            this._blockDiscretGrid.Location = new System.Drawing.Point(535, 16);
            this._blockDiscretGrid.MinimumSize = new System.Drawing.Size(238, 378);
            this._blockDiscretGrid.MultiSelect = false;
            this._blockDiscretGrid.Name = "_blockDiscretGrid";
            this._blockDiscretGrid.RowHeadersVisible = false;
            this._blockDiscretGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._blockDiscretGrid.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this._blockDiscretGrid.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._blockDiscretGrid.Size = new System.Drawing.Size(238, 378);
            this._blockDiscretGrid.TabIndex = 5;
            this._blockDiscretGrid.Scroll += new System.Windows.Forms.ScrollEventHandler(this._blockGrid_Scroll);
            // 
            // Column8
            // 
            this.Column8.HeaderText = "Column8";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column8.Visible = false;
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.HeaderText = "1";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.Width = 30;
            // 
            // dataGridViewCheckBoxColumn2
            // 
            this.dataGridViewCheckBoxColumn2.HeaderText = "2";
            this.dataGridViewCheckBoxColumn2.Name = "dataGridViewCheckBoxColumn2";
            this.dataGridViewCheckBoxColumn2.Width = 30;
            // 
            // dataGridViewCheckBoxColumn3
            // 
            this.dataGridViewCheckBoxColumn3.HeaderText = "3";
            this.dataGridViewCheckBoxColumn3.Name = "dataGridViewCheckBoxColumn3";
            this.dataGridViewCheckBoxColumn3.Width = 30;
            // 
            // dataGridViewCheckBoxColumn4
            // 
            this.dataGridViewCheckBoxColumn4.HeaderText = "4";
            this.dataGridViewCheckBoxColumn4.Name = "dataGridViewCheckBoxColumn4";
            this.dataGridViewCheckBoxColumn4.Width = 30;
            // 
            // dataGridViewCheckBoxColumn5
            // 
            this.dataGridViewCheckBoxColumn5.HeaderText = "5";
            this.dataGridViewCheckBoxColumn5.Name = "dataGridViewCheckBoxColumn5";
            this.dataGridViewCheckBoxColumn5.Width = 30;
            // 
            // dataGridViewCheckBoxColumn6
            // 
            this.dataGridViewCheckBoxColumn6.HeaderText = "6";
            this.dataGridViewCheckBoxColumn6.Name = "dataGridViewCheckBoxColumn6";
            this.dataGridViewCheckBoxColumn6.Width = 30;
            // 
            // dataGridViewCheckBoxColumn7
            // 
            this.dataGridViewCheckBoxColumn7.HeaderText = "��";
            this.dataGridViewCheckBoxColumn7.Name = "dataGridViewCheckBoxColumn7";
            this.dataGridViewCheckBoxColumn7.Width = 30;
            // 
            // _commutationGrid
            // 
            this._commutationGrid.AllowUserToAddRows = false;
            this._commutationGrid.AllowUserToDeleteRows = false;
            this._commutationGrid.AllowUserToResizeRows = false;
            dataGridViewCellStyle3.NullValue = null;
            this._commutationGrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this._commutationGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            this._commutationGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._commutationGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameD,
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7});
            this._commutationGrid.Dock = System.Windows.Forms.DockStyle.Left;
            this._commutationGrid.Location = new System.Drawing.Point(322, 16);
            this._commutationGrid.MinimumSize = new System.Drawing.Size(213, 378);
            this._commutationGrid.MultiSelect = false;
            this._commutationGrid.Name = "_commutationGrid";
            this._commutationGrid.RowHeadersVisible = false;
            this._commutationGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._commutationGrid.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this._commutationGrid.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this._commutationGrid.Size = new System.Drawing.Size(213, 378);
            this._commutationGrid.TabIndex = 4;
            // 
            // nameD
            // 
            this.nameD.HeaderText = "Column8";
            this.nameD.Name = "nameD";
            this.nameD.ReadOnly = true;
            this.nameD.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.nameD.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.nameD.Visible = false;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "1";
            this.Column1.Name = "Column1";
            this.Column1.Width = 30;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "2";
            this.Column2.Name = "Column2";
            this.Column2.Width = 30;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "3";
            this.Column3.Name = "Column3";
            this.Column3.Width = 30;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "4";
            this.Column4.Name = "Column4";
            this.Column4.Width = 30;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "5";
            this.Column5.Name = "Column5";
            this.Column5.Width = 30;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "6";
            this.Column6.Name = "Column6";
            this.Column6.Width = 30;
            // 
            // Column7
            // 
            this.Column7.HeaderText = "��";
            this.Column7.Name = "Column7";
            this.Column7.Width = 30;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.SystemColors.Info;
            this.label4.Location = new System.Drawing.Point(535, -3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(238, 22);
            this.label4.TabIndex = 3;
            this.label4.Text = "����������";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(321, -3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(214, 22);
            this.label3.TabIndex = 2;
            this.label3.Text = "����������";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _dataGridDis�retView
            // 
            this._dataGridDis�retView.AllowUserToAddRows = false;
            this._dataGridDis�retView.AllowUserToDeleteRows = false;
            this._dataGridDis�retView.AllowUserToResizeRows = false;
            dataGridViewCellStyle5.NullValue = null;
            this._dataGridDis�retView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this._dataGridDis�retView.BackgroundColor = System.Drawing.SystemColors.Control;
            this._dataGridDis�retView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._dataGridDis�retView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._extDiskretNumberCol,
            this._extDiskreTypeCol,
            this._extDiskretTypeIndCol,
            this._extDiskretTcCol,
            this._extDiskretTvCol});
            this._dataGridDis�retView.Dock = System.Windows.Forms.DockStyle.Left;
            this._dataGridDis�retView.Location = new System.Drawing.Point(3, 16);
            this._dataGridDis�retView.MinimumSize = new System.Drawing.Size(319, 378);
            this._dataGridDis�retView.MultiSelect = false;
            this._dataGridDis�retView.Name = "_dataGridDis�retView";
            this._dataGridDis�retView.RowHeadersVisible = false;
            this._dataGridDis�retView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._dataGridDis�retView.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this._dataGridDis�retView.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this._dataGridDis�retView.Size = new System.Drawing.Size(319, 378);
            this._dataGridDis�retView.TabIndex = 1;
            // 
            // _extDiskretNumberCol
            // 
            this._extDiskretNumberCol.HeaderText = "�";
            this._extDiskretNumberCol.Name = "_extDiskretNumberCol";
            this._extDiskretNumberCol.ReadOnly = true;
            this._extDiskretNumberCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._extDiskretNumberCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._extDiskretNumberCol.Width = 30;
            // 
            // _extDiskreTypeCol
            // 
            this._extDiskreTypeCol.HeaderText = "���";
            this._extDiskreTypeCol.Name = "_extDiskreTypeCol";
            this._extDiskreTypeCol.Width = 70;
            // 
            // _extDiskretTypeIndCol
            // 
            this._extDiskretTypeIndCol.HeaderText = "��� ���.";
            this._extDiskretTypeIndCol.Name = "_extDiskretTypeIndCol";
            // 
            // _extDiskretTcCol
            // 
            this._extDiskretTcCol.HeaderText = "Tc (c)";
            this._extDiskretTcCol.Name = "_extDiskretTcCol";
            this._extDiskretTcCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._extDiskretTcCol.Width = 60;
            // 
            // _extDiskretTvCol
            // 
            this._extDiskretTvCol.HeaderText = "�� (�)";
            this._extDiskretTvCol.Name = "_extDiskretTvCol";
            this._extDiskretTvCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._extDiskretTvCol.Width = 60;
            // 
            // _konfKISPage
            // 
            this._konfKISPage.Controls.Add(this.groupBox2);
            this._konfKISPage.Location = new System.Drawing.Point(4, 22);
            this._konfKISPage.Name = "_konfKISPage";
            this._konfKISPage.Padding = new System.Windows.Forms.Padding(3);
            this._konfKISPage.Size = new System.Drawing.Size(774, 403);
            this._konfKISPage.TabIndex = 1;
            this._konfKISPage.Text = "����� ���";
            this._konfKISPage.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this._blockKisGrid);
            this.groupBox2.Controls.Add(this._commutationKisGrid);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this._dataGridKISView);
            this.groupBox2.Location = new System.Drawing.Point(8, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(667, 184);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "������������ ���";
            // 
            // _blockKisGrid
            // 
            this._blockKisGrid.AllowUserToAddRows = false;
            this._blockKisGrid.AllowUserToDeleteRows = false;
            this._blockKisGrid.AllowUserToResizeRows = false;
            dataGridViewCellStyle7.NullValue = null;
            this._blockKisGrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle7;
            this._blockKisGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            this._blockKisGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column10,
            this.dataGridViewCheckBoxColumn15,
            this.dataGridViewCheckBoxColumn16,
            this.dataGridViewCheckBoxColumn17,
            this.dataGridViewCheckBoxColumn18,
            this.dataGridViewCheckBoxColumn19,
            this.dataGridViewCheckBoxColumn20,
            this.dataGridViewCheckBoxColumn21});
            this._blockKisGrid.Dock = System.Windows.Forms.DockStyle.Left;
            this._blockKisGrid.Location = new System.Drawing.Point(480, 16);
            this._blockKisGrid.MinimumSize = new System.Drawing.Size(184, 165);
            this._blockKisGrid.MultiSelect = false;
            this._blockKisGrid.Name = "_blockKisGrid";
            this._blockKisGrid.RowHeadersVisible = false;
            this._blockKisGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._blockKisGrid.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this._blockKisGrid.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._blockKisGrid.Size = new System.Drawing.Size(184, 165);
            this._blockKisGrid.TabIndex = 7;
            // 
            // Column10
            // 
            this.Column10.HeaderText = "Column10";
            this.Column10.Name = "Column10";
            this.Column10.Visible = false;
            // 
            // dataGridViewCheckBoxColumn15
            // 
            this.dataGridViewCheckBoxColumn15.HeaderText = "1";
            this.dataGridViewCheckBoxColumn15.Name = "dataGridViewCheckBoxColumn15";
            this.dataGridViewCheckBoxColumn15.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn15.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewCheckBoxColumn15.Width = 25;
            // 
            // dataGridViewCheckBoxColumn16
            // 
            this.dataGridViewCheckBoxColumn16.HeaderText = "2";
            this.dataGridViewCheckBoxColumn16.Name = "dataGridViewCheckBoxColumn16";
            this.dataGridViewCheckBoxColumn16.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn16.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewCheckBoxColumn16.Width = 25;
            // 
            // dataGridViewCheckBoxColumn17
            // 
            this.dataGridViewCheckBoxColumn17.HeaderText = "3";
            this.dataGridViewCheckBoxColumn17.Name = "dataGridViewCheckBoxColumn17";
            this.dataGridViewCheckBoxColumn17.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn17.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewCheckBoxColumn17.Width = 25;
            // 
            // dataGridViewCheckBoxColumn18
            // 
            this.dataGridViewCheckBoxColumn18.HeaderText = "4";
            this.dataGridViewCheckBoxColumn18.Name = "dataGridViewCheckBoxColumn18";
            this.dataGridViewCheckBoxColumn18.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn18.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewCheckBoxColumn18.Width = 25;
            // 
            // dataGridViewCheckBoxColumn19
            // 
            this.dataGridViewCheckBoxColumn19.HeaderText = "5";
            this.dataGridViewCheckBoxColumn19.Name = "dataGridViewCheckBoxColumn19";
            this.dataGridViewCheckBoxColumn19.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn19.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewCheckBoxColumn19.Width = 25;
            // 
            // dataGridViewCheckBoxColumn20
            // 
            this.dataGridViewCheckBoxColumn20.HeaderText = "6";
            this.dataGridViewCheckBoxColumn20.Name = "dataGridViewCheckBoxColumn20";
            this.dataGridViewCheckBoxColumn20.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn20.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewCheckBoxColumn20.Width = 25;
            // 
            // dataGridViewCheckBoxColumn21
            // 
            this.dataGridViewCheckBoxColumn21.HeaderText = "��";
            this.dataGridViewCheckBoxColumn21.Name = "dataGridViewCheckBoxColumn21";
            this.dataGridViewCheckBoxColumn21.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn21.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewCheckBoxColumn21.Width = 30;
            // 
            // _commutationKisGrid
            // 
            this._commutationKisGrid.AllowUserToAddRows = false;
            this._commutationKisGrid.AllowUserToDeleteRows = false;
            this._commutationKisGrid.AllowUserToResizeRows = false;
            dataGridViewCellStyle9.NullValue = null;
            this._commutationKisGrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle9;
            this._commutationKisGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            this._commutationKisGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column9,
            this.dataGridViewCheckBoxColumn8,
            this.dataGridViewCheckBoxColumn9,
            this.dataGridViewCheckBoxColumn10,
            this.dataGridViewCheckBoxColumn11,
            this.dataGridViewCheckBoxColumn12,
            this.dataGridViewCheckBoxColumn13,
            this.dataGridViewCheckBoxColumn14});
            this._commutationKisGrid.Dock = System.Windows.Forms.DockStyle.Left;
            this._commutationKisGrid.Location = new System.Drawing.Point(296, 16);
            this._commutationKisGrid.MinimumSize = new System.Drawing.Size(184, 165);
            this._commutationKisGrid.MultiSelect = false;
            this._commutationKisGrid.Name = "_commutationKisGrid";
            this._commutationKisGrid.RowHeadersVisible = false;
            this._commutationKisGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._commutationKisGrid.RowsDefaultCellStyle = dataGridViewCellStyle10;
            this._commutationKisGrid.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._commutationKisGrid.Size = new System.Drawing.Size(184, 165);
            this._commutationKisGrid.TabIndex = 6;
            // 
            // Column9
            // 
            this.Column9.HeaderText = "Column9";
            this.Column9.Name = "Column9";
            this.Column9.Visible = false;
            // 
            // dataGridViewCheckBoxColumn8
            // 
            this.dataGridViewCheckBoxColumn8.HeaderText = "1";
            this.dataGridViewCheckBoxColumn8.Name = "dataGridViewCheckBoxColumn8";
            this.dataGridViewCheckBoxColumn8.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn8.Width = 25;
            // 
            // dataGridViewCheckBoxColumn9
            // 
            this.dataGridViewCheckBoxColumn9.HeaderText = "2";
            this.dataGridViewCheckBoxColumn9.Name = "dataGridViewCheckBoxColumn9";
            this.dataGridViewCheckBoxColumn9.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.dataGridViewCheckBoxColumn9.Width = 25;
            // 
            // dataGridViewCheckBoxColumn10
            // 
            this.dataGridViewCheckBoxColumn10.HeaderText = "3";
            this.dataGridViewCheckBoxColumn10.Name = "dataGridViewCheckBoxColumn10";
            this.dataGridViewCheckBoxColumn10.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn10.Width = 25;
            // 
            // dataGridViewCheckBoxColumn11
            // 
            this.dataGridViewCheckBoxColumn11.HeaderText = "4";
            this.dataGridViewCheckBoxColumn11.Name = "dataGridViewCheckBoxColumn11";
            this.dataGridViewCheckBoxColumn11.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn11.Width = 25;
            // 
            // dataGridViewCheckBoxColumn12
            // 
            this.dataGridViewCheckBoxColumn12.HeaderText = "5";
            this.dataGridViewCheckBoxColumn12.Name = "dataGridViewCheckBoxColumn12";
            this.dataGridViewCheckBoxColumn12.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn12.Width = 25;
            // 
            // dataGridViewCheckBoxColumn13
            // 
            this.dataGridViewCheckBoxColumn13.HeaderText = "6";
            this.dataGridViewCheckBoxColumn13.Name = "dataGridViewCheckBoxColumn13";
            this.dataGridViewCheckBoxColumn13.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn13.Width = 25;
            // 
            // dataGridViewCheckBoxColumn14
            // 
            this.dataGridViewCheckBoxColumn14.HeaderText = "��";
            this.dataGridViewCheckBoxColumn14.Name = "dataGridViewCheckBoxColumn14";
            this.dataGridViewCheckBoxColumn14.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn14.Width = 30;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.SystemColors.Info;
            this.label5.Location = new System.Drawing.Point(477, -4);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(182, 22);
            this.label5.TabIndex = 5;
            this.label5.Text = "����������";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(297, -3);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(183, 20);
            this.label6.TabIndex = 4;
            this.label6.Text = "����������";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _dataGridKISView
            // 
            this._dataGridKISView.AllowUserToAddRows = false;
            this._dataGridKISView.AllowUserToDeleteRows = false;
            this._dataGridKISView.AllowUserToResizeRows = false;
            dataGridViewCellStyle11.NullValue = null;
            this._dataGridKISView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle11;
            this._dataGridKISView.BackgroundColor = System.Drawing.SystemColors.Control;
            this._dataGridKISView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._extKisNumberCol,
            this._extKisTypeCol,
            this._extKisKonfCol,
            this._tsCol,
            this._tvCol});
            this._dataGridKISView.Dock = System.Windows.Forms.DockStyle.Left;
            this._dataGridKISView.Location = new System.Drawing.Point(3, 16);
            this._dataGridKISView.MinimumSize = new System.Drawing.Size(293, 165);
            this._dataGridKISView.MultiSelect = false;
            this._dataGridKISView.Name = "_dataGridKISView";
            this._dataGridKISView.RowHeadersVisible = false;
            this._dataGridKISView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._dataGridKISView.RowsDefaultCellStyle = dataGridViewCellStyle12;
            this._dataGridKISView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._dataGridKISView.Size = new System.Drawing.Size(293, 165);
            this._dataGridKISView.TabIndex = 1;
            // 
            // _extKisNumberCol
            // 
            this._extKisNumberCol.HeaderText = "�";
            this._extKisNumberCol.Name = "_extKisNumberCol";
            this._extKisNumberCol.ReadOnly = true;
            this._extKisNumberCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._extKisNumberCol.Width = 30;
            // 
            // _extKisTypeCol
            // 
            this._extKisTypeCol.HeaderText = "���";
            this._extKisTypeCol.Name = "_extKisTypeCol";
            this._extKisTypeCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._extKisTypeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this._extKisTypeCol.Width = 70;
            // 
            // _extKisKonfCol
            // 
            this._extKisKonfCol.HeaderText = "�����.";
            this._extKisKonfCol.Name = "_extKisKonfCol";
            this._extKisKonfCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._extKisKonfCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this._extKisKonfCol.Width = 70;
            // 
            // _tsCol
            // 
            this._tsCol.HeaderText = "Tc (�)";
            this._tsCol.Name = "_tsCol";
            this._tsCol.Width = 60;
            // 
            // _tvCol
            // 
            this._tvCol.HeaderText = "T� (�)";
            this._tvCol.Name = "_tvCol";
            this._tvCol.Width = 60;
            // 
            // _konfRelePage
            // 
            this._konfRelePage.Controls.Add(this.groupBox1);
            this._konfRelePage.Location = new System.Drawing.Point(4, 22);
            this._konfRelePage.Name = "_konfRelePage";
            this._konfRelePage.Size = new System.Drawing.Size(774, 403);
            this._konfRelePage.TabIndex = 3;
            this._konfRelePage.Text = "����";
            this._konfRelePage.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._dataGridReleView);
            this.groupBox1.Location = new System.Drawing.Point(8, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(212, 205);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "������������ ����";
            // 
            // _dataGridReleView
            // 
            this._dataGridReleView.AllowUserToAddRows = false;
            this._dataGridReleView.AllowUserToDeleteRows = false;
            this._dataGridReleView.AllowUserToResizeColumns = false;
            this._dataGridReleView.AllowUserToResizeRows = false;
            dataGridViewCellStyle13.NullValue = null;
            this._dataGridReleView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle13;
            this._dataGridReleView.BackgroundColor = System.Drawing.SystemColors.Control;
            this._dataGridReleView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._extReleNumberCol,
            this._extReleTypeCol,
            this._extReleTypeImpCol});
            this._dataGridReleView.Dock = System.Windows.Forms.DockStyle.Fill;
            this._dataGridReleView.Location = new System.Drawing.Point(3, 16);
            this._dataGridReleView.Name = "_dataGridReleView";
            this._dataGridReleView.RowHeadersVisible = false;
            this._dataGridReleView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._dataGridReleView.RowsDefaultCellStyle = dataGridViewCellStyle14;
            this._dataGridReleView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._dataGridReleView.Size = new System.Drawing.Size(206, 186);
            this._dataGridReleView.TabIndex = 2;
            // 
            // _extReleNumberCol
            // 
            this._extReleNumberCol.HeaderText = "�";
            this._extReleNumberCol.Name = "_extReleNumberCol";
            this._extReleNumberCol.ReadOnly = true;
            this._extReleNumberCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._extReleNumberCol.Width = 30;
            // 
            // _extReleTypeCol
            // 
            this._extReleTypeCol.HeaderText = "���";
            this._extReleTypeCol.Name = "_extReleTypeCol";
            this._extReleTypeCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._extReleTypeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this._extReleTypeCol.Width = 98;
            // 
            // _extReleTypeImpCol
            // 
            this._extReleTypeImpCol.HeaderText = "����. (�)";
            this._extReleTypeImpCol.Name = "_extReleTypeImpCol";
            this._extReleTypeImpCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._extReleTypeImpCol.Width = 67;
            // 
            // _conPage
            // 
            this._conPage.Controls.Add(this.groupBox5);
            this._conPage.Location = new System.Drawing.Point(4, 22);
            this._conPage.Name = "_conPage";
            this._conPage.Size = new System.Drawing.Size(774, 403);
            this._conPage.TabIndex = 2;
            this._conPage.Text = "��������� �����";
            this._conPage.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this._timeValidation);
            this.groupBox5.Controls.Add(this.label1);
            this.groupBox5.Controls.Add(this._timeValidationSign);
            this.groupBox5.Location = new System.Drawing.Point(8, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(271, 56);
            this.groupBox5.TabIndex = 6;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "��������� �����";
            // 
            // _timeValidation
            // 
            this._timeValidation.Location = new System.Drawing.Point(46, 27);
            this._timeValidation.Name = "_timeValidation";
            this._timeValidation.Size = new System.Drawing.Size(66, 20);
            this._timeValidation.TabIndex = 5;
            this._timeValidation.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._toolTip.SetToolTip(this._timeValidation, "�������� �� 0.1-1.4 �������� ������������� ");
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(119, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "������ � �����";
            // 
            // _timeValidationSign
            // 
            this._timeValidationSign.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._timeValidationSign.Items.AddRange(new object[] {
            "+",
            "-"});
            this._timeValidationSign.Location = new System.Drawing.Point(6, 27);
            this._timeValidationSign.Name = "_timeValidationSign";
            this._timeValidationSign.Size = new System.Drawing.Size(34, 21);
            this._timeValidationSign.TabIndex = 3;
            // 
            // _netConfigPage
            // 
            this._netConfigPage.Controls.Add(this.groupBox4);
            this._netConfigPage.Location = new System.Drawing.Point(4, 22);
            this._netConfigPage.Name = "_netConfigPage";
            this._netConfigPage.Padding = new System.Windows.Forms.Padding(3);
            this._netConfigPage.Size = new System.Drawing.Size(774, 403);
            this._netConfigPage.TabIndex = 4;
            this._netConfigPage.Text = "��������� �����";
            this._netConfigPage.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this._pass);
            this.groupBox4.Controls.Add(this._addr);
            this.groupBox4.Controls.Add(this._speed);
            this.groupBox4.Location = new System.Drawing.Point(6, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(168, 100);
            this.groupBox4.TabIndex = 0;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "������������ ����� RS-485";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 75);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(45, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "������";
            this.label8.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 49);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "�����";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "��������";
            // 
            // _pass
            // 
            this._pass.Location = new System.Drawing.Point(87, 72);
            this._pass.Name = "_pass";
            this._pass.ReadOnly = true;
            this._pass.Size = new System.Drawing.Size(75, 20);
            this._pass.TabIndex = 1;
            this._pass.Visible = false;
            // 
            // _addr
            // 
            this._addr.Location = new System.Drawing.Point(87, 46);
            this._addr.Name = "_addr";
            this._addr.Size = new System.Drawing.Size(75, 20);
            this._addr.TabIndex = 1;
            // 
            // _speed
            // 
            this._speed.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._speed.FormattingEnabled = true;
            this._speed.Location = new System.Drawing.Point(87, 19);
            this._speed.Name = "_speed";
            this._speed.Size = new System.Drawing.Size(75, 21);
            this._speed.TabIndex = 0;
            // 
            // _statusStrip
            // 
            this._statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._exchangeProgressBar,
            this._processLabel});
            this._statusStrip.Location = new System.Drawing.Point(0, 464);
            this._statusStrip.Name = "_statusStrip";
            this._statusStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this._statusStrip.Size = new System.Drawing.Size(782, 22);
            this._statusStrip.TabIndex = 6;
            this._statusStrip.Text = "statusStrip1";
            // 
            // _exchangeProgressBar
            // 
            this._exchangeProgressBar.Name = "_exchangeProgressBar";
            this._exchangeProgressBar.Size = new System.Drawing.Size(100, 16);
            this._exchangeProgressBar.Step = 1;
            // 
            // _processLabel
            // 
            this._processLabel.Name = "_processLabel";
            this._processLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // _saveConfigBut
            // 
            this._saveConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._saveConfigBut.Location = new System.Drawing.Point(468, 437);
            this._saveConfigBut.Name = "_saveConfigBut";
            this._saveConfigBut.Size = new System.Drawing.Size(122, 23);
            this._saveConfigBut.TabIndex = 10;
            this._saveConfigBut.Text = "��������� � ����";
            this.toolTip1.SetToolTip(this._saveConfigBut, "��������� ������������ � ���� (CTRL+S)");
            this._saveConfigBut.UseVisualStyleBackColor = true;
            this._saveConfigBut.Click += new System.EventHandler(this._saveConfigBut_Click);
            // 
            // _loadConfigBut
            // 
            this._loadConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._loadConfigBut.Location = new System.Drawing.Point(340, 437);
            this._loadConfigBut.Name = "_loadConfigBut";
            this._loadConfigBut.Size = new System.Drawing.Size(122, 23);
            this._loadConfigBut.TabIndex = 9;
            this._loadConfigBut.Text = "��������� �� �����";
            this.toolTip1.SetToolTip(this._loadConfigBut, "��������� ������������ �� ����� (CTRL+O)");
            this._loadConfigBut.UseVisualStyleBackColor = true;
            this._loadConfigBut.Click += new System.EventHandler(this._loadConfigBut_Click);
            // 
            // _writeConfigBut
            // 
            this._writeConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._writeConfigBut.Location = new System.Drawing.Point(176, 437);
            this._writeConfigBut.Name = "_writeConfigBut";
            this._writeConfigBut.Size = new System.Drawing.Size(158, 23);
            this._writeConfigBut.TabIndex = 8;
            this._writeConfigBut.Text = "�������� � ����������";
            this.toolTip1.SetToolTip(this._writeConfigBut, "�������� ������������ � ���������� (CTRL+W)");
            this._writeConfigBut.UseVisualStyleBackColor = true;
            this._writeConfigBut.Click += new System.EventHandler(this._writeConfigBut_Click);
            // 
            // _readConfigBut
            // 
            this._readConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._readConfigBut.Location = new System.Drawing.Point(12, 437);
            this._readConfigBut.Name = "_readConfigBut";
            this._readConfigBut.Size = new System.Drawing.Size(158, 23);
            this._readConfigBut.TabIndex = 7;
            this._readConfigBut.Text = "��������� �� ����������";
            this.toolTip1.SetToolTip(this._readConfigBut, "��������� ������������ �� ���������� (CTRL+R)");
            this._readConfigBut.UseVisualStyleBackColor = true;
            this._readConfigBut.Click += new System.EventHandler(this._readConfigBut_Click);
            // 
            // _toolTip
            // 
            this._toolTip.IsBalloon = true;
            this._toolTip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Warning;
            // 
            // _saveConfigurationDlg
            // 
            this._saveConfigurationDlg.DefaultExt = "bin";
            this._saveConfigurationDlg.FileName = "CS";
            this._saveConfigurationDlg.Filter = "(*.xml) | *.xml";
            // 
            // _openConfigurationDlg
            // 
            this._openConfigurationDlg.DefaultExt = "bin";
            this._openConfigurationDlg.Filter = "(*xml)|*.xml|(*.bin)|*.bin";
            this._openConfigurationDlg.RestoreDirectory = true;
            this._openConfigurationDlg.Title = "������� ������� ��� CS";
            // 
            // _saveToXmlButton
            // 
            this._saveToXmlButton.AllowDrop = true;
            this._saveToXmlButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._saveToXmlButton.AutoSize = true;
            this._saveToXmlButton.Location = new System.Drawing.Point(596, 437);
            this._saveToXmlButton.Name = "_saveToXmlButton";
            this._saveToXmlButton.Size = new System.Drawing.Size(136, 23);
            this._saveToXmlButton.TabIndex = 34;
            this._saveToXmlButton.Text = "��������� � HTML";
            this._saveToXmlButton.UseVisualStyleBackColor = true;
            this._saveToXmlButton.Click += new System.EventHandler(this._saveToXmlButton_Click);
            // 
            // _saveXmlDialog
            // 
            this._saveXmlDialog.Filter = "(*.html) | *.html";
            // 
            // CsConfigurationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(782, 486);
            this.Controls.Add(this._saveToXmlButton);
            this.Controls.Add(this._saveConfigBut);
            this.Controls.Add(this._loadConfigBut);
            this.Controls.Add(this._writeConfigBut);
            this.Controls.Add(this._readConfigBut);
            this.Controls.Add(this._statusStrip);
            this.Controls.Add(this._tabControl);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(798, 900);
            this.MinimumSize = new System.Drawing.Size(798, 525);
            this.Name = "CsConfigurationForm";
            this.Text = "���100_ConfigurationForm";
            this.Activated += new System.EventHandler(this.CsConfigurationForm_Activated);
            this.Load += new System.EventHandler(this.CS_ConfigurationForm_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.CSConfigurationForm_KeyUp);
            this._tabControl.ResumeLayout(false);
            this.contextMenu.ResumeLayout(false);
            this._konfDiscretPage.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._blockDiscretGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._commutationGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._dataGridDis�retView)).EndInit();
            this._konfKISPage.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._blockKisGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._commutationKisGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._dataGridKISView)).EndInit();
            this._konfRelePage.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._dataGridReleView)).EndInit();
            this._conPage.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this._netConfigPage.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this._statusStrip.ResumeLayout(false);
            this._statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl _tabControl;
        private System.Windows.Forms.TabPage _konfDiscretPage;
        private System.Windows.Forms.TabPage _konfKISPage;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView _dataGridDis�retView;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView _dataGridKISView;
        private System.Windows.Forms.StatusStrip _statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel _processLabel;
        private System.Windows.Forms.ToolStripProgressBar _exchangeProgressBar;
        private System.Windows.Forms.Button _saveConfigBut;
        private System.Windows.Forms.Button _loadConfigBut;
        private System.Windows.Forms.Button _writeConfigBut;
        private System.Windows.Forms.Button _readConfigBut;
        private System.Windows.Forms.ToolTip _toolTip;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.SaveFileDialog _saveConfigurationDlg;
        private System.Windows.Forms.OpenFileDialog _openConfigurationDlg;
        private System.Windows.Forms.TabPage _conPage;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.ComboBox _timeValidationSign;
        private System.Windows.Forms.TabPage _konfRelePage;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView _dataGridReleView;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button _saveToXmlButton;
        private System.Windows.Forms.SaveFileDialog _saveXmlDialog;
        private System.Windows.Forms.MaskedTextBox _timeValidation;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem readFromDeviceItem;
        private System.Windows.Forms.ToolStripMenuItem writeToDeviceItem;
        private System.Windows.Forms.ToolStripMenuItem readFromFileItem;
        private System.Windows.Forms.ToolStripMenuItem writeToFileItem;
        private System.Windows.Forms.ToolStripMenuItem writeToHtmlItem;
        private System.Windows.Forms.DataGridView _blockDiscretGrid;
        private System.Windows.Forms.DataGridView _commutationGrid;
        private System.Windows.Forms.DataGridView _commutationKisGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn _extKisNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _extKisTypeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _extKisKonfCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tsCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tvCol;
        private System.Windows.Forms.DataGridView _blockKisGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn4;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn5;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn6;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameD;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column4;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column5;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column6;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn15;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn16;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn17;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn18;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn19;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn20;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn8;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn9;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn10;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn11;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn12;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn13;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn _extReleNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _extReleTypeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _extReleTypeImpCol;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _extDiskretNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _extDiskreTypeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _extDiskretTypeIndCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _extDiskretTcCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _extDiskretTvCol;
        private System.Windows.Forms.TabPage _netConfigPage;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MaskedTextBox _pass;
        private System.Windows.Forms.MaskedTextBox _addr;
        private System.Windows.Forms.ComboBox _speed;
    }
}