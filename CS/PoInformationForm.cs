﻿using System;
using System.Drawing;
using System.Windows.Forms;
using AssemblyResourses;
using BEMN.CS.Structures;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Interfaces;

namespace BEMN.CS
{
    public partial class PoInformationForm : Form, IFormView
    {
        #region [Private fields]
        private const string UPDATE_FAIL = "Невозможно обновить ПО. Проверте связь.";
        private const string UNICON_CLOSE = "Для обновления ПО \"Уникон\" будет закрыт\nПродолжить?";
        private const string PO_UPDATE = "Обновление ПО";
        private const string VERSION_PO = "Версия ПО - ";
        private readonly MemoryEntity<VersionStruct> _version;
        private readonly MemoryEntity<OneWordStruct> _update;
        private CS _device;
        #endregion [Private fields]


        #region [Ctor's]
        public PoInformationForm()
        {
            this.InitializeComponent();
        }

        public PoInformationForm(CS device)
        {
            this.InitializeComponent();
            this._device = device;
            this._version = device.Version;
            this._update = new MemoryEntity<OneWordStruct>(PO_UPDATE, this._device, 0x0E);
            this._update.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.UpdateOk);
            this._update.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.UpdateFail);
        }
        #endregion [Ctor's]


        #region [Memory Entity events]
        private void UpdateFail()
        {
            MessageBox.Show(UPDATE_FAIL);
        }

        private void UpdateOk()
        {
            Framework.Framework.MainWindow.Close();
        }
        
        #endregion [Memory Entity events]

        #region [Event's handlers]
        private void MdoPoInformationForm_Load(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._versionLabel.Text = VERSION_PO + this._version.Value.VersionFull;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show(UNICON_CLOSE, PO_UPDATE, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                this._update.Value = new OneWordStruct { Word = 1 };
                this._update.SaveStruct();
            }
        }
        #endregion [Event's handlers]


        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(CS); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(PoInformationForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.Calibrate; }
        }

        public string NodeName
        {
            get { return "Информация ПО"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion


    }
}
