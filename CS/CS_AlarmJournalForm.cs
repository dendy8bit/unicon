using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using AssemblyResourses;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Interfaces;

namespace BEMN.CS
{
    public partial class CsAlarmJournalForm : Form, IFormView
    {
        private CS _device;
        private bool _connectingErrors = false;

        public CsAlarmJournalForm()
        {
            this.InitializeComponent();
        }
        public CsAlarmJournalForm(CS device)
        {
            this.InitializeComponent();
            this._device = device;
            this._device.AlarmJournalInfoLoadOk += HandlerHelper.CreateHandler(this, this.ReadJournalInfoOk);
            this._device.AlarmJournalInfoLoadFail += HandlerHelper.CreateHandler(this, this.ReadJournalFail);

            this._device.AllAlarmJournalLoadOk += HandlerHelper.CreateHandler(this, this.ReadJournal);
            this._device.AlarmJournalLoadOk += HandlerHelper.CreateHandler(this, this._journalProgress.PerformStep);
        }
                
        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(BEMN.CS.CS); }
        }

        public bool Multishow { get; private set; }
        
        public Type ClassType
        {
            get { return typeof(CsAlarmJournalForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.ja; }
        }

        public string NodeName
        {
            get { return "������ �������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        private void _readBut_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._serializeBut.Enabled = false;
            this._deserializeBut.Enabled = false;
            this._journalCntLabel.Text = "���� ������...";
            this._device.LoadJournalInfo();
        }
        
        private void ReadJournalInfoOk()
        {
            this._connectingErrors = false;
            this._serializeBut.Enabled = false;
            this._deserializeBut.Enabled = false;
            this._readBut.Enabled = false;
            this._journalGrid.Rows.Clear();
            this._device.ReadJournal();
            this._journalProgress.Value = 0;
            this._journalProgress.Maximum = this._device.Journal.SlotsCount;
            this._journalProgress.Step = 1;
        }

        private void _serializeBut_Click(object sender, EventArgs e)
        {
            DataTable table = this.CreateAlarmJournalTable();

            List<object> row = new List<object>();

            for (int i = 0; i < this._journalGrid.Rows.Count; i++)
            {
                row.Clear();
                for (int j = 0; j < this._journalGrid.Columns.Count; j++)
                {
                    row.Add(this._journalGrid[j, i].Value);
                }
                table.Rows.Add(row.ToArray());
            }
            if (DialogResult.OK == this._saveSysJournalDlg.ShowDialog())
            {
                this._journalProgress.Value = 0;
                table.WriteXml(this._saveSysJournalDlg.FileName);
                this._journalCntLabel.Text = "���� " + this._saveSysJournalDlg.FileName + " ��������";
                this._journalProgress.Value = this._journalProgress.Maximum;
            }
        }

        private void _deserializeBut_Click(object sender, EventArgs e)
        {
            DataTable table = this.CreateAlarmJournalTable();

            if (DialogResult.OK == this._openSysJounralDlg.ShowDialog())
            {
                try
                {
                    table.ReadXml(this._openSysJounralDlg.FileName);
                }
                catch (System.IO.FileLoadException exc)
                {
                    this._journalCntLabel.Text = "���� " + System.IO.Path.GetFileName(exc.FileName) + " �� �������� ������ ������� ������ ��� 100 ��� ���������";
                    return;
                }
                this._journalGrid.Rows.Clear();
                this._journalProgress.Value = 0;
                this._journalCntLabel.Text = "���� " + this._openSysJounralDlg.FileName + " ��������";
                this._journalProgress.Value = this._journalProgress.Maximum;
            }

            List<object> row = new List<object>();
            for (int i = 0; i < table.Rows.Count; i++)
            {
                row.Clear();
                row.AddRange(table.Rows[i].ItemArray);
                this._journalGrid.Rows.Add(row.ToArray());

            }
        }
        
        private  DataTable CreateAlarmJournalTable()
        {
            DataTable table = new DataTable("���_������_�������");
            for (int j = 0; j < this._journalGrid.Columns.Count; j++)
            {
                table.Columns.Add(this._journalGrid.Columns[j].Name);
            }
            return table;
        }

        private void CS_AlarmJournalForm_Load(object sender, EventArgs e)
        {
            this.StartRead();
        }
        
        public void ReadJournal() 
        {
            try
            {
                if (this._connectingErrors == false)
                {
                    this._journalCntLabel.Text = "������ ������� ���������. ������� � �������: " + this._device.Journal.Records.Length.ToString();
                    this._journalProgress.Value = this._journalProgress.Maximum;
                }
                else
                {
                    this._journalCntLabel.Text = "������ ��������� ���������.";
                }
                //this._journalCntLabel.Text = "������� � �������: "+this._device.Journal.Records.Length.ToString();
                int row = 1;
                foreach (var item in this._device.Journal.Records)
                {
                    this._journalGrid.Rows.Add(row, item.Time, item.Message, item.Value, item.Discret_1, item.Discret_2, item.Discret_3, item.Discret_4, item.Relay, item.Kis);
                    row++;
                }
                if (this._journalGrid.Rows.Count == 0)
                {
                    MessageBox.Show("������ ����.");
                }
                else
                {
                    MessageBox.Show("������ ��������. ������� � �������: " + this._device.Journal.Records.Length.ToString());
                }
                this._serializeBut.Enabled = true;
                this._deserializeBut.Enabled = true;
                this._readBut.Enabled = true;
            }
            catch 
            {
            }
        }

        public void ReadJournalFail()
        {
            MessageBox.Show("������ ������ �������.");
            this._readBut.Enabled = true;
        }
    }
}