using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Xml.Serialization;
using BEMN.CS.Data;
using BEMN.CS.Utils;
using BEMN.Forms;
using BEMN.MBServer;

namespace BEMN.CS
{
    public class DiskretItem
    {
        public const int LENGTH = 4;
        private ushort[] _values;

        public DiskretItem()
        {
            SetBuffer(new ushort[LENGTH]);
        }

        private string _name;

        [XmlAttribute("��������")]
        [DisplayName("��������")]
        [Category("��������� ���")]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public override string ToString()
        {
            return "���������� �����";
        }

        public DiskretItem(ushort[] buffer)
        {
            SetBuffer(buffer);
        }

        public void SetBuffer(ushort[] buffer)
        {
            if (LENGTH != buffer.Length)
            {
                throw new ArgumentOutOfRangeException("buffer.Length", buffer.Length,
                                                      "Buffer of DiskretItem must be " + LENGTH);
            }
            _values = buffer;
        }

        [XmlAttribute("���������")]
        [DisplayName("���������")]
        [Description("��� ��������� ����������� �����")]
        [Category("��������� ���")]
        [TypeConverter(typeof(DiskretIndicationTypeConverter))]
        public string Indication
        {
            get
            {
                byte index = Common.HIBYTE(_values[3]);
                if (index >= Strings.diskretIndication.Count)
                {
                    index = (byte)(Strings.diskretIndication.Count - 1);
                }
                return Strings.diskretIndication[index];
            }
            set
            {
                try
                {
                    _values[3] =
                        Common.TOWORD((byte)Strings.diskretIndication.IndexOf(value), Common.LOBYTE(_values[3]));
                }
                catch (ArgumentOutOfRangeException)
                {
                    _values[3] =
                        Common.TOWORD((byte)(Strings.diskretIndication.Count - 1), Common.LOBYTE(_values[3]));
                }
            }
        }

        [XmlAttribute("���")]
        [DisplayName("���")]
        [Description("��� ����������� �����")]
        [Category("��������� ���")]
        [TypeConverter(typeof(DiskretInTypeConverter))]
        public string Mode
        {
            get
            {
                byte index = Common.LOBYTE(_values[3]);
                if (index >= Strings.diskretIn.Count)
                {
                    index = (byte)(Strings.diskretIn.Count - 1);
                }
                return Strings.diskretIn[index];
            }

            set
            {
                try
                {
                    _values[3] = Common.TOWORD(Common.HIBYTE(_values[3]), (byte)Strings.diskretIn.IndexOf(value));
                }
                catch (ArgumentOutOfRangeException)
                {
                    _values[3] = Common.TOWORD(Common.HIBYTE(_values[3]), (byte)(Strings.diskretIn.Count - 1));
                }
            }
        }

        [XmlAttribute("�����")]
        [DisplayName("�����")]
        [Description("����� �������� ������")]
        [Category("��������� ���")]
        public double FrontTime
        {
            get { return Measuring.GetTime(_values[0]); }
            set { _values[0] = Measuring.SetTime(value); }
        }

        [XmlAttribute("����")]
        [DisplayName("����")]
        [Description("����� �������� �����")]
        [Category("��������� ���")]
        public double DeclineTime
        {
            get { return Measuring.GetTime(_values[1]); }
            set { _values[1] = Measuring.SetTime(value); }
        }

        [XmlAttribute("��������")]
        [DisplayName("��������")]
        [Description("������ � ������� ����")]
        [Category("������� ���")]
        [Editor(typeof(RussianCollectionEditor), typeof(UITypeEditor))]
        public ushort[] Values
        {
            get { return _values; }
            set { _values = value; }
        }

        private bool[] _params;
        [XmlElement("���������")]
       // [XmlAttribute("���������")]
        public bool[] Params
        {
            get { return _params; }
            set { _params = value; }
        }
    }
}