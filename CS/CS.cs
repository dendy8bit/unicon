using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Xml;
using BEMN.Devices;
using BEMN.MBServer;
using BEMN.MBServer.Queries;
using System.Xml.Serialization;
using AssemblyResourses;
using BEMN.CS.Structures;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Framework.BusinessLogic;
using BEMN.Interfaces;

namespace BEMN.CS
{
    //������ ��������
    public class CS : Device, IDeviceView, IDeviceVersion
    {
        #region ������ �������
        private AlarmJournal journal;

        public AlarmJournal Journal
        {
            get { return this.journal; }
        }
        public event Handler AlarmJournalLoadOk;
        public event Handler AlarmJournalLoadFail;

        public event Handler AllAlarmJournalLoadOk;
        public event Handler AllAlarmJournalLoadFail;

        public event Handler AlarmJournalInfoLoadOk;
        public event Handler AlarmJournalInfoLoadFail;
        
        public event Handler CvitCommandSaveOk;
        public event Handler CvitCommandSaveFail;
   
        private slot _alarmJounalSettings = new slot(0x600,0x604);
        private slot[] _alarmJounal;

        public class AlarmJournalRecord
        {
            public static readonly int RECORD_LENGTH = 32;
            private ushort[] diskret = new ushort[4];
            private ushort[] rele = new ushort[4];
            private ushort[] kis = new ushort[4];
            private const int kisLength = 6;
            private ushort journalType;
            private ushort journalVol;
            private ushort[] time = new ushort[8];
            private string[] discrets;
            private string relay = string.Empty;
            private string kiss = string.Empty;

            private List<string> messList = new List<string>
            {
                "���",
                "���������� �����",
                "����",
                "������� ��������",
                "����� �������",
                "������������",
                "������������ �� ����������� �����",
                "�������� ������������",
                "�������������",
                "������ ���1 ����������",
                "������ ���2 ����������",
                "������ ���� ����������",
                "������ �����. ������ ����������",
                "������������ ��������",
                "������ RTCLOCK",
                "������ ���1 ��������",
                "������ ���2 ��������",
                "������ ���� ��������",
                "������ �����. ������ ��������",
                "������� ������������ ���� ���",
                "���������� ������������ ���� ���",
                "���������� ���������"
            };

            public string Time 
            {
                get 
                {
                    return this.time[2].ToString("00") + "." + this.time[1].ToString("00") + "." + this.time[0].ToString("00") + 
                        "  " + this.time[4].ToString("00") + ":" + this.time[5].ToString("00") + ":" + this.time[6].ToString("00") + 
                        "." + this.time[7].ToString("000");
                }
            }
            public string Message { get { return this.messList.Count > this.journalType? this.messList[this.journalType]: "������"; } }
            public string Discret_1 { get { return this.discrets[0]; } }
            public string Discret_2 { get { return this.discrets[1]; } }
            public string Discret_3 { get { return this.discrets[2]; } }
            public string Discret_4 { get { return this.discrets[3]; } }
            public string Relay { get { return this.relay; } }
            public string Kis { get { return this.kiss; } }
            public string Value { get { return this.journalVol.ToString(); } }

            public void Initialize(ushort[] values) 
            {
                Array.Copy(values, 0, this.diskret, 0, this.diskret.Length);
                Array.Copy(values, 4, this.rele, 0, this.rele.Length);
                Array.Copy(values, 8, this.kis, 0, this.kis.Length);
                //this.reserve1 = values[12];
                this.journalType = values[13];
                this.journalVol = (ushort)(values[14] + 1);
                //this.reserve3 = values[15];
                Array.Copy(values, 16, this.time, 0, this.time.Length);
                if (this.time[7] == 420)
                {
                }
                this.PrepareDiscrets();
                this.PrepareRelay();
                this.PrepareKiss();
            }

            private void PrepareDiscrets() 
            {
                this.discrets = new string[4];

                BitArray aBits = new BitArray(new byte[]
                {
                    Common.LOBYTE(this.diskret[0]), Common.HIBYTE(this.diskret[0]), Common.LOBYTE(this.diskret[1]),
                    Common.HIBYTE(this.diskret[1]), Common.LOBYTE(this.diskret[2])
                });
                string sBits = this.BitsToString(aBits);

                int index = 0;
                for (int i = 0; i < sBits.Length; i++)
                {
                    if (i % 10 == 0 && i != 0)
                    {
                        index++;
                        this.discrets[index] += sBits[i];
                    }
                    else
                    {
                        this.discrets[index] += sBits[i];
                    }
                }
            }

            private void PrepareRelay()
            {
                byte rBits = Common.LOBYTE(this.rele[0]);
                BitArray aBits = new BitArray(new byte[] { rBits });
                this.relay = this.AddZeroMass(aBits, 8);
            }

            private string AddZeroMass(BitArray bits, int numCount)
            {
                string ret = "";
                for (int i = 0; i < bits.Count; i++)
                {
                    if (bits[i])
                    {
                        ret += " 1  ";
                    }
                    else
                    {
                        ret += " 0  ";
                    }
                }
                if (ret.Length < numCount*5)
                {
                    for (int i = ret.Length; i < numCount; i++)
                    {
                        ret += " 0  ";
                    }
                }
                if (ret.Length > numCount*5)
                {
                    ret = ret.Remove((numCount*5)-1);
                    ret = ret.Remove(numCount*5);
                }
                return ret;
            }

            private void PrepareKiss()
            {
                byte[] kBits = Common.TOBYTES(this.kis, false);
                this.kiss += kBits[0].ToString("0");
                char[] temp;
                for (int i = 1; i < kisLength; i++)
                {
                    string tBits = kBits[i].ToString("0");
                    temp = tBits.ToCharArray();
                    Array.Reverse(temp);
                    tBits = new string(temp);
                    this.kiss += " " + tBits;
                }
                temp = this.kiss.ToCharArray();
                //Array.Reverse(temp);
                this.kiss = new string(temp);
            }

            private string BitsToString(BitArray bits)
            {
                string ret = "";
                for (int i = 0; i < bits.Count; i++)
                {
                    if (bits[i])
                    {
                        ret += "1";
                    }
                    else
                    {
                        ret += "0";
                    }
                }
                return ret;
            }
        }
        
        private class AlarmJournalInfo 
        {
            //private int _journalLength;
            //public int JournalLength
            //{
            //    get { return this._journalLength; }
            //}

            private int _recordsCount;
            public int RecordsCount
            {
                get { return this._recordsCount; }
            }

            private int _currentRecord;
            public int CurrentRecord
            {
                get { return this._currentRecord; }
            }

            public AlarmJournalInfo(ushort[] values)
            {
                this._currentRecord = values[0];
                this._recordsCount = values[1];
                //this._journalLength = values[3];
            }
        }

        public class AlarmJournal 
        {
            private AlarmJournalInfo jInfo;
            AlarmJournalRecord[] records;
            int lastValIndex;
            ushort[] values;
            public Handler RecordsReady;
            public AlarmJournalRecord[] Records
            {
                get { return this.records; }
            }
            private int slotsCount;

            public int SlotsCount
            {
                get { return this.slotsCount; }
            }


            public AlarmJournal(ushort[] journalInfo, ref slot[] slots)
            {
                this.jInfo = new AlarmJournalInfo(journalInfo);
                this.records = new AlarmJournalRecord[this.jInfo.RecordsCount];
                slots = this.GetSlots(0x0610);
                this.slotsCount = slots.Length;
            }

            private slot[] GetSlots(ushort start)
            {
                int slotLength = 64;

                int arrayLengthW = AlarmJournalRecord.RECORD_LENGTH*this.records.Length;
                this.values = new ushort[arrayLengthW];
                int arrayLength =  arrayLengthW / slotLength;
                int lastSlotLength = arrayLengthW % slotLength;
                if (lastSlotLength > 0)
                {
                    arrayLength++;
                }
                slot[] slots = new slot[arrayLength];
                ushort startAddr = start;
                if (lastSlotLength == 0)
                {
                    for (int i = 0; i < arrayLength; i++)
                    {
                        slots[i] = new slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                }
                else
                {
                    for (int i = 0; i < arrayLength - 1; i++)
                    {
                        slots[i] = new slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                    slots[arrayLength - 1] = new slot(startAddr, (ushort)(startAddr + lastSlotLength));
                }
                return slots;
            }

            public void AddValues(ushort[] val)
            {
                Array.Copy(val, 0, this.values, this.lastValIndex, val.Length);
                this.lastValIndex += val.Length;
                if (this.lastValIndex == this.values.Length)
                {
                    this.InitialiseRecords();
                    this.lastValIndex = 0;
                    if (this.RecordsReady!=null)
                    {
                        this.RecordsReady(null);
                    }
                }
            }
            private void InitialiseRecords() 
            {
                int ind = 0;
                for (int i = 0; i < this.records.Length; i++)
                {
                    ushort[] rec = new ushort[AlarmJournalRecord.RECORD_LENGTH];
                    Array.Copy(this.values, ind, rec, 0, rec.Length);
                    this.records[i] = new AlarmJournalRecord();
                    this.records[i].Initialize(rec);
                    ind += rec.Length;
                }

                if (this.jInfo.CurrentRecord != this.jInfo.RecordsCount)
                {
                    AlarmJournalRecord[] buffer = new AlarmJournalRecord[this.jInfo.RecordsCount];
                    int numBeforeZero = this.jInfo.RecordsCount - this.jInfo.CurrentRecord;
                    int numAfterZero = this.jInfo.CurrentRecord;
                    int startIndex = this.jInfo.RecordsCount - numBeforeZero;
                    Array.Copy(this.records, startIndex, buffer, 0, numBeforeZero);
                    Array.Copy(this.records, 0, buffer, numBeforeZero, numAfterZero);
                    this.records = buffer;
                }
                Array.Reverse(this.records);
            }
        }


        public void ReadJournal() 
        {
            this.journal = new AlarmJournal(this._alarmJounalSettings.Value,ref this._alarmJounal);
            this.journal.RecordsReady += new Handler(this.CS_RecordsReady);
            for (int i = 0; i < this._alarmJounal.Length; i++)
            {
                LoadSlot(DeviceNumber, this._alarmJounal[i], "LoadJournalSlot" + DeviceNumber + ":" + i, this);
            }
        }

        void CS_RecordsReady(object sender)
        {
            if (this.AllAlarmJournalLoadOk!=null)
            {
                this.AllAlarmJournalLoadOk(this);
            }
        }
        ////////////////////////////////////////////////////////////////
        public void LoadJournalInfo() 
        {
            LoadSlot(DeviceNumber, this._alarmJounalSettings, "LoadJournalInfo" + DeviceNumber.ToString(), this);
        }
        #endregion

        #region �������������

        public CS()
        {
            HaveVersion = true;
        }

        public CS(Modbus mb)
        {
            this.MB = mb;
            HaveVersion = true;
            this.Init();
        }

        public void Init()
        {
            this._version = new MemoryEntity<VersionStruct>("version", this, 0x10);//"��� ������"
            this._version.AllReadOk += HandlerHelper.CreateReadArrayHandler(this.VersionLoaded);
            this._version.AllReadFail += HandlerHelper.CreateReadArrayHandler(() =>
            {
                TreeManager.OnDeviceVersionLoadFail(this);
            });

            this._configuration = new MemoryEntity<ConfigurationStruct>("������������ ���100", this, 0x300);
            this._configurationNew = new MemoryEntity<ConfigurationStructNew>("������������New ���100", this, 0x300);
            this._command = new MemoryEntity<OneWordStruct>("���������� ��������", this, 0x0D);
        }

        [XmlIgnore]
        public override Modbus MB
        {
            get { return mb; }
            set
            {
                mb = value;
                if (null != mb)
                {
                    mb.CompleteExchange += this.mb_CompleteExchange;
                }
            }
        }

        public override string DeviceVersion
        {
            get { return base.DeviceVersion; }
            set
            {
                string deviceVersion = this.SwitchVersion(value);
                string[] vers = deviceVersion.Split('.');
                if (this._version != null && vers.Length == 3)
                {
                    this._version.Value.SetVersion(Convert.ToUInt16(vers[0]), Convert.ToUInt16(vers[1]),
                        Convert.ToUInt16(vers[2]));
                }
                base.DeviceVersion = deviceVersion;
            }
        }

        public override void LoadVersion(object deviceObj)
        {
            this._version.LoadStruct();
        }

        #endregion

        #region MemoryEntityMembers
        private MemoryEntity<VersionStruct> _version;
        public MemoryEntity<VersionStruct> Version
        {
            get { return this._version; }
        }

        private MemoryEntity<ConfigurationStruct> _configuration;
        public MemoryEntity<ConfigurationStruct> Configuration
        {
            get { return this._configuration; }
        }

        private MemoryEntity<ConfigurationStructNew> _configurationNew;

        public MemoryEntity<ConfigurationStructNew> ConfigurationNew
        {
            get { return this._configurationNew; }
        }

        private MemoryEntity<OneWordStruct> _command;
        public MemoryEntity<OneWordStruct> Command
        {
            get { return this._command; }
        }
        #endregion
        private void VersionLoaded()
        {
            this.DeviceVersion = this._version.Value.VersionFull;
            DeviceType = "���100";
            LoadVersionOk();
        }
        
        #region �����������

        private slot _diagnostic = new slot(0, 0xC);
        public event Handler DiagnosticLoadOk;
        public event Handler DiagnosticLoadFail;

        public void LoadDiagnosticCycle()
        {
            LoadSlotCycle(DeviceNumber, this._diagnostic, "���100 �" + DeviceNumber + " ������ �����������", new TimeSpan(100),
                          10, this);
        }

        public void MakeDiagnosticQuick()
        {
            MakeQueryQuick("���100 �" + DeviceNumber + " ������ �����������");
        }

        public void MakeDiagnosticSlow()
        {
            MakeQuerySlow("���100 �" + DeviceNumber + " ������ �����������", 1);
        }

        public void RemoveDiagnostic()
        {
            this.MB.RemoveQuery("���100 �" + DeviceNumber + " ������ �����������");
        }

        public const int DISKRETINPUTS_CNT = 4;
        public const int KISINPUTS_CNT = 6;
        public const int KISINPUTS_OFFSET = 8; //�������� � ������ �� ����� �����������

        public BitArray GetDiskretInputs(int index)
        {
            if (index >= DISKRETINPUTS_CNT)
            {
                throw new ArgumentOutOfRangeException("index", DISKRETINPUTS_CNT,
                                                      "Diskret inputs index must be less than " + DISKRETINPUTS_CNT);
            }
            byte[] buffer = Common.TOBYTES(this._diagnostic.Value, false);

            BitArray allBits = new BitArray(buffer);
            BitArray ret = new BitArray(10);

            int j = 0;
            for (int i = index*10; i < index*10 + 10; i++)
            {
                ret[j++] = allBits[i];
            }

            return ret;
        }

        public BitArray GetDiskretOutputs()
        {
            return new BitArray(new byte[] {Common.LOBYTE(this._diagnostic.Value[4])});
        }


        public byte[] GetKISInputs()
        {
            byte[] ret = new byte[KISINPUTS_CNT];
            Array.ConstrainedCopy(Common.TOBYTES(this._diagnostic.Value, false), KISINPUTS_OFFSET * 2, ret, 0, KISINPUTS_CNT);
            return ret;
        }

        #endregion

        #region ���� - �����
        private slot _datetime = new slot(0x200, 0x210);
        public event Handler TimeLoadOk;
        public event Handler TimeLoadFail;
        public void LoadTimeCycle()
        {
            LoadSlotCycle(DeviceNumber, this._datetime, "LoadDateTime" + DeviceNumber, new TimeSpan(1000), 10, this);
        }

        public void WriteTime() 
        {
            SaveSlot(DeviceNumber, this._datetime, "SaveTime" + DeviceNumber, this);
        }

        public void StopReadingTime() 
        {
            this.MB.RemoveQuery("LoadDateTime" + DeviceNumber);
        }


        public ushort TIME_YEAR
        {
            get { return this._datetime.Value[0]; }
            set { this._datetime.Value[0] = value; }
        }

        public ushort TIME_MONTH
        {
            get { return this._datetime.Value[2]; }
            set { this._datetime.Value[2] = value; }
        }

        public ushort TIME_DATE
        {
            get { return this._datetime.Value[4]; }
            set { this._datetime.Value[4] = value; }
        }

        public ushort TIME_HOUR
        {
            get { return this._datetime.Value[8]; }
            set { this._datetime.Value[8] = value; }
        }

        public ushort TIME_MINUTES
        {
            get { return this._datetime.Value[10]; }
            set { this._datetime.Value[10] = value; }
        }

        public ushort TIME_SECONDS
        {
            get { return this._datetime.Value[12]; }
            set { this._datetime.Value[12] = value; }
        }

        public ushort TIME_MSECONDS
        {
            get { return this._datetime.Value[14]; }
            set { this._datetime.Value[14] = value; }
        }

        #endregion

        #region ������������
        public byte[] Deserialize(string binFileName)
        {
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load(binFileName);
            }
            catch (XmlException)
            {
                throw new FileLoadException("���� ������� ���100 ���������", binFileName);
            }
            slot commutations = new slot(0x520, 0x560);
            slot blocking = new slot(0x560, 0x5A0);
            slot diskretConfig1 = new slot(0x300, 0x340);
            slot diskretConfig2 = new slot(0x340, 0x380);
            slot diskretConfig3 = new slot(0x380, 0x3C0);
            slot diskretConfig4 = new slot(0x3C0, 0x400);
            slot kisConfig = new slot(0x500, 0x520);
            slot rele = new slot(0x400, 0x420);
            List<ushort> values = new List<ushort>();
            try
            {
                this.DeserializeSlot(doc, "/CS/����������", commutations);
                this.DeserializeSlot(doc, "/CS/����������", blocking);
                this.DeserializeSlot(doc, "/CS/�������_������1", diskretConfig1);
                this.DeserializeSlot(doc, "/CS/�������_������2", diskretConfig2);
                this.DeserializeSlot(doc, "/CS/�������_������3", diskretConfig3);
                this.DeserializeSlot(doc, "/CS/�������_������4", diskretConfig4);
                this.DeserializeSlot(doc, "/CS/���_������", kisConfig);
                this.DeserializeSlot(doc, "/CS/����", rele);

            }
            catch (NullReferenceException)
            {
                throw new FileLoadException("���� ������� ���100 ���������", binFileName);
            }
            ushort[] diskretConfigBuffer = new ushort[0x100];
            Array.ConstrainedCopy(diskretConfig1.Value, 0, diskretConfigBuffer, 0, 0x40);
            Array.ConstrainedCopy(diskretConfig2.Value, 0, diskretConfigBuffer, 0x40, 0x40);
            Array.ConstrainedCopy(diskretConfig3.Value, 0, diskretConfigBuffer, 0x80, 0x40);
            Array.ConstrainedCopy(diskretConfig4.Value, 0, diskretConfigBuffer, 0xC0, 0x40);
            values.AddRange(diskretConfigBuffer);

            ushort[] relayConfigBuffer = new ushort[0x100];
            Array.ConstrainedCopy(rele.Value, 0, relayConfigBuffer, 0, 6);
            values.AddRange(relayConfigBuffer);

            ushort[] kisConfigBuffer = new ushort[0x20];
            Array.ConstrainedCopy(kisConfig.Value, 0, kisConfigBuffer, 0, 0x20);
            values.AddRange(kisConfigBuffer);

            ushort[] diskretCommutationsBuffer = new ushort[40];
            Array.ConstrainedCopy(commutations.Value, 0, diskretCommutationsBuffer, 0, 40);
            values.AddRange(diskretCommutationsBuffer);
            ushort[] kisCommutationsBuffer = new ushort[24];
            Array.ConstrainedCopy(commutations.Value, 40, kisCommutationsBuffer, 0, 6);
            values.AddRange(kisCommutationsBuffer);

            ushort[] diskretBlockingBuffer = new ushort[40];
            Array.ConstrainedCopy(blocking.Value, 0, diskretBlockingBuffer, 0, 40);
            values.AddRange(diskretBlockingBuffer);
            ushort[] kisBlockingBuffer = new ushort[24];
            Array.ConstrainedCopy(blocking.Value, 40, kisBlockingBuffer, 0, 6);
            values.AddRange(kisBlockingBuffer);

            values.AddRange(new ushort[0x60]);

            return Common.TOBYTES(values.ToArray(), false);
        }

        private void DeserializeSlot(XmlDocument doc, string nodePath, slot slot)
        {
            XmlNode selectSingleNode = doc.SelectSingleNode(nodePath);
            if (selectSingleNode != null)
                slot.Value = Common.TOWORDS(Convert.FromBase64String(selectSingleNode.InnerText), true);
        }

        #endregion

        #region ������
        private slot _commandSlot = new slot(0x0D,0x0E);

        public enum LogicCommand
        {
            START = 0,
            STOP = 1,
            CVIT = 2,
            END =3
        }
        
        public void SendCvitCommand(LogicCommand command)
        {
            this._commandSlot.Value[0] = (ushort)command;
            SaveSlot(DeviceNumber, this._commandSlot, "���100 �" + DeviceNumber + " ����.�������", this);
        }
        

        public void RemoveLogicCommand() 
        {
            this.MB.RemoveQuery("���100 �" + DeviceNumber + " ���.�������.������");
        }
        
        #endregion
        

        protected override void mb_CompleteExchange(object sender, Query query)
        {
            if (query.name == "LoadDateTime" + DeviceNumber)
            {
                Raise(query, this.TimeLoadOk, this.TimeLoadFail, ref this._datetime);
            }
            
            if (query.name == "���100 �" + DeviceNumber + " ����.�������")
            {
                Raise(query, this.CvitCommandSaveOk, this.CvitCommandSaveFail);
            }

            if (query.name == "���100 �" + DeviceNumber + " ������ �����������")
            {
                Raise(query, this.DiagnosticLoadOk, this.DiagnosticLoadFail, ref this._diagnostic);
            }
            
            if (query.name == "LoadJournalInfo" + DeviceNumber)
            {
                Raise(query, this.AlarmJournalInfoLoadOk, this.AlarmJournalInfoLoadFail,ref this._alarmJounalSettings);
            }

            string[] journ = query.name.Split(':');
            if (journ[0] == "LoadJournalSlot" + DeviceNumber)
            {
                if (query.fail == 0)
                {
                    Array.ConstrainedCopy(Common.TOWORDS(query.readBuffer, true), 0, this._alarmJounal[Convert.ToInt32(journ[1])].Value, 0, this._alarmJounal[Convert.ToInt32(journ[1])].Value.Length);
                    this.journal.AddValues(this._alarmJounal[Convert.ToInt32(journ[1])].Value);
                    if (this.AlarmJournalLoadOk != null)
                    {
                        this.AlarmJournalLoadOk(this);
                    }
                }
                else
                {
                    if (this.AlarmJournalLoadFail!=null)
                    {
                        this.AlarmJournalLoadFail(this);
                    }
                }
            }
            base.mb_CompleteExchange(sender, query);
        }

        #region IDeviceVersion members

        private const string OLD_VERSION = "�� 6.8.04";
        private const string MID_VERSION = "� 6.8.04 �� 8.1.00";
        private const string NEW_VERSION = "8.1.00 � ����";
        public List<string> Versions => new List<string>
        {
            OLD_VERSION,
            MID_VERSION,
            NEW_VERSION
        };

        private string SwitchVersion(string deviceVersion)
        {
            switch (deviceVersion)
            {
                case OLD_VERSION:
                    return "6.0.00"; //this.DeviceVersion = "6.0.00";
                    //this._version.Value.SetVersion(6, 0, 0);
                case MID_VERSION:
                    return "7.0.01";
                    //this._version.Value.SetVersion(7, 0, 1);
                case NEW_VERSION:
                    return "8.1.00";
                    //this._version.Value.SetVersion(8, 1, 0);
            }
            return deviceVersion;
        }

        public Type[] Forms
        {
            get
            {
                //if (this._version.Value.IsEmptyVersion)
                //{
                //    this.DeviceVersion = this.SwitchVersion(base.DeviceVersion);
                //}
                if (this._version.Value.Version1 >= 8 && this._version.Value.Version2 >= 1)
                {
                    return new[]
                    {
                        typeof (CsConfigurationForm),
                        typeof (CS_DiagnosticForm),
                        typeof (CsAlarmJournalForm),
                        typeof (PoInformationForm)
                    };
                }

                return new[]
                {
                    typeof (CsConfigurationForm),
                    typeof (CS_DiagnosticForm),
                    typeof (CsAlarmJournalForm)
                };
            }
        }
       

        #endregion

        #region INodeView Members

        [XmlIgnore]
        [Browsable(false)]
        public Type ClassType => typeof (CS);

        [XmlIgnore]
        [Browsable(false)]
        public bool ForceShow => false;

        [XmlIgnore]
        [Browsable(false)]
        public Image NodeImage => Resources.picon;

        [Browsable(false)]
        public string NodeName => "���100";

        [XmlIgnore]
        [Browsable(false)]
        public INodeView[] ChildNodes => new INodeView[] {};

        [XmlIgnore]
        [Browsable(false)]
        public bool Deletable => true;

        #endregion
    }
}