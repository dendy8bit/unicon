namespace BEMN.CS
{
    partial class CS_DiagnosticForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._kisInput = new System.Windows.Forms.MaskedTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this._led4_10 = new BEMN.Forms.LedControl();
            this._led4_9 = new BEMN.Forms.LedControl();
            this._led3_10 = new BEMN.Forms.LedControl();
            this._led3_9 = new BEMN.Forms.LedControl();
            this._led2_10 = new BEMN.Forms.LedControl();
            this._led2_9 = new BEMN.Forms.LedControl();
            this._led1_10 = new BEMN.Forms.LedControl();
            this._led1_9 = new BEMN.Forms.LedControl();
            this._led4_8 = new BEMN.Forms.LedControl();
            this._led4_7 = new BEMN.Forms.LedControl();
            this._led4_6 = new BEMN.Forms.LedControl();
            this._led4_5 = new BEMN.Forms.LedControl();
            this._led4_4 = new BEMN.Forms.LedControl();
            this._led4_3 = new BEMN.Forms.LedControl();
            this._led4_2 = new BEMN.Forms.LedControl();
            this._led4_1 = new BEMN.Forms.LedControl();
            this.label4 = new System.Windows.Forms.Label();
            this._led3_8 = new BEMN.Forms.LedControl();
            this._led3_7 = new BEMN.Forms.LedControl();
            this._led3_6 = new BEMN.Forms.LedControl();
            this._led3_5 = new BEMN.Forms.LedControl();
            this._led3_4 = new BEMN.Forms.LedControl();
            this._led3_3 = new BEMN.Forms.LedControl();
            this._led3_2 = new BEMN.Forms.LedControl();
            this._led3_1 = new BEMN.Forms.LedControl();
            this.label5 = new System.Windows.Forms.Label();
            this._led2_8 = new BEMN.Forms.LedControl();
            this._led2_7 = new BEMN.Forms.LedControl();
            this._led2_6 = new BEMN.Forms.LedControl();
            this._led2_5 = new BEMN.Forms.LedControl();
            this._led2_4 = new BEMN.Forms.LedControl();
            this._led2_3 = new BEMN.Forms.LedControl();
            this._led2_2 = new BEMN.Forms.LedControl();
            this._led2_1 = new BEMN.Forms.LedControl();
            this.label3 = new System.Windows.Forms.Label();
            this._led1_8 = new BEMN.Forms.LedControl();
            this._led1_7 = new BEMN.Forms.LedControl();
            this._led1_6 = new BEMN.Forms.LedControl();
            this._led1_5 = new BEMN.Forms.LedControl();
            this._led1_4 = new BEMN.Forms.LedControl();
            this._led1_3 = new BEMN.Forms.LedControl();
            this._led1_2 = new BEMN.Forms.LedControl();
            this._led1_1 = new BEMN.Forms.LedControl();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this._outLed8 = new BEMN.Forms.LedControl();
            this._outLed7 = new BEMN.Forms.LedControl();
            this._outLed6 = new BEMN.Forms.LedControl();
            this._outLed5 = new BEMN.Forms.LedControl();
            this._outLed4 = new BEMN.Forms.LedControl();
            this._outLed3 = new BEMN.Forms.LedControl();
            this._outLed2 = new BEMN.Forms.LedControl();
            this._outLed1 = new BEMN.Forms.LedControl();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this._writeDateTimeButt = new System.Windows.Forms.Button();
            this._dateTimeNowButt = new System.Windows.Forms.Button();
            this._stopCB = new System.Windows.Forms.CheckBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this._timeClockTB = new System.Windows.Forms.MaskedTextBox();
            this._dateClockTB = new System.Windows.Forms.MaskedTextBox();
            this._cvitButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // _kisInput
            // 
            this._kisInput.Location = new System.Drawing.Point(19, 4);
            this._kisInput.Mask = "0 - 0 - 0 - 0 - 0 - 0";
            this._kisInput.Name = "_kisInput";
            this._kisInput.ReadOnly = true;
            this._kisInput.Size = new System.Drawing.Size(98, 20);
            this._kisInput.TabIndex = 0;
            this._kisInput.Text = "000000";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(123, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "��� �����";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this._led4_10);
            this.groupBox1.Controls.Add(this._led4_9);
            this.groupBox1.Controls.Add(this._led3_10);
            this.groupBox1.Controls.Add(this._led3_9);
            this.groupBox1.Controls.Add(this._led2_10);
            this.groupBox1.Controls.Add(this._led2_9);
            this.groupBox1.Controls.Add(this._led1_10);
            this.groupBox1.Controls.Add(this._led1_9);
            this.groupBox1.Controls.Add(this._led4_8);
            this.groupBox1.Controls.Add(this._led4_7);
            this.groupBox1.Controls.Add(this._led4_6);
            this.groupBox1.Controls.Add(this._led4_5);
            this.groupBox1.Controls.Add(this._led4_4);
            this.groupBox1.Controls.Add(this._led4_3);
            this.groupBox1.Controls.Add(this._led4_2);
            this.groupBox1.Controls.Add(this._led4_1);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this._led3_8);
            this.groupBox1.Controls.Add(this._led3_7);
            this.groupBox1.Controls.Add(this._led3_6);
            this.groupBox1.Controls.Add(this._led3_5);
            this.groupBox1.Controls.Add(this._led3_4);
            this.groupBox1.Controls.Add(this._led3_3);
            this.groupBox1.Controls.Add(this._led3_2);
            this.groupBox1.Controls.Add(this._led3_1);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this._led2_8);
            this.groupBox1.Controls.Add(this._led2_7);
            this.groupBox1.Controls.Add(this._led2_6);
            this.groupBox1.Controls.Add(this._led2_5);
            this.groupBox1.Controls.Add(this._led2_4);
            this.groupBox1.Controls.Add(this._led2_3);
            this.groupBox1.Controls.Add(this._led2_2);
            this.groupBox1.Controls.Add(this._led2_1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this._led1_8);
            this.groupBox1.Controls.Add(this._led1_7);
            this.groupBox1.Controls.Add(this._led1_6);
            this.groupBox1.Controls.Add(this._led1_5);
            this.groupBox1.Controls.Add(this._led1_4);
            this.groupBox1.Controls.Add(this._led1_3);
            this.groupBox1.Controls.Add(this._led1_2);
            this.groupBox1.Controls.Add(this._led1_1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(10, 32);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(222, 121);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "���������� �����";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.White;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label6.Location = new System.Drawing.Point(8, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(192, 15);
            this.label6.TabIndex = 46;
            this.label6.Text = "1     2     3    4    5     6     7    8    9   10";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _led4_10
            // 
            this._led4_10.Location = new System.Drawing.Point(183, 102);
            this._led4_10.Name = "_led4_10";
            this._led4_10.Size = new System.Drawing.Size(13, 13);
            this._led4_10.State = BEMN.Forms.LedState.Off;
            this._led4_10.TabIndex = 45;
            // 
            // _led4_9
            // 
            this._led4_9.Location = new System.Drawing.Point(164, 102);
            this._led4_9.Name = "_led4_9";
            this._led4_9.Size = new System.Drawing.Size(13, 13);
            this._led4_9.State = BEMN.Forms.LedState.Off;
            this._led4_9.TabIndex = 44;
            // 
            // _led3_10
            // 
            this._led3_10.Location = new System.Drawing.Point(183, 83);
            this._led3_10.Name = "_led3_10";
            this._led3_10.Size = new System.Drawing.Size(13, 13);
            this._led3_10.State = BEMN.Forms.LedState.Off;
            this._led3_10.TabIndex = 43;
            // 
            // _led3_9
            // 
            this._led3_9.Location = new System.Drawing.Point(164, 83);
            this._led3_9.Name = "_led3_9";
            this._led3_9.Size = new System.Drawing.Size(13, 13);
            this._led3_9.State = BEMN.Forms.LedState.Off;
            this._led3_9.TabIndex = 42;
            // 
            // _led2_10
            // 
            this._led2_10.Location = new System.Drawing.Point(183, 63);
            this._led2_10.Name = "_led2_10";
            this._led2_10.Size = new System.Drawing.Size(13, 13);
            this._led2_10.State = BEMN.Forms.LedState.Off;
            this._led2_10.TabIndex = 41;
            // 
            // _led2_9
            // 
            this._led2_9.Location = new System.Drawing.Point(164, 63);
            this._led2_9.Name = "_led2_9";
            this._led2_9.Size = new System.Drawing.Size(13, 13);
            this._led2_9.State = BEMN.Forms.LedState.Off;
            this._led2_9.TabIndex = 40;
            // 
            // _led1_10
            // 
            this._led1_10.Location = new System.Drawing.Point(183, 44);
            this._led1_10.Name = "_led1_10";
            this._led1_10.Size = new System.Drawing.Size(13, 13);
            this._led1_10.State = BEMN.Forms.LedState.Off;
            this._led1_10.TabIndex = 39;
            // 
            // _led1_9
            // 
            this._led1_9.Location = new System.Drawing.Point(164, 44);
            this._led1_9.Name = "_led1_9";
            this._led1_9.Size = new System.Drawing.Size(13, 13);
            this._led1_9.State = BEMN.Forms.LedState.Off;
            this._led1_9.TabIndex = 38;
            // 
            // _led4_8
            // 
            this._led4_8.Location = new System.Drawing.Point(146, 102);
            this._led4_8.Name = "_led4_8";
            this._led4_8.Size = new System.Drawing.Size(13, 13);
            this._led4_8.State = BEMN.Forms.LedState.Off;
            this._led4_8.TabIndex = 37;
            // 
            // _led4_7
            // 
            this._led4_7.Location = new System.Drawing.Point(127, 102);
            this._led4_7.Name = "_led4_7";
            this._led4_7.Size = new System.Drawing.Size(13, 13);
            this._led4_7.State = BEMN.Forms.LedState.Off;
            this._led4_7.TabIndex = 36;
            // 
            // _led4_6
            // 
            this._led4_6.Location = new System.Drawing.Point(106, 102);
            this._led4_6.Name = "_led4_6";
            this._led4_6.Size = new System.Drawing.Size(13, 13);
            this._led4_6.State = BEMN.Forms.LedState.Off;
            this._led4_6.TabIndex = 35;
            // 
            // _led4_5
            // 
            this._led4_5.Location = new System.Drawing.Point(87, 102);
            this._led4_5.Name = "_led4_5";
            this._led4_5.Size = new System.Drawing.Size(13, 13);
            this._led4_5.State = BEMN.Forms.LedState.Off;
            this._led4_5.TabIndex = 34;
            // 
            // _led4_4
            // 
            this._led4_4.Location = new System.Drawing.Point(68, 102);
            this._led4_4.Name = "_led4_4";
            this._led4_4.Size = new System.Drawing.Size(13, 13);
            this._led4_4.State = BEMN.Forms.LedState.Off;
            this._led4_4.TabIndex = 33;
            // 
            // _led4_3
            // 
            this._led4_3.Location = new System.Drawing.Point(49, 102);
            this._led4_3.Name = "_led4_3";
            this._led4_3.Size = new System.Drawing.Size(13, 13);
            this._led4_3.State = BEMN.Forms.LedState.Off;
            this._led4_3.TabIndex = 32;
            // 
            // _led4_2
            // 
            this._led4_2.Location = new System.Drawing.Point(28, 102);
            this._led4_2.Name = "_led4_2";
            this._led4_2.Size = new System.Drawing.Size(13, 13);
            this._led4_2.State = BEMN.Forms.LedState.Off;
            this._led4_2.TabIndex = 31;
            // 
            // _led4_1
            // 
            this._led4_1.Location = new System.Drawing.Point(9, 102);
            this._led4_1.Name = "_led4_1";
            this._led4_1.Size = new System.Drawing.Size(13, 13);
            this._led4_1.State = BEMN.Forms.LedState.Off;
            this._led4_1.TabIndex = 30;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(202, 102);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(13, 13);
            this.label4.TabIndex = 29;
            this.label4.Text = "4";
            // 
            // _led3_8
            // 
            this._led3_8.Location = new System.Drawing.Point(146, 83);
            this._led3_8.Name = "_led3_8";
            this._led3_8.Size = new System.Drawing.Size(13, 13);
            this._led3_8.State = BEMN.Forms.LedState.Off;
            this._led3_8.TabIndex = 28;
            // 
            // _led3_7
            // 
            this._led3_7.Location = new System.Drawing.Point(127, 83);
            this._led3_7.Name = "_led3_7";
            this._led3_7.Size = new System.Drawing.Size(13, 13);
            this._led3_7.State = BEMN.Forms.LedState.Off;
            this._led3_7.TabIndex = 27;
            // 
            // _led3_6
            // 
            this._led3_6.Location = new System.Drawing.Point(106, 83);
            this._led3_6.Name = "_led3_6";
            this._led3_6.Size = new System.Drawing.Size(13, 13);
            this._led3_6.State = BEMN.Forms.LedState.Off;
            this._led3_6.TabIndex = 26;
            // 
            // _led3_5
            // 
            this._led3_5.Location = new System.Drawing.Point(87, 83);
            this._led3_5.Name = "_led3_5";
            this._led3_5.Size = new System.Drawing.Size(13, 13);
            this._led3_5.State = BEMN.Forms.LedState.Off;
            this._led3_5.TabIndex = 25;
            // 
            // _led3_4
            // 
            this._led3_4.Location = new System.Drawing.Point(68, 83);
            this._led3_4.Name = "_led3_4";
            this._led3_4.Size = new System.Drawing.Size(13, 13);
            this._led3_4.State = BEMN.Forms.LedState.Off;
            this._led3_4.TabIndex = 24;
            // 
            // _led3_3
            // 
            this._led3_3.Location = new System.Drawing.Point(49, 83);
            this._led3_3.Name = "_led3_3";
            this._led3_3.Size = new System.Drawing.Size(13, 13);
            this._led3_3.State = BEMN.Forms.LedState.Off;
            this._led3_3.TabIndex = 23;
            // 
            // _led3_2
            // 
            this._led3_2.Location = new System.Drawing.Point(28, 83);
            this._led3_2.Name = "_led3_2";
            this._led3_2.Size = new System.Drawing.Size(13, 13);
            this._led3_2.State = BEMN.Forms.LedState.Off;
            this._led3_2.TabIndex = 22;
            // 
            // _led3_1
            // 
            this._led3_1.Location = new System.Drawing.Point(9, 83);
            this._led3_1.Name = "_led3_1";
            this._led3_1.Size = new System.Drawing.Size(13, 13);
            this._led3_1.State = BEMN.Forms.LedState.Off;
            this._led3_1.TabIndex = 21;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(202, 83);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(13, 13);
            this.label5.TabIndex = 20;
            this.label5.Text = "3";
            // 
            // _led2_8
            // 
            this._led2_8.Location = new System.Drawing.Point(146, 63);
            this._led2_8.Name = "_led2_8";
            this._led2_8.Size = new System.Drawing.Size(13, 13);
            this._led2_8.State = BEMN.Forms.LedState.Off;
            this._led2_8.TabIndex = 19;
            // 
            // _led2_7
            // 
            this._led2_7.Location = new System.Drawing.Point(127, 63);
            this._led2_7.Name = "_led2_7";
            this._led2_7.Size = new System.Drawing.Size(13, 13);
            this._led2_7.State = BEMN.Forms.LedState.Off;
            this._led2_7.TabIndex = 18;
            // 
            // _led2_6
            // 
            this._led2_6.Location = new System.Drawing.Point(106, 63);
            this._led2_6.Name = "_led2_6";
            this._led2_6.Size = new System.Drawing.Size(13, 13);
            this._led2_6.State = BEMN.Forms.LedState.Off;
            this._led2_6.TabIndex = 17;
            // 
            // _led2_5
            // 
            this._led2_5.Location = new System.Drawing.Point(87, 63);
            this._led2_5.Name = "_led2_5";
            this._led2_5.Size = new System.Drawing.Size(13, 13);
            this._led2_5.State = BEMN.Forms.LedState.Off;
            this._led2_5.TabIndex = 16;
            // 
            // _led2_4
            // 
            this._led2_4.Location = new System.Drawing.Point(68, 63);
            this._led2_4.Name = "_led2_4";
            this._led2_4.Size = new System.Drawing.Size(13, 13);
            this._led2_4.State = BEMN.Forms.LedState.Off;
            this._led2_4.TabIndex = 15;
            // 
            // _led2_3
            // 
            this._led2_3.Location = new System.Drawing.Point(49, 63);
            this._led2_3.Name = "_led2_3";
            this._led2_3.Size = new System.Drawing.Size(13, 13);
            this._led2_3.State = BEMN.Forms.LedState.Off;
            this._led2_3.TabIndex = 14;
            // 
            // _led2_2
            // 
            this._led2_2.Location = new System.Drawing.Point(28, 63);
            this._led2_2.Name = "_led2_2";
            this._led2_2.Size = new System.Drawing.Size(13, 13);
            this._led2_2.State = BEMN.Forms.LedState.Off;
            this._led2_2.TabIndex = 13;
            // 
            // _led2_1
            // 
            this._led2_1.Location = new System.Drawing.Point(9, 63);
            this._led2_1.Name = "_led2_1";
            this._led2_1.Size = new System.Drawing.Size(13, 13);
            this._led2_1.State = BEMN.Forms.LedState.Off;
            this._led2_1.TabIndex = 12;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(202, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(13, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "2";
            // 
            // _led1_8
            // 
            this._led1_8.Location = new System.Drawing.Point(146, 44);
            this._led1_8.Name = "_led1_8";
            this._led1_8.Size = new System.Drawing.Size(13, 13);
            this._led1_8.State = BEMN.Forms.LedState.Off;
            this._led1_8.TabIndex = 10;
            // 
            // _led1_7
            // 
            this._led1_7.Location = new System.Drawing.Point(127, 44);
            this._led1_7.Name = "_led1_7";
            this._led1_7.Size = new System.Drawing.Size(13, 13);
            this._led1_7.State = BEMN.Forms.LedState.Off;
            this._led1_7.TabIndex = 9;
            // 
            // _led1_6
            // 
            this._led1_6.Location = new System.Drawing.Point(106, 44);
            this._led1_6.Name = "_led1_6";
            this._led1_6.Size = new System.Drawing.Size(13, 13);
            this._led1_6.State = BEMN.Forms.LedState.Off;
            this._led1_6.TabIndex = 8;
            // 
            // _led1_5
            // 
            this._led1_5.Location = new System.Drawing.Point(87, 44);
            this._led1_5.Name = "_led1_5";
            this._led1_5.Size = new System.Drawing.Size(13, 13);
            this._led1_5.State = BEMN.Forms.LedState.Off;
            this._led1_5.TabIndex = 7;
            // 
            // _led1_4
            // 
            this._led1_4.Location = new System.Drawing.Point(68, 44);
            this._led1_4.Name = "_led1_4";
            this._led1_4.Size = new System.Drawing.Size(13, 13);
            this._led1_4.State = BEMN.Forms.LedState.Off;
            this._led1_4.TabIndex = 6;
            // 
            // _led1_3
            // 
            this._led1_3.Location = new System.Drawing.Point(49, 44);
            this._led1_3.Name = "_led1_3";
            this._led1_3.Size = new System.Drawing.Size(13, 13);
            this._led1_3.State = BEMN.Forms.LedState.Off;
            this._led1_3.TabIndex = 5;
            // 
            // _led1_2
            // 
            this._led1_2.Location = new System.Drawing.Point(28, 44);
            this._led1_2.Name = "_led1_2";
            this._led1_2.Size = new System.Drawing.Size(13, 13);
            this._led1_2.State = BEMN.Forms.LedState.Off;
            this._led1_2.TabIndex = 4;
            // 
            // _led1_1
            // 
            this._led1_1.Location = new System.Drawing.Point(9, 44);
            this._led1_1.Name = "_led1_1";
            this._led1_1.Size = new System.Drawing.Size(13, 13);
            this._led1_1.State = BEMN.Forms.LedState.Off;
            this._led1_1.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(202, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(13, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "1";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this._outLed8);
            this.groupBox2.Controls.Add(this._outLed7);
            this.groupBox2.Controls.Add(this._outLed6);
            this.groupBox2.Controls.Add(this._outLed5);
            this.groupBox2.Controls.Add(this._outLed4);
            this.groupBox2.Controls.Add(this._outLed3);
            this.groupBox2.Controls.Add(this._outLed2);
            this.groupBox2.Controls.Add(this._outLed1);
            this.groupBox2.Location = new System.Drawing.Point(10, 159);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(177, 60);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "����";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.White;
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label7.Location = new System.Drawing.Point(7, 15);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(164, 15);
            this.label7.TabIndex = 47;
            this.label7.Text = "1    2    3     4       5     6     3    � ";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _outLed8
            // 
            this._outLed8.Location = new System.Drawing.Point(8, 39);
            this._outLed8.Name = "_outLed8";
            this._outLed8.Size = new System.Drawing.Size(13, 13);
            this._outLed8.State = BEMN.Forms.LedState.Off;
            this._outLed8.TabIndex = 46;
            // 
            // _outLed7
            // 
            this._outLed7.Location = new System.Drawing.Point(26, 39);
            this._outLed7.Name = "_outLed7";
            this._outLed7.Size = new System.Drawing.Size(13, 13);
            this._outLed7.State = BEMN.Forms.LedState.Off;
            this._outLed7.TabIndex = 45;
            // 
            // _outLed6
            // 
            this._outLed6.Location = new System.Drawing.Point(45, 39);
            this._outLed6.Name = "_outLed6";
            this._outLed6.Size = new System.Drawing.Size(13, 13);
            this._outLed6.State = BEMN.Forms.LedState.Off;
            this._outLed6.TabIndex = 44;
            // 
            // _outLed5
            // 
            this._outLed5.Location = new System.Drawing.Point(65, 39);
            this._outLed5.Name = "_outLed5";
            this._outLed5.Size = new System.Drawing.Size(13, 13);
            this._outLed5.State = BEMN.Forms.LedState.Off;
            this._outLed5.TabIndex = 43;
            // 
            // _outLed4
            // 
            this._outLed4.Location = new System.Drawing.Point(92, 39);
            this._outLed4.Name = "_outLed4";
            this._outLed4.Size = new System.Drawing.Size(13, 13);
            this._outLed4.State = BEMN.Forms.LedState.Off;
            this._outLed4.TabIndex = 42;
            // 
            // _outLed3
            // 
            this._outLed3.Location = new System.Drawing.Point(113, 39);
            this._outLed3.Name = "_outLed3";
            this._outLed3.Size = new System.Drawing.Size(13, 13);
            this._outLed3.State = BEMN.Forms.LedState.Off;
            this._outLed3.TabIndex = 41;
            // 
            // _outLed2
            // 
            this._outLed2.Location = new System.Drawing.Point(133, 39);
            this._outLed2.Name = "_outLed2";
            this._outLed2.Size = new System.Drawing.Size(13, 13);
            this._outLed2.State = BEMN.Forms.LedState.Off;
            this._outLed2.TabIndex = 40;
            // 
            // _outLed1
            // 
            this._outLed1.Location = new System.Drawing.Point(152, 39);
            this._outLed1.Name = "_outLed1";
            this._outLed1.Size = new System.Drawing.Size(13, 13);
            this._outLed1.State = BEMN.Forms.LedState.Off;
            this._outLed1.TabIndex = 39;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this._writeDateTimeButt);
            this.groupBox6.Controls.Add(this._dateTimeNowButt);
            this.groupBox6.Controls.Add(this._stopCB);
            this.groupBox6.Controls.Add(this.label20);
            this.groupBox6.Controls.Add(this.label21);
            this.groupBox6.Controls.Add(this._timeClockTB);
            this.groupBox6.Controls.Add(this._dateClockTB);
            this.groupBox6.Location = new System.Drawing.Point(240, 32);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(155, 144);
            this.groupBox6.TabIndex = 7;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "���� - �����";
            // 
            // _writeDateTimeButt
            // 
            this._writeDateTimeButt.Enabled = false;
            this._writeDateTimeButt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._writeDateTimeButt.Location = new System.Drawing.Point(5, 112);
            this._writeDateTimeButt.Name = "_writeDateTimeButt";
            this._writeDateTimeButt.Size = new System.Drawing.Size(145, 23);
            this._writeDateTimeButt.TabIndex = 6;
            this._writeDateTimeButt.Text = "����������";
            this._writeDateTimeButt.UseVisualStyleBackColor = true;
            this._writeDateTimeButt.Click += new System.EventHandler(this._writeDateTimeButt_Click);
            // 
            // _dateTimeNowButt
            // 
            this._dateTimeNowButt.Enabled = false;
            this._dateTimeNowButt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._dateTimeNowButt.Location = new System.Drawing.Point(5, 83);
            this._dateTimeNowButt.Name = "_dateTimeNowButt";
            this._dateTimeNowButt.Size = new System.Drawing.Size(145, 23);
            this._dateTimeNowButt.TabIndex = 5;
            this._dateTimeNowButt.Text = "��������� ���� � �����";
            this._dateTimeNowButt.UseVisualStyleBackColor = true;
            this._dateTimeNowButt.Click += new System.EventHandler(this._dateTimeNowButt_Click);
            // 
            // _stopCB
            // 
            this._stopCB.AutoSize = true;
            this._stopCB.Location = new System.Drawing.Point(26, 60);
            this._stopCB.Name = "_stopCB";
            this._stopCB.Size = new System.Drawing.Size(112, 17);
            this._stopCB.TabIndex = 4;
            this._stopCB.Text = "�������� �����";
            this._stopCB.UseVisualStyleBackColor = true;
            this._stopCB.CheckedChanged += new System.EventHandler(this._stopCB_CheckedChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(77, 16);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(40, 13);
            this.label20.TabIndex = 3;
            this.label20.Text = "�����";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(2, 16);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(33, 13);
            this.label21.TabIndex = 2;
            this.label21.Text = "����";
            // 
            // _timeClockTB
            // 
            this._timeClockTB.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
            this._timeClockTB.Location = new System.Drawing.Point(80, 34);
            this._timeClockTB.Mask = "90:00:00.000";
            this._timeClockTB.Name = "_timeClockTB";
            this._timeClockTB.Size = new System.Drawing.Size(70, 20);
            this._timeClockTB.TabIndex = 1;
            this._timeClockTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _dateClockTB
            // 
            this._dateClockTB.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
            this._dateClockTB.Location = new System.Drawing.Point(5, 34);
            this._dateClockTB.Mask = "00/00/00";
            this._dateClockTB.Name = "_dateClockTB";
            this._dateClockTB.Size = new System.Drawing.Size(49, 20);
            this._dateClockTB.TabIndex = 0;
            this._dateClockTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _cvitButton
            // 
            this._cvitButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._cvitButton.Location = new System.Drawing.Point(245, 188);
            this._cvitButton.Name = "_cvitButton";
            this._cvitButton.Size = new System.Drawing.Size(145, 23);
            this._cvitButton.TabIndex = 8;
            this._cvitButton.Text = "������������";
            this._cvitButton.UseVisualStyleBackColor = true;
            this._cvitButton.Click += new System.EventHandler(this._cvitButton_Click);
            // 
            // CS_DiagnosticForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(419, 223);
            this.Controls.Add(this._cvitButton);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._kisInput);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "CS_DiagnosticForm";
            this.Text = "���100_DiagnosticForm";
            this.Activated += new System.EventHandler(this.CS_DiagnosticForm_Activated);
            this.Deactivate += new System.EventHandler(this.CS_DiagnosticForm_Deactivate);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CS_DiagnosticForm_FormClosing);
            this.Load += new System.EventHandler(this.CS_DiagnosticForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MaskedTextBox _kisInput;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private BEMN.Forms.LedControl _led4_8;
        private BEMN.Forms.LedControl _led4_7;
        private BEMN.Forms.LedControl _led4_6;
        private BEMN.Forms.LedControl _led4_5;
        private BEMN.Forms.LedControl _led4_4;
        private BEMN.Forms.LedControl _led4_3;
        private BEMN.Forms.LedControl _led4_2;
        private BEMN.Forms.LedControl _led4_1;
        private System.Windows.Forms.Label label4;
        private BEMN.Forms.LedControl _led3_8;
        private BEMN.Forms.LedControl _led3_7;
        private BEMN.Forms.LedControl _led3_6;
        private BEMN.Forms.LedControl _led3_5;
        private BEMN.Forms.LedControl _led3_4;
        private BEMN.Forms.LedControl _led3_3;
        private BEMN.Forms.LedControl _led3_2;
        private BEMN.Forms.LedControl _led3_1;
        private System.Windows.Forms.Label label5;
        private BEMN.Forms.LedControl _led2_8;
        private BEMN.Forms.LedControl _led2_7;
        private BEMN.Forms.LedControl _led2_6;
        private BEMN.Forms.LedControl _led2_5;
        private BEMN.Forms.LedControl _led2_4;
        private BEMN.Forms.LedControl _led2_3;
        private BEMN.Forms.LedControl _led2_2;
        private BEMN.Forms.LedControl _led2_1;
        private System.Windows.Forms.Label label3;
        private BEMN.Forms.LedControl _led1_8;
        private BEMN.Forms.LedControl _led1_7;
        private BEMN.Forms.LedControl _led1_6;
        private BEMN.Forms.LedControl _led1_5;
        private BEMN.Forms.LedControl _led1_4;
        private BEMN.Forms.LedControl _led1_3;
        private BEMN.Forms.LedControl _led1_2;
        private BEMN.Forms.LedControl _led1_1;
        private System.Windows.Forms.GroupBox groupBox2;
        private BEMN.Forms.LedControl _outLed8;
        private BEMN.Forms.LedControl _outLed7;
        private BEMN.Forms.LedControl _outLed6;
        private BEMN.Forms.LedControl _outLed5;
        private BEMN.Forms.LedControl _outLed4;
        private BEMN.Forms.LedControl _outLed3;
        private BEMN.Forms.LedControl _outLed2;
        private BEMN.Forms.LedControl _outLed1;
        private BEMN.Forms.LedControl _led4_10;
        private BEMN.Forms.LedControl _led4_9;
        private BEMN.Forms.LedControl _led3_10;
        private BEMN.Forms.LedControl _led3_9;
        private BEMN.Forms.LedControl _led2_10;
        private BEMN.Forms.LedControl _led2_9;
        private BEMN.Forms.LedControl _led1_10;
        private BEMN.Forms.LedControl _led1_9;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button _writeDateTimeButt;
        private System.Windows.Forms.Button _dateTimeNowButt;
        private System.Windows.Forms.CheckBox _stopCB;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.MaskedTextBox _timeClockTB;
        private System.Windows.Forms.MaskedTextBox _dateClockTB;
        private System.Windows.Forms.Button _cvitButton;
    }
}