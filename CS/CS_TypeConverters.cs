using System.ComponentModel;
using BEMN.CS.Data;

namespace BEMN.CS.Utils
{
    public class DiskretInTypeConverter : StringConverter
    {
        /// <summary>
        /// ����� ������������� ����� �� ������
        /// </summary>
        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }

        /// <summary>
        /// ... � ������ �� ������
        /// </summary>
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            // false - ����� ������� �������
            // true - ������ ����� �� ������
            return true;
        }

        /// <summary>
        /// � ��� � ������
        /// </summary>
        public override StandardValuesCollection GetStandardValues(
          ITypeDescriptorContext context)
        {
            // ���������� ������ ����� �� �������� ���������
            // (���� ������, �������� � �.�.)
            return new StandardValuesCollection(Strings.diskretIn);
        }
    }
    public class DiskretIndicationTypeConverter : StringConverter
    {
        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            return true;
        }
        public override StandardValuesCollection GetStandardValues(
          ITypeDescriptorContext context)
        {
            return new StandardValuesCollection(Strings.diskretIndication);
        }
    }
    public class ReleTypeConverter : StringConverter
    {
        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            return true;
        }
        public override StandardValuesCollection GetStandardValues(
          ITypeDescriptorContext context)
        {
            return new StandardValuesCollection(Strings.releType);
        }
    }
    public class KISModeTypeConverter : StringConverter
    {
        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            return true;
        }
        public override StandardValuesCollection GetStandardValues(
          ITypeDescriptorContext context)
        {
            return new StandardValuesCollection(Strings.kisType);
        }
    }
    public class KISControlTypeConverter : StringConverter
    {
        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            return true;
        }
        public override StandardValuesCollection GetStandardValues(
          ITypeDescriptorContext context)
        {
            return new StandardValuesCollection(Strings.kisKontrol);
        }
    }
}
