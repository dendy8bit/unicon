﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using BEMN.AlarmJournal.Structures;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.FreeLogicStructures;
using BEMN.Framework;
using BEMN.Interfaces;
using BEMN.KL27.AlarmJournal;
using BEMN.KL27.BSBGL;
using BEMN.KL27.Configuration;
using BEMN.KL27.Configuration.Structures;
using BEMN.KL27.Configuration.Structures.MeasuringTransformer;
using BEMN.KL27.Measuring;
using BEMN.KL27.Measuring.Structures;
using BEMN.KL27.Properties;
using BEMN.KL27.SystemJournal;
using BEMN.KL27.SystemJournal.Structures;
using BEMN.MBServer;
using BEMN.MBServer.Queries;

namespace BEMN.KL27
{
    public class KL27Device : Device, IDeviceView, IDeviceVersion
    {
        private readonly slot _constraintSelector = new slot(0x400, 0x401);

        private MemoryEntity<AlarmJournalStruct> _alarmRecord;
        private MemoryEntity<MeasureTransStruct> _measureTransAj;
        public KL27Device()
        {
            HaveVersion = true;
        }

        public KL27Device(Modbus mb)
        {
            MB = mb;
            this.Init();
        }

        public override Modbus MB
        {
            get { return mb; }
            set
            {
                if (mb != null)
                    mb.CompleteExchange -= this.mb_CompleteExchange;

                mb = value;

                if (mb != null)
                    mb.CompleteExchange += this.mb_CompleteExchange;
            }
        }

        protected override void mb_CompleteExchange(object sender, Query query)
        {
            if (query.name == "ConfirmConfig" + DeviceNumber)
            {
                if (query.error)
                {
                    MessageBox.Show("Не удалось записать бит подтверждения изменения конфигурации. Конфигурация в устройстве не изменена",
                        "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    MessageBox.Show("Конфигурация записана успешно", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            base.mb_CompleteExchange(sender, query);
        }

        public void Init()
        {
            HaveVersion = true;
            this.Configuration = new MemoryEntity<ConfigurationStruct>("Конфигурация", this, 0x1000);
            this.SystemJournal = new MemoryEntity<SystemJournalStruct>("ЖС", this, 0x2000);
            this._alarmRecord = new MemoryEntity<AlarmJournalStruct>("ЖА", this, 0x2800);
            this._measureTransAj = new MemoryEntity<MeasureTransStruct>("Параметры измерений ЖА", this, 0x1000);
            this.DateAndTime = new MemoryEntity<DateTimeStruct>("DateAndTime", this, 0x200);
            this.DiscretDataBase = new MemoryEntity<DiscretDataBaseStruct>("Дискретная БД", this, 0x1800);
            this.AnalogDataBase = new MemoryEntity<AnalogDataBaseStruct>("Аналоговая БД", this, 0x1900);
            this.MeasureTrans = new MemoryEntity<MeasureTransStruct>("Параметры измерений", this, 0x1000);
            this.SourceProgramStruct = new MemoryEntity<SourceProgramStruct>("SaveProgram", this, 0xB000);
            this.ProgramStartStruct = new MemoryEntity<StartStruct>("SaveProgramStart", this, 0x0001);
            this.ProgramStorageStruct = new MemoryEntity<ProgramStorageStruct>("SaveLoadProgramStorage", this, 0xC000);
            this.ProgramSignalsStruct = new MemoryEntity<ProgramSignalsStruct>("LoadProgramSignals_", this, 0xA000);
            this.StopSpl = new MemoryEntity<OneWordStruct>("StopSpl", this, 0x1808);
            this.StartSpl = new MemoryEntity<OneWordStruct>("StartSpl", this, 0x1809);
            this.StateSpl = new MemoryEntity<OneWordStruct>("StatetSpl", this, 0x1805);
        }

        
        public MemoryEntity<AlarmJournalStruct> AlarmRecord => _alarmRecord;
        
        public MemoryEntity<MeasureTransStruct> MeasureTransAj => _measureTransAj;
        public MemoryEntity<DiscretDataBaseStruct> DiscretDataBase { get; private set; }
        public MemoryEntity<DateTimeStruct> DateAndTime { get; private set; }
        public MemoryEntity<AnalogDataBaseStruct> AnalogDataBase { get; private set; }
        public MemoryEntity<MeasureTransStruct> MeasureTrans { get; private set; }
        public MemoryEntity<SystemJournalStruct> SystemJournal { get; private set; }
        public MemoryEntity<ConfigurationStruct> Configuration { get; private set; }

        #region Programming

        public MemoryEntity<ProgramStorageStruct> ProgramStorageStruct { get; private set; }
        public MemoryEntity<StartStruct> ProgramStartStruct { get; private set; }
        public MemoryEntity<ProgramSignalsStruct> ProgramSignalsStruct { get; private set; }
        public MemoryEntity<SourceProgramStruct> SourceProgramStruct { get; private set; }

        public MemoryEntity<OneWordStruct> StopSpl { get; private set; }
        public MemoryEntity<OneWordStruct> StartSpl { get; private set; }
        public MemoryEntity<OneWordStruct> StateSpl { get; private set; }

        #endregion

        public void WriteConfiguration()
        {
            SetBit(DeviceNumber, 0, true, "ConfirmConfig", this);
        }

        public void ReadConfiguration()
        {
            SetBit(DeviceNumber, 0, false, "КЛ27 запрос обновления", this);
        }

        public void SelectConstraintGroup(bool mainGroup)
        {
            this._constraintSelector.Value[0] = mainGroup ? (ushort)0 : (ushort)1;
            SaveSlot(DeviceNumber, this._constraintSelector, "КЛ27 №" + DeviceNumber + " переключение группы уставок", this);
        }

        #region IDeviceView
        public Type ClassType
        {
            get { return typeof(KL27Device); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Framework.Properties.Resources.kl; }
        }

        public string NodeName
        {
            get { return "КЛ27"; }
        }

        public INodeView[] ChildNodes
        {
            get { return null; }
        }

        public bool Deletable
        {
            get { return true; }
        }
        #endregion

        #region IDeviceVersion

        public Type[] Forms
        {
            get
            {
                return new []
                {
                    typeof(MeasuringForm),
                    typeof(BSBGLEF),
                    typeof(SystemJournalForm),
                    typeof(ConfigurationForm),
                    typeof(AlarmJournalForm)
                };
            }
        }

        public List<string> Versions
        {
            get
            {
                return new List<string>
                {
                    "1.00"
                };
            }
        } 
        #endregion
    }
}
