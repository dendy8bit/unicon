using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Xml;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms.Export;
using BEMN.Forms.ValidatingClasses;
using BEMN.Forms.ValidatingClasses.Dgw;
using BEMN.Forms.ValidatingClasses.New.ColumnsInfos;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Forms.ValidatingClasses.New.Validators.TurnOff;
using BEMN.Forms.ValidatingClasses.Rules;
using BEMN.Forms.ValidatingClasses.Rules.Ushort;
using BEMN.Interfaces;
using BEMN.KL27.Configuration.Structures;
using BEMN.KL27.Configuration.Structures.CurrentDefenses;
using BEMN.KL27.Configuration.Structures.ExternalDefenses;
using BEMN.KL27.Configuration.Structures.ExternalSignals;
using BEMN.KL27.Configuration.Structures.FaultSignal;
using BEMN.KL27.Configuration.Structures.FrequencyDefenses;
using BEMN.KL27.Configuration.Structures.Indicators;
using BEMN.KL27.Configuration.Structures.Keys;
using BEMN.KL27.Configuration.Structures.MeasuringTransformer;
using BEMN.KL27.Configuration.Structures.OscConfig;
using BEMN.KL27.Configuration.Structures.Switch;
using BEMN.KL27.Configuration.Structures.Vls;
using BEMN.KL27.Configuration.Structures.VoltageDefenses;
using BEMN.KL27.Properties;
using BEMN.MBServer;
namespace BEMN.KL27.Configuration
{
    public partial class ConfigurationForm : Form, IFormView
    {
        #region [Private fields]

        private const string KL_SETPOINTS_PATH = "��27_�������_������ {0:F1}.xml";
        private const string FILE_SAVE_FAIL = "���������� ��������� ����";
        private const string KL27 = "��27";
        private const string INVALID_PORT = "���� ����������.";
        private const string ERROR_READ_CONFIG = "������ ������ ������������";
        private const string ERROR_WRITE_CONFIG = "������ ������ ������������";
        private const string CONFIG_READ_OK = "������������ ���������";

        private readonly KL27Device _device;
        private readonly MemoryEntity<ConfigurationStruct> _configuration;
        
        private RadioButtonSelector _groupSelector;
        private ConfigurationStruct _currentSetpointsStruct;

        #region [Validators]
        
        private SetpointsValidator<AllSetpointsStruct, SetpointStruct> _currentSetpointsValidator;
        private StructUnion<SetpointStruct> _currentSetpointUnion;
        private SetpointsValidator<AllAddSetpointsStruct, AllAddCurrentDefensesStruct> _currentAddSetpointsValidator;
        private SetpointsValidator<FrequencySetpointsStruct, AllFrequencyDefensesStruct> _frequencySetpoints;
        private SetpointsValidator<VoltageSetpointsStruct, AllVoltageDefensesStruct> _voltageSetpoints;

        private StructUnion<ConfigurationStruct> _configurationUnion;

        #endregion [Validators]

        #endregion [Private fields]

        #region C'tor
        public ConfigurationForm()
        {
            this.InitializeComponent();
        }

        public ConfigurationForm(KL27Device device)
        {
            this.InitializeComponent();
            this._device = device;
            this._configuration = this._device.Configuration;
            this._configuration.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadConfigOk);
            this._configuration.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this.IsProcess = false;
                this._statusLabel.Text = ERROR_READ_CONFIG;
                MessageBox.Show(ERROR_READ_CONFIG);

            });
            this._configuration.ReadOk += HandlerHelper.CreateHandler(this, () => this._exchangeProgressBar.PerformStep());
            this._configuration.WriteOk += HandlerHelper.CreateHandler(this, () => this._exchangeProgressBar.PerformStep());
            this._configuration.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this.IsProcess = false;
                this._statusLabel.Text = ERROR_WRITE_CONFIG;
                MessageBox.Show(ERROR_WRITE_CONFIG);

            });
            this._configuration.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this._device.WriteConfiguration();
                this.IsProcess = false;
                this._statusLabel.Text = "������������ ��������";
            });
            this._exchangeProgressBar.Maximum = this._configuration.Slots.Count;
            this._currentSetpointsStruct = new ConfigurationStruct();
            this.Init();
        }

        private void Init()
        {
            ToolTip toolTip = new ToolTip();
            NewStructValidator<MeasureTransStruct> measureTransValidator = new NewStructValidator<MeasureTransStruct>
                (
                toolTip,
                new ControlInfoText(this._TT_Box, new CustomUshortRule(0, 5000)),
                new ControlInfoText(this._TTNP_Box, RulesContainer.UshortTo1000),
                new ControlInfoText(this._maxTok_Box, RulesContainer.Ustavka40),
                new ControlInfoCombo(this._TT_typeCombo, StringsConfig.TtType),
                new ControlInfoCombo(this._TN_typeCombo, StringsConfig.TnType),
                new ControlInfoText(this._TN_Box, RulesContainer.Ustavka128000),
                new ControlInfoCombo(this._TN_dispepairCombo, StringsConfig.LogicSignals),
                new ControlInfoText(this._TNNP_Box, RulesContainer.Ustavka128000),
                new ControlInfoCombo(this._TNNP_dispepairCombo, StringsConfig.LogicSignals),
                new ControlInfoCheck(this._ompCheckBox),
                new ControlInfoText(this._HUD_Box, RulesContainer.DoubleTo1)
                );

            NewStructValidator<ExternalSignalStruct> externalSignalsValidator = new NewStructValidator<ExternalSignalStruct>
                (
                toolTip,
                new ControlInfoCombo(this._keyOffCombo, StringsConfig.LogicSignals),
                new ControlInfoCombo(this._keyOnCombo, StringsConfig.LogicSignals),
                new ControlInfoCombo(this._extOffCombo, StringsConfig.LogicSignals),
                new ControlInfoCombo(this._extOnCombo, StringsConfig.LogicSignals),
                new ControlInfoCombo(this._signalizationCombo, StringsConfig.LogicSignals),
                new ControlInfoCombo(this._constraintGroupCombo, StringsConfig.LogicSignals),
                new ControlInfoCombo(this._blockSDTUcombo, StringsConfig.LogicSignals)
                );

            NewStructValidator<FaultStruct> faultValidator = new NewStructValidator<FaultStruct>
                (
                toolTip,
                new ControlInfoValidator(new NewCheckedListBoxValidator<FaultSignalStruct>(this._dispepairCheckList,StringsConfig.Faults)),
                new ControlInfoText(this._releDispepairBox, RulesContainer.IntTo3M)
                );

            NewStructValidator<SwitchStruct> switchValidator = new NewStructValidator<SwitchStruct>
                (
                toolTip,
                new ControlInfoCombo(this._switcherStateOffCombo, StringsConfig.LogicSignals),
                new ControlInfoCombo(this._switcherStateOnCombo, StringsConfig.LogicSignals),
                new ControlInfoCombo(this._switcherErrorCombo, StringsConfig.LogicSignals),
                new ControlInfoCombo(this._switcherBlockCombo, StringsConfig.LogicSignals),
                new ControlInfoText(this._switcherImpulseBox, RulesContainer.IntTo3M),
                new ControlInfoText(this._switcherDurationBox, RulesContainer.IntTo3M),
                new ControlInfoCombo(this._manageSignalsButtonCombo, StringsConfig.Zr),
                new ControlInfoCombo(this._manageSignalsKeyCombo, StringsConfig.Cr),
                new ControlInfoCombo(this._manageSignalsExternalCombo, StringsConfig.Cr),
                new ControlInfoCombo(this._manageSignalsSDTU_Combo, StringsConfig.Zr),
                new ControlInfoText(this._switcherTimeBox, RulesContainer.IntTo3M),
                new ControlInfoText(this._switcherTokBox, RulesContainer.Ustavka40)
                );

            NewDgwValidatior<AllExternalDefensesStruct, DefenseExternalStruct> externalDefenseValidatior = new NewDgwValidatior<AllExternalDefensesStruct, DefenseExternalStruct>
                (this._externalDefenseGrid,
                8,
                toolTip,
                new ColumnInfoCombo(StringsConfig.ExternalDefensesNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.Mode),
                new ColumnInfoCombo(StringsConfig.ExternalDefenceSignals), 
                new ColumnInfoCombo(StringsConfig.ExternalDefenceSignals), 
                new ColumnInfoText(RulesContainer.IntTo3M), 
                new ColumnInfoCheck(),
                new ColumnInfoCombo(StringsConfig.ExternalDefenceSignals),
                new ColumnInfoText(RulesContainer.IntTo3M),
                new ColumnInfoCheck(),
                new ColumnInfoCheck()
                )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._externalDefenseGrid,
                        new TurnOffRule(1, StringsConfig.Mode[0], true, 2, 3, 4, 5, 6, 7, 8, 9),
                        new TurnOffRule(5, false, false, 6, 7)
                        )
                }
            };

            Func<IValidatingRule> currentDefenseFunc = () =>
            {
                try
                {
                    if (this._tokDefenseGrid1[9, this._tokDefenseGrid1.CurrentCell.RowIndex].Value.ToString() ==
                        StringsConfig.FeatureLight[1])
                    {
                        return new CustomUshortRule(0, 4000);
                    }
                    return RulesContainer.IntTo3M;
                }
                catch (Exception)
                {
                    return RulesContainer.IntTo3M;
                }
            };

            NewStructValidator<CornerStruct> cornerValidator = new NewStructValidator<CornerStruct>
                (
                toolTip,
                new ControlInfoText(this._tokDefenseIbox, RulesContainer.UshortTo360),
                new ControlInfoText(this._tokDefenseI0box, RulesContainer.UshortTo360),
                new ControlInfoText(this._tokDefenseI2box, RulesContainer.UshortTo360),
                new ControlInfoText(this._tokDefenseInbox, RulesContainer.UshortTo360)
                );

            NewDgwValidatior<AllCurrentDefensesStruct, CurrentDefenseStruct> currentDefenseValidatior = new NewDgwValidatior<AllCurrentDefensesStruct, CurrentDefenseStruct>
                (
                new[] {this._tokDefenseGrid1, this._tokDefenseGrid2, this._tokDefenseGrid3},
                new[] {4, 4, 2},
                toolTip,
                new ColumnInfoCombo(StringsConfig.CurrentDefensesNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.Mode),  
                new ColumnInfoCombo(StringsConfig.LogicSignals),
                new ColumnInfoCheck(), 
                new ColumnInfoText(RulesContainer.Ustavka256), 
                new ColumnInfoCombo(StringsConfig.Direction), 
                new ColumnInfoCombo(StringsConfig.DirectionBlock), 
                new ColumnInfoCombo(StringsConfig.TokParameter, ColumnsType.COMBO, true, false, false), 
                new ColumnInfoCombo(StringsConfig.TokParameter2, ColumnsType.COMBO, false, true, true),
                new ColumnInfoText(RulesContainer.Ustavka40, true, true, false),      
                new ColumnInfoText(RulesContainer.Ustavka5, false, false, true),
                new ColumnInfoCombo(StringsConfig.FeatureLight, ColumnsType.COMBO, true, false, false),          
                new ColumnInfoTextDependent(currentDefenseFunc), 
                new ColumnInfoCheck(),
                new ColumnInfoText(RulesContainer.IntTo3M),
                new ColumnInfoCombo(StringsConfig.Osc)
                )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._tokDefenseGrid1,
                        new TurnOffRule(1, StringsConfig.Mode[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15),
                        new TurnOffRule(3, false, false, 4),
                        new TurnOffRule(5, StringsConfig.Direction[0], false, 6),
                        new TurnOffRule(13, false, false, 14)
                        ),
                    new TurnOffDgv
                        (
                        this._tokDefenseGrid2,
                        new TurnOffRule(1, StringsConfig.Mode[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15),
                        new TurnOffRule(3, false, false, 4),
                        new TurnOffRule(5, StringsConfig.Direction[0], false, 6),
                        new TurnOffRule(13, false, false, 14)
                        ),
                    new TurnOffDgv
                        (
                        this._tokDefenseGrid3,
                        new TurnOffRule(1, StringsConfig.Mode[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15),
                        new TurnOffRule(3, false, false, 4),
                        new TurnOffRule(5, StringsConfig.Direction[0], false, 6),
                        new TurnOffRule(13, false, false, 14)
                        )
                }
            };

            NewDgwValidatior<AllAddCurrentDefensesStruct, AddCurrentDefenseStruct> addCurrentDefenseValidator = 
                new NewDgwValidatior<AllAddCurrentDefensesStruct, AddCurrentDefenseStruct>
                (
                     //this._tokDefenseGrid4,
                     //2,
                    new[] { this._tokDefenseGrid4, this._tokDefenseGrid5},
                    new[] { 1, 1 },
                    toolTip,
                new ColumnInfoCombo(StringsConfig.AddCurrentDefensesNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.Mode), 
                new ColumnInfoCombo(StringsConfig.LogicSignals),
                new ColumnInfoCheck(true, false), 
                new ColumnInfoText(RulesContainer.Ustavka256, true, false), 
                new ColumnInfoText(RulesContainer.Ustavka5, true, false),    //4
                new ColumnInfoText(RulesContainer.Ustavka100, false, true), //4
                new ColumnInfoText(RulesContainer.IntTo3M, true, true),
                new ColumnInfoCheck(true, false),
                new ColumnInfoText(RulesContainer.IntTo3M, true, false), 
                new ColumnInfoCombo(StringsConfig.Osc) 
                )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._tokDefenseGrid4,
                        new TurnOffRule(1, StringsConfig.Mode[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10),
                        new TurnOffRule(3, false, false, 4),
                        new TurnOffRule(7, false, false, 8)
                        ),
                    new TurnOffDgv
                    (
                    this._tokDefenseGrid5,
                    new TurnOffRule(1, StringsConfig.Mode[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10)
                    )
                },
                //AddRules = new[]
                //{
                //    new TextboxCellRule(1, 5, RulesContainer.Ustavka100)
                //},
                //Disabled = new[] {new Point(3, 1), new Point(4, 1), new Point(7, 1), new Point(8, 1)}
            };


            this._currentSetpointUnion = new StructUnion<SetpointStruct>
                (
                cornerValidator,
                currentDefenseValidatior
                );

            this._currentSetpointsValidator = new SetpointsValidator<AllSetpointsStruct, SetpointStruct>
                (
                new RadioButtonSelector(this._mainRadioButtonGroup, this._reserveRadioButtonGroup), this._currentSetpointUnion
                );

            this._currentAddSetpointsValidator = new SetpointsValidator<AllAddSetpointsStruct, AllAddCurrentDefensesStruct>
                (
                new RadioButtonSelector(this._mainRadioButtonGroup, this._reserveRadioButtonGroup), addCurrentDefenseValidator
                );

            NewDgwValidatior<AllFrequencyDefensesStruct, FrequencyDefenseStruct> frequencyDefenseValidatior 
                = new NewDgwValidatior<AllFrequencyDefensesStruct, FrequencyDefenseStruct>
                (this._frequenceDefensesGrid, 2, toolTip,
                new ColumnInfoCombo(StringsConfig.FrequencyDefensesNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.Mode), 
                new ColumnInfoCombo(StringsConfig.LogicSignals), 
                new ColumnInfoText(RulesContainer.Ustavka40To60), 
                new ColumnInfoText(RulesContainer.IntTo3M),
                new ColumnInfoCheck(), 
                new ColumnInfoText(RulesContainer.Ustavka40To60), 
                new ColumnInfoText(RulesContainer.IntTo3M),
                new ColumnInfoCombo(StringsConfig.Osc),
                new ColumnInfoCheck()
                )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._frequenceDefensesGrid,
                        new TurnOffRule(1, StringsConfig.Mode[0], true, 2, 3, 4, 5, 6, 7, 8, 9),
                        new TurnOffRule(5, false, false, 6)
                        )
                }
            };

            this._frequencySetpoints = new SetpointsValidator<FrequencySetpointsStruct, AllFrequencyDefensesStruct>
                (
                new RadioButtonSelector(this._mainRadioButtonGroup, this._reserveRadioButtonGroup), frequencyDefenseValidatior
                );

            NewDgwValidatior<AllVoltageDefensesStruct, VoltageDefenseStruct> voltageDefenseValidatior = new NewDgwValidatior<AllVoltageDefensesStruct, VoltageDefenseStruct>
                (
                new[] {this._voltageDefensesGrid1, this._voltageDefensesGrid11, this._voltageDefensesGrid2, this._voltageDefensesGrid3},
                new[] {2, 2, 2, 2},
                toolTip,
                new ColumnInfoCombo(StringsConfig.VoltageDefensesNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.Mode), //0
                new ColumnInfoCombo(StringsConfig.LogicSignals), //1
                
                new ColumnInfoCombo(StringsConfig.VoltageParameterU, ColumnsType.COMBO, true, true, false, false), //1
                new ColumnInfoCombo(StringsConfig.VoltageParameter1, ColumnsType.COMBO, false, false, true, false), //1
                new ColumnInfoCombo(StringsConfig.VoltageParameter2, ColumnsType.COMBO, false, false, false, true), //1
                
                new ColumnInfoText(RulesContainer.Ustavka256), //2 
                new ColumnInfoText(RulesContainer.IntTo3M), //3
                new ColumnInfoCheck(), //4
                new ColumnInfoText(RulesContainer.Ustavka256), //6
                new ColumnInfoText(RulesContainer.IntTo3M), //7
                new ColumnInfoCombo(StringsConfig.Osc), 
                new ColumnInfoCheck(), //12
                new ColumnInfoCheck(false, true, false, false)
                )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._voltageDefensesGrid1,
                        new TurnOffRule(1, StringsConfig.Mode[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13),
                        new TurnOffRule(8, false, false, 9, 10)
                        ),
                    new TurnOffDgv
                        (
                        this._voltageDefensesGrid11,
                        new TurnOffRule(1, StringsConfig.Mode[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13),
                        new TurnOffRule(8, false, false, 9, 10)
                        ),
                    new TurnOffDgv
                        (
                        this._voltageDefensesGrid2,
                        new TurnOffRule(1, StringsConfig.Mode[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13),
                        new TurnOffRule(8, false, false, 9, 10)
                        ),
                    new TurnOffDgv
                        (
                        this._voltageDefensesGrid3,
                        new TurnOffRule(1, StringsConfig.Mode[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13),
                        new TurnOffRule(8, false, false, 9, 10)
                        )
                }
            };

            this._voltageSetpoints = new SetpointsValidator<VoltageSetpointsStruct, AllVoltageDefensesStruct>
                (
                new RadioButtonSelector(this._mainRadioButtonGroup, this._reserveRadioButtonGroup), voltageDefenseValidatior

                );

            #region [���]

            var vlsBoxes = new[]
            {
                this.VLScheckedListBox1, this.VLScheckedListBox2, this.VLScheckedListBox3, this.VLScheckedListBox4,
                this.VLScheckedListBox5, this.VLScheckedListBox6, this.VLScheckedListBox7, this.VLScheckedListBox8
            };
            NewCheckedListBoxValidator<OutputLogicStruct>[] vlsValidators = new NewCheckedListBoxValidator<OutputLogicStruct>[OutputLogicSignalStruct.LOGIC_COUNT];
            for (int i = 0; i < OutputLogicSignalStruct.LOGIC_COUNT; i++)
            {
                vlsValidators[i] = new NewCheckedListBoxValidator<OutputLogicStruct>(vlsBoxes[i], StringsConfig.VlsSignals);
            }
            StructUnion<OutputLogicSignalStruct> vlsUnion = new StructUnion<OutputLogicSignalStruct>(vlsValidators);

            #endregion [���]

            NewDgwValidatior<AllIndicatorsStruct, IndicatorsStruct> indicatorValidator = new NewDgwValidatior<AllIndicatorsStruct, IndicatorsStruct>
                (this._outputIndicatorsGrid,
                AllIndicatorsStruct.INDICATORS_COUNT,
                toolTip,
                new ColumnInfoCombo(StringsConfig.IndicatorNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.IndicatorType),
                new ColumnInfoCombo(StringsConfig.AllSignals),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck()
                );

            NewStructValidator<OscConfigStruct> oscValidator = new NewStructValidator<OscConfigStruct>
                (
                toolTip,
                new ControlInfoText(this.oscPercent, new CustomUshortRule(1, 100)),
                new ControlInfoCombo(this.oscFix, StringsConfig.OscFixation),
                new ControlInfoCombo(this.oscLength, StringsConfig.OscSize)
                );

            NewCheckedListBoxValidator<KeysStruct> keysValidator = new NewCheckedListBoxValidator<KeysStruct>(this._keysCheckedListBox, StringsConfig.KeyNumbers);

            this._configurationUnion = new StructUnion<ConfigurationStruct>
                (
                measureTransValidator,
                keysValidator,
                externalSignalsValidator, 
                faultValidator,
                switchValidator,
                externalDefenseValidatior, 
                this._currentSetpointsValidator,
                this._currentAddSetpointsValidator,
                this._frequencySetpoints, 
                this._voltageSetpoints, 
                vlsUnion, 
                indicatorValidator,
                oscValidator
                );

            this._groupSelector = new RadioButtonSelector(this._mainRadioButtonGroup, this._reserveRadioButtonGroup, this._ChangeGroupButton2);
            this._groupSelector.NeedCopyGroup += this._groupSelector_NeedCopyGroup;
        }
        #endregion C'tor

        #region Misc
        private void _groupSelector_NeedCopyGroup()
        {
            this.WriteConfiguration();
            //�������� � ���������, ����� ��������
            if (this._groupSelector.SelectedGroup == 0)
            {
                this._currentSetpointsStruct.AllAddSetpoints.Reserve = this._currentSetpointsStruct.AllAddSetpoints.Main.Clone<AllAddCurrentDefensesStruct>();

                this._currentSetpointsStruct.AllSetpoints.Reserv = this._currentSetpointsStruct.AllSetpoints.Main.Clone<SetpointStruct>();

                this._currentSetpointsStruct.AllVoltage.Reserve = this._currentSetpointsStruct.AllVoltage.Main.Clone<AllVoltageDefensesStruct>();

                this._currentSetpointsStruct.AllFrequency.Reserve = this._currentSetpointsStruct.AllFrequency.Main.Clone<AllFrequencyDefensesStruct>();
            }
            else
            {
                this._currentSetpointsStruct.AllAddSetpoints.Main = this._currentSetpointsStruct.AllAddSetpoints.Reserve.Clone<AllAddCurrentDefensesStruct>();

                this._currentSetpointsStruct.AllSetpoints.Main = this._currentSetpointsStruct.AllSetpoints.Reserv.Clone<SetpointStruct>();

                this._currentSetpointsStruct.AllVoltage.Main = this._currentSetpointsStruct.AllVoltage.Reserve.Clone<AllVoltageDefensesStruct>();

                this._currentSetpointsStruct.AllFrequency.Main = this._currentSetpointsStruct.AllFrequency.Reserve.Clone<AllFrequencyDefensesStruct>();
            }
        }

        private void ReadConfigOk()
        {
            this.IsProcess = false;
            this._statusLabel.Text = CONFIG_READ_OK;
            this._currentSetpointsStruct = this._configuration.Value;
            this._configurationUnion.Set(this._currentSetpointsStruct);
        }

        /// <summary>
        /// ������ ������
        /// </summary>
        private void StartReadConfiguration()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._exchangeProgressBar.Value = 0;
            if (!this._device.MB.IsDisconnect)
            {
                this._device.ReadConfiguration();
                this.IsProcess = true;
                this._configuration.LoadStruct();
            }
            else
            {
                MessageBox.Show(INVALID_PORT);
            }
        }

        /// <summary>
        /// ������ ��� ������ � ������
        /// </summary>
        private bool WriteConfiguration()
        {
            string message;

            if (this._configurationUnion.Check(out message, true))
            {
                this._currentSetpointsStruct = this._configurationUnion.Get();
                return true;
            }
            else
            {
                MessageBox.Show("���������� ������������ �������. ������������ �� ����� ���� ��������.");
                return false;
            }
        }

        private void _loadConfigBut_Click(object sender, EventArgs e)
        {
            this.ReadFromFile(); 
        }

        private void ReadFromFile()
        {
            this._exchangeProgressBar.Value = 0;
            if (DialogResult.OK == this._openConfigurationDlg.ShowDialog())
            {
                this.Deserialize(this._openConfigurationDlg.FileName, KL27);
            } 
        }

        private const string FILE_LOAD_FAIL = "���������� ��������� ����";

        /// <summary>
        /// �������� ������������ �� �����
        /// </summary>
        /// <param name="binFileName">��� �����</param>
        public void Deserialize(string binFileName, string head)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(binFileName);
                List<byte> values =new List<byte>(0);
                XmlNode a = doc.FirstChild.SelectSingleNode(string.Format("{0}_SET_POINTS", head));

                bool old = doc.DocumentElement.Name == "��700_�������" || doc.DocumentElement.Name == "��74X_�������";
                values.AddRange(old ? this.LoadOldConfig(doc, doc.DocumentElement.Name) : Convert.FromBase64String(a.InnerText));

                ConfigurationStruct configurationStruct = new ConfigurationStruct();               
                int size = configurationStruct.GetSize();
                if (values.Count < size)        // ��� ������ ������������ ����� ��������� ���� �� ������� ������� ����� �������
                {
                    int c = size - values.Count;
                    for (int i = 0; i < c; i++)
                    {
                        values.Add(0);
                    }
                }
                configurationStruct.InitStruct(values.ToArray());
                this._configurationUnion.Set(configurationStruct);
                this._statusLabel.Text = string.Format("���� {0} ������� ��������", binFileName);

            }
            catch
            {
                MessageBox.Show(FILE_LOAD_FAIL);
            }
        }

        private byte[] LoadOldConfig(XmlDocument doc, string docElement)
        {
            List<byte> result = new List<byte>();
            docElement = string.Format("/{0}/", docElement);
            result.AddRange(this.DeserializeSlot(doc, docElement + "�������_�������"));
            result.AddRange(this.DeserializeSlot(doc, docElement + "����������"));
            result.AddRange(this.DeserializeSlot(doc, docElement + "������_�������"));
            result.AddRange(this.DeserializeSlot(doc, docElement + "������_�������_��������"));
            result.AddRange(this.DeserializeSlot(doc, docElement + "������_�������_���������"));
            result.AddRange(this.DeserializeSlot(doc, docElement + "������_�������2_��������"));
            result.AddRange(this.DeserializeSlot(doc, docElement + "������_�������2_���������"));
            result.AddRange(this.DeserializeSlot(doc, docElement + "������_�������_��������"));
            result.AddRange(this.DeserializeSlot(doc, docElement + "������_�������_���������"));
            result.AddRange(this.DeserializeSlot(doc, docElement + "������_����������_��������"));
            result.AddRange(this.DeserializeSlot(doc, docElement + "������_����������_���������"));
            result.AddRange(this.DeserializeSlot(doc, docElement + "��������_�������"));
            result.AddRange(new byte[] {0, 0, 0, 0, 0, 0, 0, 0});
            try
            {
                // � ��������� ������� ������ ���� ��������
                result.AddRange(this.DeserializeSlot(doc, docElement + "������������_�����������"));
            }
            catch
            {
                // � ����� ������ ������� �������� �� ����
                result.AddRange(this.DeserializeSlot(doc, docElement + "������������_������������"));
            }
            byte[] res = result.ToArray();
            Common.SwapArrayItems(ref res);
            return res;
        }

        private byte[] DeserializeSlot(XmlDocument doc, string nodePath)
        {
            return Convert.FromBase64String(doc.SelectSingleNode(nodePath).InnerText);
        }

        private void _saveConfigBut_Click(object sender, EventArgs e)
        {
            this.SaveInFile();
        }

        private void SaveInFile()
        {
            string message;
            if (this._configurationUnion.Check(out message, true))
            {
                ConfigurationStruct currentStruct = this._configurationUnion.Get();
                this._saveConfigurationDlg.FileName = string.Format(KL_SETPOINTS_PATH, this._device.DeviceVersion);
                if (this._saveConfigurationDlg.ShowDialog() == DialogResult.OK)
                {
                    this.Serialize(this._saveConfigurationDlg.FileName, currentStruct, KL27);
                }

            }
            else
            {
                MessageBox.Show(message ?? "���������� ������������ �������. ������������ �� ����� ���� ���������.");
            }
        }
        
        private bool IsProcess
        {
            set
            {
                this._readConfigBut.Enabled = !value;
                this._writeConfigBut.Enabled = !value;
                this._resetConfigBut.Enabled = !value;
                this._loadConfigBut.Enabled = !value;
                this._saveConfigBut.Enabled = !value;
                this._saveToXmlButton.Enabled = !value;
            }
        }
        /// <summary>
        /// ���������� ������������ � ����
        /// </summary>
        public void Serialize(string fileName, StructBase config, string head)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.AppendChild(doc.CreateElement(head));

                ushort[] values = config.GetValues();

                XmlElement element = doc.CreateElement(string.Format("{0}_SET_POINTS", head));
                element.InnerText = Convert.ToBase64String(Common.TOBYTES(values, false));
                if (doc.DocumentElement == null)
                {
                    throw new NullReferenceException();
                }
                doc.DocumentElement.AppendChild(element);

                doc.Save(fileName);
                this._statusLabel.Text = string.Format("���� {0} ������� ��������", fileName);
            }
            catch
            {
                MessageBox.Show(FILE_SAVE_FAIL);
            }
        }

        private void _writeConfigBut_Click(object sender, EventArgs e)
        {
            this.WriteConfig();
        }

        private void WriteConfig()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            DialogResult result = MessageBox.Show(
                string.Format("�������� ������������ ��27 �{0}?", this._device.DeviceNumber), "������",
                MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                if (this.WriteConfiguration())
                {
                    this._statusLabel.Text = "��� ������ ������������";
                    this.IsProcess = true;
                    this._exchangeProgressBar.Value = 0;
                    this._configuration.Value = this._currentSetpointsStruct;
                    this._configuration.SaveStruct();
                }
            }
        }

        private void _readConfigBut_Click(object sender, EventArgs e)
        {
            this.StartReadConfiguration();
        }

        private void ConfigurationForm_Load(object sender, EventArgs e)
        {
            if(Device.AutoloadConfig)
                this.StartReadConfiguration();
        }

        private void _resetConfigBut_Click(object sender, EventArgs e)
        {
            this._configurationUnion.Reset();
        }

        private void Mr700ConfigurationFormV2_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = false;
            this._configuration.RemoveStructQueries();
        }

        private void _saveToXmlButton_Click(object sender, EventArgs e)
        {
            this.SaveToHtmlFile();
        }

        private void SaveToHtmlFile()
        {
            if (this.WriteConfiguration())
            {
                this._currentSetpointsStruct.DeviceVersion = this._device.DeviceVersion;
                this._currentSetpointsStruct.DeviceNumber = this._device.DeviceNumber.ToString();
            //    this._statusLabel.Text = HtmlExport.Export(Resources.MR700Main, Resources.MR700Res, this._currentSetpointsStruct, 
            //        this._currentSetpointsStruct.DeviceType, this._currentSetpointsStruct.DeviceVersion);
            }
        }

        private void _dispepairCheckList_SelectedValueChanged(object sender, EventArgs e)
        {
            CheckedListBox list = sender as CheckedListBox;
            int index = list.SelectedIndex;
            if (list.Items[index].ToString()
                    .IndexOf("������", StringComparison.CurrentCultureIgnoreCase) != -1)
            {
                (sender as CheckedListBox).SetItemChecked(index, false);
            }
        }

        private void _dispepairCheckList_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            CheckedListBox list = sender as CheckedListBox;
            int index = list.SelectedIndex;
            if (list.Items[index].ToString()
                    .IndexOf("������", StringComparison.CurrentCultureIgnoreCase) != -1)
            {
                (sender as CheckedListBox).SetItemChecked(index, false);
            }
        }

        #endregion

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(KL27Device); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(ConfigurationForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get
            {
                return Resources.config.ToBitmap();
            }
        }

        public string NodeName
        {
            get { return "������������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        private void contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ((ContextMenuStrip)sender).Close();
            
            if (e.ClickedItem == this.readFromDeviceItem)
            {
                this.StartReadConfiguration();
                return;
            }
            if (e.ClickedItem == this.writeToDeviceItem)
            {
                this.WriteConfig();
                return;
            }
            if (e.ClickedItem == this.clearSetpointsItem)
            {
                this._configurationUnion.Reset();
                return;
            }
            if (e.ClickedItem == this.readFromFileItem)
            {
                this.ReadFromFile();
                return;
            }
            if (e.ClickedItem == this.writeToFileItem)
            {
                this.SaveInFile();
                return;
            }

            if (e.ClickedItem == this.writeToHtmlItem)
            {
                this.SaveToHtmlFile();
            }
        }

        private void Mr700ConfigurationForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers != Keys.Control) return;
            switch (e.KeyCode)
            {
                case Keys.W:
                    this.WriteConfig();
                    break;
                case Keys.R:
                    this.StartReadConfiguration();
                    break;
                case Keys.S:
                    this.SaveInFile();
                    break;
                case Keys.O:
                    this.ReadFromFile();
                    break;
            }
            e.Handled = true;
        }
    }
}