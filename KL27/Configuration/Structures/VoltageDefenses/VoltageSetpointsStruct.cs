﻿using System.Linq;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;

namespace BEMN.KL27.Configuration.Structures.VoltageDefenses
{
    public class VoltageSetpointsStruct : StructBase, ISetpointContainer<AllVoltageDefensesStruct>
    {

        [Layout(0, Count = 2)]
        private AllVoltageDefensesStruct[] _groups;

        [XmlIgnore]
        public AllVoltageDefensesStruct[] Setpoints
        {
            get { return this._groups.Select(o => o.Clone<AllVoltageDefensesStruct>()).ToArray(); }
            set { this._groups = value; }
        }

        [XmlElement(ElementName = "Основная")]
        public AllVoltageDefensesStruct Main
        {
            get { return this._groups[0]; }
            set { this._groups[0] = value; }
        }
        [XmlElement(ElementName = "Резервная")]
        public AllVoltageDefensesStruct Reserve
        {
            get { return this._groups[1]; }
            set { this._groups[1] = value; }
        }
    }
}
