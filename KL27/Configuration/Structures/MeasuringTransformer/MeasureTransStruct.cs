﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.KL27.Configuration.Structures.MeasuringTransformer
{
    /// <summary>
    /// Конфигурациия измерительных трансформаторов
    /// </summary>
    public class MeasureTransStruct : StructBase
    {
        #region [Public field]

        [Layout(0)] private ushort _ttMode;//res
        [Layout(1)] private ushort _tt;
        [Layout(2)] private ushort _ttnp;
        [Layout(3)] private ushort _maxI;
        [Layout(4)] private ushort _startI;//res
        [Layout(5)] private ushort _res3;
        [Layout(6)] private ushort _res1;
        [Layout(7)] private ushort _res2;
        [Layout(8)] private ushort _tnMode;
        [Layout(9)] private ushort _tn;
        [Layout(10)] private ushort _tnFault;
        [Layout(11)] private ushort _tnnp;
        [Layout(12)] private ushort _tnnpFault;
        [Layout(13)] private ushort _opmMode;
        [Layout(14)] private ushort _rOpm;

        #endregion [Public field]

        [XmlElement(ElementName = "конфигурация_ТТ")]
        [BindingProperty(0)]
        public ushort Tt
        {
            get { return this._tt; }
            set { this._tt = value; }
        }
        [XmlElement(ElementName = "конфигурация_ТТНП")]
        [BindingProperty(1)]
        public ushort Ttnp
        {
            get { return this._ttnp; }
            set { this._ttnp = value; }
        }

        [XmlElement(ElementName = "Iм")]
        [BindingProperty(2)]
        public double MaxI
        {
            get { return ValuesConverterCommon.GetIn(this._maxI); }
            set { this._maxI = ValuesConverterCommon.SetIn(value); }

        }

        [XmlElement(ElementName = "тип_ТТ")]
        [BindingProperty(3)]
        public string TtMode
        {
            get { return Validator.Get(this._ttMode, StringsConfig.TtType); }
            set { this._ttMode = Validator.Set(value, StringsConfig.TtType); }
        }

        [XmlElement(ElementName = "тип_Uo")]
        [BindingProperty(4)]
        public string TnMode
        {
            get { return Validator.Get(this._tnMode, StringsConfig.TnType); }
            set { this._tnMode = Validator.Set(value, StringsConfig.TnType); }
        }

        [XmlElement(ElementName = "ТН")]
        [BindingProperty(5)]
        public double Tn
        {
            get { return ValuesConverterCommon.GetFactor(this._tn); }
            set { this._tn = ValuesConverterCommon.SetFactor(value); }
        }

        [XmlElement(ElementName = "Неисправность_ТН")]
        [BindingProperty(6)]
        public string TnFault
        {
            get { return Validator.Get(this._tnFault, StringsConfig.LogicSignals); }
            set { this._tnFault = Validator.Set(value, StringsConfig.LogicSignals); }
        }

        [XmlElement(ElementName = "ТННП")]
        [BindingProperty(7)]
        public double Tnnp
        {
            get { return ValuesConverterCommon.GetFactor(this._tnnp); }
            set { this._tnnp = ValuesConverterCommon.SetFactor(value); }
        }

        [XmlElement(ElementName = "Неисправность_ТННП")]
        [BindingProperty(8)]
        public string TnnpFault
        {
            get { return Validator.Get(this._tnnpFault, StringsConfig.LogicSignals); }
            set { this._tnnpFault = Validator.Set(value, StringsConfig.LogicSignals); }
        }

        [XmlElement(ElementName = "ОМП")]
        [BindingProperty(9)]
        public bool Omp
        {
            get { return Common.GetBit(this._opmMode, 0); }
            set { this._opmMode = Common.SetBit(this._opmMode, 0, value); }
        }

        [XmlElement(ElementName = "Xyd")]
        [BindingProperty(10)]
        public double Xyd
        {
            get { return this._rOpm/1000.0; }
            set { this._rOpm =(ushort) (value*1000); }
        }
    }
}
