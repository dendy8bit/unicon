﻿using System.Linq;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;

namespace BEMN.KL27.Configuration.Structures.FrequencyDefenses
{
    public class FrequencySetpointsStruct : StructBase, ISetpointContainer<AllFrequencyDefensesStruct>
    {

        [Layout(0, Count = 2)] private AllFrequencyDefensesStruct[] _groups;

        [XmlIgnore]
        public AllFrequencyDefensesStruct[] Setpoints
        {
            get { return this._groups.Select(o => o.Clone<AllFrequencyDefensesStruct>()).ToArray(); }
            set { this._groups = value; }
        }
        [XmlElement(ElementName = "Основная")]
        public AllFrequencyDefensesStruct Main
        {
            get { return this._groups[0]; }
            set { this._groups[0] = value; }
        }
        [XmlElement(ElementName = "Резервная")]
        public AllFrequencyDefensesStruct Reserve
        {
            get { return this._groups[1]; }
            set { this._groups[1] = value; }
        }
    }
}
