﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.KL27.Configuration.Structures.FrequencyDefenses
{
    public class AllFrequencyDefensesStruct : StructBase, IDgvRowsContainer<FrequencyDefenseStruct>
    {
        [Layout(0)] private FrequencyDefenseStruct _fMaxDefenseStruct;
        [Layout(1)] private FrequencyDefenseStruct _reserve1;
        [Layout(2)] private FrequencyDefenseStruct _fMinDefenseStruct;
        [Layout(3)] private FrequencyDefenseStruct _reserve2;

        public FrequencyDefenseStruct[] Rows
        {
            get
            {
                return new[]
                {
                    this._fMaxDefenseStruct.Clone<FrequencyDefenseStruct>(),
                    this._fMinDefenseStruct.Clone<FrequencyDefenseStruct>()
                };
            }
            set
            {
                this._fMaxDefenseStruct = value[0].Clone<FrequencyDefenseStruct>();
                this._fMinDefenseStruct = value[1].Clone<FrequencyDefenseStruct>();
            }
        }
    }
}
