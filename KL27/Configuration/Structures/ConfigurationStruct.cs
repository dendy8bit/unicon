﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.KL27.Configuration.Structures.CurrentDefenses;
using BEMN.KL27.Configuration.Structures.ExternalDefenses;
using BEMN.KL27.Configuration.Structures.ExternalSignals;
using BEMN.KL27.Configuration.Structures.FaultSignal;
using BEMN.KL27.Configuration.Structures.FrequencyDefenses;
using BEMN.KL27.Configuration.Structures.Indicators;
using BEMN.KL27.Configuration.Structures.Keys;
using BEMN.KL27.Configuration.Structures.MeasuringTransformer;
using BEMN.KL27.Configuration.Structures.OscConfig;
using BEMN.KL27.Configuration.Structures.Switch;
using BEMN.KL27.Configuration.Structures.Vls;
using BEMN.KL27.Configuration.Structures.VoltageDefenses;

namespace BEMN.KL27.Configuration.Structures
{
    [XmlRoot(ElementName = "КЛ27")]
    public class ConfigurationStruct : StructBase
    {
        public string DeviceVersion { get; set; }
        public string DeviceNumber { get; set; }
        public string DeviceType
        {
            get { return "КЛ27"; }
        }

        #region [Private fields]

        [Layout(0)] private MeasureTransStruct _measureTrans;
        [Layout(1)] private KeysStruct _keys;
        [Layout(2)] private ExternalSignalStruct _externalSignal;
        [Layout(3)] private FaultStruct _fault;
        [Layout(4, Count = 16)] private ushort[] _inputLogicSignal;
        [Layout(5)] private SwitchStruct _switch;
        [Layout(6, Count = 20)] private ushort[] _ApvAvrLzsh;
        [Layout(7)] private AllExternalDefensesStruct _allExternalDefenses;
        [Layout(8)] private AllSetpointsStruct _allSetpoints;
        [Layout(9)] private AllAddSetpointsStruct _allAddSetpoints;
        [Layout(10)] private FrequencySetpointsStruct _allFrequency;
        [Layout(11)] private VoltageSetpointsStruct _allVoltage;
        [Layout(12)] private OutputLogicSignalStruct _vls;
        [Layout(13, Count = 32)] private ushort[] _relay;
        [Layout(14)] private AllIndicatorsStruct _indicators;
        [Layout(15, Count = 4, Ignore = true)] ushort[] _reserve3; // конфигурация связи, нельзя перезаписывать
        [Layout(16)] private OscConfigStruct _oscConfig;

        #endregion [Private fields]



        #region [Properties]

        [XmlElement(ElementName = "Измерительный_трансформатор")]
        [BindingProperty(0)]
        public MeasureTransStruct MeasureTrans
        {
            get { return this._measureTrans; }
            set { this._measureTrans = value; }
        }

        [XmlElement(ElementName = "Ключи")]
        [BindingProperty(1)]
        public KeysStruct Keys
        {
            get { return this._keys; }
            set { this._keys = value; }
        }

        [XmlElement(ElementName = "Внешние_сигналы")]
        [BindingProperty(2)]
        public ExternalSignalStruct ExternalSignal
        {
            get { return this._externalSignal; }
            set { this._externalSignal = value; }
        }

        [XmlElement(ElementName = "Реле_неисправности")]
        [BindingProperty(3)]
        public FaultStruct Fault
        {
            get { return this._fault; }
            set { this._fault = value; }
        }

        [XmlElement(ElementName = "Конфигурация_выключателя")]
        [BindingProperty(4)]
        public SwitchStruct Switch
        {
            get { return this._switch; }
            set { this._switch = value; }
        }
        
        [BindingProperty(5)]
        [XmlElement(ElementName = "Внешние")]
        public AllExternalDefensesStruct AllExternalDefenses
        {
            get { return this._allExternalDefenses; }
            set { this._allExternalDefenses = value; }
        }

        [BindingProperty(6)]
        [XmlElement(ElementName = "Токовые")]
        public AllSetpointsStruct AllSetpoints
        {
            get { return this._allSetpoints; }
            set { this._allSetpoints = value; }
        }

        [BindingProperty(7)]
        [XmlElement(ElementName = "Токовые_дополнительные")]
        public AllAddSetpointsStruct AllAddSetpoints
        {
            get { return this._allAddSetpoints; }
            set { this._allAddSetpoints = value; }
        }

        [BindingProperty(8)]
        [XmlElement(ElementName = "Частотные")]
        public FrequencySetpointsStruct AllFrequency
        {
            get { return this._allFrequency; }
            set { this._allFrequency = value; }
        }

        [BindingProperty(9)]
        [XmlElement(ElementName = "Напряжения")]
        public VoltageSetpointsStruct AllVoltage
        {
            get { return this._allVoltage; }
            set { this._allVoltage = value; }
        }

        [XmlElement(ElementName = "Все_ВЛС")]
        [BindingProperty(10)]
        public OutputLogicSignalStruct Vls
        {
            get { return this._vls; }
            set { this._vls = value; }
        }

        [XmlElement(ElementName = "Индикаторы")]
        [BindingProperty(11)]
        public AllIndicatorsStruct Indicators
        {
            get { return this._indicators; }
            set { this._indicators = value; }
        }

        [XmlElement(ElementName = "Осц")]
        [BindingProperty(12)]
        public OscConfigStruct SystemConfig
        {
            get { return this._oscConfig; }
            set { this._oscConfig = value; }
        }

        #endregion [Properties]
    }
}
