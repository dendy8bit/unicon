﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.KL27.Configuration.Structures.ExternalSignals
{
    public class ExternalSignalStruct : StructBase
    {
        [Layout(0)] private ushort _keyOn;
        [Layout(1)] private ushort _keyOff;
        [Layout(2)] private ushort _extOn;
        [Layout(3)] private ushort _extOff;
        [Layout(4)] private ushort _inpClear;
        [Layout(5)] private ushort _inpGroup;
        [Layout(6)] private ushort _inpOsc;
        [Layout(7)] private ushort _res1;
        [Layout(8)] private ushort _inpBlockSDTU;
        [Layout(9, Count = 4)] ushort[] _res2;

        [BindingProperty(0)]
        [XmlElement(ElementName = "ключ_ВКЛ")]

        public string KeyOff
        {
            get { return Validator.Get(this._keyOff, StringsConfig.LogicSignals); }
            set { this._keyOff = Validator.Set(value, StringsConfig.LogicSignals); }
        }

        [BindingProperty(1)]
        [XmlElement(ElementName = "ключ_ВЫКЛ")]
        public string KeyOn
        {
            get { return Validator.Get(this._keyOn, StringsConfig.LogicSignals); }
            set { this._keyOn = Validator.Set(value, StringsConfig.LogicSignals); }
        }

        [BindingProperty(2)]
        [XmlElement(ElementName = "вход_внеш_включить")]

        public string ExtOff
        {
            get { return Validator.Get(this._extOff, StringsConfig.LogicSignals); }
            set { this._extOff = Validator.Set(value, StringsConfig.LogicSignals); }
        }

        [BindingProperty(3)]
        [XmlElement(ElementName = "вход_внеш_выключить")]
        public string ExtOn
        {
            get { return Validator.Get(this._extOn, StringsConfig.LogicSignals); }
            set { this._extOn = Validator.Set(value, StringsConfig.LogicSignals); }
        }

        [BindingProperty(4)]
        [XmlElement(ElementName = "сброс_сигнализации")]
        public string InpClear
        {
            get { return Validator.Get(this._inpClear, StringsConfig.LogicSignals); }
            set { this._inpClear = Validator.Set(value, StringsConfig.LogicSignals); }
        }

        [BindingProperty(5)]
        [XmlElement(ElementName = "группа_уставок")]
        public string InpGroup
        {
            get { return Validator.Get(this._inpGroup, StringsConfig.LogicSignals); }
            set { this._inpGroup = Validator.Set(value, StringsConfig.LogicSignals); }
        }

        [BindingProperty(6)]
        [XmlElement(ElementName = "блокировка_СДТУ")]
        public string BlockSdtu
        {
            get { return Validator.Get(this._inpBlockSDTU, StringsConfig.LogicSignals); }
            set { this._inpBlockSDTU = Validator.Set(value, StringsConfig.LogicSignals); }
        }
    }
}
