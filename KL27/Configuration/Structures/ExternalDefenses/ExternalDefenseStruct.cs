﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.KL27.Configuration.Structures.ExternalDefenses
{
    public class DefenseExternalStruct : StructBase
    {
        [Layout(0)] private ushort _config; //конфигурация выведено/введено 
        [Layout(1)] private ushort _block; //вход блокировки
        [Layout(2)] private ushort _srab;//конфигурация дополнительная
        [Layout(3)] private ushort _ust; //уставка срабатывания
        [Layout(4)] private ushort _time; //время срабатывания
        [Layout(5)] private ushort _u; //уставка возврата
      

        [BindingProperty(0)]
        [XmlElement(ElementName = "Режим")]
        public string Mode
        {
            get { return Validator.Get(this._config, StringsConfig.Mode, 0, 1); }
            set { this._config = Validator.Set(value, StringsConfig.Mode, this._config, 0, 1); }
        }
        
        [BindingProperty(1)]
        [XmlElement(ElementName = "Блокировка")]
        public string Bloc
        {
            get { return Validator.Get(this._block, StringsConfig.ExternalDefenceSignals); }
            set { this._block = Validator.Set(value, StringsConfig.ExternalDefenceSignals); }
        }
        
        [BindingProperty(2)]
        [XmlElement(ElementName = "Вход_срабатывания")]
        public string Srab
        {
            get { return Validator.Get(this._srab, StringsConfig.ExternalDefenceSignals); }
            set { this._srab = Validator.Set(value, StringsConfig.ExternalDefenceSignals); }
        }
        
        [BindingProperty(3)]
        [XmlElement(ElementName = "Уставка_срабатывания")]
        public int TimeSrab
        {
            get { return ValuesConverterCommon.GetWaitTime(this._ust); }
            set { this._ust = ValuesConverterCommon.SetWaitTime(value); }
        }
        
        [BindingProperty(4)]
        [XmlElement(ElementName = "Возврат")]
        public bool Return
        {
            get { return Common.GetBit(this._config, 14); }
            set { this._config = Common.SetBit(this._config, 14, value); }
        }
        
        [BindingProperty(5)]
        [XmlElement(ElementName = "Вход_ВЗ")]
        public string InputReturn
        {
            get { return Validator.Get(this._time, StringsConfig.ExternalDefenceSignals); }
            set { this._time = Validator.Set(value, StringsConfig.ExternalDefenceSignals); }
        }
        
        [BindingProperty(6)]
        [XmlElement(ElementName = "Уставка_ВЗ")]
        public int UstavkaReturn
        {
            get { return ValuesConverterCommon.GetWaitTime(this._u); }
            set { this._u = ValuesConverterCommon.SetWaitTime(value); }
        }

        [BindingProperty(7)]
        [XmlElement(ElementName = "Осц")]
        public bool Osc
        {
            get { return Common.GetBit(this._config, 4); }
            set { this._config = Common.SetBit(this._config, 4, value); }
        }

        [BindingProperty(8)]
        [XmlElement(ElementName = "Сброс_ступени")]
        public bool Reset
        {
            get { return Common.GetBit(this._config, 13); }
            set { this._config = Common.SetBit(this._config, 13, value); }
        }
    }
}
