<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- Edited by XMLSpy� -->
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <html>
      <head>

      </head>
      <body>
        <h3>���������� ��27. ������ ������</h3>
        <p></p>
        <table border="1">
          <tr bgcolor="#c1ced5">
            <th>�����</th>
            <th width="120">����/�����</th>
            <th>���������</th>
            <th>�������</th>
            <th>��� �����������</th>
            <th>�������� ������������</th>
            <th>�������� ��������� ������������</th>
            <th>������</th>
            <th width="60">Ia</th>
            <th width="60">Ib</th>
            <th width="60">Ic</th>
            <th width="60">I0</th>
            <th width="60">I1</th>
            <th width="60">I2</th>
            <th width="60">In</th> 
            <th width="60">I�</th>
            <th width="60">F</th>
            <th width="60">Uab</th>
            <th width="60">Ubc</th>
            <th width="60">Uca</th>
            <th width="60">U0</th>
            <th width="60">U1</th>
            <th width="60">U2</th>
            <th width="60">Un</th>
            <th>��. ������� 1-16</th>
          </tr>
          <xsl:for-each select="DocumentElement/��700_������_������">
            <tr align="center">
              <td><xsl:value-of select="_indexCol"/></td>
              <td><xsl:value-of select="_timeCol"/></td>
              <td><xsl:value-of select="_msg1Col"/></td>
              <td><xsl:value-of select="_msgCol"/></td>
              <td><xsl:value-of select="_typeDamage"/></td>
              <td><xsl:value-of select="_paramCol"/></td>
              <td><xsl:value-of select="_valueCol"/></td>
              <td><xsl:value-of select="_groupCol"/></td>
              <td><xsl:value-of select="_IaCol"/></td>
              <td><xsl:value-of select="_IbCol"/></td>
              <td><xsl:value-of select="_IcCol"/></td>
              <td><xsl:value-of select="_I0Col"/></td>
              <td><xsl:value-of select="_I1Col"/></td>
              <td><xsl:value-of select="_I2Col"/></td>
              <td><xsl:value-of select="_InCol"/></td>
              <td><xsl:value-of select="_IgCol"/></td>
              <td><xsl:value-of select="_fColumn"/></td>
              <td><xsl:value-of select="_uabColumn"/></td>
              <td><xsl:value-of select="_ubcColumn"/></td>
              <td><xsl:value-of select="_ucaColumn"/></td>
              <td><xsl:value-of select="_u0Column"/></td>
              <td><xsl:value-of select="_u1Column"/></td>
              <td><xsl:value-of select="_u2Column"/></td>
              <td><xsl:value-of select="_unColumn"/></td>
              <td><xsl:value-of select="_D0Col"/></td>
            </tr>
          </xsl:for-each>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
