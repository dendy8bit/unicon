﻿using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms.Export;
using BEMN.Interfaces;
using BEMN.KL27.Properties;
using BEMN.KL27.SystemJournal.Structures;

namespace BEMN.KL27.SystemJournal
{
    public partial class SystemJournalForm : Form, IFormView
    {
        #region [Constants]
        private const string RECORDS_IN_JOURNAL = "Записей в журнале - {0}";
        private const string JOURNAL_SAVED = "Журнал сохранён";
        private const string READ_FAIL = "Невозможно прочитать журнал";
        private const string TABLE_NAME_SYS = "КЛ27_журнал_системы";
        private const string NUMBER_SYS = "Номер";
        private const string TIME_SYS = "Время";
        private const string MESSAGE_SYS = "Сообщение";
        private const string SYSTEM_JOURNAL = "Журнал системы";
        private const string JOURNAL_READDING = "Идёт чтение журнала";

        #endregion [Constants]

        #region [Private fields]
        private readonly MemoryEntity<SystemJournalStruct> _systemJournal;
        private DataTable _dataTable;
        private int _recordNumber;
        private KL27Device _device;
        #endregion [Private fields]

        public SystemJournalForm()
        {
            this.InitializeComponent();
        }

        public SystemJournalForm(KL27Device device)
        {
            this.InitializeComponent();
            this._device = device;

            this._systemJournal = device.SystemJournal;
            this._systemJournal.ReadOk +=  HandlerHelper.CreateHandler(this, this._configProgressBar.PerformStep);
            this._systemJournal.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadRecord);
            this._systemJournal.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadJournal);

            this._configProgressBar.Maximum = this._systemJournal.Slots.Count;

            this._dataTable = this.GetJournalDataTable();
            this._systemJournalGrid.DataSource = this._dataTable;
        }

        #region [Properties]
        /// <summary>
        /// Счётчик сообщений
        /// </summary>
        public int RecordNumber
        {
            get { return this._recordNumber; }
            set
            {
                this._recordNumber = value;
                this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._recordNumber);
                this.statusStrip1.Update();
            }
        }
        #endregion [Properties]


        #region [Help members]
        /// <summary>
        /// Устанавливает св-во Enabled для всех кнопок
        /// </summary>
        /// <param name="enabled"></param>
        private void SetButtonsState(bool enabled)
        {
            this._readJournalButton.Enabled = enabled;
            this._saveJournalButton.Enabled = enabled;
            this._loadJournalButton.Enabled = enabled;
            this._exportButton.Enabled = enabled;
        }

        private void FailReadJournal()
        {               
            this.SetButtonsState(true);
            this.RecordNumber = 0;
            this._statusLabel.Text = READ_FAIL;
        }

        private void ReadRecord()
        {
            int i = 0;
            foreach (SystemJournalRecordStruct record in this._systemJournal.Value._records.TakeWhile(record => !record.IsEmpty))
            {
                this._dataTable.Rows.Add(i + 1, record.GetRecordTime, record.GetRecordMessage);
                i++;
            }
            this.RecordNumber = this._dataTable.Rows.Count;
            this.SetButtonsState(true);
        }

        private DataTable GetJournalDataTable()
        {
            var table = new DataTable(TABLE_NAME_SYS);
            table.Columns.Add(NUMBER_SYS);
            table.Columns.Add(TIME_SYS);
            table.Columns.Add(MESSAGE_SYS);
            return table;
        }

        private void SaveJournalToFile()
        {
            if (DialogResult.OK == this._saveJournalDialog.ShowDialog())
            {
                this._dataTable.WriteXml(this._saveJournalDialog.FileName);
                this._statusLabel.Text = JOURNAL_SAVED;
            }
        }

        private void LoadJournalFromFile()
        {

            if (DialogResult.OK == this._openJournalDialog.ShowDialog())
            {
                this._dataTable.Clear();
                this._dataTable.ReadXml(this._openJournalDialog.FileName);
            }
            this.RecordNumber = this._dataTable.Rows.Count;
        }
        #endregion [Help members]


        #region [Events Handlers]
        private void Mr700SystemJournalForm_Load(object sender, EventArgs e)
        {
            this.StartReadJournal();
        }

        private void _readJournalButton_Click(object sender, EventArgs e)
        {
            this.StartReadJournal();
        }

        private void StartReadJournal()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._dataTable.Clear();
            this._configProgressBar.Value = 0;
            this.RecordNumber = 0;
            this.SetButtonsState(false);
            this._statusLabel.Text = JOURNAL_READDING;
            this._systemJournal.LoadStruct();
        }

        private void _saveJournalButton_Click(object sender, EventArgs e)
        {
            this.SaveJournalToFile();
        }

        private void _loadJournalButton_Click(object sender, EventArgs e)
        {
            this.LoadJournalFromFile();
        }
        #endregion [Events Handlers]

        private void _exportButton_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == this._saveJournalHtmlDialog.ShowDialog())
            {
                HtmlExport.Export(this._dataTable, this._saveJournalHtmlDialog.FileName, Resources.KL27JS);
               this._statusLabel.Text = JOURNAL_SAVED;
            }
        }

        #region [IFormView Members]
        public Type FormDevice
        {
            get { return typeof(KL27Device); }
        }

        public bool Multishow { get; private set; }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public Type ClassType
        {
            get { return typeof(SystemJournalForm); }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.js; }
        }

        public string NodeName
        {
            get { return SYSTEM_JOURNAL; }
        }
        #endregion [IFormView Members]
    }
}
