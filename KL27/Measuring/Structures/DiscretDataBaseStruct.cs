﻿using System.Collections;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.KL27.Measuring.Structures
{
    /// <summary>
    /// МР 550 Дискретная база данных
    /// </summary>
    public class DiscretDataBaseStruct : StructBase
    {
        #region [Constants]

        private const int BASE_SIZE = 28;

        #endregion [Constants]

        #region [Private fields]

        [Layout(0, Count = BASE_SIZE)] private ushort[] _base; //бд общаяя

        #endregion [Private fields]

        #region [Properties]

        private string GetSign(int higt, int low)
        {
            if (Common.GetBit(this._base[17], higt))
            {
                return string.Empty;
            }
            return Common.GetBit(this._base[17], low) ? "-" : "+";
        }

        public string InSign
        {
            get { return this.GetSign(1, 0); }
        }

        public string IaSign
        {
            get { return this.GetSign(3, 2); }
        }

        public string IbSign
        {
            get { return this.GetSign(5, 4); }
        }

        public string IcSign
        {
            get { return this.GetSign(7, 6); }
        }

        public string I0Sign
        {
            get { return this.GetSign(9, 8); }
        }

        public string I1Sign
        {
            get { return this.GetSign(11, 10); }
        }

        public string I2Sign
        {
            get { return this.GetSign(13, 12); }
        }

        public BitArray ManageSignals
        {
            get
            {
                bool[] manageSign = Common.GetBitsArray(this._base, 0, 9);
                manageSign[9] &= !this.FaultSignals[15]; // работа СПЛ
                return new BitArray(manageSign);
            }
        }

        public BitArray AdditionalSignals
        {
            get
            {
                BitArray temp = new BitArray(new[] {Common.LOBYTE(this._base[2])});
                BitArray ret = new BitArray(4);
                for (int i = 0; i < 4; i++)
                {
                    ret[i] = temp[i + 4];
                }
                return ret;
            }
        }

        public BitArray Indicators
        {
            get { return new BitArray(new[] {Common.HIBYTE(this._base[2])}); }
        }

        public BitArray InputSignals
        {
            get
            {
                return new BitArray(new[]
                {
                    Common.LOBYTE(this._base[0x9]),
                    Common.HIBYTE(this._base[0x9]),
                    Common.LOBYTE(this._base[0xA])
                });
            }
        }

        public BitArray OutputSignals
        {
            get { return new BitArray(new[] {Common.HIBYTE(this._base[0xA])}); }
        }

        public BitArray Rele
        {
            get { return new BitArray(new[] {Common.LOBYTE(this._base[3])}); }
        }

        public BitArray LimitSignals
        {
            get
            {
                return new BitArray(new[]
                {
                    Common.LOBYTE(this._base[0xB]),
                    Common.HIBYTE(this._base[0xB]),
                    Common.LOBYTE(this._base[0xC]),
                    Common.HIBYTE(this._base[0xC]),
                    Common.LOBYTE(this._base[0xD]),
                    Common.HIBYTE(this._base[0xD]),
                    Common.LOBYTE(this._base[0xE])
                });
            }
        }
        
        public BitArray FaultSignals
        {
            get { return new BitArray(new[] {Common.LOBYTE(this._base[5]), Common.HIBYTE(this._base[5])}); }
        }

        public BitArray FaultState
        {
            get { return new BitArray(new[] {Common.LOBYTE(this._base[4])}); }
        }

        #endregion [Properties]
    }
}
