using System;
using System.Drawing;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms;
using BEMN.Forms.MeasuringClasses;
using BEMN.Interfaces;
using BEMN.KL27.Configuration.Structures.MeasuringTransformer;
using BEMN.KL27.Measuring.Structures;
using BEMN.KL27.Properties;

namespace BEMN.KL27.Measuring
{
    public partial class MeasuringForm : Form, IFormView
    {
        private KL27Device _device;
        private LedControl[] _manageLeds;
        private LedControl[] _additionalLeds;
        private LedControl[] _indicatorLeds;
        private LedControl[] _inputLeds;
        private LedControl[] _outputLeds;
        private LedControl[] _releLeds;
        private LedControl[] _limitLeds;
        private LedControl[] _faultStateLeds;
        private LedControl[] _faultSignalsLeds;
        private MemoryEntity<DiscretDataBaseStruct> _discretDataBase;
        private MemoryEntity<DateTimeStruct> _dateTime;
        private MemoryEntity<AnalogDataBaseStruct> _analogDataBase;
        private readonly AveragerTime<AnalogDataBaseStruct> _averagerTime;
        private MemoryEntity<MeasureTransStruct> _measureTrans;
        private MeasureTransStruct _measureTransStruct;
        
        public MeasuringForm()
        {
            this.InitializeComponent();
        }

        public MeasuringForm(KL27Device device)
        {
            this.InitializeComponent();
            this._device = device;
            this._device.ConnectionModeChanged += this.StartStopRead;

            this._dateTime = device.DateAndTime;
            this._dateTime.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.DateTimeLoad);

            this._discretDataBase = device.DiscretDataBase;
            this._discretDataBase.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.DiscretBdReadOk);
            this._discretDataBase.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.DiscretBdReadFail);

            this._analogDataBase = device.AnalogDataBase;
            this._analogDataBase.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.OnAnalogSignalsLoadOk);
            this._analogDataBase.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.AnalogBdReadFail);

            this._measureTrans = device.MeasureTrans;
            this._measureTrans.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.MeasureTransReadOk);
            this._measureTrans.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.AnalogBdReadFail);

            this._averagerTime = new AveragerTime<AnalogDataBaseStruct>(500);
            this._averagerTime.Tick += this.AveragerTimeTick;
            this.Init();
        }
        
        private void MeasureTransReadOk()
        {
            this._measureTransStruct = this._measureTrans.Value;
            this._analogDataBase.LoadStruct();
        }

        private void AveragerTimeTick()
        {
            this._InBox.Text = this._discretDataBase.Value.InSign+ this._analogDataBase.Value.GetIn(this._averagerTime.ValueList, this._measureTransStruct);
            this._IaBox.Text = this._discretDataBase.Value.IaSign + this._analogDataBase.Value.GetIa(this._averagerTime.ValueList, this._measureTransStruct);
            this._IbBox.Text = this._discretDataBase.Value.IbSign + this._analogDataBase.Value.GetIb(this._averagerTime.ValueList, this._measureTransStruct);
            this._IcBox.Text = this._discretDataBase.Value.IcSign + this._analogDataBase.Value.GetIc(this._averagerTime.ValueList, this._measureTransStruct);
            this._I1Box.Text = this._discretDataBase.Value.I1Sign + this._analogDataBase.Value.GetI1(this._averagerTime.ValueList, this._measureTransStruct);
            this._I2Box.Text = this._discretDataBase.Value.I2Sign + this._analogDataBase.Value.GetI2(this._averagerTime.ValueList, this._measureTransStruct);
            this._I0Box.Text = this._discretDataBase.Value.I0Sign + this._analogDataBase.Value.GetI0(this._averagerTime.ValueList, this._measureTransStruct);
            this._IgBox.Text = this._analogDataBase.Value.GetIg(this._averagerTime.ValueList, this._measureTransStruct);
            
            this._UnBox.Text = this._analogDataBase.Value.GetUn(this._averagerTime.ValueList, this._measureTransStruct);
            this._UaBox.Text = this._analogDataBase.Value.GetUa(this._averagerTime.ValueList, this._measureTransStruct);
            this._UbBox.Text = this._analogDataBase.Value.GetUb(this._averagerTime.ValueList, this._measureTransStruct);
            this._UcBox.Text = this._analogDataBase.Value.GetUc(this._averagerTime.ValueList, this._measureTransStruct);
            this._UabBox.Text = this._analogDataBase.Value.GetUab(this._averagerTime.ValueList, this._measureTransStruct);
            this._UbcBox.Text = this._analogDataBase.Value.GetUbc(this._averagerTime.ValueList, this._measureTransStruct);
            this._UcaBox.Text = this._analogDataBase.Value.GetUca(this._averagerTime.ValueList, this._measureTransStruct);
            this._U0Box.Text = this._analogDataBase.Value.GetU0(this._averagerTime.ValueList, this._measureTransStruct);
            this._U1Box.Text = this._analogDataBase.Value.GetU1(this._averagerTime.ValueList, this._measureTransStruct);
            this._U2Box.Text = this._analogDataBase.Value.GetU2(this._averagerTime.ValueList, this._measureTransStruct);

            this._PBox.Text = this._analogDataBase.Value.GetP(this._averagerTime.ValueList, this._measureTransStruct);
            this._QBox.Text = this._analogDataBase.Value.GetQ(this._averagerTime.ValueList, this._measureTransStruct);
            this._CosBox.Text = this._analogDataBase.Value.GetCosF(this._averagerTime.ValueList);
            this._F_Box.Text = this._analogDataBase.Value.GetF(this._averagerTime.ValueList);
        }

        private void AnalogBdReadFail()
        {
            this._InBox.Text = string.Empty;
            this._IaBox.Text = string.Empty;
            this._IbBox.Text = string.Empty;
            this._IcBox.Text = string.Empty;
            this._I1Box.Text = string.Empty;
            this._I2Box.Text = string.Empty;
            this._I0Box.Text = string.Empty;
            this._IgBox.Text = string.Empty;
            
            this._UnBox.Text = string.Empty;
            this._UaBox.Text = string.Empty;
            this._UbBox.Text = string.Empty;
            this._UcBox.Text = string.Empty;
            this._UabBox.Text = string.Empty;
            this._UbcBox.Text = string.Empty;
            this._UcaBox.Text = string.Empty;
            this._U0Box.Text = string.Empty;
            this._U1Box.Text = string.Empty;
            this._U2Box.Text = string.Empty;

            this._PBox.Text = string.Empty;
            this._QBox.Text = string.Empty;
            this._CosBox.Text = string.Empty;
            this._F_Box.Text = string.Empty;
        }

        private void DateTimeLoad()
        {
            this._dateTimeControl.DateTime = this._dateTime.Value;
        }

        private void Init()
        {
            this._manageLeds = new[]
            {
                this._manageLed1, this._manageLed2, this._manageLed3, this._manageLed4, this._manageLed5,
                this._manageLed6, this._manageLed7, this._manageLed8, new LedControl(), this._manageLed9
            };
            this._additionalLeds = new[] {this._addIndLed1, this._addIndLed2, this._addIndLed3, this._addIndLed4};
            this._indicatorLeds = new[]
            {
                this._indLed1, this._indLed2, this._indLed3, this._indLed4, this._indLed5, this._indLed6
            };
            this._inputLeds = new[]
            {
                this._inD_led1, this._inD_led2, this._inD_led3, this._inD_led4, this._inD_led5, this._inD_led6,
                this._inD_led7, this._inD_led8, this._inD_led9, this._inD_led10, this._inD_led11, this._inD_led12,
                this._inD_led13, this._inD_led14, this._inD_led15, this._inD_led16, this._inD_led17, this._inD_led18,
                this._inD_led19, this._inD_led20, this._inD_led21, this._inD_led22, this._inD_led23, this._inD_led24
            };
            this._outputLeds = new[]
            {
                this._outLed1, this._outLed2, this._outLed3, this._outLed4, this._outLed5, this._outLed6, this._outLed7,
                this._outLed8
            };
            this._releLeds = new[]
            {
                this._releLed1, this._releLed2, this._releLed3, this._releLed4, this._releLed5, this._releLed6,
                this._releLed7, this._releLed8
            };

            this._limitLeds = new[]
            {
                this._ImaxLed1, this._ImaxLed2, this._ImaxLed3, this._ImaxLed4, this._ImaxLed5, this._ImaxLed6,
                this._ImaxLed7, this._ImaxLed8, this._I0maxLed1, this._I0maxLed2, this._I0maxLed3, this._I0maxLed4,
                this._I0maxLed5, this._I0maxLed6, this._I0maxLed7, this._I0maxLed8, this._InmaxLed1, this._InmaxLed2,
                this._InmaxLed3, this._InmaxLed4, this._InmaxLed5, this._InmaxLed6, this._InmaxLed7, this._InmaxLed8,
                this._FmaxLed1, this._FmaxLed2, this._FmaxLed3, this._FmaxLed4, this._inD_led25, this._inD_led26,
                this._inD_led27, new LedControl(), this._UmaxLed1, this._UmaxLed2, this._UmaxLed3, this._UmaxLed4,
                this._UmaxLed5, this._UmaxLed6, this._UmaxLed7, this._UmaxLed8, this._U0maxLed1, this._U0maxLed2,
                this._U0maxLed3, this._U0maxLed4, this._U0maxLed5, this._U0maxLed6, this._U0maxLed7, this._U0maxLed8,
                this._extDefenseLed1, this._extDefenseLed2, this._extDefenseLed3, this._extDefenseLed4,
                this._extDefenseLed5, this._extDefenseLed6, this._extDefenseLed7, this._extDefenseLed8
            };
            this._faultStateLeds = new[]
            {
                this._faultStateLed1, this._faultStateLed2, this._faultStateLed3, this._faultStateLed4,
                this._faultStateLed5, this._faultStateLed6, this._faultStateLed7, this._faultStateLed8
            };
            this._faultSignalsLeds = new[]
            {
                this._faultSignalLed1, this._faultSignalLed2, this._faultSignalLed3, this._faultSignalLed4,
                this._faultSignalLed5, this._faultSignalLed6, this._faultSignalLed7, this._faultSignalLed8,
                this._faultSignalLed9, this._faultSignalLed10, this._faultSignalLed11, this._faultSignalLed12,
                this._faultSignalLed13, this._faultSignalLed14, this._faultSignalLed15, this._faultSignalLed16
            };
        }

        private void DiscretBdReadFail()
        {
            LedManager.TurnOffLeds(this._manageLeds);
            LedManager.TurnOffLeds(this._additionalLeds);
            LedManager.TurnOffLeds(this._indicatorLeds);
            LedManager.TurnOffLeds(this._inputLeds);
            LedManager.TurnOffLeds(this._outputLeds);
            LedManager.TurnOffLeds(this._releLeds);
            LedManager.TurnOffLeds(this._limitLeds);
            LedManager.TurnOffLeds(this._faultSignalsLeds);
            LedManager.TurnOffLeds(this._faultStateLeds);
        }

        private void DiscretBdReadOk()
        {
            LedManager.SetLeds(this._manageLeds, this._discretDataBase.Value.ManageSignals);
            LedManager.SetLeds(this._additionalLeds, this._discretDataBase.Value.AdditionalSignals);
            LedManager.SetLeds(this._indicatorLeds, this._discretDataBase.Value.Indicators);
            LedManager.SetLeds(this._inputLeds, this._discretDataBase.Value.InputSignals);
            LedManager.SetLeds(this._outputLeds, this._discretDataBase.Value.OutputSignals);
            LedManager.SetLeds(this._releLeds, this._discretDataBase.Value.Rele);
            LedManager.SetLeds(this._limitLeds, this._discretDataBase.Value.LimitSignals);
            LedManager.SetLeds(this._faultSignalsLeds, this._discretDataBase.Value.FaultSignals);
            LedManager.SetLeds(this._faultStateLeds, this._discretDataBase.Value.FaultState);
        }

        private void OnAnalogSignalsLoadOk()
        {
            this._averagerTime.Add(this._analogDataBase.Value);
        }
        
        private void MeasuringForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._discretDataBase.RemoveStructQueries();
            this._dateTime.RemoveStructQueries();
            this._measureTrans.RemoveStructQueries();
            this._device.ConnectionModeChanged -= this.StartStopRead;
        }

        private void MeasuringForm_Load(object sender, EventArgs e)
        {
            this.StartStopRead();
        }

        private void StartStopRead()
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._discretDataBase.LoadStructCycle();
                this._dateTime.LoadStructCycle();
                this._measureTrans.LoadStructCycle();
            }
            else
            {
                this._discretDataBase.RemoveStructQueries();
                this._dateTime.RemoveStructQueries();
                this._measureTrans.RemoveStructQueries();
                this.AnalogBdReadFail();
                this.DiscretBdReadFail();
            }
        }

        private void _breakerOnBut_Click(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x1801, "�������� �����������");
        }

        private void ConfirmSDTU(ushort address, string msg)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (DialogResult.Yes == MessageBox.Show(msg + " ?", "������ ��27", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation))
            {
                this._device.SetBit(this._device.DeviceNumber, address, true, msg + this._device.DeviceNumber, this._device);
            }
        }

        private void _breakerOffBut_Click(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x1800, "��������� �����������");
        }

        private void _resetFaultBut_Click(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x1805, "�������� �������������");
        }

        private void _resetJS_But_Click(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x1806, "������������� ������� �������");
        }

        private void _resetJA_But_Click(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x1807, "������������� ������� ������");
        }

        private void _resetIndicatBut_Click(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x1804, "�������� ���������");
        }


        private void OnSplBtnClock(object o, EventArgs e)
        {
            if (MessageBox.Show("��������� �������� ��������������� ������ � ����������?", "������ ���",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;
            this._device.SetBit(this._device.DeviceNumber, 0x1809, true, "������ ���", this._device);
        }
        
        private void OffSplBtnClock(object o, EventArgs e)
        {
            if (MessageBox.Show("���������� �������� ��������������� ������ � ����������? ��������! ��� ����� �������� � ������ �� ������ ������ ������� ����������", "������� ���",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;
            this._device.SetBit(this._device.DeviceNumber, 0x1808, true, "������� ���", this._device);
        }
        
        private void _selectConstraintGroupBut_Click(object sender, EventArgs e)
        {
            string msg;
            bool constraintGroup = false;
            if (this._manageLed4.State == LedState.NoSignaled)
            {
                constraintGroup = true;
                msg = "����������� �� �������� ������";
            }
            else if (this._manageLed4.State == LedState.Signaled)
            {
                msg = "����������� �� ��������� ������";
            }
            else
            {
                return;
            }
            if (DialogResult.Yes == MessageBox.Show(msg + " ?", "������ ��27", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation))
            {
                this._device.SelectConstraintGroup(constraintGroup);
            }
        }

        private void _dateTimeControl_TimeChanged()
        {
            this._dateTime.Value = this._dateTimeControl.DateTime;
            this._dateTime.SaveStruct();
        }


        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(KL27Device); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(MeasuringForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.measuring.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "���������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion
    }
}