﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.ComponentModel;
using BEMN.Devices;
using BEMN.MBServer;
using BEMN.TEZ.Configuration.Structures;
using BEMN.TEZ.NewConfiguration;
using BEMN.TEZ.POInformation;
using BEMN.TEZ.SystemJournal;
using BEMN.TEZ.TezStatus;
using System.Drawing;
using System.Windows.Forms;
using AssemblyResources;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Framework.BusinessLogic;
using BEMN.Interfaces;
using BEMN.TEZ.Configuration;
using BEMN.TEZ.SupportClasses;
using BEMN.TEZ.SystemJournal.Structures;
using BEMN.TEZ.TezStatus.Structures;
using DateTimeStruct = BEMN.TEZ.TezStatus.Structures.DateTimeStruct;

namespace BEMN.TEZ
{
    public class Tez : Device, IDeviceView, IDeviceVersion
    {
        #region Поля структур
        private MemoryEntity<RamMemoryStruct> _ramMemory;
        private MemoryEntity<DateTimeStruct> _dateAndTime;
        private MemoryEntity<CommandStruct> _commandKvit;
        private MemoryEntity<CommandStruct> _command;
        private MemoryEntity<NewJournalV10Struct> _jmessageV10New;
        private MemoryEntity<NewJournalV12Struct> _jmessageV12New;
        private MemoryEntity<FlashMemoryStructNewV14> _flashMemoryV14;
        private MemoryEntity<FlashMemoryStructOld> _flashMemoryOldVers;
        private MemoryEntity<OneWordStruct> _versionEntity;
        private MemoryEntity<OneWordStruct> _versionEntitySj;
        private MemoryEntity<OneWordStruct> _updateEntity;
        private MemoryEntity<OneWordStruct> _stopTime;
        
        #endregion

        #region Конструкторы, получение объекта modbus
        public Tez()
        {
            HaveVersion = true;
        }

        public Tez(Modbus mb)
        {
            MB = mb;
            HaveVersion = true;
            this.InitAddr();
        }
        
        #endregion

        #region Инициализация структур
        private void InitAddr()
        {
            this._ramMemory = new MemoryEntity<RamMemoryStruct>("RAMMEM", this, 0x0);
            this._dateAndTime = new MemoryEntity<DateTimeStruct>("DATETIME", this, 0x1000);
            this._command = new MemoryEntity<CommandStruct>("Command", this, 0x4F);
            this._commandKvit = new MemoryEntity<CommandStruct>("CommandKvit", this, 0x4F);
            this._jmessageV10New = new MemoryEntity<NewJournalV10Struct>("JOURNAL10", this, 0x1100);
            this._jmessageV12New = new MemoryEntity<NewJournalV12Struct>("JOURNAL12", this, 0x1100);
            this._flashMemoryV14 = new MemoryEntity<FlashMemoryStructNewV14>("FlashMemory", this, 0x100);
            this._flashMemoryOldVers = new MemoryEntity<FlashMemoryStructOld>("FlashMemory", this, 0x100);
            this._versionEntity = new MemoryEntity<OneWordStruct>("version", this, 0x53);
            this._versionEntitySj = new MemoryEntity<OneWordStruct>("Версия ПО Sj", this, 0x53);
            this._updateEntity = new MemoryEntity<OneWordStruct>("Обновление ПО", this, 0x54);
            this._stopTime = new MemoryEntity<OneWordStruct>("Остановка времени", this, 0x55);

            this._versionEntity.AllReadOk += HandlerHelper.CreateReadArrayHandler(this.VersionLoaded);
            this._versionEntity.AllReadFail += HandlerHelper.CreateReadArrayHandler(() =>
            {
                TreeManager.OnDeviceVersionLoadFail(this);
            });

        }

        #endregion

        public override Modbus MB
        {
            get { return mb; }
            set
            {
                mb = value;
                if (null != mb)
                {
                    mb.CompleteExchange += this.mb_CompleteExchange;
                }
            }
        }

        private void VersionLoaded()
        {
            string versionToString = this._versionEntity.Value.Word.ToString();

            try
            {
                Strings.Version1 = Convert.ToInt32(versionToString[0].ToString());
                Strings.Version2 = Convert.ToInt32(versionToString[1].ToString());
                Strings.Version3 = Convert.ToInt32(versionToString[2].ToString());

                versionToString = Strings.Version1 + "." + Strings.Version2 + "." + Strings.Version3;
            }
            catch (Exception e)
            {
                MessageBox.Show("Ошибка чтения версии.");
            }

            this.DeviceVersion = versionToString;
            DeviceType = "ТЕЗ-24";
            LoadVersionOk();
        }

        public override void LoadVersion(object deviceObj)
        {
            this._versionEntity.LoadStruct();
        }
        

        #region Свойства для доступа к структурам
        public MemoryEntity<OneWordStruct> VersionEntity => _versionEntity;

        public MemoryEntity<OneWordStruct> VersionEntitySj => _versionEntitySj;

        public MemoryEntity<OneWordStruct> UpdateEntity => _updateEntity;

        public MemoryEntity<OneWordStruct> StopTime => _stopTime;

        public MemoryEntity<FlashMemoryStructNewV14> FlashMemoryNewV14 => _flashMemoryV14;

        public MemoryEntity<FlashMemoryStructOld> FlashMemoryOld => _flashMemoryOldVers;
        
        public MemoryEntity<NewJournalV10Struct> JournalV10 => _jmessageV10New;

        public MemoryEntity<NewJournalV12Struct> JournalV12 => _jmessageV12New;

        public MemoryEntity<CommandStruct> CommandProperty => _command;

        public MemoryEntity<CommandStruct> CommandKvit => this._commandKvit;

        public MemoryEntity<RamMemoryStruct> RamMemory => _ramMemory;

        public MemoryEntity<DateTimeStruct> DateAndTime => _dateAndTime;

        #endregion
        
        public double VersionConverterForTEZ(string version)
        {
            version = version.Replace("до ", "").Replace("xx", "00").Replace(" и выше","");
            string[] vers = version.Split('.');
            double ret = Common.VersionConverter(vers[0] + "." + vers[1]);

            Strings.Version1 = Convert.ToInt32(vers[0]);
            Strings.Version2 = Convert.ToInt32(vers[1]);
            Strings.Version3 = Convert.ToInt32(vers[2]);
            
            return ret;
        }
        
        public Type[] Forms
        {
            get
            {
                double ver = 0;

                if (!DeviceDlgInfo.IsConnectionMode)
                {
                   ver = VersionConverterForTEZ(this.DeviceVersion);
                }

                if (ver < 1.4 && Strings.Version2 < 4)
                {
                    return new[]
                    {
                        typeof (TezStateForm),
                        typeof (TezSystemJournalForm),
                        typeof (TezPoInformationForm),
                        typeof (TezConfigurationForm)
                    };
                }
                return new[]
                {
                    typeof (TezStateForm),
                    typeof (TezSystemJournalForm),
                    typeof (TezPoInformationForm),
                    typeof (MDOTEZConfigurationFormV14)
                };
            }
        }

        public List<string> Versions => new List<string>
        {
            "до 1.3.00",
            "1.4.xx",
            "1.5.xx",
            "1.6.xx"
        };

        #region INodeView Members

        [XmlIgnore]
        [Browsable(false)]
        public Type ClassType => typeof(Tez);

        [XmlIgnore]
        [Browsable(false)]
        public bool ForceShow => false;

        [XmlIgnore]
        [Browsable(false)]
        public Image NodeImage => Framework.Properties.Resources.tez; 

        [Browsable(false)]
        public string NodeName => "ТЭЗ-24";

        [XmlIgnore]
        [Browsable(false)]
        public INodeView[] ChildNodes => new INodeView[] { };

        [XmlIgnore]
        [Browsable(false)]
        public bool Deletable => true;

        #endregion
    }
}
