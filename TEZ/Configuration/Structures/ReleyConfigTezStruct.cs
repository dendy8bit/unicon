﻿using System.Runtime.InteropServices;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.MBServer;

namespace BEMN.TEZ.Configuration.Structures
{
    /// <summary>
    /// структура CONFIG_RELAY
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct ReleyConfigTezStruct : IStruct, IStructInit
    {
        //тип реле ТЭЗа
        private byte _relayType;
        //время импульса
        private byte _relayTime;
        //сигнал реле ТЭЗа
        private byte _relayLogic;
        //Резерв
        private byte _reserv;

        public ReleyConfigTezStruct(bool a)
        {
            this._relayType = 0;
            this._relayTime = 1;
            this._relayLogic = 0;
            this._reserv = 0;
        }
        /// <summary>
        /// Тип сигнала реле ТЭЗ
        /// </summary>
        public byte ReleType
        {
            get { return this._relayType; }
            set { _relayType = value; }
        }
        /// <summary>
        /// Тип логического сигнала реле ТЭЗ
        /// </summary>
        public byte ReleSignal
        {
            get { return _relayLogic; }
            set { _relayLogic = value; }
        }
        /// <summary>
        /// Время импульса реле ТЭЗ
        /// </summary>
        public byte RelayTime
        {
            get { return _relayTime; }
            set { _relayTime = value; }
        }

        public StructInfo GetStructInfo(int len)
        {
            return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool slotArray, int slotLength)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType());
        }

        public void InitStruct(byte[] array)
        {
            int index = 0;
            this._relayType = array[index];
            index += sizeof(byte);
            this._relayTime = array[index];
            index += sizeof(byte);
            this._relayLogic = array[index];
            index += sizeof(byte);
            this._reserv = array[index];
      
         //   this._type = Common.TOWORD(array[index + 1], array[index]);
        }

        public ushort[] GetValues()
        {
            return new[]
                {
                    Common.TOWORD(this._relayTime, this._relayType),
                    Common.TOWORD(this._reserv, this._relayLogic)
                };
        }
    }
}
