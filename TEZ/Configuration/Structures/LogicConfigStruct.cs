﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.MBServer;

namespace BEMN.TEZ.Configuration.Structures
{
    /// <summary>
    /// структура CONFIG_LOGIC
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct LogicConfigStruct : IStruct, IStructInit
    {
        //LOGIC_RELAY
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        private byte[] _logicRele;

        public LogicConfigStruct(bool a)
        {
            this._logicRele = new byte[4]{0,0,0,0};
        }

        public StructInfo GetStructInfo(int len)
        {
            return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool slotArray, int slotLength)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType());
        }


        public void InitStruct(byte[] array)
        {
            this._logicRele = new byte[4];
            Array.ConstrainedCopy(array, 0, this._logicRele, 0, this._logicRele.Length);
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
          result.Add(Common.TOWORD(this._logicRele[0],this._logicRele[1]));
          result.Add(Common.TOWORD(this._logicRele[2], this._logicRele[3]));
            return result.ToArray();
        }


    }
}
