﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.MBServer;

namespace BEMN.TEZ.Configuration.Structures
{
    /// <summary>
    /// структура ADC_ARCCH_CONFIG, используется только первое поле
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct AdcArcchConfigStruct : IStruct, IStructInit
    {
        //структура CHANNEL канал включён
        private byte _channelOn;
        //уровень канала (кЛк)
        private byte _lowVoltLevel;
        //время ожидания канала
        private byte _timeLimit;
        //разерв
        private byte _reserv;

        public AdcArcchConfigStruct(bool a)
        {
            this._channelOn = 0;
            this._lowVoltLevel = 0;
            this._timeLimit = 0;
            this._reserv = 0;
        }
        public bool[] Connects
        {
            get
            {
                var bits = new BitArray(new[] {this._channelOn});
                return new[] {bits[1], bits[2], bits[3], bits[4]};
            }
            set 
            { 
                var bits = new BitArray(new[] { this._channelOn });
                bits[1] = value[0];
                bits[2] = value[1];
                bits[3] = value[2];
                bits[4] = value[3];
                byte[] result = new byte[1];
                bits.CopyTo(result,0);
                this._channelOn = result[0];
            }
        }

        public bool ChannelOn
        {
            get
            {
                return 0 != (_channelOn & 1);
            }
            set
            {
                _channelOn = (byte) (value ? (_channelOn |= 1) : (_channelOn & ~1));
            }
        }

        public StructInfo GetStructInfo(int len)
        {
            return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool slotArray, int slotLength)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType());
        }

        public void InitStruct(byte[] array)
        { 
            this._channelOn = array[0];
            this._lowVoltLevel = array[1];       
            this._timeLimit = array[2];
            this._reserv = array[3];
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.Add(Common.TOWORD(this._lowVoltLevel,this._channelOn ));
            result.Add(Common.TOWORD(this._timeLimit, this._reserv));
            return result.ToArray();
        }
    }
}
