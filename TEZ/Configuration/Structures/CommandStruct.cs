﻿using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.MBServer;

namespace BEMN.TEZ.Configuration.Structures
{
    public struct CommandStruct : IStruct, IStructInit
    {
        private ushort _command;

        public ushort Command
        {
            get { return _command; }
            set { _command = value; }
        }

        public StructInfo GetStructInfo(int len)
        {
           return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool slotArray, int slotLength)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType());
        }

        public void InitStruct(byte[] array)
        {
            this.Command = Common.TOWORD(array[1], array[0]);
        }

        public ushort[] GetValues()
        {
            return new[] {this.Command};
        }
    }
}
