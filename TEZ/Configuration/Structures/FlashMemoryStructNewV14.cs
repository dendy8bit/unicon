﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;

namespace BEMN.TEZ.Configuration.Structures
{
    /// <summary>
    /// Общая структура для версии 1.4 и выше
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct FlashMemoryStructNewV14 : IStruct, IStructInit
    {
        public const int MDO_COUNTS = 24;
        public const int TEZ_DISCRETS_COUNTS = 6;
        public const int TEZ_RELEYS_COUNTS = 3;

        #region PrivateFields

        //CONFIG_MDO
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = MDO_COUNTS)]
        private MdoConfigStruct[] _modulConfig;
        //CONFIG_LOGIC
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = MDO_COUNTS)]
        private LogicConfigStruct[] _logicConfig;
        //CONFIG_DIS
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = TEZ_DISCRETS_COUNTS)]
        private DiscretConfigTezStruct[] _discretConfig;
        //CONFIG_RELAY
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = TEZ_RELEYS_COUNTS)]
        private ReleyConfigTezStruct[] _releyConfig;
        
        private TezUrovRelayStruct _terUrovRelay;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = MDO_COUNTS)]
        private MDOVLSStruct[] _mdoVLS;

        #endregion PrivateFields

        public FlashMemoryStructNewV14(bool a)
        {
            this._modulConfig = new MdoConfigStruct[MDO_COUNTS];
            this._logicConfig = new LogicConfigStruct[MDO_COUNTS];
            this._discretConfig = new DiscretConfigTezStruct[TEZ_DISCRETS_COUNTS];
            this._releyConfig = new ReleyConfigTezStruct[TEZ_RELEYS_COUNTS];
            this._terUrovRelay = new TezUrovRelayStruct(true);
            this._mdoVLS = new MDOVLSStruct[MDO_COUNTS];
            for (int i = 0; i < MDO_COUNTS; i++)
            {
                this._modulConfig[i] = new MdoConfigStruct(true);
                this._logicConfig[i] = new LogicConfigStruct(true);
                if (i < TEZ_DISCRETS_COUNTS)
                    this._discretConfig[i] = new DiscretConfigTezStruct(true);
                if (i < TEZ_RELEYS_COUNTS)
                    this._releyConfig[i] = new ReleyConfigTezStruct(true);
                this._mdoVLS[i] = new MDOVLSStruct(true);
            }
        }

        public MDOVLSStruct[] MdovlsStruct
        {
            get { return _mdoVLS; }
            set { _mdoVLS = value; }
        }
        public TezUrovRelayStruct TezUrovRelayStruct
        {
            get { return this._terUrovRelay; }
            set { this._terUrovRelay = value; }
        }
        public MdoConfigStruct[] MdoConfig
        {
            get { return _modulConfig; }
            set { _modulConfig = value; }
        }

        public DiscretConfigTezStruct[] Discrets
        {
            get { return _discretConfig; }
            set { _discretConfig = value; }
        }

        public ReleyConfigTezStruct[] Releys
        {
            get { return _releyConfig; }
            set { _releyConfig = value; }
        }

        public StructInfo GetStructInfo(int len)
        {
            return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool slotArray, int slotLength)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType());
        }

        public void InitStruct(byte[] array)
        {
            this._modulConfig = new MdoConfigStruct[MDO_COUNTS];
            this._logicConfig = new LogicConfigStruct[MDO_COUNTS];
            this._discretConfig = new DiscretConfigTezStruct[TEZ_DISCRETS_COUNTS];
            this._releyConfig = new ReleyConfigTezStruct[TEZ_RELEYS_COUNTS];
            this._terUrovRelay = new TezUrovRelayStruct(true);
            this._mdoVLS = new MDOVLSStruct[MDO_COUNTS];

            int index = 0;
            byte[] oneStruct;

            for (int i = 0; i < this._modulConfig.Length; i++)
            {
                oneStruct = new byte[Marshal.SizeOf(typeof(MdoConfigStruct))];
                Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
                this._modulConfig[i].InitStruct(oneStruct);
                index += Marshal.SizeOf(typeof(MdoConfigStruct));
            }

            for (int i = 0; i < this._logicConfig.Length; i++)
            {
                oneStruct = new byte[Marshal.SizeOf(typeof(LogicConfigStruct))];
                Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
                this._logicConfig[i].InitStruct(oneStruct);
                index += Marshal.SizeOf(typeof(LogicConfigStruct));
            }

            for (int i = 0; i < this.Discrets.Length; i++)
            {
                oneStruct = new byte[Marshal.SizeOf(typeof(DiscretConfigTezStruct))];
                Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
                this.Discrets[i].InitStruct(oneStruct);
                index += Marshal.SizeOf(typeof(DiscretConfigTezStruct));
            }

            for (int i = 0; i < this._releyConfig.Length; i++)
            {
                oneStruct = new byte[Marshal.SizeOf(typeof(ReleyConfigTezStruct))];
                Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
                this._releyConfig[i].InitStruct(oneStruct);
                index += Marshal.SizeOf(typeof(ReleyConfigTezStruct));
            }

            this._terUrovRelay = StructHelper.GetOneStruct(array, ref index, TezUrovRelayStruct);

            for (int i = 0; i < this._mdoVLS.Length; i++)
            {
                oneStruct = new byte[Marshal.SizeOf(typeof(MDOVLSStruct))];
                Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
                this._mdoVLS[i].InitStruct(oneStruct);
                index += Marshal.SizeOf(typeof(MDOVLSStruct));
            }
        }

        public ushort[] GetValues()
        {
            var result = new List<ushort>();

            for (int j = 0; j < this._modulConfig.Length; j++)
            {
                result.AddRange(this._modulConfig[j].GetValues());
            }

            for (int j = 0; j < this._logicConfig.Length; j++)
            {
                result.AddRange(this._logicConfig[j].GetValues());
            }

            for (int j = 0; j < this.Discrets.Length; j++)
            {
                result.AddRange(this.Discrets[j].GetValues());
            }

            for (int j = 0; j < this._releyConfig.Length; j++)
            {
                result.AddRange(this._releyConfig[j].GetValues());
            }

            result.AddRange(this.TezUrovRelayStruct.GetValues());

            for (int j = 0; j < _mdoVLS.Length; j++)
            {
                result.AddRange(this._mdoVLS[j].GetValues());
            }
            
            return result.ToArray();
        }
    }
}
