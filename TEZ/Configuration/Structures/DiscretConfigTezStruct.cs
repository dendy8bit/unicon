﻿using System.Collections.Generic;
using System.Runtime.InteropServices;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.MBServer;

namespace BEMN.TEZ.Configuration.Structures
{
    /// <summary>
    /// структура CONFIG_DIS
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct DiscretConfigTezStruct : IStruct, IStructInit
    {
        //Тип реле.
        private ushort _type;
        //Резерв.
        private ushort _reserv;

        public DiscretConfigTezStruct(bool a)
        {
            this._type = 0;
            this._reserv = 0;
        }

        public ushort DiscretType
        {
            get { return _type; }
            set { _type = value; }
        }

        public StructInfo GetStructInfo(int len)
        {
            return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool slotArray, int slotLength)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType());
        }

        public void InitStruct(byte[] array)
        {
            int index = 0;

            this.DiscretType = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this._reserv = Common.TOWORD(array[index + 1], array[index]);
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.Add(this.DiscretType);
            result.Add(this._reserv);
            return result.ToArray();
        }
    }
}
