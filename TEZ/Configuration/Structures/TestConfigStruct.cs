﻿using System.Runtime.InteropServices;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.MBServer;


namespace BEMN.TEZ.Configuration.Structures
{
    /// <summary>
    /// структура TEST_CONFIG
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct TestConfigStruct : IStruct, IStructInit
    {
        //тест включен
        public byte _testOn;
        //период теста
        public byte _testPeriod;
        
       

        public TestConfigStruct(bool a)
        {
            this._testOn = 0;
            this._testPeriod = 10;
        }
        public StructInfo GetStructInfo(int len)
        {
            return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool slotArray, int slotLength)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType());
        }

        public void InitStruct(byte[] array)
        {
            int index = 0;
            this._testOn = array[index];
            index += sizeof (byte);
            this._testPeriod = array[index];
        }

        public ushort[] GetValues()
        {
            return new[]{Common.TOWORD(this._testPeriod,this._testOn)};
        }
    }
}
