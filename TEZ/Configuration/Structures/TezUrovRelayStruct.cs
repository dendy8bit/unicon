﻿using System.Runtime.InteropServices;
using System.Collections.Generic;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.MBServer;

namespace BEMN.TEZ.Configuration.Structures
{
    // [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct TezUrovRelayStruct : IStruct, IStructInit
    {
        //3 ВЛС 
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
        private ushort[] _logicSignal;
        //Реле неисправности
        private ushort _pas;

        private ushort _typeDiscretInput;
        private ushort _t1;
        private ushort _t0;

        private ushort _urovEnabled;
        private ushort _urovTime;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5)]
        private ushort[] _reservWords;

        public TezUrovRelayStruct(bool a)
        {
            this._urovEnabled = 0;
            this._urovTime = 10;
            this._pas = 0;
            this._typeDiscretInput = 0;
            this._t1 = 0;
            this._t0 = 0;
            this._reservWords = new ushort[5];
            this._logicSignal = new ushort[3];
        }

        public ushort UrovTime
        {
            get { return this._urovTime; }
            set { this._urovTime = value; }
        }

        public bool UrovEnabled
        {
            get { return Common.GetBit(this._urovEnabled, 0); }
            set { this._urovEnabled = Common.SetBit(this._urovEnabled, 0, value); }
        }

        public ushort[] LogicSignals
        {
            get { return this._logicSignal; }
            set { this._logicSignal = value; }
        }

        public bool[] RelayFaultConfiguration
        {
            get
            {
                return new[]
                    {
                        Common.GetBit(this._pas, 0),
                        Common.GetBit(this._pas, 1),
                        Common.GetBit(this._pas, 2)
                    };
            }

            set
            {
                this._pas = Common.SetBit(this._pas, 0, value[0]);
                this._pas = Common.SetBit(this._pas, 1, value[1]);
                this._pas = Common.SetBit(this._pas, 2, value[2]);
            }
        }

        public bool TypeDiscretInput
        {
            get { return Common.GetBit(this._typeDiscretInput, 0); }
            set { this._typeDiscretInput = Common.SetBit(this._typeDiscretInput, 0, value); }
        }

        public ushort T1
        {
            get { return this._t1; }
            set { this._t1 = value; }
        }

        public ushort T0
        {
            get { return this._t0; }
            set { this._t0 = value; }
        }

        public void InitStruct(byte[] array)
        {
            this._logicSignal = new ushort[3];
            this._pas = 0;
            this._typeDiscretInput = 0;
            this._t1 = 0;
            this._t0 = 0;
            this._reservWords = new ushort[5];
            //index смещение относительно базового адресса
            //необходимо инициализировать последовательно, как поля.
            int index = 0;
            for (int i = 0; i < 3; i++)
            {
                this._logicSignal[i] = Common.TOWORD(array[index + 1], array[index]);
                index += sizeof(ushort);
            }

            this._pas = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this._typeDiscretInput = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this._t1 = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this._t0 = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this._urovEnabled = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this._urovTime = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            for (int i = 0; i < this._reservWords.Length; i++)
            {
                this._reservWords[i] = Common.TOWORD(array[index + 1], array[index]);
                index += sizeof(ushort);
            }
        }

        public ushort[] GetValues()
        {
            var result = new List<ushort>();
            for (int j = 0; j < this._logicSignal.Length; j++)
            {
                result.Add(this._logicSignal[j]);
            }
            result.Add(this._pas);
            result.Add(this._typeDiscretInput);
            result.Add(this._t1);
            result.Add(this._t0);
            result.Add(this._urovEnabled);
            result.Add(this._urovTime);
            result.AddRange(this._reservWords);
            return result.ToArray();
        }
        public StructInfo GetStructInfo(int len)
        {
            return StructHelper.GetStructInfo(GetType());
        }

        public object GetSlots(ushort start, bool slotArray, int slotLength)
        {
            return StructHelper.GetSlots(start, slotArray, GetType());
        }

    }
}
