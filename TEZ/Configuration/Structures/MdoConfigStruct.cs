﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;
using BEMN.TEZ.SupportClasses;

namespace BEMN.TEZ.Configuration.Structures
{
    /// <summary>
    /// структура CONFIG_MDO
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct MdoConfigStruct : IStruct, IStructInit
    {
        public const int RELEYS_COUNT = 4;
        public const int CHANEL_COUNT = 3;

        #region Private Members
        //конфигурация типа
        public ushort _devTypeConfig;
        //структура ADC_ARCCH_CONFIG конфигурация каналов (3 канала по 2 слова) 
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = CHANEL_COUNT)] private AdcArcchConfigStruct[] _adcArcchConfig;
        //структура TEST_CONFIG конфигурация теста
        public TestConfigStruct _testConfig;
        //структура RELAY_CONFIG конфигурация реле (4 реле по 2 слова)
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = RELEYS_COUNT)]
        public RelayConfigStruct[] _relayConfig;

        #endregion Private Members
        
        public MdoConfigStruct(bool a)
        {
            this._devTypeConfig = 0;
            this._adcArcchConfig = new AdcArcchConfigStruct[]
                {
                    new AdcArcchConfigStruct(true), 
                    new AdcArcchConfigStruct(true), 
                    new AdcArcchConfigStruct(true)
                };
    
            this._testConfig = new TestConfigStruct(false);
            this._relayConfig = new RelayConfigStruct[]
                {
                    new RelayConfigStruct(true),
                    new RelayConfigStruct(true),
                    new RelayConfigStruct(true),
                    new RelayConfigStruct(true)
                };

        }

        public bool[] OfflineRelaysType
        {
            get
            {
                return new[]
                    {
                        this._relayConfig[0].ReleOfflineType, this._relayConfig[1].ReleOfflineType,
                        this._relayConfig[2].ReleOfflineType, this._relayConfig[3].ReleOfflineType
                    };
            }
            set
            {
                for (int i = 0; i < this._relayConfig.Length; i++)
                {
                    this._relayConfig[i].ReleOfflineType = value[i];
                }
            }
        }

        public byte[] OfflineRelaysPulse
        {
            get
            {
                return new[]
                    {
                        this._relayConfig[0].ReleOfflinePulseTime, this._relayConfig[1].ReleOfflinePulseTime,
                        this._relayConfig[2].ReleOfflinePulseTime, this._relayConfig[3].ReleOfflinePulseTime
                    };
            }
            set
            {
                for (int i = 0; i < this._relayConfig.Length; i++)
                {
                    this._relayConfig[i].ReleOfflinePulseTime = value[i];
                }
            }
        }

        public byte[][] Releys
        {
            get
            {
                var result = new byte[RELEYS_COUNT][];
                for (int i = 0; i < RELEYS_COUNT; i++)
                {
                    result[i] = this._relayConfig[i].Rele;
                }
                return result;
            }
            set
            {
                for (int i = 0; i < RELEYS_COUNT; i++)
                {
                    this._relayConfig[i].Rele = value[i];
                }
            }
        }

        public bool IsTest
        {
            get { return Common.GetBit(this._testConfig._testOn, 0); }
            set { this._testConfig._testOn = (byte)Common.SetBit(this._testConfig._testOn, 0, value);/*Common.*/ }
        }

        public byte TestTime
        {
            get { return this._testConfig._testPeriod; }
            set { this._testConfig._testPeriod = value; }
        }

        public string MdoType
        {
            get { return Validator.Get(this._devTypeConfig,Strings.MdoType) ; }
            set { this._devTypeConfig = Validator.Set(value, Strings.MdoType); }
        }
        
        public bool[] SensorsState
        {
            get
            {
                return new[]
                    {
                        this._adcArcchConfig[0].ChannelOn,
                        this._adcArcchConfig[1].ChannelOn,
                        this._adcArcchConfig[2].ChannelOn
                    };
            }
            set
            {
                this._adcArcchConfig[0].ChannelOn = value[0];
                this._adcArcchConfig[1].ChannelOn = value[1];
                this._adcArcchConfig[2].ChannelOn = value[2];
            }
        }

        public bool[,] SensorsRelaysState
        {
            get
            {
                var result = new bool[3,4];
                for (int i = 0; i < 3; i++)
                {
                    var connects = this._adcArcchConfig[i].Connects;
                     for (int j = 0; j < 4; j++)
                    {
                        result[i, j] = connects[j];
                    }
                }
                   

                return result;

            }
            set
            {
                for (int i = 0; i < 3; i++)
                    this._adcArcchConfig[i].Connects = new[] {value[i, 0], value[i, 1], value[i, 2], value[i, 3]};
            }
        }

        public StructInfo GetStructInfo(int len)
        {
            return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool slotArray, int slotLength)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType());
        }

        public void InitStruct(byte[] array)
        {
            this._devTypeConfig = 0;
            this._adcArcchConfig = new AdcArcchConfigStruct[3];
            this._testConfig = new TestConfigStruct();
            this._relayConfig = new RelayConfigStruct[4];
            int index = 0;
            byte[] oneStruct;

            this._devTypeConfig = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            for (int i = 0; i < this._adcArcchConfig.Length; i++)
            {
                oneStruct = new byte[Marshal.SizeOf(typeof(AdcArcchConfigStruct))];
                Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
                this._adcArcchConfig[i].InitStruct(oneStruct);
                index += Marshal.SizeOf(typeof(AdcArcchConfigStruct));
            }

            oneStruct = new byte[Marshal.SizeOf(typeof(TestConfigStruct))];
            Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
            this._testConfig.InitStruct(oneStruct);
            index += Marshal.SizeOf(typeof(TestConfigStruct));

            for (int i = 0; i < this._relayConfig.Length; i++)
            {
                oneStruct = new byte[Marshal.SizeOf(typeof(RelayConfigStruct))];
                Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
                this._relayConfig[i].InitStruct(oneStruct);
                index += Marshal.SizeOf(typeof(RelayConfigStruct));
            }

        }

        public ushort[] GetValues()
        {
            var result = new List<ushort>();
            result.Add(this._devTypeConfig);

            for (int j = 0; j < this._adcArcchConfig.Length; j++)
            {
                result.AddRange(this._adcArcchConfig[j].GetValues());
            }

            result.AddRange(this._testConfig.GetValues());

            for (int j = 0; j < this._relayConfig.Length; j++)
            {
                result.AddRange(this._relayConfig[j].GetValues());
            }
            return result.ToArray();

        }

    }
}
