﻿using System.Collections.Generic;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.MBServer;

namespace BEMN.TEZ.Configuration.Structures
{
    /// <summary>
    /// структура RELAY_CONFIG
    /// </summary>
    public struct RelayConfigStruct : IStruct, IStructInit
    {
        //тип в обоих режимах
        private byte _releType;
        //время импульса в обычном режиме
        private byte _relePulseTime;
        //время импульса в автономном режиме
        private byte _relePulseOfflineTime;
        //сигнал реле в обычном режиме
        private byte _releLogicSignal;
        //тип , время и сигнал реле в обычном режиме

  
        public byte[] Rele
        {
            get
            {
                var releType = (byte)(Common.GetBit(this._releType, 0) ? 1 : 0);
                return new[] { releType, this._relePulseTime, this._releLogicSignal };
            }
            set
            {
                this._releType = (byte) Common.SetBit(this._releType,0,Common.GetBit(value[0],0)) ;
                this._relePulseTime = value[1];
                this._releLogicSignal = value[2];
            }
        }
        //тип реле в автономном режиме
        public bool ReleOfflineType
        {
            get { return Common.GetBit(this._releType, 1); }
            set { this._releType =  (byte) Common.SetBit(this._releType, 1,value); }
        }

        //время реле в автономном режиме
        public byte ReleOfflinePulseTime
        {
            get { return this._relePulseOfflineTime; }
            set { this._relePulseOfflineTime = value; }
        }


        public RelayConfigStruct(bool a)
       {
           this._releType = 0;
           this._relePulseTime = 1;
            this._relePulseOfflineTime = 1;
            this._releLogicSignal = 0;
       }
        public StructInfo GetStructInfo(int len)
        {
            return StructHelper.GetStructInfo(this.GetType());
           
        }

        public object GetSlots(ushort start, bool slotArray, int slotLength)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType());
        }

        public void InitStruct(byte[] array)
        {
          
            int index = 0;
            this._releType = array[index];
            index += sizeof(byte);
            this._relePulseTime = array[index];
            index += sizeof(byte);
            this._relePulseOfflineTime = array[index];
            index += sizeof(byte);
            this._releLogicSignal = array[index];
        }

        public ushort[] GetValues()
        {
            var result = new List<ushort>();

            result.Add(Common.TOWORD(this._relePulseTime, this._releType));
            result.Add(Common.TOWORD(this._releLogicSignal, this._relePulseOfflineTime));
            return result.ToArray();
        }

    }
}
