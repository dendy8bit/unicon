﻿using System.Collections.Generic;
using System.Runtime.InteropServices;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.MBServer;

namespace BEMN.TEZ.Configuration.Structures
{
    /// <summary>
    /// Структура МДО_ВЛС
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct MDOVLSStruct : IStruct, IStructInit
    {

        // Сигналы ВЛС для МДО по 4
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        private ushort[] _mdoLogicSignal;

        public ushort[] MDOLogicSignal
        {
            get { return _mdoLogicSignal; }
            set { _mdoLogicSignal = value; }
        }
        public MDOVLSStruct(bool b)
        {
            _mdoLogicSignal = new ushort[4];
        }

        public StructInfo GetStructInfo(int len)
        {
            return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool slotArray, int slotLength)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType());
        }

        public void InitStruct(byte[] array)
        {
            _mdoLogicSignal = new ushort[4];
            int _index = 0;
            for (int i = 0; i < 4; i++)
            {
                this._mdoLogicSignal[i] = Common.TOWORD(array[_index + 1], array[_index]);
                _index += sizeof(ushort);
            }
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            for (int i = 0; i < _mdoLogicSignal.Length; i++)
            {
                result.Add(_mdoLogicSignal[i]);
            }
            return result.ToArray();
        }
    }
}
