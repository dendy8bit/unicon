﻿using BEMN.TEZ.Controls;

namespace BEMN.TEZ.Configuration
{
    partial class TezConfigurationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this._relayOfflineGroupBox = new System.Windows.Forms.GroupBox();
            this._dataGridReleView = new System.Windows.Forms.DataGridView();
            this._extReleNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._extReleTypeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._pulseOfflinePeriodColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._sensor1Column = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._sensor2Column = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._sensor3Column = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._mdoSensorSwitchControl = new BEMN.TEZ.Controls.MdoSensorSwitchControl();
            this._relayInSystemGroupBox = new System.Windows.Forms.GroupBox();
            this._releMdoDataGridView = new System.Windows.Forms.DataGridView();
            this._numberMdoReleColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._typeMdoReleColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._pulsePeriodColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._signalMdoReleColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._testSensorsGroupBox = new System.Windows.Forms.GroupBox();
            this.label30 = new System.Windows.Forms.Label();
            this._testTime = new System.Windows.Forms.MaskedTextBox();
            this.label31 = new System.Windows.Forms.Label();
            this._testEnabled = new System.Windows.Forms.CheckBox();
            this._mdoTypeComboBox = new System.Windows.Forms.ComboBox();
            this._deviceAddr = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._urovEnabled = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this._urovTextBox = new System.Windows.Forms.MaskedTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._tezFaultCheckBox3 = new System.Windows.Forms.CheckBox();
            this._tezFaultCheckBox2 = new System.Windows.Forms.CheckBox();
            this._tezFaultCheckBox1 = new System.Windows.Forms.CheckBox();
            this._releGroupBox = new System.Windows.Forms.GroupBox();
            this._releDataGridView = new System.Windows.Forms.DataGridView();
            this._releNumberColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._releTypeColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._pulseTezRelayColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._releSignalColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._discretGroupBox = new System.Windows.Forms.GroupBox();
            this._discretsDataGridView = new System.Windows.Forms.DataGridView();
            this._numberColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._typeColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._logicSignalsControl = new BEMN.TEZ.Controls.LogicSignalsControl();
            this._writeConfigBut = new System.Windows.Forms.Button();
            this._readConfigBut = new System.Windows.Forms.Button();
            this._saveConfigBut = new System.Windows.Forms.Button();
            this._loadConfigBut = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip();
            this._saveConfigurationDlg = new System.Windows.Forms.SaveFileDialog();
            this._openConfigurationDlg = new System.Windows.Forms.OpenFileDialog();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this._configProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this._statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolTip2 = new System.Windows.Forms.ToolTip();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip();
            this.readfromDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToDevice = new System.Windows.Forms.ToolStripMenuItem();
            this.readFromFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this._relayOfflineGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._dataGridReleView)).BeginInit();
            this._relayInSystemGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._releMdoDataGridView)).BeginInit();
            this._testSensorsGroupBox.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this._releGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._releDataGridView)).BeginInit();
            this._discretGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._discretsDataGridView)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.contextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(607, 361);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this._relayOfflineGroupBox);
            this.tabPage1.Controls.Add(this._mdoSensorSwitchControl);
            this.tabPage1.Controls.Add(this._relayInSystemGroupBox);
            this.tabPage1.Controls.Add(this._testSensorsGroupBox);
            this.tabPage1.Controls.Add(this._mdoTypeComboBox);
            this.tabPage1.Controls.Add(this._deviceAddr);
            this.tabPage1.Controls.Add(this.label26);
            this.tabPage1.Controls.Add(this.label25);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(599, 335);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Уставки МДО";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // _relayOfflineGroupBox
            // 
            this._relayOfflineGroupBox.Controls.Add(this._dataGridReleView);
            this._relayOfflineGroupBox.Location = new System.Drawing.Point(188, 157);
            this._relayOfflineGroupBox.Name = "_relayOfflineGroupBox";
            this._relayOfflineGroupBox.Size = new System.Drawing.Size(399, 145);
            this._relayOfflineGroupBox.TabIndex = 50;
            this._relayOfflineGroupBox.TabStop = false;
            this._relayOfflineGroupBox.Text = "Конфигурация реле в автономном режиме";
            // 
            // _dataGridReleView
            // 
            this._dataGridReleView.AllowUserToAddRows = false;
            this._dataGridReleView.AllowUserToDeleteRows = false;
            this._dataGridReleView.AllowUserToResizeColumns = false;
            this._dataGridReleView.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.NullValue = null;
            this._dataGridReleView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this._dataGridReleView.BackgroundColor = System.Drawing.SystemColors.Control;
            this._dataGridReleView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this._dataGridReleView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._dataGridReleView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._extReleNumberCol,
            this._extReleTypeCol,
            this._pulseOfflinePeriodColumn,
            this._sensor1Column,
            this._sensor2Column,
            this._sensor3Column});
            this._dataGridReleView.Location = new System.Drawing.Point(7, 19);
            this._dataGridReleView.Name = "_dataGridReleView";
            this._dataGridReleView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this._dataGridReleView.RowHeadersVisible = false;
            this._dataGridReleView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._dataGridReleView.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this._dataGridReleView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._dataGridReleView.Size = new System.Drawing.Size(390, 116);
            this._dataGridReleView.TabIndex = 2;
            this._dataGridReleView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._dataGridReleView_CellEndEdit);
            // 
            // _extReleNumberCol
            // 
            this._extReleNumberCol.HeaderText = "№";
            this._extReleNumberCol.Name = "_extReleNumberCol";
            this._extReleNumberCol.ReadOnly = true;
            this._extReleNumberCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._extReleNumberCol.Width = 30;
            // 
            // _extReleTypeCol
            // 
            this._extReleTypeCol.HeaderText = "Тип";
            this._extReleTypeCol.Name = "_extReleTypeCol";
            this._extReleTypeCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._extReleTypeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // _pulseOfflinePeriodColumn
            // 
            this._pulseOfflinePeriodColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this._pulseOfflinePeriodColumn.HeaderText = "Тимп, мс";
            this._pulseOfflinePeriodColumn.MaxInputLength = 4;
            this._pulseOfflinePeriodColumn.Name = "_pulseOfflinePeriodColumn";
            this._pulseOfflinePeriodColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._pulseOfflinePeriodColumn.Width = 79;
            // 
            // _sensor1Column
            // 
            this._sensor1Column.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this._sensor1Column.HeaderText = "Датчик 1";
            this._sensor1Column.MinimumWidth = 30;
            this._sensor1Column.Name = "_sensor1Column";
            this._sensor1Column.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._sensor1Column.Width = 59;
            // 
            // _sensor2Column
            // 
            this._sensor2Column.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this._sensor2Column.HeaderText = "Датчик 2";
            this._sensor2Column.MinimumWidth = 30;
            this._sensor2Column.Name = "_sensor2Column";
            this._sensor2Column.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._sensor2Column.Width = 59;
            // 
            // _sensor3Column
            // 
            this._sensor3Column.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this._sensor3Column.HeaderText = "Датчик 3";
            this._sensor3Column.MinimumWidth = 30;
            this._sensor3Column.Name = "_sensor3Column";
            this._sensor3Column.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._sensor3Column.Width = 59;
            // 
            // _mdoSensorSwitchControl
            // 
            this._mdoSensorSwitchControl.Location = new System.Drawing.Point(7, 102);
            this._mdoSensorSwitchControl.Name = "_mdoSensorSwitchControl";
            this._mdoSensorSwitchControl.Sensors = new bool[] {
        false,
        false,
        false};
            this._mdoSensorSwitchControl.Size = new System.Drawing.Size(175, 108);
            this._mdoSensorSwitchControl.TabIndex = 49;
            this._mdoSensorSwitchControl.CheckChanges += new System.EventHandler(this.mdoSensorSwitchControl1_CheckChanges);
            // 
            // _relayInSystemGroupBox
            // 
            this._relayInSystemGroupBox.Controls.Add(this._releMdoDataGridView);
            this._relayInSystemGroupBox.Location = new System.Drawing.Point(188, 6);
            this._relayInSystemGroupBox.Name = "_relayInSystemGroupBox";
            this._relayInSystemGroupBox.Size = new System.Drawing.Size(368, 145);
            this._relayInSystemGroupBox.TabIndex = 48;
            this._relayInSystemGroupBox.TabStop = false;
            this._relayInSystemGroupBox.Text = "Конфигурация реле при работе в системе";
            // 
            // _releMdoDataGridView
            // 
            this._releMdoDataGridView.AllowUserToAddRows = false;
            this._releMdoDataGridView.AllowUserToDeleteRows = false;
            this._releMdoDataGridView.AllowUserToResizeColumns = false;
            this._releMdoDataGridView.AllowUserToResizeRows = false;
            dataGridViewCellStyle3.NullValue = null;
            this._releMdoDataGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this._releMdoDataGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this._releMdoDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._numberMdoReleColumn,
            this._typeMdoReleColumn,
            this._pulsePeriodColumn,
            this._signalMdoReleColumn});
            this._releMdoDataGridView.GridColor = System.Drawing.SystemColors.ControlDarkDark;
            this._releMdoDataGridView.Location = new System.Drawing.Point(7, 19);
            this._releMdoDataGridView.Name = "_releMdoDataGridView";
            this._releMdoDataGridView.RowHeadersVisible = false;
            this._releMdoDataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._releMdoDataGridView.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this._releMdoDataGridView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._releMdoDataGridView.Size = new System.Drawing.Size(355, 116);
            this._releMdoDataGridView.TabIndex = 2;
            this._releMdoDataGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._releMdoDataGridView_CellEndEdit);
            // 
            // _numberMdoReleColumn
            // 
            this._numberMdoReleColumn.HeaderText = "№";
            this._numberMdoReleColumn.MinimumWidth = 30;
            this._numberMdoReleColumn.Name = "_numberMdoReleColumn";
            this._numberMdoReleColumn.ReadOnly = true;
            this._numberMdoReleColumn.Width = 30;
            // 
            // _typeMdoReleColumn
            // 
            this._typeMdoReleColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._typeMdoReleColumn.HeaderText = "Тип";
            this._typeMdoReleColumn.MinimumWidth = 100;
            this._typeMdoReleColumn.Name = "_typeMdoReleColumn";
            this._typeMdoReleColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._typeMdoReleColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // _pulsePeriodColumn
            // 
            this._pulsePeriodColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this._pulsePeriodColumn.HeaderText = "Тимп, мс";
            this._pulsePeriodColumn.MaxInputLength = 4;
            this._pulsePeriodColumn.Name = "_pulsePeriodColumn";
            this._pulsePeriodColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._pulsePeriodColumn.Width = 79;
            // 
            // _signalMdoReleColumn
            // 
            this._signalMdoReleColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._signalMdoReleColumn.HeaderText = "Сигнал";
            this._signalMdoReleColumn.MinimumWidth = 100;
            this._signalMdoReleColumn.Name = "_signalMdoReleColumn";
            this._signalMdoReleColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._signalMdoReleColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // _testSensorsGroupBox
            // 
            this._testSensorsGroupBox.Controls.Add(this.label30);
            this._testSensorsGroupBox.Controls.Add(this._testTime);
            this._testSensorsGroupBox.Controls.Add(this.label31);
            this._testSensorsGroupBox.Controls.Add(this._testEnabled);
            this._testSensorsGroupBox.Location = new System.Drawing.Point(7, 216);
            this._testSensorsGroupBox.Name = "_testSensorsGroupBox";
            this._testSensorsGroupBox.Size = new System.Drawing.Size(169, 57);
            this._testSensorsGroupBox.TabIndex = 43;
            this._testSensorsGroupBox.TabStop = false;
            this._testSensorsGroupBox.Text = "Тестирование датчиков";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label30.Location = new System.Drawing.Point(125, 29);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(13, 13);
            this.label30.TabIndex = 6;
            this.label30.Text = "с";
            // 
            // _testTime
            // 
            this._testTime.Enabled = false;
            this._testTime.Location = new System.Drawing.Point(59, 26);
            this._testTime.Name = "_testTime";
            this._testTime.Size = new System.Drawing.Size(48, 20);
            this._testTime.TabIndex = 5;
            this._testTime.Tag = "1;65535";
            this._testTime.Text = "10";
            this._testTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.toolTip1.SetToolTip(this._testTime, "Значение должно быть в интервале 0-250");
            this._testTime.TextChanged += new System.EventHandler(this._testTime_TextChanged);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(8, 29);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(45, 13);
            this.label31.TabIndex = 4;
            this.label31.Text = "Период";
            // 
            // _testEnabled
            // 
            this._testEnabled.AutoSize = true;
            this._testEnabled.Location = new System.Drawing.Point(148, 0);
            this._testEnabled.Name = "_testEnabled";
            this._testEnabled.Size = new System.Drawing.Size(15, 14);
            this._testEnabled.TabIndex = 0;
            this._testEnabled.UseVisualStyleBackColor = true;
            this._testEnabled.CheckedChanged += new System.EventHandler(this._TestEnabled_CheckedChanged);
            // 
            // _mdoTypeComboBox
            // 
            this._mdoTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._mdoTypeComboBox.FormattingEnabled = true;
            this._mdoTypeComboBox.Location = new System.Drawing.Point(97, 52);
            this._mdoTypeComboBox.Name = "_mdoTypeComboBox";
            this._mdoTypeComboBox.Size = new System.Drawing.Size(85, 21);
            this._mdoTypeComboBox.TabIndex = 47;
            this._mdoTypeComboBox.SelectedIndexChanged += new System.EventHandler(this._mdoTypeComboBox_SelectedIndexChanged);
            // 
            // _deviceAddr
            // 
            this._deviceAddr.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._deviceAddr.FormattingEnabled = true;
            this._deviceAddr.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24"});
            this._deviceAddr.Location = new System.Drawing.Point(97, 20);
            this._deviceAddr.Name = "_deviceAddr";
            this._deviceAddr.Size = new System.Drawing.Size(85, 21);
            this._deviceAddr.TabIndex = 46;
            this._deviceAddr.SelectedIndexChanged += new System.EventHandler(this._deviceAddr_SelectedIndexChanged);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(25, 55);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(55, 13);
            this.label26.TabIndex = 45;
            this.label26.Text = "Тип МДО";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(10, 23);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(70, 13);
            this.label25.TabIndex = 44;
            this.label25.Text = "Номер МДО";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Controls.Add(this.groupBox2);
            this.tabPage2.Controls.Add(this._releGroupBox);
            this.tabPage2.Controls.Add(this._discretGroupBox);
            this.tabPage2.Controls.Add(this._logicSignalsControl);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(599, 335);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Уставки ТЭЗ-24";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._urovEnabled);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this._urovTextBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(220, 125);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(169, 57);
            this.groupBox1.TabIndex = 52;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "УРОВ";
            // 
            // _urovEnabled
            // 
            this._urovEnabled.AutoSize = true;
            this._urovEnabled.Location = new System.Drawing.Point(47, 0);
            this._urovEnabled.Name = "_urovEnabled";
            this._urovEnabled.Size = new System.Drawing.Size(15, 14);
            this._urovEnabled.TabIndex = 7;
            this._urovEnabled.UseVisualStyleBackColor = true;
            this._urovEnabled.CheckedChanged += new System.EventHandler(this._urovEnabled_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(125, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "мс";
            // 
            // _urovTextBox
            // 
            this._urovTextBox.Location = new System.Drawing.Point(59, 26);
            this._urovTextBox.Name = "_urovTextBox";
            this._urovTextBox.Size = new System.Drawing.Size(48, 20);
            this._urovTextBox.TabIndex = 5;
            this._urovTextBox.Tag = "1;65535";
            this._urovTextBox.Text = "0";
            this._urovTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.toolTip1.SetToolTip(this._urovTextBox, "Значение должно быть в интервале 0-250");
            this._urovTextBox.TextChanged += new System.EventHandler(this._urovTextBox_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Время";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this._tezFaultCheckBox3);
            this.groupBox2.Controls.Add(this._tezFaultCheckBox2);
            this.groupBox2.Controls.Add(this._tezFaultCheckBox1);
            this.groupBox2.Location = new System.Drawing.Point(386, 188);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(206, 97);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Конфигурация реле неисправности";
            // 
            // _tezFaultCheckBox3
            // 
            this._tezFaultCheckBox3.AutoSize = true;
            this._tezFaultCheckBox3.Location = new System.Drawing.Point(6, 74);
            this._tezFaultCheckBox3.Name = "_tezFaultCheckBox3";
            this._tezFaultCheckBox3.Size = new System.Drawing.Size(134, 17);
            this._tezFaultCheckBox3.TabIndex = 2;
            this._tezFaultCheckBox3.Text = "Неисправность МДО";
            this._tezFaultCheckBox3.UseVisualStyleBackColor = true;
            this._tezFaultCheckBox3.CheckedChanged += new System.EventHandler(this._tezFaultCheckBox1_CheckedChanged);
            // 
            // _tezFaultCheckBox2
            // 
            this._tezFaultCheckBox2.AutoSize = true;
            this._tezFaultCheckBox2.Location = new System.Drawing.Point(6, 51);
            this._tezFaultCheckBox2.Name = "_tezFaultCheckBox2";
            this._tezFaultCheckBox2.Size = new System.Drawing.Size(176, 17);
            this._tezFaultCheckBox2.TabIndex = 1;
            this._tezFaultCheckBox2.Text = "Неисправность связи с МДО";
            this._tezFaultCheckBox2.UseVisualStyleBackColor = true;
            this._tezFaultCheckBox2.CheckedChanged += new System.EventHandler(this._tezFaultCheckBox1_CheckedChanged);
            // 
            // _tezFaultCheckBox1
            // 
            this._tezFaultCheckBox1.AutoSize = true;
            this._tezFaultCheckBox1.Location = new System.Drawing.Point(6, 28);
            this._tezFaultCheckBox1.Name = "_tezFaultCheckBox1";
            this._tezFaultCheckBox1.Size = new System.Drawing.Size(144, 17);
            this._tezFaultCheckBox1.TabIndex = 0;
            this._tezFaultCheckBox1.Text = "Неисправность ТЭЗ-24";
            this._tezFaultCheckBox1.UseVisualStyleBackColor = true;
            this._tezFaultCheckBox1.CheckedChanged += new System.EventHandler(this._tezFaultCheckBox1_CheckedChanged);
            // 
            // _releGroupBox
            // 
            this._releGroupBox.Controls.Add(this._releDataGridView);
            this._releGroupBox.Location = new System.Drawing.Point(6, 6);
            this._releGroupBox.Name = "_releGroupBox";
            this._releGroupBox.Size = new System.Drawing.Size(386, 110);
            this._releGroupBox.TabIndex = 2;
            this._releGroupBox.TabStop = false;
            this._releGroupBox.Text = "Реле";
            // 
            // _releDataGridView
            // 
            this._releDataGridView.AllowUserToAddRows = false;
            this._releDataGridView.AllowUserToDeleteRows = false;
            this._releDataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this._releDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._releDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._releNumberColumn,
            this._releTypeColumn,
            this._pulseTezRelayColumn,
            this._releSignalColumn});
            this._releDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this._releDataGridView.Location = new System.Drawing.Point(3, 16);
            this._releDataGridView.MultiSelect = false;
            this._releDataGridView.Name = "_releDataGridView";
            this._releDataGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this._releDataGridView.RowHeadersVisible = false;
            this._releDataGridView.RowHeadersWidth = 11;
            this._releDataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._releDataGridView.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._releDataGridView.Size = new System.Drawing.Size(380, 91);
            this._releDataGridView.TabIndex = 1;
            this._releDataGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._releDataGridView_CellEndEdit);
            // 
            // _releNumberColumn
            // 
            this._releNumberColumn.HeaderText = "№";
            this._releNumberColumn.MinimumWidth = 30;
            this._releNumberColumn.Name = "_releNumberColumn";
            this._releNumberColumn.ReadOnly = true;
            this._releNumberColumn.Width = 30;
            // 
            // _releTypeColumn
            // 
            this._releTypeColumn.HeaderText = "Тип";
            this._releTypeColumn.Name = "_releTypeColumn";
            this._releTypeColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._releTypeColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // _pulseTezRelayColumn
            // 
            this._pulseTezRelayColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this._pulseTezRelayColumn.HeaderText = "Тимп(мс)";
            this._pulseTezRelayColumn.Name = "_pulseTezRelayColumn";
            this._pulseTezRelayColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._pulseTezRelayColumn.Width = 79;
            // 
            // _releSignalColumn
            // 
            this._releSignalColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._releSignalColumn.HeaderText = "Сигнал";
            this._releSignalColumn.Name = "_releSignalColumn";
            this._releSignalColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // _discretGroupBox
            // 
            this._discretGroupBox.Controls.Add(this._discretsDataGridView);
            this._discretGroupBox.Location = new System.Drawing.Point(395, 6);
            this._discretGroupBox.Name = "_discretGroupBox";
            this._discretGroupBox.Size = new System.Drawing.Size(200, 176);
            this._discretGroupBox.TabIndex = 1;
            this._discretGroupBox.TabStop = false;
            this._discretGroupBox.Text = "Дискретные входы";
            // 
            // _discretsDataGridView
            // 
            this._discretsDataGridView.AllowUserToAddRows = false;
            this._discretsDataGridView.AllowUserToDeleteRows = false;
            this._discretsDataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this._discretsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._discretsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._numberColumn,
            this._typeColumn});
            this._discretsDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this._discretsDataGridView.Location = new System.Drawing.Point(3, 16);
            this._discretsDataGridView.MultiSelect = false;
            this._discretsDataGridView.Name = "_discretsDataGridView";
            this._discretsDataGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this._discretsDataGridView.RowHeadersVisible = false;
            this._discretsDataGridView.RowHeadersWidth = 11;
            this._discretsDataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._discretsDataGridView.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._discretsDataGridView.Size = new System.Drawing.Size(194, 157);
            this._discretsDataGridView.TabIndex = 1;
            this._discretsDataGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._discretsDataGridView_CellEndEdit);
            // 
            // _numberColumn
            // 
            this._numberColumn.HeaderText = "№";
            this._numberColumn.MinimumWidth = 30;
            this._numberColumn.Name = "_numberColumn";
            this._numberColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._numberColumn.Width = 30;
            // 
            // _typeColumn
            // 
            this._typeColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._typeColumn.HeaderText = "Тип";
            this._typeColumn.MinimumWidth = 50;
            this._typeColumn.Name = "_typeColumn";
            this._typeColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // _logicSignalsControl
            // 
            this._logicSignalsControl.Location = new System.Drawing.Point(9, 119);
            this._logicSignalsControl.Name = "_logicSignalsControl";
            this._logicSignalsControl.Signals = new ushort[] {
        ((ushort)(0)),
        ((ushort)(0)),
        ((ushort)(0))};
            this._logicSignalsControl.Size = new System.Drawing.Size(190, 212);
            this._logicSignalsControl.TabIndex = 3;
            this._logicSignalsControl.UpdateData += new System.EventHandler(this.logicSignalsControl1_UpdateData);
            // 
            // _writeConfigBut
            // 
            this._writeConfigBut.AutoSize = true;
            this._writeConfigBut.Location = new System.Drawing.Point(12, 408);
            this._writeConfigBut.Name = "_writeConfigBut";
            this._writeConfigBut.Size = new System.Drawing.Size(149, 23);
            this._writeConfigBut.TabIndex = 35;
            this._writeConfigBut.Text = "Записать в устройство";
            this.toolTip2.SetToolTip(this._writeConfigBut, "Записать конфигурацию в устройство (CTRL+W)");
            this._writeConfigBut.UseVisualStyleBackColor = true;
            this._writeConfigBut.Click += new System.EventHandler(this._writeConfigBut_Click);
            // 
            // _readConfigBut
            // 
            this._readConfigBut.AutoSize = true;
            this._readConfigBut.Location = new System.Drawing.Point(12, 379);
            this._readConfigBut.Name = "_readConfigBut";
            this._readConfigBut.Size = new System.Drawing.Size(149, 23);
            this._readConfigBut.TabIndex = 34;
            this._readConfigBut.Text = "Прочитать из устройства";
            this.toolTip2.SetToolTip(this._readConfigBut, "Прочитать конфигурацию из устройства (CTRL+R)");
            this._readConfigBut.UseVisualStyleBackColor = true;
            this._readConfigBut.Click += new System.EventHandler(this._readConfigBut_Click);
            // 
            // _saveConfigBut
            // 
            this._saveConfigBut.AllowDrop = true;
            this._saveConfigBut.AutoSize = true;
            this._saveConfigBut.Location = new System.Drawing.Point(429, 408);
            this._saveConfigBut.Name = "_saveConfigBut";
            this._saveConfigBut.Size = new System.Drawing.Size(149, 23);
            this._saveConfigBut.TabIndex = 37;
            this._saveConfigBut.Text = "Сохранить в файл";
            this.toolTip2.SetToolTip(this._saveConfigBut, "Сохранить конфигурацию в файл (CTRL+S)");
            this._saveConfigBut.UseVisualStyleBackColor = true;
            this._saveConfigBut.Click += new System.EventHandler(this._saveConfigBut_Click);
            // 
            // _loadConfigBut
            // 
            this._loadConfigBut.AutoSize = true;
            this._loadConfigBut.Location = new System.Drawing.Point(429, 379);
            this._loadConfigBut.Name = "_loadConfigBut";
            this._loadConfigBut.Size = new System.Drawing.Size(149, 23);
            this._loadConfigBut.TabIndex = 36;
            this._loadConfigBut.Text = "Загрузить из файла";
            this.toolTip2.SetToolTip(this._loadConfigBut, "Загрузить конфигурацию из файла (CTRL+O)");
            this._loadConfigBut.UseVisualStyleBackColor = true;
            this._loadConfigBut.Click += new System.EventHandler(this._loadConfigBut_Click);
            // 
            // _saveConfigurationDlg
            // 
            this._saveConfigurationDlg.FileName = "Уставки ТЭЗ-24";
            this._saveConfigurationDlg.Filter = "Файл уставок ТЭЗ-24 (*.xml)|*.xml";
            this._saveConfigurationDlg.Title = "Сохранение файла уставок ТЭЗ-24";
            // 
            // _openConfigurationDlg
            // 
            this._openConfigurationDlg.FileName = "Уставки ТЭЗ-24";
            this._openConfigurationDlg.Filter = "Файл уставок ТЭЗ-24 (*.xml)|*.xml";
            this._openConfigurationDlg.Title = "Загрузка файла уставок ТЭЗ-24";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._configProgressBar,
            this._statusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 434);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(625, 22);
            this.statusStrip1.TabIndex = 38;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // _configProgressBar
            // 
            this._configProgressBar.Name = "_configProgressBar";
            this._configProgressBar.Size = new System.Drawing.Size(100, 16);
            // 
            // _statusLabel
            // 
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.Size = new System.Drawing.Size(16, 17);
            this._statusLabel.Text = "...";
            // 
            // contextMenu
            // 
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.readfromDeviceItem,
            this.writeToDevice,
            this.readFromFileItem,
            this.writeToFileItem});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(213, 92);
            this.contextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenu_Opening);
            this.contextMenu.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.contextMenu_ItemClicked);
            // 
            // readfromDeviceItem
            // 
            this.readfromDeviceItem.Name = "readfromDeviceItem";
            this.readfromDeviceItem.Size = new System.Drawing.Size(212, 22);
            this.readfromDeviceItem.Text = "Прочитать из устройства";
            // 
            // writeToDevice
            // 
            this.writeToDevice.Name = "writeToDevice";
            this.writeToDevice.Size = new System.Drawing.Size(212, 22);
            this.writeToDevice.Text = "Записать в устройство";
            // 
            // readFromFileItem
            // 
            this.readFromFileItem.Name = "readFromFileItem";
            this.readFromFileItem.Size = new System.Drawing.Size(212, 22);
            this.readFromFileItem.Text = "Загрузить из файла";
            // 
            // writeToFileItem
            // 
            this.writeToFileItem.Name = "writeToFileItem";
            this.writeToFileItem.Size = new System.Drawing.Size(212, 22);
            this.writeToFileItem.Text = "Записать в файл";
            // 
            // TezConfigurationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(625, 456);
            this.ContextMenuStrip = this.contextMenu;
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this._writeConfigBut);
            this.Controls.Add(this._readConfigBut);
            this.Controls.Add(this._saveConfigBut);
            this.Controls.Add(this._loadConfigBut);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "TezConfigurationForm";
            this.Text = "TezConfigurationForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TezConfigurationForm_FormClosing);
            this.Load += new System.EventHandler(this.TezConfigurationForm_Load);
            this.Shown += new System.EventHandler(this.TezConfigurationForm_Shown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TezConfigurationForm_KeyUp);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this._relayOfflineGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._dataGridReleView)).EndInit();
            this._relayInSystemGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._releMdoDataGridView)).EndInit();
            this._testSensorsGroupBox.ResumeLayout(false);
            this._testSensorsGroupBox.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this._releGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._releDataGridView)).EndInit();
            this._discretGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._discretsDataGridView)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.contextMenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button _writeConfigBut;
        private System.Windows.Forms.Button _readConfigBut;
        private System.Windows.Forms.Button _saveConfigBut;
        private System.Windows.Forms.Button _loadConfigBut;
        private System.Windows.Forms.GroupBox _discretGroupBox;
        private System.Windows.Forms.DataGridView _discretsDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn _numberColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _typeColumn;
        private System.Windows.Forms.GroupBox _releGroupBox;
        private System.Windows.Forms.DataGridView _releDataGridView;
        private System.Windows.Forms.GroupBox _testSensorsGroupBox;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.MaskedTextBox _testTime;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.CheckBox _testEnabled;
        private System.Windows.Forms.ComboBox _mdoTypeComboBox;
        private System.Windows.Forms.ComboBox _deviceAddr;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.GroupBox _relayInSystemGroupBox;
        private System.Windows.Forms.DataGridView _releMdoDataGridView;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.SaveFileDialog _saveConfigurationDlg;
        private System.Windows.Forms.OpenFileDialog _openConfigurationDlg;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripProgressBar _configProgressBar;
        private System.Windows.Forms.ToolStripStatusLabel _statusLabel;
        private MdoSensorSwitchControl _mdoSensorSwitchControl;
        private System.Windows.Forms.GroupBox _relayOfflineGroupBox;
        private System.Windows.Forms.DataGridView _dataGridReleView;
        private System.Windows.Forms.DataGridViewTextBoxColumn _releNumberColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _releTypeColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _pulseTezRelayColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _releSignalColumn;
        private LogicSignalsControl _logicSignalsControl;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox _tezFaultCheckBox3;
        private System.Windows.Forms.CheckBox _tezFaultCheckBox2;
        private System.Windows.Forms.CheckBox _tezFaultCheckBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _extReleNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _extReleTypeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _pulseOfflinePeriodColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _sensor1Column;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _sensor2Column;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _sensor3Column;
        private System.Windows.Forms.DataGridViewTextBoxColumn _numberMdoReleColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _typeMdoReleColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _pulsePeriodColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _signalMdoReleColumn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MaskedTextBox _urovTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox _urovEnabled;
        private System.Windows.Forms.ToolTip toolTip2;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem readfromDeviceItem;
        private System.Windows.Forms.ToolStripMenuItem writeToDevice;
        private System.Windows.Forms.ToolStripMenuItem readFromFileItem;
        private System.Windows.Forms.ToolStripMenuItem writeToFileItem;
    }
}