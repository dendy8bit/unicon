﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using AssemblyResources;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.TEZ.Configuration.Structures;
using BEMN.TEZ.SupportClasses;

namespace BEMN.TEZ.Configuration
{
    public partial class TezConfigurationForm : Form, IFormView
    {
        #region [Constants]
        private const int DISCRETS_COUNT = 6;
        private const int RELE_COUNT = 3;
        private const int MDO_RELE_COUNT = 4;
        private const string SUCCESSFUL_READ_CONFIGURATION = "Конфигурация прочитана";
        private const string FILE_IS_SAVED_PATTERN = "Файл {0} сохранен";
        private const string SETPOINT_IS_WRITE = "Уставки записаны";
        private const string TEZ_SAVE_MESSAGE_PATTERN = "Записать конфигурацию ТЭЗ-24 № {0} ?";
        private const string IMPOSSIBLE_READ_CONFIGURATION = "Невозможно прочитать конфигурацию";
        private const string ATTENTION = "Внимание";
        private const string READING_SETPOINTS = "Идёт чтение уставок";
        private const string CONFIGURATION_IS_WRITTING = "Идёт запись конфигурации";
        private const string IMPOSSIBLE_WRITE_CONFIGURATION = "Невозможно записать конфигурацию";
        private const string ERROR_READ_CONFIGURATION = "Ошибка чтения конфигурации";
        private const int TEN_FACTOR = 10;
        private const string PULSE_INCORRECT_VALUE = "Неккоректные данные, проверьте ввод. Значение должно лежать в диапазоне 10-1000 и должно быть кратно 10.";
        private const string DATA_ERROR = "Ошибка данных";
        private const string PULSE_ZERO_ERROR_MESSAGE = "Значение времени импульса не может быть нулевым";
        private const string PULSE_IS_ZERO_ERROR = "Импульс не может быть равен 0";
        private const string TEST_TIME_IS_ERROR = "Время тестирования должно быть в пределах 1-250с";
        private const string VLS_VALUE_ERROR = "Выбранный ВЛС не может быть пустым";
        private const string UROV_ERROR = "Время УРОВ должно быть в пределах 0 - 1000 мс";
        private const string FILE_SAVE_FAIL = "Не удалось сохранить файл конфигурации";
        private const string FILE_LOAD_FAIL = "Не удалось загрузить файл конфигурации";
        private const string XML_HEAD = "TEZ_SET_POINTS";

        #endregion [Constants]

        #region Поля
        private readonly Tez _device;
        //  TezUrovRelayStruct _tezUrov;
        private bool _errorMessage;
        private FlashWriteStates _writeFlashMemoryState;
        private FlashMemoryStructOld _fmOld;
        private MemoryEntity<FlashMemoryStructOld> _flashMemory;
        private MemoryEntity<CommandStruct> _command;
        #endregion Поля
        
        #region [Nested types]
        //Этапы записи флеш памяти
        private enum FlashWriteStates
        {
            //Базовое состояние
            NONE = 0,
            //Команда перед записью
            START_WRITE = 2,
            //Команда об окончании записи
            END_WRITE = 3
        }
        #endregion [Nested types]


        #region Конструкторы
        public TezConfigurationForm()
        {
            this.InitializeComponent();
        }

        public TezConfigurationForm(Tez device)
        {
            this.InitializeComponent();
            this.InitializeDataGridView();
            this._device = device;
            this._fmOld = new FlashMemoryStructOld(true);
            
            this._command = this._device.CommandProperty;
            this._command.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.StructAllWriteFail);
            this._command.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.CommandIsWrite);

            this._flashMemory = this._device.FlashMemoryOld;
            this._flashMemory.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.StructRead);
            this._flashMemory.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.StructAllReadFail);
            this._flashMemory.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.StructAllWriteFail);
            this._flashMemory.ReadFail += HandlerHelper.CreateHandler(this, this.FlashMemoryReadFail);
            this._flashMemory.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.FlashMemory_AllWriteOk);
            this._flashMemory.ReadOk += HandlerHelper.CreateHandler(this, this.ProgressBarIncrement);
            this._flashMemory.WriteOk += HandlerHelper.CreateHandler(this, this.ProgressBarIncrement);
            this._configProgressBar.Maximum = this._flashMemory.Slots.Count;
        }
        //Инициализация таблиц
        private void InitializeDataGridView()
        {
            //Инициализация таблицы дискрет ТЭЗ
            this._typeColumn.DataSource = Strings.DiscretTypeView;
            this._discretsDataGridView.Rows.Clear();
            for (int i = 0; i < TezConfigurationForm.DISCRETS_COUNT; i++)
            {
                this._discretsDataGridView.Rows.Add(new object[] { i + 1, Strings.DiscretType[0] });
            }

            //Инициализация таблицы реле ТЭЗ
            this._releSignalColumn.DataSource = Strings.ReleTezSignalView;
            this._releTypeColumn.DataSource = Strings.ReleType;
            this._releDataGridView.Rows.Clear();
            for (int i = 0; i < TezConfigurationForm.RELE_COUNT; i++)
            {
                this._releDataGridView.Rows.Add(new object[] { i + 1, Strings.ReleType[0], 0, Strings.ReleTezSignal[0] });
            }

            //Инициализация таблицы реле МДО
            this._typeMdoReleColumn.DataSource = Strings.ReleType;
            this._signalMdoReleColumn.DataSource = Strings.ReleMdoSignalOldVersion;
            this._extReleTypeCol.DataSource = Strings.ReleType;
            for (int i = 0; i < TezConfigurationForm.MDO_RELE_COUNT; i++)
            {
                this._releMdoDataGridView.Rows.Add(new object[] { i + 1, Strings.ReleType[0], 0, Strings.ReleMdoSignalOldVersion[0] });
                this._dataGridReleView.Rows.Add(new object[] { i + 1, Strings.ReleType[0], 0, true, true, true, true });
            }
            //Тип МДО
            this._mdoTypeComboBox.DataSource = Strings.MdoType;
        }
        #endregion Конструкторы

        #region IFormView
        public Type FormDevice
        {
            get { return typeof(Tez); }
        }

        public bool Multishow { get; private set; }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public Type ClassType
        {
            get { return typeof(TezConfigurationForm); }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.config.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Конфигурация"; }
        }
        #endregion IFormView

        #region Обработчики событий StObj

        private void FlashMemory_AllWriteOk()
        {
            if (this._writeFlashMemoryState == FlashWriteStates.START_WRITE)
            {
                this._writeFlashMemoryState = FlashWriteStates.END_WRITE;
                this.WriteCommand((ushort)this._writeFlashMemoryState);
            }
            else
            {
                this._writeFlashMemoryState = FlashWriteStates.NONE;
            }
        }

        #endregion Обработчики событий StObj

        #region Вспомогательные методы
        
        //Запуск чтения структур из устройства
        private void LoadConfigurationBlocks()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._statusLabel.Text = READING_SETPOINTS;
            this._configProgressBar.Value = 0;
            this._flashMemory.LoadStruct();
        }

        private void ProgressBarIncrement()
        {
            if (this._configProgressBar.Value < this._configProgressBar.Maximum)
                this._configProgressBar.Value++;
        }

        //Обработка ошибки запроса на чтение
        private void FlashMemoryReadFail()
        {
            this._statusLabel.Text = ERROR_READ_CONFIGURATION;
            this.ProgressBarIncrement();
        }
        //Обработка прочитанной из устройства структуры
        private void StructRead()
        {
            this._fmOld = this._flashMemory.Value;

            //Выводим статус о удачном прочтении памяти
            this._statusLabel.Text = SUCCESSFUL_READ_CONFIGURATION;
            //Заполняем полосу загрузки
            this._configProgressBar.Value = this._configProgressBar.Maximum;
            //Выводим сообщение о удачном прочтении памяти
            MessageBox.Show(SUCCESSFUL_READ_CONFIGURATION);
            //Выбираем первый Мдо.
            this._deviceAddr.SelectedIndex = 0;
            //включаем ввод
            this.tabControl1.Enabled = true;
            //Вывод параметров ТЭЗа.
            this.ShowTezStatus();

        }
        //Прекращаем чтение из устройства
        private void CloseForm()
        {
            this._flashMemory.RemoveStructQueries();
        }
        //Запись флеш памяти в устройство
        private void FlashMemoryWrite()
        {
            this._flashMemory.Value = this._fmOld;
            this._flashMemory.SaveStruct();
        }
        private bool TimePulseIsZero(int pulse)
        {
            if (pulse == 0)
            {
                MessageBox.Show(PULSE_ZERO_ERROR_MESSAGE);
                return true;
            }
            return false;
        }
        //Запись конфигурации в устройство
        private void WriteConfigurationToDevice()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            ReleyConfigTezStruct[] releys = this._fmOld.Releys;
            //Проверяем что если выбран какойто ВЛС что он не 0
            IEnumerable<int> vls = releys.Select(o => o.ReleSignal - 11).Where(a => a >= 0).Distinct();
            foreach (int signal in vls)
            {
                if (this._fmOld.TezUrovRelayStruct.LogicSignals[signal] == 0)
                    MessageBox.Show(VLS_VALUE_ERROR);
            }

            MdoConfigStruct[] mdoConfig = this._fmOld.MdoConfig;

            foreach (MdoConfigStruct mdoConfigStruct in mdoConfig)
            {
                //Проверяем что время импульса не 0
                if (mdoConfigStruct.OfflineRelaysPulse.Any(offlinePulse => this.TimePulseIsZero(offlinePulse)))
                {
                    MessageBox.Show(PULSE_IS_ZERO_ERROR);
                    return;
                }
                //Проверяем что время импульса не 0
                if (mdoConfigStruct.Releys.Any(t => this.TimePulseIsZero(t[1])))
                {
                    MessageBox.Show(PULSE_IS_ZERO_ERROR);
                    return;
                }

                //Время теста не 0, если он включен
                if (((mdoConfigStruct.TestTime <= 0) | (mdoConfigStruct.TestTime > 250)) & mdoConfigStruct.IsTest)
                {
                    MessageBox.Show(TEST_TIME_IS_ERROR);
                    return;
                }
            }

            if (!this.UrovValidate())
            {
                return;
            }

            string saveMessage = string.Format(TEZ_SAVE_MESSAGE_PATTERN, this._device.DeviceNumber);
            if (DialogResult.Yes ==
                MessageBox.Show(saveMessage, ATTENTION, MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                                MessageBoxDefaultButton.Button2))
            {
                this._writeFlashMemoryState = FlashWriteStates.START_WRITE;
                this._configProgressBar.Value = 0;
                this._statusLabel.Text = CONFIGURATION_IS_WRITTING;
                this.WriteCommand((ushort)this._writeFlashMemoryState);
            }
        }

        //Запись команды
        private void WriteCommand(ushort command)
        {
            CommandStruct cs = new CommandStruct();
            cs.Command = command;

            if ((FlashWriteStates)command == FlashWriteStates.END_WRITE)
                this._writeFlashMemoryState = FlashWriteStates.NONE;
            this._command.Value = cs;
            this._command.SaveStruct();
        }

        //Вывод статуса Тез
        private void ShowTezStatus()
        {
            try
            {
                this._urovEnabled.Checked = this._fmOld.TezUrovRelayStruct.UrovEnabled;
                this._urovTextBox.Text = this._fmOld.TezUrovRelayStruct.UrovTime.ToString();
                //Временное исправления для вывода в реле неисправностей
                bool[] _arrHelperChBox = new bool[3];
                for (int i = 0; i < this._fmOld.TezUrovRelayStruct.RelayFaultConfiguration.Length; i++)
                {
                    _arrHelperChBox[i] = this._fmOld.TezUrovRelayStruct.RelayFaultConfiguration[i];
                }
                this._tezFaultCheckBox1.Checked = _arrHelperChBox[0];
                this._tezFaultCheckBox2.Checked = _arrHelperChBox[1];
                this._tezFaultCheckBox3.Checked = _arrHelperChBox[2];
                //дискреты
                DiscretConfigTezStruct[] discrets = this._fmOld.Discrets;
                this._discretsDataGridView.Rows.Clear();
                for (int i = 0; i < FlashMemoryStructOld.TEZ_DISCRETS_COUNTS; i++)
                {
                    ushort type = discrets[i].DiscretType;
                    this._discretsDataGridView.Rows.Add(i + 1, Strings.DiscretType[type]);
                }
                //реле
                ReleyConfigTezStruct[] releys = this._fmOld.Releys;
                this._releDataGridView.Rows.Clear();
                for (int i = 0; i < FlashMemoryStructOld.TEZ_RELEYS_COUNTS; i++)
                {
                    byte type = releys[i].ReleType;
                    int pulse = releys[i].RelayTime * TEN_FACTOR;
                    byte signal = releys[i].ReleSignal;

                    this._releDataGridView.Rows.Add(i + 1, Strings.ReleType[type], pulse, Strings.ReleTezSignal[signal]);
                }
                this._logicSignalsControl.Signals = this._fmOld.TezUrovRelayStruct.LogicSignals;
                //Статус первого МДО
                this.ShowMdoStatus(0);

            }
            catch (Exception)
            {
                MessageBox.Show(DATA_ERROR);
            }


        }
        //Вывод статуса Мдо
        private void ShowMdoStatus(int index)
        {
            //Конфигурация автономного режима
            bool[,] sensorsRelaysState = this._fmOld.MdoConfig[index].SensorsRelaysState;
            bool[] offlineRelaysType = this._fmOld.MdoConfig[index].OfflineRelaysType;
            byte[] offlineRelaysPulse = this._fmOld.MdoConfig[index].OfflineRelaysPulse;
            this._dataGridReleView.Rows.Clear();

            for (int i = 0; i < 4; i++)
            {
                int offlineReleTypeIndex = offlineRelaysType[i] ? 1 : 0;
                this._dataGridReleView.Rows.Add(i + 1,
                                           Strings.ReleType[offlineReleTypeIndex],
                                           offlineRelaysPulse[i] * TEN_FACTOR,
                                           sensorsRelaysState[0, i],
                                           sensorsRelaysState[1, i],
                                           sensorsRelaysState[2, i]);
            }

            this._mdoSensorSwitchControl.Sensors = this._fmOld.MdoConfig[index].SensorsState;
            //Тип МДО
            this._mdoTypeComboBox.SelectedItem = this._fmOld.MdoConfig[index].MdoType;
            //Тест включен
            this._testEnabled.Checked = this._fmOld.MdoConfig[index].IsTest;
            //Период теста
            this._testTime.Text = this._fmOld.MdoConfig[index].TestTime.ToString(CultureInfo.InvariantCulture);
            //Конфигурация реле
            this._releMdoDataGridView.Rows.Clear();
            byte[][] releys = this._fmOld.MdoConfig[index].Releys;
            for (int i = 0; i < MDO_RELE_COUNT; i++)
            {
                byte releTypeIndex = releys[i][0];
                int pulse = releys[i][1] * TEN_FACTOR;
                byte releSignalIndex = releys[i][2];

                this._releMdoDataGridView.Rows.Add(i + 1,
                                              Strings.ReleType[releTypeIndex],
                                              pulse,
                                              Strings.ReleMdoSignalOldVersion[releSignalIndex]);
            }
        }

        //Обработка изменения состояния реле датчиков  в автономном режиме
        private void MdoOfflineSensorsChanged()
        {
            bool[,] sensorsRelaysState = new bool[3, 4];
            bool[] types = new bool[4];
            byte[] pulses = new byte[4];
            for (int i = 0; i < 4; i++)
            {
                types[i] = Strings.ReleType.IndexOf(this._dataGridReleView[1, i].Value.ToString()) != 0;
                pulses[i] = (byte)(int.Parse(this._dataGridReleView[2, i].Value.ToString()) / TEN_FACTOR);
                sensorsRelaysState[0, i] = Convert.ToBoolean(this._dataGridReleView[3, i].Value);
                sensorsRelaysState[1, i] = Convert.ToBoolean(this._dataGridReleView[4, i].Value);
                sensorsRelaysState[2, i] = Convert.ToBoolean(this._dataGridReleView[5, i].Value);

            }
            int index = this.GetMdoIndex();
            this._fmOld.MdoConfig[index].SensorsRelaysState = sensorsRelaysState;
            this._fmOld.MdoConfig[index].OfflineRelaysType = types;
            this._fmOld.MdoConfig[index].OfflineRelaysPulse = pulses;
        }
        //Обработка изменения состояния датчиков
        private void MdoSensorsChanged()
        {
            int index = this.GetMdoIndex();
            this._fmOld.MdoConfig[index].SensorsState = this._mdoSensorSwitchControl.Sensors;
        }
        //Обработка изменение состояния чекбокса "Тест"
        private void TestChange()
        {
            bool flag = this._testEnabled.Checked;
            this._testTime.Enabled = flag;
            int index = this.GetMdoIndex();
            this._fmOld.MdoConfig[index].IsTest = flag;
        }
        //Изменение типа МДО
        private void MdoTypeChanged()
        {
            int index = this.GetMdoIndex();
            if (index >= 0)
            {
                this._fmOld.MdoConfig[index].MdoType = this._mdoTypeComboBox.SelectedItem.ToString();
                bool workInSystem = this._fmOld.MdoConfig[index].MdoType != Strings.MdoType[0];
                this._mdoSensorSwitchControl.Enabled = workInSystem;
                this._testSensorsGroupBox.Enabled = workInSystem;
                this._relayInSystemGroupBox.Enabled = workInSystem;
                this._relayOfflineGroupBox.Enabled = workInSystem;
                this._releMdoDataGridView.ForeColor = workInSystem ? Color.Black : Color.Gray;
                this._dataGridReleView.ForeColor = workInSystem ? Color.Black : Color.Gray;
            }
        }
        //Изменение "Время теста"
        private void TestTimeChanged()
        {
            int index = this.GetMdoIndex();
            byte result;
            if (byte.TryParse(this._testTime.Text, out result))
            {
                if (((this._testEnabled.Checked) & (result >= 1) & (result <= 250)) | (!this._testEnabled.Checked))
                {
                    this._fmOld.MdoConfig[index].TestTime = result;
                    return;
                }
            }
            MessageBox.Show(TEST_TIME_IS_ERROR);

        }
        //Обновляет структуру введёнными данными(Дискреты Теза)
        private void TezDiscretsChanged()
        {

            DiscretConfigTezStruct[] discrets = this._fmOld.Discrets;
            for (int i = 0; i < FlashMemoryStructOld.TEZ_DISCRETS_COUNTS; i++)
            {
                string value = this._discretsDataGridView[1, i].Value.ToString();
                discrets[i].DiscretType = (ushort)Strings.DiscretType.IndexOf(value);
            }
        }
        //Обновляет структуру введёнными данными(Реле Теза)
        private void TezReleChanged()
        {
            ReleyConfigTezStruct[] releys = this._fmOld.Releys;
            for (int i = 0; i < FlashMemoryStructOld.TEZ_RELEYS_COUNTS; i++)
            {
                string type = this._releDataGridView[1, i].Value.ToString();
                string pulse = this._releDataGridView[2, i].Value.ToString();
                string signal = this._releDataGridView[3, i].Value.ToString();
                releys[i].ReleSignal = (byte)Strings.ReleTezSignal.IndexOf(signal);
                releys[i].RelayTime = (byte)(int.Parse(pulse) / TEN_FACTOR);
                releys[i].ReleType = (byte)Strings.ReleType.IndexOf(type);
            }
        }
        //Обновляет структуру введёнными данными(Реле МДО)
        private void MdoReleChanged()
        {
            byte[][] releys = new byte[MdoConfigStruct.RELEYS_COUNT][];
            for (int i = 0; i < MdoConfigStruct.RELEYS_COUNT; i++)
            {
                string releType = this._releMdoDataGridView[1, i].Value.ToString();
                string relePulse = this._releMdoDataGridView[2, i].Value.ToString();
                string releSignal = this._releMdoDataGridView[3, i].Value.ToString();


                releys[i] = new[]
                    {
                        (byte) Strings.ReleType.IndexOf(releType),
                        (byte) (int.Parse(relePulse)/TEN_FACTOR),
                        (byte) Strings.ReleMdoSignalOldVersion.IndexOf(releSignal)
                    };
            }
            int index = this.GetMdoIndex();
            this._fmOld.MdoConfig[index].Releys = releys;
        }
        //Индекс выбранного МДО(0-23)
        private int GetMdoIndex()
        {
            return this._deviceAddr.SelectedIndex;
        }

        //Команда успешно записана
        private void CommandIsWrite()
        {
            if (this._writeFlashMemoryState == FlashWriteStates.START_WRITE)
                this.FlashMemoryWrite();
            if (this._writeFlashMemoryState == FlashWriteStates.NONE)
            {
                this._statusLabel.Text = SETPOINT_IS_WRITE;
                this._configProgressBar.Value = this._configProgressBar.Maximum;
                MessageBox.Show(SETPOINT_IS_WRITE);
            }

        }
        //Конфигурация не записана
        private void StructAllWriteFail()
        {
            this._statusLabel.Text = IMPOSSIBLE_WRITE_CONFIGURATION;
            MessageBox.Show(IMPOSSIBLE_WRITE_CONFIGURATION);
        }
        //Конфигурация не прочитана
        private void StructAllReadFail()
        {
            this._configProgressBar.Value = this._configProgressBar.Maximum;
            if (!this._errorMessage)
            {
                MessageBox.Show(IMPOSSIBLE_READ_CONFIGURATION);
                this._errorMessage = true;
            }
        }

        #endregion Вспомогательные методы


        #region EventHandlers
        //Загрузка формы
        private void TezConfigurationForm_Load(object sender, EventArgs e)
        {
            if(Device.AutoloadConfig)
                this.LoadConfigurationBlocks();
        }
        //Изменение состояния чекбокса "Тест"
        private void _TestEnabled_CheckedChanged(object sender, EventArgs e)
        {
            this.TestChange();
        }
        
        //Закрытие формы
        private void TezConfigurationForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.CloseForm();
        }

        //Выбран другой МДО
        private void _deviceAddr_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.ShowMdoStatus(this._deviceAddr.SelectedIndex);
        }
        //"Чтение из устройства"
        private void _readConfigBut_Click(object sender, EventArgs e)
        {
            this.LoadConfigurationBlocks();
        }
        //Дискреты Теза изменены
        private void _discretsDataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            this.TezDiscretsChanged();
        }
        //Реле Теза изменены
        private void _releDataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 2)
            {
                DataGridView sourse = sender as DataGridView;
                if (sourse != null)
                    if (this.PulseValidate(sourse, e.ColumnIndex, e.RowIndex))
                        this.TezReleChanged();
            }
            else
            {
                this.TezReleChanged();
            }

        }

        //Нажатие "Записать в устройство"
        private void _writeConfigBut_Click(object sender, EventArgs e)
        {
            this.WriteConfigurationToDevice();
        }

        //Выбран новый тип МДО
        private void _mdoTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.MdoTypeChanged();
        } 

        //Реле МДО изменены
        private void _releMdoDataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 2)
            {
                DataGridView sourse = sender as DataGridView;
                if (sourse != null)
                    if (this.PulseValidate(sourse, e.ColumnIndex, e.RowIndex))
                        this.MdoReleChanged();
            }
            else
            {
                this.MdoReleChanged();
            }


        }
        private bool PulseValidate(DataGridView sourse, int column, int row)
        {
            int value;
            if (int.TryParse(sourse[column, row].Value.ToString(), out value))
            {
                if ((value >= 10 & value <= 1000) & (value % 10 == 0))
                {
                    sourse[column, row].Style.BackColor = Color.White;
                    return true;
                }
            }
            sourse[column, row].Style.BackColor = Color.Red;
            MessageBox.Show(PULSE_INCORRECT_VALUE);
            return false;
        }
        //Сохранить в файл
        private void _saveConfigBut_Click(object sender, EventArgs e)
        {
            this.SaveToFileConfiguration();
        }
        //Загрузить из файла
        private void _loadConfigBut_Click(object sender, EventArgs e)
        {
            this.LoadFromFileConfiguration();
        }
        //Изменение включенных датчиков
        private void mdoSensorSwitchControl1_CheckChanges(object sender, EventArgs e)
        {
            this.MdoSensorsChanged();
        }
        //Изменение реле в автономном режиме
        private void _dataGridReleView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 2)
            {
                DataGridView sourse = sender as DataGridView;
                if (sourse != null)
                    if (this.PulseValidate(sourse, e.ColumnIndex, e.RowIndex))
                        this.MdoOfflineSensorsChanged();
            }
            else
            {
                this.MdoOfflineSensorsChanged();
            }
        }

        #endregion EventHandlers

        private void TezConfigurationForm_Shown(object sender, EventArgs e)
        {
            this._deviceAddr.SelectedIndex = 0;
        }

        private void logicSignalsControl1_UpdateData(object sender, EventArgs e)
        {
            TezUrovRelayStruct tezurov = this._fmOld.TezUrovRelayStruct;
            tezurov.LogicSignals = this._logicSignalsControl.Signals;
            this._fmOld.TezUrovRelayStruct = tezurov;
        }

        private void _tezFaultCheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            TezUrovRelayStruct tezurov = this._fmOld.TezUrovRelayStruct;
            tezurov.RelayFaultConfiguration = new[]
                {
                    this._tezFaultCheckBox1.Checked,
                    this._tezFaultCheckBox2.Checked,
                    this._tezFaultCheckBox3.Checked
                };
            this._fmOld.TezUrovRelayStruct = tezurov;
        }

        private void _testTime_TextChanged(object sender, EventArgs e)
        {
            this.TestTimeChanged();
        }

        private void _urovTextBox_TextChanged(object sender, EventArgs e)
        {
            this.UrovValidate();
        }

        private bool UrovValidate()
        {
            ushort result;
            TezUrovRelayStruct tezurov = this._fmOld.TezUrovRelayStruct;
            tezurov.UrovEnabled = this._urovEnabled.Checked;
            this._fmOld.TezUrovRelayStruct = tezurov;
            // _flashMemory.Value = _fmOld;
            // _flashMemory.SaveStruct();
            if (!this._urovEnabled.Checked)
            {
                return true;
            }

            if (ushort.TryParse(this._urovTextBox.Text, out result))
            {
                if ((result >= 10) & (result <= 1000))
                {

                    tezurov.UrovTime = result;
                    this._fmOld.TezUrovRelayStruct = tezurov;
                    //   _flashMemory.Value = _fmOld;
                    //  _flashMemory.SaveStruct();
                    return true;
                }
            }
            MessageBox.Show(UROV_ERROR);
            return false;

        }

        private void _urovEnabled_CheckedChanged(object sender, EventArgs e)
        {
            this._urovTextBox.Enabled = this._urovEnabled.Checked;
        }

        #region Сохранение конфигурации в файл
        /// <summary>
        /// Сохранение конфигурации в файл
        /// </summary>
        /// <param name="binFileName">Имя файла</param>
        public void Serialize(string binFileName)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.AppendChild(doc.CreateElement("TEZ"));


                ushort[] values = this._fmOld.GetValues();

                XmlElement element = doc.CreateElement(XML_HEAD);
                element.InnerText = Convert.ToBase64String(Common.TOBYTES(values, false));
                if (doc.DocumentElement == null)
                {
                    throw new NullReferenceException();
                }
                doc.DocumentElement.AppendChild(element);

                doc.Save(binFileName);
                this._statusLabel.Text = string.Format("Файл {0} успешно сохранён", binFileName);
            }
            catch
            {
                MessageBox.Show(FILE_SAVE_FAIL);
            }
        }

        /// <summary>
        /// Загрузка конфигурации из файла
        /// </summary>
        /// <param name="binFileName">Имя файла</param>
        public void Deserialize(string binFileName)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(binFileName);
                XmlNode a = doc.FirstChild.SelectSingleNode(XML_HEAD);
                if (a == null)
                    throw new NullReferenceException();

                byte[] values = Convert.FromBase64String(a.InnerText);
                FlashMemoryStructOld _fmStruct = new FlashMemoryStructOld(true);
                _fmStruct.InitStruct(values);
                this._fmOld = _fmStruct;
                this.ShowTezStatus();
                this._statusLabel.Text = string.Format("Файл {0} успешно загружен", binFileName);
            }
            catch
            {
                MessageBox.Show(FILE_LOAD_FAIL);
            }
        }


        //Сохранение уставок в файл
        private void SaveToFileConfiguration()
        {
            this._saveConfigurationDlg.FileName = $"Уставки ТЭЗ-24 Версия {this._device.DeviceVersion}";
            if (DialogResult.OK == this._saveConfigurationDlg.ShowDialog())
            {
                this.Serialize(this._saveConfigurationDlg.FileName);
                MessageBox.Show(string.Format(FILE_IS_SAVED_PATTERN, this._saveConfigurationDlg.FileName));
            }
        }
        //Загрузка уставок из файла
        private void LoadFromFileConfiguration()
        {
            if (DialogResult.OK == this._openConfigurationDlg.ShowDialog())
            {
                this.Deserialize(this._openConfigurationDlg.FileName);
            }
        }
        #endregion

        private void TezConfigurationForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers != Keys.Control) return;
            switch (e.KeyCode)
            {
                case Keys.W:
                    this.WriteConfigurationToDevice();
                    break;
                case Keys.R:
                    this.LoadConfigurationBlocks();
                    break;
                case Keys.S:
                    this.SaveToFileConfiguration();
                    break;
                case Keys.O:
                    this.LoadFromFileConfiguration();
                    break;
            }
            e.Handled = true;
        }

        private void contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ((ContextMenuStrip)sender).Close();
            if (e.ClickedItem == this.readfromDeviceItem)
            {
                this.LoadConfigurationBlocks();
                return;
            }
            if (e.ClickedItem == this.writeToDevice)
            {
                this.WriteConfigurationToDevice();
                return;
            }
            if (e.ClickedItem == this.readFromFileItem)
            {
                this.LoadFromFileConfiguration();
                return;
            }
            if (e.ClickedItem == this.writeToFileItem)
            {
                this.SaveToFileConfiguration();
                return;
            }
        }

        private void contextMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.readfromDeviceItem.Enabled =
                this.writeToDevice.Enabled = this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode;
        }
    }
}
