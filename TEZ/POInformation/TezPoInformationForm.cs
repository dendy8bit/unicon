﻿using System;
using System.Drawing;
using System.Globalization;
using System.Text;
using System.Windows.Forms;
using AssemblyResources;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Interfaces;

namespace BEMN.TEZ.POInformation
{
    public partial class TezPoInformationForm : Form, IFormView
    {
        #region [Private fields]
        private const string UPDATE_FAIL = "Невозможно обновить ПО. Проверте связь.";
        private const string VERSION_READ_FAIL = "Нет связи с устройством";
        private const string UNICON_CLOSE = "Для обновления ПО \"Уникон\" будет закрыт\nПродолжить?";
        private const string PO_UPDATE = "Обновление ПО";
        private const string VERSION_PO = "Версия ПО - ";
        private readonly MemoryEntity<OneWordStruct> _version;
        private readonly MemoryEntity<OneWordStruct> _update;
        private Tez _device;
        #endregion [Private fields]


        #region [Ctor's]
        public TezPoInformationForm()
        {
            InitializeComponent();
        }

        public TezPoInformationForm(Tez device)
        {
            InitializeComponent();
            this._device = device;
            this._version = device.VersionEntity;
            this._version.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, VersionReadOk);
            this._version.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, VersionReadFail);
            this._update = device.UpdateEntity;
            this._update.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, UpdateOk);
            this._update.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, UpdateFail);
        }
        #endregion [Ctor's]


        #region [Memory Entity events]
        private void UpdateFail()
        {
            MessageBox.Show(UPDATE_FAIL);
        }

        private void UpdateOk()
        {
            Framework.Framework.MainWindow.Close();
           
        }

        private void VersionReadFail()
        {
            this._versionLabel.Text = VERSION_READ_FAIL;
            this._updateButton.Enabled = false;
        }

        private void VersionReadOk()
        {
            try
            {
                this._updateButton.Enabled = this._version.Value.Word >= 120;

                var value = new StringBuilder(this._version.Value.Word.ToString(CultureInfo.InvariantCulture));
                value.Insert(1, ".");
                value.Insert(3, ".");
                this._versionLabel.Text = VERSION_PO + value;
            }
            catch (Exception)
            {
                this._versionLabel.Text = "Ошибка определения версии";
            }

        }
        #endregion [Memory Entity events]


        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(Tez); }
        }

        public bool Multishow { get; private set; }

        #endregion


        #region INodeView Members

        public Type ClassType
        {
            get { return typeof(TezPoInformationForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.Calibrate; }
        }

        public string NodeName
        {
            get { return "Информация ПО"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion


        #region [Event's handlers]
        private void MdoPoInformationForm_Load(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._version.LoadStruct();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show(UNICON_CLOSE, PO_UPDATE, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                this._update.Value = new OneWordStruct { Word = 1 };
                this._update.SaveStruct();
            }
        }
        #endregion [Event's handlers]
    }
}
