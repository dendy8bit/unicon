﻿using System.Collections.Generic;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.Devices.Structures;
using BEMN.MBServer;
using BEMN.TEZ.SupportClasses;

namespace BEMN.TEZ.SystemJournal.Structures
{
    public struct JournalRecordStruct : IStruct, IStructInit
    {
        public ushort years;
        public ushort month;
        public ushort date;
        public ushort day;
        public ushort hour;
        public ushort min;
        public ushort sec;
        public ushort msec;
        public ushort msg;//JournalType
        public ushort JournalVol;//числовое значение 0-24


        public string JournalRecordTime
        {
            get
            {
                return this.date.ToString("00") +
                    "." +
                    this.month.ToString("00") +
                    "." +
                    this.years.ToString("0000") +
                    "   " +
                    this.hour.ToString("00") +
                    ":" +
                    this.min.ToString("00") +
                    ":" +
                    this.sec.ToString("00") +
                    "," +
                    this.msec.ToString("000");
            }
        }

        public string JournalRecordMessage
        {
            get { return this.msg <= Strings.JMessage.Count ? Strings.JMessage[this.msg] : this.msg.ToString(); }
        }

        public int JournalRecordMessageNumber
        {
            get { return this.msg; }
        }

        public int JournalVolume
        {
            get { return this.JournalVol; }
        }

        public StructInfo GetStructInfo(int len = StructBase.MAX_SLOT_LENGHT_DEFAULT)
        {
            return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool slotArray, int slotLength)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType());
        }

        public void InitStruct(byte[] array)
        {
            int index = 0;
            this.years = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.month = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.date = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.day = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.hour = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.min = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.sec = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.msec = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.msg = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.JournalVol = Common.TOWORD(array[index + 1], array[index]);
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.Add(this.years);
            result.Add(this.month);
            result.Add(this.date);
            result.Add(this.day);
            result.Add(this.hour);
            result.Add(this.min);
            result.Add(this.sec);
            result.Add(this.msec);
            result.Add(this.msg);
            result.Add(this.JournalVol);
            return result.ToArray();
        }
    }
}