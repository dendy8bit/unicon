﻿using System.Collections.Generic;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;

namespace BEMN.TEZ.SystemJournal.Structures
{
    /// <summary>
    /// Структура для журнала 250 записей
    /// </summary>
    public struct NewJournalV10Struct : IStruct, IStructInit
    {
        public JournalInfoStruct _JournalInfoStruct;
        public JournalStructV10 _JournalStructV10;


        public NewJournalV10Struct(bool a)
        {
            this._JournalInfoStruct = new JournalInfoStruct();
            this._JournalStructV10 = new JournalStructV10();

        }

        public StructInfo GetStructInfo(int len)
        {
            return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool slotArray, int arrayLength)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType());
        }

        public void InitStruct(byte[] array)
        {
            int index = 0;
            this._JournalInfoStruct = StructHelper.GetOneStruct(array, ref index, this._JournalInfoStruct);
            this._JournalStructV10 = StructHelper.GetOneStruct(array, ref index, this._JournalStructV10);
        }

        public ushort[] GetValues()
        {
            List<ushort> res = new List<ushort>();
            res.AddRange(this._JournalInfoStruct.GetValues());
            res.AddRange(this._JournalStructV10.GetValues());
            return res.ToArray();
        }
    }
}
