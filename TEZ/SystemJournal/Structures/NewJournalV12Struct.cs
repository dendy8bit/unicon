﻿using System.Collections.Generic;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;

namespace BEMN.TEZ.SystemJournal.Structures
{
    /// <summary>
    /// Структура которая содержит в себе 2 структуры
    /// </summary>
    public struct NewJournalV12Struct : IStruct, IStructInit
    {
        public JournalInfoStruct _JournalInfoStruct;
        public JournalStructV12 _JournalStructV12;

        public StructInfo GetStructInfo(int len)
        {
            return StructHelper.GetStructInfo(this.GetType(), len);
        }

        public object GetSlots(ushort start, bool slotArray, int length)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType(), length);
        }

        public void InitStruct(byte[] array)
        {
            int _index = 0;
            this._JournalInfoStruct = StructHelper.GetOneStruct(array, ref _index, this._JournalInfoStruct);
            this._JournalStructV12 = StructHelper.GetOneStruct(array, ref _index, this._JournalStructV12);
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.AddRange(this._JournalInfoStruct.GetValues());
            result.AddRange(this._JournalStructV12.GetValues());
            return result.ToArray();
        }
    }

}
