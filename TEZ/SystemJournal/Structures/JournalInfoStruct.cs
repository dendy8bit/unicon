﻿using System.Collections.Generic;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.MBServer;

namespace BEMN.TEZ.SystemJournal.Structures
{
    /// <summary>
    /// Информация о журнале 
    /// </summary>
    public struct JournalInfoStruct : IStruct, IStructInit
    {
        #region Поля
        /// <summary>
        /// Количество записей в журнале
        /// </summary>
        private ushort _journalRecordCount;

        /// <summary>
        /// Длина (смещение) 
        /// </summary>
        private ushort _journalRecordEnd;

        /// <summary>
        /// Флаг новой записи
        /// </summary>
        private ushort _flag;
        #endregion

        #region Свойства
        public ushort JournalRecordCount
        {
            get { return this._journalRecordCount; }
        }
        public ushort JournalRecordEnd
        {
            get { return this._journalRecordEnd; }
        }
        public ushort Flag
        {
            get { return this._flag; }
            set { this._flag = value; }
        }
        public StructInfo GetStructInfo(int len)
        {
            return StructHelper.GetStructInfo(this.GetType());
        }
        #endregion

        #region Методы
        public object GetSlots(ushort start, bool slotArray, int slotLength)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType());
        }

        public void InitStruct(byte[] array)
        {
            int _index = 0;
            this._journalRecordCount = Common.TOWORD(array[_index + 1], array[_index]);
            _index += sizeof(ushort);

            this._journalRecordEnd = Common.TOWORD(array[_index + 1], array[_index]);
            _index += sizeof(ushort);

            this._flag = Common.TOWORD(array[_index + 1], array[_index]);
        }

        public ushort[] GetValues()
        {
            var result = new List<ushort>();
            result.Add(this.JournalRecordCount);
            result.Add(this.JournalRecordEnd);
            result.Add(this.Flag);
            return result.ToArray();
        }
        #endregion
    }
}