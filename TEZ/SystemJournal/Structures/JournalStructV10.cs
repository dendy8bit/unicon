﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;

namespace BEMN.TEZ.SystemJournal.Structures
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct JournalStructV10 : IStruct, IStructInit
    {
        public const int JOURNAL_RECORDS_COUNT = 400;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = JOURNAL_RECORDS_COUNT)]
        public JournalRecordStruct[] Records;

        public JournalStructV10(bool a)
        {
            this.Records = new JournalRecordStruct[JOURNAL_RECORDS_COUNT];
        }

        public StructInfo GetStructInfo(int len)
        {
            return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool slotArray, int slotLength)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType());
        }

        public void InitStruct(byte[] array)
        {
            this.Records = new JournalRecordStruct[JOURNAL_RECORDS_COUNT];

            byte[] oneStruct;
            int index = 0;
            for (int i = 0; i < this.Records.Length; i++)
            {
                oneStruct = new byte[Marshal.SizeOf(typeof(JournalRecordStruct))]; // одна запись журнала в байтах
                Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
                this.Records[i].InitStruct(oneStruct);
                index += Marshal.SizeOf(typeof(JournalRecordStruct));
            }
        }

        public ushort[] GetValues()
        {
            var result = new List<ushort>();
            for (var j = 0; j < this.Records.Length; j++)
            {
                result.AddRange(this.Records[j].GetValues());
            }
            return result.ToArray();
        }
    }
}