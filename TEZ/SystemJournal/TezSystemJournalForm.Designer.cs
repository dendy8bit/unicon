﻿using System.Windows.Forms;

namespace BEMN.TEZ.SystemJournal
{
    partial class TezSystemJournalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._sysJournalGrid = new System.Windows.Forms.DataGridView();
            this._indexCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._timeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._msgCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._valueCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this._configProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this._statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._deserializeSysJournalBut = new System.Windows.Forms.Button();
            this._serializeSysJournalBut = new System.Windows.Forms.Button();
            this._readSysJournalBut = new System.Windows.Forms.Button();
            this._saveSysJournalDlg = new System.Windows.Forms.SaveFileDialog();
            this._openSysJounralDlg = new System.Windows.Forms.OpenFileDialog();
            this._readAllBut = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this._sysJournalGrid)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _sysJournalGrid
            // 
            this._sysJournalGrid.AllowUserToAddRows = false;
            this._sysJournalGrid.AllowUserToDeleteRows = false;
            this._sysJournalGrid.AllowUserToResizeColumns = false;
            this._sysJournalGrid.AllowUserToResizeRows = false;
            this._sysJournalGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._sysJournalGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._sysJournalGrid.BackgroundColor = System.Drawing.Color.White;
            this._sysJournalGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this._sysJournalGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._sysJournalGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._indexCol,
            this._timeCol,
            this._msgCol,
            this._valueCol});
            this._sysJournalGrid.Location = new System.Drawing.Point(0, 0);
            this._sysJournalGrid.Name = "_sysJournalGrid";
            this._sysJournalGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this._sysJournalGrid.RowHeadersVisible = false;
            this._sysJournalGrid.RowHeadersWidth = 42;
            this._sysJournalGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._sysJournalGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._sysJournalGrid.Size = new System.Drawing.Size(510, 438);
            this._sysJournalGrid.TabIndex = 15;
            // 
            // _indexCol
            // 
            this._indexCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._indexCol.Frozen = true;
            this._indexCol.HeaderText = "№";
            this._indexCol.MinimumWidth = 30;
            this._indexCol.Name = "_indexCol";
            this._indexCol.ReadOnly = true;
            this._indexCol.Width = 43;
            // 
            // _timeCol
            // 
            this._timeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._timeCol.Frozen = true;
            this._timeCol.HeaderText = "Время";
            this._timeCol.MinimumWidth = 150;
            this._timeCol.Name = "_timeCol";
            this._timeCol.ReadOnly = true;
            this._timeCol.Width = 150;
            // 
            // _msgCol
            // 
            this._msgCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._msgCol.Frozen = true;
            this._msgCol.HeaderText = "Сообщение";
            this._msgCol.MinimumWidth = 150;
            this._msgCol.Name = "_msgCol";
            this._msgCol.ReadOnly = true;
            this._msgCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._msgCol.Width = 150;
            // 
            // _valueCol
            // 
            this._valueCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._valueCol.HeaderText = "Значение";
            this._valueCol.MinimumWidth = 40;
            this._valueCol.Name = "_valueCol";
            this._valueCol.ReadOnly = true;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._configProgressBar,
            this._statusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 478);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(510, 22);
            this.statusStrip1.TabIndex = 19;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // _configProgressBar
            // 
            this._configProgressBar.Name = "_configProgressBar";
            this._configProgressBar.Size = new System.Drawing.Size(100, 16);
            // 
            // _statusLabel
            // 
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.Size = new System.Drawing.Size(16, 17);
            this._statusLabel.Text = "...";
            // 
            // _deserializeSysJournalBut
            // 
            this._deserializeSysJournalBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._deserializeSysJournalBut.Location = new System.Drawing.Point(372, 444);
            this._deserializeSysJournalBut.Name = "_deserializeSysJournalBut";
            this._deserializeSysJournalBut.Size = new System.Drawing.Size(126, 23);
            this._deserializeSysJournalBut.TabIndex = 22;
            this._deserializeSysJournalBut.Text = "Загрузить из файла";
            this._deserializeSysJournalBut.UseVisualStyleBackColor = true;
            this._deserializeSysJournalBut.Click += new System.EventHandler(this._deserializeSysJournalBut_Click);
            // 
            // _serializeSysJournalBut
            // 
            this._serializeSysJournalBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._serializeSysJournalBut.Location = new System.Drawing.Point(255, 444);
            this._serializeSysJournalBut.Name = "_serializeSysJournalBut";
            this._serializeSysJournalBut.Size = new System.Drawing.Size(111, 23);
            this._serializeSysJournalBut.TabIndex = 21;
            this._serializeSysJournalBut.Text = "Сохранить в файл";
            this._serializeSysJournalBut.UseVisualStyleBackColor = true;
            this._serializeSysJournalBut.Click += new System.EventHandler(this._serializeSysJournalBut_Click);
            // 
            // _readSysJournalBut
            // 
            this._readSysJournalBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._readSysJournalBut.Location = new System.Drawing.Point(12, 444);
            this._readSysJournalBut.Name = "_readSysJournalBut";
            this._readSysJournalBut.Size = new System.Drawing.Size(111, 23);
            this._readSysJournalBut.TabIndex = 20;
            this._readSysJournalBut.Text = "Прочитать";
            this._readSysJournalBut.UseVisualStyleBackColor = true;
            this._readSysJournalBut.Click += new System.EventHandler(this._readSysJournalBut_Click);
            // 
            // _saveSysJournalDlg
            // 
            this._saveSysJournalDlg.DefaultExt = "xml";
            this._saveSysJournalDlg.FileName = "ЖурналСистемы ТЭЗ-24";
            this._saveSysJournalDlg.Filter = "Журнал системы ТЭЗ-24 | *.xml";
            this._saveSysJournalDlg.Title = "Сохранить  журнал системы для ТЭЗ-24";
            // 
            // _openSysJounralDlg
            // 
            this._openSysJounralDlg.DefaultExt = "xml";
            this._openSysJounralDlg.FileName = "Журнал системы ТЭЗ-24";
            this._openSysJounralDlg.Filter = "Журнал системы ТЭЗ-24 | *.xml";
            this._openSysJounralDlg.RestoreDirectory = true;
            this._openSysJounralDlg.Title = "Открыть журнал системы для ТЭЗ-24";
            // 
            // _readAllBut
            // 
            this._readAllBut.Location = new System.Drawing.Point(129, 444);
            this._readAllBut.Name = "_readAllBut";
            this._readAllBut.Size = new System.Drawing.Size(120, 23);
            this._readAllBut.TabIndex = 23;
            this._readAllBut.Text = "Прочитать всё";
            this._readAllBut.UseVisualStyleBackColor = true;
            this._readAllBut.Visible = false;
            this._readAllBut.Click += new System.EventHandler(this._readAllBut_Click);
            // 
            // TezSystemJournalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(510, 500);
            this.Controls.Add(this._readAllBut);
            this.Controls.Add(this._deserializeSysJournalBut);
            this.Controls.Add(this._serializeSysJournalBut);
            this.Controls.Add(this._readSysJournalBut);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this._sysJournalGrid);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(526, 538);
            this.Name = "TezSystemJournalForm";
            this.Text = "TEZ_SystemJournal";
            this.Activated += new System.EventHandler(this.TezSystemJournalForm_Activated);
            this.Load += new System.EventHandler(this.TEZ_SystemJournal_Load);
            ((System.ComponentModel.ISupportInitialize)(this._sysJournalGrid)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView _sysJournalGrid;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripProgressBar _configProgressBar;
        private System.Windows.Forms.ToolStripStatusLabel _statusLabel;
        private System.Windows.Forms.Button _deserializeSysJournalBut;
        private System.Windows.Forms.Button _serializeSysJournalBut;
        private System.Windows.Forms.Button _readSysJournalBut;
        private System.Windows.Forms.SaveFileDialog _saveSysJournalDlg;
        private System.Windows.Forms.OpenFileDialog _openSysJounralDlg;
        private DataGridViewTextBoxColumn _indexCol;
        private DataGridViewTextBoxColumn _timeCol;
        private DataGridViewTextBoxColumn _msgCol;
        private DataGridViewTextBoxColumn _valueCol;
        private Button _readAllBut;
    }
}