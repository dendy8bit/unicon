﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using AssemblyResources;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Interfaces;
using BEMN.TEZ.SupportClasses;
using BEMN.TEZ.SystemJournal.Structures;

namespace BEMN.TEZ.SystemJournal
{
    public partial class TezSystemJournalForm : Form, IFormView
    {
        #region Consts
        private const string FIND_MESSAGES_PATTERN = "В журнале {0} сообщений";
        private const string FROM_REMOTE = "С пульта";
        private const string SDTU = "СДТУ";
        private const string MDO_PATTERN = "МДО {0}";
        private const string JOURNAL_READ_ERROR = "Невозможно прочитать журнал";
        private const string JOURNAL_READING = "Идёт чтение журнала";
        private const string CRC_STATE = "CRC уставок";
        private const string DISCRET_ERROR = "Ошибка дискрета";
        private const string CLOCK_ERROR = "Ошибка часов";
        #endregion Consts
        
        #region Поля
        private readonly Tez _device;
        private List<JournalRecordStruct> _journal = new List<JournalRecordStruct>();
        private int _recsCount;
        private int _endRecIndex;
        private MemoryEntity<NewJournalV10Struct> _journalStructV10New;
        private MemoryEntity<NewJournalV12Struct> _journalStructV12New;
        private MemoryEntity<OneWordStruct> _versionEntity;
        #endregion Поля
        
        #region Конструкторы
        public TezSystemJournalForm()
        {
            this.InitializeComponent();
        }

        public TezSystemJournalForm(Tez device)
        {
            this.InitializeComponent();
            this._device = device;
            this._journalStructV10New = this._device.JournalV10;
            this._journalStructV12New = this._device.JournalV12;
            this._versionEntity = this._device.VersionEntitySj;
            this._versionEntity.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.LoadJournal);
            this._versionEntity.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.TEZ_Journal_AllReadFail);
            this._device.JournalV10.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.TEZ_Journal_AllReadOk);
            this._device.JournalV10.ReadOk += HandlerHelper.CreateHandler(this, this.TEZ_Journal_ReadOk);
            this._device.JournalV12.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.TEZ_Journal_AllReadOk);
            this._device.JournalV12.ReadOk += HandlerHelper.CreateHandler(this, this.TEZ_Journal_ReadOk);
        }
        #endregion Конструкторы
        
        #region события StObj
        
        //Полоса загрузки
        private void TEZ_Journal_ReadOk()
        {
            this._configProgressBar.Increment(1);
        }

        // журнал
        private void LoadJournal()
        {
            if (this._versionEntity.Value.Word > 120 || this._versionEntity.Value.Word > 113)
            {
                this._journalStructV12New.LoadStruct();
                this._configProgressBar.Maximum = this._journalStructV12New.Slots.Count;
            }
            else
            {
                this._journalStructV10New.LoadStruct();
                this._configProgressBar.Maximum = this._journalStructV10New.Slots.Count;     
            }

            this._statusLabel.Text = JOURNAL_READING;
        }

        private void TEZ_Journal_AllReadOk()
        {
            if (this._versionEntity.Value.Word > 120 || this._versionEntity.Value.Word > 113)
            {
                //Подготовка значений(смещение, размер журнала)
                this._recsCount = this._device.JournalV12.Value._JournalInfoStruct.JournalRecordCount;
                this._endRecIndex = this._device.JournalV12.Value._JournalInfoStruct.JournalRecordEnd / new JournalRecordStruct().GetStructInfo().FullSize;
            }
            else
            {
                //Подготовка значений(смещение, размер журнала)
                this._recsCount = this._journalStructV10New.Value._JournalInfoStruct.JournalRecordCount;
                this._endRecIndex = this._journalStructV10New.Value._JournalInfoStruct.JournalRecordEnd / new JournalRecordStruct().GetStructInfo().FullSize;               
            }
            //Вывод журнала
            this.ReadJournal();
        }
        private void TEZ_Journal_AllReadFail()
        {
            MessageBox.Show(JOURNAL_READ_ERROR);
        }
        #endregion события StObj

        //Вывод журнала на экран
        private void ReadJournal()
        {
            this._readSysJournalBut.Enabled = true;
            JournalRecordStruct[] records;
            if (this._versionEntity.Value.Word > 120 || this._versionEntity.Value.Word > 113)
            {
                //400
                records = this._journalStructV12New.Value._JournalStructV12.Records;
            }
            else
            {
                records = this._journalStructV10New.Value._JournalStructV10.Records;
            }
            this._journal.AddRange(records);

            //Находим и сортируем записи журнала
            if (this._endRecIndex - this._recsCount >= 0)
            {
                this._journal = this._journal.GetRange(this._endRecIndex - this._recsCount, this._recsCount);
                this._journal.Reverse();
            }
            else
            {
                List<JournalRecordStruct> firstJournal = this._journal.GetRange(0, this._endRecIndex);
                List<JournalRecordStruct> secondJournal = this._journal.GetRange(this._endRecIndex, this._recsCount - this._endRecIndex);
                firstJournal.Reverse();
                secondJournal.Reverse();
                this._journal.Clear();
                this._journal.AddRange(firstJournal);
                this._journal.AddRange(secondJournal);
            }
            //Выводим журнал в DataGrid
            for (var i = 0; i < this._journal.Count; i++)
            {
                //datatable
                this._sysJournalGrid.Rows.Add((i + 1).ToString("000"), this._journal[i].JournalRecordTime, this._journal[i].JournalRecordMessage,
                                         JournalVolumeToString(this._journal[i].JournalRecordMessageNumber, this._journal[i].JournalVolume));
            }

            this._statusLabel.Text = string.Format(FIND_MESSAGES_PATTERN, this._journal.Count);
        }
        //Запуск чтения журнала
        private void LoadConfigurationBlocks()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            //Делаем кнопку не доступной для чтения
            this._readSysJournalBut.Enabled = false;
            this._configProgressBar.Value = 0;
            this._sysJournalGrid.Rows.Clear();
            this._journal.Clear();
            this._versionEntity.LoadStruct();  
        }
        //Подготовка DataTable
        private DataTable GetDataTable()
        {
            var table = new DataTable("ТЭЗ_журнал_системы");
            table.Columns.Add("Номер");
            table.Columns.Add("Время");
            table.Columns.Add("Сообщение");
            table.Columns.Add("Значение");
            return table;
        }
        //Сохранение журнала в файл
        private void SaveJournalToFile()
        {
            if (this._saveSysJournalDlg.ShowDialog() == DialogResult.OK)
            {
                var table = this.GetDataTable();
                for (int i = 0; i < this._sysJournalGrid.Rows.Count; i++)
                {
                    table.Rows.Add(new[]
                        {
                            this._sysJournalGrid["_indexCol", i].Value, this._sysJournalGrid["_timeCol", i].Value, this._sysJournalGrid["_msgCol", i].Value, this._sysJournalGrid["_valueCol", i].Value
                        });
                }
                table.WriteXml(this._saveSysJournalDlg.FileName);
            }
        }
        //Загрузка журнала из файла
        private void LoadJournalFromFile()
        {

            if (DialogResult.OK == this._openSysJounralDlg.ShowDialog())
            {
                this._sysJournalGrid.Rows.Clear();
                var table = this.GetDataTable();
                table.ReadXml(this._openSysJounralDlg.FileName);

                for (int i = 0; i < table.Rows.Count; i++)
                {
                    this._sysJournalGrid.Rows.Add(new[]
                        {
                            table.Rows[i].ItemArray[0],
                            table.Rows[i].ItemArray[1],
                            table.Rows[i].ItemArray[2],
                            table.Rows[i].ItemArray[3]
                        });
                }
            }
        }
        
        //Преобразование численного значения в строку
        private static string JournalVolumeToString(int messageNumber, int messageVolume)
        {
            if ((messageNumber < 0x2) | (messageNumber == 0x3))
                return string.Empty;
            if ((messageNumber >= 0x21) & (messageNumber <= 0x27) || (messageNumber >= 0x28))
            {
                if (messageVolume == 25)
                {
                    messageVolume = 0;
                    return messageVolume.ToString(CultureInfo.InvariantCulture); 
                }
                else
                {
                    return messageVolume.ToString(CultureInfo.InvariantCulture);
                }
            }
            if (messageNumber == 0x5)
            {
                switch (messageVolume)
                {
                    case 1:
                        return CRC_STATE;
                    case 2:
                        return DISCRET_ERROR;
                    case 3:
                        return CLOCK_ERROR;
                    default:
                        return string.Empty;
                }
            }
            if ((messageNumber == 0x2) | (messageNumber == 0x4))
            {
                switch (messageVolume)
                {
                    case 1:
                        return FROM_REMOTE;
                    case 2:
                        return SDTU;
                    default:
                        return string.Empty;
                }
            }
            if ((messageNumber >= 0x7) & (messageNumber <= 0x1b))
                return string.Format(MDO_PATTERN, messageVolume);
            //Вывод сообщения с номером Мдо для Уров
            if ((messageNumber == 0x1E))
            {
                return string.Format(MDO_PATTERN, messageVolume);
            }
            if ((messageNumber == 0x1c) | (messageNumber == 0x1d) | (messageNumber == 0x6))
                return messageVolume.ToString(CultureInfo.InvariantCulture);
            return string.Empty;
        }

        private void TezSystemJournalForm_Activated(object sender, EventArgs e)
        {

        }
        
        #region Обработчики событий
        private void TEZ_SystemJournal_Load(object sender, EventArgs e)
        {
            this._configProgressBar.Step = 1;
            this.LoadConfigurationBlocks();
        }

        private void _readSysJournalBut_Click(object sender, EventArgs e)
        {
            this.LoadConfigurationBlocks();
        }

        private void _serializeSysJournalBut_Click(object sender, EventArgs e)
        {
            this.SaveJournalToFile();
        }

        private void _deserializeSysJournalBut_Click(object sender, EventArgs e)
        {
            this.LoadJournalFromFile();
        }

        #endregion Обработчики событий

        #region IFormView members
        public Type FormDevice
        {
            get { return typeof(Tez); }
        }

        public bool Multishow { get; private set; }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public Type ClassType
        {
            get { return typeof(TezSystemJournalForm); }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.js; }
        }

        public string NodeName
        {
            get { return "Журнал системы"; }
        }
        #endregion IFormView members

        private void _readAllBut_Click(object sender, EventArgs e)
        {
            this.LoadConfigurationBlocks();
        }
    }
}
