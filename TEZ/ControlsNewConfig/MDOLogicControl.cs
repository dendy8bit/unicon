﻿using System;
using System.Collections;
using System.Collections.Generic;
using BEMN.MBServer;
using System.Windows.Forms;
using BEMN.TEZ.SupportClasses;

namespace BEMN.TEZ.ControlsNewConfig
{
    public partial class MDOLogicControl : UserControl
    {
        private const int SIGNALS_COUNT = 4;
        private ushort[] _signals = new ushort[SIGNALS_COUNT];
        private bool vn1;
        private bool vn2;
        private bool vn3;
        private bool input3;
        private bool offSv;
        private bool offSvv;
        private bool srabDoc;
        private bool doc1;
        private bool doc2;
        private bool doc3;
        bool value;
        List<bool> massiveValues = new List<bool>();

        public MDOLogicControl()
        {
            this.InitializeComponent();

            if (Strings.Version2 >= 6)
            {
                this.dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
                this.dataGridView.Size = new System.Drawing.Size(222, 244);
            }

            foreach (var signal in Strings.MDOLogicSignalView)
            {
                this.dataGridView.Rows.Add(signal, false, false, false, false);
            }
        }
        
        /// <summary>
        /// Логические сигналы
        /// </summary>

        public ushort[] SignalsMDO
        {
            get
            {
                this.GetSignals();
                return this._signals;
            }
            set
            {
                this._signals = value;
                this.SetSignals();
            }
        }

        private void GetSignals()
        {
            this._signals = new ushort[SIGNALS_COUNT];

            try
            {
                for (int i = 0; i < SIGNALS_COUNT; i++)
                {
                    if (Strings.Version2 >= 6)
                    {
                        massiveValues.Clear();

                        input3 = (bool)dataGridView[i + 1, 4].Value;
                        vn1 = (bool)dataGridView[i + 1, 5].Value;
                        vn2 = (bool)dataGridView[i + 1, 6].Value;
                        vn3 = (bool)dataGridView[i + 1, 7].Value;
                        offSv = (bool)dataGridView[i + 1, 8].Value;
                        offSvv = (bool)dataGridView[i + 1, 9].Value;
                        doc1 = (bool)dataGridView[i + 1, 10].Value;
                        doc2 = (bool)dataGridView[i + 1, 11].Value;
                        doc3 = (bool)dataGridView[i + 1, 12].Value;

                        for (int j = 0; j < this.dataGridView.Rows.Count; j++)
                        {
                            value = Convert.ToBoolean(dataGridView[i + 1, j].Value);
                            massiveValues.Add(value);
                        }

                        massiveValues[4] = vn1;
                        massiveValues[5] = vn2;
                        massiveValues[6] = offSv;
                        massiveValues[7] = offSvv;
                        massiveValues[8] = doc1;
                        massiveValues[9] = doc2;
                        massiveValues[10] = doc3;
                        massiveValues[11] = input3;
                        massiveValues[12] = vn3;

                    }

                    for (int j = 0; j < this.dataGridView.Rows.Count; j++)  //Strings.MDOLogicSignal.Count
                    {

                        if (Strings.Version2 >= 6)
                        {
                            value = massiveValues[j];
                            _signals[i] = Common.SetBit(_signals[i], j, value);
                        }
                        else
                        {
                            value = Convert.ToBoolean(dataGridView[i + 1, j].Value);
                            _signals[i] = Common.SetBit(_signals[i], j, value);
                        }
                    }

                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        private void SetSignals()
        {
            try
            {
                for (int i = 0; i < SIGNALS_COUNT; i++)
                {
                    var a = new BitArray(new int[] { this._signals[i] });
                    for (int j = 0; j < this.dataGridView.Rows.Count; j++) //Strings.MDOLogicSignal.Count
                    {
                        this.dataGridView[i + 1, j].Value = a[j];
                    }

                    if (Strings.Version2 >= 6)
                    {
                        vn1 = (bool)dataGridView[i + 1, 4].Value;
                        vn2 = (bool)dataGridView[i + 1, 5].Value;
                        offSv = (bool)dataGridView[i + 1, 6].Value;
                        offSvv = (bool)dataGridView[i + 1, 7].Value;
                        doc1 = (bool)dataGridView[i + 1, 8].Value;
                        doc2 = (bool)dataGridView[i + 1, 9].Value;
                        doc3 = (bool)dataGridView[i + 1, 10].Value;
                        input3 = (bool)dataGridView[i + 1, 11].Value;
                        vn3 = (bool)dataGridView[i + 1, 12].Value;

                        dataGridView[i + 1, 4].Value = input3;
                        dataGridView[i + 1, 5].Value = vn1;
                        dataGridView[i + 1, 6].Value = vn2;
                        dataGridView[i + 1, 7].Value = vn3;
                        dataGridView[i + 1, 8].Value = offSv;
                        dataGridView[i + 1, 9].Value = offSvv;
                        dataGridView[i + 1, 10].Value = doc1;
                        dataGridView[i + 1, 11].Value = doc2;
                        dataGridView[i + 1, 12].Value = doc3;
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
    }
}
