﻿namespace BEMN.TEZ.ControlsNewConfig
{
    partial class MDOLogicControl
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this._nameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._vls1Column = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._vls2Column = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._vls3Column = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._vls4Column = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView
            // 
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.AllowUserToDeleteRows = false;
            this.dataGridView.AllowUserToResizeColumns = false;
            this.dataGridView.AllowUserToResizeRows = false;
            this.dataGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._nameColumn,
            this._vls1Column,
            this._vls2Column,
            this._vls3Column,
            this._vls4Column});
            this.dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView.GridColor = System.Drawing.SystemColors.ControlDarkDark;
            this.dataGridView.Location = new System.Drawing.Point(0, 0);
            this.dataGridView.MultiSelect = false;
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.RowHeadersVisible = false;
            this.dataGridView.Size = new System.Drawing.Size(266, 204);
            this.dataGridView.TabIndex = 5;
            // 
            // _nameColumn
            // 
            this._nameColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._nameColumn.Frozen = true;
            this._nameColumn.HeaderText = "Сигнал";
            this._nameColumn.Name = "_nameColumn";
            this._nameColumn.ReadOnly = true;
            this._nameColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._nameColumn.Width = 49;
            // 
            // _vls1Column
            // 
            this._vls1Column.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this._vls1Column.Frozen = true;
            this._vls1Column.HeaderText = "ВЛС 1";
            this._vls1Column.Name = "_vls1Column";
            this._vls1Column.Width = 44;
            // 
            // _vls2Column
            // 
            this._vls2Column.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this._vls2Column.Frozen = true;
            this._vls2Column.HeaderText = "ВЛС 2";
            this._vls2Column.Name = "_vls2Column";
            this._vls2Column.Width = 44;
            // 
            // _vls3Column
            // 
            this._vls3Column.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this._vls3Column.Frozen = true;
            this._vls3Column.HeaderText = "ВЛС 3";
            this._vls3Column.Name = "_vls3Column";
            this._vls3Column.Width = 44;
            // 
            // _vls4Column
            // 
            this._vls4Column.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this._vls4Column.Frozen = true;
            this._vls4Column.HeaderText = "ВЛС 4";
            this._vls4Column.Name = "_vls4Column";
            this._vls4Column.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._vls4Column.Width = 44;
            // 
            // MDOLogicControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dataGridView);
            this.Name = "MDOLogicControl";
            this.Size = new System.Drawing.Size(266, 204);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn _nameColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _vls1Column;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _vls2Column;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _vls3Column;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _vls4Column;
    }
}
