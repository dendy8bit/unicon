﻿using System.Collections.Generic;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.MBServer;

namespace BEMN.TEZ.TezStatus.Structures
{
    public struct StatusMdoStruct : IStruct, IStructInit
    {
        public ushort DevAddr_DevType;
        public ushort Channel1_Channel2;
        public ushort Channel3_Alarm;


        #region Реле МДО
        public bool[] MdoReles
        {
            get
            {
                var result = new[]
                    {
                        Common.GetBit(this.Channel3_Alarm, 10),
                        Common.GetBit(this.Channel3_Alarm, 11),
                        Common.GetBit(this.Channel3_Alarm, 12),
                        Common.GetBit(this.Channel3_Alarm, 13),
                        !Common.GetBit(this.Channel3_Alarm, 15)
                    };
                return result;
            }

        }
        #endregion Реле МДО

        #region Датчики
        public bool D1Z
        {
            get
            {
                return Common.GetBit(this.Channel1_Channel2, 1);
            }
            set
            {
                this.Channel1_Channel2 = Common.SetBit(this.Channel1_Channel2, 1, value);
            }
        }

        public bool D1E
        {
            get
            {
                return Common.GetBit(this.Channel1_Channel2, 4);
            }
            set
            {
                this.Channel1_Channel2 = Common.SetBit(this.Channel1_Channel2, 4, value);
            }
        }

        public bool D1D
        {
            get
            {
                return Common.GetBit(this.Channel1_Channel2, 2);
            }
            set
            {
                this.Channel1_Channel2 = Common.SetBit(this.Channel1_Channel2, 2, value);
            }
        }

        public bool D1N
        {
            get
            {
                return Common.GetBit(this.Channel1_Channel2, 3);
            }
            set
            {
                this.Channel1_Channel2 = Common.SetBit(this.Channel1_Channel2, 3, value);
            }
        }

        public bool D2E
        {
            get
            {
                return Common.GetBit(this.Channel1_Channel2, 12);
            }
            set
            {
                this.Channel1_Channel2 = Common.SetBit(this.Channel1_Channel2, 12, value);
            }
        }

        public bool D2Z
        {
            get
            {
                return Common.GetBit(this.Channel1_Channel2, 9);
            }
            set
            {
                this.Channel1_Channel2 = Common.SetBit(this.Channel1_Channel2, 9, value);
            }
        }

        public bool D2D
        {
            get
            {
                return Common.GetBit(this.Channel1_Channel2, 10);
            }
            set
            {
                this.Channel1_Channel2 = Common.SetBit(this.Channel1_Channel2, 10, value);
            }
        }

        public bool D2N
        {
            get
            {
                return Common.GetBit(this.Channel1_Channel2, 11);
            }
            set
            {
                this.Channel1_Channel2 = Common.SetBit(this.Channel1_Channel2, 11, value);
            }
        }

        public bool D3E
        {
            get
            {
                return Common.GetBit(this.Channel3_Alarm, 4);
            }
            set
            {
                this.Channel3_Alarm = Common.SetBit(this.Channel3_Alarm, 4, value);
            }
        }

        public bool D3Z
        {
            get
            {
                return Common.GetBit(this.Channel3_Alarm, 1);
            }
            set
            {
                this.Channel3_Alarm = Common.SetBit(this.Channel3_Alarm, 1, value);
            }
        }

        public bool D3D
        {
            get
            {
                return Common.GetBit(this.Channel3_Alarm, 2);
            }
            set
            {
                this.Channel3_Alarm = Common.SetBit(this.Channel3_Alarm, 2, value);
            }
        }

        public bool D3N
        {
            get
            {
                return Common.GetBit(this.Channel3_Alarm, 3);
            }
            set
            {
                this.Channel3_Alarm = Common.SetBit(this.Channel3_Alarm, 3, value);
            }
        }
        #endregion

        public bool[][] AllSensors
        {
            get
            {
                return new[]
                    {
                        new[] {this.D1E,this.D1D, this.D1Z, this.D1N},
                        new[] {this.D2E,this.D2D,this.D2Z,this.D2N},
                        new[] {this.D3E,this.D3D,this.D3Z,this.D3N}
                    };
            }

        }

        public ushort DeviceTypeState
        {
            get { return (ushort) (Common.GetBits(this.DevAddr_DevType, 8, 9, 10, 11, 12, 13, 14, 15) >> 8); }
            set { this.DevAddr_DevType = Common.SetBits(this.DevAddr_DevType, value, 8, 9, 10, 11, 12, 13, 14, 15); }
        }

        #region Конструктор
        public StatusMdoStruct(bool a)
        {
            this.DevAddr_DevType = 0;
            this.Channel1_Channel2 = 0;
            this.Channel3_Alarm = 0;
        }
        #endregion

        public StructInfo GetStructInfo(int len)
        {
            return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool slotArray, int slotLength)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType());
        }

        public void InitStruct(byte[] array)
        {
            int index = 0;
            this.DevAddr_DevType = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.Channel1_Channel2 = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.Channel3_Alarm = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.Add(this.DevAddr_DevType);
            result.Add(this.Channel1_Channel2);
            result.Add(this.Channel3_Alarm);
            return result.ToArray();
        }
    }
}