﻿using System.Collections.Generic;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.MBServer;

namespace BEMN.TEZ.TezStatus.Structures
{
    public struct DateTimeStruct : IStruct, IStructInit
    {
        #region поля
        public ushort years;
        public ushort month;
        public ushort date;
        public ushort day;
        public ushort hour;
        public ushort min;
        public ushort sec;
        public ushort msec;
        #endregion
        #region Свойства



        public ushort Date
        {
            get
            {
                return this.date;
            }
            set
            {
                this.date = value;
            }
        }
        //добавил день
        //public ushort Day
        //{
        //    get
        //    {
        //        return _dateAndTime.day;
        //    }
        //    set
        //    {
        //        _dateAndTime.day = value;
        //    }
        //}

        public ushort Month
        {
            get
            {
                return this.month;
            }
            set
            {
                this.month = value;
            }
        }

        public ushort Year
        {
            get
            {
                return this.years;
            }
            set
            {
                this.years = value;
            }
        }

        public ushort Hour
        {
            get
            {
                return this.hour;
            }
            set
            {
                this.hour = value;
            }
        }

        public ushort Minutes
        {
            get
            {
                return this.min;
            }
            set
            {
                this.min = value;
            }
        }

        public ushort Seconds
        {
            get
            {
                return this.sec;
            }
            set
            {
                this.sec = value;
            }
        }

        public ushort Milliseconds
        {
            get
            {
                return this.msec;
            }
            set
            {
                this.msec = value;
            }
        }
        #endregion

        public StructInfo GetStructInfo(int len)
        {
            return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool slotArray, int slotLength)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType());
        }

        public void InitStruct(byte[] array)
        {
            int index = 0;
            this.years = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.month = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.date = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.day = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.hour = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.min = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.sec = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.msec = Common.TOWORD(array[index + 1], array[index]);
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.Add(this.years);
            result.Add(this.month);
            result.Add(this.date);
            result.Add(this.day);
            result.Add(this.hour);
            result.Add(this.min);
            result.Add(this.sec);
            result.Add(this.msec);
            return result.ToArray();
        }
    }
}