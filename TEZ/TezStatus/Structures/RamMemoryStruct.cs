﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.MBServer;

namespace BEMN.TEZ.TezStatus.Structures
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct RamMemoryStruct : IStruct, IStructInit
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 24)]
        public StatusMdoStruct[] Status_Mdo;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
        public ushort[] Diskrets;
        public ushort Relay;

        public ushort Command;

        public ushort ErrorsFirstWord;
        public ushort ErrorsSecondWord;
        public ushort ReleAlarm;
        //Добавил метод



        #region Квинтирование
        public ushort CommandKvint
        {
            get
            {
                return this.Command;
            }
            set
            {
                this.Command = value;
            }
        }
        #endregion Квинтирование

        #region [Состояние дискретов]
        public byte[] DiscretStatus
        {
            get
            {

                return Common.TOBYTES(this.Diskrets, true);
            }
            /* set
             {
                 _mdoObj.Channel1_Channel2 = Common.SetBit(_mdoObj.Channel1_Channel2, 0, value);
             }*/
        }
        #endregion Состояние дискретов

        #region [Неисправности]

        public bool[] Faults
        {
            get
            {
                return new bool[]
                    {
                       Common.GetBit(this.ReleAlarm,0),
                       Common.GetBit(this.ReleAlarm,1),
                       Common.GetBit(this.ReleAlarm,2)
                    };

            }
        }

        #endregion [Неисправности]

        #region Состояние реле
        public ushort ReleStatus
        {
            get
            {
                return this.Relay;
            }
            /* set
             {
                 _mdoObj.Channel1_Channel2 = Common.SetBit(_mdoObj.Channel1_Channel2, 0, value);
             }*/
        }
        #endregion Состояние реле

        #region Конструктор
        public RamMemoryStruct(bool a)
        {
            this.Status_Mdo = new StatusMdoStruct[24];
            this.Diskrets = new ushort[6];
            this.Relay = 0;
            this.Command = 0;
            this.ErrorsFirstWord = 0;
            this.ErrorsSecondWord = 0;
            this.ReleAlarm = 0;
        }
        #endregion

        public StructInfo GetStructInfo(int len)
        {
            return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool slotArray, int slotLength)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType());
        }

        public void InitStruct(byte[] array)
        {
            this.Status_Mdo = new StatusMdoStruct[24];
            this.Diskrets = new ushort[6];
            int index = 0;

            byte[] oneStruct;
            for (int i = 0; i < this.Status_Mdo.Length; i++)
            {
                oneStruct = new byte[System.Runtime.InteropServices.Marshal.SizeOf(typeof(StatusMdoStruct))];
                Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
                this.Status_Mdo[i].InitStruct(oneStruct);
                index += System.Runtime.InteropServices.Marshal.SizeOf(typeof(StatusMdoStruct));
            }
            for (int i = 0; i < this.Diskrets.Length; i++)
            {
                this.Diskrets[i] = Common.TOWORD(array[index + 1], array[index]);
                index += sizeof(ushort);
            }
            this.Relay = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);
            this.Command = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);
            this.ErrorsFirstWord = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);
            this.ErrorsSecondWord = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);
            this.ReleAlarm = Common.TOWORD(array[index + 1], array[index]);
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            for (int j = 0; j < this.Status_Mdo.Length; j++)
            {
                result.AddRange(this.Status_Mdo[j].GetValues());
            }
            result.AddRange(this.Diskrets);
            result.Add(this.Relay);
            result.Add(this.Command);
            result.Add(this.ErrorsFirstWord);
            result.Add(this.ErrorsSecondWord);
            result.Add(this.ReleAlarm);
            return result.ToArray();
        }
    }
}