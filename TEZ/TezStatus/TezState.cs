﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using AssemblyResources;
using BEMN.Forms;
using BEMN.TEZ.Controls;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.TEZ.Configuration.Structures;
using BEMN.TEZ.SupportClasses;
using BEMN.TEZ.TezStatus.Structures;
using DateTimeStruct = BEMN.TEZ.TezStatus.Structures.DateTimeStruct;


namespace BEMN.TEZ.TezStatus
{
    public partial class TezStateForm : Form, IFormView
    {
        #region [Constants]
        //общее количество МДО
        private const int MDO_COUNTS = 24;
        //Количество дискрет ТЭЗа
        private const int TEZ_DISCRET_COUNT = 6;
        private const string UNCORRECT_DATE_TIME = "Некорректное значение даты или времени.";
        private const string IMPOSSIBLE_READ_CONFIGURATION = "Невозможно прочитать конфигурацию";
        private const string KVINT_OK = "Квитирование проведено успешно";
        private const string KVINT_FAIL = "Невозможно провести квинтирование";
        #endregion [Constants]


        #region Поля и константы
        private readonly Tez _device;
        private readonly MdoStatusControl[] _mdoStatus = new MdoStatusControl[MDO_COUNTS];
        //флаг изменения времени вручную
        private bool _timeChange;
        //флаг сообщения об ошибке
        private bool _errorMessage;
        private LedControl[] _tezDicrets;
        private LedControl[] _faultLeds;
        private Label[] _tezDicretsType;
        private MemoryEntity<OneWordStruct> _stopTime;
        private MemoryEntity<RamMemoryStruct> _ramMemory;
        private MemoryEntity<DateTimeStruct> _dateTime;
        #endregion Поля и константы


        #region Конструкторы и инициализация
        public TezStateForm()
        {
            this.InitializeComponent();
        }

        public TezStateForm(Tez device)
        {
            this.InitializeComponent();
            this._device = device;
            this._device.ConnectionModeChanged += this.StartStopLoad;
            this._ramMemory = device.RamMemory;
            this._ramMemory.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.StructRead);
            this._ramMemory.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.AllReadFail);
            
            this._device.CommandKvit.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                if (this._device.CommandKvit.Value.Command == 0)
                {
                    this._device.CommandKvit.RemoveStructQueries();
                    MessageBox.Show(KVINT_OK);
                    this.kvint.Enabled = true;
                }
            });
            this._device.CommandKvit.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                MessageBox.Show(KVINT_FAIL);
                this.kvint.Enabled = true;
            });
            this._device.CommandKvit.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this,
                () => this._device.CommandKvit.LoadStructCycle());
            this._device.CommandKvit.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                MessageBox.Show(KVINT_FAIL);
                this.kvint.Enabled = true;
            });

            this._dateTime = device.DateAndTime;
            this._dateTime.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ShowTime);
            this._dateTime.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.DateTimeReadFail);
            this.MdoInit();
            this.TezInit();
            this._stopTime = device.StopTime;

        }
        //инициализация панели статуса МДО
        private void MdoInit()
        {
            for (int i = 0; i < MDO_COUNTS; i++)
            {
                this._mdoStatus[i] = new MdoStatusControl(i + 1) { Location = new Point(0, 50 + 20 * i) };
                this._mdoPanel.Controls.Add(this._mdoStatus[i]);
            }
        }
        //Инициализация дискретов ТЭЗ
        private void TezInit()
        {
            this._faultLeds = new LedControl[]
                {
                    this._fault1Led, this._fault2Led, this._fault3Led
                };
            this._tezDicrets = new[]
                {
                    this._d1_led, this._d2_led, this._d3_led, this._d4_led, this._d5_led, this._d6_led
                };
            this._tezDicretsType = new[]
                {
                    this._discretTypeLabel1, this._discretTypeLabel2, this._discretTypeLabel3, this._discretTypeLabel4, this._discretTypeLabel5, this._discretTypeLabel6
                };
        }
        #endregion Конструкторы и инициализация


        #region IFormView Members
        public Type FormDevice
        {
            get { return typeof(Tez); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(TezStateForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.measuring.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Состояния"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }
        #endregion


        #region Дополнительные функции
        //Чтение структур из устройства
        private void StartStopLoad()
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._ramMemory.LoadStructCycle();
                this._dateTime.LoadStructCycle();
            }
            else
            {
                this._ramMemory.RemoveStructQueries();
                this._dateTime.RemoveStructQueries();
                this.AllReadFail();
                this.DateTimeReadFail();
            }
        }
        //Вывод состояний МДО на экран
        private void ShowMdoCollection()
        {
            try
            {
                for (int i = 0; i < MDO_COUNTS; i++)
                {
                    StatusMdoStruct statusMdo = this._ramMemory.Value.Status_Mdo[i];
                    if (statusMdo.DevAddr_DevType != 0)
                    {

                        this._mdoStatus[i].SetMDOStatus((i + 1).ToString(CultureInfo.InvariantCulture),
                            this._ramMemory.Value.Status_Mdo[i].DeviceTypeState, statusMdo.AllSensors,
                            this._ramMemory.Value.Status_Mdo[i].MdoReles);
                    }
                    else
                    {
                        this._mdoStatus[i].Clear();
                    }
                }
            }
            catch (ArgumentOutOfRangeException)
            {
                // на случай, если подключен не ТЭЗ
            }
        }
        //выводит статус реле
        private void ShowReleStatus()
        {
            for (int i = 0; i <= 2; i++)
            {
                this._faultLeds[i].SetState(this._ramMemory.Value.Faults[i]);
            }

            this._rn_led.SetState(Common.GetBit(this._ramMemory.Value.ReleStatus, 0));
            this._r1_led.SetState(Common.GetBit(this._ramMemory.Value.ReleStatus, 1));
            this._r2_led.SetState(Common.GetBit(this._ramMemory.Value.ReleStatus, 2));
            this._r3_led.SetState(Common.GetBit(this._ramMemory.Value.ReleStatus, 3));
        }
        //Выводит дискреты
        private void ShowDiscret()
        {
            byte[] discretStatus = this._ramMemory.Value.DiscretStatus;
            for (int i = 0; i < TEZ_DISCRET_COUNT; i++)
            {
                try
                {
                    this._tezDicrets[i].SetState(discretStatus[i * 2] == 1);
                    this._tezDicretsType[i].Text = Strings.DiscretType[discretStatus[i * 2 + 1]];
                }
                catch (Exception e)
                {
                    this._tezDicretsType[i].Text = "#######";
                    this._tezDicretsType[i].ForeColor = Color.Red;
                }
            }
        }

        //вывод времени
        private void ShowTime()
        {
            if (this._timeChange) return;
            this._dateClockTB.TextMaskFormat = MaskFormat.IncludeLiterals;
            this._dateClockTB.Text = this._dateTime.Value.Date.ToString("00") +
                                     this._dateTime.Value.Month.ToString("00") +
                                     this._dateTime.Value.Year.ToString("0000");
            this._timeClockTB.TextMaskFormat = MaskFormat.IncludeLiterals;
            this._timeClockTB.Text = this._dateTime.Value.Hour.ToString("00") +
                                     this._dateTime.Value.Minutes.ToString("00") +
                                     this._dateTime.Value.Seconds.ToString("00") +
                                     this._dateTime.Value.Milliseconds.ToString("000");
        }
        //Очистка статуса
        public void AllReadFail()
        {
            this._rn_led.State = LedState.Off;
            this._r1_led.State = LedState.Off;
            this._r2_led.State = LedState.Off;
            this._r3_led.State = LedState.Off;
            for (int i = 0; i < TEZ_DISCRET_COUNT; i++)
            {
                this._tezDicrets[i].State = LedState.Off;
                this._tezDicretsType[i].Text = string.Empty;
            }
            foreach (MdoStatusControl status in this._mdoStatus)
            {
                status.Clear();
            }
            if (!this._errorMessage)
            {
                MessageBox.Show(IMPOSSIBLE_READ_CONFIGURATION);
                this._errorMessage = true;
            }
        }

        private void DateTimeReadFail()
        {
            this._dateClockTB.Text = string.Empty;
            this._timeClockTB.Text = string.Empty;
        }
        #endregion


        #region Запись
        //Системные дата-время(запись в устройство)
        private void WriteNowTime()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            DateTimeStruct dt = new DateTimeStruct();
            dt.Date = Convert.ToUInt16(DateTime.Now.Day);
            dt.Month = Convert.ToUInt16(DateTime.Now.Month);
            dt.Year = Convert.ToUInt16(DateTime.Now.Year);
            dt.Hour = Convert.ToUInt16(DateTime.Now.Hour);
            dt.Minutes = Convert.ToUInt16(DateTime.Now.Minute);
            dt.Seconds = Convert.ToUInt16(DateTime.Now.Second);
            dt.Milliseconds = Convert.ToUInt16(DateTime.Now.Millisecond);
            this._dateTime.Value = dt;
            this._dateTime.SaveStruct();
        }
        //Запись Даты и времени в устройство
        private void WriteDateTime()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            try
            {
                List<string> dateTime = new List<string>();
                dateTime.AddRange(this._dateClockTB.Text.Split('.'));
                dateTime.AddRange(this._timeClockTB.Text.Split(':', ','));

                ushort date = Convert.ToUInt16(dateTime[0]);
                ushort month = Convert.ToUInt16(dateTime[1]);
                ushort year = Convert.ToUInt16(dateTime[2]);
                ushort hour = Convert.ToUInt16(dateTime[3]);
                ushort minutes = Convert.ToUInt16(dateTime[4]);
                ushort seconds = Convert.ToUInt16(dateTime[5]);
                ushort mseconds = Convert.ToUInt16(dateTime[6]);

                if (date <= 31 & month <= 12 & year > 2000 & year < 2100 & hour < 24 & minutes < 60 & seconds < 60 &
                    mseconds < 1000)
                {
                    DateTimeStruct dt = new DateTimeStruct();
                    dt.Date = date;
                    dt.Month = month;
                    dt.Year = year;
                    dt.Hour = hour;
                    dt.Minutes = minutes;
                    dt.Seconds = seconds;
                    dt.Milliseconds = mseconds;
                    this._dateTime.Value = dt;
                    this._dateTime.SaveStruct();
                    this._stopCB.Checked = true;
                }
                else
                {
                    MessageBox.Show(UNCORRECT_DATE_TIME);
                }
            }
            catch
            {
                MessageBox.Show(UNCORRECT_DATE_TIME);
            }
        }
        #endregion Запись


        #region Обработчики событий
        //Изменить время
        private void _stopCB_CheckedChanged(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (this._stopCB.Checked)
            {
                this._dateTime.RemoveStructQueries();
            }
            else
            {
                this._dateTime.LoadStructCycle();
            }
            this._writeDateTimeButt.Enabled = this._timeChange = this._stopCB.Checked;
            this._dateClockTB.ReadOnly = this._timeClockTB.ReadOnly = !this._stopCB.Checked;
        }

        private void _dateTimeNowButt_Click(object sender, EventArgs e)
        {
            this.WriteNowTime();
        }

        private void MDO_Configuration_Load(object sender, EventArgs e)
        {
            this.StartStopLoad();
        }

        #region [MemoryEntity Events Handlers]
        /// <summary>
        /// Конфигурация успешно прочитана
        /// </summary>
        private void StructRead()
        {
            this.ShowMdoCollection(); //выводит статус МДО на экран                    
            this.ShowReleStatus(); //вывод статуса реле
            this.ShowDiscret(); //вывод дискрет
        }
        #endregion [MemoryEntity Events Handlers]

        private void kvint_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._device.CommandKvit.Value = new CommandStruct {Command = 1};
            this._device.CommandKvit.SaveStruct();
            this.kvint.Enabled = false;
        }

        private void _writeDateTimeButt_Click(object sender, EventArgs e)
        {
            this.WriteDateTime();
        }

        private void TEZ_State_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._ramMemory.RemoveStructQueries();
            this._dateTime.RemoveStructQueries();
            this._device.ConnectionModeChanged -= this.StartStopLoad;
        }
        #endregion Обработчики событий

        private void _stopTimeButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._stopTime.Value = new OneWordStruct { Word = 1 };
            this._stopTime.SaveStruct();
        }
    }
}
