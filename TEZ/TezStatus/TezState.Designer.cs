﻿using AssemblyResources;

namespace BEMN.TEZ.TezStatus
{
    partial class TezStateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.label34 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this._timeClockTB = new System.Windows.Forms.MaskedTextBox();
            this._dateClockTB = new System.Windows.Forms.MaskedTextBox();
            this._dateTimeBox = new System.Windows.Forms.GroupBox();
            this._stopTimeButton = new System.Windows.Forms.Button();
            this._writeDateTimeButt = new System.Windows.Forms.Button();
            this._dateTimeNowButt = new System.Windows.Forms.Button();
            this._stopCB = new System.Windows.Forms.CheckBox();
            this.kvint = new System.Windows.Forms.Button();
            this._mdoPanel = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this._discretBox = new System.Windows.Forms.GroupBox();
            this._discretTypeLabel6 = new System.Windows.Forms.Label();
            this._discretTypeLabel5 = new System.Windows.Forms.Label();
            this._discretTypeLabel4 = new System.Windows.Forms.Label();
            this._discretTypeLabel3 = new System.Windows.Forms.Label();
            this._discretTypeLabel2 = new System.Windows.Forms.Label();
            this._discretTypeLabel1 = new System.Windows.Forms.Label();
            this._d6_led = new BEMN.Forms.LedControl();
            this._d5_led = new BEMN.Forms.LedControl();
            this._d4_led = new BEMN.Forms.LedControl();
            this._d3_led = new BEMN.Forms.LedControl();
            this._d2_led = new BEMN.Forms.LedControl();
            this._d1_led = new BEMN.Forms.LedControl();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this._releBox = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this._rn_led = new BEMN.Forms.LedControl();
            this._r3_led = new BEMN.Forms.LedControl();
            this.label4 = new System.Windows.Forms.Label();
            this._r2_led = new BEMN.Forms.LedControl();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this._r1_led = new BEMN.Forms.LedControl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this._fault2Led = new BEMN.Forms.LedControl();
            this._fault3Led = new BEMN.Forms.LedControl();
            this._fault1Led = new BEMN.Forms.LedControl();
            this._dateTimeBox.SuspendLayout();
            this._mdoPanel.SuspendLayout();
            this._discretBox.SuspendLayout();
            this._releBox.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _toolTip
            // 
            this._toolTip.ShowAlways = true;
            this._toolTip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Warning;
            // 
            // label34
            // 
            this.label34.BackColor = System.Drawing.Color.Transparent;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label34.ForeColor = System.Drawing.Color.Transparent;
            this.label34.Location = new System.Drawing.Point(450, 16);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(21, 20);
            this.label34.TabIndex = 56;
            this._toolTip.SetToolTip(this.label34, "Неисправность МДО");
            // 
            // label17
            // 
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label17.ForeColor = System.Drawing.Color.Transparent;
            this.label17.Location = new System.Drawing.Point(326, 30);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(18, 16);
            this.label17.TabIndex = 50;
            this._toolTip.SetToolTip(this.label17, "Неисправность  датчик 3");
            // 
            // label16
            // 
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label16.ForeColor = System.Drawing.Color.Transparent;
            this.label16.Location = new System.Drawing.Point(256, 30);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(18, 16);
            this.label16.TabIndex = 49;
            this._toolTip.SetToolTip(this.label16, "Неисправность датчик 2");
            // 
            // label15
            // 
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label15.ForeColor = System.Drawing.Color.Transparent;
            this.label15.Location = new System.Drawing.Point(186, 30);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(18, 16);
            this.label15.TabIndex = 48;
            this._toolTip.SetToolTip(this.label15, "Неисправность  датчик 1");
            // 
            // label14
            // 
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label14.ForeColor = System.Drawing.Color.Transparent;
            this.label14.Location = new System.Drawing.Point(308, 30);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(17, 16);
            this.label14.TabIndex = 47;
            this._toolTip.SetToolTip(this.label14, "Засветка датчик 3");
            // 
            // label13
            // 
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label13.ForeColor = System.Drawing.Color.Transparent;
            this.label13.Location = new System.Drawing.Point(238, 30);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(17, 16);
            this.label13.TabIndex = 46;
            this._toolTip.SetToolTip(this.label13, "Засветка датчик 2");
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label12.ForeColor = System.Drawing.Color.Transparent;
            this.label12.Location = new System.Drawing.Point(168, 30);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(17, 16);
            this.label12.TabIndex = 45;
            this._toolTip.SetToolTip(this.label12, "Засветка  датчик 1");
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.ForeColor = System.Drawing.Color.Transparent;
            this.label11.Location = new System.Drawing.Point(289, 30);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(17, 16);
            this.label11.TabIndex = 44;
            this._toolTip.SetToolTip(this.label11, "Дуга датчик 3");
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.ForeColor = System.Drawing.Color.Transparent;
            this.label10.Location = new System.Drawing.Point(219, 30);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(17, 16);
            this.label10.TabIndex = 43;
            this._toolTip.SetToolTip(this.label10, "Дуга  датчик 2");
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.ForeColor = System.Drawing.Color.Transparent;
            this.label9.Location = new System.Drawing.Point(149, 30);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(17, 16);
            this.label9.TabIndex = 42;
            this._toolTip.SetToolTip(this.label9, "Дуга датчик 1");
            // 
            // label33
            // 
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label33.ForeColor = System.Drawing.Color.Transparent;
            this.label33.Location = new System.Drawing.Point(418, 30);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(15, 16);
            this.label33.TabIndex = 55;
            this._toolTip.SetToolTip(this.label33, "Реле 4");
            // 
            // label32
            // 
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label32.ForeColor = System.Drawing.Color.Transparent;
            this.label32.Location = new System.Drawing.Point(399, 30);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(15, 16);
            this.label32.TabIndex = 54;
            this._toolTip.SetToolTip(this.label32, "Реле 3");
            // 
            // label31
            // 
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label31.ForeColor = System.Drawing.Color.Transparent;
            this.label31.Location = new System.Drawing.Point(380, 30);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(15, 16);
            this.label31.TabIndex = 53;
            this._toolTip.SetToolTip(this.label31, "Реле 2");
            // 
            // label30
            // 
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label30.ForeColor = System.Drawing.Color.Transparent;
            this.label30.Location = new System.Drawing.Point(361, 30);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(15, 16);
            this.label30.TabIndex = 52;
            this._toolTip.SetToolTip(this.label30, "Реле 1");
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(149, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 20);
            this.label1.TabIndex = 32;
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(95, 17);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(40, 13);
            this.label20.TabIndex = 40;
            this.label20.Text = "Время";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(15, 17);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(33, 13);
            this.label21.TabIndex = 39;
            this.label21.Text = "Дата";
            // 
            // _timeClockTB
            // 
            this._timeClockTB.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
            this._timeClockTB.Location = new System.Drawing.Point(98, 35);
            this._timeClockTB.Mask = "90:00:00.00";
            this._timeClockTB.Name = "_timeClockTB";
            this._timeClockTB.ReadOnly = true;
            this._timeClockTB.Size = new System.Drawing.Size(83, 20);
            this._timeClockTB.TabIndex = 38;
            this._timeClockTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _dateClockTB
            // 
            this._dateClockTB.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
            this._dateClockTB.Location = new System.Drawing.Point(13, 35);
            this._dateClockTB.Mask = "00/00/0000";
            this._dateClockTB.Name = "_dateClockTB";
            this._dateClockTB.ReadOnly = true;
            this._dateClockTB.Size = new System.Drawing.Size(79, 20);
            this._dateClockTB.TabIndex = 37;
            this._dateClockTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _dateTimeBox
            // 
            this._dateTimeBox.Controls.Add(this._stopTimeButton);
            this._dateTimeBox.Controls.Add(this._writeDateTimeButt);
            this._dateTimeBox.Controls.Add(this._dateTimeNowButt);
            this._dateTimeBox.Controls.Add(this._stopCB);
            this._dateTimeBox.Controls.Add(this._timeClockTB);
            this._dateTimeBox.Controls.Add(this.label20);
            this._dateTimeBox.Controls.Add(this._dateClockTB);
            this._dateTimeBox.Controls.Add(this.label21);
            this._dateTimeBox.Location = new System.Drawing.Point(545, 12);
            this._dateTimeBox.Name = "_dateTimeBox";
            this._dateTimeBox.Size = new System.Drawing.Size(195, 162);
            this._dateTimeBox.TabIndex = 41;
            this._dateTimeBox.TabStop = false;
            this._dateTimeBox.Text = "Дата-Время";
            // 
            // _stopTimeButton
            // 
            this._stopTimeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._stopTimeButton.Location = new System.Drawing.Point(21, 132);
            this._stopTimeButton.Name = "_stopTimeButton";
            this._stopTimeButton.Size = new System.Drawing.Size(156, 24);
            this._stopTimeButton.TabIndex = 44;
            this._stopTimeButton.Text = "Остановить часы";
            this._stopTimeButton.UseVisualStyleBackColor = true;
            this._stopTimeButton.Click += new System.EventHandler(this._stopTimeButton_Click);
            // 
            // _writeDateTimeButt
            // 
            this._writeDateTimeButt.Enabled = false;
            this._writeDateTimeButt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._writeDateTimeButt.Location = new System.Drawing.Point(21, 107);
            this._writeDateTimeButt.Name = "_writeDateTimeButt";
            this._writeDateTimeButt.Size = new System.Drawing.Size(156, 23);
            this._writeDateTimeButt.TabIndex = 43;
            this._writeDateTimeButt.Text = "Установить";
            this._writeDateTimeButt.UseVisualStyleBackColor = true;
            this._writeDateTimeButt.Click += new System.EventHandler(this._writeDateTimeButt_Click);
            // 
            // _dateTimeNowButt
            // 
            this._dateTimeNowButt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._dateTimeNowButt.Location = new System.Drawing.Point(21, 82);
            this._dateTimeNowButt.Name = "_dateTimeNowButt";
            this._dateTimeNowButt.Size = new System.Drawing.Size(156, 23);
            this._dateTimeNowButt.TabIndex = 42;
            this._dateTimeNowButt.Text = "Системные дата и время";
            this._dateTimeNowButt.UseVisualStyleBackColor = true;
            this._dateTimeNowButt.Click += new System.EventHandler(this._dateTimeNowButt_Click);
            // 
            // _stopCB
            // 
            this._stopCB.AutoSize = true;
            this._stopCB.Location = new System.Drawing.Point(29, 60);
            this._stopCB.Name = "_stopCB";
            this._stopCB.Size = new System.Drawing.Size(112, 17);
            this._stopCB.TabIndex = 41;
            this._stopCB.Text = "Изменить время";
            this._stopCB.UseVisualStyleBackColor = true;
            this._stopCB.CheckedChanged += new System.EventHandler(this._stopCB_CheckedChanged);
            // 
            // kvint
            // 
            this.kvint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.kvint.Location = new System.Drawing.Point(545, 549);
            this.kvint.Name = "kvint";
            this.kvint.Size = new System.Drawing.Size(195, 23);
            this.kvint.TabIndex = 48;
            this.kvint.Text = "Сквитировать";
            this.kvint.UseVisualStyleBackColor = true;
            this.kvint.Click += new System.EventHandler(this.kvint_Click);
            // 
            // _mdoPanel
            // 
            this._mdoPanel.BackColor = System.Drawing.Color.White;
            this._mdoPanel.BackgroundImage = Resources.BackGround;
            this._mdoPanel.Controls.Add(this.label7);
            this._mdoPanel.Controls.Add(this.label34);
            this._mdoPanel.Controls.Add(this.label33);
            this._mdoPanel.Controls.Add(this.label32);
            this._mdoPanel.Controls.Add(this.label31);
            this._mdoPanel.Controls.Add(this.label30);
            this._mdoPanel.Controls.Add(this.label29);
            this._mdoPanel.Controls.Add(this.label17);
            this._mdoPanel.Controls.Add(this.label16);
            this._mdoPanel.Controls.Add(this.label15);
            this._mdoPanel.Controls.Add(this.label14);
            this._mdoPanel.Controls.Add(this.label13);
            this._mdoPanel.Controls.Add(this.label12);
            this._mdoPanel.Controls.Add(this.label11);
            this._mdoPanel.Controls.Add(this.label10);
            this._mdoPanel.Controls.Add(this.label9);
            this._mdoPanel.Controls.Add(this.label3);
            this._mdoPanel.Controls.Add(this.label2);
            this._mdoPanel.Controls.Add(this.label6);
            this._mdoPanel.Controls.Add(this.label5);
            this._mdoPanel.Controls.Add(this.label1);
            this._mdoPanel.Location = new System.Drawing.Point(12, 12);
            this._mdoPanel.Name = "_mdoPanel";
            this._mdoPanel.Size = new System.Drawing.Size(527, 531);
            this._mdoPanel.TabIndex = 45;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.ForeColor = System.Drawing.Color.Transparent;
            this.label7.Location = new System.Drawing.Point(480, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(44, 20);
            this.label7.TabIndex = 57;
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label29
            // 
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.ForeColor = System.Drawing.Color.Transparent;
            this.label29.Location = new System.Drawing.Point(361, 4);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(72, 20);
            this.label29.TabIndex = 51;
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.ForeColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(289, 4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 20);
            this.label3.TabIndex = 38;
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.ForeColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(219, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 20);
            this.label2.TabIndex = 37;
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.ForeColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(53, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 20);
            this.label6.TabIndex = 36;
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.ForeColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(3, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 20);
            this.label5.TabIndex = 35;
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _discretBox
            // 
            this._discretBox.Controls.Add(this._discretTypeLabel6);
            this._discretBox.Controls.Add(this._discretTypeLabel5);
            this._discretBox.Controls.Add(this._discretTypeLabel4);
            this._discretBox.Controls.Add(this._discretTypeLabel3);
            this._discretBox.Controls.Add(this._discretTypeLabel2);
            this._discretBox.Controls.Add(this._discretTypeLabel1);
            this._discretBox.Controls.Add(this._d6_led);
            this._discretBox.Controls.Add(this._d5_led);
            this._discretBox.Controls.Add(this._d4_led);
            this._discretBox.Controls.Add(this._d3_led);
            this._discretBox.Controls.Add(this._d2_led);
            this._discretBox.Controls.Add(this._d1_led);
            this._discretBox.Controls.Add(this.label25);
            this._discretBox.Controls.Add(this.label24);
            this._discretBox.Controls.Add(this.label23);
            this._discretBox.Controls.Add(this.label22);
            this._discretBox.Controls.Add(this.label19);
            this._discretBox.Controls.Add(this.label18);
            this._discretBox.Location = new System.Drawing.Point(545, 180);
            this._discretBox.Name = "_discretBox";
            this._discretBox.Size = new System.Drawing.Size(195, 155);
            this._discretBox.TabIndex = 46;
            this._discretBox.TabStop = false;
            this._discretBox.Text = "Состояние дискретов";
            // 
            // _discretTypeLabel6
            // 
            this._discretTypeLabel6.AutoSize = true;
            this._discretTypeLabel6.Location = new System.Drawing.Point(54, 128);
            this._discretTypeLabel6.Name = "_discretTypeLabel6";
            this._discretTypeLabel6.Size = new System.Drawing.Size(0, 13);
            this._discretTypeLabel6.TabIndex = 17;
            // 
            // _discretTypeLabel5
            // 
            this._discretTypeLabel5.AutoSize = true;
            this._discretTypeLabel5.Location = new System.Drawing.Point(54, 108);
            this._discretTypeLabel5.Name = "_discretTypeLabel5";
            this._discretTypeLabel5.Size = new System.Drawing.Size(0, 13);
            this._discretTypeLabel5.TabIndex = 16;
            // 
            // _discretTypeLabel4
            // 
            this._discretTypeLabel4.AutoSize = true;
            this._discretTypeLabel4.Location = new System.Drawing.Point(54, 88);
            this._discretTypeLabel4.Name = "_discretTypeLabel4";
            this._discretTypeLabel4.Size = new System.Drawing.Size(0, 13);
            this._discretTypeLabel4.TabIndex = 15;
            // 
            // _discretTypeLabel3
            // 
            this._discretTypeLabel3.AutoSize = true;
            this._discretTypeLabel3.Location = new System.Drawing.Point(54, 68);
            this._discretTypeLabel3.Name = "_discretTypeLabel3";
            this._discretTypeLabel3.Size = new System.Drawing.Size(0, 13);
            this._discretTypeLabel3.TabIndex = 14;
            // 
            // _discretTypeLabel2
            // 
            this._discretTypeLabel2.AutoSize = true;
            this._discretTypeLabel2.Location = new System.Drawing.Point(54, 48);
            this._discretTypeLabel2.Name = "_discretTypeLabel2";
            this._discretTypeLabel2.Size = new System.Drawing.Size(0, 13);
            this._discretTypeLabel2.TabIndex = 13;
            // 
            // _discretTypeLabel1
            // 
            this._discretTypeLabel1.AutoSize = true;
            this._discretTypeLabel1.Location = new System.Drawing.Point(54, 28);
            this._discretTypeLabel1.Name = "_discretTypeLabel1";
            this._discretTypeLabel1.Size = new System.Drawing.Size(0, 13);
            this._discretTypeLabel1.TabIndex = 12;
            // 
            // _d6_led
            // 
            this._d6_led.Location = new System.Drawing.Point(33, 128);
            this._d6_led.Name = "_d6_led";
            this._d6_led.Size = new System.Drawing.Size(13, 13);
            this._d6_led.State = BEMN.Forms.LedState.Off;
            this._d6_led.TabIndex = 11;
            // 
            // _d5_led
            // 
            this._d5_led.Location = new System.Drawing.Point(33, 108);
            this._d5_led.Name = "_d5_led";
            this._d5_led.Size = new System.Drawing.Size(13, 13);
            this._d5_led.State = BEMN.Forms.LedState.Off;
            this._d5_led.TabIndex = 10;
            // 
            // _d4_led
            // 
            this._d4_led.Location = new System.Drawing.Point(33, 88);
            this._d4_led.Name = "_d4_led";
            this._d4_led.Size = new System.Drawing.Size(13, 13);
            this._d4_led.State = BEMN.Forms.LedState.Off;
            this._d4_led.TabIndex = 9;
            // 
            // _d3_led
            // 
            this._d3_led.Location = new System.Drawing.Point(33, 68);
            this._d3_led.Name = "_d3_led";
            this._d3_led.Size = new System.Drawing.Size(13, 13);
            this._d3_led.State = BEMN.Forms.LedState.Off;
            this._d3_led.TabIndex = 8;
            // 
            // _d2_led
            // 
            this._d2_led.Location = new System.Drawing.Point(33, 48);
            this._d2_led.Name = "_d2_led";
            this._d2_led.Size = new System.Drawing.Size(13, 13);
            this._d2_led.State = BEMN.Forms.LedState.Off;
            this._d2_led.TabIndex = 7;
            // 
            // _d1_led
            // 
            this._d1_led.Location = new System.Drawing.Point(33, 28);
            this._d1_led.Name = "_d1_led";
            this._d1_led.Size = new System.Drawing.Size(13, 13);
            this._d1_led.State = BEMN.Forms.LedState.Off;
            this._d1_led.TabIndex = 6;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(14, 88);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(13, 13);
            this.label25.TabIndex = 5;
            this.label25.Text = "4";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(14, 108);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(13, 13);
            this.label24.TabIndex = 4;
            this.label24.Text = "5";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(14, 128);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(13, 13);
            this.label23.TabIndex = 3;
            this.label23.Text = "6";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(14, 68);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(13, 13);
            this.label22.TabIndex = 2;
            this.label22.Text = "3";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(14, 48);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(13, 13);
            this.label19.TabIndex = 1;
            this.label19.Text = "2";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(14, 28);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(13, 13);
            this.label18.TabIndex = 0;
            this.label18.Text = "1";
            // 
            // _releBox
            // 
            this._releBox.Controls.Add(this.label8);
            this._releBox.Controls.Add(this._rn_led);
            this._releBox.Controls.Add(this._r3_led);
            this._releBox.Controls.Add(this.label4);
            this._releBox.Controls.Add(this._r2_led);
            this._releBox.Controls.Add(this.label27);
            this._releBox.Controls.Add(this.label26);
            this._releBox.Controls.Add(this._r1_led);
            this._releBox.Location = new System.Drawing.Point(545, 341);
            this._releBox.Name = "_releBox";
            this._releBox.Size = new System.Drawing.Size(195, 110);
            this._releBox.TabIndex = 47;
            this._releBox.TabStop = false;
            this._releBox.Text = "Состояние реле";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 85);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(20, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Рн";
            // 
            // _rn_led
            // 
            this._rn_led.Location = new System.Drawing.Point(33, 85);
            this._rn_led.Name = "_rn_led";
            this._rn_led.Size = new System.Drawing.Size(13, 13);
            this._rn_led.State = BEMN.Forms.LedState.Off;
            this._rn_led.TabIndex = 5;
            // 
            // _r3_led
            // 
            this._r3_led.Location = new System.Drawing.Point(33, 65);
            this._r3_led.Name = "_r3_led";
            this._r3_led.Size = new System.Drawing.Size(13, 13);
            this._r3_led.State = BEMN.Forms.LedState.Off;
            this._r3_led.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 65);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(20, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Р3";
            // 
            // _r2_led
            // 
            this._r2_led.Location = new System.Drawing.Point(33, 45);
            this._r2_led.Name = "_r2_led";
            this._r2_led.Size = new System.Drawing.Size(13, 13);
            this._r2_led.State = BEMN.Forms.LedState.Off;
            this._r2_led.TabIndex = 4;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(7, 45);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(20, 13);
            this.label27.TabIndex = 2;
            this.label27.Text = "Р2";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(7, 25);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(20, 13);
            this.label26.TabIndex = 1;
            this.label26.Text = "Р1";
            // 
            // _r1_led
            // 
            this._r1_led.Location = new System.Drawing.Point(33, 25);
            this._r1_led.Name = "_r1_led";
            this._r1_led.Size = new System.Drawing.Size(13, 13);
            this._r1_led.State = BEMN.Forms.LedState.Off;
            this._r1_led.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label28);
            this.groupBox1.Controls.Add(this.label35);
            this.groupBox1.Controls.Add(this.label36);
            this.groupBox1.Controls.Add(this._fault2Led);
            this.groupBox1.Controls.Add(this._fault3Led);
            this.groupBox1.Controls.Add(this._fault1Led);
            this.groupBox1.Location = new System.Drawing.Point(550, 457);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(183, 86);
            this.groupBox1.TabIndex = 49;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Неисправности";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(49, 48);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(75, 13);
            this.label28.TabIndex = 9;
            this.label28.Text = "связи с МДО";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(49, 67);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(33, 13);
            this.label35.TabIndex = 8;
            this.label35.Text = "МДО";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(49, 29);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(43, 13);
            this.label36.TabIndex = 7;
            this.label36.Text = "ТЭЗ-24";
            // 
            // _fault2Led
            // 
            this._fault2Led.Location = new System.Drawing.Point(28, 48);
            this._fault2Led.Name = "_fault2Led";
            this._fault2Led.Size = new System.Drawing.Size(13, 13);
            this._fault2Led.State = BEMN.Forms.LedState.Off;
            this._fault2Led.TabIndex = 2;
            // 
            // _fault3Led
            // 
            this._fault3Led.Location = new System.Drawing.Point(28, 67);
            this._fault3Led.Name = "_fault3Led";
            this._fault3Led.Size = new System.Drawing.Size(13, 13);
            this._fault3Led.State = BEMN.Forms.LedState.Off;
            this._fault3Led.TabIndex = 1;
            // 
            // _fault1Led
            // 
            this._fault1Led.Location = new System.Drawing.Point(28, 29);
            this._fault1Led.Name = "_fault1Led";
            this._fault1Led.Size = new System.Drawing.Size(13, 13);
            this._fault1Led.State = BEMN.Forms.LedState.Off;
            this._fault1Led.TabIndex = 0;
            // 
            // TezStateForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(750, 574);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.kvint);
            this.Controls.Add(this._releBox);
            this.Controls.Add(this._discretBox);
            this.Controls.Add(this._mdoPanel);
            this.Controls.Add(this._dateTimeBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "TezStateForm";
            this.Text = " ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TEZ_State_FormClosing);
            this.Load += new System.EventHandler(this.MDO_Configuration_Load);
            this._dateTimeBox.ResumeLayout(false);
            this._dateTimeBox.PerformLayout();
            this._mdoPanel.ResumeLayout(false);
            this._discretBox.ResumeLayout(false);
            this._discretBox.PerformLayout();
            this._releBox.ResumeLayout(false);
            this._releBox.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolTip _toolTip;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.MaskedTextBox _timeClockTB;
        private System.Windows.Forms.MaskedTextBox _dateClockTB;
        private System.Windows.Forms.GroupBox _dateTimeBox;
        private System.Windows.Forms.Panel _mdoPanel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox _discretBox;
        private BEMN.Forms.LedControl _d3_led;
        private BEMN.Forms.LedControl _d2_led;
        private BEMN.Forms.LedControl _d1_led;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.GroupBox _releBox;
        private BEMN.Forms.LedControl _r1_led;
        private BEMN.Forms.LedControl _d6_led;
        private BEMN.Forms.LedControl _d5_led;
        private BEMN.Forms.LedControl _d4_led;
        private BEMN.Forms.LedControl _rn_led;
        private BEMN.Forms.LedControl _r2_led;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Button _writeDateTimeButt;
        private System.Windows.Forms.Button _dateTimeNowButt;
        private System.Windows.Forms.CheckBox _stopCB;
        private System.Windows.Forms.Button kvint;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label _discretTypeLabel6;
        private System.Windows.Forms.Label _discretTypeLabel5;
        private System.Windows.Forms.Label _discretTypeLabel4;
        private System.Windows.Forms.Label _discretTypeLabel3;
        private System.Windows.Forms.Label _discretTypeLabel2;
        private System.Windows.Forms.Label _discretTypeLabel1;
        private BEMN.Forms.LedControl _r3_led;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox1;
        private BEMN.Forms.LedControl _fault2Led;
        private BEMN.Forms.LedControl _fault3Led;
        private BEMN.Forms.LedControl _fault1Led;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Button _stopTimeButton;
    }
}