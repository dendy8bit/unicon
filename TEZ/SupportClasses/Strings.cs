﻿using System.Collections.Generic;
using System.Linq;

namespace BEMN.TEZ.SupportClasses
{
    class Strings
    {
        public static int Version1 { get; set; }
        public static int Version2 { get; set; }
        public static int Version3 { get; set; }

        public static int DiskretCount
        {
            get
            {
                if (Version2 >= 6)
                {
                    return 10;
                }

                return 6;
            }
        }

        public static List<string> ReleType
        {
            get
            {
                return new List<string>
                    {
                        "Повторитель",
                        "Блинкер"
                    };
            }
        }

        public static List<string> ReleMdoSignalNewVersionView
        {
            get
            {
                List<string> retOld = new List<string>
                {
                    "нет",
                    "откл. ОЛ без I",
                    "откл. ОЛ с I",
                    "откл. Ввод 1",
                    "откл. Ввод 2",
                    "откл. ВН 1",
                    "откл. ВН 2",
                    "откл. СВ",
                    "откл. СВв",
                    "",
                    "",
                    "ВЛС1",
                    "ВЛС2",
                    "ВЛС3",
                    "ВЛС4"
                };

                List<string> retNew = new List<string>
                {
                    "нет",
                    "откл. ОЛ без I",
                    "откл. ОЛ с I",
                    "откл. Ввод 1",
                    "откл. Ввод 2",
                    "откл. Ввод 3",
                    "откл. ВН 1",
                    "откл. ВН 2",
                    "откл. ВН 3",
                    "откл. СВ",
                    "откл. СВв",
                    "ВЛС1",
                    "ВЛС2",
                    "ВЛС3",
                    "ВЛС4"
                };

                if (Version1 >= 1 && Version2 >= 6)
                {
                    return retNew;
                }

                return retOld;
            }
        }

        /// <summary>
        /// Сигналы реле МДО Новые
        /// </summary>
        public static List<string> ReleMdoSignalNewVersion
        {
            get
            {
                List<string> ret = new List<string>
                    {
                        "нет",
                        "откл. ОЛ без I",
                        "откл. ОЛ с I",
                        "откл. Ввод 1",
                        "откл. Ввод 2",
                        "откл. ВН 1",
                        "откл. ВН 2",
                        "откл. СВ",
                        "откл. СВв",
                        "",
                        "",
                        "ВЛС1",
                        "ВЛС2",
                        "ВЛС3",
                        "ВЛС4"
                    };

                if (Version1 >= 1 && Version2 >= 6)
                {
                    ret[9] = "откл. Ввод 3";
                    ret[10] = "откл. ВН 3";
                }

                return ret;
            }
        }

        public static List<string> ReleMdoSignalOldVersion
        {
            get
            {
                return new List<string>
                {
                    "нет",
                    "откл. ОЛ без I",
                    "откл. ОЛ с I",
                    "откл. Ввод 1",
                    "откл. Ввод 2",
                    "откл. ВН 1",
                    "откл. ВН 2",
                    "откл. СВ",
                    "откл. СВв"
                };
            }
        }

        public static List<string> ReleTezSignalView
        {
            get
            {
                List<string> retOld = new List<string>
                {
                    "нет",//0
                    "откл. ОЛ без I",//1
                    "откл. ОЛ с I",//2
                    "откл. Ввод 1",//3
                    "откл. Ввод 2",//4
                    "откл. ВН 1",//5
                    "откл. ВН 2",//6
                    "откл. СВ",//7
                    "откл. СВв",//8
                    "сраб. ДОК",//9
                    "УРОВ",//10
                    "ВЛС 1",//11
                    "ВЛС 2",//12
                    "ВЛС 3"//13
                };

                List<string> retNew = new List<string>
                {
                    "нет",//0
                    "откл. ОЛ без I",//1
                    "откл. ОЛ с I",//2
                    "откл. Ввод 1",//3
                    "откл. Ввод 2",//4
                    "откл. Ввод 3",//4
                    "откл. ВН 1",//5
                    "откл. ВН 2",//6
                    "откл. ВН 3",//6
                    "откл. СВ",//7
                    "откл. СВв",//8
                    "сраб. ДОК",//9
                    "УРОВ",//10
                    "ВЛС 1",//11
                    "ВЛС 2",//12
                    "ВЛС 3"//13
                };
                if (Version1 >= 1 && Version2 >= 6)
                {
                    return retNew;
                }

                return retOld;
            }
        }

        /// <summary>
        /// Сигналы реле ТЭЗ
        /// </summary>
        public static List<string> ReleTezSignal
        {
            get
            {
                List<string> ret = new List<string>
                    {
                        "нет",//0
                        "откл. ОЛ без I",//1
                        "откл. ОЛ с I",//2
                        "откл. Ввод 1",//3
                        "откл. Ввод 2",//4
                        "откл. ВН 1",//5
                        "откл. ВН 2",//6
                        "откл. СВ",//7
                        "откл. СВв",//8
                        "сраб. ДОК",//9
                        "УРОВ",//10
                        "ВЛС 1",//11
                        "ВЛС 2",//12
                        "ВЛС 3"//13
                    };
                if (Version1 >= 1 && Version2 >= 6)
                {
                    ret.Add("откл. Ввод 3");
                    ret.Add("откл. ВН 3");
                }

                return ret;
            }
        }

        public static List<string> ReleLogicSignalView
        {
            get
            {
                List<string> retOld = new List<string>
                {
                    "откл. ОЛ без I",
                    "откл. ОЛ с I",
                    "откл. Ввод 1",
                    "откл. Ввод 2",
                    "откл. ВН 1",
                    "откл. ВН 2",
                    "откл. СВ",
                    "откл. СВв",
                    "сраб. ДОК",
                    "УРОВ"
                };
                List<string> retNew = new List<string>
                {
                    "откл. ОЛ без I",
                    "откл. ОЛ с I",
                    "откл. Ввод 1",
                    "откл. Ввод 2",
                    "откл. Ввод 3",
                    "откл. ВН 1",
                    "откл. ВН 2",
                    "откл. ВН 3",
                    "откл. СВ",
                    "откл. СВв",
                    "сраб. ДОК",
                    "УРОВ"
                };
                if (Version2 >= 6)
                {
                    return retNew;
                }
                return retOld;
            }
        }
        /// <summary>
        /// Внешние логические сигналы реле ТЕЗ
        /// </summary>
        public static List<string> ReleLogicSignal
        {
            get
            {
                List<string> ret = new List<string>
                    {
                        "откл. ОЛ без I",
                        "откл. ОЛ с I",
                        "откл. Ввод 1",
                        "откл. Ввод 2",
                        "откл. ВН 1",
                        "откл. ВН 2",
                        "откл. СВ",
                        "откл. СВв",
                        "сраб. ДОК",
                        "УРОВ"
                    };
                if (Version2 >= 6)
                {
                    ret.Add("откл. Ввод 3");
                    ret.Add("откл. ВН 3");
                }
                return ret;
            }
        }

        public static List<string> DeviceType
        {
            get
            {
                return new List<string>
                    {
                        "Нет",
                        "ОЛ",
                        "Ввод 1",
                        "Ввод 2",
                        "СВ",
                        "ТН",
                        "ТСН",
                       
                    };
            }
        }

        public static List<string> DeviceTypeState
        {
            get
            {
                List<string> ret = new List<string>
                    {
                        "Нет",  //0
                        "ОЛ",//1
                        "Ввод 1",//2
                        "Ввод 2",//3
                        "СВ",//4
                        "ТН",//5
                        "ТСН",//6
                        "СР", //7
                        "Ошибка CAN" //8
                    };
                if (Version2 >= 6)
                {
                    ret[6] = "ТСН1";

                    ret.Add("Ввод 3");
                    ret.Add("ТСН2");
                    ret.Add("ТСН3");
                }
                return ret;
            }
        }

        public static List<string> DiscretTypeView
        {
            get
            {
                List<string> retOld = new List<string>
                {
                    "Нет",                  //0
                    "Контроль I ввод 1",    //1
                    "Контроль I ввод 2",    //2
                    "Контроль I ВН 1",      //3
                    "Контроль I ВН 2",      //4
                    "Контроль I СВ",        //5
                    "Контроль I СВв",       //6
                    "Блокировка"            //7
                };
                List<string> retNew = new List<string>
                {
                    "Нет",                  //0
                    "Контроль I ввод 1",    //1
                    "Контроль I ввод 2",    //2
                    "Контроль I ввод 3",
                    "Контроль I ВН 1",      //3
                    "Контроль I ВН 2",      //4
                    "Контроль I ВН 3",
                    "Контроль I СВ",        //5
                    "Контроль I СВв",       //6
                    "Блокировка"            //7
                };
                
                if (Version2 >= 6)
                {
                    return retNew;
                }

                return retOld;
            }
        }

        public static List<string> DiscretType
        {
            get
            {
                List<string> ret = new List<string>
                    {
                        "Нет",                  //0
                        "Контроль I ввод 1",    //1
                        "Контроль I ввод 2",    //2
                        "Контроль I ВН 1",      //3
                        "Контроль I ВН 2",      //4
                        "Контроль I СВ",        //5
                        "Контроль I СВв",       //6
                        "Блокировка"            //7
                    };
                if (Version1 >= 1 && Version2 >= 6)
                {
                    ret.Add("Контроль I ввод 3");
                    ret.Add("Контроль I ВН 3");
                }

                return ret;
            }
        }
        
        public static List<string> JMessage
        {
            get
            {
                List<string> ret = new List<string>
                {
                    "Питание включено", //0   
                    "Питание отключено", //1
                    "Конфигурация изменена", //2
                    "Сброс журнала", //3
                    "Квитирование", //4
                    "Неисправность", //5
                    "Дискрет", //6
                    "Датчик 1: засветка", //7
                    "Датчик 1: дуга", //8
                    "Датчик 1: неисправен", //9
                    "Датчик 2: засветка", //10
                    "Датчик 2: дуга", //11
                    "Датчик 2: неисправен", //12
                    "Датчик 3: засветка", //13
                    "Датчик 3: дуга", //14
                    "Датчик 3: неисправен", //15
                    "Команда на реле", //16
                    "Ошибка теста HL", //17
                    "Реле 1 замкнуто", //18
                    "Реле 1 разомкнуто", //19
                    "Реле 2 замкнуто", //20
                    "Реле 2 разомкнуто", //21
                    "Реле 3 замкнуто", //22
                    "Реле 3 разомкнуто", //23
                    "Реле 4 замкнуто", //24
                    "Реле 4 разомкнуто", //25
                    "Ошибка связи CAN", //26
                    "Восстановлена связь CAN", //27
                    "Реле замкнуто", //28
                    "Реле разомкнуто", //29
                    "УРОВ", //30
                    "", //31
                    "", //32
                    "Контроль I ввод 1", //33
                    "Контроль I ввод 2", //34
                    "Контроль I ВН 1", //35
                    "Контроль I ВН 2", //36
                    "Контроль I СВ", //37
                    "Контроль I СВв", //38
                    "Блокировка" //39
                };
                if (Version1 >= 1 && Version2 >= 4)
                {
                    ret[7] = ret[7].Replace("засветка", "исправен");
                    ret[10] = ret[10].Replace("засветка", "исправен");
                    ret[13] = ret[13].Replace("засветка", "исправен");
                }

                if (Version1 >= 1 && Version2 >= 6)
                {
                    ret.Add("Контроль I ввод 3");
                    ret.Add("Контроль I ВН 3");
                }

                return ret;
            }
        }

        public static List<string> MDOLogicSignalView
        {
            get
            {
                List<string> ret14 = new List<string>
                {
                    "откл. ОЛ без I",
                    "откл. ОЛ с I",
                    "откл. Ввод 1",
                    "откл. Ввод 2",
                    "откл. ВН 1",
                    "откл. ВН 2",
                    "откл. СВ",
                    "откл. СВв"
                };
                List<string> ret15 = new List<string>
                {
                    "откл. ОЛ без I",
                    "откл. ОЛ с I",
                    "откл. Ввод 1",
                    "откл. Ввод 2",
                    "откл. ВН 1",
                    "откл. ВН 2",
                    "откл. СВ",
                    "откл. СВв",
                    "сраб. ДОК1",
                    "сраб. ДОК2",
                    "сраб. ДОК3",
                };

                List<string> ret16 = new List<string>
                {
                    "откл. ОЛ без I",
                    "откл. ОЛ с I",
                    "откл. Ввод 1",
                    "откл. Ввод 2",
                    "откл. Ввод 3",
                    "откл. ВН 1",
                    "откл. ВН 2",
                    "откл. ВН 3",
                    "откл. СВ",
                    "откл. СВв",
                    "сраб. ДОК1",
                    "сраб. ДОК2",
                    "сраб. ДОК3"
                };
                if (Version1 >= 1 && Version2 == 6)
                {
                    return ret16;
                }

                if (Version1 >= 1 && Version2 == 5)
                {
                    return ret15;
                }

                return ret14;
            }
        }

        /// <summary>
        /// Внешние логические сигналы для МДО
        /// </summary>
        public static List<string> MDOLogicSignal
        {
            get
            {
                List<string> ret = new List<string>
                {
                    "откл. ОЛ без I",
                    "откл. ОЛ с I",
                    "откл. Ввод 1",
                    "откл. Ввод 2",
                    "откл. ВН 1",
                    "откл. ВН 2",
                    "откл. СВ",
                    "откл. СВв"
                };
                if (Version1 >= 1 && Version2 >= 5)
                {
                    ret.Add("сраб. ДОК1");
                    ret.Add("сраб. ДОК2");
                    ret.Add("сраб. ДОК3");
                }

                if (Version1 >= 1 && Version2 >= 6)
                {
                    ret.Add("откл. Ввод 3");
                    ret.Add("откл. ВН 3");
                }
                return ret;
            }
        }
        public static List<string> MdoTypeView
        {
            get
            {
                List<string> retOld = new List<string>
                {
                    "Нет",
                    "ОЛ",
                    "Ввод 1",
                    "Ввод 2",
                    "СВ",
                    "ТН",
                    "ТСН",
                    "СР"
                };

                List<string> retNew = new List<string>
                {
                    "Нет",
                    "ОЛ",
                    "Ввод 1",
                    "Ввод 2",
                    "Ввод 3",
                    "СВ",
                    "ТН",
                    "ТСН1",
                    "ТСН2",
                    "ТСН3",
                    "СР"
                };
                if (Version1 >= 1 && Version2 >= 6)
                {
                    return retNew;
                }
                return retOld;
            }
        }

        public static List<string> MdoType
        {
            get
            {
                List<string> ret = new List<string>
                    {
                        "Нет",
                        "ОЛ",
                        "Ввод 1",
                        "Ввод 2",
                        "СВ",
                        "ТН",
                        "ТСН",
                        "СР"
                    };
                if (Version1 >= 1 && Version2 >= 6)
                {
                    ret[6] = "ТСН1";

                    ret.Add("Ошибка связи");
                    ret.Add("Ввод 3");
                    ret.Add("ТСН2");
                    ret.Add("ТСН3");
                }
                return ret;
            }
        }

    }
}
