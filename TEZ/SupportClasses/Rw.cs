﻿namespace BEMN.TEZ
{
    public enum Rw
    {
        NONE = 0,
        READ = 1,
        WRITE = 2
    }
}