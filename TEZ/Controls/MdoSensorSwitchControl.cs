﻿using System;
using System.Windows.Forms;

namespace BEMN.TEZ.Controls
{
    public partial class MdoSensorSwitchControl : UserControl
    {
        public event EventHandler CheckChanges;

        public MdoSensorSwitchControl()
        {
            InitializeComponent();
        }

        private void _chEnabled_CheckedChanged(object sender, EventArgs e)
        {
            CheckChanges.Invoke(this, new EventArgs());
        }

        public bool[] Sensors
        {
            get
            {
                return new[]
                    {
                        _ch1Enabled.Checked,
                        _ch2Enabled.Checked,
                        _ch3Enabled.Checked
                    };
            }
            set
            {
                _ch1Enabled.Checked = value[0];
                _ch2Enabled.Checked = value[1];
                _ch3Enabled.Checked = value[2];
            }
        }
    }
}
