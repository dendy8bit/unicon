﻿using System;
using System.Collections;
using System.Windows.Forms;
using BEMN.TEZ.SupportClasses;

namespace BEMN.TEZ.Controls
{
    public partial class LogicSignalsControl : UserControl
    {
        private const int SIGNALS_COUNT = 3;
        private ushort[] _signals = new ushort[SIGNALS_COUNT];

        public event EventHandler UpdateData;

        public LogicSignalsControl()
        {
            InitializeComponent();
            //Список логических сигналов
            this._logicSignalsCheckList.DataSource = Strings.ReleLogicSignal;
            this._inputLogicSignalComboBox.SelectedIndex = 0;
        }
        /// <summary>
        /// Номер выбранного ВЛС
        /// </summary>
        public int SelectedIndex
        {
            get { return this._inputLogicSignalComboBox.SelectedIndex; }
        }
        /// <summary>
        /// Логические сигналы
        /// </summary>
        public ushort[] Signals
        {
            get { return _signals; }
            set
            {
                _signals = value;
                this._inputLogicSignalComboBox.SelectedIndex = 0;
                SetSelectedSignals(Signals[SelectedIndex]);
            }
        }


        private void _logicSignalsCheckList_Leave(object sender, EventArgs e)
        {
            this._signals[SelectedIndex] = GetSelectedSignals();
            if (this.UpdateData != null)
            this.UpdateData.Invoke(this,new EventArgs());
        }

        private ushort GetSelectedSignals()
        {
            bool[] result = new bool[_logicSignalsCheckList.Items.Count];
            for (int i = 0; i < _logicSignalsCheckList.Items.Count; i++)
                result[i] = _logicSignalsCheckList.GetItemChecked(i);
            BitArray a = new BitArray(result);
            int[] b = new int[1];
            a.CopyTo(b,0);
            return (ushort) b[0];
        }

        private void SetSelectedSignals(ushort signal)
        {
            BitArray a = new BitArray(new int[] {signal});
            for (int i = 0; i < _logicSignalsCheckList.Items.Count; i++)
                this._logicSignalsCheckList.SetItemChecked(i,a[i]);
        }

        private void _inputLogicSignalComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetSelectedSignals(Signals[SelectedIndex]);
        }
    }
}
