﻿using System;
using System.Collections;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using AssemblyResources;
using BEMN.Forms;
using BEMN.TEZ.SupportClasses;

namespace BEMN.TEZ.Controls
{
    public partial class MdoStatusControl : UserControl
    {
        private const int SENSOR_COUNTS = 3;
        private const int SENSOR_LENGHT = 4;
        private readonly LedControl[][] _sensors = new LedControl[SENSOR_COUNTS][];
        private readonly LedControl[] _relays;

        private bool _focus;


        public MdoStatusControl(int number)
        {
            InitializeComponent();
            this._sensors[0] = new[]
                {
                    this._sensor1_1,
                    this._sensor1_2,
                    this._sensor1_3
                };
            this._sensors[1] = new[]
                {
                    this._sensor2_1,
                    this._sensor2_2,
                    this._sensor2_3
                };
            this._sensors[2] = new[]
                {
                    this._sensor3_1,
                    this._sensor3_2,
                    this._sensor3_3
                };
            this._relays = new[]
                {
                    this._relay1,
                    this._relay2,
                    this._relay3,
                    this._relay4,
                    this._defect
                };

            this._numberLabel.Text = number.ToString(CultureInfo.InvariantCulture);
            this._numberLabelCopy.Text = number.ToString(CultureInfo.InvariantCulture);
            this._typeLabel.Text = Strings.DeviceTypeState[0];
        }

        public void Clear()
        {
            TurnOff();
            this._typeLabel.Text = Strings.DeviceTypeState[0];
            this.BackgroundImage = Resources.MdoStatusControlBackGround;
        }

        private void TurnOff()
        {
            LedManager.TurnOffLeds(this._sensors[0]);
            LedManager.TurnOffLeds(this._sensors[1]);
            LedManager.TurnOffLeds(this._sensors[2]);
            LedManager.TurnOffLeds(this._relays); 
        }

        private void SetSensorStatus(int sensorIndex, bool[] flags)
        {
            var sensorsFlags = new bool[SENSOR_LENGHT - 1];
            Array.Copy(flags, 1, sensorsFlags, 0, MdoStatusControl.SENSOR_LENGHT - 1);
            if (flags[0])
            {

                LedManager.SetLeds(this._sensors[sensorIndex], new BitArray(sensorsFlags));

            }
            else
            {
                LedManager.TurnOffLeds(this._sensors[sensorIndex]);
            }

        }

        public void SetMDOStatus(string number, ushort mdoType, bool[][] flags, bool[] reles)
        {
            this._typeLabel.Text = Strings.DeviceTypeState[mdoType];
            if (mdoType == 8)
            {
                this.BackgroundImage = Resources.MdoStatusControlErrorBackGround;
                TurnOff();
                this._relays[4].State = LedState.NoSignaled;
            }
            else
            {

                this.BackgroundImage = Resources.MdoStatusControlBackGround;
                for (var i = 0; i < MdoStatusControl.SENSOR_COUNTS; i++)
                {
                    SetSensorStatus(i, flags[i]);
                }
                LedManager.SetLeds(this._relays, new BitArray(reles));
            }
            this.Invalidate();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            if (this._focus)
            {
                var greenPen = new Pen(Color.Green,6);
                var blackPen = new Pen(Color.Black);
                var a = new Rectangle(new Point(0,0), this.Size);
                e.Graphics.DrawRectangle(greenPen, a);
                e.Graphics.DrawLine(blackPen, 0, this.Height-1, this.Width, this.Height-1);
            }
            
        }

        private void MdoStatusControl_Enter(object sender, EventArgs e)
        {
            this._focus = true;
            this.Focus();
            this.Invalidate();
        }

        private void MdoStatusControl_Leave(object sender, EventArgs e)
        {
            this._focus = false;
            this.Invalidate();
        }

        private void _defect_LedClicked(object sender, MouseEventArgs e)
        {
            this.Focus();
        }
    }
}
