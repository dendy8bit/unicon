﻿using AssemblyResources;

namespace BEMN.TEZ.Controls
{
    partial class MdoStatusControl
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this._numberLabel = new System.Windows.Forms.Label();
            this._typeLabel = new System.Windows.Forms.Label();
            this._numberLabelCopy = new System.Windows.Forms.Label();
            this._defect = new BEMN.Forms.LedControl();
            this._relay4 = new BEMN.Forms.LedControl();
            this._relay3 = new BEMN.Forms.LedControl();
            this._relay1 = new BEMN.Forms.LedControl();
            this._relay2 = new BEMN.Forms.LedControl();
            this._sensor3_3 = new BEMN.Forms.LedControl();
            this._sensor3_2 = new BEMN.Forms.LedControl();
            this._sensor3_1 = new BEMN.Forms.LedControl();
            this._sensor2_3 = new BEMN.Forms.LedControl();
            this._sensor2_2 = new BEMN.Forms.LedControl();
            this._sensor2_1 = new BEMN.Forms.LedControl();
            this._sensor1_3 = new BEMN.Forms.LedControl();
            this._sensor1_2 = new BEMN.Forms.LedControl();
            this._sensor1_1 = new BEMN.Forms.LedControl();
            this.SuspendLayout();
            // 
            // _numberLabel
            // 
            this._numberLabel.AutoSize = true;
            this._numberLabel.BackColor = System.Drawing.Color.Transparent;
            this._numberLabel.Location = new System.Drawing.Point(15, 3);
            this._numberLabel.Name = "_numberLabel";
            this._numberLabel.Size = new System.Drawing.Size(13, 13);
            this._numberLabel.TabIndex = 0;
            this._numberLabel.Text = "1";
            this._numberLabel.Click += new System.EventHandler(this.MdoStatusControl_Enter);
            this._numberLabel.Enter += new System.EventHandler(this.MdoStatusControl_Enter);
            this._numberLabel.Leave += new System.EventHandler(this.MdoStatusControl_Leave);
            // 
            // _typeLabel
            // 
            this._typeLabel.AutoSize = true;
            this._typeLabel.BackColor = System.Drawing.Color.Transparent;
            this._typeLabel.Location = new System.Drawing.Point(60, 3);
            this._typeLabel.Name = "_typeLabel";
            this._typeLabel.Size = new System.Drawing.Size(69, 13);
            this._typeLabel.TabIndex = 13;
            this._typeLabel.Text = "Отсутствует";
            this._typeLabel.Click += new System.EventHandler(this.MdoStatusControl_Enter);
            this._typeLabel.Enter += new System.EventHandler(this.MdoStatusControl_Enter);
            this._typeLabel.Leave += new System.EventHandler(this.MdoStatusControl_Leave);
            // 
            // _numberLabelCopy
            // 
            this._numberLabelCopy.AutoSize = true;
            this._numberLabelCopy.BackColor = System.Drawing.Color.Transparent;
            this._numberLabelCopy.Location = new System.Drawing.Point(494, 3);
            this._numberLabelCopy.Name = "_numberLabelCopy";
            this._numberLabelCopy.Size = new System.Drawing.Size(13, 13);
            this._numberLabelCopy.TabIndex = 28;
            this._numberLabelCopy.Text = "1";
            this._numberLabelCopy.Click += new System.EventHandler(this.MdoStatusControl_Enter);
            this._numberLabelCopy.Enter += new System.EventHandler(this.MdoStatusControl_Enter);
            this._numberLabelCopy.Leave += new System.EventHandler(this.MdoStatusControl_Leave);
            // 
            // _defect
            // 
            this._defect.Location = new System.Drawing.Point(453, 4);
            this._defect.Name = "_defect";
            this._defect.Size = new System.Drawing.Size(13, 13);
            this._defect.State = BEMN.Forms.LedState.Off;
            this._defect.TabIndex = 27;
            this._defect.LedClicked += new System.EventHandler<System.Windows.Forms.MouseEventArgs>(this._defect_LedClicked);
            this._defect.Leave += new System.EventHandler(this.MdoStatusControl_Leave);
            // 
            // _relay4
            // 
            this._relay4.Location = new System.Drawing.Point(418, 4);
            this._relay4.Name = "_relay4";
            this._relay4.Size = new System.Drawing.Size(13, 13);
            this._relay4.State = BEMN.Forms.LedState.Off;
            this._relay4.TabIndex = 26;
            this._relay4.LedClicked += new System.EventHandler<System.Windows.Forms.MouseEventArgs>(this._defect_LedClicked);
            this._relay4.Leave += new System.EventHandler(this.MdoStatusControl_Leave);
            // 
            // _relay3
            // 
            this._relay3.Location = new System.Drawing.Point(399, 4);
            this._relay3.Name = "_relay3";
            this._relay3.Size = new System.Drawing.Size(13, 13);
            this._relay3.State = BEMN.Forms.LedState.Off;
            this._relay3.TabIndex = 25;
            this._relay3.LedClicked += new System.EventHandler<System.Windows.Forms.MouseEventArgs>(this._defect_LedClicked);
            this._relay3.Leave += new System.EventHandler(this.MdoStatusControl_Leave);
            // 
            // _relay1
            // 
            this._relay1.Location = new System.Drawing.Point(361, 4);
            this._relay1.Name = "_relay1";
            this._relay1.Size = new System.Drawing.Size(13, 13);
            this._relay1.State = BEMN.Forms.LedState.Off;
            this._relay1.TabIndex = 24;
            this._relay1.LedClicked += new System.EventHandler<System.Windows.Forms.MouseEventArgs>(this._defect_LedClicked);
            this._relay1.EnabledChanged += new System.EventHandler(this.MdoStatusControl_Enter);
            this._relay1.Leave += new System.EventHandler(this.MdoStatusControl_Leave);
            // 
            // _relay2
            // 
            this._relay2.Location = new System.Drawing.Point(380, 4);
            this._relay2.Name = "_relay2";
            this._relay2.Size = new System.Drawing.Size(13, 13);
            this._relay2.State = BEMN.Forms.LedState.Off;
            this._relay2.TabIndex = 23;
            this._relay2.LedClicked += new System.EventHandler<System.Windows.Forms.MouseEventArgs>(this._defect_LedClicked);
            this._relay2.Leave += new System.EventHandler(this.MdoStatusControl_Leave);
            // 
            // _sensor3_3
            // 
            this._sensor3_3.Location = new System.Drawing.Point(330, 4);
            this._sensor3_3.Name = "_sensor3_3";
            this._sensor3_3.Size = new System.Drawing.Size(13, 13);
            this._sensor3_3.State = BEMN.Forms.LedState.Off;
            this._sensor3_3.TabIndex = 22;
            this._sensor3_3.LedClicked += new System.EventHandler<System.Windows.Forms.MouseEventArgs>(this._defect_LedClicked);
            this._sensor3_3.Leave += new System.EventHandler(this.MdoStatusControl_Leave);
            // 
            // _sensor3_2
            // 
            this._sensor3_2.Location = new System.Drawing.Point(311, 4);
            this._sensor3_2.Name = "_sensor3_2";
            this._sensor3_2.Size = new System.Drawing.Size(13, 13);
            this._sensor3_2.State = BEMN.Forms.LedState.Off;
            this._sensor3_2.TabIndex = 21;
            this._sensor3_2.LedClicked += new System.EventHandler<System.Windows.Forms.MouseEventArgs>(this._defect_LedClicked);
            this._sensor3_2.Leave += new System.EventHandler(this.MdoStatusControl_Leave);
            // 
            // _sensor3_1
            // 
            this._sensor3_1.Location = new System.Drawing.Point(292, 4);
            this._sensor3_1.Name = "_sensor3_1";
            this._sensor3_1.Size = new System.Drawing.Size(13, 13);
            this._sensor3_1.State = BEMN.Forms.LedState.Off;
            this._sensor3_1.TabIndex = 20;
            this._sensor3_1.LedClicked += new System.EventHandler<System.Windows.Forms.MouseEventArgs>(this._defect_LedClicked);
            this._sensor3_1.Leave += new System.EventHandler(this.MdoStatusControl_Leave);
            // 
            // _sensor2_3
            // 
            this._sensor2_3.Location = new System.Drawing.Point(259, 4);
            this._sensor2_3.Name = "_sensor2_3";
            this._sensor2_3.Size = new System.Drawing.Size(13, 13);
            this._sensor2_3.State = BEMN.Forms.LedState.Off;
            this._sensor2_3.TabIndex = 19;
            this._sensor2_3.LedClicked += new System.EventHandler<System.Windows.Forms.MouseEventArgs>(this._defect_LedClicked);
            this._sensor2_3.Leave += new System.EventHandler(this.MdoStatusControl_Leave);
            // 
            // _sensor2_2
            // 
            this._sensor2_2.Location = new System.Drawing.Point(240, 4);
            this._sensor2_2.Name = "_sensor2_2";
            this._sensor2_2.Size = new System.Drawing.Size(13, 13);
            this._sensor2_2.State = BEMN.Forms.LedState.Off;
            this._sensor2_2.TabIndex = 18;
            this._sensor2_2.LedClicked += new System.EventHandler<System.Windows.Forms.MouseEventArgs>(this._defect_LedClicked);
            this._sensor2_2.Leave += new System.EventHandler(this.MdoStatusControl_Leave);
            // 
            // _sensor2_1
            // 
            this._sensor2_1.Location = new System.Drawing.Point(221, 4);
            this._sensor2_1.Name = "_sensor2_1";
            this._sensor2_1.Size = new System.Drawing.Size(13, 13);
            this._sensor2_1.State = BEMN.Forms.LedState.Off;
            this._sensor2_1.TabIndex = 17;
            this._sensor2_1.LedClicked += new System.EventHandler<System.Windows.Forms.MouseEventArgs>(this._defect_LedClicked);
            this._sensor2_1.Leave += new System.EventHandler(this.MdoStatusControl_Leave);
            // 
            // _sensor1_3
            // 
            this._sensor1_3.Location = new System.Drawing.Point(188, 4);
            this._sensor1_3.Name = "_sensor1_3";
            this._sensor1_3.Size = new System.Drawing.Size(13, 13);
            this._sensor1_3.State = BEMN.Forms.LedState.Off;
            this._sensor1_3.TabIndex = 16;
            this._sensor1_3.LedClicked += new System.EventHandler<System.Windows.Forms.MouseEventArgs>(this._defect_LedClicked);
            this._sensor1_3.Leave += new System.EventHandler(this.MdoStatusControl_Leave);
            // 
            // _sensor1_2
            // 
            this._sensor1_2.Location = new System.Drawing.Point(169, 4);
            this._sensor1_2.Name = "_sensor1_2";
            this._sensor1_2.Size = new System.Drawing.Size(13, 13);
            this._sensor1_2.State = BEMN.Forms.LedState.Off;
            this._sensor1_2.TabIndex = 15;
            this._sensor1_2.LedClicked += new System.EventHandler<System.Windows.Forms.MouseEventArgs>(this._defect_LedClicked);
            this._sensor1_2.Leave += new System.EventHandler(this.MdoStatusControl_Leave);
            // 
            // _sensor1_1
            // 
            this._sensor1_1.Location = new System.Drawing.Point(150, 4);
            this._sensor1_1.Name = "_sensor1_1";
            this._sensor1_1.Size = new System.Drawing.Size(13, 13);
            this._sensor1_1.State = BEMN.Forms.LedState.Off;
            this._sensor1_1.TabIndex = 14;
            this._sensor1_1.LedClicked += new System.EventHandler<System.Windows.Forms.MouseEventArgs>(this._defect_LedClicked);
            this._sensor1_1.Leave += new System.EventHandler(this.MdoStatusControl_Leave);
            // 
            // MdoStatusControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = Resources.MdoStatusControlBackGround;
            this.Controls.Add(this._numberLabelCopy);
            this.Controls.Add(this._defect);
            this.Controls.Add(this._relay4);
            this.Controls.Add(this._relay3);
            this.Controls.Add(this._relay1);
            this.Controls.Add(this._relay2);
            this.Controls.Add(this._sensor3_3);
            this.Controls.Add(this._sensor3_2);
            this.Controls.Add(this._sensor3_1);
            this.Controls.Add(this._sensor2_3);
            this.Controls.Add(this._sensor2_2);
            this.Controls.Add(this._sensor2_1);
            this.Controls.Add(this._sensor1_3);
            this.Controls.Add(this._sensor1_2);
            this.Controls.Add(this._sensor1_1);
            this.Controls.Add(this._typeLabel);
            this.Controls.Add(this._numberLabel);
            this.Name = "MdoStatusControl";
            this.Size = new System.Drawing.Size(527, 21);
            this.Enter += new System.EventHandler(this.MdoStatusControl_Enter);
            this.Leave += new System.EventHandler(this.MdoStatusControl_Leave);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label _numberLabel;
        private System.Windows.Forms.Label _typeLabel;
        private BEMN.Forms.LedControl _sensor1_1;
        private BEMN.Forms.LedControl _sensor1_2;
        private BEMN.Forms.LedControl _sensor1_3;
        private BEMN.Forms.LedControl _sensor2_1;
        private BEMN.Forms.LedControl _sensor2_2;
        private BEMN.Forms.LedControl _sensor2_3;
        private BEMN.Forms.LedControl _sensor3_1;
        private BEMN.Forms.LedControl _sensor3_2;
        private BEMN.Forms.LedControl _sensor3_3;
        private BEMN.Forms.LedControl _relay2;
        private BEMN.Forms.LedControl _relay1;
        private BEMN.Forms.LedControl _relay3;
        private BEMN.Forms.LedControl _relay4;
        private BEMN.Forms.LedControl _defect;
        private System.Windows.Forms.Label _numberLabelCopy;
    }
}
