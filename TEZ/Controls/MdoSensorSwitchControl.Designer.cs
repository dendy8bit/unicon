﻿namespace BEMN.TEZ.Controls
{
    partial class MdoSensorSwitchControl
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this._ch3Enabled = new System.Windows.Forms.CheckBox();
            this._ch1Enabled = new System.Windows.Forms.CheckBox();
            this._ch2Enabled = new System.Windows.Forms.CheckBox();
            this.groupBox7.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this._ch3Enabled);
            this.groupBox7.Controls.Add(this._ch1Enabled);
            this.groupBox7.Controls.Add(this._ch2Enabled);
            this.groupBox7.Location = new System.Drawing.Point(3, 3);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(99, 104);
            this.groupBox7.TabIndex = 50;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Ввод датчиков";
            // 
            // _ch3Enabled
            // 
            this._ch3Enabled.AutoSize = true;
            this._ch3Enabled.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this._ch3Enabled.Location = new System.Drawing.Point(6, 71);
            this._ch3Enabled.Name = "_ch3Enabled";
            this._ch3Enabled.Size = new System.Drawing.Size(72, 17);
            this._ch3Enabled.TabIndex = 45;
            this._ch3Enabled.Text = "Датчик 3";
            this._ch3Enabled.UseVisualStyleBackColor = true;
            this._ch3Enabled.CheckedChanged += new System.EventHandler(this._chEnabled_CheckedChanged);
            // 
            // _ch1Enabled
            // 
            this._ch1Enabled.AutoSize = true;
            this._ch1Enabled.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this._ch1Enabled.Location = new System.Drawing.Point(6, 25);
            this._ch1Enabled.Name = "_ch1Enabled";
            this._ch1Enabled.Size = new System.Drawing.Size(72, 17);
            this._ch1Enabled.TabIndex = 43;
            this._ch1Enabled.Text = "Датчик 1";
            this._ch1Enabled.UseVisualStyleBackColor = true;
            this._ch1Enabled.CheckedChanged += new System.EventHandler(this._chEnabled_CheckedChanged);
            // 
            // _ch2Enabled
            // 
            this._ch2Enabled.AutoSize = true;
            this._ch2Enabled.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this._ch2Enabled.Location = new System.Drawing.Point(6, 48);
            this._ch2Enabled.Name = "_ch2Enabled";
            this._ch2Enabled.Size = new System.Drawing.Size(72, 17);
            this._ch2Enabled.TabIndex = 44;
            this._ch2Enabled.Text = "Датчик 2";
            this._ch2Enabled.UseVisualStyleBackColor = true;
            this._ch2Enabled.CheckedChanged += new System.EventHandler(this._chEnabled_CheckedChanged);
            // 
            // MdoSensorSwitchControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox7);
            this.Name = "MdoSensorSwitchControl";
            this.Size = new System.Drawing.Size(106, 108);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.CheckBox _ch3Enabled;
        private System.Windows.Forms.CheckBox _ch1Enabled;
        private System.Windows.Forms.CheckBox _ch2Enabled;
    }
}
