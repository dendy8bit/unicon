﻿namespace BEMN.TEZ.Controls
{
    partial class LogicSignalsControl
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._logicSignalsCheckList = new System.Windows.Forms.CheckedListBox();
            this._inputLogicSignalComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this._logicSignalsCheckList);
            this.groupBox2.Controls.Add(this._inputLogicSignalComboBox);
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(188, 210);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Выходные логические сигналы";
            // 
            // _logicSignalsCheckList
            // 
            this._logicSignalsCheckList.CheckOnClick = true;
            this._logicSignalsCheckList.FormattingEnabled = true;
            this._logicSignalsCheckList.Location = new System.Drawing.Point(7, 46);
            this._logicSignalsCheckList.Name = "_logicSignalsCheckList";
            this._logicSignalsCheckList.Size = new System.Drawing.Size(120, 154);
            this._logicSignalsCheckList.TabIndex = 4;
            this._logicSignalsCheckList.Leave += new System.EventHandler(this._logicSignalsCheckList_Leave);
            // 
            // _inputLogicSignalComboBox
            // 
            this._inputLogicSignalComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._inputLogicSignalComboBox.FormattingEnabled = true;
            this._inputLogicSignalComboBox.Items.AddRange(new object[] {
            "ВЛС 1",
            "ВЛС 2",
            "ВЛС 3"});
            this._inputLogicSignalComboBox.Location = new System.Drawing.Point(6, 19);
            this._inputLogicSignalComboBox.Name = "_inputLogicSignalComboBox";
            this._inputLogicSignalComboBox.Size = new System.Drawing.Size(121, 21);
            this._inputLogicSignalComboBox.TabIndex = 4;
            this._inputLogicSignalComboBox.SelectedIndexChanged += new System.EventHandler(this._inputLogicSignalComboBox_SelectedIndexChanged);
            // 
            // LogicSignalsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox2);
            this.Name = "LogicSignalsControl";
            this.Size = new System.Drawing.Size(194, 216);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckedListBox _logicSignalsCheckList;
        private System.Windows.Forms.ComboBox _inputLogicSignalComboBox;
    }
}
