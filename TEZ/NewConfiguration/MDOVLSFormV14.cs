﻿using BEMN.TEZ.Configuration.Structures;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using BEMN.TEZ.ControlsNewConfig;
using BEMN.TEZ.SupportClasses;

namespace BEMN.TEZ.NewConfiguration
{
    /// <summary>
    /// Форма для отображение ВЛС и МДО(реле)
    /// </summary>
    public partial class MDOVLSFormV14 : Form
    {
        #region Передача свойств, до закрытия формы
        public event Action<MDOVLSFormV14> ClickEventOk;
        private List<string> _newList = new List<string>();

        public void OnClick()
        {
            if (this.ClickEventOk!= null)
            {
                this.ClickEventOk(this);
            }
           
        }
        #endregion

        #region Константы
        private const int MDO_RELE_COUNT = 4;
        private const int TEN_FACTOR = 10;
        private const string PULSE_INCORRECT_VALUE = "Некорректные данные, проверьте ввод Тимп(мс). Значение должно лежать в диапазоне 10-1000 и должно быть кратно 10.";
        private const string EMPTY_STRING = "Некорректные данные, поле не может быть пустым";
        #endregion

        #region Поля
        private ushort[] arrMDOVLS;
        private MDOVLSStruct _mdovls;
        private MdoConfigStruct _mdoConfig;
        #endregion

        #region Свойства
        public MDOVLSStruct MDOVLSProp { get { return this._mdovls; } set { this._mdovls = value; } }
        public MdoConfigStruct MdoConfigStr { get { return this._mdoConfig; } set { this._mdoConfig = value; } }
        #endregion

        public MDOVLSFormV14()
        {
            this.InitializeComponent();
            this.InitializationDataGrid();
            this.arrMDOVLS = new ushort[4];
        }

        private int _mdoNumber;

        public int MdoNumber
        {
            get { return this._mdoNumber; }
            set
            {
                this._mdoNumber = value;
                Text = string.Format("ВЛС и Реле МДО № {0}", this._mdoNumber+1);
            }
        }

        //Инициализация таблицы реле МДО
        private void InitializationDataGrid()
        {
            this._typeMdoReleColumn.DataSource = Strings.ReleType;

            this._newList = RemoveAllEmptyStrings(Strings.ReleMdoSignalNewVersion);

            if (Strings.Version2 >= 6)
            {
                this.ClientSize = new System.Drawing.Size(713, 387);
                this.mdoLogicControl1.Size = new System.Drawing.Size(278, 315);
                this.groupBox1.Size = new System.Drawing.Size(284, 334);
                this.btn_Cansel.Location = new System.Drawing.Point(580, 352);
                this._relayOfflineGroupBox.Location = new System.Drawing.Point(12, 198);
                this.btn_Ok.Location = new System.Drawing.Point(475, 352);
            }

            this._extReleTypeCol.DataSource = Strings.ReleType;
            this._signalMdoReleColumn.DataSource = Strings.ReleMdoSignalNewVersionView;

            if (Strings.Version2 >= 4 && Strings.Version2 < 6)
            {
                this._signalMdoReleColumn.DataSource = RemoveAllEmptyStrings(Strings.ReleMdoSignalNewVersionView);
            }

            for (int i = 0; i < MDO_RELE_COUNT; i++)
            {
                this._releMdoDataGridView.Rows.Add(i + 1, Strings.ReleType[0], 0, Strings.ReleMdoSignalNewVersion[0]);
                this._dataGridReleView.Rows.Add(i + 1, Strings.ReleType[0], 0, true, true, true, true);
            }
        }

        //удаления пустых строк для Datasource, в Datagrid Реле мдо
        public static List<string> RemoveAllEmptyStrings(List<string> lst)
        {
            List<string> newList = new List<string>();
            foreach (var s in lst)
            {
                if (s != "")
                {
                    newList.Add(s);
                }
            }
            return newList;
        }

        //Вывод данных на экран
        public void ShowVLSandRele()
        {
            //Передача сигналов контролу в форме 
            this.MDOVLSProp.MDOLogicSignal.CopyTo(this.arrMDOVLS,0);
            this.mdoLogicControl1.SignalsMDO = this.arrMDOVLS;
            //Вывод Реле
            this.ShowReleStatus();
        }

        #region Реле в системе и авто.
        public void ShowReleStatus()
        {
            //Конфигурация автономного режима
            bool[,] sensorsRelaysState = this.MdoConfigStr.SensorsRelaysState;
            bool[] offlineRelaysType = this.MdoConfigStr.OfflineRelaysType;
            byte[] offlineRelaysPulse = this.MdoConfigStr.OfflineRelaysPulse;
            this._dataGridReleView.Rows.Clear();

            for (int i = 0; i < 4; i++)
            {
                var offlineReleTypeIndex = offlineRelaysType[i] ? 1 : 0;
                this._dataGridReleView.Rows.Add(i + 1,
                                           Strings.ReleType[offlineReleTypeIndex],
                                           offlineRelaysPulse[i] * TEN_FACTOR,
                                           sensorsRelaysState[0, i],
                                           sensorsRelaysState[1, i],
                                           sensorsRelaysState[2, i]);
            }

            //Конфигурация реле
            this._releMdoDataGridView.Rows.Clear();
            byte[][] releys = this.MdoConfigStr.Releys;

            for (int i = 0; i < MDO_RELE_COUNT; i++)
            {
                var releTypeIndex = releys[i][0];
                var pulse = releys[i][1] * TEN_FACTOR;
                var releSignalIndex = releys[i][2];

                try
                {
                    this._releMdoDataGridView.Rows.Add(i + 1,
                        Strings.ReleType[releTypeIndex],
                        pulse,
                        this._newList[releSignalIndex]);
                }
                catch (Exception e)
                {
                    this._releMdoDataGridView.Rows.Add(i + 1,
                        Strings.ReleType[0],
                        pulse,
                        this._newList[0]);
                    MessageBox.Show($"Недопустимое значение конфигурации реле при работе в системе №{i + 1}.", "Ошибка данных");
                }
                
            }
        }

        private bool PulseValidate(DataGridView sourse, int column, int row)
        {
            //Исключение на случай пустой строки
            try
            {
                int value;
                if (int.TryParse(sourse[column, row].Value.ToString(), out value))
                {
                    if ((value >= 10 & value <= 1000) & (value % 10 == 0))
                    {
                        sourse[column, row].Style.BackColor = Color.White;
                        return true;
                    }
                }
                sourse[column, row].Style.BackColor = Color.Red;
              //  MessageBox.Show(PULSE_INCORRECT_VALUE);
                return false;
            }
            catch
            {
                MessageBox.Show(EMPTY_STRING);
                return false;
            }

        }
        private int PulseValidate1(string pulse, DataGridView sourse, int column, int row)
        {

            int minValue = 10;
            try
            {
                
                int resPulse;
                if (int.TryParse(pulse, out resPulse))
                {
                    if ((resPulse >= 10 & resPulse <= 1000) & (resPulse%10 == 0))
                    {
                        return resPulse;
                    }
                    //MessageBox.Show(PULSE_INCORRECT_VALUE);
                    sourse[column, row].Style.BackColor = Color.Red;
                }
                else
                {
                  //  MessageBox.Show(PULSE_INCORRECT_VALUE);
                    sourse[column, row].Style.BackColor = Color.Red;
                    return minValue;
                }      
            }
            catch
            {
                MessageBox.Show(EMPTY_STRING);
                sourse[column, row].Style.BackColor = Color.Red;
            }
            return minValue;
        }

        private void SaveConfigForm()
        {
            //Сохранение ВЛС
            var mdoVls = this.MDOVLSProp;
            mdoVls.MDOLogicSignal = this.mdoLogicControl1.SignalsMDO; //arrMDOVLS;
            this.MDOVLSProp = mdoVls;

            //Сохранение реле
            var releys = new byte[MdoConfigStruct.RELEYS_COUNT][];
            for (int i = 0; i < MdoConfigStruct.RELEYS_COUNT; i++)
            {
                var releType = this._releMdoDataGridView[1, i].Value.ToString();
                var relePulse = this._releMdoDataGridView[2, i].Value.ToString();
                var releSignal = this._releMdoDataGridView[3, i].Value.ToString();
                releys[i] = new[]
                {
                    (byte) Strings.ReleType.IndexOf(releType),
                    (byte) (int.Parse(relePulse) / TEN_FACTOR),
                    (byte) Strings.ReleMdoSignalNewVersion.IndexOf(releSignal)
                };
            }
            this._mdoConfig.Releys = releys;

            //Сохранение реле автономного режима
            var sensorsRelaysState = new bool[3, 4];
            var types = new bool[4];
            var pulses = new byte[4];
            for (var i = 0; i < 4; i++)
            {
                types[i] = Strings.ReleType.IndexOf(this._dataGridReleView[1, i].Value.ToString()) != 0;
                if (this.PulseValidate(this._dataGridReleView,2,i))
                {
                    pulses[i] = (byte)(int.Parse(this._dataGridReleView[2, i].Value.ToString()) / TEN_FACTOR);
                }
                
                sensorsRelaysState[0, i] = Convert.ToBoolean(this._dataGridReleView[3, i].Value);
                sensorsRelaysState[1, i] = Convert.ToBoolean(this._dataGridReleView[4, i].Value);
                sensorsRelaysState[2, i] = Convert.ToBoolean(this._dataGridReleView[5, i].Value);

            }
            this._mdoConfig.SensorsRelaysState = sensorsRelaysState;
            this._mdoConfig.OfflineRelaysType = types;
            this._mdoConfig.OfflineRelaysPulse = pulses;
        }
        #endregion

        private void btnOk_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < 4; i++)
            {
                if (this.PulseValidate(this._dataGridReleView, 2, i) != true)
                {
                    MessageBox.Show(PULSE_INCORRECT_VALUE);
                    return;
                }
                if (this.PulseValidate(this._releMdoDataGridView, 2, i) != true)
                {
                    MessageBox.Show(PULSE_INCORRECT_VALUE);
                    return;
                }
            }

            this.SaveConfigForm();
            this.OnClick();
            Close();
        }

        private void btn_Cansel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
