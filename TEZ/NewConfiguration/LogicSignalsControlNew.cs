﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using BEMN.MBServer;
using BEMN.TEZ.SupportClasses;

namespace BEMN.TEZ.NewConfiguration
{
    public partial class LogicSignalsControlNew : UserControl
    {
        private const int SIGNALS_COUNT = 3;
        private ushort[] _signals = new ushort[SIGNALS_COUNT];
        private bool vn1;
        private bool vn2;
        private bool vn3;
        private bool input3;
        private bool offSv;
        private bool offSvv;
        private bool srabDoc;
        private bool urov;
        bool value;
        List<bool> massiveValues = new List<bool>();

        public LogicSignalsControlNew()
        {
            InitializeComponent();

            if (Strings.Version2 >= 6)
            {
                this.dataGridView.Size = new System.Drawing.Size(218, 293);
                this.Size = new System.Drawing.Size(218, 293);
            }
            
            foreach (var signal in Strings.ReleLogicSignalView)
            {
                dataGridView.Rows.Add(signal, false, false, false);
            }
            
        }
   
        /// <summary>
        /// Логические сигналы
        /// </summary>
        public ushort[] Signals
        {
            get
            {
                NewGetSignals();
                return _signals;
            }
            set
            {
                _signals = value;
                NewSetSignals();
            }
        }

        private void NewSetSignals()
        {
            try
            {
                for (int i = 0; i < SIGNALS_COUNT; i++)
                {
                    var a = new BitArray(new int[] { _signals[i] });

                    for (int j = 0; j < this.dataGridView.Rows.Count; j++)
                    {
                        dataGridView[i + 1, j].Value = a[j];
                    }

                    if (Strings.Version2 >= 6)
                    {
                        vn1 = (bool)dataGridView[i + 1, 4].Value;
                        vn2 = (bool)dataGridView[i + 1, 5].Value;
                        offSv = (bool)dataGridView[i + 1, 6].Value;
                        offSvv = (bool)dataGridView[i + 1, 7].Value;
                        srabDoc = (bool)dataGridView[i + 1, 8].Value;
                        urov = (bool)dataGridView[i + 1, 9].Value;
                        input3 = (bool)dataGridView[i + 1, 10].Value;
                        vn3 = (bool)dataGridView[i + 1, 11].Value;

                        dataGridView[i + 1, 4].Value = input3;
                        dataGridView[i + 1, 5].Value = vn1;
                        dataGridView[i + 1, 6].Value = vn2;
                        dataGridView[i + 1, 7].Value = vn3;
                        dataGridView[i + 1, 8].Value = offSv;
                        dataGridView[i + 1, 9].Value = offSvv;
                        dataGridView[i + 1, 10].Value = srabDoc;
                        dataGridView[i + 1, 11].Value = urov;
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            
        }

        private void NewGetSignals()
        {
            this._signals = new ushort[SIGNALS_COUNT];

            try
            {
                for (int i = 0; i < SIGNALS_COUNT; i++)
                {
                    if (Strings.Version2 >= 6)
                    {
                        massiveValues.Clear();

                        input3 = (bool)dataGridView[i + 1, 4].Value;
                        vn1 = (bool)dataGridView[i + 1, 5].Value;
                        vn2 = (bool)dataGridView[i + 1, 6].Value;
                        vn3 = (bool)dataGridView[i + 1, 7].Value;
                        offSv = (bool)dataGridView[i + 1, 8].Value;
                        offSvv = (bool)dataGridView[i + 1, 9].Value;
                        srabDoc = (bool)dataGridView[i + 1, 10].Value;
                        urov = (bool)dataGridView[i + 1, 11].Value;

                        for (int j = 0; j < this.dataGridView.Rows.Count; j++)
                        {
                            value = Convert.ToBoolean(dataGridView[i + 1, j].Value);
                            massiveValues.Add(value);
                        }

                        massiveValues[4] = vn1;
                        massiveValues[5] = vn2;
                        massiveValues[6] = offSv;
                        massiveValues[7] = offSvv;
                        massiveValues[8] = srabDoc;
                        massiveValues[9] = urov;
                        massiveValues[10] = input3;
                        massiveValues[11] = vn3;
                    }

                    for (int j = 0; j < this.dataGridView.Rows.Count; j++)
                    {
                        if (Strings.Version2 >= 6)
                        {
                            value = massiveValues[j];
                            _signals[i] = Common.SetBit(_signals[i], j, value);
                        }
                        else
                        {
                            value = Convert.ToBoolean(dataGridView[i + 1, j].Value);
                            _signals[i] = Common.SetBit(_signals[i], j, value);
                        }
                    }

                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
    }
}
