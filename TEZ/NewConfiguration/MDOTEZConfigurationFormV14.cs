﻿using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using AssemblyResources;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms.ValidatingClasses;
using BEMN.Forms.ValidatingClasses.Rules.Int;
using BEMN.Framework.BusinessLogic;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.TEZ.Configuration.Structures;
using BEMN.TEZ.SupportClasses;

namespace BEMN.TEZ.NewConfiguration
{

    public partial class MDOTEZConfigurationFormV14 : Form, IFormView
    {
        #region [Константы]
        private const int MDO_COUNTS = 24;
        private const string SUCCESSFUL_READ_CONFIGURATION = "Конфигурация прочитана";
        private const string FILE_IS_SAVED_PATTERN = "Файл {0} сохранен";
        private const string SETPOINT_IS_WRITE = "Уставки записаны";
        private const string TEZ_SAVE_MESSAGE_PATTERN = "Записать конфигурацию ТЭЗ-24 № {0} ?";
        private const string IMPOSSIBLE_READ_CONFIGURATION = "Невозможно прочитать конфигурацию";
        private const string ATTENTION = "Внимание";
        private const string READING_SETPOINTS = "Идёт чтение уставок";
        private const string CONFIGURATION_IS_WRITTING = "Идёт запись конфигурации";
        private const string IMPOSSIBLE_WRITE_CONFIGURATION = "Невозможно записать конфигурацию";
        private const string ERROR_READ_CONFIGURATION = "Ошибка чтения конфигурации";
        private const string FILE_SAVE_FAIL = "Не удалось сохранить файл конфигурации";
        private const string FILE_LOAD_FAIL = "Не удалось загрузить файл конфигурации";
        private const string XML_HEAD = "TEZ_SET_POINTS";
        //private const int DISCRETS_COUNT = 6;
        private const int RELE_COUNT = 3;
        private const int TEN_FACTOR = 10;
        private const string DATA_ERROR = "Ошибка данных (недопустимое значение дискретных входов).";
        private const string PULSE_INCORRECT_VALUE = "Некорректные данные, проверьте ввод Тимп(мс). Значение должно лежать в диапазоне 10-1000 и должно быть кратно 10.";
        private const string DISCR_TIME_ERR = "Задержка дискретных входов должна быть в диапазоне 1...100";
        private const string UROV_ERROR = "Время УРОВ должно быть в пределах 10 - 1000 мс";
        private const string EMPTY_STRING = "Некорректные данные, поле не может быть пустым.";
        #endregion [Constants]

        #region [Поля]
        private int _indexMdo;
        private readonly Tez _device;
        private bool _errorMessage;
        private FlashWriteStates _writeFlashMemoryState;
        private FlashMemoryStructNewV14 _fm;
        private MemoryEntity<FlashMemoryStructNewV14> _flashMemory;
        private MemoryEntity<CommandStruct> _command;
        private MemoryEntity<OneWordStruct> _version;
        #endregion Поля

        #region [Nested types]
        //Этапы записи флеш памяти
        private enum FlashWriteStates
        {
            //Базовое состояние
            NONE = 0,
            //Команда перед записью
            START_WRITE = 2,
            //Команда об окончании записи
            END_WRITE = 3
        }
        #endregion [Nested types]

        #region IFormView
        public Type FormDevice
        {
            get { return typeof(Tez); }
        }

        public bool Multishow { get; private set; }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public Type ClassType
        {
            get { return typeof(MDOTEZConfigurationFormV14); }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.config.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Конфигурация TЭЗ"; }
        }
        #endregion IFormView

        #region [Конструкторы и иницализация]
        public MDOTEZConfigurationFormV14()
        {
            this.InitializeComponent();
        }

        public MDOTEZConfigurationFormV14(Tez device)
        {
            this.InitializeComponent();
            this._fm = new FlashMemoryStructNewV14(true);
            this._device = device;

            this._version = new MemoryEntity<OneWordStruct>("version", device, 0x53);
            this._version.AllReadOk += HandlerHelper.CreateReadArrayHandler(this.OnVersionReadOk);
            this._version.AllReadFail += HandlerHelper.CreateReadArrayHandler(() =>
            {
                TreeManager.OnDeviceVersionLoadFail(this);
            });

            this._flashMemory = this._device.FlashMemoryNewV14;
            this._command = this._device.CommandProperty;
            this._flashMemory.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.StructRead);
            this._flashMemory.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.StructAllReadFail);
            this._flashMemory.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.StructAllWriteFail);
            this._command.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.StructAllWriteFail);
            this._command.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.CommandIsWrite);
            this._flashMemory.ReadFail += HandlerHelper.CreateHandler(this, this.FlashMemoryReadFail);
            this._flashMemory.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.FlashMemory_AllWriteOk);
            this._flashMemory.ReadOk += HandlerHelper.CreateHandler(this, this.ProgressBarIncrement);
            this._flashMemory.WriteOk += HandlerHelper.CreateHandler(this, this.ProgressBarIncrement);
            this._toolStripProgressBar.Maximum = this._flashMemory.Slots.Count;
            
            this.InitializeDataGridView();
            
        }

        private void OnVersionReadOk()
        {
            ushort value = this._version.Value.Word;
            if (value >= 1000)
            {
                this.Version1 = value / 1000;
                this.Version2 = (value - this.Version1 * 1000) / 100;
                this.Version3 = value - this.Version1 * 1000 - this.Version2 * 100;
            }
            else if (value >= 100)
            {
                this.Version1 = value / 100;
                this.Version2 = (value - this.Version1 * 100) / 10;
                this.Version3 = value - this.Version1 * 100 - this.Version2 * 10;
            }

            BeginInvoke(new MethodInvoker(delegate
            {
                if (Version1 == 1 && Version2 == 5 && Version3 >= 0)
                {
                    this.discretConfigGroup.Visible = true;
                }

                if (Version2 >= 6)
                {
                    this.panel1.Size = new System.Drawing.Size(1017, 641);
                    this.groupBox3.Size = new System.Drawing.Size(454, 554);
                    this.groupBox4.Size = new System.Drawing.Size(228, 310);
                    this.groupBox2.Location = new System.Drawing.Point(220, 335);
                    this.groupBox1.Location = new System.Drawing.Point(9, 335);
                    this._releGroupBox.Location = new System.Drawing.Point(6, 438);
                    this._confDgw.Size = new System.Drawing.Size(549, 612);
                    this._statusStrip1.Location = new System.Drawing.Point(0, 619);
                    this._btnLoadFromFile.Location = new System.Drawing.Point(839, 592);
                    this._btnSaveInFile.Location = new System.Drawing.Point(839, 563);
                    this._btnWriteConfig.Location = new System.Drawing.Point(558, 592);
                    this._btnReadConfig.Location = new System.Drawing.Point(558, 563);
                    this.logicSignalsControl1.Size = new System.Drawing.Size(214, 286);
                    this.ClientSize = new System.Drawing.Size(1017, 641);
                }
            }));
            
            this.LoadConfig();
        }

        public int Version1 { get; private set; }
        public int Version2 { get; private set; }
        public int Version3 { get; private set; }

        //Инициализация таблиц
        private void InitializeDataGridView()
        {
            this.Column1.DataSource = Strings.MdoTypeView;
            this._confDgw.EditingControlShowing += this.dgv_EditingControlShowing;

            //Инициализация таблицы дискрет ТЭЗ
            this._typeColumn.DataSource = Strings.DiscretTypeView;
            this._discretsDataGridView.Rows.Clear();
            for (int i = 0; i < Strings.DiskretCount; i++)
            {
                this._discretsDataGridView.Rows.Add(new object[] { i + 1, Strings.DiscretTypeView[0] });
            }
            //Инициализация таблицы реле ТЭЗ
            this._releSignalColumn.DataSource = Strings.ReleTezSignalView;
            this._releTypeColumn.DataSource = Strings.ReleType;
            this._releDataGridView.Rows.Clear();
            for (int i = 0; i < RELE_COUNT; i++)
            {
                this._releDataGridView.Rows.Add(new object[] { i + 1, Strings.ReleType[0], 0, Strings.ReleTezSignalView[0] });
            }
        }

        public void ShowTezConfiguration()
        {
            try
            {
                this._urovEnabled.Checked = this._fm.TezUrovRelayStruct.UrovEnabled;
                this._urovTextBox.Text = this._fm.TezUrovRelayStruct.UrovTime.ToString();
                //Конфигурация реле неисправности
                //Временное исправления для вывода в реле неисправностей
                bool[] _arrHelperChBox = new bool[3];
                for (int i = 0; i < this._fm.TezUrovRelayStruct.RelayFaultConfiguration.Length; i++)
                {
                    _arrHelperChBox[i] = this._fm.TezUrovRelayStruct.RelayFaultConfiguration[i];
                }
                this._tezFaultCheckBox1.Checked = _arrHelperChBox[0];
                this._tezFaultCheckBox2.Checked = _arrHelperChBox[1];
                this._tezFaultCheckBox3.Checked = _arrHelperChBox[2];
                //дискреты
                DiscretConfigTezStruct[] discrets = this._fm.Discrets;
                this._discretsDataGridView.Rows.Clear();

                for (int i = 0; i < FlashMemoryStructNewV14.TEZ_DISCRETS_COUNTS; i++)
                {
                    ushort type = discrets[i].DiscretType;
                    if (type > Strings.DiscretType.Count - 1)
                    {
                        this._discretsDataGridView.Rows.Add(i + 1, Strings.DiscretType[0]);
                        MessageBox.Show($"Недопустимое значение дискретных входов №{i + 1}.", "Ошибка данных");
                    }
                    else
                    {
                        this._discretsDataGridView.Rows.Add(i + 1, Strings.DiscretType[type]);
                    }
                }
                this._constDiscr.Checked = this._fm.TezUrovRelayStruct.TypeDiscretInput;
                this._t1Discr.Text = this._fm.TezUrovRelayStruct.T1.ToString("D");
                this._t0Discr.Text = this._fm.TezUrovRelayStruct.T0.ToString("D");

                //реле

                this._releDataGridView.Rows.Clear();
                for (int i = 0; i < FlashMemoryStructNewV14.TEZ_RELEYS_COUNTS; i++)
                {
                    byte type = this._fm.Releys[i].ReleType;
                    int pulse = this._fm.Releys[i].RelayTime * TEN_FACTOR;
                    byte signal = this._fm.Releys[i].ReleSignal;

                    this._releDataGridView.Rows.Add(i + 1, Strings.ReleType[type], pulse, Strings.ReleTezSignal[signal]);
                }

                //Передача ВЛС контролу
                this.logicSignalsControl1.Signals = this._fm.TezUrovRelayStruct.LogicSignals;
            }
            catch (Exception ex)
            {
                MessageBox.Show(DATA_ERROR);
            }
        }

        private void dgv_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            dgv.EditingControl.KeyPress += this.EditingControl_KeyPress;
            dgv.EditingControl.TextChanged += this.EditingControl_TextChanged;
            
        }

        private readonly CustomIntRule _rule = new CustomIntRule(1, 250);

        private void EditingControl_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                e.Handled = this._rule.IsValidKey(e.KeyChar);
            }
            catch
            {
                e.Handled = false;
            }
        }

        private void EditingControl_TextChanged(object sender, EventArgs e)
        {
            DataGridView dgv = (DataGridView)(((Control)sender).Parent.Parent);
            try
            {
                TextBox tb = (TextBox) sender;
                bool flag = this._rule.IsValid(tb.Text);
                
                Panel panel = (Panel) tb.Parent;
                
                if (flag)
                {
                    dgv.CurrentCell.Style.SelectionBackColor = SystemColors.Highlight;
                    panel.BackColor = Color.White;

                    dgv.CurrentCell.Style.BackColor = Color.White;
                    tb.BackColor = Color.White;
                    this._toolTip.Hide(tb);
                }

                else
                {
                    dgv.CurrentCell.Style.SelectionBackColor = Color.Red;
                    panel.BackColor = Color.Red;

                    dgv.CurrentCell.Style.BackColor = Color.Red;
                    tb.BackColor = Color.Red;
                    this._toolTip.Show(this._rule.ErrorMessage, tb, tb.Width, 0);
                }
            }
            catch (Exception)
            {
            }

        }
        #endregion


        #region подписчики на события MemoryEntity
        //Чтение структуры
        private void StructRead()
        {
            //Выводим статус о удачном прочтении памяти
            this._toolStripStatusLabel1.Text = SUCCESSFUL_READ_CONFIGURATION;
            //Заполняем полосу загрузки
            this._toolStripProgressBar.Value = this._toolStripProgressBar.Maximum;
            //Выводим сообщение о удачном прочтении памяти
            MessageBox.Show(SUCCESSFUL_READ_CONFIGURATION);
            ////Вывод параметров ТЭЗа.
            this._fm = this._flashMemory.Value;
            this.ShowConfiguration();
            this._btnReadConfig.Enabled = true;
            this._btnWriteConfig.Enabled = true;
            this._btnLoadFromFile.Enabled = true;
            this._btnSaveInFile.Enabled = true;
        }

        //Конфигурация не записана
        private void StructAllWriteFail()
        {
            this._toolStripStatusLabel1.Text = IMPOSSIBLE_WRITE_CONFIGURATION;
            MessageBox.Show(IMPOSSIBLE_WRITE_CONFIGURATION);
            this._btnWriteConfig.Enabled = true;
            this._btnReadConfig.Enabled = true;
        }

        //Конфигурация не прочитана
        private void StructAllReadFail()
        {
            this._toolStripProgressBar.Value = this._toolStripProgressBar.Maximum;
            if (!this._errorMessage)
            {
                MessageBox.Show(IMPOSSIBLE_READ_CONFIGURATION);
                this._errorMessage = true;
            }
            this._btnReadConfig.Enabled = true;
            this._btnWriteConfig.Enabled = true;
        }

        //Команда успешно записана
        private void CommandIsWrite()
        {
            if (this._writeFlashMemoryState == FlashWriteStates.START_WRITE)
                this.FlashMemoryWrite();
            if (this._writeFlashMemoryState == FlashWriteStates.NONE)
            {
                this._toolStripStatusLabel1.Text = SETPOINT_IS_WRITE;
                this._toolStripProgressBar.Value = this._toolStripProgressBar.Maximum;
                MessageBox.Show(SETPOINT_IS_WRITE);
            }
        }

        //Обработка ошибки запроса на чтение
        private void FlashMemoryReadFail()
        {
            this._toolStripStatusLabel1.Text = ERROR_READ_CONFIGURATION;
            this.ProgressBarIncrement();
            this._btnReadConfig.Enabled = true;
            this._btnWriteConfig.Enabled = true;
        }

        private void FlashMemory_AllWriteOk()
        {
            if (this._writeFlashMemoryState == FlashWriteStates.START_WRITE)
            {
                this._writeFlashMemoryState = FlashWriteStates.END_WRITE;
                this.WriteCommand((ushort)this._writeFlashMemoryState);
            }
            else
            {
                this._writeFlashMemoryState = FlashWriteStates.NONE;
            }

            this._btnLoadFromFile.Enabled = true;
            this._btnReadConfig.Enabled = true;
            this._btnSaveInFile.Enabled = true;
            this._btnWriteConfig.Enabled = true;
        }
        #endregion

        #region Дополнительные функции
        private bool PulseValidate(DataGridView sourse, int column, int row)
        {
            try
            {
                int value;
                if (int.TryParse(sourse[column, row].Value.ToString(), out value))
                {
                    if ((value >= 10 & value <= 1000) & (value % 10 == 0))
                    {
                        sourse[column, row].Style.BackColor = Color.White;
                        return true;
                    }
                }
                sourse[column, row].Style.BackColor = Color.Red;
                return false;
            }
            catch
            {
                MessageBox.Show(EMPTY_STRING);
                return false;
            }

        }

        private void _releDataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 2)
            {
                DataGridView source = sender as DataGridView;
                if (source != null)
                    if (this.PulseValidate(source, e.ColumnIndex, e.RowIndex))
                        this.TezReleChanged();
            }
            else
            {
                this.TezReleChanged();
            }
        }

        private void TezReleChanged()
        {
            for (int i = 0; i < FlashMemoryStructNewV14.TEZ_RELEYS_COUNTS; i++)
            {
                string type = this._releDataGridView[1, i].Value.ToString();
                string pulse = this._releDataGridView[2, i].Value.ToString();
                string signal = this._releDataGridView[3, i].Value.ToString();
                this._fm.Releys[i].ReleSignal = (byte)Strings.ReleTezSignal.IndexOf(signal);
                if (this.PulseValidate(this._releDataGridView, 2, i))
                {
                    this._fm.Releys[i].RelayTime = (byte)(int.Parse(pulse) / TEN_FACTOR);
                }
                this._fm.Releys[i].ReleType = (byte)Strings.ReleType.IndexOf(type);
            }
        }
        

        private void _urovEnabled_CheckedChanged(object sender, EventArgs e)
        {
            this._urovTextBox.Enabled = this._urovEnabled.Checked;
            TezUrovRelayStruct tezUrov = this._fm.TezUrovRelayStruct;
            tezUrov.UrovEnabled = this._urovEnabled.Checked;
            this._fm.TezUrovRelayStruct = tezUrov;
        }

        private void _urovTextBox_TextChanged(object sender, EventArgs e)
        {
            this.ValidateTextTime(this._urovTextBox, 10, 1000); //this.UrovValidate();
        }

        private bool WriteUrov(out string message)
        {
            TezUrovRelayStruct tezUrovRelay = this._fm.TezUrovRelayStruct;
            tezUrovRelay.LogicSignals = this.logicSignalsControl1.Signals;
            //Реле неисправности
            tezUrovRelay.RelayFaultConfiguration = new[]
                {
                    this._tezFaultCheckBox1.Checked,
                    this._tezFaultCheckBox2.Checked,
                    this._tezFaultCheckBox3.Checked
                };
            tezUrovRelay.UrovEnabled = this._urovEnabled.Checked;
            tezUrovRelay.TypeDiscretInput = this._constDiscr.Checked;

            if (this._constDiscr.Checked && this.discretConfigGroup.Visible)
            {
                if (this.ValidateTextTime(this._t1Discr, 1, 100) && this.ValidateTextTime(this._t0Discr, 1, 100))
                {
                    tezUrovRelay.T1 = ushort.Parse(this._t1Discr.Text);
                    tezUrovRelay.T0 = ushort.Parse(this._t0Discr.Text);
                }
                else
                {
                    message = DISCR_TIME_ERR;
                    return false;
                }
            }

            if (this._urovEnabled.Checked)
            {
                if (this.ValidateTextTime(this._urovTextBox, 10, 1000))
                {
                    tezUrovRelay.UrovTime = ushort.Parse(this._urovTextBox.Text);
                }
                else
                {
                    message = UROV_ERROR;
                    return false;
                }
            }

            this._fm.TezUrovRelayStruct = tezUrovRelay;
            message = null;
            return true;
        }

        private bool ValidateTextTime(MaskedTextBox box, int min, int max)
        {
            ushort time;
            if (ushort.TryParse(box.Text, out time) && time >= min && time <= max)
            {
                box.BackColor = Color.White;
                return true;
            }
            box.BackColor = Color.Red;
            return false;
        }

        //вывод статусов МДО
        private void ShowConfiguration()
        {
            this.ShowTezConfiguration();

            this._confDgw.Rows.Clear();
            
            try
            {
                for (int i = 0; i < MDO_COUNTS; i++)
                {
                    
                    if (this._fm.MdoConfig[i].MdoType == "XXXXX")
                    {
                        this._confDgw.Rows.Add
                        (
                            (i + 1).ToString(), "######", this._fm.MdoConfig[0].SensorsState[0], this._fm.MdoConfig[0].SensorsState[0], this._fm.MdoConfig[0].SensorsState[0], this._fm.MdoConfig[0].IsTest, this._fm.MdoConfig[0].TestTime,
                            "Настроить"
                        );
                        MessageBox.Show($"Недопустимое значение в конфигурации МДО №{i + 1}.", "Ошибка данных");
                    }
                    else
                    {
                        this._confDgw.Rows.Add
                        (
                            (i + 1).ToString(), this._fm.MdoConfig[i].MdoType, this._fm.MdoConfig[i].SensorsState[0], this._fm.MdoConfig[i].SensorsState[1], this._fm.MdoConfig[i].SensorsState[2], this._fm.MdoConfig[i].IsTest, this._fm.MdoConfig[i].TestTime,
                            "Настроить"
                        );
                    }

                    if (this._fm.MdoConfig[i].MdoType == Strings.MdoType[0] || this._fm.MdoConfig[i].MdoType == "XXXXX")
                    {
                        for (int j = 2; j < this._confDgw.ColumnCount; j++)
                        {
                            Validator.CellEnabled(this._confDgw[j, i], false);
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("Ошибка данных! /nНедопустимое значение в конфигурации МДО.");
            }
            
        }
        //Запись команды
        private void WriteCommand(ushort command)
        {
            CommandStruct cs = new CommandStruct();
            cs.Command = command;

            if ((FlashWriteStates)command == FlashWriteStates.END_WRITE)
                this._writeFlashMemoryState = FlashWriteStates.NONE;
            this._command.Value = cs;
            this._command.SaveStruct();
        }
        //Запись флеш памяти в устройство
        private void FlashMemoryWrite()
        {
            this._flashMemory.Value = this._fm;
            this._flashMemory.SaveStruct();
        }
        #endregion

        //Запись конфигурации в устройство
        private void WriteConfigurationToDevice()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (this.WriteMdoConfig())
            {
                string saveMessage = string.Format(TEZ_SAVE_MESSAGE_PATTERN, this._device.DeviceNumber);
                if (DialogResult.Yes ==
                    MessageBox.Show(saveMessage, ATTENTION, MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                        MessageBoxDefaultButton.Button2))
                {
                    this._btnWriteConfig.Enabled = false;
                    this._btnReadConfig.Enabled = false;
                    this._btnLoadFromFile.Enabled = false;
                    this._btnSaveInFile.Enabled = false;
                    this._writeFlashMemoryState = FlashWriteStates.START_WRITE;
                    this._toolStripProgressBar.Value = 0;
                    this._toolStripStatusLabel1.Text = CONFIGURATION_IS_WRITTING;
                    this.WriteCommand((ushort) this._writeFlashMemoryState);
                }
            }
        }

        private void ProgressBarIncrement()
        {
            if (this._toolStripProgressBar.Value < this._toolStripProgressBar.Maximum)
                this._toolStripProgressBar.Value++;
        }


        private void LoadConfig()
        {
            BeginInvoke(new MethodInvoker(delegate
            {
                this._btnReadConfig.Enabled = false;
                this._btnWriteConfig.Enabled = false;
                this._btnSaveInFile.Enabled = false;
                this._btnLoadFromFile.Enabled = false;
                this._toolStripStatusLabel1.Text = READING_SETPOINTS;
                this._toolStripProgressBar.Value = 0;
            }));
            this._flashMemory.LoadStruct(new TimeSpan(500));
        }

        private void CloseForm()
        {
            this._flashMemory.RemoveStructQueries();
        }
        #region Обработчики событий

        //Прочитать конфигурацию
        private void MDOConfigurationForm_Load(object sender, EventArgs e)
        {
            this.ShowConfiguration();
            if (Device.AutoloadConfig && this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode) this._version.LoadStruct();
        }


        private void MDOConfigurationForm_Closing(object sender, FormClosingEventArgs e)
        {
            this.CloseForm();
        }

        //Запись в цикле мдо контролов
        public bool WriteMdoConfig()
        {
            for (int i = 0; i < MDO_COUNTS; i++)
            {
                this._fm.MdoConfig[i].MdoType = this._confDgw[1, i].Value.ToString();
                bool[] states = new[]
                {
                    Convert.ToBoolean(this._confDgw[2, i].Value), Convert.ToBoolean(this._confDgw[3, i].Value),
                    Convert.ToBoolean(this._confDgw[4, i].Value)
                };
                this._fm.MdoConfig[i].SensorsState = states;
                this._fm.MdoConfig[i].IsTest = Convert.ToBoolean(this._confDgw[5, i].Value);
                this._fm.MdoConfig[i].TestTime = Convert.ToByte(this._confDgw[6, i].Value);
            }
            string message;
            if (!this.WriteUrov(out message))
            {
                MessageBox.Show(message);
                return false;
            }
            for (int i = 0; i <= 2; i++)
            {
                if (this.PulseValidate(this._releDataGridView, 2, i) != true)
                {
                    MessageBox.Show(PULSE_INCORRECT_VALUE);
                    return false;
                }
            }

            //Дискреты
            for (int i = 0; i < FlashMemoryStructNewV14.TEZ_DISCRETS_COUNTS; i++)
            {
                string value = this._discretsDataGridView[1, i].Value.ToString();
                this._fm.Discrets[i].DiscretType = (ushort)Strings.DiscretType.IndexOf(value);
            }

            return true;
        }
        
        private void _btnReadConfig_Click(object sender, EventArgs e)
        {
            if (Device.AutoloadConfig && this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode) this.LoadConfig();
        }
        //Записать конфигурацию
        private void _btnWriteConfig_Click(object sender, EventArgs e)
        {
            if (Device.AutoloadConfig && this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode) this.WriteConfigurationToDevice();
        }
        //Загрузить конфигурацию из файла
        private void _btnLoadFromFile_Click(object sender, EventArgs e)
        {
            this.LoadFromFileConfiguration();
        }
        //Сохранить конфигурацию в файл
        private void _btnSaveInFile_Click(object sender, EventArgs e)
        {
            this.SaveToFileConfiguration();
        }
        #endregion


        /// <summary>
        /// Сохранение конфигурации в файл
        /// </summary>
        /// <param name="binFileName">Имя файла</param>
        public void Serialize(string binFileName)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.AppendChild(doc.CreateElement("TEZ"));

                this.WriteMdoConfig();

                ushort[] values = this._fm.GetValues();

                XmlElement element = doc.CreateElement(XML_HEAD);
                element.InnerText = Convert.ToBase64String(Common.TOBYTES(values, false));
                if (doc.DocumentElement == null)
                {
                    throw new NullReferenceException();
                }
                doc.DocumentElement.AppendChild(element);

                doc.Save(binFileName);
                this._statusStrip1.Text = string.Format("Файл {0} успешно сохранён", binFileName);
            }
            catch
            {
                MessageBox.Show(FILE_SAVE_FAIL);
            }
        }

        /// <summary>
        /// Загрузка конфигурации из файла
        /// </summary>
        /// <param name="binFileName">Имя файла</param>
        public void Deserialize(string binFileName)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(binFileName);
                XmlNode a = doc.FirstChild.SelectSingleNode(XML_HEAD);
                if (a == null)
                    throw new NullReferenceException();

                byte[] values = Convert.FromBase64String(a.InnerText);
                FlashMemoryStructNewV14 _fmStruct = new FlashMemoryStructNewV14(true);
                _fmStruct.InitStruct(values);
                this._fm = _fmStruct;
                this.ShowConfiguration();
                this._statusStrip1.Text = string.Format("Файл {0} успешно загружен", binFileName);
            }
            catch
            {
                MessageBox.Show(FILE_LOAD_FAIL);
            }
        }

        //Сохранение уставок в файл
        private void SaveToFileConfiguration()
        {
            this._saveConfigurationDlg.FileName = $"Уставки ТЭЗ-24 Версия {this._device.DeviceVersion}";
            if (DialogResult.OK == this._saveConfigurationDlg.ShowDialog())
            {
                this.Serialize(this._saveConfigurationDlg.FileName);
                MessageBox.Show(string.Format(FILE_IS_SAVED_PATTERN, this._saveConfigurationDlg.FileName));
            }
        }
        //Загрузка уставок из файла
        private void LoadFromFileConfiguration()
        {
            if (DialogResult.OK == this._openConfigurationDlg.ShowDialog())
            {
                this.Deserialize(this._openConfigurationDlg.FileName);
            }
        }

        private void _confDgw_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex<0)
            {
                return;
            }
           
            if (e.ColumnIndex == 7)
            {
                if (this._confDgw[1,e.RowIndex].Value.ToString() == Strings.MdoType[0])
                {
                    return;
                }

                MDOVLSFormV14  mdoVlsForm = new MDOVLSFormV14();
                mdoVlsForm.ClickEventOk += this.WriteMDOControl;
                //mdoVlsForm.MdiParent = MdiParent;
                mdoVlsForm.MDOVLSProp = this._fm.MdovlsStruct[e.RowIndex];
                mdoVlsForm.MdoConfigStr = this._fm.MdoConfig[e.RowIndex];
                mdoVlsForm.MdoNumber = e.RowIndex;
                mdoVlsForm.ShowVLSandRele();
                mdoVlsForm.StartPosition = FormStartPosition.CenterParent;
                mdoVlsForm.ShowDialog();
            }
        }

        private void WriteMDOControl(MDOVLSFormV14 obj)
        {
            this._fm.MdovlsStruct[obj.MdoNumber] = obj.MDOVLSProp;
          
        }

        private void _confDgw_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex ==1)
            {
                bool flag = this._confDgw[1, e.RowIndex].Value.ToString() != Strings.MdoType[0];

                for (int j = 2; j < this._confDgw.ColumnCount; j++)
                {
                    Validator.CellEnabled(this._confDgw[j, e.RowIndex], flag);
                }
                if (flag && !Convert.ToBoolean(this._confDgw[5, e.RowIndex].Value))
                {
                    Validator.CellEnabled(this._confDgw[6, e.RowIndex], false);
                }
            }

             if (e.ColumnIndex == 5)
             {
                 if (this._confDgw[1, e.RowIndex].Value.ToString() == Strings.MdoType[0])
                 {
                     return;
                 }
                 Validator.CellEnabled(this._confDgw[6, e.RowIndex], Convert.ToBoolean(this._confDgw[5, e.RowIndex].Value));
             }

        }

        private void _rightDiscr_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rb = sender as RadioButton;
            if(rb == null) return;
            this._varDiscr.Checked = !rb.Checked;
            this._t1Discr.Enabled = rb.Checked;
            this._t0Discr.Enabled = rb.Checked;
        }

        private void MDOTEZConfigurationFormV14_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers != Keys.Control) return;
            switch (e.KeyCode)
            {
                case Keys.W:
                    this.WriteConfigurationToDevice();
                    break;
                case Keys.R:
                    this.LoadConfig();
                    break;
                case Keys.S:
                    this.SaveToFileConfiguration();
                    break;
                case Keys.O:
                    this.LoadFromFileConfiguration();
                    break;
            }
            e.Handled = true;
        }

        private void contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ((ContextMenuStrip)sender).Close();
            if (e.ClickedItem == this.readFromDeviceItem)
            {
                this.LoadConfig();
                return;
            }
            if (e.ClickedItem == this.writeToDeviceItem)
            {
                this.WriteConfigurationToDevice();
                return;
            }
            if (e.ClickedItem == this.readFromFileItem)
            {
                this.LoadFromFileConfiguration();
                return;
            }
            if (e.ClickedItem == this.writeToFileItem)
            {
                this.SaveToFileConfiguration();
            }
        }

        private void contextMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.readFromDeviceItem.Enabled =
                this.writeToDeviceItem.Enabled = this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode;
        }

        private void _confDgw_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show("Ошибка данных конфигурации");
        }
    }
}
