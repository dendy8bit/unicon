﻿namespace BEMN.TEZ.NewConfiguration
{
    partial class MDOVLSFormV14
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btn_Ok = new System.Windows.Forms.Button();
            this._relayInSystemGroupBox = new System.Windows.Forms.GroupBox();
            this._releMdoDataGridView = new System.Windows.Forms.DataGridView();
            this._numberMdoReleColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._typeMdoReleColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._pulsePeriodColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._signalMdoReleColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._relayOfflineGroupBox = new System.Windows.Forms.GroupBox();
            this._dataGridReleView = new System.Windows.Forms.DataGridView();
            this._extReleNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._extReleTypeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._pulseOfflinePeriodColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._sensor1Column = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._sensor2Column = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._sensor3Column = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.btn_Cansel = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.mdoLogicControl1 = new BEMN.TEZ.ControlsNewConfig.MDOLogicControl();
            this._relayInSystemGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._releMdoDataGridView)).BeginInit();
            this._relayOfflineGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._dataGridReleView)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_Ok
            // 
            this.btn_Ok.Location = new System.Drawing.Point(465, 333);
            this.btn_Ok.Name = "btn_Ok";
            this.btn_Ok.Size = new System.Drawing.Size(75, 23);
            this.btn_Ok.TabIndex = 0;
            this.btn_Ok.Text = "Принять";
            this.btn_Ok.UseVisualStyleBackColor = true;
            this.btn_Ok.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // _relayInSystemGroupBox
            // 
            this._relayInSystemGroupBox.Controls.Add(this._releMdoDataGridView);
            this._relayInSystemGroupBox.Location = new System.Drawing.Point(12, 12);
            this._relayInSystemGroupBox.Name = "_relayInSystemGroupBox";
            this._relayInSystemGroupBox.Size = new System.Drawing.Size(399, 145);
            this._relayInSystemGroupBox.TabIndex = 50;
            this._relayInSystemGroupBox.TabStop = false;
            this._relayInSystemGroupBox.Text = "Конфигурация реле при работе в системе";
            // 
            // _releMdoDataGridView
            // 
            this._releMdoDataGridView.AllowUserToAddRows = false;
            this._releMdoDataGridView.AllowUserToDeleteRows = false;
            this._releMdoDataGridView.AllowUserToResizeColumns = false;
            this._releMdoDataGridView.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.NullValue = null;
            this._releMdoDataGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this._releMdoDataGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._releMdoDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this._releMdoDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._numberMdoReleColumn,
            this._typeMdoReleColumn,
            this._pulsePeriodColumn,
            this._signalMdoReleColumn});
            this._releMdoDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this._releMdoDataGridView.GridColor = System.Drawing.SystemColors.ControlDarkDark;
            this._releMdoDataGridView.Location = new System.Drawing.Point(3, 16);
            this._releMdoDataGridView.Name = "_releMdoDataGridView";
            this._releMdoDataGridView.RowHeadersVisible = false;
            this._releMdoDataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._releMdoDataGridView.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this._releMdoDataGridView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._releMdoDataGridView.Size = new System.Drawing.Size(393, 126);
            this._releMdoDataGridView.TabIndex = 2;
            // 
            // _numberMdoReleColumn
            // 
            this._numberMdoReleColumn.HeaderText = "№";
            this._numberMdoReleColumn.MinimumWidth = 30;
            this._numberMdoReleColumn.Name = "_numberMdoReleColumn";
            this._numberMdoReleColumn.ReadOnly = true;
            this._numberMdoReleColumn.Width = 30;
            // 
            // _typeMdoReleColumn
            // 
            this._typeMdoReleColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._typeMdoReleColumn.HeaderText = "Тип";
            this._typeMdoReleColumn.MinimumWidth = 125;
            this._typeMdoReleColumn.Name = "_typeMdoReleColumn";
            this._typeMdoReleColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._typeMdoReleColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this._typeMdoReleColumn.Width = 125;
            // 
            // _pulsePeriodColumn
            // 
            this._pulsePeriodColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this._pulsePeriodColumn.HeaderText = "Тимп, мс";
            this._pulsePeriodColumn.MaxInputLength = 4;
            this._pulsePeriodColumn.Name = "_pulsePeriodColumn";
            this._pulsePeriodColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._pulsePeriodColumn.Width = 79;
            // 
            // _signalMdoReleColumn
            // 
            this._signalMdoReleColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._signalMdoReleColumn.HeaderText = "Сигнал";
            this._signalMdoReleColumn.MinimumWidth = 100;
            this._signalMdoReleColumn.Name = "_signalMdoReleColumn";
            this._signalMdoReleColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._signalMdoReleColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // _relayOfflineGroupBox
            // 
            this._relayOfflineGroupBox.Controls.Add(this._dataGridReleView);
            this._relayOfflineGroupBox.Location = new System.Drawing.Point(12, 179);
            this._relayOfflineGroupBox.Name = "_relayOfflineGroupBox";
            this._relayOfflineGroupBox.Size = new System.Drawing.Size(399, 145);
            this._relayOfflineGroupBox.TabIndex = 52;
            this._relayOfflineGroupBox.TabStop = false;
            this._relayOfflineGroupBox.Text = "Конфигурация реле в автономном режиме";
            // 
            // _dataGridReleView
            // 
            this._dataGridReleView.AllowUserToAddRows = false;
            this._dataGridReleView.AllowUserToDeleteRows = false;
            this._dataGridReleView.AllowUserToResizeColumns = false;
            this._dataGridReleView.AllowUserToResizeRows = false;
            dataGridViewCellStyle4.NullValue = null;
            this._dataGridReleView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this._dataGridReleView.BackgroundColor = System.Drawing.SystemColors.Control;
            this._dataGridReleView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._dataGridReleView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this._dataGridReleView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._dataGridReleView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._extReleNumberCol,
            this._extReleTypeCol,
            this._pulseOfflinePeriodColumn,
            this._sensor1Column,
            this._sensor2Column,
            this._sensor3Column});
            this._dataGridReleView.Dock = System.Windows.Forms.DockStyle.Fill;
            this._dataGridReleView.Location = new System.Drawing.Point(3, 16);
            this._dataGridReleView.Name = "_dataGridReleView";
            this._dataGridReleView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this._dataGridReleView.RowHeadersVisible = false;
            this._dataGridReleView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._dataGridReleView.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this._dataGridReleView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._dataGridReleView.Size = new System.Drawing.Size(393, 126);
            this._dataGridReleView.TabIndex = 2;
            // 
            // _extReleNumberCol
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._extReleNumberCol.DefaultCellStyle = dataGridViewCellStyle6;
            this._extReleNumberCol.HeaderText = "№";
            this._extReleNumberCol.Name = "_extReleNumberCol";
            this._extReleNumberCol.ReadOnly = true;
            this._extReleNumberCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._extReleNumberCol.Width = 30;
            // 
            // _extReleTypeCol
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._extReleTypeCol.DefaultCellStyle = dataGridViewCellStyle7;
            this._extReleTypeCol.HeaderText = "Тип";
            this._extReleTypeCol.Name = "_extReleTypeCol";
            this._extReleTypeCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // _pulseOfflinePeriodColumn
            // 
            this._pulseOfflinePeriodColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this._pulseOfflinePeriodColumn.HeaderText = "Тимп, мс";
            this._pulseOfflinePeriodColumn.MaxInputLength = 4;
            this._pulseOfflinePeriodColumn.Name = "_pulseOfflinePeriodColumn";
            this._pulseOfflinePeriodColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._pulseOfflinePeriodColumn.Width = 79;
            // 
            // _sensor1Column
            // 
            this._sensor1Column.HeaderText = "ДОК 1";
            this._sensor1Column.MinimumWidth = 30;
            this._sensor1Column.Name = "_sensor1Column";
            this._sensor1Column.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._sensor1Column.Width = 59;
            // 
            // _sensor2Column
            // 
            this._sensor2Column.HeaderText = "ДОК 2";
            this._sensor2Column.MinimumWidth = 30;
            this._sensor2Column.Name = "_sensor2Column";
            this._sensor2Column.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._sensor2Column.Width = 59;
            // 
            // _sensor3Column
            // 
            this._sensor3Column.HeaderText = "ДОК 3";
            this._sensor3Column.MinimumWidth = 30;
            this._sensor3Column.Name = "_sensor3Column";
            this._sensor3Column.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._sensor3Column.Width = 59;
            // 
            // btn_Cansel
            // 
            this.btn_Cansel.Location = new System.Drawing.Point(568, 333);
            this.btn_Cansel.Name = "btn_Cansel";
            this.btn_Cansel.Size = new System.Drawing.Size(75, 23);
            this.btn_Cansel.TabIndex = 53;
            this.btn_Cansel.Text = "Отмена";
            this.btn_Cansel.UseVisualStyleBackColor = true;
            this.btn_Cansel.Click += new System.EventHandler(this.btn_Cansel_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.mdoLogicControl1);
            this.groupBox1.Location = new System.Drawing.Point(417, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(263, 312);
            this.groupBox1.TabIndex = 54;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Выходные логические сигналы";
            // 
            // mdoLogicControl1
            // 
            this.mdoLogicControl1.Location = new System.Drawing.Point(3, 16);
            this.mdoLogicControl1.Name = "mdoLogicControl1";
            this.mdoLogicControl1.SignalsMDO = new ushort[] {
        ((ushort)(0)),
        ((ushort)(0)),
        ((ushort)(0)),
        ((ushort)(0))};
            this.mdoLogicControl1.Size = new System.Drawing.Size(257, 293);
            this.mdoLogicControl1.TabIndex = 1;
            // 
            // MDOVLSFormV14
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(690, 368);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btn_Cansel);
            this.Controls.Add(this._relayOfflineGroupBox);
            this.Controls.Add(this._relayInSystemGroupBox);
            this.Controls.Add(this.btn_Ok);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "MDOVLSFormV14";
            this.Text = "MDOVLS";
            this._relayInSystemGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._releMdoDataGridView)).EndInit();
            this._relayOfflineGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._dataGridReleView)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        #endregion

        private System.Windows.Forms.Button btn_Ok;
        private ControlsNewConfig.MDOLogicControl mdoLogicControl1;
        private System.Windows.Forms.GroupBox _relayInSystemGroupBox;
        private System.Windows.Forms.DataGridView _releMdoDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn _numberMdoReleColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _typeMdoReleColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _pulsePeriodColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _signalMdoReleColumn;
        private System.Windows.Forms.GroupBox _relayOfflineGroupBox;
        private System.Windows.Forms.DataGridView _dataGridReleView;
        private System.Windows.Forms.Button btn_Cansel;
        private System.Windows.Forms.DataGridViewTextBoxColumn _extReleNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _extReleTypeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _pulseOfflinePeriodColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _sensor1Column;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _sensor2Column;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _sensor3Column;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}