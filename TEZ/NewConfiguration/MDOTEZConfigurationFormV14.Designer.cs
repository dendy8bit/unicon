﻿using System.Drawing;
using System.Security.AccessControl;
using System.Windows.Forms;

namespace BEMN.TEZ.NewConfiguration
{
    partial class MDOTEZConfigurationFormV14
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this._openConfigurationDlg = new System.Windows.Forms.OpenFileDialog();
            this._saveConfigurationDlg = new System.Windows.Forms.SaveFileDialog();
            this._toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.logicSignalsControl1 = new BEMN.TEZ.NewConfiguration.LogicSignalsControlNew();
            this._discretGroupBox = new System.Windows.Forms.GroupBox();
            this._discretsDataGridView = new System.Windows.Forms.DataGridView();
            this._numberColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._typeColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._tezFaultCheckBox3 = new System.Windows.Forms.CheckBox();
            this._tezFaultCheckBox2 = new System.Windows.Forms.CheckBox();
            this._tezFaultCheckBox1 = new System.Windows.Forms.CheckBox();
            this.discretConfigGroup = new System.Windows.Forms.GroupBox();
            this._varDiscr = new System.Windows.Forms.RadioButton();
            this._constDiscr = new System.Windows.Forms.RadioButton();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this._t1Discr = new System.Windows.Forms.MaskedTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this._t0Discr = new System.Windows.Forms.MaskedTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._urovEnabled = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this._urovTextBox = new System.Windows.Forms.MaskedTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this._releGroupBox = new System.Windows.Forms.GroupBox();
            this._releDataGridView = new System.Windows.Forms.DataGridView();
            this._releNumberColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._releTypeColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._pulseTezRelayColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._releSignalColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._confDgw = new System.Windows.Forms.DataGridView();
            this._iStageColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewButtonColumn();
            this._statusStrip1 = new System.Windows.Forms.StatusStrip();
            this._toolStripProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this._toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this._btnSaveInFile = new System.Windows.Forms.Button();
            this._btnLoadFromFile = new System.Windows.Forms.Button();
            this._btnWriteConfig = new System.Windows.Forms.Button();
            this._btnReadConfig = new System.Windows.Forms.Button();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.readFromDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readFromFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this._discretGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._discretsDataGridView)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.discretConfigGroup.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this._releGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._releDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._confDgw)).BeginInit();
            this._statusStrip1.SuspendLayout();
            this.contextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // _openConfigurationDlg
            // 
            this._openConfigurationDlg.FileName = "Уставки ТЭЗ-24";
            this._openConfigurationDlg.Filter = "Файл уставок ТЭЗ-24 (*.xml)|*.xml";
            this._openConfigurationDlg.Title = "Загрузка файла уставок ТЭЗ-24";
            // 
            // _saveConfigurationDlg
            // 
            this._saveConfigurationDlg.FileName = "Уставки ТЭЗ-24";
            this._saveConfigurationDlg.Filter = "Файл уставок ТЭЗ-24 (*.xml)|*.xml";
            this._saveConfigurationDlg.Title = "Сохранение файла уставок ТЭЗ-24";
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this._confDgw);
            this.panel1.Controls.Add(this._statusStrip1);
            this.panel1.Controls.Add(this._btnSaveInFile);
            this.panel1.Controls.Add(this._btnLoadFromFile);
            this.panel1.Controls.Add(this._btnWriteConfig);
            this.panel1.Controls.Add(this._btnReadConfig);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1017, 624);
            this.panel1.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.groupBox4);
            this.groupBox3.Controls.Add(this._discretGroupBox);
            this.groupBox3.Controls.Add(this.groupBox2);
            this.groupBox3.Controls.Add(this.discretConfigGroup);
            this.groupBox3.Controls.Add(this.groupBox1);
            this.groupBox3.Controls.Add(this._releGroupBox);
            this.groupBox3.Location = new System.Drawing.Point(558, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(454, 541);
            this.groupBox3.TabIndex = 70;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Конфигурация ТЭЗ";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.logicSignalsControl1);
            this.groupBox4.Location = new System.Drawing.Point(220, 19);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(228, 265);
            this.groupBox4.TabIndex = 62;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Выходные логические сигналы";
            // 
            // logicSignalsControl1
            // 
            this.logicSignalsControl1.Location = new System.Drawing.Point(6, 19);
            this.logicSignalsControl1.Name = "logicSignalsControl1";
            this.logicSignalsControl1.Signals = new ushort[] {
        ((ushort)(0)),
        ((ushort)(0)),
        ((ushort)(0))};
            this.logicSignalsControl1.Size = new System.Drawing.Size(218, 244);
            this.logicSignalsControl1.TabIndex = 63;
            // 
            // _discretGroupBox
            // 
            this._discretGroupBox.Controls.Add(this._discretsDataGridView);
            this._discretGroupBox.Location = new System.Drawing.Point(6, 19);
            this._discretGroupBox.Name = "_discretGroupBox";
            this._discretGroupBox.Size = new System.Drawing.Size(208, 176);
            this._discretGroupBox.TabIndex = 58;
            this._discretGroupBox.TabStop = false;
            this._discretGroupBox.Text = "Дискретные входы";
            // 
            // _discretsDataGridView
            // 
            this._discretsDataGridView.AllowUserToAddRows = false;
            this._discretsDataGridView.AllowUserToDeleteRows = false;
            this._discretsDataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this._discretsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._discretsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._numberColumn,
            this._typeColumn});
            this._discretsDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this._discretsDataGridView.Location = new System.Drawing.Point(3, 16);
            this._discretsDataGridView.MultiSelect = false;
            this._discretsDataGridView.Name = "_discretsDataGridView";
            this._discretsDataGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this._discretsDataGridView.RowHeadersVisible = false;
            this._discretsDataGridView.RowHeadersWidth = 11;
            this._discretsDataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._discretsDataGridView.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._discretsDataGridView.Size = new System.Drawing.Size(202, 157);
            this._discretsDataGridView.TabIndex = 1;
            // 
            // _numberColumn
            // 
            this._numberColumn.HeaderText = "№";
            this._numberColumn.MinimumWidth = 30;
            this._numberColumn.Name = "_numberColumn";
            this._numberColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._numberColumn.Width = 30;
            // 
            // _typeColumn
            // 
            this._typeColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._typeColumn.HeaderText = "Тип";
            this._typeColumn.MinimumWidth = 50;
            this._typeColumn.Name = "_typeColumn";
            this._typeColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this._tezFaultCheckBox3);
            this.groupBox2.Controls.Add(this._tezFaultCheckBox2);
            this.groupBox2.Controls.Add(this._tezFaultCheckBox1);
            this.groupBox2.Location = new System.Drawing.Point(220, 290);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(208, 97);
            this.groupBox2.TabIndex = 60;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Конфигурация реле неисправности";
            // 
            // _tezFaultCheckBox3
            // 
            this._tezFaultCheckBox3.AutoSize = true;
            this._tezFaultCheckBox3.Location = new System.Drawing.Point(6, 74);
            this._tezFaultCheckBox3.Name = "_tezFaultCheckBox3";
            this._tezFaultCheckBox3.Size = new System.Drawing.Size(134, 17);
            this._tezFaultCheckBox3.TabIndex = 2;
            this._tezFaultCheckBox3.Text = "Неисправность МДО";
            this._tezFaultCheckBox3.UseVisualStyleBackColor = true;
            // 
            // _tezFaultCheckBox2
            // 
            this._tezFaultCheckBox2.AutoSize = true;
            this._tezFaultCheckBox2.Location = new System.Drawing.Point(6, 51);
            this._tezFaultCheckBox2.Name = "_tezFaultCheckBox2";
            this._tezFaultCheckBox2.Size = new System.Drawing.Size(176, 17);
            this._tezFaultCheckBox2.TabIndex = 1;
            this._tezFaultCheckBox2.Text = "Неисправность связи с МДО";
            this._tezFaultCheckBox2.UseVisualStyleBackColor = true;
            // 
            // _tezFaultCheckBox1
            // 
            this._tezFaultCheckBox1.AutoSize = true;
            this._tezFaultCheckBox1.Location = new System.Drawing.Point(6, 28);
            this._tezFaultCheckBox1.Name = "_tezFaultCheckBox1";
            this._tezFaultCheckBox1.Size = new System.Drawing.Size(144, 17);
            this._tezFaultCheckBox1.TabIndex = 0;
            this._tezFaultCheckBox1.Text = "Неисправность ТЭЗ-24";
            this._tezFaultCheckBox1.UseVisualStyleBackColor = true;
            // 
            // discretConfigGroup
            // 
            this.discretConfigGroup.Controls.Add(this._varDiscr);
            this.discretConfigGroup.Controls.Add(this._constDiscr);
            this.discretConfigGroup.Controls.Add(this.label5);
            this.discretConfigGroup.Controls.Add(this.label7);
            this.discretConfigGroup.Controls.Add(this.label3);
            this.discretConfigGroup.Controls.Add(this._t1Discr);
            this.discretConfigGroup.Controls.Add(this.label6);
            this.discretConfigGroup.Controls.Add(this._t0Discr);
            this.discretConfigGroup.Controls.Add(this.label4);
            this.discretConfigGroup.Location = new System.Drawing.Point(6, 201);
            this.discretConfigGroup.Name = "discretConfigGroup";
            this.discretConfigGroup.Size = new System.Drawing.Size(205, 99);
            this.discretConfigGroup.TabIndex = 61;
            this.discretConfigGroup.TabStop = false;
            this.discretConfigGroup.Text = "Тип и задержки дискретных входов";
            this.discretConfigGroup.Visible = false;
            // 
            // _varDiscr
            // 
            this._varDiscr.AutoSize = true;
            this._varDiscr.Checked = true;
            this._varDiscr.Location = new System.Drawing.Point(43, 14);
            this._varDiscr.Name = "_varDiscr";
            this._varDiscr.Size = new System.Drawing.Size(91, 17);
            this._varDiscr.TabIndex = 8;
            this._varDiscr.TabStop = true;
            this._varDiscr.Text = "Переменный";
            this._varDiscr.UseVisualStyleBackColor = true;
            // 
            // _constDiscr
            // 
            this._constDiscr.AutoSize = true;
            this._constDiscr.Location = new System.Drawing.Point(43, 32);
            this._constDiscr.Name = "_constDiscr";
            this._constDiscr.Size = new System.Drawing.Size(88, 17);
            this._constDiscr.TabIndex = 8;
            this._constDiscr.Text = "Постоянный";
            this._constDiscr.UseVisualStyleBackColor = true;
            this._constDiscr.CheckedChanged += new System.EventHandler(this._rightDiscr_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(26, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Тип";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label7.Location = new System.Drawing.Point(128, 57);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(21, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "мс";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(128, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "мс";
            // 
            // _t1Discr
            // 
            this._t1Discr.Enabled = false;
            this._t1Discr.Location = new System.Drawing.Point(62, 54);
            this._t1Discr.Mask = "000";
            this._t1Discr.Name = "_t1Discr";
            this._t1Discr.PromptChar = ' ';
            this._t1Discr.Size = new System.Drawing.Size(48, 20);
            this._t1Discr.TabIndex = 5;
            this._t1Discr.Tag = "1;65535";
            this._t1Discr.Text = "0";
            this._t1Discr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._t1Discr.TextChanged += new System.EventHandler(this._urovTextBox_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 57);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(20, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "T1";
            // 
            // _t0Discr
            // 
            this._t0Discr.Enabled = false;
            this._t0Discr.Location = new System.Drawing.Point(62, 73);
            this._t0Discr.Mask = "000";
            this._t0Discr.Name = "_t0Discr";
            this._t0Discr.PromptChar = ' ';
            this._t0Discr.Size = new System.Drawing.Size(48, 20);
            this._t0Discr.TabIndex = 5;
            this._t0Discr.Tag = "1;65535";
            this._t0Discr.Text = "0";
            this._t0Discr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._t0Discr.TextChanged += new System.EventHandler(this._urovTextBox_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 76);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(20, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "T0";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._urovEnabled);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this._urovTextBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(9, 306);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(205, 57);
            this.groupBox1.TabIndex = 61;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "УРОВ";
            // 
            // _urovEnabled
            // 
            this._urovEnabled.AutoSize = true;
            this._urovEnabled.Location = new System.Drawing.Point(47, 0);
            this._urovEnabled.Name = "_urovEnabled";
            this._urovEnabled.Size = new System.Drawing.Size(15, 14);
            this._urovEnabled.TabIndex = 7;
            this._urovEnabled.UseVisualStyleBackColor = true;
            this._urovEnabled.CheckedChanged += new System.EventHandler(this._urovEnabled_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(125, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "мс";
            // 
            // _urovTextBox
            // 
            this._urovTextBox.Enabled = false;
            this._urovTextBox.Location = new System.Drawing.Point(59, 26);
            this._urovTextBox.Mask = "0000";
            this._urovTextBox.Name = "_urovTextBox";
            this._urovTextBox.PromptChar = ' ';
            this._urovTextBox.Size = new System.Drawing.Size(48, 20);
            this._urovTextBox.TabIndex = 5;
            this._urovTextBox.Tag = "1;65535";
            this._urovTextBox.Text = "10";
            this._urovTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._urovTextBox.TextChanged += new System.EventHandler(this._urovTextBox_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Время";
            // 
            // _releGroupBox
            // 
            this._releGroupBox.Controls.Add(this._releDataGridView);
            this._releGroupBox.Location = new System.Drawing.Point(6, 425);
            this._releGroupBox.Name = "_releGroupBox";
            this._releGroupBox.Size = new System.Drawing.Size(363, 110);
            this._releGroupBox.TabIndex = 59;
            this._releGroupBox.TabStop = false;
            this._releGroupBox.Text = "Реле";
            // 
            // _releDataGridView
            // 
            this._releDataGridView.AllowUserToAddRows = false;
            this._releDataGridView.AllowUserToDeleteRows = false;
            this._releDataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this._releDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._releDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._releNumberColumn,
            this._releTypeColumn,
            this._pulseTezRelayColumn,
            this._releSignalColumn});
            this._releDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this._releDataGridView.Location = new System.Drawing.Point(3, 16);
            this._releDataGridView.MultiSelect = false;
            this._releDataGridView.Name = "_releDataGridView";
            this._releDataGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this._releDataGridView.RowHeadersVisible = false;
            this._releDataGridView.RowHeadersWidth = 11;
            this._releDataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._releDataGridView.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._releDataGridView.Size = new System.Drawing.Size(357, 91);
            this._releDataGridView.TabIndex = 1;
            this._releDataGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._releDataGridView_CellEndEdit);
            // 
            // _releNumberColumn
            // 
            this._releNumberColumn.HeaderText = "№";
            this._releNumberColumn.MinimumWidth = 30;
            this._releNumberColumn.Name = "_releNumberColumn";
            this._releNumberColumn.ReadOnly = true;
            this._releNumberColumn.Width = 30;
            // 
            // _releTypeColumn
            // 
            this._releTypeColumn.HeaderText = "Тип";
            this._releTypeColumn.Name = "_releTypeColumn";
            this._releTypeColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._releTypeColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // _pulseTezRelayColumn
            // 
            this._pulseTezRelayColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this._pulseTezRelayColumn.HeaderText = "Тимп(мс)";
            this._pulseTezRelayColumn.MaxInputLength = 4;
            this._pulseTezRelayColumn.Name = "_pulseTezRelayColumn";
            this._pulseTezRelayColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._pulseTezRelayColumn.Width = 79;
            // 
            // _releSignalColumn
            // 
            this._releSignalColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._releSignalColumn.HeaderText = "Сигнал";
            this._releSignalColumn.Name = "_releSignalColumn";
            this._releSignalColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // _confDgw
            // 
            this._confDgw.AllowUserToAddRows = false;
            this._confDgw.AllowUserToDeleteRows = false;
            this._confDgw.AllowUserToResizeColumns = false;
            this._confDgw.AllowUserToResizeRows = false;
            this._confDgw.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._confDgw.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this._confDgw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._confDgw.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._iStageColumn,
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7});
            this._confDgw.Location = new System.Drawing.Point(3, 3);
            this._confDgw.Name = "_confDgw";
            this._confDgw.RowHeadersVisible = false;
            this._confDgw.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._confDgw.RowTemplate.Height = 24;
            this._confDgw.ShowCellErrors = false;
            this._confDgw.ShowRowErrors = false;
            this._confDgw.Size = new System.Drawing.Size(549, 599);
            this._confDgw.TabIndex = 69;
            this._confDgw.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this._confDgw_CellContentClick);
            this._confDgw.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._confDgw_CellEndEdit);
            this._confDgw.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this._confDgw_DataError);
            // 
            // _iStageColumn
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
            this._iStageColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this._iStageColumn.HeaderText = "№";
            this._iStageColumn.Name = "_iStageColumn";
            this._iStageColumn.ReadOnly = true;
            this._iStageColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._iStageColumn.Width = 30;
            // 
            // Column1
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Column1.DefaultCellStyle = dataGridViewCellStyle3;
            this.Column1.HeaderText = "Тип";
            this.Column1.Name = "Column1";
            this.Column1.Width = 80;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "ДОК 1";
            this.Column2.Name = "Column2";
            this.Column2.Width = 46;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "ДОК 2";
            this.Column3.Name = "Column3";
            this.Column3.Width = 46;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "ДОК 3";
            this.Column4.Name = "Column4";
            this.Column4.Width = 46;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Тест ДОК";
            this.Column5.Name = "Column5";
            this.Column5.Width = 64;
            // 
            // Column6
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.Column6.DefaultCellStyle = dataGridViewCellStyle4;
            this.Column6.FillWeight = 500F;
            this.Column6.HeaderText = "Период теста, с";
            this.Column6.Name = "Column6";
            this.Column6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column7
            // 
            this.Column7.HeaderText = "Выходные сигналы";
            this.Column7.Name = "Column7";
            this.Column7.Width = 133;
            // 
            // _statusStrip1
            // 
            this._statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._toolStripProgressBar,
            this._toolStripStatusLabel1});
            this._statusStrip1.Location = new System.Drawing.Point(0, 602);
            this._statusStrip1.Name = "_statusStrip1";
            this._statusStrip1.Size = new System.Drawing.Size(1017, 22);
            this._statusStrip1.TabIndex = 68;
            this._statusStrip1.Text = "statusStrip1";
            // 
            // _toolStripProgressBar
            // 
            this._toolStripProgressBar.Name = "_toolStripProgressBar";
            this._toolStripProgressBar.Size = new System.Drawing.Size(100, 16);
            // 
            // _toolStripStatusLabel1
            // 
            this._toolStripStatusLabel1.Name = "_toolStripStatusLabel1";
            this._toolStripStatusLabel1.Size = new System.Drawing.Size(16, 17);
            this._toolStripStatusLabel1.Text = "...";
            // 
            // _btnSaveInFile
            // 
            this._btnSaveInFile.Location = new System.Drawing.Point(839, 547);
            this._btnSaveInFile.Name = "_btnSaveInFile";
            this._btnSaveInFile.Size = new System.Drawing.Size(143, 23);
            this._btnSaveInFile.TabIndex = 67;
            this._btnSaveInFile.Text = "Сохранить в файл";
            this._btnSaveInFile.UseVisualStyleBackColor = true;
            this._btnSaveInFile.Click += new System.EventHandler(this._btnSaveInFile_Click);
            // 
            // _btnLoadFromFile
            // 
            this._btnLoadFromFile.Location = new System.Drawing.Point(839, 576);
            this._btnLoadFromFile.Name = "_btnLoadFromFile";
            this._btnLoadFromFile.Size = new System.Drawing.Size(143, 23);
            this._btnLoadFromFile.TabIndex = 66;
            this._btnLoadFromFile.Text = "Загрузить из файла";
            this._btnLoadFromFile.UseVisualStyleBackColor = true;
            this._btnLoadFromFile.Click += new System.EventHandler(this._btnLoadFromFile_Click);
            // 
            // _btnWriteConfig
            // 
            this._btnWriteConfig.Location = new System.Drawing.Point(558, 576);
            this._btnWriteConfig.Name = "_btnWriteConfig";
            this._btnWriteConfig.Size = new System.Drawing.Size(143, 23);
            this._btnWriteConfig.TabIndex = 65;
            this._btnWriteConfig.Text = "Записать в устройство";
            this._btnWriteConfig.UseVisualStyleBackColor = true;
            this._btnWriteConfig.Click += new System.EventHandler(this._btnWriteConfig_Click);
            // 
            // _btnReadConfig
            // 
            this._btnReadConfig.Location = new System.Drawing.Point(558, 547);
            this._btnReadConfig.Name = "_btnReadConfig";
            this._btnReadConfig.Size = new System.Drawing.Size(143, 23);
            this._btnReadConfig.TabIndex = 64;
            this._btnReadConfig.Text = "Прочитать из устройства";
            this._btnReadConfig.UseVisualStyleBackColor = true;
            this._btnReadConfig.Click += new System.EventHandler(this._btnReadConfig_Click);
            // 
            // contextMenu
            // 
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.readFromDeviceItem,
            this.writeToDeviceItem,
            this.writeToFileItem,
            this.readFromFileItem});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(213, 92);
            this.contextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenu_Opening);
            this.contextMenu.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.contextMenu_ItemClicked);
            // 
            // readFromDeviceItem
            // 
            this.readFromDeviceItem.Name = "readFromDeviceItem";
            this.readFromDeviceItem.Size = new System.Drawing.Size(212, 22);
            this.readFromDeviceItem.Text = "Прочитать из устройства";
            // 
            // writeToDeviceItem
            // 
            this.writeToDeviceItem.Name = "writeToDeviceItem";
            this.writeToDeviceItem.Size = new System.Drawing.Size(212, 22);
            this.writeToDeviceItem.Text = "Записать в устройство";
            // 
            // writeToFileItem
            // 
            this.writeToFileItem.Name = "writeToFileItem";
            this.writeToFileItem.Size = new System.Drawing.Size(212, 22);
            this.writeToFileItem.Text = "Сохранитьт в файл";
            // 
            // readFromFileItem
            // 
            this.readFromFileItem.Name = "readFromFileItem";
            this.readFromFileItem.Size = new System.Drawing.Size(212, 22);
            this.readFromFileItem.Text = "Загрузить из файла";
            // 
            // MDOTEZConfigurationFormV14
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(1017, 624);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(16, 662);
            this.Name = "MDOTEZConfigurationFormV14";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MDOConfigurationForm_Closing);
            this.Load += new System.EventHandler(this.MDOConfigurationForm_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MDOTEZConfigurationFormV14_KeyUp);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this._discretGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._discretsDataGridView)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.discretConfigGroup.ResumeLayout(false);
            this.discretConfigGroup.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this._releGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._releDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._confDgw)).EndInit();
            this._statusStrip1.ResumeLayout(false);
            this._statusStrip1.PerformLayout();
            this.contextMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }


        #endregion

        private System.Windows.Forms.SaveFileDialog _saveConfigurationDlg;
        private System.Windows.Forms.OpenFileDialog _openConfigurationDlg;
        private ToolTip _toolTip;
        private Panel panel1;
        private GroupBox groupBox3;
        private GroupBox groupBox4;
        private LogicSignalsControlNew logicSignalsControl1;
        private GroupBox _discretGroupBox;
        private DataGridView _discretsDataGridView;
        private DataGridViewTextBoxColumn _numberColumn;
        private DataGridViewComboBoxColumn _typeColumn;
        private GroupBox groupBox2;
        private CheckBox _tezFaultCheckBox3;
        private CheckBox _tezFaultCheckBox2;
        private CheckBox _tezFaultCheckBox1;
        private GroupBox groupBox1;
        private CheckBox _urovEnabled;
        private Label label1;
        private MaskedTextBox _urovTextBox;
        private Label label2;
        private GroupBox _releGroupBox;
        private DataGridView _releDataGridView;
        private DataGridViewTextBoxColumn _releNumberColumn;
        private DataGridViewComboBoxColumn _releTypeColumn;
        private DataGridViewTextBoxColumn _pulseTezRelayColumn;
        private DataGridViewComboBoxColumn _releSignalColumn;
        private DataGridView _confDgw;
        private DataGridViewTextBoxColumn _iStageColumn;
        private DataGridViewComboBoxColumn Column1;
        private DataGridViewCheckBoxColumn Column2;
        private DataGridViewCheckBoxColumn Column3;
        private DataGridViewCheckBoxColumn Column4;
        private DataGridViewCheckBoxColumn Column5;
        private DataGridViewTextBoxColumn Column6;
        private DataGridViewButtonColumn Column7;
        private StatusStrip _statusStrip1;
        private ToolStripProgressBar _toolStripProgressBar;
        private ToolStripStatusLabel _toolStripStatusLabel1;
        private Button _btnSaveInFile;
        private Button _btnLoadFromFile;
        private Button _btnWriteConfig;
        private Button _btnReadConfig;
        private GroupBox discretConfigGroup;
        private RadioButton _varDiscr;
        private RadioButton _constDiscr;
        private Label label5;
        private Label label7;
        private Label label3;
        private MaskedTextBox _t1Discr;
        private Label label6;
        private MaskedTextBox _t0Discr;
        private Label label4;
        private ContextMenuStrip contextMenu;
        private ToolStripMenuItem readFromDeviceItem;
        private ToolStripMenuItem writeToDeviceItem;
        private ToolStripMenuItem writeToFileItem;
        private ToolStripMenuItem readFromFileItem;
    }
}

