﻿namespace ITKZ
{
    partial class ItkzMeasuringForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._statusLed8 = new BEMN.Forms.LedControl();
            this._statusLed7 = new BEMN.Forms.LedControl();
            this._statusLed6 = new BEMN.Forms.LedControl();
            this._statusLed5 = new BEMN.Forms.LedControl();
            this.label5 = new System.Windows.Forms.Label();
            this._statusLed4 = new BEMN.Forms.LedControl();
            this.label4 = new System.Windows.Forms.Label();
            this._statusLed3 = new BEMN.Forms.LedControl();
            this._statusLed2 = new BEMN.Forms.LedControl();
            this.label2 = new System.Windows.Forms.Label();
            this._statusLed1 = new BEMN.Forms.LedControl();
            this.label1 = new System.Windows.Forms.Label();
            this._curFazaC = new System.Windows.Forms.TextBox();
            this._curFazaB = new System.Windows.Forms.TextBox();
            this._curFazaA = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this._lastFazaC = new System.Windows.Forms.TextBox();
            this._lastFazaB = new System.Windows.Forms.TextBox();
            this._lastFazaA = new System.Windows.Forms.TextBox();
            this._maxFazaC = new System.Windows.Forms.TextBox();
            this._maxFazaB = new System.Windows.Forms.TextBox();
            this._maxFazaA = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.KvitBtn = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _statusLed8
            // 
            this._statusLed8.Location = new System.Drawing.Point(133, 131);
            this._statusLed8.Name = "_statusLed8";
            this._statusLed8.Size = new System.Drawing.Size(13, 13);
            this._statusLed8.State = BEMN.Forms.LedState.Off;
            this._statusLed8.TabIndex = 13;
            this._statusLed8.Visible = false;
            // 
            // _statusLed7
            // 
            this._statusLed7.Location = new System.Drawing.Point(133, 131);
            this._statusLed7.Name = "_statusLed7";
            this._statusLed7.Size = new System.Drawing.Size(13, 13);
            this._statusLed7.State = BEMN.Forms.LedState.Off;
            this._statusLed7.TabIndex = 12;
            this._statusLed7.Visible = false;
            // 
            // _statusLed6
            // 
            this._statusLed6.Location = new System.Drawing.Point(133, 131);
            this._statusLed6.Name = "_statusLed6";
            this._statusLed6.Size = new System.Drawing.Size(13, 13);
            this._statusLed6.State = BEMN.Forms.LedState.Off;
            this._statusLed6.TabIndex = 11;
            this._statusLed6.Visible = false;
            // 
            // _statusLed5
            // 
            this._statusLed5.Location = new System.Drawing.Point(133, 131);
            this._statusLed5.Name = "_statusLed5";
            this._statusLed5.Size = new System.Drawing.Size(13, 13);
            this._statusLed5.State = BEMN.Forms.LedState.Off;
            this._statusLed5.TabIndex = 10;
            this._statusLed5.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 149);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(121, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "(состояние контактов)";
            // 
            // _statusLed4
            // 
            this._statusLed4.Location = new System.Drawing.Point(93, 131);
            this._statusLed4.Name = "_statusLed4";
            this._statusLed4.Size = new System.Drawing.Size(13, 13);
            this._statusLed4.State = BEMN.Forms.LedState.Off;
            this._statusLed4.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 131);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Реле аварии";
            // 
            // _statusLed3
            // 
            this._statusLed3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this._statusLed3.Location = new System.Drawing.Point(81, 91);
            this._statusLed3.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._statusLed3.Name = "_statusLed3";
            this._statusLed3.Size = new System.Drawing.Size(13, 13);
            this._statusLed3.State = BEMN.Forms.LedState.Off;
            this._statusLed3.TabIndex = 6;
            // 
            // _statusLed2
            // 
            this._statusLed2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this._statusLed2.Location = new System.Drawing.Point(81, 63);
            this._statusLed2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._statusLed2.Name = "_statusLed2";
            this._statusLed2.Size = new System.Drawing.Size(13, 13);
            this._statusLed2.State = BEMN.Forms.LedState.Off;
            this._statusLed2.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 35);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(16, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Ia";
            // 
            // _statusLed1
            // 
            this._statusLed1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this._statusLed1.Location = new System.Drawing.Point(81, 35);
            this._statusLed1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._statusLed1.Name = "_statusLed1";
            this._statusLed1.Size = new System.Drawing.Size(13, 13);
            this._statusLed1.State = BEMN.Forms.LedState.Off;
            this._statusLed1.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Фазы";
            // 
            // _curFazaC
            // 
            this._curFazaC.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this._curFazaC.Location = new System.Drawing.Point(157, 88);
            this._curFazaC.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._curFazaC.Name = "_curFazaC";
            this._curFazaC.ReadOnly = true;
            this._curFazaC.Size = new System.Drawing.Size(55, 20);
            this._curFazaC.TabIndex = 5;
            // 
            // _curFazaB
            // 
            this._curFazaB.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this._curFazaB.Location = new System.Drawing.Point(157, 60);
            this._curFazaB.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._curFazaB.Name = "_curFazaB";
            this._curFazaB.ReadOnly = true;
            this._curFazaB.Size = new System.Drawing.Size(55, 20);
            this._curFazaB.TabIndex = 3;
            // 
            // _curFazaA
            // 
            this._curFazaA.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this._curFazaA.Location = new System.Drawing.Point(157, 32);
            this._curFazaA.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._curFazaA.Name = "_curFazaA";
            this._curFazaA.ReadOnly = true;
            this._curFazaA.Size = new System.Drawing.Size(55, 20);
            this._curFazaA.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(55, 2);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Состояние";
            // 
            // _lastFazaC
            // 
            this._lastFazaC.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this._lastFazaC.Location = new System.Drawing.Point(290, 88);
            this._lastFazaC.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._lastFazaC.Name = "_lastFazaC";
            this._lastFazaC.ReadOnly = true;
            this._lastFazaC.Size = new System.Drawing.Size(55, 20);
            this._lastFazaC.TabIndex = 5;
            // 
            // _lastFazaB
            // 
            this._lastFazaB.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this._lastFazaB.Location = new System.Drawing.Point(290, 60);
            this._lastFazaB.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._lastFazaB.Name = "_lastFazaB";
            this._lastFazaB.ReadOnly = true;
            this._lastFazaB.Size = new System.Drawing.Size(55, 20);
            this._lastFazaB.TabIndex = 3;
            // 
            // _lastFazaA
            // 
            this._lastFazaA.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this._lastFazaA.Location = new System.Drawing.Point(290, 32);
            this._lastFazaA.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._lastFazaA.Name = "_lastFazaA";
            this._lastFazaA.ReadOnly = true;
            this._lastFazaA.Size = new System.Drawing.Size(55, 20);
            this._lastFazaA.TabIndex = 1;
            // 
            // _maxFazaC
            // 
            this._maxFazaC.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this._maxFazaC.Location = new System.Drawing.Point(422, 88);
            this._maxFazaC.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._maxFazaC.Name = "_maxFazaC";
            this._maxFazaC.ReadOnly = true;
            this._maxFazaC.Size = new System.Drawing.Size(55, 20);
            this._maxFazaC.TabIndex = 5;
            // 
            // _maxFazaB
            // 
            this._maxFazaB.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this._maxFazaB.Location = new System.Drawing.Point(422, 60);
            this._maxFazaB.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._maxFazaB.Name = "_maxFazaB";
            this._maxFazaB.ReadOnly = true;
            this._maxFazaB.Size = new System.Drawing.Size(55, 20);
            this._maxFazaB.TabIndex = 3;
            // 
            // _maxFazaA
            // 
            this._maxFazaA.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this._maxFazaA.Location = new System.Drawing.Point(422, 32);
            this._maxFazaA.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._maxFazaA.Name = "_maxFazaA";
            this._maxFazaA.ReadOnly = true;
            this._maxFazaA.Size = new System.Drawing.Size(55, 20);
            this._maxFazaA.TabIndex = 1;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Inset;
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.833164F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.54489F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.76348F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28.98553F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 22.87294F));
            this.tableLayoutPanel1.Controls.Add(this._maxFazaC, 4, 3);
            this.tableLayoutPanel1.Controls.Add(this.label8, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this._lastFazaC, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.label7, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this._curFazaC, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this._maxFazaB, 4, 2);
            this.tableLayoutPanel1.Controls.Add(this._lastFazaB, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this._maxFazaA, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.label15, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label3, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this._curFazaB, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.label6, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this._lastFazaA, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.label16, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this._statusLed1, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this._curFazaA, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this._statusLed3, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this._statusLed2, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.5102F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.5102F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.5102F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 23.46939F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(509, 113);
            this.tableLayoutPanel1.TabIndex = 8;
            // 
            // label15
            // 
            this.label15.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(18, 63);
            this.label15.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(16, 13);
            this.label15.TabIndex = 4;
            this.label15.Text = "Ib";
            // 
            // label16
            // 
            this.label16.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(18, 91);
            this.label16.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(16, 13);
            this.label16.TabIndex = 5;
            this.label16.Text = "Ic";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(129, 2);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Текущ. значение, А";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(249, 2);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(132, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "При послед. сработке, А";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(395, 2);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "Макс. значение, А";
            // 
            // KvitBtn
            // 
            this.KvitBtn.Location = new System.Drawing.Point(417, 131);
            this.KvitBtn.Name = "KvitBtn";
            this.KvitBtn.Size = new System.Drawing.Size(90, 26);
            this.KvitBtn.TabIndex = 0;
            this.KvitBtn.Text = "Квитирование";
            this.KvitBtn.UseVisualStyleBackColor = true;
            this.KvitBtn.Click += new System.EventHandler(this.KvitBtn_Click);
            // 
            // ItkzMeasuringForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(533, 171);
            this.Controls.Add(this.KvitBtn);
            this.Controls.Add(this._statusLed8);
            this.Controls.Add(this._statusLed7);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this._statusLed6);
            this.Controls.Add(this._statusLed5);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this._statusLed4);
            this.Name = "ItkzMeasuringForm";
            this.Text = "Measuring";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ItkzMeasuringForm_FormClosing);
            this.Load += new System.EventHandler(this.ItkzMeasuringForm_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private BEMN.Forms.LedControl _statusLed8;
        private BEMN.Forms.LedControl _statusLed7;
        private BEMN.Forms.LedControl _statusLed6;
        private BEMN.Forms.LedControl _statusLed5;
        private System.Windows.Forms.Label label5;
        private BEMN.Forms.LedControl _statusLed4;
        private System.Windows.Forms.Label label4;
        private BEMN.Forms.LedControl _statusLed3;
        private BEMN.Forms.LedControl _statusLed2;
        private System.Windows.Forms.Label label2;
        private BEMN.Forms.LedControl _statusLed1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox _curFazaC;
        private System.Windows.Forms.TextBox _curFazaB;
        private System.Windows.Forms.TextBox _curFazaA;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox _lastFazaC;
        private System.Windows.Forms.TextBox _lastFazaB;
        private System.Windows.Forms.TextBox _lastFazaA;
        private System.Windows.Forms.TextBox _maxFazaC;
        private System.Windows.Forms.TextBox _maxFazaB;
        private System.Windows.Forms.TextBox _maxFazaA;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button KvitBtn;
    }
}