﻿using System;
using System.Xml.Serialization;
using System.ComponentModel;
using System.Drawing;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Interfaces;
using BEMN.MBServer;
using ITKZ.Structures;

namespace ITKZ
{
    public class Itkz : Device, IDeviceView
    {
        #region Fields
        private MemoryEntity<UstavkiStruct> _ustavki;
        private MemoryEntity<ConfigStruct> _configStruct;
        private MemoryEntity<OneWordStruct> _accept;
        private MemoryEntity<ItkzMeasuringStruct> _measuring;
        private MemoryEntity<OneWordStruct> _kvit; 
        #endregion

        #region Constructor
        public Itkz()
        {
            HaveVersion = false;
        }

        public Itkz(Modbus mb)
        {
            HaveVersion = false;
            MB = mb;
            this._configStruct = new MemoryEntity<ConfigStruct>("Конфигурация усройства", this, 0x0000);
            this._accept = new MemoryEntity<OneWordStruct>("Подтверждение изменения конфигурации устройства", this, 0x0001);
            this._ustavki = new MemoryEntity<UstavkiStruct>("ИТКЗ уставки", this, 0x0002);
            this._measuring = new MemoryEntity<ItkzMeasuringStruct>("ИТКЗ измерения", this, 0x0005);
            this._kvit = new MemoryEntity<OneWordStruct>("Квитирование", this, 0x0001);
        }
        
        #endregion

        #region Properties

        public MemoryEntity<UstavkiStruct> Ustavki
        {
            get { return this._ustavki; }
        }

        public MemoryEntity<ConfigStruct> Config
        {
            get { return this._configStruct; }
        }

        public MemoryEntity<OneWordStruct> Accept
        {
            get { return this._accept; }
        }

        public MemoryEntity<ItkzMeasuringStruct> Measuring
        {
            get { return this._measuring; }
        }

        public MemoryEntity<OneWordStruct> Kvitirovanie
        {
            get { return this._kvit; }
        }
        #endregion

        #region INodeView Members

        [XmlIgnore]
        [Browsable(false)]
        public Type ClassType
        {
            get { return typeof(Itkz); }
        }
        [XmlIgnore]
        [Browsable(false)]
        public bool ForceShow
        {
            get { return false; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public Image NodeImage
        {
            get { return BEMN.Framework.Properties.Resources.itkz; }
        }

        [Browsable(false)]
        public string NodeName => "ИТКЗ исполнение 1"; 

        [XmlIgnore]
        [Browsable(false)]
        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }
        [XmlIgnore]
        [Browsable(false)]
        public bool Deletable
        {
            get { return true; }
        }

        #endregion
    }
}
