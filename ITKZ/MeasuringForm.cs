﻿using System;
using System.Drawing;
using System.Windows.Forms;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms;
using BEMN.Interfaces;
using BEMN.MBServer;

namespace ITKZ
{
    public partial class ItkzMeasuringForm : Form, IFormView
    {
        private Itkz _device;
        private MemoryEntity<OneWordStruct> _kvitirovanie;
        private LedControl[] _statusLeds;

        public ItkzMeasuringForm()
        {
            this.InitializeComponent();
        }

        public ItkzMeasuringForm(Itkz device)
        {
            this.InitializeComponent();
            this._device = device;
            this._device.ConnectionModeChanged += this.StartStopLoad;
            this._kvitirovanie = this._device.Kvitirovanie;
            this._kvitirovanie.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
                 MessageBox.Show("Квитирование завершено", "Квитирование", MessageBoxButtons.OK, MessageBoxIcon.Information));
            this._kvitirovanie.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                MessageBox.Show("Невозможно провести квитирование", "Квитирование", MessageBoxButtons.OK, MessageBoxIcon.Information));

            this._device.Measuring.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.MeasuringReadOk);
            this._device.Measuring.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.MeasuringReadFail);

            this._statusLeds = new LedControl[]
            {
                this._statusLed1, this._statusLed2, this._statusLed3, this._statusLed4, this._statusLed5,
                this._statusLed6, this._statusLed7, this._statusLed8
            };
        }

        private void MeasuringReadOk()
        {
            LedManager.SetLeds(this._statusLeds, this._device.Measuring.Value.Status);
            this._curFazaA.Text = this._device.Measuring.Value.IaValue.ToString();
            this._curFazaB.Text = this._device.Measuring.Value.IbValue.ToString();
            this._curFazaC.Text = this._device.Measuring.Value.IcValue.ToString();
            this._lastFazaA.Text = this._device.Measuring.Value.IaLastexcessValue.ToString();
            this._lastFazaB.Text = this._device.Measuring.Value.IbLastexcessValue.ToString();
            this._lastFazaC.Text = this._device.Measuring.Value.IcLastexcessValue.ToString();
            this._maxFazaA.Text = this._device.Measuring.Value.IaMaxValue.ToString();
            this._maxFazaB.Text = this._device.Measuring.Value.IbMaxValue.ToString();
            this._maxFazaC.Text = this._device.Measuring.Value.IcMaxValue.ToString();
        }

        private void MeasuringReadFail()
        {
            LedManager.TurnOffLeds(this._statusLeds);
            this._curFazaA.Text = string.Empty;
            this._curFazaB.Text = string.Empty;
            this._curFazaC.Text = string.Empty;
            this._lastFazaA.Text = string.Empty;
            this._lastFazaB.Text = string.Empty;
            this._lastFazaC.Text = string.Empty;
            this._maxFazaA.Text = string.Empty;
            this._maxFazaB.Text = string.Empty;
            this._maxFazaC.Text = string.Empty;
        }

        private void ItkzMeasuringForm_Load(object sender, EventArgs e)
        {
            this.StartStopLoad();
        }

        private void ItkzMeasuringForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._device.Measuring.RemoveStructQueries();
            
        }

        private void StartStopLoad()
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._device.Measuring.LoadStructCycle();
            }
            else
            {
                this._device.Measuring.RemoveStructQueries();
                this.MeasuringReadFail();
            }
        }

        private void KvitBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show("Провести квитирование?", "Квитирование", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                            != DialogResult.Yes) return;
            this._kvitirovanie.Value.Word = 0xAAAA;
            this._kvitirovanie.SaveStruct();
        }

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(Itkz); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(ItkzMeasuringForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return ITKZ.Properties.Resources.measuring.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Измерения"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion
    }
}
