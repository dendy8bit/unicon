﻿using System.Collections.Generic;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace ITKZ.Structures
{
    public class ConfigStruct : StructBase
    {
        [Layout(0)]
        private ushort _config;

        [BindingProperty(0)]
        public byte DvNum
        {
            get { return Common.LOBYTE(_config); }
            set { _config = Common.TOWORD(Common.HIBYTE(_config), value); }
        }

        [BindingProperty(1)]
        public string BaundRate
        {
            get { return Validator.Get(_config, Rate, 8); }
            set { _config = Validator.Set(value, Rate, _config, 8); }
        }

        public static List<string> Rate
        {
            get
            {
                return new List<string>
                {
                    "115200",
                    "19200"
                };
            }

        }
    }
}