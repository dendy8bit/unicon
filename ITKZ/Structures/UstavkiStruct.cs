﻿using System;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace ITKZ.Structures
{
    public class UstavkiStruct : StructBase
    {
        [Layout(0)]
        private ushort _iaLimit;
        [Layout(1)]
        private ushort _ibLimit;
        [Layout(2)]
        private ushort _icLimit;

        [XmlElement(ElementName = "Уставка_фазы_А")]
        [BindingProperty(0)]
        public double IaLimit
        {
            get { return (double)_iaLimit / 100; }
            set { _iaLimit = (ushort)Math.Round(value * 100); }
        }

        [XmlElement(ElementName = "Уставка_фазы_B")]
        [BindingProperty(1)]
        public double IbLimit
        {
            get { return (double)_ibLimit / 100; }
            set { _ibLimit = (ushort)Math.Round(value * 100); }
        }

        [XmlElement(ElementName = "Уставка_фазы_C")]
        [BindingProperty(2)]
        public double IcLimit
        {
            get { return (double)_icLimit / 100; }
            set { _icLimit = (ushort)Math.Round(value * 100); }
        }
    }
}
