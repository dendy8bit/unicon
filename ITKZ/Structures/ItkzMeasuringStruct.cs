﻿using System.Collections;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace ITKZ.Structures
{
    public class ItkzMeasuringStruct : StructBase
    {
        [Layout(0)] private ushort _status;
        [Layout(1)] private ushort _iaValue;
        [Layout(2)] private ushort _ibValue;
        [Layout(3)] private ushort _icValue;
        [Layout(4)] private ushort _iaLastexcessValue;
        [Layout(5)] private ushort _ibLastexcessValue;
        [Layout(6)] private ushort _icLastexcessValue;
        [Layout(7)] private ushort _iaMaxValue;
        [Layout(8)] private ushort _ibMaxValue;
        [Layout(9)] private ushort _icMaxValue;

        public BitArray Status
        {
            get
            {
                BitArray array = new BitArray(new byte[] { Common.LOBYTE(_status) });
                array[3] = !array[3];
                return array;
            }
        }

        public ushort IaValue
        {
            get { return _iaValue; }
        }

        public ushort IbValue
        {
            get { return _ibValue; }
        }

        public ushort IcValue
        {
            get { return _icValue; }
        }

        public ushort IaLastexcessValue
        {
            get { return _iaLastexcessValue; }
        }

        public ushort IbLastexcessValue
        {
            get { return _ibLastexcessValue; }
        }

        public ushort IcLastexcessValue
        {
            get { return _icLastexcessValue; }
        }

        public ushort IaMaxValue
        {
            get { return _iaMaxValue; }
        }

        public ushort IbMaxValue
        {
            get { return _ibMaxValue; }
        }

        public ushort IcMaxValue
        {
            get { return _iaMaxValue; }
        }
    }
}
