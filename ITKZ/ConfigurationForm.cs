﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Xml;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms.ValidatingClasses.New;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Forms.ValidatingClasses.Rules.Byte;
using BEMN.Forms.ValidatingClasses.Rules.Double;
using BEMN.Interfaces;
using BEMN.MBServer;
using ITKZ.Structures;

namespace ITKZ
{
    public partial class ItkzConfigurationForm : Form, IFormView
    {
        #region Feilds
        private Itkz _device;
        private readonly NewStructValidator<ConfigStruct> _devNumValidator;
        private readonly NewStructValidator<UstavkiStruct> _ustavkiValidator;
        #endregion

        #region Constructor
        public ItkzConfigurationForm()
        {
            InitializeComponent();
        }

        public ItkzConfigurationForm(Itkz device)
        {
            InitializeComponent();
            _device = device;

            _device.Config.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, ReadDevNumOk);
            _device.Config.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _device.Accept.Value.Word = 0x5555;
                _device.Accept.SaveStruct();
            });
            _device.Config.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                MessageBox.Show("Ошибка чтения номера устройства", "Чтение", MessageBoxButtons.OK, MessageBoxIcon.Error));
            _device.Config.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                MessageBox.Show("Ошибка записи номера устройства", "Запись", MessageBoxButtons.OK, MessageBoxIcon.Error));

            _device.Accept.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                MessageBox.Show(
                    "Запись нового номера устройства прошла успешно.\nЧтобы изменения вступили в силу, требуется отключить питание",
                    "Запись номера устройства", MessageBoxButtons.OK, MessageBoxIcon.Information);
            });
            _device.Accept.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                MessageBox.Show("Ошибка записи номера устройства", "Запись номера устройства",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            });

            _device.Ustavki.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, ReadConfigOk);
            _device.Ustavki.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
                MessageBox.Show("Конфигурация записана", "Запись", MessageBoxButtons.OK, MessageBoxIcon.Information));
            _device.Ustavki.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                MessageBox.Show("Ошибка чтения конфигурации", "Чтение", MessageBoxButtons.OK, MessageBoxIcon.Error));
            _device.Ustavki.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                MessageBox.Show("Ошибка записи конфигурации", "Запись", MessageBoxButtons.OK, MessageBoxIcon.Error));

            ToolTip toolTip = new ToolTip();
            _devNumValidator = new NewStructValidator<ConfigStruct>
                (
                    toolTip,
                    new ControlInfoText(_devNumBox, new CustomByteRule(1, 247)),
                    new ControlInfoCombo(_baundRateCmb, ConfigStruct.Rate)
                );
            _ustavkiValidator = new NewStructValidator<UstavkiStruct>
                (
                toolTip,
                new ControlInfoText(_fazaA, new CustomDoubleRule(0.05, 1)),
                new ControlInfoText(_fazaB, new CustomDoubleRule(0.05, 1)),
                new ControlInfoText(_fazaC, new CustomDoubleRule(0.05, 1))
                );
        }
        #endregion

        #region Handlers
        private void ReadDevNumOk()
        {
            _devNumValidator.Set(_device.Config.Value);
        }

        private void ReadConfigOk()
        {
            _ustavkiValidator.Set(_device.Ustavki.Value);
        }

        private void ItkzConfigurationForm_Load(object sender, EventArgs e)
        {
            if (Device.AutoloadConfig)
            {
                this.StartLoad();
            }
        }

        private void StartLoad()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._device.Config.LoadStruct();
            this._device.Ustavki.LoadStruct();
        }

        private void _readBtn_Click(object sender, EventArgs e)
        {
            StartRead();
        }

        private void StartRead()
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
                _device.Ustavki.LoadStruct();
        }
        private void _writeBtn_Click(object sender, EventArgs e)
        {
            WriteConfig();
        }

        private void WriteConfig()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            string str;
            if (_ustavkiValidator.Check(out str, true))
            {
                _device.Ustavki.Value = _ustavkiValidator.Get();
                _device.Ustavki.SaveStruct();
            }
            else
            {
                MessageBox.Show("Введены неверные уставки. Невозможно записать конфигурацию", "Внимание",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void _readDevNumBtn_Click(object sender, EventArgs e)
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
                _device.Config.LoadStruct();
        }

        private void _acceptBtn_Click(object sender, EventArgs e)
        {
            string str;
            if (_devNumValidator.Check(out str, true))
            {
                _device.Config.Value = _devNumValidator.Get();
                _device.Config.SaveStruct();
            }
            else
            {
                MessageBox.Show("Проверьте правильность введенного номера и скорости устройства", "Внимание",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void readFromFileBtn_Click(object sender, EventArgs e)
        {
            ReadFromFile();
        }

        private void ReadFromFile()
        {
            if (openFileDialog.ShowDialog() != DialogResult.OK) return;
            Deserialize(openFileDialog.FileName, "ITKZ");
        }
        private void writeToFileBtn_Click(object sender, EventArgs e)
        {
            SaveinFile();
        }

        private void SaveinFile()
        {
            if (saveFileDialog.ShowDialog() != DialogResult.OK) return;
            string message;
            if (_ustavkiValidator.Check(out message, true) && _devNumValidator.Check(out message, true))
            {
                var ustavki = (StructBase)_ustavkiValidator.Get();
                var devNum = (StructBase)_devNumValidator.Get();
                Serialize(saveFileDialog.FileName, ustavki, devNum, "ITKZ");
            }
            else
            {
                MessageBox.Show("Обнаружены неккоректные уставки. Конфигурация не может быть сохранена.",
                    "Сохранение уставок", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void Serialize(string binFileName, StructBase config, StructBase devNum, string head)
        {
            try
            {
                var doc = new XmlDocument();
                doc.AppendChild(doc.CreateElement(head));

                List<ushort> values = new List<ushort>(config.GetValues());
                values.AddRange(devNum.GetValues());

                XmlElement element = doc.CreateElement(string.Format("{0}_SET_POINTS", head));
                element.InnerText = Convert.ToBase64String(Common.TOBYTES(values.ToArray(), false));
                if (doc.DocumentElement == null)
                {
                    throw new NullReferenceException();
                }
                doc.DocumentElement.AppendChild(element);
                doc.Save(binFileName);

                MessageBox.Show("Запись конфигурации прошла успешно", "Запись конфигурации", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            catch
            {
                MessageBox.Show("Ошибка записи конфигурации", "Сохранение крнфигурации", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private void Deserialize(string binFileName, string head)
        {
            try
            {
                var doc = new XmlDocument();
                doc.Load(binFileName);

                var a = doc.FirstChild.SelectSingleNode(string.Format("{0}_SET_POINTS", head));
                if (a == null)
                    throw new NullReferenceException();

                var values = Convert.FromBase64String(a.InnerText);
                int ustLen = this._ustavkiValidator.Get().GetValues().Length * 2;
                int devNumLen = this._devNumValidator.Get().GetValues().Length * 2;

                byte[] ustavkiValues = new byte[ustLen];
                byte[] devNumValues = new byte[devNumLen];

                Array.ConstrainedCopy(values, 0, ustavkiValues, 0, ustLen);
                Array.ConstrainedCopy(values, ustLen, devNumValues, 0, devNumLen);

                UstavkiStruct ustavki = new UstavkiStruct();
                ustavki.InitStruct(ustavkiValues);
                _ustavkiValidator.Set(ustavki);
                ConfigStruct devNum = new ConfigStruct();
                devNum.InitStruct(devNumValues);
                _devNumValidator.Set(devNum);

                MessageBox.Show("Файл конфигурации загружен успешно", "Загрузка конфигурации", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            catch
            {
                MessageBox.Show("Невозможно загрузить файл конфигурации", "Загрузка конфигурации", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }
        
        private void ITKZConfigurationForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers != Keys.Control) return;
            switch (e.KeyCode)
            {
                case Keys.W:
                    this.WriteConfig();
                    break;
                case Keys.R:
                    this.StartLoad();
                    break;
                case Keys.S:
                    this.SaveinFile();
                    break;
                case Keys.O:
                    this.ReadFromFile();
                    break;
            }
            e.Handled = true;
        }
        #endregion



        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(Itkz); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(ItkzConfigurationForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return ITKZ.Properties.Resources.config.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Конфигурация"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }


        #endregion

        private void contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ((ContextMenuStrip)sender).Close();
            if (e.ClickedItem == this.readFromDeviceItem)
            {
                this.StartRead();
                return;
            }
            if (e.ClickedItem == this.writeToDeviceItem)
            {
                this.WriteConfig();
                return;
            }
            if (e.ClickedItem == this.readfromFileItem)
            {
                this.ReadFromFile();
                return;
            }
            if (e.ClickedItem == this.writeToFileItem)
            {
                this.SaveinFile();
                return;
            }

        }

        private void contextMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.readFromDeviceItem.Enabled =
                this.writeToDeviceItem.Enabled = this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode;
        }
    }
}
