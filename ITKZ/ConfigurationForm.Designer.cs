﻿namespace ITKZ
{
    partial class ItkzConfigurationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._fazaC = new System.Windows.Forms.MaskedTextBox();
            this._writeBtn = new System.Windows.Forms.Button();
            this.readFromFileBtn = new System.Windows.Forms.Button();
            this.writeToFileBtn = new System.Windows.Forms.Button();
            this._readBtn = new System.Windows.Forms.Button();
            this._fazaB = new System.Windows.Forms.MaskedTextBox();
            this._fazaA = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this._baundRateCmb = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this._devNumBox = new System.Windows.Forms.MaskedTextBox();
            this._readDevNumBtn = new System.Windows.Forms.Button();
            this._acceptBtn = new System.Windows.Forms.Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.readFromDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readfromFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.contextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._fazaC);
            this.groupBox1.Controls.Add(this._writeBtn);
            this.groupBox1.Controls.Add(this.readFromFileBtn);
            this.groupBox1.Controls.Add(this.writeToFileBtn);
            this.groupBox1.Controls.Add(this._readBtn);
            this.groupBox1.Controls.Add(this._fazaB);
            this.groupBox1.Controls.Add(this._fazaA);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(193, 202);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Уставки срабатывания от макс. значения, (0,05...1)Imax";
            // 
            // _fazaC
            // 
            this._fazaC.Location = new System.Drawing.Point(28, 74);
            this._fazaC.Mask = "CCCC";
            this._fazaC.Name = "_fazaC";
            this._fazaC.PromptChar = ' ';
            this._fazaC.Size = new System.Drawing.Size(53, 20);
            this._fazaC.TabIndex = 8;
            // 
            // _writeBtn
            // 
            this._writeBtn.Location = new System.Drawing.Point(6, 127);
            this._writeBtn.Name = "_writeBtn";
            this._writeBtn.Size = new System.Drawing.Size(156, 23);
            this._writeBtn.TabIndex = 1;
            this._writeBtn.Text = "Записать в устройство";
            this.toolTip1.SetToolTip(this._writeBtn, "Записать конфигурацию в устройство (CTRL+W)");
            this._writeBtn.UseVisualStyleBackColor = true;
            this._writeBtn.Click += new System.EventHandler(this._writeBtn_Click);
            // 
            // readFromFileBtn
            // 
            this.readFromFileBtn.Location = new System.Drawing.Point(6, 151);
            this.readFromFileBtn.Name = "readFromFileBtn";
            this.readFromFileBtn.Size = new System.Drawing.Size(156, 23);
            this.readFromFileBtn.TabIndex = 6;
            this.readFromFileBtn.Text = "Загрузить из файла";
            this.toolTip1.SetToolTip(this.readFromFileBtn, "Загрузить конфигурацию из файла (CTRL+O)");
            this.readFromFileBtn.UseVisualStyleBackColor = true;
            this.readFromFileBtn.Click += new System.EventHandler(this.readFromFileBtn_Click);
            // 
            // writeToFileBtn
            // 
            this.writeToFileBtn.Location = new System.Drawing.Point(6, 175);
            this.writeToFileBtn.Name = "writeToFileBtn";
            this.writeToFileBtn.Size = new System.Drawing.Size(156, 23);
            this.writeToFileBtn.TabIndex = 6;
            this.writeToFileBtn.Text = "Сохранить в файл";
            this.toolTip1.SetToolTip(this.writeToFileBtn, "Сохранить конфигурацию в файл (CTRL+S)");
            this.writeToFileBtn.UseVisualStyleBackColor = true;
            this.writeToFileBtn.Click += new System.EventHandler(this.writeToFileBtn_Click);
            // 
            // _readBtn
            // 
            this._readBtn.Location = new System.Drawing.Point(6, 103);
            this._readBtn.Name = "_readBtn";
            this._readBtn.Size = new System.Drawing.Size(156, 23);
            this._readBtn.TabIndex = 6;
            this._readBtn.Text = "Прочитать из устройства";
            this.toolTip1.SetToolTip(this._readBtn, "Прочитать конфигурацию из устройства (CTRL+R)");
            this._readBtn.UseVisualStyleBackColor = true;
            this._readBtn.Click += new System.EventHandler(this._readBtn_Click);
            // 
            // _fazaB
            // 
            this._fazaB.Location = new System.Drawing.Point(28, 52);
            this._fazaB.Mask = "CCCC";
            this._fazaB.Name = "_fazaB";
            this._fazaB.PromptChar = ' ';
            this._fazaB.Size = new System.Drawing.Size(53, 20);
            this._fazaB.TabIndex = 7;
            // 
            // _fazaA
            // 
            this._fazaA.Location = new System.Drawing.Point(28, 30);
            this._fazaA.Mask = "CCCC";
            this._fazaA.Name = "_fazaA";
            this._fazaA.PromptChar = ' ';
            this._fazaA.Size = new System.Drawing.Size(53, 20);
            this._fazaA.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(16, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Ic";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(16, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Ib";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(16, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ia";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Controls.Add(this._readDevNumBtn);
            this.groupBox2.Controls.Add(this._acceptBtn);
            this.groupBox2.Location = new System.Drawing.Point(211, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(90, 202);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Параметры";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this._baundRateCmb);
            this.groupBox4.Location = new System.Drawing.Point(6, 74);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(78, 52);
            this.groupBox4.TabIndex = 5;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Скорость";
            // 
            // _baundRateCmb
            // 
            this._baundRateCmb.FormattingEnabled = true;
            this._baundRateCmb.Location = new System.Drawing.Point(6, 19);
            this._baundRateCmb.Name = "_baundRateCmb";
            this._baundRateCmb.Size = new System.Drawing.Size(65, 21);
            this._baundRateCmb.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this._devNumBox);
            this.groupBox3.Location = new System.Drawing.Point(6, 19);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(78, 49);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Номер";
            // 
            // _devNumBox
            // 
            this._devNumBox.Location = new System.Drawing.Point(6, 19);
            this._devNumBox.Name = "_devNumBox";
            this._devNumBox.Size = new System.Drawing.Size(66, 20);
            this._devNumBox.TabIndex = 1;
            // 
            // _readDevNumBtn
            // 
            this._readDevNumBtn.Location = new System.Drawing.Point(6, 148);
            this._readDevNumBtn.Name = "_readDevNumBtn";
            this._readDevNumBtn.Size = new System.Drawing.Size(75, 23);
            this._readDevNumBtn.TabIndex = 0;
            this._readDevNumBtn.Text = "Считать";
            this._readDevNumBtn.UseVisualStyleBackColor = true;
            this._readDevNumBtn.Click += new System.EventHandler(this._readDevNumBtn_Click);
            // 
            // _acceptBtn
            // 
            this._acceptBtn.Location = new System.Drawing.Point(6, 173);
            this._acceptBtn.Name = "_acceptBtn";
            this._acceptBtn.Size = new System.Drawing.Size(75, 23);
            this._acceptBtn.TabIndex = 0;
            this._acceptBtn.Text = "Установить";
            this._acceptBtn.UseVisualStyleBackColor = true;
            this._acceptBtn.Click += new System.EventHandler(this._acceptBtn_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "ИТКЗ уставки";
            this.openFileDialog.Filter = "(*.xml) | *.xml";
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.FileName = "ИТКЗ уставки";
            this.saveFileDialog.Filter = "(*.xml) | *.xml";
            // 
            // contextMenu
            // 
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.readFromDeviceItem,
            this.writeToDeviceItem,
            this.readfromFileItem,
            this.writeToFileItem});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(213, 92);
            this.contextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenu_Opening);
            this.contextMenu.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.contextMenu_ItemClicked);
            // 
            // readFromDeviceItem
            // 
            this.readFromDeviceItem.Name = "readFromDeviceItem";
            this.readFromDeviceItem.Size = new System.Drawing.Size(212, 22);
            this.readFromDeviceItem.Text = "Прочитать из устройства";
            // 
            // writeToDeviceItem
            // 
            this.writeToDeviceItem.Name = "writeToDeviceItem";
            this.writeToDeviceItem.Size = new System.Drawing.Size(212, 22);
            this.writeToDeviceItem.Text = "Записать в устройство";
            // 
            // readfromFileItem
            // 
            this.readfromFileItem.Name = "readfromFileItem";
            this.readfromFileItem.Size = new System.Drawing.Size(212, 22);
            this.readfromFileItem.Text = "Загрузить из файла";
            // 
            // writeToFileItem
            // 
            this.writeToFileItem.Name = "writeToFileItem";
            this.writeToFileItem.Size = new System.Drawing.Size(212, 22);
            this.writeToFileItem.Text = "Записать в файл";
            // 
            // ItkzConfigurationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(313, 226);
            this.ContextMenuStrip = this.contextMenu;
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ItkzConfigurationForm";
            this.Text = "ConfigurationForm";
            this.Load += new System.EventHandler(this.ItkzConfigurationForm_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ITKZConfigurationForm_KeyUp);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.contextMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button _readBtn;
        private System.Windows.Forms.Button _writeBtn;
        private System.Windows.Forms.MaskedTextBox _fazaC;
        private System.Windows.Forms.MaskedTextBox _fazaB;
        private System.Windows.Forms.MaskedTextBox _fazaA;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button _acceptBtn;
        private System.Windows.Forms.MaskedTextBox _devNumBox;
        private System.Windows.Forms.Button _readDevNumBtn;
        private System.Windows.Forms.Button readFromFileBtn;
        private System.Windows.Forms.Button writeToFileBtn;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox _baundRateCmb;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem readFromDeviceItem;
        private System.Windows.Forms.ToolStripMenuItem writeToDeviceItem;
        private System.Windows.Forms.ToolStripMenuItem readfromFileItem;
        private System.Windows.Forms.ToolStripMenuItem writeToFileItem;
    }
}