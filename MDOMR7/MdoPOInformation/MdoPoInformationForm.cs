﻿using System;
using System.Drawing;
using System.Globalization;
using System.Text;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Framework;
using BEMN.Interfaces;
using BEMN.MDOMR7.Properties;

namespace BEMN.MDOMR7.MdoPOInformation
{
    public partial class MdoPoInformationForm : Form, IFormView
    {
        #region [Private fields]
        private const string UPDATE_FAIL = "Невозможно обновить ПО. Проверте связь.";
        private const string VERSION_READ_FAIL = "Нет связи с устройством";
        private const string UNICON_CLOSE = "Для обновления ПО \"Уникон\" будет закрыт\nПродолжить?";
        private const string PO_UPDATE = "Обновление ПО";
        private const string VERSION_PO = "Версия ПО - ";
        private readonly MemoryEntity<OneWordStruct> _version;
        private readonly MemoryEntity<OneWordStruct> _update;
        private MDOMR7 _device;
        #endregion [Private fields]


        #region [Ctor's]
        public MdoPoInformationForm()
        {
            this.InitializeComponent();
        }

        public MdoPoInformationForm(MDOMR7 device)
        {
            this.InitializeComponent();
            this._device = device;
            this._version = device.VersionEntity;
            this._version.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.VersionReadOk);
            this._version.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.VersionReadFail);
            this._update = device.UpdateEntity;
            this._update.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.UpdateOk);
            this._update.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.UpdateFail);
            this._device.ConnectionModeChanged += this._version.LoadStruct;
        }
        #endregion [Ctor's]


        #region [Memory Entity events]
        private void UpdateFail()
        {
            MessageBox.Show(UPDATE_FAIL);
        }

        private void UpdateOk()
        {
            Framework.Framework.MainWindow.Close();
            
        }

        private void VersionReadFail()
        {
            this._versionLabel.Text = VERSION_READ_FAIL;
            this._updateButton.Enabled = false;
        }

        private void VersionReadOk()
        {
            try
            {
                var value = new StringBuilder(this._version.Value.Word.ToString(CultureInfo.InvariantCulture));
                value.Insert(1, ".");
                value.Insert(3, ".");
                this._versionLabel.Text = VERSION_PO + value;
            }
            catch (Exception)
            {

                this._versionLabel.Text = "Ошибка определения версии";
            }
        }
        #endregion [Memory Entity events]


        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MDOMR7); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(MdoPoInformationForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.Calibrate; }
        }

        public string NodeName
        {
            get { return "Информация ПО"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion


        #region [Event's handlers]
        private void MdoPoInformationForm_Load(object sender, EventArgs e)
        {
            this._device.DeviceCanNumberChanged += i =>
            {
                if (this.IsHandleCreated)
                    Invoke(new Action(() => this.Text = this._device.CreateFormCaption(this, i)));
            };

            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
                this._version.LoadStruct();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show(UNICON_CLOSE, PO_UPDATE, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                this._update.Value = new OneWordStruct { Word = 1 };
                this._update.SaveStruct();
            }
        }
        #endregion [Event's handlers]
    }
}
