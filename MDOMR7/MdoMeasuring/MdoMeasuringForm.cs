﻿using System;
using System.Collections;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms;
using BEMN.Interfaces;
using BEMN.MDOMR7.MdoMeasuring.Structures;
using BEMN.MDOMR7.Properties;
using BEMN.MDOMR7.SomeStructures;
using BEMN.MDOMR7.SupportClasses;

namespace BEMN.MDOMR7.MdoMeasuring
{
    public partial class MdoMeasuringForm : Form, IFormView
    {
        #region Константы
        private const string KVINT_FINISHED = "Квитирование завершено";
        private const string DEFAULT_DATE = "00000000";
        private const string DEFAULT_CLOCK = "000000000";
        private const string ZERO_SYS = "0";
        #endregion

        private MDOMR7 _device;
        private LedControl[] _ch1Leds;
        private LedControl[] _ch2Leds;
        private LedControl[] _ch3Leds;
        private LedControl[] _errorsRelayLeds;
        private CommandStruct _cm;
        private readonly MemoryEntity<OneWordStruct> _stopTime;
        private readonly MemoryEntity<RamStruct> _ram;
        private readonly MemoryEntity<DatetimeStruct> _dateTime;
        private readonly MemoryEntity<CommandStruct> _command;
        private CommandStates _cmSt;

        public MdoMeasuringForm()
        {
            this.InitializeComponent();
        }

        public MdoMeasuringForm(MDOMR7 device)
        {
            this.InitializeComponent();
            this._device = device;
            this._device.ConnectionModeChanged += this.StartStopLoad;
            this._stopTime = device.StopTime;
            this._ram = this._device.Ram;
            this._dateTime = this._device.DateTimeStr;
            this._command = this._device.CommandStr;
            this._cmSt = this._device.CommandState;
            this._ram.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadMeasuring);
            this._ram.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.RamMemStructFail);
            this._command.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.KvintWriteOk);
            this._command.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.CommandStructRead);
        }

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MDOMR7); }
        }

        public bool Multishow { get; private set; }

        #endregion

        #region INodeView Members

        public Type ClassType
        {
            get { return typeof(MdoMeasuringForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.measuring.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Состояния"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        #region Измерения
        public void PrepareMeasuring()
        {
            this._CH1Min.Text = ZERO_SYS;
            this._CH2Min.Text = ZERO_SYS;
            this._CH3Min.Text = ZERO_SYS;
        }

        public void ReadMeasuring()
        {
            this._CH1Min.Text = this._ram.Value.Ch1Min.ToString(CultureInfo.InvariantCulture);
            this._CH2Min.Text = this._ram.Value.Ch2Min.ToString(CultureInfo.InvariantCulture);
            this._CH3Min.Text = this._ram.Value.Ch3Min.ToString(CultureInfo.InvariantCulture);

            this.SetSensor(this._ch1Leds, this._ram.Value.Ch1Sygnals);
            this.SetSensor(this._ch2Leds, this._ram.Value.Ch2Sygnals);
            this.SetSensor(this._ch3Leds, this._ram.Value.Ch3Sygnals);

            LedManager.SetLeds(this._errorsRelayLeds, this._ram.Value.ErrorsRelaySygnals);
        }
        #endregion
        
        #region Дополнительные функции
        private void SetSensor(LedControl[] views, BitArray values)
        {
            if (values[4])
            {
                LedManager.SetLeds(views, values);
                views[0].SetState(false);
            }
            else
            {
                LedManager.TurnOffLeds(views);
            }
        }

        private void StartStopLoad()
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._device.RefreshNumber();
                this._ram.LoadStructCycle();
                this._dateTime.LoadStructCycle();
            }
            else
            {
                this._ram.RemoveStructQueries();
                this._dateTime.RemoveStructQueries();
                this.RamMemStructFail();
            }
        }

        private void CommandStructRead()
        {
            if (this._cmSt == CommandStates.KVINT)
            {
                if (this.CheckKvint())
                {
                    this._command.RemoveStructQueries();
                    this.SaveCommand(CommandStates.NONE);
                    this.kvint.Enabled = true;
                    MessageBox.Show(KVINT_FINISHED);
                }
            }
        }
        
        private void RamMemStructFail()
        {
            this.PrepareMeasuring();
            LedManager.TurnOffLeds(this._ch1Leds);
            LedManager.TurnOffLeds(this._ch2Leds);
            LedManager.TurnOffLeds(this._ch3Leds);
            LedManager.TurnOffLeds(this._errorsRelayLeds);
        }

        #endregion

        private void MDO_Measuring_Load(object sender, EventArgs e)
        {
            this._device.DeviceCanNumberChanged += i => 
                Invoke(new Action(() => Text = this._device.CreateFormCaption(this, i)));
            this._ch1Leds = new[] {this._CH1Enabled, this._CH1Light, this._CH1Arc, this._CH1Error };
            this._ch2Leds = new[] {this._CH2Enabled, this._CH2Light, this._CH2Arc, this._CH2Error };
            this._ch3Leds = new[] {this._CH3Enabled, this._CH3Light, this._CH3Arc, this._CH3Error };
            this._errorsRelayLeds = new[] {this._Error5V, new LedControl(), this._relay1, this._relay2, this._relay3, new LedControl(), this._ErrorTestOn, new LedControl() };
            this.PrepareMeasuring();
            this.StartStopLoad();
        }

        private void MDO_Measuring_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._ram.RemoveStructQueries();
            this._dateTime.RemoveStructQueries();
            this._command.RemoveStructQueries();
            this._device.ConnectionModeChanged -= this.StartStopLoad;
        }

        #region Команда
        public void Kvintir()
        {
            this.SaveCommand(CommandStates.KVINT);
        }

        public bool CheckKvint()
        {
            return this._command.Value.Kvint == 0;
        }

        public void SaveCommand(CommandStates command)
        {
            this._cmSt = command;
            if (this._cmSt != CommandStates.NONE)
            {
                this._cm.Kvint = (ushort)command;
                this._command.Value = this._cm;
                this._command.SaveStruct();
            }
        }

        #endregion Команда

        private void kvint_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.Kvintir();
            this.kvint.Enabled = false;
        }

        public void KvintWriteOk()
        {
            if (this._cmSt == CommandStates.KVINT)
            {
                this._command.LoadStructCycle();
            }
        }
    }
}
