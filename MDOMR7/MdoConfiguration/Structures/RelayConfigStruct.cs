﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;
using BEMN.MDO.SupportClasses;

namespace BEMN.MDOMR7.MdoConfiguration.Structures
{
    public class RelayConfigStruct : StructBase
    {
        //тип в обоих режимах, время импульса в обычном режиме
        [Layout(0)] private ushort _releType;
        
        //время импульса в автономном режиме, сигнал реле в обычном режиме
        [Layout(1)] private ushort _relePulseOfflineTime;

        public byte ReleType
        {
            get { return Common.LOBYTE(this._releType); }
            set
            {
                byte pulseTime = Common.HIBYTE(this._releType);
                this._releType = Common.TOWORD(pulseTime, value);
            }
        }

        #region Реле конфигурация
        public string RelayTypeStr
        {
            get { return Validator.Get(this.ReleType, Strings.SygnalType, 1); }
            set { this.ReleType = (byte)Validator.Set(value, Strings.SygnalType, this.ReleType, 1); }
        }

        public ushort RelePulseOfflineTime
        {
            get
            {
                byte relePulseOfflineTime = Common.LOBYTE(this._relePulseOfflineTime);
                return (ushort)(relePulseOfflineTime * 10);
            }
            set
            {
                byte hiByte = Common.HIBYTE(this._relePulseOfflineTime);
                byte relePulseOfflineTime = (byte)(value / 10);
                this._relePulseOfflineTime = Common.TOWORD(hiByte, relePulseOfflineTime);
            }
        } 
        #endregion
    }
}