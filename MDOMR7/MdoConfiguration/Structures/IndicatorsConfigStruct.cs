﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;
using BEMN.MDO.SupportClasses;

namespace BEMN.MDOMR7.MdoConfiguration.Structures
{
    public class IndicatorsConfigStruct : StructBase
    {
        //тип в обоих режимах, время импульса в обычном режиме
        [Layout(0)] private ushort _indType;
        
        //время импульса в автономном режиме, сигнал реле в обычном режиме
        [Layout(1)] private ushort _indPulseOfflineTime;

        public byte IndType
        {
            get { return Common.LOBYTE(this._indType); }
            set
            {
                byte pulseTime = Common.HIBYTE(this._indType);
                this._indType = Common.TOWORD(pulseTime, value);
            }
        }

        #region Реле конфигурация
        public string IndTypeStr
        {
            get { return Validator.Get(this.IndType, Strings.SygnalType, 1); }
            set { this.IndType = (byte)Validator.Set(value, Strings.SygnalType, this.IndType, 1); }
        }

        public ushort IndPulseOfflineTime
        {
            get
            {
                byte relePulseOfflineTime = Common.LOBYTE(this._indPulseOfflineTime);
                return (ushort)(relePulseOfflineTime);
            }
            set
            {
                byte hiByte = Common.HIBYTE(this._indPulseOfflineTime);
                byte relePulseOfflineTime = (byte)value;
                this._indPulseOfflineTime = Common.TOWORD(hiByte, relePulseOfflineTime);
            }
        } 
        #endregion
    }
}