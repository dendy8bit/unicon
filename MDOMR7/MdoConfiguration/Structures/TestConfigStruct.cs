﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MDOMR7.MdoConfiguration.Structures
{
    public class TestConfigStruct : StructBase
    {
        [Layout(0)] private ushort _testOn;
        [Layout(1)] private ushort _testPeriod;

        public ushort TestOn
        {
            get { return this._testOn; }
            set { this._testOn = value; }
        }

        public ushort TestPeriod
        {
            get { return this._testPeriod; }
            set { this._testPeriod = value; }
        }
    }
}