﻿namespace BEMN.MDOMR7.SupportClasses
{
    public enum CommandStates
    {
        NONE = 0,
        KVINT = 1,
        START_WRITE_FLASH_MEMORY = 2,
        END_WRITE_FLASH_MEMORY = 3
    };
}