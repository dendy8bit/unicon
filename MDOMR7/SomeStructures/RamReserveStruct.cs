﻿using System;
using System.Runtime.InteropServices;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;

namespace BEMN.MDOMR7.SomeStructures
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct RamReserveStruct : IStruct, IStructInit
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 239)]
        public ushort[] reserve;

        public RamReserveStruct(bool a)
        {
            this.reserve = new ushort[239];
        }

        public StructInfo GetStructInfo(int slotLen)
        {
            return StructHelper.GetStructInfo(this.GetType(), slotLen);
        }

        public object GetSlots(ushort start, bool slotArray, int slotLen)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType(), slotLen);
        }

        public void InitStruct(byte[] array)
        {
            throw new NotImplementedException();
        }

        public ushort[] GetValues()
        {
            throw new NotImplementedException();
        }
    }
}