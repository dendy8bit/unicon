﻿using System.Collections.Generic;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.MBServer;

namespace BEMN.MDOMR7.SomeStructures
{
    public struct CommandStruct : IStruct, IStructInit
    {
        public ushort Kvint;

        public StructInfo GetStructInfo(int slotLen)
        {
            return StructHelper.GetStructInfo(this.GetType(), slotLen);
        }

        public object GetSlots(ushort start, bool slotArray, int slotLen)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType(), slotLen);
        }

        public void InitStruct(byte[] array)
        {
            this.Kvint = Common.TOWORD(array[1], array[0]);
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.Add(this.Kvint);
            return result.ToArray();
        }
    }
}