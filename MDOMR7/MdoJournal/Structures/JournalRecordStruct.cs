﻿using System.Collections.Generic;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.Devices.Structures;
using BEMN.MBServer;
using BEMN.MDO.SupportClasses;

namespace BEMN.MDOMR7.MdoJournal.Structures
{
    public struct JournalRecordStruct : IStruct, IStructInit
    {
        public ushort number; // number of record
        public ushort msg; // message
        public ushort journalVol; // value of message

        public string JournalRecordMessage
        {
            get { return this.msg <= Strings.JournalMessageType.Count ? Strings.JournalMessageType[this.msg] : this.msg.ToString(); }
        }

        public int JournalRecordMessageNumber
        {
            get { return this.msg; }
        }

        public int JournalVolume
        {
            get { return this.journalVol; }
        }

        public StructInfo GetStructInfo(int len = StructBase.MAX_SLOT_LENGHT_DEFAULT)
        {
            return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool slotArray, int slotLength)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType());
        }

        public void InitStruct(byte[] array)
        {
            int index = 0;

            this.number = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.msg = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.journalVol = Common.TOWORD(array[index + 1], array[index]);
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.Add(this.number);
            result.Add(this.msg);
            result.Add(this.journalVol);
            return result.ToArray();
        }
    }
}