﻿using System.Collections.Generic;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.MBServer;

namespace BEMN.MDOMR7.MdoJournal.Structures
{
    public struct JournalInfoStruct : IStruct, IStructInit
    {
        public ushort JournalRecordCount;
        public ushort JournalRecordEnd;

        public StructInfo GetStructInfo(int slotLen)
        {
            return StructHelper.GetStructInfo(GetType(), slotLen);
        }

        public object GetSlots(ushort start, bool slotArray, int slotLen)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType(), slotLen);
        }

        public void InitStruct(byte[] array)
        {
            int _index = 0;
            this.JournalRecordCount = Common.TOWORD(array[_index + 1], array[_index]);
            _index += sizeof(ushort);

            this.JournalRecordEnd = Common.TOWORD(array[_index + 1], array[_index]);
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.Add(this.JournalRecordCount);
            result.Add(this.JournalRecordEnd);
            return result.ToArray();
        }
    }
}