﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;

namespace BEMN.MDOMR7.MdoJournal.Structures
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct JournalStruct : IStruct, IStructInit
    {
        public const int JOURNAL_RECORDS_COUNT = 400;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = JOURNAL_RECORDS_COUNT)]
        public JournalRecordStruct[] records;

        public JournalStruct(bool a)
        {
            this.records = new JournalRecordStruct[JOURNAL_RECORDS_COUNT];
        }

        public StructInfo GetStructInfo(int len)
        {
            return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool slotArray, int slotLength)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType());
        }

        public void InitStruct(byte[] array)
        {
            this.records = new JournalRecordStruct[JOURNAL_RECORDS_COUNT];

            byte[] oneStruct;
            int index = 0;
            for (int i = 0; i < this.records.Length; i++)
            {
                oneStruct = new byte[Marshal.SizeOf(typeof(JournalRecordStruct))];
                Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
                this.records[i].InitStruct(oneStruct);
                index += Marshal.SizeOf(typeof(JournalRecordStruct));
            }
        }

        public ushort[] GetValues()
        {
            var result = new List<ushort>();
            for (var j = 0; j < this.records.Length; j++)
            {
                result.AddRange(this.records[j].GetValues());
            }
            return result.ToArray();
        }
    }
}