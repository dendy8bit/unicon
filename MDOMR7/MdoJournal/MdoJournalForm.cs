﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Interfaces;
using BEMN.MDOMR7.MdoJournal.Structures;
using BEMN.MDOMR7.Properties;

namespace BEMN.MDOMR7.MdoJournal
{
    public partial class MdoJournalForm : Form, IFormView
    {
        #region Константы
        private const string NUMBER = "Номер";
        private const string TIME = "Время";
        private const string MESSAGE = "Сообщение";
        private const string FIND_MESSAGES_PATTERN = "Найдено {0} сообщений";
        private const string MDO_SJ_SYS = "МДО_журнал_системы";
        private const string JOURNAL_READ_FAIL = "Невозможно прочитать журнал";
        private const string MDO_JOURNAL_FILE_IS_DAMAGED = "Файл не содержит записей журнала МДО-МР7";
        private const string FROM_REMOTE = "С пульта";
        private const string SDTU = "СДТУ";
        private const string MDO_PATTERN = "МДО {0}";
        private const string CRC_STATE = "CRC уставок";
        private const string DISCRET_ERROR = "Ошибка дискрета";
        private const string CLOCK_ERROR = "Ошибка часов";
        #endregion

        #region Поля
        private readonly MDOMR7 _device;
        private List<JournalRecordStruct> _journal = new List<JournalRecordStruct>();
        private int _recsCount;
        private int _endRecIndex;
        private readonly MemoryEntity<JournalInfoStruct> _journalInfo;
        private readonly MemoryEntity<JournalStruct> _journalStruct;
        #endregion

        #region Конструкторы
        public MdoJournalForm()
        {
            this.InitializeComponent();
        }

        public MdoJournalForm(MDOMR7 device)
        {
            this.InitializeComponent();
            this._device = device;

            this._journalInfo = device.JournalInfoStruct;
            this._journalStruct = device.JournalStruct;

            this._journalStruct.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.JournalStructRead);
            this._journalStruct.ReadOk += HandlerHelper.CreateHandler(this, this.ProgressBarInc);

            this._journalStruct.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.JournalReadFail);

            this._journalInfo.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.InfoStructRead);
            this._journalInfo.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.JournalReadFail);
        }
        #endregion
        
        #region Обработчики событий
        //Загрузка формы и инициализация структур
        private void MDO_Journal_Load(object sender, EventArgs e)
        {
            if (_device.IsConnect) return;

            this._device.DeviceCanNumberChanged += i =>
            {
                if (this.IsHandleCreated)
                    Invoke(new Action(() => this.Text = this._device.CreateFormCaption(this, i)));
            };

            this._configProgressBar.Step = 1;
            this.LoadConfigurationBlocks();
        }


        private void _serializeSysJournalBut_Click(object sender, EventArgs e)
        {
            this.SaveJournal();
        }

        private void _deserializeSysJournalBut_Click(object sender, EventArgs e)
        {
            this.LoadJournal();
        }

        //чтение
        private void _readSysJournalBut_Click(object sender, EventArgs e)
        {
            if (_device.IsConnect) this.LoadConfigurationBlocks();
        }
        #endregion Обработчики событий
        
        #region Обработчики событий StObj

        private void JournalStructRead()
        {
            this.ReadJournal();
            this._statusLabel.Text = string.Format(FIND_MESSAGES_PATTERN, this._recsCount);
            this._readSysJournalBut.Enabled = true;
        }

        private void InfoStructRead()
        {
            this.PrepareJournal();
            this._journalStruct.LoadStruct();
            this._configProgressBar.Maximum = this._journalStruct.Slots.Count;
            
        }
        private void ProgressBarInc()
        {
            this._configProgressBar.Increment(1);
        }

        private void JournalReadFail()
        {
            this._statusLabel.Text = JOURNAL_READ_FAIL;
            this._readSysJournalBut.Enabled = true;
        }

        #endregion Обработчики событий StObj

        #region Вспомогательные функции
        //вычисление параметров журнала
        private void PrepareJournal()
        {
            //Количество записей.
            this._recsCount = this._journalInfo.Value.JournalRecordCount;
            //Смещение первой записи.
            this._endRecIndex = this._journalInfo.Value.JournalRecordEnd / new JournalRecordStruct().GetStructInfo().FullSize;
        }

        //Чтение и Вывод записей на экран
        private void ReadJournal()
        {
            this._readSysJournalBut.Enabled = true;
            JournalRecordStruct[] records = this._journalStruct.Value.records;
            this._journal.AddRange(records);

            if (this._endRecIndex - this._recsCount >= 0)
            {
                this._journal = this._journal.GetRange(0, this._recsCount);
                //this._journal.Reverse();
            }
            else
            {
                List<JournalRecordStruct> firstJournal = this._journal.GetRange(0, this._endRecIndex);
                List<JournalRecordStruct> secondJournal = this._journal.GetRange(this._endRecIndex, this._recsCount - this._endRecIndex);
                firstJournal.Reverse();
                secondJournal.Reverse();
                this._journal.Clear();
                this._journal.AddRange(firstJournal);
                this._journal.AddRange(secondJournal);
            }
            
            for (int i = 0; i < this._journal.Count; i++)
            {
                //datatable
                this._sysJournalGrid.Rows.Add((i + 1).ToString("000"), this._journal[i].JournalRecordMessage, this._journal[i].JournalVolume);
            }
        }
        
        //сохранение файла журнала
        private void SaveJournal()
        {
            DataTable table = this.GetDataTable();
            for (int i = 0; i < this._sysJournalGrid.Rows.Count; i++)
            {
                table.Rows.Add(new[]
                    {
                        this._sysJournalGrid["_indexCol", i].Value,
                        this._sysJournalGrid["_msgCol", i].Value,
                        this._sysJournalGrid["_valueCol", i].Value
                    });
            }

            if (DialogResult.OK == this._saveSysJournalDlg.ShowDialog())
            {
                table.WriteXml(this._saveSysJournalDlg.FileName);
            }
        }
        //загрузка файла журнала
        private void LoadJournal()
        {
            var table = this.GetDataTable();

            if (DialogResult.OK == this._openSysJounralDlg.ShowDialog())
            {
                this._sysJournalGrid.Rows.Clear();
                table.ReadXml(this._openSysJounralDlg.FileName);
                if (table.Rows.Count == 0)
                {
                    MessageBox.Show(MDO_JOURNAL_FILE_IS_DAMAGED);
                }
                else
                {
                    for (var i = 0; i < table.Rows.Count; i++)
                    {
                        this._sysJournalGrid.Rows.Add(new[]
                            {
                                table.Rows[i].ItemArray[0],
                                table.Rows[i].ItemArray[1],
                                table.Rows[i].ItemArray[2]
                            });
                    }
                }
            }
        }

        private DataTable GetDataTable()
        {
            var table = new DataTable(MDO_SJ_SYS);
            table.Columns.Add(NUMBER);
            table.Columns.Add(TIME);
            table.Columns.Add(MESSAGE);
            return table;
        }


        //Запуск чтения из устройства
        private void LoadConfigurationBlocks()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._configProgressBar.Value = 0;
            this._readSysJournalBut.Enabled = false;
            this._sysJournalGrid.Rows.Clear();
            this._journal.Clear();
            this._journalInfo.LoadStruct();
        }

        #endregion Вспомогательные функции

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MDOMR7); }
        }

        public bool Multishow { get; private set; }

        #endregion

        #region INodeView Members

        public Type ClassType
        {
            get { return typeof(MdoJournalForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.js; }
        }

        public string NodeName
        {
            get { return "Журнал системы"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion
    }
}
